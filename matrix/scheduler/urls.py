
from django.conf.urls import url, include
from .views import ( ActivityViewSet, SlotViewSet, BlockViewSet,
                     RequestViewSet,TechnicianSCViewSet )

# API section
from customers.urls import router

router.register(r"activities", ActivityViewSet)
router.register(r"slots", SlotViewSet)
router.register(r"blocks", BlockViewSet)
router.register(r"requests", RequestViewSet)
router.register(r"technician-schedules", TechnicianSCViewSet)


urlpatterns = [
    
]
