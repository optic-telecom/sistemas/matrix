from import_export import resources
from .models import (Slot, Block, Request, 
    Technician, Activity, Duration )

class SlotResource(resources.ModelResource):

    class Meta:
        model = Slot

class BlockResource(resources.ModelResource):

    class Meta:
        model = Block

class RequestResource(resources.ModelResource):

    class Meta:
        model = Request

class TechnicianResource(resources.ModelResource):

    class Meta:
        model = Technician

class ActivityResource(resources.ModelResource):

    class Meta:
        model = Activity

class DurationResource(resources.ModelResource):

    class Meta:
        model = Duration