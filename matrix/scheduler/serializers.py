from rest_framework import serializers
from .models import (Slot, Block, Request, 
    Technician, Activity )
from drf_queryfields import QueryFieldsMixin

class SlotSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Slot
        fields = ('block','request')

class BlockSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Block
        fields = ('dt','technician','group')
class RequestSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Request
        fields = ('created_at','contract')

class TechnicianSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Technician
        fields = ('name')

class ActivitySerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Activity
        fields = ('name','description','default_duration','required_skill_set')



