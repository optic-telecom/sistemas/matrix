from .models import (Slot, Block, Request, 
    Technician, Activity )
from .serializers import (SlotSerializer, BlockSerializer,
    RequestSerializer, TechnicianSerializer, ActivitySerializer)
from rest_framework import viewsets
from customers.views import IsSystemInternalPermission
from url_filter.integrations.drf import DjangoFilterBackend

###### API ###############

class SlotViewSet(viewsets.ModelViewSet):
    queryset = Slot.objects.all()
    serializer_class = SlotSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

class BlockViewSet(viewsets.ModelViewSet):
    queryset = Block.objects.all()
    serializer_class = BlockSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

class RequestViewSet(viewsets.ModelViewSet):
    queryset = Request.objects.all()
    serializer_class = RequestSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]


class TechnicianSCViewSet(viewsets.ModelViewSet):
    queryset = Technician.objects.all()
    serializer_class = TechnicianSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

class ActivityViewSet(viewsets.ModelViewSet):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]
