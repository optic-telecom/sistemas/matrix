import pytz, random
from collections import defaultdict
from django.conf import settings
from django.db import models, transaction, IntegrityError
from django.db.models import Count, Case, When, IntegerField
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


MAX_DAILY_BLOCKS = 2
MAX_BLOCK_SLOTS = 3
AM_BLOCK_HOUR = 6 # + 3
PM_BLOCK_HOUR = 11 # + 3


def pick_lazy_technician():
    tx = Technician.objects.annotate(block__count=Count(Case(When(block__dt__gt=timezone.now(),
                                                                  then='block'),
                                                             output_field=IntegerField())))
    ranking = defaultdict(list)
    for t in tx:
        ranking[t.block__count].append(t)
    min_blocks = min(ranking.keys())
    return random.choice(ranking[min_blocks])


# if we don't use the slot, then we're in trouble
# more slots will be created, all empty
# waste
#
# make sure you always delete the slot if not used for a request
#
# HEY, maybe a context manager would do! Just like with file open:
def get_next_slot(group):
    while True:
        now = timezone.now()
        local_blocks = Block.objects.filter(group=group)
        future_blocks = local_blocks.filter(dt__gt=now)
        annotated_blocks = future_blocks.annotate(slot_count=Count('slot'))
        available_blocks = annotated_blocks.filter(slot_count__lt=MAX_BLOCK_SLOTS)
        if available_blocks.count():
            slot = available_blocks.earliest('dt').next_slot
            if slot:
                break
        else:
            pick_lazy_technician().create_next_block(now, group)
    return slot


class Slot(models.Model):
    block = models.ForeignKey('Block')
    request = models.OneToOneField('Request', null=True)

    def __str__(self):
        return str(self.pk)


class Block(models.Model):
    technician = models.ForeignKey('Technician')
    group = models.ForeignKey('infra.Group')
    dt = models.DateTimeField()

    @property
    def next_slot(self):
        with transaction.atomic():
            if self.slot_set.count() < MAX_BLOCK_SLOTS:
                slot = Slot.objects.create(block=self)
                if self.slot_set.count() > MAX_BLOCK_SLOTS:
                    raise IntegrityError
                else:
                    return slot

    def __str__(self):
        return str(self.pk)

    class Meta:
        unique_together = ('technician', 'dt')


class Request(models.Model):
    service = models.ForeignKey('customers.Service')
    created_at = models.DateTimeField(auto_now_add=True)


class Technician(models.Model):
    name = models.CharField(_('name'), max_length=255)

    def create_next_block(self, now, group):
        while True:
            if now.hour < AM_BLOCK_HOUR:
                dt = timezone.datetime(now.year, now.month, now.day, AM_BLOCK_HOUR, tzinfo=pytz.UTC)
            elif AM_BLOCK_HOUR < now.hour < PM_BLOCK_HOUR:
                dt = timezone.datetime(now.year, now.month, now.day, PM_BLOCK_HOUR, tzinfo=pytz.UTC)
            else:
                tomorrow = now + timezone.timedelta(days=1)
                dt = timezone.datetime(tomorrow.year, tomorrow.month, tomorrow.day, AM_BLOCK_HOUR, tzinfo=pytz.UTC)

            try:
                with transaction.atomic():
                    block = Block.objects.create(technician=self,
                                                 group=group,
                                                 dt=dt)
                break
            except IntegrityError:
                now += timezone.timedelta(hours=PM_BLOCK_HOUR-AM_BLOCK_HOUR)

        return block

    def __str__(self):
        return self.name


class Activity(models.Model):
    DURATION_CHOICES = ((0, '30 minutos'),
                        (1, '1 hora'),
                        (2, '2 horas'),
                        (4, '4 horas'),
                        (24, '1 día'),
                        (72, '3 días'),
                        (96, '4 días'),
                        (120, '5 días'),
                        (144, '6 días'),
                        (168, '7 días'),)

    name = models.CharField(_('name'), max_length=255)
    description = models.TextField(_('description'))
    default_duration = models.PositiveSmallIntegerField(_('default duration'), choices=DURATION_CHOICES)
    required_skill_set = models.ManyToManyField('hr.Skill')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('activity')
        verbose_name_plural = _('activities')


class Duration(models.Model):
    activity = models.ForeignKey('Activity', verbose_name=_('activity'))
    node = models.ForeignKey('infra.Node', verbose_name=_('node'))
    duration = models.PositiveSmallIntegerField(_('duration'), choices=Activity.DURATION_CHOICES)
