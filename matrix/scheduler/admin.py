from django.contrib import admin
from .models import (Slot, Block, Request, 
    Technician, Activity, Duration )
from import_export.admin import ImportExportModelAdmin
from .resources import (SlotResource, BlockResource, RequestResource,
    TechnicianResource, DurationResource, ActivityResource )


@admin.register(Activity)
class ActivityAdmin(ImportExportModelAdmin):
    list_display = ('name', 'description', 'default_duration')
    resource_class = ActivityResource

@admin.register(Slot)
class SlotAdmin(ImportExportModelAdmin):
    resource_class = SlotResource

@admin.register(Block)
class BlockAdmin(ImportExportModelAdmin):
    resource_class = BlockResource

@admin.register(Request)
class RequestAdmin(ImportExportModelAdmin):
    resource_class = RequestResource

@admin.register(Technician)
class TechnicianAdmin(ImportExportModelAdmin):
    resource_class = TechnicianResource

@admin.register(Duration)
class DurationAdmin(ImportExportModelAdmin):
    resource_class = DurationResource