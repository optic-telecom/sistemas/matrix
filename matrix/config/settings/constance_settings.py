# https://www.previred.com/web/previred/indicadores-previsionales


SLACK_SETTINGS = {
    "NETWORK_DROP_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "Corte de red",
    ),
    "NETWORK_CONNECT_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "NETWORK_CONNECT_WEBHOOK",
    ),
    "PROSPECT_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "PROSPECT_WEBHOOK",
    ),
    "ARICA_PAYMENTS_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "ARICA_PAYMENTS_WEBHOOK",
    ),
    "ACTIVE_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "ACTIVE_CUSTOMER_WEBHOOK",
    ),
    "PENDING_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "PENDING_CUSTOMER_WEBHOOK",
    ),
    "INACTIVE_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "INACTIVE_CUSTOMER_WEBHOOK",
    ),
    "NOT_INSTALLED_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "NOT_INSTALLED_CUSTOMER_WEBHOOK",
    ),
    "FREE_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "FREE_CUSTOMER_WEBHOOK",
    ),
    "INSTALL_REJECTED_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "INSTALL_REJECTED_CUSTOMER_WEBHOOK",
    ),
    "OFF_PLAN_SALE_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "OFF_PLAN_SALE_CUSTOMER_WEBHOOK",
    ),
    "DELINQUENT_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "DELINQUENT_CUSTOMER_WEBHOOK",
    ),
    "TO_RESCHEDULE_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "TO_RESCHEDULE_CUSTOMER_WEBHOOK",
    ),
    "HOLDER_CHANGE_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "HOLDER_CHANGE_CUSTOMER_WEBHOOK",
    ),
    "DISCARDED_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "DISCARDED_CUSTOMER_WEBHOOK",
    ),
    "ON_HOLD_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "ON_HOLD_CUSTOMER_WEBHOOK",
    ),
    "LOST_CUSTOMER_WEBHOOK": (
        "https://hooks.slack.com/services/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",
        "LOST_CUSTOMER_WEBHOOK",
    ),
    "allow_send_slack": (True, "Permite enviar notificaciones a slack"),
}

CONSTANCE_CONFIG = {
    "CAPITAL": (11.44, "Tasa AFP Capital"),
    "CUPRUM": (11.48, "Tasa AFP Cuprum"),
    "HABITAT": (11.27, "Tasa AFP Habitat"),
    "PLANVITAL": (10.41, "Tasa AFP PlanVital"),
    "PROVIDA": (11.45, "Tasa AFP ProVida"),
    "MODELO": (10.77, "Tasa AFP Model"),
    "UF": (26824.94, "Valor UF"),
    "UTM": (47019, "Valor UTM"),
    "allow_generate_factura": (
        "no",
        "Valor que indica si se crean nuevas Facturas desde matrix",
        "yes_no_select",
    ),
    "month_invoice": ("Abril", "Valor month of invoices"),
    "year_invoice": (2021, "Valor year of invoices"),
    "taxation_address": (
        "https://190.113.247.198:8080/document/",
        "Dirección de facturación",
    ),
    "response_address": (
        "https://190.113.247.198:8000/api/v1/taxation-response/",
        "Dirección de la respuesta de facturación",
    ),
    "user": ("BANDAANCHA", "Usuario de facturación"),
    "password": ("plano91098", "Clave de facturación"),
    "rut": ("1-9", "RUT de la empresa"),
    "port": ("10177", "Puerto"),
    "ICLASS_LOGIN": ("OPTIWS", "Login API ICLASS"),
    "ICLASS_PASSWORD": ("S4wjbx8b", "Password API ICLASS"),
    "iris_server": ("https://iris7.cl/", "Servidor de Iris"),
    "iris_server_devel": ("https://iris.devel7.cl/", "Servidor de Iris Devel"),
    "iris_service_change": (
        "api/v1/communications/trigger_event/{ID}/active/",
        "Api cambio estado servicio",
    ),
    "iris_server_contact": ("https://Backend.iris7.cl/", "Servidor de  Iris para contactos"),
    "iris_server_typify": ("https://Backend.iris7.cl/", "Servidor de  Iris para tipificaciones"),
    "iris_send_invoice_url": (
        "api/v1/communications/trigger_event/{ID}/active/",
        "Url para envio de informacion de boletas",
    ),
    "invoices_to_iris_task_url": (
        "https://iris.devel7.cl/api/v1/communications/trigger_event/active/",
        "Url para el envío de documentos a iris mediante tarea periódica",
    ),
    "invoices_to_iris_task_token": (
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0",
        "Token para el envío de documentos a iris mediante tarea periódica",
    ),
    "iris_create_customer": ("api/v1/customers/customer/", "Api para crear cliente en Iris"),
    "iris_notify_remove_equipament": ("api/v1/formerCustomers/remove_equipament/", "Api para notificar retiro de equipo en Iris"),
    "iris_add_email": ("api/v1/customers/customer/{ID}/add_email/", "Api para asociar email a cliente"),
    "iris_add_phone": ("api/v1/customers/customer/{ID}/add_phone/", "Api para asociar telefono a cliente"),
    "evento_send_invoice":("Evento_Pruebaa", "Nombre evento envio de Boletas a Iris"),
    "evento_change_plan":("Evento_Plan", "Nombre evento notificacion cambio de Plan a Iris"),
    "iris_tipificacion": ("api/v1/tickets/category/?{SEARCH}", "Api de tipificaciones"),
    "iris_tipificaciones_token": (
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU5NTk2MTY0NiwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.cerP4osMCA7DY_r28jObG1D5CBq3i2jSaiw1KpvlKzM",
        "Token para Api de tipificaciones",
    ),
    "sentinel_server": ("https://sentinel7.cl/", "Servidor de Sentinel"),
    "sentinel_token": (
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
        "Token para Apis de Sentinel",
    ),
    "sentinel_get_service_network_url": (
        "api/v1/service/{NUMBER}/network/",
        "Url para obtener de informacion de red de servicio",
    ),
    "aceptacion_contrato_canales": (
        "https://hooks.slack.com/services/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",
        "Canales a los que se enviara si el cliente acepto o no el contrato",
    ),
    "matrix_server": ("http://127.0.0.1:8000", "Servidor de Matrix"),
    "pulso_server": ("https://pulso.multifiber.cl/", "Servidor de pulso"),
    "pulso_token": (
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
        "Token para Apis de Pulso",
    ),
    "search_region": ("region-ac/", "url buscar region en pulso"),
    "search_commune": ("commune-ac/", "url buscar comuna en pulso"),
    "search_street": ("street-ac/", "url buscar calle en pulso"),
    "search_street_location": ("street-location-ac/", "url buscar street location en pulso"),
    "search_complete_location": ("complete-location-ac/", "url buscar complete location en pulso"),
    "number_of_unpaid_invoice": (1, "Cantidad de boletas que se aceptan sin pagar en prorroga"),
    "money_without_paid": (100, "Cantidad que puede deber de una boleta para marcar como pagada"),
    "allow_generate_contract": (
        "yes",
        "Valor que indica si se crean nuevos contratos desde matrix",
        "yes_no_select",
    ),
    "allow_status_validation": (
        "yes",
        "Valor que indica si se debe realizar validación del cambio de status",
        "yes_no_select",
    ),
    "block_slack_status_change": (
        "no",
        "Valor que indica si las notificaciones de Slack al cambiar de estado estan bloqueadas",
    ),
    "allow_send_onboarding_email": (
        "yes",
        "Valor que indica si se debe enviar correo al finalizar onboarding",
        "yes_no_select",
    ),
    "allow_send_whatsapp": (
        "yes", 
        "Valor que indica si se debe enviar whatsapp al 4to dia despues de la activacion",
        "yes_no_select",
    ),
    "allow_promotion_anytime": (
        "no",
        "Valor que indica si se puede crear y asociar promociones en cualquier momento",
        "yes_no_select",
    ),
    "allow_service_order_slack": (
        "yes",
        "Valor que indica si se debe enviar Slack al crear Orden de Servicio",
        "yes_no_select",
    ),
    "allow_send_nuevo_cliente_email": (
        "yes",
        "Valor que indica si se debe enviar correo dos horas después de registrado el cliente",
        "yes_no_select",
    ),
    "allow_send_post_activacion_email": (
        "yes",
        "Valor que indica si se debe enviar correo dos horas después de activado el servicio",
        "yes_no_select",
    ),
    "allow_send_contract_acceptance": (
        "yes",
        "Valor que indica si se debe enviar a slack la aceptacion del contrato",
        "yes_no_select",
    ),
    "evento_cierre_de_ventas": ("nuevo_cliente_email", "Nombre del Evento Cierre de Ventas"),
    "evento_correo_onboarding": ("send_onboarding_email", "Nombre del Evento Fin Onboarding"),
    "evento_correo_contrato": ("bienvenida_cliente", "Nombre del Evento Bienvenida al Cliente"),
    "evento_post_activacion": ("post_activacion_email", "Nombre del evento post activacion"),
    "maximo_export": (500, "Cantidad maxima de registros que se pueden exportar al mismo tiempo en reportes personalizados"),
    "feriados": ("", "Indicar feriados dd/mm/yyyy, separados por comas"),
    "allow_feriados": (
        "no",
        "Valor que indica si se pueden agendar OS en días feriados",
        "yes_no_select",
    ),
    **SLACK_SETTINGS,
}

CONSTANCE_ADDITIONAL_FIELDS = {
    "yes_no_select": [
        "django.forms.fields.ChoiceField",
        {"widget": "django.forms.Select", "choices": (("yes", "Yes"), ("no", "No"))},
    ],
}

CONSTANCE_CONFIG_FIELDSETS = {
    "Indicadores Financieros": (
        "CAPITAL",
        "CUPRUM",
        "HABITAT",
        "PLANVITAL",
        "PROVIDA",
        "MODELO",
        "UF",
        "UTM",
    ),
    "Valores de casos especiales": (
        "number_of_unpaid_invoice",
        "money_without_paid",
    ),
    "Integración IClass": ("ICLASS_LOGIN", "ICLASS_PASSWORD"),
    "Iris": (
        "iris_server",
        "iris_server_devel",
        "iris_service_change",
        "iris_send_invoice_url",
        "iris_tipificacion",
        "iris_tipificaciones_token",
        "invoices_to_iris_task_url",
        "invoices_to_iris_task_token",
        "iris_create_customer",
        "iris_add_email",
        "iris_add_phone",
        "evento_send_invoice",
        "evento_change_plan",
        "iris_notify_remove_equipament",
        "iris_server_contact",
        "iris_server_typify"
    ),
    "Sentinel": (
        "sentinel_server",
        "sentinel_token",
        "sentinel_get_service_network_url",
    ),
    "Pulso": ("pulso_server","pulso_token",
        "search_region","search_commune",
        "search_street","search_street_location",
        "search_complete_location",
        ),
    "Facturacion.cl": (
        "allow_generate_factura",
        "taxation_address",
        "response_address",
        "user",
        "password",
        "rut",
        "port",
    ),
    "Matrix": (
        "matrix_server",
        "month_invoice",
        "year_invoice",
        "allow_generate_contract",
        "allow_status_validation",
        "block_slack_status_change",
        "allow_send_onboarding_email",
        "allow_promotion_anytime",
        "allow_send_whatsapp",
        "allow_service_order_slack",
        "allow_send_nuevo_cliente_email",
        "allow_send_post_activacion_email",
        "evento_cierre_de_ventas",
        "evento_correo_onboarding",
        "evento_correo_contrato",
        "evento_post_activacion",
        "allow_send_contract_acceptance",
        "aceptacion_contrato_canales",
        "maximo_export",
        "feriados",
        "allow_feriados",
        "evento_post_activacion",),
    "slack": SLACK_SETTINGS.keys(),
}
