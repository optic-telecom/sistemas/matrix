from .base import *  # noqa
from .celery_conf import *

ENVIROMENT = "DEV"
DEBUG = True
DEVELOPMENT_APPS = []

INSTALLED_APPS = PREREQ_APPS + PROJECT_APPS + DEVELOPMENT_APPS

ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

STATIC_ROOT = "/home/matrix/matrix_backend/matrix/static/"
MEDIA_ROOT = "/home/matrix/matrix_backend/matrix/media/"

# LOGGING = {
#     'version': 1,
#     'filters': {
#         'require_debug_true': {
#             '()': 'django.utils.log.RequireDebugTrue',
#         },
#     },
#     'handlers': {
#         'console': {
#             'level': 'DEBUG',
#             'filters': ['require_debug_true'],
#             'class': 'logging.StreamHandler',
#         },
#     },
#     'loggers': {
#         'django.db.backends': {
#             'level': 'DEBUG',
#             'handlers': ['console'],
#         },
#     },
# }


LOGGING = {
    "version": 1,
    "formatters": {
        'sqlformat': {
            'format': '{asctime} {message}',
            'style': '{'
        },
        "verbose": {
            "format": "%(name)s: %(message)s"
        },
        'sqlformatter': {
            '()': 'ddquery.SqlFormatter',
            'format': '%(message)s %(asctime)s',
            'reindent': False,
            'highlight': False,
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        'sqlhandler': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'sqlformatter',
        },
        'sqlfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'debug.log',
            'maxBytes': 100000,
            'backupCount': 3,
            'formatter': 'sqlformat',
        }
    },
    "loggers": {
        'django.db.backends': {
            'handlers': ['sqlhandler'],
            'level': 'DEBUG',
        },
        'django.db.backends': {
            'handlers': ['sqlfile'],
            'level': 'DEBUG',
        },
        "zeep.transports": {
            "level": "DEBUG",
            "propagate": True,
            "handlers": ["console"],
        },
        "customers.mikrotik": {
            "level": "DEBUG",
            "propagate": True,
            "handlers": ["console"],
        },
        "matplotlib": {
            "handlers": ["console"],
            "level": "ERROR",
            "propagate": False,
        },
    },
}


try:
    _CONSTANCE_REDIS_CONNECTION = "unix://{}".format(
        os.path.join(os.getenv("REDIS_DIR"), "redis.sock")
    )
except Exception as e:
    pass



SKIP_SENTRY_ERRORS = ['DriveDefinitionModel.DoesNotExist']