import sys
import sentry_sdk

from os.path import dirname, abspath, join
from sentry_sdk.integrations.django import DjangoIntegration
from multifiberpy.sentry_multifiber import send_slack_event
from pathlib import Path
from dotenv import load_dotenv

from .constance_settings import *
from . import get_env_variable

env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)


BASE_DIR = dirname(dirname(abspath(__file__)))

FILE_UPLOAD_PERMISSIONS = 0o644

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_env_variable("SECRET_KEY")

# Sentry
sentry_sdk.init(
    dsn=get_env_variable("DSN"),
    integrations=[DjangoIntegration()],
    before_send=send_slack_event,
    send_default_pii=True,
)

ALLOWED_HOSTS = []

DATE_INPUT_FORMATS = ("%Y-%m-%d", "%d-%m-%Y","%d/%m/%Y")



USE_DEBUG_TOOLBAL = False

# Application definition

PREREQ_APPS = [
    "dal",
    "dal_select2",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.postgres",
    "django.contrib.humanize",
    "django.contrib.admin",
    "django_extensions",
    "taggit",
    "imagekit",
    "localflavor",
    "widget_tweaks",
    "simple_history",
    "easy_select2",
    "django_markdown2",
    "constance",
    "rest_framework",
    "import_export",
    "django_celery_beat",
    "corsheaders",
    #'silk'
]

PROJECT_APPS = [
    "customers",
    "providers",
    "infra",
    "gallery",
    "documents",
    "scheduler",
    "reports",
    "hr",
    "billing",
]


MIDDLEWARE_CLASSES = [
    #'silk.middleware.SilkyMiddleware',    
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.auth.middleware.SessionAuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "simple_history.middleware.HistoryRequestMiddleware",
    "config.middlewares.ActiveUserMiddleware",
    #"config.middlewares.TimeViewsMiddleware",
    'corsheaders.middleware.CorsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = False

BANDAANCHA2 = r'^http://bandaancha.cl/*$'

DEMOGIGA = r'^http://demo-giga.devel7.cl/*$'
DEMOGIGA_SSL = r'^https://demo-giga.devel7.cl/*$'

CORS_ORIGIN_REGEX_WHITELIST  =  [ 
    BANDAANCHA2,
    #DEMOGIGA,
    #DEMOGIGA_SSL
]

CORS_ORIGIN_WHITELIST  =  [ 
    'http://127.0.0.1',
    'http://localhost',
    'https://bandaancha.cl',
    'https://iris.devel7.cl',
    'https://matrix.devel7.cl',
    'https://pomaire.devel7.cl',
    'https://aguila.devel7.cl',
    'https://sentinel.devel7.cl',
    'https://demo-iris.devel7.cl',
    'https://demo-matrix.devel7.cl',
    'https://demo-sentinel.devel7.cl',
    'https://pulso-dev.devel7.cl',
    'http://pulso.multifiber.cl',
    'https://coloradmin.devel7.cl',
    'https://sentinel-coloradmin.devel7.cl',
    'https://optic.matrix2.cl',
    'https://sentinel7.cl',
    'https://iris7.cl',
    'http://gigainternet.cl',
    'https://gigainternet.cl',      
]


CORS_ALLOW_METHODS = [
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
]

#ALLOWED_HOSTS=['*']

CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
]

if USE_DEBUG_TOOLBAL:
    MIDDLEWARE_CLASSES.insert(0, "debug_toolbar.middleware.DebugToolbarMiddleware")
    PREREQ_APPS.append("debug_toolbar")

ROOT_URLCONF = "config.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "customers.context_processors.enviroment",
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "config.wsgi.application"


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases


#DATABASE_ROUTERS = ['config.db.ReplicationRouter']

DATABASES = {

    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": get_env_variable("DB_NAME"),
        "USER": get_env_variable("DB_USER"),
        "PASSWORD": get_env_variable("PASSWORD_DB"),
        "HOST": get_env_variable("DB_HOST", ""),
        "PORT": get_env_variable("DB_PORT", 5432),
        'OPTIONS': {
            'options': '-c statement_timeout={}'.format(get_env_variable("statement_timeout", 7000)),
        }        
    },
    "replica_1": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": get_env_variable("DB_NAME"),
        "USER": get_env_variable("DB_USER"),
        "PASSWORD": get_env_variable("PASSWORD_DB"),
        "HOST": get_env_variable("DB_HOST", "") + '_replica1',
        "PORT": get_env_variable("DB_PORT", 5432),
        'OPTIONS': {
            'options': '-c statement_timeout={}'.format(get_env_variable("statement_timeout", 3000)),
        }        
    },
    "replica_2": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": get_env_variable("DB_NAME"),
        "USER": get_env_variable("DB_USER"),
        "PASSWORD": get_env_variable("PASSWORD_DB"),
        "HOST": get_env_variable("DB_HOST", "") + '_replica2',
        "PORT": get_env_variable("DB_PORT", 5432),
        'OPTIONS': {
            'options': '-c statement_timeout={}'.format(get_env_variable("statement_timeout", 3000)),
        }        
    },    
}

CACHE_TTL = 60 * 3

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': get_env_variable("URL_REDIS", 'localhost:6379'),
        'OPTIONS': {
            'DB': 5,            
            "CLIENT_CLASS": "django_redis.client.DefaultClient"
        },
        "KEY_PREFIX": "mat_cache_",
        "PARSER_CLASS": "redis.connection.HiredisParser",
    }
}

# Número de segundos de inactividad antes de que un usuario se marca fuera de línea
USER_ONLINE_TIMEOUT = 120
 
# Número de segundos que vamos a hacer seguimiento de los usuarios inactivos, para antes de su 
# visto por última vez. Se elimina de la memoria caché
USER_LASTSEEN_TIMEOUT = 60 * 60 * 24 * 7

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

# AUTH_PASSWORD_VALIDATORS = [
#     {
#         'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
#     },
# ]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = "es-cl"

LANGUAGES = (("en", "English"), ("es", "Spanish"))

LOCALE_PATHS = (join(BASE_DIR, "locale"),)

TIME_ZONE =  "America/Argentina/Buenos_Aires" # "America/Santiago"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = "/static/"

# django-wkhtmltopdf
WKHTMLTOPDF_CMD = "wkhtmltopdf"
# WKHTMLTOPDF_CMD = '/nix/store/zm6s01rff5jfkznqnx8c42630w4mf5nb-wkhtmltopdf-0.12.4/bin/wkhtmltopdf'
WKHTMLTOPDF_CMD_OPTIONS = {"quiet": True, "page-size": "Letter"}

MEDIA_URL = "/media/"

TAGGIT_CASE_INSENSITIVE = True

REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "PAGE_SIZE": 100,
    "DEFAULT_FILTER_BACKENDS": ["url_filter.integrations.drf.DjangoFilterBackend",],
}


LOGIN_URL = "/accounts/required-login/"

LOGIN_REDIRECT_URL = "/"


SITE_NAME = "matrix"

# celery
#CELERY_BROKER_URL = "amqp://localhost"
#CELERY_RESULT_BACKEND = "amqp://localhost"
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_RESULT_SERIALIZER = "json"
CELERY_TASK_SERIALIZER = "json"
CELERY_IMPORTS = ["billing.tasks"]
CELERY_CREATE_MISSING_QUEUES = True
CELERY_BROKER_URL = "redis://localhost:6379"
CELERY_RESULT_BACKEND = "redis://localhost:6379"
CELERY_DEFAULT_QUEUE = 'queue1'



 


def DEBUG_TOOLBAR_FUN(request):
    if hasattr(request, 'user'):
        if request.user.is_superuser:
            return True
        return False
    return True

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK':'config.settings.development.DEBUG_TOOLBAR_FUN'
}

