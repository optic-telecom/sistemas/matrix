from .base import *  # noqa
from os.path import dirname, join
from .celery_conf import *

ENVIROMENT = "STAGE"
DEBUG = True

PRODUCTION_APPS = []

INSTALLED_APPS = PREREQ_APPS + PROJECT_APPS + PRODUCTION_APPS

ALLOWED_HOSTS = ["demo-matrix.devel7.cl", "190.113.247.203"]

STATIC_ROOT = "/home/matrix/matrix_backend/matrix/static/"
MEDIA_ROOT = "/home/matrix/matrix_backend/matrix/media/"

BASE_DIR_LOG = "/home/matrix/matrix_backend/logs"

LOGGING = {
    "version": 1,
    "formatters": {"verbose": {"format": "%(levelname)s:%(name)s: %(message)s"}},
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "file": {
            "level": "INFO",
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "filename": join(BASE_DIR_LOG, "debug_django.log"),
        },
    },
    "loggers": {
        "django.request": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "matplotlib": {
            "handlers": ["file", "console"],
            "level": "ERROR",
            "propagate": False,
        },
        "": {"level": "DEBUG", "handlers": ["console"],},
    },
}
