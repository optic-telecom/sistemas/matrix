import time, datetime
from django.core.cache import cache
from django.conf import settings
#import debug_toolbar
#from debug_toolbar.middleware import debugToolbarMiddleware


class TimeViewsMiddleware:
    time_init = 0
    request_init = ''
    def process_request(self, request):
        self.request_init = request.path
        self.time_init = time.time()
        return None

    def process_response(self, request, response):
        inf = "{},{},{}\n".format(datetime.datetime.now(), self.request_init, time.time() - self.time_init)
        f = open('urls_time.txt', 'a+')
        f.write(inf)
        f.close()        
        return response

 
class ActiveUserMiddleware:
     
    # Esté middleware setea en cache cuando un usuario se loguea
    def process_request(self, request):
        current_user = request.user
        if request.user.is_authenticated():
            now = datetime.datetime.now()
            cache.set('seen_%s' % (current_user.username), now, settings.USER_LASTSEEN_TIMEOUT)        