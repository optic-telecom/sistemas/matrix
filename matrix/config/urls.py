from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from hr import urls as hr_urls
from providers import urls as providers_urls
from customers.urls import urlpatterns as customers_urlpatterns
from billing.urls import urlpatterns as billing_urlpatterns
from infra.urls import urlpatterns as infra_urlpatterns
from reports.urls import urlpatterns as reports_urlpatterns
from scheduler.urls import urlpatterns as scheduler_urlpatterns
from django.contrib.auth import urls as auth_urls

# Router
from customers.urls import router

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(r"^providers/", include(providers_urls)),
    url(r"^hr/", include(hr_urls)),
    url(r"^accounts/", include(auth_urls)),
    ##########API URLS###############
    url(r"^api/v1/", include(router.urls, namespace="api")),
    url(r"^api-auth/", include("rest_framework.urls")),
] + customers_urlpatterns + billing_urlpatterns + infra_urlpatterns + scheduler_urlpatterns+ reports_urlpatterns \
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
  
#urlpatterns += [url(r'^silk/', include('silk.urls', namespace='silk'))]

if settings.USE_DEBUG_TOOLBAL:
    import debug_toolbar
    urlpatterns = [url('__debug__/', include(debug_toolbar.urls)),] + urlpatterns