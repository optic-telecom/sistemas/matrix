# coding: utf-8
import io
import xlsxwriter
import requests
from django.core.management import BaseCommand
from infra.models import Node
import pandas as pd


class Command(BaseCommand):
    help = """
        Comando que crea un reporte en excel para ver cuales nodos
        tienen direcciones asociadas en pulso
        """
    url = "http://localhost:8000/"
    headers = {
            "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s"
        }

    def get_connection(self, url, **kwargs):
        """
        Retorna una conexion a un endpoint
        recibe por parametro una url y
        un posible diccionario con los parametros
        a utilizar via GET
        """
        response = requests.get(
            url,
            headers=self.headers,
            verify=False,
            params=kwargs
        )
        try:
            if response.status_code == 200:
                data = response.json()
            else:
                data = None
            return data
        except Exception as e:
            print(str(e))

    def add_arguments(self, parser):
        parser.add_argument(
            "-u"
            '--url',
            type=str,
            help='Indica la url de pulso a utilizar')

    def get_confirm_node(self, url):
        """ Preguntamos al select2 autocomplete los nodos en pulso """
        try:
            results = self.get_connection(url)["results"]
            if len(results) == 0:
                return "NO"
            elif len(results) == 1:
                return "SI"
            elif len(results) > 1:
                return f"SI REPETIDO {len(results)} VECES"
        except Exception as e:
            print(str(e))

    def get_node_address(self, url, **kwargs):
        """ Buscamos coincidencias entre nodos y direcciones en pulso """
        data = kwargs
        params = {
            "commune": data["commune"],
            "street": data["street"],
            "number": data["house_number"]
        }

        results = self.get_connection(url, **params)
        if "results" in results:
            results = results["results"]
        try:
            results = list(
                filter(
                    lambda x: x["number"] == data["house_number"],
                    results
                    )
                    )
            # obtenemos en nro de items dentro de la lista results
            size = len(results)
            if size == 0:
                return {
                    "id": "---",
                    "street_location_pk": "---",
                    "observacion": "No tiene coincidencias de direcciones en Pulso"
                }
            elif size == 1:
                return {
                    "id": int(results[0]['id']),
                    "street_location_pk": int(
                        results[0]["street_location_pk"]
                        ),
                    "observacion": "Tiene una sola coincidencia de dirección en Pulso"
                }
            elif size > 1:
                return {
                    "id": "+++++",
                    "street_location_pk": "+++++",
                    "observacion": "Tiene multiples coincidencias de direcciones en Pulso"
                }
        except Exception as e:
            print(str(e))

    def handle(self, *args, **options):
        nx = Node.objects.order_by('code').distinct("code")
        excel = list()
        if "url" in options:
            url = f"{options['url']}api/v1/search-location/"
        else:
            url = f"{self.url}api/v1/search-location/"
        for node in nx:
            kwargs = dict(
                commune=node.commune,
                street=node.street,
                house_number=node.house_number
            )
            pulso = self.get_node_address(url, **kwargs)
            url_node = f"{self.url}node-ac/?q={node.code}"
            registrado = self.get_confirm_node(url_node)
            excel.append({
                "node_id": node.id,
                "node": node.code,
                "node_commune": node.commune,
                "node_street": node.street,
                "node_house_number": node.house_number,
                "pulso_complete_location_id": pulso["id"],
                "pulso_street_location_id": pulso["street_location_pk"],
                "observacion": pulso["observacion"],
                "registrado_en_pulso": registrado
            })
        columns = [
            "node_id",
            "node", "node_commune", "node_street",
            "node_house_number", "pulso_complete_location_id",
            "pulso_street_location_id", "observacion",
            "registrado_en_pulso"
        ]
        df = pd.DataFrame(excel, columns=columns)
        df = df.groupby(columns).node_id.agg([len])
        df.to_excel("matrix_reporte_nodos.xlsx", sheet_name='resumen_nodos')
        # print(df)
