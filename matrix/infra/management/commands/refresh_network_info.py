# coding: utf-8
from django.core.management import BaseCommand
from infra.models import Node


class Command(BaseCommand):
    def handle(self, *args, **options):
        nx = Node.objects.all()
        
        for node in nx:
            node.refresh_network_equipment()
