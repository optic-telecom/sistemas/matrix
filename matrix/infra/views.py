import json
from django.contrib import messages
from django.db.models import Q
from django.http import Http404, HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView
from documents.forms import DocumentForm
from gallery.forms import PicForm
from .models import *
from customers.views import IsSystemInternalPermission
from rest_framework import viewsets
from .serializers import *
from .forms import NodeForm


    



class NodeList(LoginRequiredMixin, ListView):
    model = Node
    queryset = Node.objects.all()[:40]

node_list = NodeList.as_view()


class NodeCreate(LoginRequiredMixin, CreateView):
    model = Node
    form_class = NodeForm
    success_url = '/nodes/'

node_create = NodeCreate.as_view()


class NodeUpdate(LoginRequiredMixin, UpdateView):
    model = Node
    form_class = NodeForm
    success_url = '/nodes/'

node_update = NodeUpdate.as_view()


class NodeGallery(LoginRequiredMixin, DetailView):
    model = Node
    template_name = 'infra/node_gallery.html'

node_gallery = NodeGallery.as_view()


def node_gallery_add(request, pk):
    node = get_object_or_404(Node, pk=pk)
    if request.method == 'POST':
        form = PicForm(request.POST, request.FILES)
        if form.is_valid():
            pic = form.save(commit=False)
            pic.content_object = node
            pic.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha subido la imagen.')
            return redirect(reverse('node-gallery',
                                    kwargs={'pk': node.pk}))
    else:
        form = PicForm()

    context = {'object': node,
               'form': form,
               'cancel_url': reverse('node-gallery', kwargs={'pk': node.pk})}
    return render(request, 'infra/node_gallery_add.html', context)


class NodeDocuments(LoginRequiredMixin, DetailView):
    model = Node
    template_name = 'infra/node_documents.html'

node_documents = NodeDocuments.as_view()


def node_documents_add(request, pk):
    node = get_object_or_404(Node, pk=pk)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            doc = form.save(commit=False)
            doc.content_object = node
            doc.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha subido el documento.')
            return redirect(reverse('node-documents',
                                    kwargs={'pk': node.pk}))
    else:
        form = DocumentForm()

    context = {'object': node,
               'form': form,
               'cancel_url': reverse('node-documents', kwargs={'pk': node.pk})}
    return render(request, 'infra/node_documents_add.html', context)


def prepare_node(node):
    return {'DT_RowId': node.pk,
            'pk': node.pk,
            'code': node.truncated_code,
            'address': node.truncated_address,
            'commune': node.commune,
            'parent': node.parent.code if node.parent else None,
            'services': node.service_set.count()}




@login_required
def node_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {'0': 'pk',
                   '1': 'code',
                   '2': 'address',
                   '3': 'commune'}
        orderings = {'asc': '',
                     'desc': '-'}
        order_column = request.GET.get('order[0][column]', None)
        order_dir = request.GET.get('order[0][dir]', None)

        # search
        search = request.GET.get('search[value]', '')
        start = int(request.GET.get('start', 0))
        length = int(request.GET.get('length', 10))

        if order_column and order_dir:
            all_nodes = Node.objects.order_by(orderings[order_dir]+columns[order_column])
        else:
            all_nodes = Node.objects.all()

        all_count = all_nodes.count()

        if search:
            filtered_nodes = all_nodes.filter(Q(pk=int(search) if search.isdigit() else None) |
                                              Q(code__unaccent__icontains=search) |
                                              Q(address__unaccent__icontains=search) |
                                              Q(commune__unaccent__icontains=search)).distinct()
        else:
            filtered_nodes = all_nodes
        filtered_count = filtered_nodes.count()

        if length == -1:
            sliced_nodes = filtered_nodes
        else:
            sliced_nodes = filtered_nodes[start:start+length]

        data = json.dumps({'draw': request.GET.get('draw', 1),
                           'recordsTotal': all_count,
                           'recordsFiltered': filtered_count,
                           'data': list(map(prepare_node, sliced_nodes))})

        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404


