from rest_framework import serializers
from infra.models import NetworkEquipment
from customers.models import Service


class NetworkEquipmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = NetworkEquipment
        fields = (
            "service",
            "ip",
            "brand",
            "mac",
            "model"
        )

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        if instance.service:
            rep["service"] = {
                "id": instance.service.id,
                "number": instance.service.number,
                "address": instance.service.address,
                # debe venir del NODO
                "status_service": instance.service.network_status[1],
                "status_lease": '---',
                # debe venir del NODO
            }
        return rep

    def to_internal_value(self, data):
        try:
            try:
                service_id = data["id"]
                return Service.objects.get(pk=service_id)
            except KeyError:
                raise serializers.ValidationError(
                        "El id es un campo requerido"
                    )
            except ValueError:
                raise serializers.ValidationError(
                        "Este campo debe ser un número entero"
                    )
        except Service.DoesNotExist:
            raise serializers.ValidationError(
                    "Este Equipo no se encuentra registrado"
                )