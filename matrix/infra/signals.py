from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from .models import Node, Gear

@receiver(post_save, sender=Node)
def clear_addresses(sender, **kwargs):
    i = kwargs['instance']
    if i.no_address_binding:
        i.availability_set.all().delete()

@receiver(pre_save, sender=Gear)
def update_date_cache(sender, **kwargs):
    i = kwargs['instance']
    i.latest_backup_date_cache = i.latest_backup_date
