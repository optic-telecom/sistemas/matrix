# Node views
from .node import refresh_network_equipment  # noqa
from .node import node_list  # noqa
from .node import node_create  # noqa
from .node import node_update  # noqa
from .node import node_fiber_update  # noqa
from .node import node_gear  # noqa
from .node import node_gallery  # noqa
from .node import node_gallery_add  # noqa
from .node import node_gallery_mass_add  # noqa
from .node import node_gallery_update  # noqa
from .node import node_documents  # noqa
from .node import node_documents_add  # noqa
from .node import node_services  # noqa
from .node import node_leases  # noqa
from .node import node_tie_address  # noqa
from .node import node_untie_address  # noqa
from .node import find_availability  # noqa
from .node import find_availability_api  # noqa
from .node import RegionAutocomplete  # noqa
from .node import StreetAutocomplete  # noqa
from .node import ComunaAutocomplete  # noqa
from .node import DireccionAutocomplete  # noqa
from .node import node_ajax  # noqa
from .node import node_detail  # noqa
from .node import node_activities  # noqa
from .node import node_activity_add  # noqa
from .node import node_location  # noqa
from .node import node_refresh  # noqa
from .node import nodes_cities  # noqa
from .node import node_available_ips_ajax
from .node import NodeViewSet  # noqa


# Gear views
from .gear import gear_list  # noqa
from .gear import gear_detail  # noqa
from .gear import gear_ajax  # noqa
from .gear import gear_create  # noqa
from .gear import gear_update  # noqa

from .gear import gear_gallery  # noqa
from .gear import gear_gallery_add  # noqa
from .gear import gear_gallery_update  # noqa
from .gear import gear_gallery_delete  # noqa

from .gear import gear_documents  # noqa
from .gear import gear_documents_add  # noqa
from .gear import gear_documents_update  # noqa
from .gear import gear_documents_delete  # noqa
