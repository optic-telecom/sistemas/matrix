import io
import json
import zipfile
from itertools import chain
from django.contrib import messages
from django.core.files import File
from django.db.models import Q
from django.http import Http404, HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import ListView, DetailView
from django.views.generic.base import RedirectView
from django.views.generic.edit import CreateView, UpdateView

from documents.forms import DocumentForm
from gallery.models import Pic
from gallery.forms import PicForm, MassPicUploadForm, PicUpdateForm
from infra.models import Node
from infra.forms import NodeForm,CitiesForm, NodeFiberForm
from django.db.models import Count
from customers.lefu.cl_territory import COMMUNE_CHOICES
from rest_framework import viewsets
from .mixins import GroupRequiredMixin
from ..forms import DurationForm, ContactFormSet
from ..models import Availability
from ..serializers import NodeSerializer
from customers.views import IsSystemInternalPermission
from rest_framework import viewsets
from ..serializers import *
from customers.models import Plan
from url_filter.integrations.drf import DjangoFilterBackend

class RefreshNetworkEquipmentView(GroupRequiredMixin, RedirectView):
    permanent = False
    group_required = ['Refrescar Red']

    def get_redirect_url(self, *args, **kwargs):
        nx = Node.objects.all()
        for node in nx:
            node.refresh_network_equipment()
        messages.add_message(self.request,
                             messages.SUCCESS,
                             'Se han refrescado los equipos de red.')
        return reverse('node-list')


refresh_network_equipment = RefreshNetworkEquipmentView.as_view()


class NodeList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Node
    queryset = Node.objects.all()[:40]  # FIXME
    permission_required = 'infra.list_node'

    def get_context_data(self, *args, **kwargs):
        ctx = super(NodeList, self).get_context_data(*args, **kwargs)
        ctx['can_refresh'] = self.request.user.groups.filter(name='Refrescar Red').exists()
        return ctx

node_list = NodeList.as_view()


class NodeCreate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Node
    form_class = NodeForm
    success_url = '/nodes/'
    success_message = 'Se ha creado un nodo con éxito'
    permission_required = 'infra.add_node'

    def get_context_data(self, **kwargs):
        context = super(NodeCreate, self).get_context_data(**kwargs)
        context['plans_all'] = Plan.objects.all()
        return context
node_create = NodeCreate.as_view()


class NodeUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Node
    form_class = NodeForm
    success_url = '/nodes/'
    success_message = 'Se ha actualizado el nodo con éxito'
    permission_required = 'infra.change_node'

    def get_context_data(self, **kwargs):
        ctx = super(NodeUpdate, self).get_context_data(**kwargs)
        if self.request.method == 'POST':
            ctx['inlines'] = ContactFormSet(self.request.POST, instance=self.object)
        else:
            ctx['inlines'] = ContactFormSet(instance=self.object)
        ctx['plans_all'] = Plan.objects.all()
        current_plans = []
        if self.object.plans:
            for plan in self.object.plans.all():
                current_plans.append(plan.pk)
        ctx['current_plans'] = current_plans
        return ctx

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        inlines = context['inlines']

        self.object = form.save()
        if inlines.is_valid():
            inlines.instance = self.object
            inlines.save()

        return super(NodeUpdate, self).form_valid(form)


node_update = NodeUpdate.as_view()


class NodeFiberUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Node
    form_class = NodeFiberForm
    template_name = 'infra/node_fiber_form.html'
    success_url = '/nodes/'
    success_message = 'Se ha actualizado el nodo con éxito'
    permission_required  = 'infra.change_node'

node_fiber_update = NodeFiberUpdate.as_view()


class NodeGear(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Node
    template_name = 'infra/node_gear.html'
    permission_required = 'infra.view_node'

node_gear = NodeGear.as_view()


class NodeGallery(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Node
    template_name = 'infra/node_gallery.html'
    permission_required = 'infra.view_node'

node_gallery = NodeGallery.as_view()


class NodeServices(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Node
    template_name = 'infra/node_services.html'
    permission_required = 'infra.view_node'

node_services = NodeServices.as_view()


class NodeLeases(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Node
    template_name = 'infra/node_leases.html'
    permission_required = 'infra.view_node'

node_leases = NodeLeases.as_view()


@login_required
@permission_required('infra.change_node')
def node_gallery_add(request, pk):
    node = get_object_or_404(Node, pk=pk)
    if request.method == 'POST':
        form = PicForm(request.POST, request.FILES)
        if form.is_valid():
            pic = form.save(commit=False)
            pic.content_object = node
            pic.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha subido la imagen.')
            return redirect(reverse('node-gallery',
                                    kwargs={'pk': node.pk}))
    else:
        form = PicForm()

    context = {'object': node,
               'form': form,
               'cancel_url': reverse('node-gallery', kwargs={'pk': node.pk})}
    return render(request, 'infra/node_gallery_add.html', context)


@login_required
@permission_required('infra.change_node')
def node_gallery_mass_add(request, pk):
    node = get_object_or_404(Node, pk=pk)
    if request.method == 'POST':
        form = MassPicUploadForm(request.POST, request.FILES)
        if form.is_valid():
            zip_f = zipfile.ZipFile(form.cleaned_data['photos'])
            generic_caption = form.cleaned_data['caption']

            for i, filename in enumerate(zip_f.namelist(), 1):
                if (".png" in filename or ".PNG" in filename or ".jpg" in filename or ".jpeg" in filename or ".JPG" in filename):
                    photo = zip_f.read(filename)
                    photo_file = File(filename)
                    photo_file.file = io.BytesIO(photo)
                    new_pic = Pic.objects.create(caption=generic_caption+" {}".format(i),
                                                 content_object=node)
                    new_pic.photo.save(filename, photo_file)

            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se han subido las imagenes.')
            return redirect(reverse('node-gallery',
                                    kwargs={'pk': node.pk}))
    else:
        form = MassPicUploadForm()

    context = {'object': node,
               'form': form,
               'cancel_url': reverse('node-gallery', kwargs={'pk': node.pk})}
    return render(request, 'infra/node_gallery_mass_add.html', context)


@login_required
@permission_required('infra.change_node')
def node_gallery_update(request, node_pk, pk):
    node = get_object_or_404(Node,
                             pk=node_pk)
    pic = get_object_or_404(Pic,
                            pk=pk)

    if request.method == 'POST':
        form = PicUpdateForm(request.POST, request.FILES)
        if form.is_valid():
            pic.caption = form.cleaned_data['caption']
            pic.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha editado la imagen.')
            return redirect(reverse('node-gallery',
                                    kwargs={'pk': node_pk}))
    else:
        form = PicUpdateForm(instance=pic)

    context = {'object': pic,
               'node': node,
               'form': form,
               'cancel_url': reverse('node-gallery',
                                     kwargs={'pk': node_pk})}
    return render(request, 'infra/node_gallery_update.html', context)


class NodeDocuments(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Node
    template_name = 'infra/node_documents.html'
    permission_required = 'infra.view_node'

node_documents = NodeDocuments.as_view()


@login_required
@permission_required('infra.change_node')
def node_documents_add(request, pk):
    node = get_object_or_404(Node, pk=pk)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            doc = form.save(commit=False)
            doc.content_object = node
            doc.agent = request.user
            doc.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha subido el documento.')
            return redirect(reverse('node-documents',
                                    kwargs={'pk': node.pk}))
    else:
        form = DocumentForm()

    context = {'object': node,
               'form': form,
               'cancel_url': reverse('node-documents', kwargs={'pk': node.pk})}
    return render(request, 'infra/node_documents_add.html', context)


@login_required
@permission_required('infra.view_node')
def node_detail(request, pk):
    node = get_object_or_404(Node, pk=pk)
    context = {'object': node, 'node': node }
    return render(request, 'infra/node_detail.html', context)


@login_required
@permission_required('infra.change_node')
def node_refresh(request, pk):
    node = get_object_or_404(Node, pk=pk)
    context = {'object': node, 'node': node }
    node.refresh_network_equipment()
    messages.add_message(request,
                         messages.SUCCESS,
                         'Se ha refrescado la información de red.')
    return redirect(reverse('node-detail', kwargs={'pk': node.pk}))


@login_required
@permission_required('infra.view_node')
def node_location(request, pk):
    node = get_object_or_404(Node, pk=pk)
    context = {'object': node, 'node': node }
    return render(request, 'infra/node_location.html', context)


@login_required
@permission_required('infra.view_node')
def node_activities(request, pk):
    node = get_object_or_404(Node, pk=pk)
    context = {'object': node, 'node': node }
    return render(request, 'infra/node_activities.html', context)


@login_required
@permission_required('infra.change_node')
def node_activity_add(request, pk):
    node = get_object_or_404(Node, pk=pk)
    if request.method == 'POST':
        form = DurationForm(request.POST)
        if form.is_valid():
            duration = form.save(commit=False)
            duration.node = node
            duration.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha añadido la actividad.')
            return redirect(reverse('node-activities',
                                    kwargs={'pk': node.pk}))
    else:
        form = DurationForm()

    context = {'object': node,
               'form': form,
               'cancel_url': reverse('node-activities', kwargs={'pk': node.pk})}
    return render(request, 'infra/node_activity_add.html', context)


@login_required
@permission_required('infra.list_node')
def nodes_cities(request):
    if request.method == 'POST':
        form = CitiesForm(request.POST)
        if form.is_valid():
            city = form.cleaned_data['location']
            nodes = Node.objects.filter(location=city)
            return render(request, 'infra/nodes_cities.html', {'nodes': nodes, 'form':form })     
    else:
 
        form = CitiesForm()
        nodes = []
        return render(request, 'infra/nodes_cities.html',  {'form':form,  'nodes': nodes})


def prepare_node(node):
    return {'DT_RowId': node.pk,
            'pk': node.pk,
            'code': node.truncated_code,
            'address': node.truncated_address,
            'commune': node.commune,
            'parent': node.parent.code if node.parent else None,
            'services': node.service_set.count(),
            'status': node.get_status_display(),
            'status_class': node.get_status_class,
            'seller': node.seller.name if node.seller else 'mesón'}


@login_required
@permission_required('infra.list_node')
def node_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {'0': 'pk',
                   '1': 'code',
                   '2': 'address',
                   '3': 'commune',
                   '7': 'seller'}
        orderings = {'asc': '',
                     'desc': '-'}
        order_column = request.GET.get('order[0][column]', None)
        order_dir = request.GET.get('order[0][dir]', None)

        # search
        search = request.GET.get('search[value]', '')
        start = int(request.GET.get('start', 0))
        length = int(request.GET.get('length', 10))

        if order_column and order_dir:
            all_nodes = Node.objects.order_by(orderings[order_dir]+columns[order_column])
        else:
            all_nodes = Node.objects.all()

        all_count = all_nodes.count()

        if search:
            filtered_nodes = all_nodes.filter(Q(pk=int(search) if search.isdigit() else None) |
                                              Q(code__unaccent__icontains=search) |
                                              Q(address__unaccent__icontains=search) |
                                              Q(commune__unaccent__icontains=search) |
                                              Q(seller__name__icontains=search) |
                                              Q(parent__code__icontains=search))
        else:
            filtered_nodes = all_nodes

        filtered_nodes = filtered_nodes.distinct()

        filtered_count = filtered_nodes.count()

        if length == -1:
            sliced_nodes = filtered_nodes
        else:
            sliced_nodes = filtered_nodes[start:start+length]

        data = json.dumps({'draw': request.GET.get('draw', 1),
                           'recordsTotal': all_count,
                           'recordsFiltered': filtered_count,
                           'data': list(map(prepare_node, sliced_nodes))})

        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404


@login_required
def node_available_ips_ajax(request, pk):
    if request.is_ajax():
        node = get_object_or_404(Node, pk=pk)
        data = json.dumps({'available_ips': node.available_ips})
        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404

from ..forms import TieAddressForm

@login_required
def node_tie_address(request, pk):
    node = get_object_or_404(Node, pk=pk)
    if request.method == 'POST':
        form = TieAddressForm(request.POST)
        if form.is_valid():
            Availability.objects.update_or_create(node=node, direccion=form.cleaned_data['direccion'])
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha asociado la dirección.')
            return redirect(reverse('node-detail', kwargs={'pk': node.pk}))
    else:
        form = TieAddressForm()
    return render(request, 'infra/node_tie_address.html', {'form': form, 'object': node})

@login_required
def node_untie_address(request, pk):
    av = get_object_or_404(Availability, pk=pk)
    node_pk = av.node.pk
    av.delete()
    messages.add_message(request,
                         messages.SUCCESS,
                         'Se ha eliminado la dirección asociada.')
    return redirect(reverse('node-detail', kwargs={'pk': node_pk}))


@login_required
def find_availability(request):
    from constance import config
    ctx = {'url':config.pulso_server}
    return render(request, 'infra/find_availability.html', ctx)

# FIXME
#@login_required
def find_availability_api(request):
    direccion_id = request.GET.get('direccion_id')

    try:
        addr = Direccion.objects.get(pk=direccion_id)
        avail = addr.availability_set.count() > 0
        plans = list(chain(*[map(lambda ss: str(ss), avx.node.plans.all()) for avx in addr.availability_set.all()]))
    except Direccion.DoesNotExist:
        addr = None
        avail = False
        plans = []

    if avail:
        data = {'available': True,
                'plans': plans}
    else:
        data = {'available': False}
    return HttpResponse(json.dumps(data), content_type='application/json')


from dal import autocomplete

from customers.models import Region
from customers.models import Comuna
from customers.models import Calle
from customers.models import Direccion

class RegionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # FIXME: permissions
        qs = Region.objects.all()

        if self.q:
            qs = qs.filter(nombre__unaccent__icontains=self.q)
        return qs


class StreetAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # FIXME: permissions
        qs = Calle.objects.all()

        comuna = self.forwarded.get('comuna', None)

        if comuna:
            qs = qs.filter(comuna=comuna)

        if self.q:
            qs = qs.filter(nombre__unaccent__icontains=self.q)

        return qs


class ComunaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # FIXME: permissions
        qs = Comuna.objects.all()

        region = self.forwarded.get('region', None)

        if region:
            qs = qs.filter(region=region)

        if self.q:
            qs = qs.filter(nombre__unaccent__icontains=self.q)

        return qs


class DireccionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # FIXME: permissions
        qs = Direccion.objects.all()

        calle = self.forwarded.get('calle', None)

        if calle:
            qs = qs.filter(calle=calle).distinct('numeroMunicipal')

        if self.q:
            qs = qs.filter(numeroMunicipal__icontains=self.q).distinct('numeroMunicipal')

        return qs

class NodeViewSet(viewsets.ModelViewSet):
    queryset = Node.objects.all()
    serializer_class = NodeSerializer

    permission_classes_by_action = {'create': [IsSystemInternalPermission],
                                    'list': [IsSystemInternalPermission],
                                    'update' : [IsSystemInternalPermission],
                                    'delete' : [IsSystemInternalPermission],
                                    'retrieve' : [IsSystemInternalPermission]}
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

    def get_permissions(self):
        try:
            # return permission_classes depending on `action` 
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]

