import json
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q, Count
from django.http import Http404, HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import redirect, get_object_or_404, render
from gallery.forms import PicForm, PicUpdateForm
from gallery.models import Pic
from documents.forms import DocumentForm, DocumentUpdateForm
from documents.models import Document
from infra.models import Gear, Node
from infra.forms import GearForm


class GearList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Gear
    queryset = Gear.objects.all()[:40]  # FIXME
    permission_required = 'infra.list_gear'

    def get_context_data(self, *args, **kwargs):
        ctx = super(GearList, self).get_context_data(*args, **kwargs)
        ctx['nodes'] = Gear.objects.values('node__pk','node__code').annotate(count=Count('node__code'))
        ctx['kinds'] = Gear.objects.values('kind').exclude(kind='').annotate(count=Count('kind'))
        ctx['vlans'] = Gear.objects.values('vlan').exclude(vlan='').annotate(count=Count('vlan'))
        ctx['brands'] = Gear.objects.values('brand').exclude(brand='').annotate(count=Count('brand'))
        return ctx

gear_list = GearList.as_view()


def prepare_gear(gear):
    return {'DT_RowId': gear.pk,
            'pk': gear.pk,
            'kind': gear.kind,
            'description': gear.truncated_description,
            'ip': gear.ip,
            'mac': gear.mac,
            'vlan': gear.vlan,
            'node': getattr(gear.node, 'code', None),
            'days': gear.days_since_last_backup,
            'days_class': gear.days_since_last_backup_class}


@login_required
@permission_required('infra.list_gear')
def gear_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {'0': 'pk',
                   '1': 'ip',
                   '4': 'kind',
                   '7': 'latest_backup_date_cache'}
        orderings = {'asc': '',
                     'desc': '-'}
        order_column = request.GET.get('order[0][column]', None)
        order_dir = request.GET.get('order[0][dir]', None)

        # search
        search = request.GET.get('search[value]', '')
        start = int(request.GET.get('start', 0))
        length = int(request.GET.get('length', 10))

        #filters
        vlan = request.GET.get('vlan', None)
        brand = request.GET.get('brand', None)
        kind = request.GET.get('kind', None)
        raw_node = request.GET.get('node', None)

        if raw_node:
            try:
                node = int(raw_node)
            except ValueError:
                node = None
        else:
            node = None

        if order_column and order_dir:
            all_gear = Gear.objects.order_by(orderings[order_dir]+columns[order_column])
        else:
            all_gear = Gear.objects.all()

        all_count = all_gear.count()

        if vlan:
            all_gear = all_gear.filter(vlan = vlan)
        if brand:
            all_gear = all_gear.filter(brand = brand)
        if kind:
            all_gear = all_gear.filter(kind = kind)
        if node:
            all_gear = all_gear.filter(node__pk = node)

        if search:
            filtered_gear = all_gear.filter(Q(pk=int(search) if search.isdigit() else None) |
                                            Q(kind__unaccent__icontains=search) |
                                            Q(ip__icontains=search) |
                                            Q(mac__unaccent__icontains=search) |
                                            Q(vlan__unaccent__icontains=search) |
                                            Q(notes__unaccent__icontains=search))

            filtered_count = filtered_gear.count()
        else:
            filtered_gear = all_gear
            
        filtered_gear = filtered_gear.distinct()
            
        filtered_count = filtered_gear.count()

        if length == -1:
            sliced_gear = filtered_gear
        else:
            sliced_gear = filtered_gear[start:start+length]

        data = json.dumps({'draw': request.GET.get('draw', 1),
                           'recordsTotal': all_count,
                           'recordsFiltered': filtered_count,
                           'data': list(map(prepare_gear, sliced_gear))})

        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404


class GearCreate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Gear
    form_class = GearForm
    success_message = 'Se ha creado un equipo con éxito.'
    permission_required = 'infra.add_gear'

gear_create = GearCreate.as_view()


class GearUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Gear
    form_class = GearForm
    success_message = 'Se ha actualizado un equipo con éxito.'
    permission_required = 'infra.update_gear'


gear_update = GearUpdate.as_view()


class GearDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Gear
    permission_required = 'infra.view_gear'


gear_detail = GearDetail.as_view()


@login_required
@permission_required('infra.view_gear')
def gear_gallery(request, pk):
    gear = get_object_or_404(Gear,
                             pk=pk)
    context = {'object': gear,
               'gear': gear}
    return render(request, 'infra/gear_gallery.html', context)


@login_required
@permission_required('gallery.add_pic')
def gear_gallery_add(request, pk):
    gear = get_object_or_404(Gear,
                             pk=pk)
    if request.method == 'POST':
        form = PicForm(request.POST, request.FILES)
        if form.is_valid():
            pic = form.save(commit=False)
            pic.content_object = gear
            pic.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha subido la imagen.')
            return redirect(reverse('gear-gallery',
                                    kwargs={'pk': gear.pk}))
    else:
        form = PicForm()

    context = {'object': gear,
               'gear': gear,
               'form': form,
               'cancel_url': reverse('gear-gallery',
                                     kwargs={'pk': gear.pk})}
    return render(request, 'infra/gear_gallery_add.html', context)


@login_required
@permission_required('gallery.change_pic')
def gear_gallery_update(request, pk, pic_pk):
    gear = get_object_or_404(Gear,
                             pk=pk)
    pic = get_object_or_404(Pic,
                            pk=pic_pk)

    if request.method == 'POST':
        form = PicUpdateForm(request.POST, request.FILES)
        if form.is_valid():
            pic.caption = form.cleaned_data['caption']
            pic.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha editado la imagen.')
            return redirect(reverse('gear-gallery',
                                    kwargs={'pk': gear.pk}))
    else:
        form = PicUpdateForm(instance=pic)

    context = {'object': pic,
               'gear': gear,
               'form': form,
               'cancel_url': reverse('gear-gallery',
                                     kwargs={'pk': gear.pk})}
    return render(request, 'infra/gear_gallery_update.html', context)


@login_required
def gear_gallery_delete(request, pk, pic_pk):
    pic = get_object_or_404(Pic,
                            pk=pic_pk)
    pic.delete()

    messages.add_message(request,
                         messages.SUCCESS,
                         'Se ha eliminado la imagen.')

    return redirect(reverse('gear-gallery',
                            kwargs={'pk': pk}))


@login_required
@permission_required('infra.view_gear')
def gear_documents(request, pk):
    gear = get_object_or_404(Gear,
                             pk=pk)
    context = {'object': gear,
               'gear': gear}
    return render(request, 'infra/gear_documents.html', context)


@login_required
@permission_required('documents.add_document')
def gear_documents_add(request, pk):
    gear = get_object_or_404(Gear,
                             pk=pk)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            document = form.save(commit=False)
            document.content_object = gear
            document.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha subido la imagen.')
            return redirect(reverse('gear-documents',
                                    kwargs={'pk': gear.pk}))
    else:
        form = DocumentForm()

    context = {'object': gear,
               'gear': gear,
               'form': form,
               'cancel_url': reverse('gear-documents',
                                     kwargs={'pk': gear.pk})}
    return render(request, 'infra/gear_documents_add.html', context)


@login_required
@permission_required('documents.change_document')
def gear_documents_update(request, pk, document_pk):
    gear = get_object_or_404(Gear,
                             pk=pk)
    document = get_object_or_404(Document,
                            pk=document_pk)

    if request.method == 'POST':
        form = DocumentUpdateForm(request.POST, request.FILES)
        if form.is_valid():
            document.caption = form.cleaned_data['caption']
            document.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha editado la imagen.')
            return redirect(reverse('gear-documents',
                                    kwargs={'pk': gear.pk}))
    else:
        form = DocumentUpdateForm(instance=document)

    context = {'object': document,
               'gear': gear,
               'form': form,
               'cancel_url': reverse('gear-documents',
                                     kwargs={'pk': gear.pk})}
    return render(request, 'infra/gear_documents_update.html', context)


@login_required
def gear_documents_delete(request, pk, document_pk):
    document = get_object_or_404(Document,
                            pk=document_pk)
    document.delete()

    messages.add_message(request,
                         messages.SUCCESS,
                         'Se ha eliminado la imagen.')

    return redirect(reverse('gear-documents',
                            kwargs={'pk': pk}))
