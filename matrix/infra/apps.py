from django.apps import AppConfig


class InfraConfig(AppConfig):
    name = 'infra'

    def ready(self):
        from . import signals
