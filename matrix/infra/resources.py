from import_export import resources
from .models import (Group, BuildingContact, Node, Gear,
    NetworkEquipment, NotFoundEquipment, Availability, PlanOrderKind)

class GroupResource(resources.ModelResource):

    class Meta:
        model = Group

class BuildingContactResource(resources.ModelResource):

    class Meta:
        model = BuildingContact

class NodeResource(resources.ModelResource):

    class Meta:
        model = Node

class GearResource(resources.ModelResource):

    class Meta:
        model = Gear

class NetworkEquipmentResource(resources.ModelResource):

    class Meta:
        model = NetworkEquipment

class NotFoundEquipmentResource(resources.ModelResource):

    class Meta:
        model = NotFoundEquipment

class AvailabilityResource(resources.ModelResource):

    class Meta:
        model = Availability

class PlanOrderKindResource(resources.ModelResource):

    class Meta:
        model = PlanOrderKind






