from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from infra.models import NetworkEquipment
from infra.infra_serializers import NetworkEquipmentSerializer
from django.db.models import Q


class NetworkEquipmentViewSet(ReadOnlyModelViewSet):
    queryset = NetworkEquipment.objects.all()
    serializer_class = NetworkEquipmentSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        qs = NetworkEquipment.objects.select_related(
            "service",
            "service__node"
            ).order_by(
                "service__number",
                "ip",
                "mac",
            )

        qs = qs.distinct("service__number", "ip", "mac",)
        return qs

    def filter_qs(self, qs):
        search = self.request.GET.get("search[value]", None)
        ip = self.request.GET.get('ip', None)
        mac = self.request.GET.get('mac', None)
        service = self.request.GET.get('service', None)
        if search:
            qs = qs.filter(
                Q(service__number=search) |
                Q(mac__istartswith=search) |
                Q(brand__icontains=search) |
                Q(model__icontains=search)
            )
        if mac:
            qs = qs.filter(
                mac=mac
            )
        if service:
            qs = qs.filter(
                service__id=service
            )
        if ip:
            qs = qs.filter(
                ip=ip
            )
        return qs

    def list(self, request, *args, **kwargs):
        page = self.request.GET.get('page')
        if page:
            try:
                page = self.request.GET.get('page', int(page-1))
            except:
                page = self.request.GET.get('page', int(page))

        page_size = self.request.GET.get('length', 10)
        self.paginator.page_size = int(page_size)

        queryset = self.filter_qs(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            res = self.get_paginated_response(serializer.data)
            res.data["recordsFiltered"] = queryset.count()
            res.data["recordsTotal"] = self.get_queryset().count()
            return res

        serializer = self.get_serializer(queryset, many=True)
        serializer.data["recordsFiltered"] = queryset.count()
        serializer.data["recordsTotal"] = self.get_queryset().count()
        return Response(serializer.data)
