from .models import (Node, BuildingContact, Gear, Availability, NetworkEquipment)
from rest_framework import serializers
from drf_queryfields import QueryFieldsMixin

class NodeSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = Node
        fields = ('id','group','code','status','address',
        'street','house_number','commune','location',
        'parent','default_connection_node','olt_config',
        'is_building','is_optical_fiber','gpon','gepon',
        'pon_port','box_location','splitter_location',
        'expected_power','fiber_notes','towers',
        'apartments','phone')

class BuildingContactSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = BuildingContact
        fields = ('kind','name','phone','email','node')

class GearSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Gear
        fields = ('kind','description','ip','mac','vlan','node',
        'firmware_version','notes')

class AvailabilitySerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Availability
        fields = ('node','direccion')

class NetworkEquipmentSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    service = serializers.HyperlinkedRelatedField(view_name='api:service-detail', read_only=True)
    class Meta:
        model = NetworkEquipment
        fields = ('ip','primary', 'service')