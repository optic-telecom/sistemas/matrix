from dal import autocomplete
from infra.models import NetworkEquipment


class ServiceNumNetworkEquipmenAutocomplete(autocomplete.Select2QuerySetView):
    """ Maneja un listado de ip """
    def get_queryset(self):
        qs = NetworkEquipment.objects.none()
        if self.q:
            qs = NetworkEquipment.objects.select_related(
                "service"
                ).filter(
                    service__number__iexact=self.q
                ).order_by(
                    "service__number"
                ).distinct("service__number")

        return qs

    def get_result_value(self, result):
        """Return the value of a result."""
        return str(result.service.pk)

    def get_result_label(self, item):
        return item.service.number

    def get_selected_result_label(self, item):
        return item.service.number