from dal import autocomplete
from infra.models import NetworkEquipment


class MACNetworkEquipmenAutocomplete(autocomplete.Select2ListView):
    """ Maneja un listado de ip """
    def get_list(self):
        qs = NetworkEquipment.objects.none()
        if self.q:
            qs = NetworkEquipment.objects.filter(
                mac__istartswith=self.q
            ).values_list(
                "mac"
            )
            qs = list(map(lambda x: x[0], qs))
            qs = set(qs)
        return qs