from dal import autocomplete
from infra.models import NetworkEquipment


class IPNetworkEquipmenAutocomplete(autocomplete.Select2ListView):
    """ Maneja un listado de ip """
    def get_list(self):
        qs = NetworkEquipment.objects.none()
        if self.q:
            qs = NetworkEquipment.objects.filter(
                ip__istartswith=self.q
            ).values_list(
                "ip"
            )
            qs = list(map(lambda x: x[0], qs))
            qs = set(qs)
        return qs
