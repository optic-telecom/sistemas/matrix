from django.conf.urls import url, include
from .views import (node_list, refresh_network_equipment, nodes_cities,
    node_create, node_detail, node_refresh, node_location,node_activities,
    node_activity_add, node_update, node_fiber_update, node_gear, node_gallery,
    node_gallery_add, node_gallery_mass_add, node_gallery_update, node_documents,
    node_documents_add, node_services, node_leases,  node_tie_address, 
    node_untie_address, find_availability, find_availability_api, 
    RegionAutocomplete, StreetAutocomplete, ComunaAutocomplete,
    DireccionAutocomplete, node_ajax, node_available_ips_ajax, gear_list, 
    gear_detail, gear_ajax, gear_create, gear_update, gear_gallery,
    gear_gallery_add, gear_gallery_update, gear_gallery_delete,
    gear_documents, gear_documents_add, gear_documents_update,
    gear_documents_delete, NodeViewSet
    )
from infra.infra_viewsets import (
    NetworkEquipmentViewSet
)
from infra.infra_views import (
    NetworkEquipmentView
)
from infra.autocompletes import (
    IPNetworkEquipmenAutocomplete,
    MACNetworkEquipmenAutocomplete,
    ServiceNumNetworkEquipmenAutocomplete
)
from customers.urls import router
router.register(r"nodes", NodeViewSet)

urlpatterns = [
    url(
        r"^network-equipment-api",
        NetworkEquipmentViewSet.as_view({'get': 'list'}),
        name="network_equipment_api"
        ),
    url(
        "network-equipment",
        NetworkEquipmentView.as_view(),
        name="network_equipment"
    ),

    url(
        "autocompletes/ips-networks-equipment/",
        IPNetworkEquipmenAutocomplete.as_view(),
        name="ac_ips_networks_equipment"
    ),
    url(
        "autocompletes/macs-networks-equipment/",
        MACNetworkEquipmenAutocomplete.as_view(),
        name="ac_macs_networks_equipment"
    ),
    url(
        "autocompletes/service-networks-equipment/",
        ServiceNumNetworkEquipmenAutocomplete.as_view(),
        name="ac_service_networks_equipment"
    ),

    url(r"^nodes/$", node_list, name="node-list"),
    url(
        r"^nodes/refresh-network-equipment/$",
        refresh_network_equipment,
        name="refresh-network-equipment",
    ),
    url(r"^nodes/cities/$", nodes_cities, name="nodes-cities"),
    url(r"^nodes/new/$", node_create, name="node-create"),
    url(r"^nodes/(?P<pk>\d+)/$", node_detail, name="node-detail"),
    url(r"^nodes/(?P<pk>\d+)/refresh/$", node_refresh, name="node-refresh"),
    url(r"^nodes/(?P<pk>\d+)/location/$", node_location, name="node-location"),
    url(r"^nodes/(?P<pk>\d+)/activities/$", node_activities, name="node-activities"),
    url(
        r"^nodes/(?P<pk>\d+)/activities/add/$",
        node_activity_add,
        name="node-activity-add",
    ),
    url(r"^nodes/(?P<pk>\d+)/edit/$", node_update, name="node-update"),
    url(r"^nodes/(?P<pk>\d+)/fiber/$", node_fiber_update, name="node-fiber-update"),
    url(r"^nodes/(?P<pk>\d+)/gear/$", node_gear, name="node-gear"),
    url(r"^nodes/(?P<pk>\d+)/gallery/$", node_gallery, name="node-gallery"),
    url(r"^nodes/(?P<pk>\d+)/gallery/new/$", node_gallery_add, name="node-gallery-add"),
    url(
        r"^nodes/(?P<pk>\d+)/gallery/mass-upload/$",
        node_gallery_mass_add,
        name="node-gallery-mass-add",
    ),
    url(
        r"^nodes/(?P<node_pk>\d+)/gallery/(?P<pk>\d+)/edit/$",
        node_gallery_update,
        name="node-gallery-update",
    ),
    url(r"^nodes/(?P<pk>\d+)/documents/$", node_documents, name="node-documents"),
    url(
        r"^nodes/(?P<pk>\d+)/documents/new/$",
        node_documents_add,
        name="node-documents-add",
    ),
    url(r"^nodes/(?P<pk>\d+)/services/$", node_services, name="node-services"),
    url(r"^nodes/(?P<pk>\d+)/leases/$", node_leases, name="node-leases"),
    url(r"^nodes/(?P<pk>\d+)/tie-address/$", node_tie_address, name="node-tie-address"),
    url(
        r"^nodes/untie-address/(?P<pk>\d+)/$",
        node_untie_address,
        name="node-untie-address",
    ),
    url(r"^find-availability/$", find_availability, name="find-availability"),
    url(
        r"^find-availability-api/$", find_availability_api, name="find-availability-api"
    ),
    url(
        r"^nodes/region-autocomplete/$",
        RegionAutocomplete.as_view(),
        name="region-autocomplete",
    ),
    url(
        r"^nodes/street-autocomplete/$",
        StreetAutocomplete.as_view(),
        name="street-autocomplete",
    ),
    url(
        r"^nodes/comuna-autocomplete/$",
        ComunaAutocomplete.as_view(),
        name="comuna-autocomplete",
    ),
    url(
        r"^nodes/direccion-autocomplete/$",
        DireccionAutocomplete.as_view(),
        name="direccion-autocomplete",
    ),
    url(r"^nodes/ajax/$", node_ajax, name="node-ajax"),
    url(
        r"^nodes/(?P<pk>\d+)/available-ips/$",
        node_available_ips_ajax,
        name="node-available-ips-ajax",
    ),
    url(r"^gear/$", gear_list, name="gear-list"),
    url(r"^gear/(?P<pk>\d+)/$", gear_detail, name="gear-detail"),
    url(r"^gear/ajax/$", gear_ajax, name="gear-ajax"),
    url(r"^gear/new/$", gear_create, name="gear-create"),
    url(r"^gear/(?P<pk>\d+)/edit/$", gear_update, name="gear-update"),
    url(r"^gear/(?P<pk>\d+)/gallery/$", gear_gallery, name="gear-gallery"),
    url(r"^gear/(?P<pk>\d+)/gallery/new/$", gear_gallery_add, name="gear-gallery-add"),
    url(
        r"^gear/(?P<pk>\d+)/gallery/(?P<pic_pk>\d+)/edit/$",
        gear_gallery_update,
        name="gear-gallery-update",
    ),
    url(
        r"^gear/(?P<pk>\d+)/gallery/(?P<pic_pk>\d+)/delete/$",
        gear_gallery_delete,
        name="gear-gallery-delete",
    ),
    url(r"^gear/(?P<pk>\d+)/documents/$", gear_documents, name="gear-documents"),
    url(
        r"^gear/(?P<pk>\d+)/documents/new/$",
        gear_documents_add,
        name="gear-documents-add",
    ),
    url(
        r"^gear/(?P<pk>\d+)/documents/(?P<document_pk>\d+)/edit/$",
        gear_documents_update,
        name="gear-documents-update",
    ),
    url(
        r"^gear/(?P<pk>\d+)/documents/(?P<document_pk>\d+)/delete/$",
        gear_documents_delete,
        name="gear-documents-delete",
    ),
    
]
