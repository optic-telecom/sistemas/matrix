from operator import itemgetter
from itertools import tee, islice, chain
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import truncatechars
from simple_history.models import HistoricalRecords
from customers.lefu.cl_territory import COMMUNE_CHOICES
from customers.mikrotik import get_leases, get_dropped
from customers.models import Service


# BIG CRUNCH!!!1
def previous_and_next(some_iterable):
    prevs, items, nexts = tee(some_iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(prevs, items, nexts)


class BaseModel(models.Model):
    history = HistoricalRecords(inherit=True)

    def action_log(self, just=[]):
        fields = self.__class__._meta.fields
        filtered_fields = list(filter(lambda f: f.get_attname() in just, fields) if just else fields)
        field_names = [f.get_attname() for f in filtered_fields]
        historic_field_names = (just or field_names) + ['history_user_id', 'history_date']
        users = {None: 'matrix'}
        users.update({u.pk: u.username for u in get_user_model().objects.filter(id__in=self.history.values_list('history_user_id', flat=True).distinct())})
        rx = self.history.values(*historic_field_names)
        changes = []

        
        for prev, record, nxt in previous_and_next(rx):
            if nxt:
                changed_fields = [f for f in filtered_fields if record[f.get_attname()] != nxt[f.get_attname()]]
                verbose_changed_fields = [f.verbose_name for f in changed_fields]
                if changed_fields:
                    changes.append({'user': users[record['history_user_id']],
                                    'action': _('changed'),
                                    'changes': verbose_changed_fields,
                                    'current_values': {field.attname: record[field.attname] for field in changed_fields},
                                    'previous_values': {field.attname: nxt[field.attname] for field in changed_fields},
                                    'dt': record['history_date']})
            else:
                if not just:
                    changes.append({'user': users[record['history_user_id']],
                                    'action': _('created'),
                                    'dt': record['history_date']})
        return changes

    class Meta:
        abstract = True


class Group(BaseModel):
    name = models.CharField(_('name'), max_length=20, unique=True)


class BuildingContact(BaseModel):
    CONSERJE = 1
    MAYORDOMO = 2
    ADMINISTRADOR = 3
    MIEMBRO_COMITE = 4
    OTRO = 5
    TYPE_CHOICES = ((CONSERJE, _('conserje')),
                    (MAYORDOMO, _('mayordomo')),
                    (ADMINISTRADOR, _('administrador')),
                    (MIEMBRO_COMITE, _('miembro del comité')),
                    (OTRO, _('otro')))

    kind = models.PositiveSmallIntegerField(_('kind'), default=CONSERJE, choices=TYPE_CHOICES)
    name = models.CharField(_('name'), max_length=255, blank=True)
    phone = models.CharField(_('phone'), max_length=75, blank=True)
    email = models.EmailField(blank=True)
    node = models.ForeignKey('Node', verbose_name=_('node'))

    class Meta:
        verbose_name = _('contact')
        verbose_name_plural = _('contacts')


class Node(BaseModel):
    WIRED = 1
    PENDING = 2
    IN_PROGRESS = 3
    NO_LABEL = 4
    DISCARDED = 5

    STATUS_CHOICES = ((WIRED, _('wired')),
                      (PENDING, _('pending')),
                      (IN_PROGRESS, _('in progress')),
                      (NO_LABEL, _('no label')),
                      (DISCARDED, _('discarded')))

    STATUS_CLASSES = {WIRED: 'success',
                      PENDING: 'danger',
                      IN_PROGRESS: 'warning'}

    group = models.ForeignKey('Group', verbose_name=_('group'), null=True, blank=True)
    code = models.CharField(_('code'), max_length=40)
    status = models.PositiveSmallIntegerField(_('status'), default=WIRED, choices=STATUS_CHOICES)
    address = models.CharField(_('address'), max_length=255)
    street = models.CharField(_('street'), max_length=75, blank=True)
    house_number = models.CharField(_('house number'), max_length=75, blank=True)
    commune = models.CharField(_('commune'), max_length=30)
    location = models.CharField(_('location'), max_length=5, choices=COMMUNE_CHOICES)
    parent = models.ForeignKey('self', verbose_name=_('parent'), null=True, blank=True, on_delete=models.CASCADE)
    default_connection_node = models.ForeignKey('self', verbose_name=_('default connection node'), null=True, blank=True, on_delete=models.CASCADE, related_name='+')
    olt_config = models.TextField(null=True, blank=True)
    is_building = models.BooleanField(default=False)

    is_optical_fiber = models.BooleanField(_('is optical fiber?'), default=False)
    gpon = models.BooleanField(_('GPON'), default=False)
    gepon = models.BooleanField(_('GEPON'), default=False)
    pon_port = models.CharField(_('PON port'), max_length=255, default='', blank=True)
    box_location = models.CharField(_('box location'), max_length=255, default='', blank=True)
    splitter_location = models.CharField(_('splitter location'), max_length=255, default='', blank=True)
    expected_power = models.CharField(_('expected power'), max_length=255, default='', blank=True)
    fiber_notes = models.TextField(_('fiber notes'), blank=True)

    towers = models.PositiveSmallIntegerField(default=0)
    apartments = models.PositiveIntegerField(default=0)
    phone = models.CharField(_('phone'), max_length=16, default='', blank=True)
    pics = GenericRelation('gallery.Pic')
    documents = GenericRelation('documents.Document')
    mikrotik_ip = models.GenericIPAddressField(_('ip'), protocol='IPv4')
    mikrotik_username = models.CharField(max_length=255)
    mikrotik_password = models.CharField(max_length=255)
    notes = models.TextField(_('notes'), blank=True)
    plans = models.ManyToManyField('customers.Plan')
    seller = models.ForeignKey('customers.Seller', verbose_name=_('seller'), null=True)
    antennas_left = models.PositiveSmallIntegerField(_('antennas left'), default=0)
    ip_range = models.GenericIPAddressField(_('ip range'), protocol='IPv4', unique=True, blank=True, null=True)
    plan_order_kinds = models.ManyToManyField('PlanOrderKind', verbose_name='tipo OS habilitadas', blank=True)
    no_address_binding = models.BooleanField('no asociar direcciones', default=False)

    @property
    def available_ips(self):
        try:
            ip_range = self.ip_range
            valid = set(['.'.join(ip_range.split('.')[:-1]+[str(n)]) for n in range(2,255)])
            used = set(Gear.objects.filter(ip__startswith='.'.join(ip_range.split('.')[:-1])).values_list('ip', flat=True))
            return sorted(list(valid - used), key=lambda x: int(x.split('.')[-1]))
        except:
            return []

    def refresh_network_equipment(self):
        NotFoundEquipment.objects.all().delete()
        # reset current node's mismatch status
        self.service_set.update(network_mismatch=False, node_mismatch_id=None)

        leases = self.leases()
        for ll in  leases:
            hn = ll['host_name']
            addr = ll['address']
            mac = ll['mac_address']

            number = hn.strip().split('-')[0]
            try:
                cc = Service.objects.get(number=int(number))

                # mark service as network mismatch if found in other node
                if self.pk != cc.node.pk:
                    Service.objects.filter(pk=cc.pk).update(network_mismatch=True, node_mismatch_id=self.id)

                # change ip of primary if is set
                primary = cc.primary_equipment
                if primary:
                    if mac == primary.mac:
                        primary.ip = addr
                        primary.save()
                    else:
                        # save old primary as 0.0.0.0 for history
                        primary.ip = '0.0.0.0'
                        primary.primary = False
                        primary.save()

                        # create new one
                        # with new ip and mac and set as primary
                        NetworkEquipment.objects.create(brand='AUTO',
                                                        model='AUTO',
                                                        ip=addr,
                                                        mac=mac,
                                                        primary=True,
                                                        service=cc)
                else:
                    cc.networkequipment_set.update(primary=False)
                    NetworkEquipment.objects.create(brand='AUTO',
                                                    model='AUTO',
                                                    ip=addr,
                                                    mac=mac,
                                                    primary=True,
                                                    service=cc)
            except (ValueError, Service.DoesNotExist) as e:
                NotFoundEquipment.objects.create(ip=addr,
                                                 mac=mac,
                                                 host=hn)

        
        dropped = self.dropped_clients()

        if (dropped != None):
            self.service_set.filter(number__in=dropped).update(seen_connected=False)
            self.service_set.exclude(number__in=dropped).update(seen_connected=True)
        

    def leases(self):
        try:
            leases = get_leases(self.mikrotik_ip,
                                self.mikrotik_username,
                                self.mikrotik_password)
            return sorted(leases, key=itemgetter('host_name'))
        except:
            return []

    def dropped_clients(self):
        try:
            dropped = get_dropped(self.mikrotik_ip,
                                  self.mikrotik_username,
                                  self.mikrotik_password)
            return dropped
        except:
            return []

    @property
    def get_status_class(self):
        return self.STATUS_CLASSES.get(self.status, 'default')

    @property
    def truncated_code(self):
        return truncatechars(self.code, 30)

    @property
    def composite_address(self):
        return "{} {}".format(self.street, self.house_number)

    @property
    def best_address(self):
        return self.composite_address.strip() or self.address

    @property
    def truncated_address(self):
        return truncatechars(self.best_address, 30)
    
    def __str__(self):
        return "{} ({})".format(self.truncated_address, self.truncated_code)

    class Meta:
        verbose_name = _('node')
        verbose_name_plural = _('nodes')
        ordering = ['code']
        permissions = (('view_node', 'Can view node'),
                       ('list_node', 'Can list nodes'))


class Gear(BaseModel):
    kind = models.CharField(_('Model'), max_length=255)
    description = models.TextField(_('Description'), blank=True)
    ip = models.GenericIPAddressField(_('ip'), protocol='IPv4', unique=True)
    mac = models.CharField(_('mac'), max_length=17)
    vlan = models.CharField(_('Management Vlan'), max_length=255)
    node = models.ForeignKey('Node', verbose_name=_('node'))
    firmware_version = models.CharField(_('Firmware Version'), max_length=255)
    notes = models.TextField(_('Internal Notes'), blank=True)
    pics = GenericRelation('gallery.Pic')
    documents = GenericRelation('documents.Document')
    latest_backup_date_cache = models.DateTimeField(_('Last Backup'), blank=True, null=True)
    brand = models.CharField(_('Brand'), max_length=70)
    
    def get_absolute_url(self):
        return reverse('gear-detail', kwargs={'pk': self.pk})

    @property
    def latest_backup_date(self):
        return self.documents.order_by('-created_at').values_list('created_at', flat=True).first()

    @property
    def days_since_last_backup(self):
        lbd = self.latest_backup_date
        if lbd:
            return (timezone.now() - lbd).days

    @property
    def days_since_last_backup_class(self):
        dslb = self.days_since_last_backup
        if dslb is not None:
            if 0 <= dslb <= 20:
                return 'success'
            elif 20 < dslb <= 29:
                return 'warning'
        return 'danger'

    @property
    def modified_at(self):
        return self.history.first().history_date

    @property
    def truncated_description(self):
        return truncatechars(self.description, 30)

    class Meta:
        verbose_name = _('gear')
        verbose_name_plural = _('gears')
        permissions = (('view_gear', 'Can view gears'),
                       ('list_gear', 'Can list gears'))


class NetworkEquipmentQuerySet(models.QuerySet):
    def for_real(self):
        return self.exclude(ip='0.0.0.0')


class NetworkEquipment(BaseModel):
    brand = models.CharField(_('brand'), max_length=255)
    model = models.CharField(_('model'), max_length=255)
    ip = models.GenericIPAddressField(_('ip'), protocol='IPv4')
    mac = models.CharField(_('mac'), max_length=17)
    serial_number = models.CharField(_('serial number'), max_length=255, null=True, blank=True)
    primary = models.BooleanField(default=False)
    notes = models.TextField(_('notes'), blank=True)
    service = models.ForeignKey('customers.Service', null=True, blank=True)
    objects = NetworkEquipmentQuerySet.as_manager()

    def last_history_ip(self):
        first, *hx = self.history.order_by('history_date')
        history = [first]        
        for hh in hx:
            if history[-1].ip != hh.ip:
                history.append(hh)
        last = history.pop()
        return last

    def __str__(self):
        return " - ".join([self.serial_number or '', self.model or '', self.brand or ''])

    class Meta:
        verbose_name = _('network equipment')
        verbose_name_plural = _('network equipments')
        ordering = ['-pk']


class NotFoundEquipment(BaseModel):
    ip = models.GenericIPAddressField(_('ip'), protocol='IPv4')
    mac = models.CharField(_('mac'), max_length=17)
    host = models.CharField(_('host'), max_length=255)

    class Meta:
        verbose_name = _('not found equipment')
        verbose_name_plural = _('not found equipments')


class Availability(models.Model):
    node = models.ForeignKey('Node')
    direccion = models.ForeignKey('customers.Direccion')

    def __str__(self):
        return "{} {}, {}, {}".format(self.direccion.calle, self.direccion, self.direccion.calle.comuna, self.direccion.calle.comuna.region)


class PlanOrderKind(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
