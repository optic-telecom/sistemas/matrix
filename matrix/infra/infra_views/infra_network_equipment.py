from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class NetworkEquipmentView(LoginRequiredMixin, TemplateView):
    template_name = "infra/network_equipment.html"
    extra_context = {
        "dt_api": "/network-equipment-api/"
    }