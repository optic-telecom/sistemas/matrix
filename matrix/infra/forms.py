from django.db.models import Count
from django.forms import ModelForm, Select
from django.forms.models import inlineformset_factory
from easy_select2 import apply_select2
from customers.models import Service
from .models import BuildingContact, Node, Gear, NetworkEquipment
from hr.models import Technician
from django import forms
from customers.lefu.cl_territory import COMMUNE_CHOICES
from scheduler.models import Duration
from dal import autocomplete

ContactFormSet = inlineformset_factory(Node,
                                       BuildingContact,
                                       fields=['kind',
                                               'name',
                                               'phone',
                                               'email'],
                                       max_num=3)


class DurationForm(ModelForm):
    class Meta:
        model = Duration
        fields = ['activity',
                  'duration']
        widgets = {
            'activity': apply_select2(Select),
            'duration': apply_select2(Select),
        }


class NodeForm(ModelForm):
    class Meta:
        model = Node
        fields = ['code',
                  'status',
                  'address',
                  'street',
                  'house_number',
                  'seller',
                  'commune',
                  'location',
                  'parent',
                  'default_connection_node',
                  'ip_range',
                  'mikrotik_ip',
                  'mikrotik_username',
                  'mikrotik_password',
                  'is_building',
                  'is_optical_fiber',
                  'antennas_left',
                  'towers',
                  'apartments',
                  'phone',
                  'plans',
                  'notes',
                  'plans']
        widgets = {
            'seller': apply_select2(Select),
            'parent': apply_select2(Select),
            'default_connection_node': apply_select2(Select),
            'location': apply_select2(Select),
        }


class NodeFiberForm(ModelForm):
    class Meta:
        model = Node
        fields = ['gpon',
                  'gepon',
                  'pon_port',
                  'box_location',
                  'splitter_location',
                  'expected_power',
                  'fiber_notes']


class GearForm(ModelForm):
    class Meta:
        model = Gear
        fields = ['brand',
                  'kind',
                  'ip',
                  'mac',                
                  'vlan',
                  'node',
                  'firmware_version',
                  'description',
                  'notes']
        widgets = {
            'node': apply_select2(Select),
        }

from django.forms.widgets import HiddenInput


class NetworkEquipmentForm(ModelForm):
    technician = forms.ModelChoiceField(
        queryset=Technician.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='tecnicos-ac',
            attrs={
                'data-placeholder': 'Técnico',
                'class': 'form-control',
                "style": "width:100%"
                },
        ),
        required=False)

    def __init__(self, *args, **kwargs):
        if 'service_id' in kwargs:
            self.service_id = kwargs.pop('service_id')
        else:
            self.service_id = None
        super().__init__(*args, **kwargs)
        if "instance" in kwargs:
            self.fields["technician"].widget = HiddenInput()
            self.fields['technician'].label = ''
        else:
            if self.service_id:
                servicio = Service.objects.get(pk=self.service_id)
                equipos_associados = servicio.networkequipment_set.all()
                if equipos_associados.count() > 0:
                    self.fields["technician"].widget = HiddenInput()
                    self.fields['technician'].label = ''


    class Meta:
        model = NetworkEquipment
        fields = ['ip',
                  'brand',
                  'model',
                  'mac',
                  'serial_number',
                  'notes', "technician"]

    class Media:
        css = {
            'all': ('assets/plugins/select2/dist/css/select2.min.css',)
        }
        js = ('assets/plugins/select2/dist/js/select2.min.js', )




class CitiesForm(forms.Form):
    location = forms.ChoiceField(widget=apply_select2(forms.Select), required=False)
    
    def __init__(self, *args, **kwargs):
        super(CitiesForm, self).__init__(*args, **kwargs)
        self.fields['location'].choices = [('', '----')] + COMMUNE_CHOICES


from dal import autocomplete
from customers.models import Region
from customers.models import Comuna
from customers.models import Calle as Street
from customers.models import Direccion

class TieAddressForm(forms.Form):
    region = forms.ModelChoiceField(label='Región', queryset=Region.objects.annotate(num_comunas=Count('comuna')).order_by('-num_comunas'))
    comuna = forms.ModelChoiceField(queryset=Comuna.objects.all(),
                                    widget=autocomplete.ModelSelect2(url='comuna-autocomplete',
                                                                     forward=['region']))
    calle = forms.ModelChoiceField(queryset=Street.objects.all(),
                                   widget=autocomplete.ModelSelect2(url='street-autocomplete',
                                                                    forward=['comuna']))
    direccion = forms.ModelChoiceField(label='Número', queryset=Direccion.objects.all(),
                                   widget=autocomplete.ModelSelect2(url='direccion-autocomplete',
                                                                    forward=['calle']))
