import io
import xlsxwriter
from django.http import HttpResponse
from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from django.db.models import Count
from .models import (PlanOrderKind, Node, Gear, 
    NetworkEquipment, NotFoundEquipment)
from import_export.admin import ImportExportModelAdmin
from .resources import *



def export_nodes(modeladmin, request, queryset):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('ID', 'Dirección', 'Comuna', 'Estado', 'Nombre', 'Es fibra', 'GPON', 'GEPON')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for node in queryset:
        worksheet.write(row, 0, node.pk)
        worksheet.write(row, 1, node.best_address)
        worksheet.write(row, 2, node.get_location_display())
        worksheet.write(row, 3, node.get_status_display())
        worksheet.write(row, 4, node.code)
        worksheet.write(row, 5, node.is_optical_fiber)
        worksheet.write(row, 6, node.gpon)
        worksheet.write(row, 7, node.gepon)
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="nodos.xlsx"'
    return response
export_nodes.short_description = "Exportar información de nodos"


@admin.register(Node)
class NodeAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('id', 'truncated_code', 'truncated_address', 'location', 'parent', 'status', 'no_address_binding')
    list_editable = ('no_address_binding',)
    list_filter = ('status',)
    search_fields = ('id', 'code', 'location')
    raw_id_fields = ('parent', 'seller')
    actions = [export_nodes,]
    resource_class = NodeResource


@admin.register(Gear)
class GearAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    raw_id_fields = ('node',)
    list_display = ('id', 'latest_backup_date_cache', 'ip', 'mac', 'kind', 'firmware_version', 'vlan', 'description')
    resource_class = GearResource

def find_duplicated_macs(modeladmin, request, queryset):
    dupes = NetworkEquipment.objects.values('mac').annotate(Count('id')).order_by().filter(id__count__gt=1)

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('MAC duplicada', 'Ocurrencias')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for dd in dupes:
        worksheet.write(row, 0, dd['mac'])
        worksheet.write(row, 1, dd['id__count'])
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="macs_duplicadas.xlsx"'
    return response
find_duplicated_macs.short_description = "Encontrar MACs duplicadas"
    
def find_duplicated_ips(modeladmin, request, queryset):
    dupes = NetworkEquipment.objects.values('ip').annotate(Count('id')).order_by().filter(id__count__gt=1)

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('IP duplicada', 'Ocurrencias')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for dd in dupes:
        worksheet.write(row, 0, dd['ip'])
        worksheet.write(row, 1, dd['id__count'])
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="ips_duplicadas.xlsx"'
    return response
find_duplicated_ips.short_description = "Encontrar IPs duplicadas"


@admin.register(NetworkEquipment)
class NetworkEquipmentAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ['brand', 'model', 'ip', 'mac', 'service']
    search_fields = ['brand', 'model', 'ip', 'mac', 'service__number', 'serial_number']
    list_filter = ['brand']
    list_select_related = ('service', 'service__plan')
    defer = ('notes',)
    raw_id_fields = ('service',)
    actions = [find_duplicated_macs, find_duplicated_ips]
    resource_class = NetworkEquipmentResource


@admin.register(NotFoundEquipment)
class NotFoundEquipmentAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ['ip', 'mac', 'host']
    search_fields = ['ip', 'mac', 'host']
    resource_class = NotFoundEquipmentResource


@admin.register(PlanOrderKind)
class PlanOrderKindAdmin(ImportExportModelAdmin):
    list_display = ['name']
    resource_class = PlanOrderKindResource

@admin.register(Group)
class GroupAdmin(ImportExportModelAdmin):
    resource_class = GroupResource

@admin.register(Availability)
class AvailabilityAdmin(ImportExportModelAdmin):
    resource_class = AvailabilityResource