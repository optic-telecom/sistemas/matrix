import requests
import base64, calendar, json, pdfkit, pickle
from django.conf import settings
from django.core.exceptions import ValidationError
from constance import config
from customers.models import Invoice
from django.template import loader
from django.template.loader import get_template
from io import BytesIO, StringIO
from pdf417gen import render_image, encode
from django.shortcuts import render

def notify_create_invoice(id, pdf):
    invoice=Invoice.objects.get(id=id)  
    # Solo si es una Boleta, Factura o nota de credito.
    if invoice.kind in [1,2,3]:
        try:
            services=[]
            for i in invoice.service.all():
                services.append(i.number)
            url = config.iris_send_invoice_url.format(ID=services[0])
            
            r = requests.post(url,
                data={
                'event':'nuevo_dte',
                'info':[
                {
                    "service":services,
                    "operator": invoice.operator.pk,
                    "context":[
                    {'name':invoice.customer.name,
                    'rut':invoice.customer.rut
                    }
                ],
                'files':[base64.b64encode(pdf).decode()
                    ],
                }
                ]
                },
                headers={
                    "Authorization":'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.\
                    eyJ1c2VyX2lkIjoxOSwidXNlcm5hbWUiOiJqb3Zhbi5wYWNoZWNvQG11bHRpZmliZXIuY2wiLCJleHAiOjE1ODYzNjE4MDUsImVtYWlsIjoiIn0.\
                    oY8lmiOsmMk4p3dJ9crXfugjcV7_UD5t6p2bdStm6zA'
                })
            
        except Exception as e:
            print (e)

