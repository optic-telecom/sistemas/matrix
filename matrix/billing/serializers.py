import json, calendar, pdfkit, base64
from customers.models import *
from django.db.models.functions import Concat, ExtractMonth, ExtractYear
from django.db.models import Value, Q, F, Max, Count, Sum
from datetime import datetime
from django.utils.timezone import timedelta
from django.contrib.postgres.fields import JSONField
from rest_framework import serializers
from config.settings.constance_settings import *
from decimal import *
from django.conf import settings
from io import BytesIO, StringIO
from pdf417gen import render_image, encode
from billing.signals import notify_create_invoice
from django.template.loader import get_template
from constance import config
from json import loads
from dateutil import relativedelta

# Generate a PDF417 barcode from a string
def generate_barcode(data):
    # Create a memory bytes buffered
    buffered = BytesIO()
    # Render and save the barcode's pic
    image = render_image(encode(data, columns=13))
    image.save(buffered, format="JPEG")
    # Encode barcode into base64 for PDF can read
    img_str = base64.b64encode(buffered.getvalue())
    return img_str


# Order and group information to display in PDF for a service
def get_invoiceData(atr):
    invoice = Invoice.objects.filter(pk=atr)
    for s in invoice:
        data = {"fecha": s.created_at.strftime("%d/%m/%Y")}
        data["service"] = s.service.all().annotate(price=Count("id"))
        # Credits information
        credits = s.credit.all()
        credit = Credit.objects.none()
        for c in credits:
            credit = credit | Credit.objects.filter(id=c.credit_applied.id)
        if credit.count() != 0:
            data["credit"] = credit
            for c in data["credit"]:
                # Save the amount without VAT for a credit
                c.amount = s.credit.get(credit_applied=c).amount

        # Charges information
        charges = s.charge.all()
        charge = Charge.objects.none()
        for c in charges:
            charge = charge | Charge.objects.filter(id=c.charge_applied.id)
        if charge.count() != 0:
            data["charge"] = charge
            for c in data["charge"]:
                # Save the amount without VAT for a credit
                c.amount = s.charge.get(charge_applied=c).amount
        # Save operator information
        data["operator"] = OperatorInformation.objects.filter(operator_id=s.operator_id)
        iva = float(
            Indicators.objects.filter(kind=2,company=s.operator.company)
            .distinct("kind")
            .values("kind", "created_at", "value")
            .first()["value"]
        )
        if s.iva:
            data["iva"] = int(s.iva * 100)
            data["simple_iva"] = Decimal(s.iva)
        else:
            # Save VAT amount
            data["iva"] = int(iva * 100)
            data["simple_iva"] = Decimal(iva)
        # Save subtotal information
        data["extra"] = prepare_pdf_after(data)
        # Save total amount

        credit_notes = Invoice.objects.filter(parent_id=s, kind=3)
        cobro_notes = Invoice.objects.filter(parent_id=s, kind=6)

        price = 0
        for c in credit_notes:
            price = price - c.total

        for c in cobro_notes:
            price = price + c.total

        data["total"] = s.total
        data["subtotal_plan"] = s.total - data["extra"][3] + data["extra"][2]
        data["invoice"] = s
        data["without_iva"] = round(data["total"] / (1 + iva), 2)
        data["only_iva"] = round(data["total"] - data["without_iva"], 2)

        commune = []
        for c in data["service"]:
            if not (c.node.commune in commune):
                commune.append(c.node.commune)
        data["commune"] = commune

        # Price that is show in the pdf
        if len(data["service"]) == 1:
            if s.json:
                value = s.json[str(data["service"][0].id)]["MontoItem"]
                data["service"][0].price = round(float(value) * (1 + iva))
            else:
                data["service"][0].price = data["subtotal_plan"]
        else:
            if s.json:
                for service in data["service"]:
                    value = s.json[str(service.id)]["MontoItem"]
                    service.price = round(float(value) * (1 + iva))
            else:
                for service in data["service"]:
                    service.price = data["subtotal_plan"] / len(data["service"])

        # For credit notes, debit note and payment note (cobro)
        if s.kind in [3, 4, 5, 6]:
            data["fecha2"] = s.created_at
            if s.parent_id:
                data["fecha_parent"] = s.parent_id.created_at.strftime("%d-%m-%Y")
        return data


def prepare_pdf_after(data):
    """ Function that calculates the VAT of subtotal for credits and charges,
    and also the subtotal with it """
    iva = data["simple_iva"]
    subtotal_credit = 0
    ivaCredit = 0
    total_ivaCredit = 0
    credit = data.get("credit")
    if credit:
        for i in data["credit"]:
            subtotal = i.amount
            if i.service.document_type == 1:
                # Si es Boleta no tiene iva
                i.unit_price = Decimal(subtotal)
                if i.quantity:
                    subtotal = subtotal * i.quantity
                ivaCredit = 0
                subtotal_credit = subtotal_credit + Decimal(subtotal)
            else:
                # Colocando el iva.
                i.unit_price = Decimal(subtotal) * (1 + iva)
                if i.quantity:
                    subtotal = subtotal * i.quantity
                total_ivaCredit = total_ivaCredit + Decimal(subtotal) * (iva)
                ivaCredit = Decimal(subtotal) * (1 + iva)
                ivaCredit = round(ivaCredit, 0)
                subtotal_credit = subtotal_credit + ivaCredit
    subtotal_charge = 0
    ivaCharge = 0
    total_ivaCharge = 0
    charge = data.get("charge")
    if charge:
        for i in data["charge"]:
            subtotal = i.amount
            if i.service.document_type == 1:
                # Colocando el iva.
                i.unit_price = Decimal(subtotal)
                if i.quantity:
                    subtotal = subtotal * i.quantity
                ivaCharge = 0
                subtotal_charge = subtotal_charge + Decimal(subtotal)
            else:
                # Colocando el iva.
                i.unit_price = Decimal(subtotal) * (1 + iva)
                if i.quantity:
                    subtotal = subtotal * i.quantity
                total_ivaCharge = total_ivaCharge + Decimal(subtotal) * (iva)
                ivaCharge = Decimal(subtotal) * (1 + iva)
                ivaCharge = round(ivaCharge, 0)
                subtotal_charge = subtotal_charge + ivaCharge

    total_ivaCredit = round(total_ivaCredit, 0)
    total_ivaCharge = round(total_ivaCharge, 0)
    return (
        credit,
        charge,
        subtotal_credit,
        subtotal_charge,
        total_ivaCredit,
        total_ivaCharge,
    )


def calculo(i):
    """ Function that calculates the unit price without the VAT of the 
    credits or charges that are passed to it as a parameter """
    iva = (
        Indicators.objects.filter(kind=2, company=i.service.operator.company)
        .distinct("kind")
        .values("kind", "created_at", "value")
        .first()["value"]
    )
    # Si se calcula por UF, siempre se carga el valor multiplicado por el UF
    if i.reason.uf:
        uf = (
            Indicators.objects.filter(kind=1,company=i.service.operator.company)
            .distinct("kind")
            .values("kind", "created_at", "value")
            .first()["value"]
        )
        subtotal = i.reason.valor * uf
    else:
        # Si es porcentaje, se aplica el % correspondiente.
        if i.reason.percent:
            if i.service.price_override and i.service.price_override != 0:
                plan_price = i.service.price_override
                if i.service.plan.uf:
                    uf = (
                        Indicators.objects.filter(kind=1,company=i.service.operator.company)
                        .distinct("kind")
                        .values("kind", "created_at", "value")
                        .first()["value"]
                    )
                    plan_price = round(
                        i.service.price_override * uf * Decimal(1 + iva), 0
                    )
            else:
                plan_price = i.service.plan.price

            if i.reason.calculate_by_days:
                range_month = calendar.monthrange(i.year, i.month)
                porcentaje = (i.days_count * 100) / range_month[1]
                if i.service.document_type == 1:
                    price_sin_iva = round(plan_price)
                else:
                    price_sin_iva = round(plan_price / (1 + iva))
                subtotal = ((price_sin_iva) * Decimal(porcentaje)) / Decimal("100")
            else:
                if i.service.document_type == 1:
                    price_sin_iva = round(plan_price / (1 + iva)) # round(plan_price)
                else:
                    price_sin_iva = round(plan_price / (1 + iva))
                subtotal = Decimal((price_sin_iva) * i.reason.valor) / Decimal("100")
        else:
            # Si es por monto y es fijo
            if i.permanent:
                # Si calcula por los días o se pone el valor directo.
                if i.reason.calculate_by_days:
                    subtotal = (i.days_count) * i.reason.valor
                else:
                    subtotal = i.reason.valor
            else:
                # Si no es fijo, se calcula cual es el valor.
                if i.times_applied > 0:
                    if i.reason.calculate_by_days:
                        days = int(i.amount_applied / i.reason.valor)
                        falta_days = i.days_count - days
                        falta = i.number_billing - i.times_applied
                        subtotal = (falta_days / falta) * i.reason.valor
                    else:
                        cantidad = i.reason.valor - i.amount_applied
                        falta = i.number_billing - i.times_applied
                        subtotal = cantidad / falta
                else:
                    if i.reason.calculate_by_days:
                        subtotal = (i.days_count / i.number_billing) * i.reason.valor
                    else:
                        subtotal = i.reason.valor / i.number_billing
    subtotal = round(subtotal, 0)
    return subtotal


class ResponseSerializer(serializers.Serializer):
    response = serializers.JSONField()
    order = serializers.JSONField()

    """ dataResponse = loads(response)
    if "response" in dataResponse and "addBarCode" in dataResponse["response"]:
        Invoice.objects.filter(folio=int(dataResponse["response"]["folio"])).update(
            barcode=dataResponse["response"]["barCode"]
        ) """

    def create(self, validated_data):
        print("Iniciando create")
        if (
            validated_data["order"] == False
            and "addBarCode" in validated_data["response"]
        ):
            kind = int(validated_data["response"]["kind"])
            if kind == 39:
                kind = 1
            elif kind == 33:
                kind = 2

            Invoice.objects.filter(
                folio=int(validated_data["response"]["folio"]), kind=kind
            ).update(barcode=validated_data["response"]["barCode"])
        else:
            dataToModel = {"response": validated_data, "date": datetime.now()}
            TaxationResponse.objects.create(**dataToModel)
            # Updating data
            data = validated_data
            for d in data["response"]:
                if int(d["tipoDte"]) == 39:
                    kindOrder = "boleta"
                if int(d["tipoDte"]) == 33:
                    kindOrder = "factura"
                folio = data["order"][kindOrder]["completed"][d["folio"]]["idFolio"]

                if "observation" in data["order"][kindOrder]["completed"][d["folio"]]:
                    observation = data["order"][kindOrder]["completed"][d["folio"]][
                        "observation"
                    ]
                    if observation:
                        addObservation = observation
                    else:
                        addObservation = ""
                else:
                    addObservation = ""
                # Error case
                if d["resultado"] == "False":
                    # Add the folio to the corresponding batch
                    batch = InvoiceBatch.objects.filter(id=folio).first()
                    key = list(
                        data["order"][kindOrder]["completed"][d["folio"]]["folio"]
                    )[0]
                    batch.batch[key] = int(d["folio"])
                    # Update batch
                    InvoiceBatch.objects.filter(id=folio).update(
                        batch=batch.batch, stock=len(batch.batch)
                    )
                    # Add error to processedBatch and make it True
                    idProcessed = data["order"][kindOrder]["completed"][d["folio"]][
                        "idProcessed"
                    ]
                    processed = ProcessedBatch.objects.filter(id=idProcessed).first()
                    processed.processed[d["folio"]]["error"] = d["error"]
                    # Update processed
                    ProcessedBatch.objects.filter(id=idProcessed).update(
                        processed=processed.processed, status=True
                    )
                # Without error
                else:
                    # Add barCode to processedBatch and make it True
                    idProcessed = data["order"][kindOrder]["completed"][d["folio"]][
                        "idProcessed"
                    ]
                    processed = ProcessedBatch.objects.filter(id=idProcessed).first()
                    processed.processed[d["folio"]]["barCode"] = d["barCode"]
                    # Update processed
                    ProcessedBatch.objects.filter(id=idProcessed).update(
                        processed=processed.processed, status=True
                    )
                    ### INVOICE CREATION ###
                    if d["create_invoice"]:
                        if (
                            "unified"
                            in data["order"][kindOrder]["completed"][d["folio"]]
                        ):
                            customer = data["order"][kindOrder]["completed"][
                                d["folio"]
                            ]["unified"]["customer"]
                            services = data["order"][kindOrder]["completed"][
                                d["folio"]
                            ]["unified"]["services"]
                            customer = Customer.objects.get(id=int(customer))
                            service = Service.objects.filter(id__in=services)
                        else:
                            services = data["order"][kindOrder]["completed"][
                                d["folio"]
                            ]["service"]
                            customer = Service.objects.get(id=services).customer
                            service = Service.objects.filter(id=int(services))

                        json_inv = {}
                        iva = (
                            Indicators.objects.filter(kind=2, company=customer.company)
                            .distinct("kind")
                            .values("kind", "created_at", "value")
                            .first()["value"]
                        )
                        for s in service:
                            if s.price_override and s.price_override != 0:
                                price_to_apply = s.price_override
                                if s.plan.uf:
                                    uf = (
                                        Indicators.objects.filter(kind=1,company=s.operator.company)
                                        .distinct("kind")
                                        .values("kind", "created_at", "value")
                                        .first()["value"]
                                    )
                                    price_to_apply = round(
                                        s.price_override * uf * Decimal(1 + iva), 0
                                    )
                            else:
                                price_to_apply = s.plan.price
                            json_inv[str(s.id)] = {
                                "QtyItem": 1,
                                "PrcItem": str(round(price_to_apply / (1 + iva), 2)),
                                "MontoItem": str(round(price_to_apply / (1 + iva), 0)),
                            }

                        totalAmount = d["invoiceData"][str(d["folio"])]["totalAmount"]
                        operatorRut = d["invoiceData"][str(d["folio"])]["operatorRut"]
                        operatorId = (
                            OperatorInformation.objects.filter(rut=operatorRut)
                            .first()
                            .operator_id
                        )
                        iva = (
                            Indicators.objects.filter(kind=2,company=customer.company)
                            .distinct("kind")
                            .values("kind", "created_at", "value")
                            .first()["value"]
                        )
                        # Factura
                        if int(d["tipoDte"]) == 33:
                            comment = "Factura electrónica #" + str(d["folio"])
                            inv = Invoice.objects.create(
                                created_at=datetime.now(),  # CAMBIAR POR FECHA DEL DOCUMENTO
                                due_date=(datetime.now().replace(day=5) + relativedelta.relativedelta(months=1)),
                                #due_date=(
                                #    datetime.now() + timedelta(365 / 12)
                                #).strftime("%Y-%m-05"),
                                folio=int(d["folio"]),
                                kind=2,
                                comment=comment,
                                customer=customer,
                                total=round(totalAmount, 0),
                                operator_id=operatorId,
                                barcode=d["barCode"],
                                iva=iva,
                                json=json_inv,
                                observation=addObservation,
                            )

                            # Inicializa queryset vacio
                            credits = Credit.objects.none()
                            charges = Charge.objects.none()
                            for i in service:
                                inv.service.add(i)
                                inv.save()
                                credits = credits | Credit.objects.filter(
                                    service=i,
                                    status_field=False,
                                    cleared_at__isnull=True,
                                    active=True
                                ).annotate(
                                    unit_price=Count("id"),
                                    monto=Count("id"),
                                    year=ExtractYear("created_at"),
                                    month=ExtractMonth("created_at"),
                                )
                                charges = charges | Charge.objects.filter(
                                    service=i,
                                    status_field=False,
                                    cleared_at__isnull=True,
                                ).annotate(
                                    unit_price=Count("id"),
                                    monto=Count("id"),
                                    year=ExtractYear("created_at"),
                                    month=ExtractMonth("created_at"),
                                )

                            if credits:
                                for i in credits:
                                    subtotal = calculo(i)
                                    descuento = CreditApplied.objects.create(
                                        credit_applied=i, amount=subtotal
                                    )
                                    if i.permanent:
                                        # Si no tiene monto aplicado, se guarda 0
                                        if i.amount_applied == None:
                                            i.amount_applied = 0
                                        # Se aumentan ambos valores, para que sea infinito la cant de ciclos.
                                        i.times_applied = i.times_applied + 1
                                        i.number_billing = i.number_billing + 1
                                        i.save()
                                    else:
                                        # Si no se aplicado se guarda el subtotal
                                        if i.amount_applied == None:
                                            i.amount_applied = subtotal
                                        else:
                                            # Si ya se aplico antes, se suman los valores.
                                            i.amount_applied = (
                                                i.amount_applied + subtotal
                                            )
                                        i.times_applied = i.times_applied + 1
                                        if i.times_applied == i.number_billing:
                                            i.cleared_at = date.today()
                                        i.save()
                                    inv.credit.add(descuento)
                                    inv.save()

                            if charges:
                                for i in charges:
                                    subtotal = calculo(i)
                                    cargo = ChargeApplied.objects.create(
                                        charge_applied=i, amount=subtotal
                                    )
                                    if i.permanent:
                                        # Si no tiene monto aplicado, se guarda 0
                                        if i.amount_applied == None:
                                            i.amount_applied = 0
                                        # Se aumentan ambos valores, para que sea infinito la cant de ciclos.
                                        i.times_applied = i.times_applied + 1
                                        i.number_billing = i.number_billing + 1
                                        i.save()
                                    else:
                                        # Si no se aplicado se guarda el subtotal
                                        if i.amount_applied == None:
                                            i.amount_applied = subtotal
                                        else:
                                            # Si ya se aplico antes, se suman los valores.
                                            i.amount_applied = (
                                                i.amount_applied + subtotal
                                            )
                                        i.times_applied = i.times_applied + 1
                                        if i.times_applied == i.number_billing:
                                            i.cleared_at = date.today()
                                        i.save()
                                    inv.charge.add(cargo)
                                    inv.save()

                        # Boleta
                        if int(d["tipoDte"]) == 39:
                            comment = "Boleta electrónica #" + str(d["folio"])
                            inv = Invoice.objects.create(
                                created_at=datetime.now(),
                                due_date=(datetime.now().replace(day=5) + relativedelta.relativedelta(months=1)),
                                #due_date=(
                                #    datetime.now() + timedelta(365 / 12)
                                #).strftime("%Y-%m-05"),
                                folio=int(d["folio"]),
                                kind=1,
                                comment=comment,
                                customer=customer,
                                total=round(totalAmount, 0),
                                operator_id=operatorId,
                                barcode=d["barCode"],
                                iva=iva,
                                json=json_inv,
                                observation=addObservation,
                            )

                            # Inicializa queryset vacio
                            credits = Credit.objects.none()
                            charges = Charge.objects.none()
                            for i in service:
                                inv.service.add(i)
                                inv.save()
                                credits = credits | Credit.objects.filter(
                                    service=i,
                                    status_field=False,
                                    cleared_at__isnull=True,
                                    active=True
                                ).annotate(
                                    unit_price=Count("id"),
                                    monto=Count("id"),
                                    year=ExtractYear("created_at"),
                                    month=ExtractMonth("created_at"),
                                )
                                charges = charges | Charge.objects.filter(
                                    service=i,
                                    status_field=False,
                                    cleared_at__isnull=True,
                                ).annotate(
                                    unit_price=Count("id"),
                                    monto=Count("id"),
                                    year=ExtractYear("created_at"),
                                    month=ExtractMonth("created_at"),
                                )

                            if credits:
                                for i in credits:
                                    subtotal = calculo(i)
                                    descuento = CreditApplied.objects.create(
                                        credit_applied=i, amount=subtotal
                                    )
                                    if i.permanent:
                                        # Si no tiene monto aplicado, se guarda 0
                                        if i.amount_applied == None:
                                            i.amount_applied = 0
                                        # Se aumentan ambos valores, para que sea infinito la cant de ciclos.
                                        i.times_applied = i.times_applied + 1
                                        i.number_billing = i.number_billing + 1
                                        i.save()
                                    else:
                                        # Si no se aplicado se guarda el subtotal
                                        if i.amount_applied == None:
                                            i.amount_applied = subtotal
                                        else:
                                            # Si ya se aplico antes, se suman los valores.
                                            i.amount_applied = (
                                                i.amount_applied + subtotal
                                            )
                                        i.times_applied = i.times_applied + 1
                                        if i.times_applied == i.number_billing:
                                            i.cleared_at = date.today()
                                        i.save()
                                    inv.credit.add(descuento)
                                    inv.save()

                            if charges:
                                for i in charges:
                                    subtotal = calculo(i)
                                    cargo = ChargeApplied.objects.create(
                                        charge_applied=i, amount=subtotal
                                    )
                                    if i.permanent:
                                        # Si no tiene monto aplicado, se guarda 0
                                        if i.amount_applied == None:
                                            i.amount_applied = 0
                                        # Se aumentan ambos valores, para que sea infinito la cant de ciclos.
                                        i.times_applied = i.times_applied + 1
                                        i.number_billing = i.number_billing + 1
                                        i.save()
                                    else:
                                        # Si no se aplicado se guarda el subtotal
                                        if i.amount_applied == None:
                                            i.amount_applied = subtotal
                                        else:
                                            # Si ya se aplico antes, se suman los valores.
                                            i.amount_applied = (
                                                i.amount_applied + subtotal
                                            )
                                        i.times_applied = i.times_applied + 1
                                        if i.times_applied == i.number_billing:
                                            i.cleared_at = date.today()
                                        i.save()
                                    inv.charge.add(cargo)
                                    inv.save()
        return validated_data

    class Meta:
        model = TaxationResponse
        fields = "__all__"
