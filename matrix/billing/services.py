import requests
from constance import config


def generate_request(url, params):
    response = requests.post(url, data=params, timeout=30)
    return response.json()


def get_invoices(params={}):
    response = generate_request(getattr(config, "taxation_address"), params)
    if response:
        return response
    else:
        return "Error", response
