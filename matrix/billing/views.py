import base64, calendar, json, pdfkit, pickle
from customers.models import CalendarioCortes
from customers.forms import *
from billing.services import *
from .processing import *
from customers.models import *
from customers.serializers import *
from datetime import date, datetime, timedelta
from decimal import *
from django import template
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.db import transaction
from django.db.models import Q, Count, Sum, Min, Max
from django.db.models.functions import ExtractMonth, ExtractYear
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.template.loader import get_template
from django.views.generic import ListView, CreateView
from infra.models import Node
from io import BytesIO, StringIO
from pdf417gen import render_image, encode
from rest_framework import views, viewsets
from constance import config
from rest_framework.decorators import action
from django.db.models.functions import Length
from django.contrib.auth.models import Group, User
from customers.models import UserOperator, Company, Operator, UserCompany
from django.contrib import messages
from django.core.urlresolvers import reverse

# Function to register invoice's batch


def createInvoiceBatch(inicial, final, kind, expiration, operator):
    index = list(range(0, final - inicial + 1))
    folios = list(range(inicial, final + 1))
    allFolios = {}
    for i in index:
        allFolios[i] = folios[i]
    InvoiceBatch.objects.create(
        batch=allFolios,
        kind=kind,
        created_on=datetime.now(),
        expiration=expiration,
        stock=(final - inicial + 1),
        operator=Operator.objects.get(id=int(operator)),
    )
    return allFolios


################# INVOICE PREPARE ##################


def prepare_invoice_pdf_after(data):
    """ Function that calculates the VAT of subtotal for credits and charges,
    and also the subtotal with it """
    iva = data["simple_iva"]
    subtotal_credit = 0
    ivaCredit = 0
    total_ivaCredit = 0
    credit = data.get("credit")
    if credit:
        for i in data["credit"]:
            subtotal = i.amount
            if i.service.document_type == 1:
                # Si es Boleta no tiene iva
                i.unit_price = Decimal(subtotal)
                if i.quantity:
                    subtotal = subtotal * i.quantity
                ivaCredit = 0
                subtotal_credit = subtotal_credit + Decimal(subtotal)
            else:
                # Colocando el iva.
                i.unit_price = Decimal(subtotal) * (1 + iva)
                if i.quantity:
                    subtotal = subtotal * i.quantity
                total_ivaCredit = total_ivaCredit + Decimal(subtotal) * (iva)
                ivaCredit = Decimal(subtotal) * (1 + iva)
                ivaCredit = round(ivaCredit, 0)
                subtotal_credit = subtotal_credit + ivaCredit
    subtotal_charge = 0
    ivaCharge = 0
    total_ivaCharge = 0
    charge = data.get("charge")
    if charge:
        for i in data["charge"]:
            subtotal = i.amount
            if i.service.document_type == 1:
                # Colocando el iva.
                i.unit_price = Decimal(subtotal)
                if i.quantity:
                    subtotal = subtotal * i.quantity
                ivaCharge = 0
                subtotal_charge = subtotal_charge + Decimal(subtotal)
            else:
                # Colocando el iva.
                i.unit_price = Decimal(subtotal) * (1 + iva)
                if i.quantity:
                    subtotal = subtotal * i.quantity
                total_ivaCharge = total_ivaCharge + Decimal(subtotal) * (iva)
                ivaCharge = Decimal(subtotal) * (1 + iva)
                ivaCharge = round(ivaCharge, 0)
                subtotal_charge = subtotal_charge + ivaCharge

    total_ivaCredit = round(total_ivaCredit, 0)
    total_ivaCharge = round(total_ivaCharge, 0)
    return (
        credit,
        charge,
        subtotal_credit,
        subtotal_charge,
        total_ivaCredit,
        total_ivaCharge,
    )


################# INVOICE PRESENTAION ##################

# Generate a PDF417 barcode from a string
def barcode(data):
    # Create a memory bytes buffered
    buffered = BytesIO()
    # Render and save the barcode's pic
    try:
        image = render_image(encode(data, columns=13))
    except Exception as e:
        image = render_image(encode(""" Error el codigo de barra es muy largo"""))
    image.save(buffered, format="JPEG")
    # Encode barcode into base64 for PDF can read
    img_str = base64.b64encode(buffered.getvalue())
    return img_str


# Order and group information to display in PDF for a service
def invoiceData(atr):
    data = {"fecha": datetime.now().strftime("%d/%m/%Y")}
    service = Service.objects.filter(pk=atr).annotate(
        total=Count("id"), price_without_iva=Count("id")
    )
    for s in service:
        # Credits information
        credit = Credit.objects.filter(
            service_id=s.id, status_field=False, cleared_at__isnull=True, active=True
        ).annotate(year=ExtractYear("created_at"), month=ExtractMonth("created_at"),)
        total_credit = 0
        total_charge = 0
        iva = (
            Indicators.objects.filter(kind=2,company=s.operator.company)
            .distinct("kind")
            .values("kind", "created_at", "value")
        )
        iva = float(iva.first()["value"])
        if credit:
            data["credit"] = credit
            for c in data["credit"]:
                if s.document_type==1:
                    # Save the amount without VAT for a credit
                    c.amount = calculo(c)
                    #c.amount =  round( s.credit.get(credit_applied=c).amount / (1 + iva), 0)
                else:
                    c.amount = calculo(c)
                """if s.document_type==1:
                    # Save the amount without VAT for a credit
                    c.amount = round( float(calculo(c)) / (1 + iva), 0)
                    #c.amount =  round( s.credit.get(credit_applied=c).amount / (1 + iva), 0)
                else:
                    c.amount = calculo(c)"""
                total_credit = total_credit + Decimal(c.amount)

        # Charges information
        charge = Charge.objects.filter(
            service_id=s.id, status_field=False, cleared_at__isnull=True
        ).annotate(year=ExtractYear("created_at"), month=ExtractMonth("created_at"),)
        if charge:
            data["charge"] = charge
            for c in data["charge"]:
                # Save the amount without VAT for a credit
                c.amount =int(round(float(calculo(c)), 0))
                total_charge = total_charge + c.amount
                #c.amount = int(round(float(calculo(c)) / float(1 + iva), 0))

        # Save operator information
        data["operator"] = OperatorInformation.objects.filter(operator_id=s.operator_id)
        # Save VAT amount
        data["iva"] = int(iva * 100)
        # Save subtotal information
        data["extra"] = prepare_invoice_pdf(s.id)
        # Save total amount
        if s.price_override and s.price_override != 0:
            price_to_apply = s.price_override
            if s.plan.uf:
                uf = (
                    Indicators.objects.filter(kind=1, company=s.operator.company)
                    .distinct("kind")
                    .values("kind", "created_at", "value")
                    .first()["value"]
                )
                price_to_apply = round(s.price_override * uf * Decimal(1 + iva), 0)
        else:
            price_to_apply = s.plan.price
        s.total = price_to_apply
        data["total_credit"] = int(round(float(total_credit), 0)) # / float(1 + iva), 0))
        s.price_without_iva = int(round(float(price_to_apply) / float(1 + iva), 0))
        #print(s.price_without_iva)
        #data["total"] = int(float(price_to_apply) + data["extra"][3] - data["total_credit"]) #data["extra"][2])
        data["total_antes"] = int(float(s.price_without_iva) + total_charge - data["total_credit"])
        #print(data["total_antes"])
        data["service"] = s

        data["monto_neto"] = int(round(float(data["total_antes"]), 0))
        data["monto_iva"] = int(round(float(data["monto_neto"]) * (iva), 0))
        data["total"]=data["monto_neto"]+ data["monto_iva"]

        balance_service = Balance.objects.get(customer=s.customer).balance
        if balance_service < 0:
            data["saldo"] = (-1) * balance_service
            data["total_pagar"] = int(data["total"] + (-1) * balance_service)
        else:
            data["saldo"] =  (-1) * balance_service
            if (data["total"] - balance_service)<0:
                data["total_pagar"]=0
            else:
                data["total_pagar"] = int(data["total"] - balance_service)

        Month = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }
        data["mes"] = getattr(config, "month_invoice") #Month[s.created_at.month]
        data["year"] = getattr(config, "year_invoice") #s.created_at.year

        # data["due"] = s.due_date.date
        # print(data)
        return data


# Order and group information to display in PDF for a unified document
def unifiedInvoiceData(atr):
    customer = Customer.objects.filter(id=atr).first()
    services = Service.objects.filter(customer_id=atr, status=1, unified_billing=True)
    operator = OperatorInformation.objects.filter(
        operator_id=services.first().operator_id
    ).first()
    iva = (
        Indicators.objects.filter(kind=2, company=customer.company)
        .distinct("kind")
        .values("kind", "created_at", "value")
    )
    iva = float(iva.first()["value"])
    serviceList = []
    creditList = []
    chargeList = []
    subTotalServices = 0
    total_credit = 0
    montoNeto = 0
    total_charge=0
    subTotalServices_no_iva=0
    for s in services:
        if s.price_override and s.price_override != 0:
            price_to_apply = s.price_override
            if s.plan.uf:
                uf = (
                    Indicators.objects.filter(kind=1, company=customer.company)
                    .distinct("kind")
                    .values("kind", "created_at", "value")
                    .first()["value"]
                )
                price_to_apply = round(s.price_override * uf * Decimal(1 + iva), 0)
        else:
            price_to_apply = s.plan.price
        serviceList.append(
            {
                "number": s.number,
                "quantity": 1,
                "code": s.plan.id,
                "description": s.plan.description_facturacion,
                "amount": float(price_to_apply),
                "price_without_iva": int(round(float(price_to_apply) / float(1 + iva), 0)),
            }
        )
        subTotalServices += float(price_to_apply)
        subTotalServices_no_iva += round(float(price_to_apply) / float(1 + iva), 2)
        montoNeto = montoNeto + round(float(price_to_apply) / float(1 + iva), 2)
        # Credits information
        credit = Credit.objects.filter(
            service_id=s.id, status_field=False, cleared_at__isnull=True, active=True
        ).annotate(year=ExtractYear("created_at"), month=ExtractMonth("created_at"),)
        if credit:
            for c in credit:
                creditList.append(
                    {
                        "quantity": c.quantity,
                        "code": c.id,
                        "description": c.reason.reason,
                        "amount": float(calculo(c)),
                    }
                )
                total_credit = total_credit + float(calculo(c))
                montoNeto = montoNeto - float(calculo(c))
        # Charges information
        charge = Charge.objects.filter(
            service_id=s.id, status_field=False, cleared_at__isnull=True
        ).annotate(year=ExtractYear("created_at"), month=ExtractMonth("created_at"),)
        if charge:
            for c in charge:
                chargeList.append(
                    {
                        "quantity": c.quantity,
                        "code": c.id,
                        "description": "#"+str(c.service.number)+" | " +str(c.reason.reason),
                        "amount": int(round(float(calculo(c)), 0)),
                    }
                )
                total_charge=total_charge+ float(calculo(c))
                montoNeto = montoNeto + float(calculo(c))
    document_type = services.first().document_type
    extra = prepare_invoice_pdf(services.first().id)

    data = {
        "fecha": datetime.now().strftime("%d/%m/%Y"),
        "operator": operator,
        "customer": {
            "code": customer.id,
            "rut": customer.rut,
            "name": customer.name,
            "address": (
                customer.street
                + customer.location
                + " Torre "
                + customer.tower
                + " Apto. "
                + customer.apartment_number
                + " Casa Nro. "
                + customer.house_number
            ),
            "composite_address": customer.address,
            "commune": services.first().best_location, #services.first().node.commune,
        },
        "service": serviceList,
        "subTotalServices": float(subTotalServices),
        "subTotalServices_no_iva": float(subTotalServices_no_iva),
        "charge": chargeList if len(chargeList) > 0 else False,
        "totalCharge": float(extra[3]),
        "credit": creditList if len(creditList) > 0 else False,
        "totalCredit": float(extra[2]),
        "total": int(subTotalServices + extra[3] - extra[2]),
        "document_type": document_type,
    }
        
    data["total_credit"] = int(round(float(total_credit), 0)) # / float(1 + iva), 0))
    data["total_antes"] = int(subTotalServices_no_iva + total_charge - data["total_credit"])
    #print(subTotalServices)
    #print(subTotalServices_no_iva)
    data["monto_neto"] = int(round(float(data["total_antes"]), 0))
    data["monto_iva"] = int(round(float(data["monto_neto"]) * (iva), 0))
    data["total"]=data["monto_neto"]+ data["monto_iva"]
    #data["monto_neto"] = int(round(montoNeto, 0))
    #data["monto_iva"] = int(round(data["total"] - data["monto_neto"], 2))
    Month = {
        1: "Enero",
        2: "Febrero",
        3: "Marzo",
        4: "Abril",
        5: "Mayo",
        6: "Junio",
        7: "Julio",
        8: "Agosto",
        9: "Septiembre",
        10: "Octubre",
        11: "Noviembre",
        12: "Diciembre",
    }
    bal = Balance.objects.get(customer=customer).balance
    balance_service = Balance.objects.get(customer=customer).balance
    if balance_service < 0:
        data["saldo"] = (-1) * balance_service
        data["total_pagar"] = int(data["total"] + (-1) * balance_service)
    else:
        data["saldo"] =  (-1) * balance_service
        if (data["total"]  - balance_service)<0:
            data["total_pagar"]=0
        else:
            data["total_pagar"] = int(data["total"]  - balance_service)
    #data["total_credit"] = int(round(float(extra[2]) / float(1 + iva), 0)) 
    data["mes"] = getattr(config, "month_invoice") #Month[s.created_at.month]
    data["year"] = getattr(config, "year_invoice") #s.created_at.year
    return data


# Order and group information to display in PDF for a service
def invoiceDataAfter(atr):
    invoice = Invoice.objects.filter(pk=atr)
    for s in invoice:
        data = {"fecha": s.created_at.strftime("%d/%m/%Y")}
        data["service"] = s.service.all().annotate(
            price=Count("id"), price_without_iva=Count("id")
        )
        iva = float(
            (
                Indicators.objects.filter(kind=2, company=s.customer.company)
                .distinct("kind")
                .values("kind", "created_at", "value")
                .first()["value"]
            )
        )
        # Credits information
        monto_neto = 0
        credits = s.credit.all()
        credit = Credit.objects.none()
        for c in credits:
            credit = credit | Credit.objects.filter(id=c.credit_applied.id)
        if credit.count() != 0:
            data["total_credit"] = 0
            data["credit"] = credit
            for c in data["credit"]:
                if s.kind==1:
                    # Save the amount without VAT for a credit
                    c.amount =  s.credit.get(credit_applied=c).amount # round( s.credit.get(credit_applied=c).amount / (1 + iva), 0)
                else:
                    c.amount =  s.credit.get(credit_applied=c).amount
                data["total_credit"] = data["total_credit"] + c.amount
            data["total_credit"] = int(round(float(data["total_credit"]), 0)) #/ (1 + iva)), 0))
            monto_neto = monto_neto - data["total_credit"]

        # Charges information
        charges = s.charge.all()
        charge = Charge.objects.none()
        for c in charges:
            charge = charge | Charge.objects.filter(id=c.charge_applied.id)
        if charge.count() != 0:
            data["charge"] = charge
            for c in data["charge"]:
                if s.kind==1:
                    # Save the amount without VAT for a credit
                    c.amount =  s.charge.get(charge_applied=c).amount
                    #c.amount =  round(s.charge.get(charge_applied=c).amount / (1 + iva), 0)
                else:
                    c.amount =  s.charge.get(charge_applied=c).amount
                # Save the amount without VAT for a credit
                #c.amount = int(round(float(s.charge.get(charge_applied=c).amount / (1 + iva)), 0))
                monto_neto = monto_neto + c.amount
        # Save operator information
        data["operator"] = OperatorInformation.objects.filter(operator_id=s.operator_id)
        if s.iva:
            data["iva"] = int(s.iva * 100)
            data["simple_iva"] = Decimal(s.iva)
        else:
            # Save VAT amount
            data["iva"] = int(iva * 100)
            data["simple_iva"] = Decimal(iva)
        # Save subtotal information
        data["extra"] = prepare_invoice_pdf_after(data)
        # Save total amount

        credit_notes = Invoice.objects.filter(parent_id=s, kind=3)
        cobro_notes = Invoice.objects.filter(parent_id=s, kind=6)

        price = 0
        for c in credit_notes:
            price = price - c.total

        for c in cobro_notes:
            price = price + c.total

        data["total"] = s.total  # + price
        data["subtotal_plan"] = s.total + data["extra"][2] - data["extra"][3]  # + price
        data["invoice"] = s
        data["without_iva"] = round(float(data["total"]) / (1 + iva), 2)
        data["only_iva"] = round(float(data["total"]) - data["without_iva"], 2)

        commune = []
        sum_bal = 0
        balances = []
        for c in data["service"]:
            if not (c.best_location in commune): #(c.node.commune in commune):
                commune.append(c.best_location)
            balances.append(BalanceService.objects.get(service=c).balance + s.total)
        data["commune"] = commune

        if min(balances) < 0:
            data["saldo"] = (-1) * min(balances)
            data["total_pagar"] = s.total + (-1) * min(balances)
        else:
            data["saldo"] =  (-1) * max(balances)
            if (s.total - max(balances))<0:
                data["total_pagar"]=0
            else:
                data["total_pagar"] = s.total - max(balances)

        # Price that is show in the pdf
        if len(data["service"]) == 1:
            if s.json:
                value = s.json[str(data["service"][0].id)]["MontoItem"]
                data["service"][0].price_without_iva = int(round(float(value), 0))
                data["service"][0].price = round(float(value) * (1 + iva), 0)
                monto_neto = monto_neto + data["service"][0].price_without_iva
            else:
                data["service"][0].price = data["subtotal_plan"]
                data["service"][0].price_without_iva = int(round(
                    float(data["subtotal_plan"]) / float(1 + iva), 0
                ))
                monto_neto = monto_neto + data["service"][0].price_without_iva
        else:
            if s.json:
                for service in data["service"]:
                    value = s.json[str(service.id)]["MontoItem"]
                    service.price_without_iva = int(round(float(value),0))
                    service.price = round(float(value) * (1 + iva), 0)
                    monto_neto = monto_neto + service.price_without_iva
            else:
                for service in data["service"]:
                    service.price = data["subtotal_plan"] / len(data["service"])
                    service.price_without_iva =int(round(
                        float(service.price) / float(1 + iva), 0
                    ))
                    monto_neto = monto_neto + service.price_without_iva

        data["monto_neto"] = monto_neto
        data["monto_iva"] = int(round(float(data["monto_neto"]) * (iva), 0))
        Month = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }
        if s.pdf_date:
            data["mes"] = Month[s.pdf_date.month]
            data["year"] = s.pdf_date.year
            data["due"] = s.due_date.date
        else:
            data["mes"] = getattr(config, "month_invoice") #Month[s.created_at.month]
            data["year"] = getattr(config, "year_invoice") #s.created_at.year
            data["due"] = s.due_date.date

        # For credit notes, debit note and payment note (cobro)
        if s.kind in [3, 4, 5, 6]:
            data["fecha2"] = s.created_at
            if s.parent_id:
                data["fecha_parent"] = s.parent_id.created_at.strftime("%d-%m-%Y")
        return data


# Order and group information to be sended to facturacion.cl by a service
def taxation(srv):
    billList = []
    
    for i in srv.values():
        serv = i["service"]
        
        folio = i["folio"][list(i["folio"])[0]]
        observation = i["observation"]
        s = Service.objects.filter(pk=serv).first()
        iva = float(
            (
                Indicators.objects.filter(kind=2, company=s.operator.company)
                .distinct("kind")
                .values("kind", "created_at", "value")
                .first()["value"]
            )
        )
        plan = Plan.objects.filter(id=s.plan.id)
        operator = OperatorInformation.objects.filter(operator_id=s.operator_id).first()
        # Boleta
        if s.document_type == 1:
            ### IdDoc ###
            idDoc = {
                "TipoDTE": 39,
                "Folio": folio,
                "FchEmis": str(datetime.now()).split(" ")[0],
                "IndServicio": 3,
                "IndMntNeto": 2,
                "FchVenc": (datetime.now() + timedelta(365 / 12)).strftime("%Y-%m-05"),
            }
            #### Emisor ####
            emisor = {
                "RUTEmisor": operator.rut,
                "RznSocEmisor": operator.name,
                "GiroEmisor": "Servicio de telecomunicaciones e instalación de redes.",
                "DirOrigen": operator.address.split(", ")[0],
                "CmnaOrigen": operator.address.split(", ")[1],
                "CiudadOrigen": operator.address.split(", ")[2],
            }
            ### Receptor ###
            receptor = {
                "RUTRecep": str(s.customer.rut).replace(".", ""),
                "CdgIntRecep": s.number,
                "RznSocRecep": (s.customer.name)[0:40],
                "Contacto": "sincorreo@sincorreo.com",
                "DirRecep": (s.customer.composite_address)[0:70],
                "CmnaRecep": s.best_location, # s.node.commune,
                "CiudadRecep": s.best_location, # s.node.commune,
            }
            if s.customer.commercial_activity:
                receptor["GiroRecep"] = str(s.customer.commercial_activity.name)[0:40]
            else:
                receptor["GiroRecep"] = "No específico"

            if s.price_override and s.price_override != 0:
                price_to_apply = s.price_override
                if s.plan.uf:
                    uf = (
                        Indicators.objects.filter(kind=1, company=s.operator.company)
                        .distinct("kind")
                        .values("kind", "created_at", "value")
                        .first()["value"]
                    )
                    price_to_apply = round(s.price_override * uf * Decimal(1 + iva), 0)
            else:
                price_to_apply = s.plan.price
            total_price_to_apply = float(price_to_apply)
            ### Detalle ###
            detalle = {
                "NroLinDet": 1,
                "CdgItem": {"TpoCodigo": "INT1", "VlrCodigo": s.plan.id},
                "NmbItem": s.plan.description_facturacion,
                "QtyItem": 1,
                "UnmdItem": "UN",
                "PrcItem": int(round(float(price_to_apply) / (1 + iva), 0)),
                "MontoItem": int(round(float(price_to_apply) / (1 + iva), 0)),
            }
            ### Cargos y Descuentos ###
            CDList = []
            a = 1
            ### Cargos ###
            charge = Charge.objects.filter(
                service_id=s.id, status_field=False, cleared_at__isnull=True
            ).annotate(
                year=ExtractYear("created_at"), month=ExtractMonth("created_at"),
            )
            totalCharge = 0
            if charge:
                total = 0
                for ch in charge:
                    cargoDescuento = {
                        "NroLinDR": a,
                        "TpoMov": "R",
                        "GlosaDR": (ch.reason.reason)[0:45],
                        "IndExeDR": 0,
                    }
                    if ch.reason.percent:
                        drValue = (total_price_to_apply / (1 + iva)) * (
                            ch.reason.valor / 100
                        )
                        cargoDescuento["ValorDR"] = int(round(drValue, 0))
                    else:
                        cargoDescuento["ValorDR"] = int(
                            round(ch.reason.valor, 0)
                        )
                    cargoDescuento["TpoValor"] = "$"
                    CDList.append(cargoDescuento)
                    a += 1
                    # Calculation
                    totalCharge = float(calculo(ch)) + float(total)
                    total = totalCharge
            ### Descuentos ###
            credit = Credit.objects.filter(
                service_id=s.id, status_field=False, cleared_at__isnull=True, active=True
            ).annotate(
                year=ExtractYear("created_at"), month=ExtractMonth("created_at"),
            )
            totalCredit = 0
            if credit:
                total = 0
                for cr in credit:
                    cargoDescuento = {
                        "NroLinDR": a,
                        "TpoMov": "D",
                        "GlosaDR": (cr.reason.reason)[0:45],
                        "IndExeDR": 0,
                    }
                    if cr.reason.percent:
                        drValue = (total_price_to_apply / (1 + iva)) * (
                            cr.reason.valor / 100
                        )
                        cargoDescuento["ValorDR"] = int(round(drValue, 0))
                    else:
                        cargoDescuento["ValorDR"] = int(
                            round(cr.reason.valor, 0)
                        )
                    cargoDescuento["TpoValor"] = "$"
                    CDList.append(cargoDescuento)
                    a += 1
                    # Calculation
                    totalCredit = float(calculo(cr)) + float(total)
                    total = totalCredit
            ### Totales ###
            if CDList:
                netAmount = (
                    float(total_price_to_apply) / (1 + iva)
                    - float(totalCredit)
                    + float(totalCharge)
                )
            else:
                netAmount = float(total_price_to_apply) / (1 + iva)
            totalValue = int(round(netAmount, 0)) + int(round(netAmount * iva, 0))
            totales = {
                "MntNeto": int(round(netAmount, 0)),
                "IVA": int(round(netAmount * iva, 0)),
                "MntTotal": totalValue,
                "SaldoAnterior": 0,
                "MontoNF": 0,
                "TotalPeriodo": totalValue,
                "SaldoAnterior": 0,
                "VlrPagar": totalValue,
            }
            # Factura
            bill = {
                "Documento": {
                    "Encabezado": {
                        "IdDoc": idDoc,
                        "Emisor": emisor,
                        "Receptor": receptor,
                        "Totales": totales,
                    },
                    "Detalle": detalle,
                }
            }
            if CDList:
                bill["Documento"]["DscRcgGlobal"] = CDList
            #print(bill)
            billList.append(bill)
        # Factura
        if s.document_type == 2:
            ### IdDoc ###
            idDoc = {
                "TipoDTE": 33,
                "Folio": folio,
                "FchEmis": str(datetime.now()).split(" ")[0],
                "FchVenc": (datetime.now() + timedelta(365 / 12)).strftime("%Y-%m-%d"),
            }
            ### Emisor ###
            emisor = {
                "RUTEmisor": operator.rut,
                "RznSoc": operator.name,
                "GiroEmis": "Servicio de telecomunicaciones e instalación de redes.",
                "DirOrigen": operator.address.split(", ")[0],
                "CmnaOrigen": operator.address.split(", ")[1],
                "CiudadOrigen": operator.address.split(", ")[2],
            }
            ### Receptor ###
            receptor = {
                "RUTRecep": str(s.customer.rut).replace(".", ""),
                "RznSocRecep": (s.customer.name)[0:40],
                "Contacto": s.customer.email,
                "DirRecep": (s.customer.composite_address)[0:70],
                "CmnaRecep": s.best_location, # s.node.commune,
                "CiudadRecep": s.best_location,  # s.node.commune,
            }
            if s.customer.commercial_activity:
                receptor["GiroRecep"] = s.customer.commercial_activity.name[0:40]
            else:
                receptor["GiroRecep"] = "No específico"
            ### Detalle ###
            if s.price_override and s.price_override != 0:
                price_to_apply = s.price_override
                if s.plan.uf:
                    uf = (
                        Indicators.objects.filter(kind=1,company=s.operator.company)
                        .distinct("kind")
                        .values("kind", "created_at", "value")
                        .first()["value"]
                    )
                    price_to_apply = round(s.price_override * uf * Decimal(1 + iva), 0)
            else:
                price_to_apply = s.plan.price
            neto = 0
            montoItem = int(round(float(price_to_apply) / (1 + iva), 0))
            detalle = {
                "NroLinDet": 1,
                "CdgItem": {"TpoCodigo": "INT1", "VlrCodigo": s.plan.id},
                "NmbItem": s.plan.description_facturacion,
                "QtyItem": 1,
                "PrcItem": montoItem,
            }
            ### Cargos ###
            charge = Charge.objects.filter(
                service_id=s.id, status_field=False, cleared_at__isnull=True
            ).annotate(
                year=ExtractYear("created_at"), month=ExtractMonth("created_at"),
            )
            if charge:
                total = 0
                for ch in charge:
                    totalCharge = float(calculo(ch)) + float(total)
                    total = totalCharge
                detalle["RecargoMonto"] = totalCharge
                montoItem += totalCharge
            ### Descuentos ###
            credit = Credit.objects.filter(
                service_id=s.id, status_field=False, cleared_at__isnull=True, active=True
            ).annotate(
                year=ExtractYear("created_at"), month=ExtractMonth("created_at"),
            )
            if credit:
                total = 0
                for cr in credit:
                    totalCredit = float(calculo(cr)) + float(total)
                    total = totalCredit
                detalle["DescuentoMonto"] = totalCredit
                montoItem -= float(totalCredit)
            detalle["MontoItem"] = montoItem
            neto += montoItem
            ### Totales ###
            totalValue = int(round(neto * iva, 0)) + int(round(neto, 0))
            totales = {
                "MntNeto": int(round(neto, 0)),
                "MntExe": 0,
                "TasaIVA": iva * 100,
                "IVA": int(round(neto * iva, 0)),
                "MntTotal": totalValue,
            }
            bill = {
                "Documento": {
                    "Encabezado": {
                        "IdDoc": idDoc,
                        "Emisor": emisor,
                        "Receptor": receptor,
                        "Totales": totales,
                    },
                    "Detalle": detalle,
                }
            }
            ### Adicional ###
            if observation:
                bill["Documento"]["Adicional"] = {"NodosA": {"A1": observation}}
            #print(bill)
            billList.append(bill)
    return billList


################# FOR BILLING AJAX ##################

def price_tag2(number):
    value = ('%.6f' % number).rstrip('0').rstrip('.')
    return intcomma(value).replace(".", "*").replace(",", ".").replace("*", ",")
    """result = intcomma(number)
    result = result.replace(".", "*").replace(",", ".").replace("*", ",")
    total = result.split(".")
    if len(total) == 2:
        if total[1] == "00":
            return total[0]
        else:
            return result
    else:
        return result"""

# INDIVIDUAL
def prepare_service(service):
    dictionary = {
        "pk": service.pk,
        "customer": service.customer.name,
        "number": service.number,
        "plan": str(service.plan.description_facturacion),
        "plan_price": price_tag2(service.plan.price),
        "operator": service.operator.name,
    }
    return dictionary

@login_required
def service_check_ajax(request, **kwargs):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "number",
            "1": "customer",
            "2": "plan",
            "4": "operator",
        }
        buttonResult = 0
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        operator = request.GET.get("operator", None)
        docType = request.GET.get("document_type", None)
        # Search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))


        # Boletas que no estan asociadas a ningun pago
        inv=Invoice.objects.filter(payment__isnull=True).values('id')
        #print(inv)
        # Filter processed services
        if getattr(config, "allow_generate_factura") == "no":
            all_services = Service.objects.filter(
                (Q(status=1)) & (Q(document_type=1))
            ).filter(invoice__id__in=inv)
            all_services=all_services.filter(invoice__id__in=inv).distinct()
        else:
            all_services = Service.objects.filter(
                (Q(status=1))
                & (Q(document_type=1) | Q(document_type=2))
            )
            #print(all_services.count())
            all_services=all_services.filter(invoice__id__in=inv).distinct()
            #print(all_services.count())

        """
        # Services on the cycle
        cycle = BillingCycle.objects.filter(id=kwargs["pk"]).values()[0]
        if cycle["processed"]:
            processedIdList = json.loads(cycle["processed"])
        else:
            processedIdList = []
        processed = ProcessedBatch.objects.filter(id__in=processedIdList)
        blackList = []
        # Exclude services in process
        for p in processed.filter(status=False):
            lBatch = list(p.processed)
            for l in lBatch:
                try:
                    for srvc in p.processed[l]["unified"]["services"]:
                        blackList.append(srvc)
                except:
                    try:
                        blackList.append(p.processed[l]["service"])
                    except:
                        pass
        # Exclude services success
        for p in processed.filter(status=True):
            lBatch = list(p.processed)
            for l in lBatch:
                try:
                    if p.processed[l]["barCode"]:
                        blackList.append(p.processed[l]["service"])
                except:
                    pass
        whiteList = allS.exclude(id__in=blackList)
        # Order
        if order_column and order_dir:
            all_services = whiteList.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_services = whiteList
        """
        cycle_operator= BillingCycle.objects.get(id=kwargs["pk"]).operator
        if cycle_operator:
            all_services = all_services.filter(operator__id=cycle_operator.id)
            
        if docType:
            all_services = all_services.filter(Q(document_type=docType))
        
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_services=all_services.filter(operator__company__id=op.company)
                else:
                    all_services=all_services.filter(operator__id=op.operator)
            except:
                pass

        all_count = all_services.count()
        if search:
            filtered_services = all_services.filter(
                Q(number=int(search) if search.isdigit() else None)
            )
        else:
            filtered_services = all_services
        filtered_services = filtered_services.distinct()
        filtered_count = filtered_services.count()
        if length == -1:
            sliced_services = filtered_services
        else:
            sliced_services = filtered_services[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_service, sliced_services)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
def service_ajax_1(request, **kwargs):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "number",
            "1": "customer",
            "2": "plan",
            "3": "plan_price",
            "4": "operator",
        }
        buttonResult = 0
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        operator = request.GET.get("operator", None)
        docType = request.GET.get("document_type", None)
        # Search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))
        # Filter processed services
        if getattr(config, "allow_generate_factura") == "no":
            allS = Service.objects.filter(
                (Q(status=1) & Q(unified_billing=False)) & (Q(document_type=1))
            )
        else:
            allS = Service.objects.filter(
                (Q(status=1) & Q(unified_billing=False))
                & (Q(document_type=1) | Q(document_type=2))
            )
        cycle_operator= BillingCycle.objects.get(id=kwargs["pk"]).operator
        # Se filtra por operador del ciclo
        if cycle_operator:
            allS = allS.filter(operator__id=cycle_operator.id)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    allS=allS.filter(operator__company__id=op.company)
                else:
                    allS=allS.filter(operator__id=op.operator)
            except:
                pass

        # Services on the cycle
        cycle = BillingCycle.objects.filter(id=kwargs["pk"]).values()[0]
        if cycle["processed"]:
            processedIdList = json.loads(cycle["processed"])
        else:
            processedIdList = []
        processed = ProcessedBatch.objects.filter(id__in=processedIdList)
        blackList = []
        # Exclude services in process
        for p in processed.filter(status=False):
            lBatch = list(p.processed)
            for l in lBatch:
                try:
                    for srvc in p.processed[l]["unified"]["services"]:
                        blackList.append(srvc)
                except:
                    try:
                        blackList.append(p.processed[l]["service"])
                    except:
                        pass
        # Exclude services success
        for p in processed.filter(status=True):
            lBatch = list(p.processed)
            for l in lBatch:
                try:
                    if p.processed[l]["barCode"]:
                        blackList.append(p.processed[l]["service"])
                except:
                    pass
        whiteList = allS.exclude(id__in=blackList)
        # Order
        if order_column and order_dir:
            all_services = whiteList.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_services = whiteList
        if cycle_operator:
            all_services = all_services.filter(Q(operator_id=cycle_operator.id))
        if docType:
            all_services = all_services.filter(Q(document_type=docType))
        all_count = all_services.count()
        if search:
            filtered_services = all_services.filter(
                Q(number=int(search) if search.isdigit() else None)
            )
        else:
            filtered_services = all_services
        filtered_services = filtered_services.distinct()
        filtered_count = filtered_services.count()
        if length == -1:
            sliced_services = filtered_services
        else:
            sliced_services = filtered_services[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_service, sliced_services)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@transaction.atomic
@login_required
def service_ajax_2(request, **kwargs):
    if request.is_ajax():
        data = request.POST
        result = []
        i = 0
        for d in data:
            if i % 2 == 0:
                last = json.loads(data[d])
            else:
                if data[d] == "false":
                    obser = False
                else:
                    obser = data[d]
                result.append({"id": last, "observation": obser})
            i += 1
        # Folios order and allocation
        order = orderForAllocation(result, kwargs["pk"])
        # Keys from order dict
        orderKeys = list(order)
        # Empty list to process data
        invoicesReady = []
        for key in orderKeys:
            # Ideal case. Without errors
            if order[key]["error"] == False:
                invoice = taxation(order[key]["completed"])
                invoicesReady += invoice
            else:
                # Insufficient folios, only some were processed
                if order[key]["error"]["code"] == 1:
                    invoice = taxation(order[key]["completed"])
                    invoicesReady += invoice
                    # Add data with error to processed batch
                    dateP = datetime.now()
                    ProcessedBatch.objects.create(
                        processed={
                            "notProcessed": order[key]["error"]["affected"],
                            "message": order[key]["error"]["message"],
                        },
                        processed_on=dateP,
                        status=True,
                    )
                    # Add to billcycle
                    processed = ProcessedBatch.objects.filter(
                        processed={
                            "notProcessed": order[key]["error"]["affected"],
                            "message": order[key]["error"]["message"],
                        },
                        processed_on=dateP,
                        status=True,
                    ).first()
                    billCycle = BillingCycle.objects.filter(id=kwargs["pk"]).first()
                    if not (billCycle.processed is None):
                        cycleProcessed = json.loads(billCycle.processed)
                    else:
                        cycleProcessed=[]
                    cycleProcessed.append(processed.id)
                    BillingCycle.objects.filter(id=kwargs["pk"]).update(
                        processed=json.dumps(cycleProcessed)
                    )

                # There is no batch of folios for this type of document
                if order[key]["error"]["code"] == 2:
                    # Add data with error to processed batch
                    dateP = datetime.now()
                    ProcessedBatch.objects.create(
                        processed={
                            "notProcessed": order[key]["error"]["affected"],
                            "message": order[key]["error"]["message"],
                        },
                        processed_on=dateP,
                        status=True,
                    )
                    # Add to billcycle
                    processed = ProcessedBatch.objects.filter(
                        processed={
                            "notProcessed": order[key]["error"]["affected"],
                            "message": order[key]["error"]["message"],
                        },
                        processed_on=dateP,
                        status=True,
                    ).first()
                    billCycle = BillingCycle.objects.filter(id=kwargs["pk"]).first()
                    if not (billCycle.processed is None):
                        cycleProcessed = json.loads(billCycle.processed)
                    else:
                        cycleProcessed=[]
                    cycleProcessed.append(processed.id)
                    BillingCycle.objects.filter(id=kwargs["pk"]).update(
                        processed=json.dumps(cycleProcessed)
                    )
        if len(invoicesReady) > 0:
            # Dict to be sended to TaxationAPI
            documentsReady = {
                "user": getattr(config, "user"),
                "rut": getattr(config, "rut"),
                "password": getattr(config, "password"),
                "puerto": getattr(config, "port"),
                "documentList": json.dumps(invoicesReady),
                "endPoint": getattr(config, "response_address"),
                "order": json.dumps(order),
                "celery": True,
            }
            print(documentsReady["documentList"])
            try:
                # Consume TaxationAPI
                go = get_invoices(documentsReady)
                buttonResult = True
            except:
                transaction.set_rollback(True)
                buttonResult = False
            result = json.dumps({"buttonResult": buttonResult,})
            return HttpResponse(result, content_type="application/json")
        else:
            return HttpResponse(
                json.dumps({"buttonResult": False,}), content_type="application/json"
            )
    else:
        raise Http404


# UNIFIED


def prepare_unified(customer):
    services = Service.objects.filter(customer_id=customer.id)
    srvList = []
    for s in services:
        srvList.append(
            {
                "number": str(s.number),
                "plan": str(s.plan.description_facturacion),
                "plan_price": price_tag2(s.plan.price),
                "operator": s.operator.name,
            }
        )
    dictionary = {
        "id": customer.id,
        "name": customer.name,
        "services": srvList,
    }
    return dictionary


@login_required
def service_unified_ajax(request, **kwargs):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "id",
            "1": "name",
            "2": "services",
        }
        buttonResult = 0
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        operator = request.GET.get("operator", None)
        # Search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))
        # Getting information
        if getattr(config, "allow_generate_factura") == "no":
            srv = Service.objects.filter(
                (Q(status=1) & Q(unified_billing=True))
                & (Q(document_type=1))
                & Q(status=1)
            )
        else:
            srv = Service.objects.filter(
                (Q(status=1) & Q(unified_billing=True))
                & (Q(document_type=1) | Q(document_type=2))
                & Q(status=1)
            )

        cycle_operator= BillingCycle.objects.get(id=kwargs["pk"]).operator
        # Se filtra por operador del ciclo
        if cycle_operator:
            srv = srv.filter(operator__id=cycle_operator.id)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    srv=srv.filter(operator__company__id=op.company)
                else:
                    srv=srv.filter(operator__id=op.operator)
            except:
                pass

        serviceOwners = (
            srv.distinct()
            .values("customer_id")
            .annotate(customer_count=Count("customer_id"))
            .filter(customer_count__gt=1)
            .order_by("customer_id")
        )
        customerId = []
        for s in serviceOwners:
            customerId.append(s["customer_id"])
        ### EXCEPTING CUSTOMERS ###
        cycle = BillingCycle.objects.filter(id=kwargs["pk"]).first()
        if cycle.processed:
            processedIdList = json.loads(cycle.processed)
        else:
            processedIdList = []
        processed = ProcessedBatch.objects.filter(id__in=processedIdList)
        toExclude = []
        # Exclude customers in process
        for p in processed.filter(status=False):
            lBatch = list(p.processed)
            for l in lBatch:
                try:
                    toExclude.append(p.processed[l]["unified"]["customer"])
                except:
                    continue
        # Exclude customers success
        for p in processed.filter(status=True):
            lBatch = list(p.processed)
            for l in lBatch:
                try:
                    if p.processed[l]["barCode"]:
                        toExclude.append(p.processed[l]["unified"]["customer"])
                except:
                    continue
        blackList = []
        for black in toExclude:
            if black not in blackList:
                blackList.append(black)

        customers = Customer.objects.filter(id__in=customerId).exclude(id__in=blackList)
        # Handle data
        if order_column and order_dir:
            all_customers = customers.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_customers = customers
        all_count = all_customers.count()
        filtered_customers = all_customers
        filtered_customers = filtered_customers.distinct()
        filtered_count = filtered_customers.count()
        if length == -1:
            sliced_customers = filtered_customers
        else:
            sliced_customers = filtered_customers[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_unified, sliced_customers)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@transaction.atomic
@login_required
def service_unified_ajax_2(request, **kwargs):
    if request.is_ajax():
        data = request.POST
        result = []
        i = 0
        for d in data:
            if i % 2 == 0:
                last = json.loads(data[d])
            else:
                if data[d] == "false":
                    obser = False
                else:
                    obser = data[d]
                result.append({"customer": last, "observation": obser})
            i += 1
        order = classification(result, kwargs["pk"])
        invoicesReady = []
        for key in list(order):
            # Ideal case. Without errors
            if order[key]["error"] == False:
                invoice = packaging(order[key])
                invoicesReady += invoice
            else:
                # Insufficient folios, only some were processed
                if order[key]["error"]["code"] == 1:
                    invoice = packaging(order[key])
                    invoicesReady += invoice
                    # Add data with error to processed batch
                    dateP = datetime.now()
                    ProcessedBatch.objects.create(
                        processed={
                            "notProcessed": order[key]["error"]["affected"],
                            "message": order[key]["error"]["message"],
                        },
                        processed_on=dateP,
                        status=True,
                    )
                    # Add to billcycle
                    processed = ProcessedBatch.objects.filter(
                        processed={
                            "notProcessed": order[key]["error"]["affected"],
                            "message": order[key]["error"]["message"],
                        },
                        processed_on=dateP,
                        status=True,
                    ).first()
                    billCycle = BillingCycle.objects.filter(id=kwargs["pk"]).first()
                    cycleProcessed = json.loads(billCycle.processed)
                    cycleProcessed.append(processed.id)
                    BillingCycle.objects.filter(id=kwargs["pk"]).update(
                        processed=json.dumps(cycleProcessed)
                    )
                # There is no batch of folios for this type of document
                if order[key]["error"]["code"] == 2:
                    # Add data with error to processed batch
                    dateP = datetime.now()
                    ProcessedBatch.objects.create(
                        processed={
                            "notProcessed": order[key]["error"]["affected"],
                            "message": order[key]["error"]["message"],
                        },
                        processed_on=dateP,
                        status=True,
                    )
                    # Add to billcycle
                    processed = ProcessedBatch.objects.filter(
                        processed={
                            "notProcessed": order[key]["error"]["affected"],
                            "message": order[key]["error"]["message"],
                        },
                        processed_on=dateP,
                        status=True,
                    ).first()
                    billCycle = BillingCycle.objects.filter(id=kwargs["pk"]).first()
                    cycleProcessed = json.loads(billCycle.processed)
                    cycleProcessed.append(processed.id)
                    BillingCycle.objects.filter(id=kwargs["pk"]).update(
                        processed=json.dumps(cycleProcessed)
                    )
        if len(invoicesReady) > 0:
            documentsReady = {
                "user": getattr(config, "user"),
                "rut": getattr(config, "rut"),
                "password": getattr(config, "password"),
                "puerto": getattr(config, "port"),
                "documentList": json.dumps(invoicesReady),
                "endPoint": getattr(config, "response_address"),
                "order": json.dumps(order),
                "unified": True,
                "celery": True,
            }
            try:
                # Consume TaxationAPI
                go = get_invoices(documentsReady)
                buttonResult = True
            except:
                transaction.set_rollback(True)
                buttonResult = False
            result = json.dumps({"buttonResult": buttonResult,})
            return HttpResponse(result, content_type="application/json")
        else:
            return HttpResponse(
                json.dumps({"buttonResult": False,}), content_type="application/json"
            )
    else:
        raise Http404


################# BILLING CYCLE AJAX ##################


def prepare_cycles(cycle):
    dictionary = {
        "id": cycle.id,
        "interval": str(cycle.start).split(" ")[0]
        + " - "
        + str(cycle.end).split(" ")[0],
        "operator":str(cycle.operator),
    }
    return dictionary


@login_required
def cycles_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "id",
            "1": "interval",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))
        if order_column and order_dir:
            all_cycles = BillingCycle.objects.all().order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_cycles = BillingCycle.objects.all()
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_cycles=all_cycles.filter(operator__company__id=op.company)
                else:
                    all_cycles=all_cycles.filter(operator__id=op.operator)
            except:
                pass
        all_count = all_cycles.count()
        if search:
            filtered_cycles = all_cycles.all()
        else:
            filtered_cycles = all_cycles
        filtered_cycles = filtered_cycles.distinct()
        filtered_count = filtered_cycles.count()
        if length == -1:
            sliced_cycles = filtered_cycles
        else:
            sliced_cycles = filtered_cycles[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_cycles, sliced_cycles)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
def cycles_ajax_2(request):
    if request.is_ajax():
        newCycle = request.POST["data"]
        s = datetime.strptime(newCycle, "%Y-%m-%d")
        e = (s + timedelta(15)).strftime("%Y-%m-19")
        print("enss")
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    print("No se crea el ciclo hasta que especifique el operador")
                    pass
                else:
                    BillingCycle.objects.create(start=s, end=e, operator=Operator.objects.get(id=op.operator))
            except Exception as e:
                print(e)
                pass

        return HttpResponse(json.dumps(True), content_type="application/json")
    else:
        raise Http404

@login_required
def calendario_cortes_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "id",
            "1": "billing_date",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))
        if order_column and order_dir:
            all_calendar = CalendarioCortes.objects.all().order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_calendar = CalendarioCortes.objects.all()

        all_calendar = all_calendar.distinct()
        all_count = all_calendar.count()
        
        if length == -1:
            sliced_calendar = all_calendar
        else:
            sliced_calendar = all_calendar[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": all_count,
                "data": list(map(prepare_calendar, sliced_calendar)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_calendar(calendar):
    dictionary = {
        "id": calendar.id,
        "billing_date": calendar.billing_date.strftime("%Y/%m/%d"),
    }
    return dictionary

def delete_calendario_cortes(request, slug):
    cc = get_object_or_404(CalendarioCortes, id=slug)
    
    if cc:
        cc.delete()
    
    return HttpResponseRedirect(reverse_lazy("calendario-cortes"))

################# INVOICES REPORT ##################


def prepareReport(invoice):
    allServices = []
    for service in invoice.service.all():
        allServices.append(service.number)

    dictionary = {
        "invoice": invoice.id,
        "kind": invoice.INVOICE_CHOICES[invoice.kind][1],
        "created": invoice.created_at.strftime("%Y/%m/%d, %H:%M:%S"),
        "customer": invoice.customer.name,
        "services": allServices,
        "total": invoice.total,
    }

    allServices = None

    print(dictionary)

    return dictionary


@login_required
def reportInvoiceAjax(request, **kwargs):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "invoice",
            "1": "kind",
            "2": "created",
            "3": "customer",
            "4": "services",
            "5": "total",
        }

        # Sort request
        sort = {"asc": "", "desc": "-"}
        sortCol = request.GET.get("order[0][column]", None)
        sortDirection = request.GET.get("order[0][dir]", None)

        # Search request
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        # cycleStart = "2020-07-20"
        # cycleEnd = "2020-08-19"
        cycle = BillingCycle.objects.filter(id=kwargs["pk"]).first()
        cycleStart = cycle.start.strftime("%Y-%m-%d")
        cycleEnd = cycle.end.strftime("%Y-%m-%d")
        print(cycleStart, cycleEnd)

        invoice = Invoice.objects.filter(
            (Q(created_at__gte=cycleStart) & Q(created_at__lte=cycleEnd))
            & Q(kind__gte=1, kind__lte=2)
        )
        if sortCol and sortDirection:
            allInvoice = invoice.order_by(sort[sortDirection] + columns[sortCol])
        else:
            allInvoice = invoice.order_by("kind", "-created_at")

        if search:
            pass

        totalRecord = allInvoice.count()

        sortedInvoice = allInvoice.distinct()
        filteredInvoice = sortedInvoice.count()

        if length == -1:
            slicedInvoice = sortedInvoice
        else:
            slicedInvoice = sortedInvoice[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": totalRecord,
                "recordsFiltered": filteredInvoice,
                "data": list(map(prepareReport, slicedInvoice)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


################# FOLIOS AJAX ##################


def prepare_folios(folio):
    invoice = Invoice.objects.all().first()
    kind = dict(invoice.INVOICE_CHOICES)[folio.kind]
    if folio.expiration:
        expiration = str(folio.expiration).split(" ")[0]
    else:
        expiration = "Sin vencimiento"
    dictionary = {
        "id": str(folio.id),
        "kind": kind,
        "created": str(folio.created_on).split(" ")[0],
        "expiration": expiration,
        "stock": str(folio.stock),
        "operator": str(folio.operator.name),
    }
    return dictionary


@login_required
def folios_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "id",
            "1": "kind",
            "2": "created",
            "3": "expiration",
            "4": "stock",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))
        if order_column and order_dir:
            all_folios = InvoiceBatch.objects.filter(
                Q(stock__gt=0)
                & (Q(expiration__gte=datetime.now()) | Q(expiration__isnull=True))
            ).order_by(orderings[order_dir] + columns[order_column])
        else:
            all_folios = InvoiceBatch.objects.filter(
                Q(stock__gt=0) & Q(expiration__gte=datetime.now())
                | Q(expiration__isnull=True)
            )

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_folios=all_folios.filter(operator__company__id=op.company)
                else:
                    all_folios=all_folios.filter(operator__id=op.operator)
            except:
                pass

        all_count = all_folios.count()
        if search:
            pass
        else:
            filtered_folios = all_folios
        filtered_folios = filtered_folios.distinct()
        filtered_count = filtered_folios.count()
        if length == -1:
            sliced_folios = filtered_folios
        else:
            sliced_folios = filtered_folios[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_folios, sliced_folios)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@permission_required("customers.add_invoicebatch")
def folios_ajax_2(request):
    if request.is_ajax():
        data = request.POST
        if data["expiration"] == "false":
            expiration = None
        else:
            expiration = datetime.strptime(data["expiration"], "%Y-%m-%d")
        createInvoiceBatch(
            int(data["first"]), int(data["last"]), int(data["type"]), expiration, int(data["operator"])
        )
        return HttpResponse(json.dumps(True), content_type="application/json")
    else:
        raise Http404


@login_required
def folio_delete(request):
    if request.is_ajax():
        response = {"message": ""}
        data = request.POST
        invoiceBatch = InvoiceBatch.objects.filter(Q(pk=data["batch_id"]))
        if invoiceBatch:
            batch = invoiceBatch[0].batch
            try:
                key = list(batch.keys())[
                    list(batch.values()).index(int(data["folio_number"]))
                ]
                batch.pop(key)
                invoiceBatch[0].batch = batch
                invoiceBatch[0].stock = len(batch)
                invoiceBatch[0].save()
            except:
                response[
                    "message"
                ] = "El folio número {} no se encuentra disponible en el lote".format(
                    data["folio_number"]
                )
        else:
            response[
                "message"
            ] = "Se encontro un error al intentar procesar su solicitud.\nPor favor intente mas tarde."

        return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        raise Http404


################# INDICATORS AJAX ##################


def prepare_indicators(attr):
    option = dict(Indicators.INDICATOR_CHOICES)
    dictionary = {
        "kind": option[attr["kind"]],
        "kind_id": attr["kind"],
        "created": str(attr["created_at"]).split(" ")[0],
        "value": float(attr["value"]),
        "operator": str(attr["company"]),
    }
    return dictionary


@login_required
def indicators_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "kind",
            "1": "created",
            "3": "value",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))
        all_indicators = (
            Indicators.objects.all()
            .distinct("kind")
            .values("kind", "created_at", "value", "company")
        )
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                all_indicators = (
                        Indicators.objects.filter(company__id=op.company)
                        .distinct("kind")
                        .values("kind", "created_at", "value", "company")
                )
            except:
                pass
        all_count = all_indicators.count()
        filtered_indicators = all_indicators
        filtered_count = filtered_indicators.count()
        if length == -1:
            sliced_indicators = filtered_indicators
        else:
            sliced_indicators = filtered_indicators[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_indicators, sliced_indicators)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def indicators_ajax_2(request):
    if request.is_ajax():
        data = request.POST
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                company=op.company    
            except:
                pass
        Indicators.objects.create(
            created_at=datetime.now(), kind=data["type"], value=data["value"], company=Company.objects.get(id=company)
        )
        return HttpResponse(json.dumps(True), content_type="application/json")
    else:
        raise Http404


################# PROCESSED AJAX ##################


@login_required
def processed_ajax(request, **kwargs):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "id",
            "1": "date",
            "2": "type",
            "3": "processed",
            "4": "status",
        }
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))
        # Prepare data
        data = []
        cycle = BillingCycle.objects.filter(id=kwargs["pk"]).values()[0]
        cycle_operator= BillingCycle.objects.get(id=kwargs["pk"]).operator
        show=False
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    if cycle_operator.company.id==op.company:
                        show=True
                else:
                    if cycle_operator.id==op.operator:
                        show=True
            except:
                pass

        cycle["processed"] = cycle["processed"] if cycle["processed"] else None
        if cycle["processed"] != None and show:
            processedList = json.loads(cycle["processed"])
            for id in reversed(processedList):
                batch = ProcessedBatch.objects.filter(id=id).first()
                try:
                    if batch.processed["message"]:
                        dictionary = {
                            "id": batch.id,
                            "date": str(batch.processed_on).split(" ")[0],
                            "processed": batch.processed,
                            "message": batch.processed["message"],
                            "type": "No procesado",
                            "status": True,
                        }
                        data.append(dictionary)
                except:
                    invoice = InvoiceBatch.objects.filter(id=batch.batch_id)
                    for folio in batch.processed:
                        try:
                            if batch.processed[folio]["unified"]["services"]:
                                a = 0
                                for s in batch.processed[folio]["unified"]["services"]:
                                    batch.processed[folio]["unified"]["services"][a] = {
                                        "number": (
                                        Service.objects.filter(id=int(s)).first().number
                                        ),
                                        "id": s}
                                
                                    a += 1
                        except:
                            serviceNumber = {
                                        "number": (Service.objects.filter(
                                    id=int(batch.processed[folio]["service"])
                                )
                                .first()
                                .number),
                                        "id": batch.processed[folio]["service"]}
                            batch.processed[folio]["service"] = serviceNumber

                    dictionary = {
                        "id": batch.id,
                        "date": str(batch.processed_on).split(" ")[0],
                        "processed": batch.processed,
                        "status": batch.status,
                    }
                    for i in invoice:
                        dictionary["type"] = dict(i.INVOICE_CHOICES)[i.kind]
                    data.append(dictionary)
        else:
            data = []
        # Handle data
        all_count = len(data)
        if length == -1:
            sliced_processed = data
        else:
            sliced_processed = data[start : start + length]

        # all_count = len(data)
        response = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": all_count,
                "data": sliced_processed,
            }
        )
        return HttpResponse(response, content_type="application/json")
    else:
        raise Http404


################# INVOICE DISPLAY ##################

# Display pre-bill by a service
def PreBill(request, *args, **kwargs):
    # Invoice information
    data = invoiceData(kwargs["pk"])

    # Generate barcode pic
    bar = barcode(
        """<TED version="1.0"><DD><RE>76442475-1</RE><TD>39</TD><F>1234</F><FE>2019-10-23</FE><RR>13499710-9</RR><RSR>JESSICA DE LAS MERCEDES GATICA GUAJARDO</RSR><MNT>17990</MNT><IT1>BA-FIBRA-GPON-200Megas</IT1><CAF version="1.0"><DA><RE>76442475-1</RE><RS>BANDA ANCHA TELECOMUNICACIONES SPA</RS><TD>39</TD><RNG><D>101</D><H>4100</H></RNG><FA>2019-07-09</FA><RSAPK><M>zc5CUekXF7YpkAkll9zGcpfZDIePduTAKJ4sgmMHZSM8hvWAWa7KwJ4vzaenA16ZgsF0FkyKGZJDDywp47H0gw==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo="SHA1withRSA">YGblfxonEwCvaQWAVBzFrJyCvre/SWJ3zIpJFQFrlIkxcEhGNizxGImwHinXwXcDpxtqqwpwwpYqbipj/9xpTQ==</FRMA></CAF><TSTED>2019-10-23T18:01:26</TSTED></DD><FRMT algoritmo="SHA1withRSA">zPPezQfFkgl1H6bjQBPCHYbBzmRHspn3AhyhsdKszxxS8zFVL07eFkHpEfAbAG0PHLBJdqvZqOnmQx5IcB/p5g==</FRMT></TED>"""
    )
    # Content to display in pre-bill
    content = {"data": data, "barcode": bar}
    return render(request, "pre_bill.html", content)


def PreBillUnified(request, *args, **kwargs):
    # Invoice information
    data = unifiedInvoiceData(kwargs["pk"])

    # Generate barcode pic
    bar = barcode(
        """<TED version="1.0"><DD><RE>76442475-1</RE><TD>39</TD><F>1234</F><FE>2019-10-23</FE><RR>13499710-9</RR><RSR>JESSICA DE LAS MERCEDES GATICA GUAJARDO</RSR><MNT>17990</MNT><IT1>BA-FIBRA-GPON-200Megas</IT1><CAF version="1.0"><DA><RE>76442475-1</RE><RS>BANDA ANCHA TELECOMUNICACIONES SPA</RS><TD>39</TD><RNG><D>101</D><H>4100</H></RNG><FA>2019-07-09</FA><RSAPK><M>zc5CUekXF7YpkAkll9zGcpfZDIePduTAKJ4sgmMHZSM8hvWAWa7KwJ4vzaenA16ZgsF0FkyKGZJDDywp47H0gw==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo="SHA1withRSA">YGblfxonEwCvaQWAVBzFrJyCvre/SWJ3zIpJFQFrlIkxcEhGNizxGImwHinXwXcDpxtqqwpwwpYqbipj/9xpTQ==</FRMA></CAF><TSTED>2019-10-23T18:01:26</TSTED></DD><FRMT algoritmo="SHA1withRSA">zPPezQfFkgl1H6bjQBPCHYbBzmRHspn3AhyhsdKszxxS8zFVL07eFkHpEfAbAG0PHLBJdqvZqOnmQx5IcB/p5g==</FRMT></TED>"""
    )
    # Content to display in pre-bill
    content = {"data": data, "barcode": bar}
    return render(request, "pre_unified_bill.html", content)


# Download bill PDF by a invoice pk
def BillPDF(request, *args, **kwargs):
    # Invoice information
    data = invoiceDataAfter(kwargs["pk"])
    # Generate barcode pic
    if data["invoice"].barcode:
        try:
            bar = barcode(str(data["invoice"].barcode))
        except:
            bar = barcode(
            """<TED version="1.0"><DD><RE>76442475-1</RE><TD>39</TD><F>1234</F><FE>2019-10-23</FE><RR>13499710-9</RR><RSR>JESSICA DE LAS MERCEDES GATICA GUAJARDO</RSR><MNT>17990</MNT><IT1>BA-FIBRA-GPON-200Megas</IT1><CAF version="1.0"><DA><RE>76442475-1</RE><RS>BANDA ANCHA TELECOMUNICACIONES SPA</RS><TD>39</TD><RNG><D>101</D><H>4100</H></RNG><FA>2019-07-09</FA><RSAPK><M>zc5CUekXF7YpkAkll9zGcpfZDIePduTAKJ4sgmMHZSM8hvWAWa7KwJ4vzaenA16ZgsF0FkyKGZJDDywp47H0gw==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo="SHA1withRSA">YGblfxonEwCvaQWAVBzFrJyCvre/SWJ3zIpJFQFrlIkxcEhGNizxGImwHinXwXcDpxtqqwpwwpYqbipj/9xpTQ==</FRMA></CAF><TSTED>2019-10-23T18:01:26</TSTED></DD><FRMT algoritmo="SHA1withRSA">zPPezQfFkgl1H6bjQBPCHYbBzmRHspn3AhyhsdKszxxS8zFVL07eFkHpEfAbAG0PHLBJdqvZqOnmQx5IcB/p5g==</FRMT></TED>"""
            )
    else:
        bar = barcode(
            """<TED version="1.0"><DD><RE>76442475-1</RE><TD>39</TD><F>1234</F><FE>2019-10-23</FE><RR>13499710-9</RR><RSR>JESSICA DE LAS MERCEDES GATICA GUAJARDO</RSR><MNT>17990</MNT><IT1>BA-FIBRA-GPON-200Megas</IT1><CAF version="1.0"><DA><RE>76442475-1</RE><RS>BANDA ANCHA TELECOMUNICACIONES SPA</RS><TD>39</TD><RNG><D>101</D><H>4100</H></RNG><FA>2019-07-09</FA><RSAPK><M>zc5CUekXF7YpkAkll9zGcpfZDIePduTAKJ4sgmMHZSM8hvWAWa7KwJ4vzaenA16ZgsF0FkyKGZJDDywp47H0gw==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo="SHA1withRSA">YGblfxonEwCvaQWAVBzFrJyCvre/SWJ3zIpJFQFrlIkxcEhGNizxGImwHinXwXcDpxtqqwpwwpYqbipj/9xpTQ==</FRMA></CAF><TSTED>2019-10-23T18:01:26</TSTED></DD><FRMT algoritmo="SHA1withRSA">zPPezQfFkgl1H6bjQBPCHYbBzmRHspn3AhyhsdKszxxS8zFVL07eFkHpEfAbAG0PHLBJdqvZqOnmQx5IcB/p5g==</FRMT></TED>"""
        )
    if data["invoice"].kind in [3, 4, 5, 6]:
        template = get_template("bill_credit.html")
    else:
        template = get_template("bill_invoice_bolpdf.html")
    kind = data["invoice"].get_kind_display().upper()
    domain = getattr(config, "matrix_server")
    # Render the themplate
    html = template.render(
        {"barcode": bar, "data": data, "domain": domain, "kind": kind, "color": False}
    )
    # PDF options
    options = {"page-size": "letter", "encoding": "UTF-8"}
    pdf = pdfkit.from_string(html, False, options)

    # Create the name of the file with the invoice data.
    filename_pdf = data["invoice"].get_kind_display().replace(" ", "") + str(
        data["invoice"].id
    )

    # Download settings
    response = HttpResponse(pdf, content_type="application/pdf")
    response["Content-Disposition"] = (
        'attachment; filename="' + str(filename_pdf) + '.pdf"'
    )
    return response


# Download pre-bill PDF by a service
def unifiedBillPDF(request, *args, **kwargs):
    # Invoice information
    data = unifiedInvoiceData(kwargs["pk"])

    # Generate barcode pic
    if data["invoice"].barcode:
        try:
            bar = barcode(str(data["invoice"].barcode))
        except:
            bar = barcode(
            """<TED version="1.0"><DD><RE>76442475-1</RE><TD>39</TD><F>1234</F><FE>2019-10-23</FE><RR>13499710-9</RR><RSR>JESSICA DE LAS MERCEDES GATICA GUAJARDO</RSR><MNT>17990</MNT><IT1>BA-FIBRA-GPON-200Megas</IT1><CAF version="1.0"><DA><RE>76442475-1</RE><RS>BANDA ANCHA TELECOMUNICACIONES SPA</RS><TD>39</TD><RNG><D>101</D><H>4100</H></RNG><FA>2019-07-09</FA><RSAPK><M>zc5CUekXF7YpkAkll9zGcpfZDIePduTAKJ4sgmMHZSM8hvWAWa7KwJ4vzaenA16ZgsF0FkyKGZJDDywp47H0gw==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo="SHA1withRSA">YGblfxonEwCvaQWAVBzFrJyCvre/SWJ3zIpJFQFrlIkxcEhGNizxGImwHinXwXcDpxtqqwpwwpYqbipj/9xpTQ==</FRMA></CAF><TSTED>2019-10-23T18:01:26</TSTED></DD><FRMT algoritmo="SHA1withRSA">zPPezQfFkgl1H6bjQBPCHYbBzmRHspn3AhyhsdKszxxS8zFVL07eFkHpEfAbAG0PHLBJdqvZqOnmQx5IcB/p5g==</FRMT></TED>"""
            )
    else:
        bar = barcode(
            """<TED version="1.0"><DD><RE>76442475-1</RE><TD>39</TD><F>1234</F><FE>2019-10-23</FE><RR>13499710-9</RR><RSR>JESSICA DE LAS MERCEDES GATICA GUAJARDO</RSR><MNT>17990</MNT><IT1>BA-FIBRA-GPON-200Megas</IT1><CAF version="1.0"><DA><RE>76442475-1</RE><RS>BANDA ANCHA TELECOMUNICACIONES SPA</RS><TD>39</TD><RNG><D>101</D><H>4100</H></RNG><FA>2019-07-09</FA><RSAPK><M>zc5CUekXF7YpkAkll9zGcpfZDIePduTAKJ4sgmMHZSM8hvWAWa7KwJ4vzaenA16ZgsF0FkyKGZJDDywp47H0gw==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo="SHA1withRSA">YGblfxonEwCvaQWAVBzFrJyCvre/SWJ3zIpJFQFrlIkxcEhGNizxGImwHinXwXcDpxtqqwpwwpYqbipj/9xpTQ==</FRMA></CAF><TSTED>2019-10-23T18:01:26</TSTED></DD><FRMT algoritmo="SHA1withRSA">zPPezQfFkgl1H6bjQBPCHYbBzmRHspn3AhyhsdKszxxS8zFVL07eFkHpEfAbAG0PHLBJdqvZqOnmQx5IcB/p5g==</FRMT></TED>"""
        )

    template = get_template("unified_bill.html")
    domain = request.get_host()
    # Render the themplate
    html = template.render(
        {"barcode": bar, "data": data, "domain": domain, "kind": kind, "color": False}
    )
    # PDF options
    options = {"page-size": "letter", "encoding": "UTF-8"}
    pdf = pdfkit.from_string(html, False, options)

    # Create the name of the file with the invoice data.
    filename_pdf = "Documento unificado"

    # Download settings
    response = HttpResponse(pdf, content_type="application/pdf")
    response["Content-Disposition"] = (
        'attachment; filename="' + str(filename_pdf) + '.pdf"'
    )
    return response


# Display bill by id of invoice
def Bill(request, *args, **kwargs):
    # Invoice information
    data = invoiceDataAfter(kwargs["pk"])

    # Generate barcode pic
    if data["invoice"].barcode:
        try:
            bar = barcode(str(data["invoice"].barcode))
        except:
            bar = barcode(
            """<TED version="1.0"><DD><RE>76442475-1</RE><TD>39</TD><F>1234</F><FE>2019-10-23</FE><RR>13499710-9</RR><RSR>JESSICA DE LAS MERCEDES GATICA GUAJARDO</RSR><MNT>17990</MNT><IT1>BA-FIBRA-GPON-200Megas</IT1><CAF version="1.0"><DA><RE>76442475-1</RE><RS>BANDA ANCHA TELECOMUNICACIONES SPA</RS><TD>39</TD><RNG><D>101</D><H>4100</H></RNG><FA>2019-07-09</FA><RSAPK><M>zc5CUekXF7YpkAkll9zGcpfZDIePduTAKJ4sgmMHZSM8hvWAWa7KwJ4vzaenA16ZgsF0FkyKGZJDDywp47H0gw==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo="SHA1withRSA">YGblfxonEwCvaQWAVBzFrJyCvre/SWJ3zIpJFQFrlIkxcEhGNizxGImwHinXwXcDpxtqqwpwwpYqbipj/9xpTQ==</FRMA></CAF><TSTED>2019-10-23T18:01:26</TSTED></DD><FRMT algoritmo="SHA1withRSA">zPPezQfFkgl1H6bjQBPCHYbBzmRHspn3AhyhsdKszxxS8zFVL07eFkHpEfAbAG0PHLBJdqvZqOnmQx5IcB/p5g==</FRMT></TED>"""
            )
    else:
        bar = barcode(
            """<TED version="1.0"><DD><RE>76442475-1</RE><TD>39</TD><F>1234</F><FE>2019-10-23</FE><RR>13499710-9</RR><RSR>JESSICA DE LAS MERCEDES GATICA GUAJARDO</RSR><MNT>17990</MNT><IT1>BA-FIBRA-GPON-200Megas</IT1><CAF version="1.0"><DA><RE>76442475-1</RE><RS>BANDA ANCHA TELECOMUNICACIONES SPA</RS><TD>39</TD><RNG><D>101</D><H>4100</H></RNG><FA>2019-07-09</FA><RSAPK><M>zc5CUekXF7YpkAkll9zGcpfZDIePduTAKJ4sgmMHZSM8hvWAWa7KwJ4vzaenA16ZgsF0FkyKGZJDDywp47H0gw==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo="SHA1withRSA">YGblfxonEwCvaQWAVBzFrJyCvre/SWJ3zIpJFQFrlIkxcEhGNizxGImwHinXwXcDpxtqqwpwwpYqbipj/9xpTQ==</FRMA></CAF><TSTED>2019-10-23T18:01:26</TSTED></DD><FRMT algoritmo="SHA1withRSA">zPPezQfFkgl1H6bjQBPCHYbBzmRHspn3AhyhsdKszxxS8zFVL07eFkHpEfAbAG0PHLBJdqvZqOnmQx5IcB/p5g==</FRMT></TED>"""
        )

    domain = getattr(config, "matrix_server")
    # Content to display in bill
    content = {"barcode": bar, "data": data, "domain": domain, "color": True}
    if data["invoice"].kind in [3, 4, 5, 6]:
        content["kind"] = data["invoice"].get_kind_display().upper()
        return render(request, "bill_credit.html", content)
    else:
        content["kind"] = data["invoice"].get_kind_display().upper()
        return render(request, "bill_invoice_bol.html", content)


################# VIEWSET ##################

# Views of services for billing
class ForBilling(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Service
    paginate_by = 8
    template_name = "billing_list.html"
    permission_required = "customers.list_billing_cycle"

    def dispatch(self, *args, **kwargs):
        self.cycle = get_object_or_404(BillingCycle, pk=self.kwargs["pk"])
        return super(ForBilling, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(ForBilling, self).get_context_data(*args, **kwargs)
        context["cycle"]=self.cycle.id
        context["operator_options"] =Operator.objects.filter(id=self.cycle.operator.id)
        context["document_options"] = Invoice.objects.first().INVOICE_CHOICES
        return context


for_billing = ForBilling.as_view()

# Views of services for billing
class ForBillingCheck(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Service
    paginate_by = 8
    template_name = "billing_list_check.html"
    permission_required = "customers.list_billing_cycle"

    def dispatch(self, *args, **kwargs):
        self.cycle = get_object_or_404(BillingCycle, pk=self.kwargs["pk"])
        return super(ForBillingCheck, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(ForBillingCheck, self).get_context_data(*args, **kwargs)
        context["operator_options"] = Operator.objects.filter(id=self.cycle.operator.id)
        context["document_options"] = Invoice.objects.first().INVOICE_CHOICES
        return context


for_billing_check = ForBillingCheck.as_view()

class Cycle(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = BillingCycle
    paginate_by = 10
    template_name = "billing_cycle.html"
    permission_required = "customers.list_billing_cycle"

    def get_context_data(self, *args, **kwargs):
        context = super(Cycle, self).get_context_data(*args, **kwargs)

        operators=Operator.objects.all()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                     context["operators"] = Operator.objects.filter(company__id=op.company)
                else:
                    context["operators"] = Operator.objects.filter(id=op.operator)
            except:
                pass
        return context


billing_cycle = Cycle.as_view()


class Folios(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = InvoiceBatch
    paginate_by = 10
    template_name = "folios.html"
    permission_required = "customers.list_invoice_batch"

    def get_context_data(self, *args, **kwargs):
        context = super(Folios, self).get_context_data(*args, **kwargs)
        invoice = Invoice.objects.all()
        for i in invoice[:1]:
            context["document_type"] = i.INVOICE_CHOICES

        operators=Operator.objects.all()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                     context["operators"] = Operator.objects.filter(company__id=op.company)
                else:
                    context["operators"] = Operator.objects.filter(id=op.operator)
            except:
                pass
        return context


folios = Folios.as_view()


class reportInvoices(LoginRequiredMixin, ListView):  # PermissionRequiredMixin
    model = Invoice
    paginate_by = 10
    template_name = "report_invoice.html"

    def get_context_data(self, *args, **kwargs):
        context = super(reportInvoices, self).get_context_data(*args, **kwargs)
        return context


report_invoices = reportInvoices.as_view()


class Processed(LoginRequiredMixin, ListView):  # PermissionRequiredMixin,
    model = ProcessedBatch
    paginate_by = 10
    template_name = "processed.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Processed, self).get_context_data(*args, **kwargs)
        return context


processed = Processed.as_view()


class IndicatorsModule(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Indicators
    paginate_by = 10
    template_name = "indicators.html"
    permission_required = "customers.list_indicators"

    def get_context_data(self, *args, **kwargs):
        context = super(IndicatorsModule, self).get_context_data(*args, **kwargs)
        context["indicators_options"] = Indicators.INDICATOR_CHOICES
        print(context)
        return context


indicators = IndicatorsModule.as_view()


class GenerateBarcode(ListView):  # LoginRequiredMixin, PermissionRequiredMixin,
    model = ProcessedBatch
    template_name = "generateBarcode.html"

    def get_context_data(self, *args, **kwargs):
        context = super(GenerateBarcode, self).get_context_data(*args, **kwargs)
        return context


generateBarcode = GenerateBarcode.as_view()

class Calendario(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = CalendarioCortes
    paginate_by = 10
    template_name = "calendario_cortes.html"
    permission_required = "customers.calendario_cortes"

    def get_context_data(self, *args, **kwargs):
        context = super(Calendario, self).get_context_data(*args, **kwargs)
        return context

calendario_cortes = Calendario.as_view()

class CalendarioCortesCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = CalendarioCortes
    form_class = CalendarioCortesCreateForm
    template_name = "customers/calendario.html"
    permission_required = "customers.add_calendario"

    def get_context_data(self, **kwargs):
        ctx = super(CalendarioCortesCreate, self).get_context_data(**kwargs)
        return ctx

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        self.object = form.save()
        return super(CalendarioCortesCreate, self).form_valid(form)
    
    def get_success_url(self, *args, **kwargs):
        return reverse("calendario-cortes")
    

calendario_cortes_create = CalendarioCortesCreate.as_view()

# API that manage responses from TaxationAPI
class ResponseViewSet(viewsets.ModelViewSet):
    model = TaxationResponse
    queryset = TaxationResponse.objects.all()
    serializer_class = ResponseSerializer


##### INVOICE TO IRIS #####
def InvoiceToIris():
    daysAgo = date.today() - timedelta(30)  # From 30 days ago to now
    invoices = Invoice.objects.annotate(barcode_len=Length("barcode")).filter(
        barcode_len__gt=300, created_at__gte=daysAgo, sended=False
    )
    info = []
    invoiceSended = []
    for invoice in invoices[:10]: # Only 10 will be sended
        invoiceSended.append(invoice.id)
        data = invoiceDataAfter(invoice.pk)
        # Generate barcode pic
        bar = barcode(str(invoice.barcode))
        if invoice.kind in [3, 4, 5, 6]:
            template = get_template("bill_credit.html")
        else:
            template = get_template("bill_invoice.html")

        kind = invoice.get_kind_display().upper()

        # Render template
        html = template.render(
            {"barcode": bar, "data": data, "kind": kind, "color": False}
        )
        # PDF options
        options = {"page-size": "letter", "encoding": "UTF-8"}

        # PDF
        pdf = pdfkit.from_string(html, False, options)
        pdf64 = base64.b64encode(pdf).decode("UTF-8")

        services = invoice.service.all()
        for service in services:
            if service.unified_billing:
                pass
            else:
                info.append(
                    {
                        "number": str(service.number),
                        "street": str(service.street),
                        "house_number": str(service.house_number),
                        "apartment_number": str(service.apartment_number),
                        "tower": str(service.tower),
                        "location": str(service.location),
                        "price_override": str(service.price_override),
                        "due_day": str(service.due_day),
                        "activated_on": str(service.activated_on),
                        "installed_on": str(service.installed_on),
                        "uninstalled_on": str(service.uninstalled_on),
                        "expired_on": str(service.expired_on),
                        "status": str(service.status),
                        "network_mismatch": str(service.network_mismatch),
                        "ssid": str(service.ssid),
                        "technology_kind": str(service.technology_kind),
                        "allow_auto_cut": str(service.allow_auto_cut),
                        "seen_connected": str(service.seen_connected),
                        "mac_onu": str(service.mac_onu),
                        "composite_address": str(service.customer.composite_address),
                        "commune": str(service.node.commune),
                        "node__code": str(service.node.code),
                        "node__address": str(service.node.address),
                        "node__street": str(service.node.street),
                        "node__house_number": str(service.node.house_number),
                        "node__commune": str(service.node.commune),
                        "node__olt_config": str(service.node.olt_config),
                        "node__is_building": str(service.node.is_building),
                        "node__is_optical_fiber": str(service.node.is_optical_fiber),
                        "node__gpon": str(service.node.gpon),
                        "node__gepon": str(service.node.gepon),
                        "node__pon_port": str(service.node.pon_port),
                        "node__box_location": str(service.node.box_location),
                        "node__splitter_location": str(service.node.splitter_location),
                        "node__expected_power": str(service.node.expected_power),
                        "node__towers": str(service.node.towers),
                        "node__apartments": str(service.node.apartments),
                        "node__phone": str(service.node.phone),
                        "plan__name": str(service.plan.description_facturacion),
                        "plan__price": str(service.plan.price),
                        "plan__category": str(service.plan.category),
                        "plan__uf": str(service.plan.uf),
                        "plan__active": str(service.plan.active),
                        "customer__rut": str(service.customer.rut),
                        "customer__name": str(service.customer.name),
                        "customer__first_name": str(service.customer.first_name),
                        "customer__last_name": str(service.customer.last_name), ###NUEVO###
                        "customer__email": str(service.customer.email),
                        "customer__street": str(service.customer.street),
                        "customer__house_number": str(service.customer.house_number),
                        "customer__apartment_number": str(
                            service.customer.apartment_number
                        ),
                        "customer__tower": str(service.customer.tower),
                        "customer__location": str(service.customer.location),
                        "customer__phone": str(service.customer.phone),
                        "customer__default_due_day": str(
                            service.customer.default_due_day
                        ),
                        "customer__composite_address": str(
                            service.customer.composite_address
                        ),
                        "customer__commune": str(service.customer.commune),
                        "files": [],
                    }
                )
    data = {"event": "Evento_Pruebaa", "info": info}
    try:
        send = requests.post(
            config.invoices_to_iris_task_url,
            json=data,
            headers={"Authorization": config.invoices_to_iris_task_token,},
            timeout=30,
        ).json()
        print(send)

        # Update sended = True for batch of invoices to iris
        Invoice.objects.filter(id__in=invoiceSended).update(sended=True)
    except:
        print("invoices not sended", invoiceSended)


# InvoiceToIris()
