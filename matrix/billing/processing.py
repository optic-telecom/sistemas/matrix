import json, os
from .services import *
from billing.serializers import *
from customers.models import *
from customers.serializers import *
from datetime import date, datetime, timedelta
from decimal import *
from django.db.models import F, Q, Count, Sum, Min
from django.db.models.functions import ExtractMonth, ExtractYear
from django.shortcuts import get_object_or_404
from infra.models import Node
from constance import config


################# LOG FUNCTION ##################


def writeTxt(text):
    logDir = str(os.getcwd()) + "/matrix/logs/"
    if not os.path.exists(logDir):
        os.makedirs(logDir)
    file = open(logDir + "processing.txt", "a")
    file.write("[" + str(datetime.now()) + "]" + "\n")
    file.write(text + "\n\n")
    file.close()


################# SINGLE SERVICE INVOICE ##################
# Function to allocate folios on invoices
def allocation(data, kind, cycle):
    cycle_operator=BillingCycle.objects.get(id=cycle).operator
    batch = InvoiceBatch.objects.filter(operator=cycle_operator).filter(
        Q(kind=kind)
        & Q(stock__gt=0)
        & (Q(expiration__gte=datetime.now()) | Q(expiration=None))
    )
    if batch:
        # The one that have less stock
        selectedBatch = batch.order_by("stock").values()
        result = {}
        for s in selectedBatch:
            toProcess = {}
            if len(data) == 0:
                break
            # Set new batch for update
            lBatch = []
            newBatch = s["batch"]
            for x in s["batch"]:
                if len(data) == 0:
                    break
                item = data.pop(0)
                toProcess[s["batch"][x]] = {
                    "idFolio": s["id"],
                    "service": item["id"],
                    "folio": {x: s["batch"][x]},
                    "observation": item["observation"],
                }
                lBatch.append(x)
            # Delete folio already allocated
            for l in lBatch:
                newBatch.pop(l)
            # Update the batch
            InvoiceBatch.objects.filter(pk=s["id"]).update(
                batch=newBatch, stock=len(newBatch)
            )
            # Add the processed data
            ProcessedBatch.objects.create(
                processed=toProcess,
                processed_on=datetime.now(),
                batch_id=s["id"],
                status=False,
            )
            # Add to the cycle
            processed = ProcessedBatch.objects.filter(
                processed=toProcess, batch_id=s["id"], status=False,
            ).values()[0]
            ###
            for t in list(toProcess):
                toProcess[t]["idProcessed"] = processed["id"]
            result.update(toProcess)
            ###
            billCycle = BillingCycle.objects.filter(id=cycle).values()[0]
            try:
                cycleProcessed = json.loads(billCycle["processed"])
                cycleProcessed.append(processed["id"])
                BillingCycle.objects.filter(id=cycle).update(
                    processed=json.dumps(cycleProcessed)
                )
            except:
                BillingCycle.objects.filter(id=cycle).update(
                    processed=json.dumps([processed["id"]])
                )
        if len(data) > 0:
            return {
                "completed": result,
                "error": {
                    "code": 1,
                    "message": "No hay suficientes folios para procesar todos los documentos.",
                    "affected": data,
                },
            }
        else:
            return {"completed": result, "error": False}
    else:
        return {
            "error": {
                "code": 2,
                "message": "No hay folios disponibles para procesar estos documentos.",
                "affected": data,
            }
        }


def orderForAllocation(data, cycle):
    boleta = []
    factura = []
    nCredito = []
    nDebito = []
    devolucion = []
    nCobro = []
    res = {}
    # Create a lists by kind with elements: [doc-type, service-id]
    for d in data:
        doc = Service.objects.filter(pk=d["id"]).values_list("document_type")[0][0]
        if doc == 1:
            boleta.append(d)
        if doc == 2:
            factura.append(d)
        if doc == 3:
            nCredito.append(d)
        if doc == 4:
            nDebito.append(d)
        if doc == 5:
            devolucion.append(d)
        if doc == 6:
            nCobro.append(d)
        if doc == 7:
            nVenta.append(d)
    if boleta:
        res["boleta"] = allocation(boleta, 1, cycle)
    if factura:
        res["factura"] = allocation(factura, 2, cycle)
    if nCredito:
        res["nCredito"] = allocation(nCredito, 3, cycle)
    if nDebito:
        res["nDebito"] = allocation(nDebito, 4, cycle)
    if devolucion:
        res["devolucion"] = allocation(devolucion, 5, cycle)
    if nCobro:
        res["nCobro"] = allocation(nCobro, 6, cycle)
    return res


# dt = [[1, 8338], [2, 10575], [1, 9426], [2, 10403], [1, 10159]]  # 1,4
# dt = [8338, 10575, 9426, 10403, 10159]
# order = orderForAllocation(dt, 8)

################# INVOICE CALCULATION ##################


def prepare_invoice_pdf(pk):
    """ Function that calculates the VAT of subtotal for credits and charges,
    and also the subtotal with it """
    services = get_object_or_404(Service, pk=pk)
    # Si el servicio esta unificado, se trae todos los servicios que estan unificados con el.
    customer=services.customer
    if services.unified_billing:
        services = Service.objects.filter(
            customer=services.customer.id, unified_billing=True
        )
    else:
        services = Service.objects.filter(id=pk)
    # Inicializa queryset vacio
    credits = Credit.objects.none()
    charges = Charge.objects.none()
    # Carga los credits y charges asociados a los servicios.
    for service in services:
        credits = credits | service.credit_set.filter(
            status_field=False, cleared_at__isnull=True
        ).annotate(
            unit_price=Count("id"),
            monto=Count("id"),
            year=ExtractYear("created_at"),
            month=ExtractMonth("created_at"),
        )
        charges = charges | service.charge_set.filter(
            status_field=False, cleared_at__isnull=True
        ).annotate(
            unit_price=Count("id"),
            monto=Count("id"),
            year=ExtractYear("created_at"),
            month=ExtractMonth("created_at"),
        )
    iva = Decimal(
        (
            Indicators.objects.filter(kind=2, company=customer.company)
            .distinct("kind")
            .values("kind", "created_at", "value")
            .first()["value"]
        )
    )
    subtotal_credit = 0
    total_ivaCredit = 0
    ivaCredit = 0
    if credits:
        for i in credits:
            subtotal = calculo(i)
            if i.service.document_type == 1:
                # Colocando el iva.
                i.unit_price = float(Decimal(subtotal))
                if i.quantity:
                    subtotal = subtotal * i.quantity
                ivaCredit = 0
                subtotal_credit += float(Decimal(subtotal)) + float(Decimal(ivaCredit))
            else:
                # Colocando el iva.
                i.unit_price = float(Decimal(subtotal)) * float((1 + iva))
                if i.quantity:
                    subtotal = subtotal * i.quantity
                total_ivaCredit = total_ivaCredit + float(Decimal(subtotal)) * float(
                    iva
                )
                ivaCredit = float(Decimal(subtotal)) * float((iva))
                ivaCredit = round(ivaCredit, 2)
                subtotal_credit += float(Decimal(subtotal)) + float(Decimal(ivaCredit))
    subtotal_charge = 0
    total_ivaCharge = 0
    ivaCharge = 0
    if charges:
        for i in charges:
            subtotal = calculo(i)
            if i.service.document_type == 1:
                # Colocando el iva.
                i.unit_price = float(Decimal(subtotal))
                if i.quantity:
                    subtotal = subtotal * i.quantity
                ivaCharge = 0
                subtotal_charge += float(Decimal(subtotal)) + float(Decimal(ivaCharge))
            else:
                # Colocando el iva.
                i.unit_price = float(Decimal(subtotal)) * float((1 + iva))
                if i.quantity:
                    subtotal = subtotal * i.quantity
                total_ivaCharge = total_ivaCharge + float(Decimal(subtotal)) * float(
                    iva
                )
                ivaCharge = float(Decimal(subtotal)) * float(iva)
                ivaCharge = round(ivaCharge, 2)
                subtotal_charge += float(Decimal(subtotal)) + float(Decimal(ivaCharge))
    # Quitando los decimales porque ellos no los utilizan.
    total_ivaCredit = round(total_ivaCredit, 0)
    subtotal_credit = round(subtotal_credit, 0)
    total_ivaCharge = round(total_ivaCharge, 0)
    subtotal_charge = round(subtotal_charge, 0)
    return (
        credits,
        charges,
        subtotal_credit,
        subtotal_charge,
        total_ivaCredit,
        total_ivaCharge,
    )


################# UNIFIED SERVICES INVOICE ##################


def granting(data, kind, cycle):
    cycle_operator=BillingCycle.objects.get(id=cycle).operator
    batch = InvoiceBatch.objects.filter(operator=cycle_operator).filter(
        Q(kind=kind)
        & Q(stock__gt=0)
        & (Q(expiration__gte=datetime.now()) | Q(expiration=None))
    )
    if batch:
        # The one that have less stock
        selectedBatch = batch.order_by("stock").values()
        result = {}
        for s in selectedBatch:
            toProcess = {}
            if len(data) == 0:
                break
            # Set new batch for update
            lBatch = []
            newBatch = s["batch"]
            for key in s["batch"]:
                if len(data) == 0:
                    break
                adding = data.pop(0)
                toProcess[s["batch"][key]] = {
                    "idFolio": s["id"],
                    "unified": {
                        "customer": adding["customer"],
                        "services": adding["services"],
                    },
                    "folio": {key: s["batch"][key]},
                    "observation": adding["observation"],
                }
                lBatch.append(key)
            # Delete folio already allocated
            for l in lBatch:
                newBatch.pop(l)
            # Update the batch
            InvoiceBatch.objects.filter(pk=s["id"]).update(
                batch=newBatch, stock=len(newBatch)
            )
            # Add the processed data
            ProcessedBatch.objects.create(
                processed=toProcess,
                processed_on=datetime.now(),
                batch_id=s["id"],
                status=False,
            )
            # Add to the cycle
            processed = ProcessedBatch.objects.filter(
                processed=toProcess, batch_id=s["id"], status=False,
            ).values()[0]
            ###
            for t in list(toProcess):
                toProcess[t]["idProcessed"] = processed["id"]
            result.update(toProcess)
            ###
            billCycle = BillingCycle.objects.filter(id=cycle).values()[0]
            try:
                cycleProcessed = json.loads(billCycle["processed"])
                cycleProcessed.append(processed["id"])
                BillingCycle.objects.filter(id=cycle).update(
                    processed=json.dumps(cycleProcessed)
                )
            except:
                BillingCycle.objects.filter(id=cycle).update(
                    processed=json.dumps([processed["id"]])
                )
        if data != []:
            return {
                "completed": result,
                "error": {
                    "code": 1,
                    "message": "No hay suficientes folios para procesar todos los documentos.",
                    "affected": data,
                },
            }
        else:
            return {"completed": result, "error": False}
    else:
        return {
            "error": {
                "code": 2,
                "message": "No hay folios disponibles para procesar estos documentos.",
                "affected": data,
            }
        }


def classification(data, cycle):
    customersList = []
    for cust in data:
        customersList.append(cust["customer"])
    print(customersList)
    boleta = []
    factura = []
    nCredito = []
    nDebito = []
    devolucion = []
    nCobro = []
    res = {}
    # customers = Customer.objects.filter(id__in=data)
    services = Service.objects.filter(
        Q(customer_id__in=customersList)
        & (Q(status=1) & Q(unified_billing=True))
        & (Q(document_type=1) | Q(document_type=2))
        & Q(status=1)
    )
    # Clasification by document type
    for d in data:
        srv = services.filter(customer_id=d["customer"])
        lSrv = []
        for s in srv:
            lSrv.append(s.id)
        content = {
            "customer": d["customer"],
            "services": lSrv,
            "observation": d["observation"],
        }
        doc = srv.first().document_type
        if doc == 1:
            boleta.append(content)
        if doc == 2:
            factura.append(content)
        if doc == 3:
            nCredito.append(content)
        if doc == 4:
            nDebito.append(content)
        if doc == 5:
            devolucion.append(content)
        if doc == 6:
            nCobro.append(content)
        if doc == 7:
            nVenta.append(content)
    # Add folios to documents
    if boleta:
        res["boleta"] = granting(boleta, 1, cycle)
    if factura:
        res["factura"] = granting(factura, 2, cycle)
    if nCredito:
        res["nCredito"] = granting(nCredito, 3, cycle)
    if nDebito:
        res["nDebito"] = granting(nDebito, 4, cycle)
    if devolucion:
        res["devolucion"] = granting(devolucion, 5, cycle)
    if nCobro:
        res["nCobro"] = granting(nCobro, 6, cycle)
    return res


def packaging(data):
    # Iteration for documents type
    billList = []
    document = data["completed"]
    # Iteration to process every single document from type
    for folio in list(document):
        totalCredit = 0
        totalCharge = 0
        doc = document[folio]
        kind = InvoiceBatch.objects.filter(id=doc["idFolio"]).first().kind
        services = Service.objects.filter(id__in=document[folio]["unified"]["services"])
        plan = Plan.objects.filter(id__in=services.values_list("plan"))
        customer = Customer.objects.filter(
            id=document[folio]["unified"]["customer"]
        ).first()
        operator = OperatorInformation.objects.filter(
            operator_id=services.first().operator_id
        ).first()
        iva = float(
            (
                Indicators.objects.filter(kind=2, company=customer.company)
                .distinct("kind")
                .values("kind", "created_at", "value")
                .first()["value"]
            )
        )
        # Boleta
        if kind == 1:
            ### IdDoc ###
            idDoc = {
                "TipoDTE": 39,
                "Folio": folio,
                "FchEmis": str(datetime.now()).split(" ")[0],
                "IndServicio": 3,
                "IndMntNeto": 2,
                "FchVenc": (datetime.now() + timedelta(365 / 12)).strftime("%Y-%m-05"),
            }
            ### Emisor ###
            emisor = {
                "RUTEmisor": operator.rut,
                "RznSocEmisor": operator.name,
                "GiroEmisor": "Servicio de telecomunicaciones e instalación de redes.",
            }
            try:
                emisor.update(
                    {
                        "DirOrigen": data["operator"].address.split(", ")[0],
                        "CmnaOrigen": data["operator"].address.split(", ")[1],
                        "CiudadOrigen": data["operator"].address.split(", ")[2],
                    }
                )
            except:
                emisor.update(
                    {
                        "DirOrigen": "Ernesto Pinto Lagarrigue DPTO 156 H",
                        "CmnaOrigen": "Recoleta",
                        "CiudadOrigen": "Santiago",
                    }
                )
            ### Receptor ###
            if services.count()>1:
                receptor = {
                    "RUTRecep": str(customer.rut).replace(".", ""),
                    "CdgIntRecep": services.first().number,
                    "RznSocRecep": (customer.name)[0:40],
                    "Contacto": customer.email[0:200],
                    "DirRecep": (customer.composite_address)[0:70],
                    "CmnaRecep": services.first().best_location, # services.first().node.commune,
                    "CiudadRecep": services.first().best_location,  # services.first().node.commune,
                }
            else:
                receptor = {
                    "RUTRecep": str(customer.rut).replace(".", ""),
                    "CdgIntRecep": services.first().number,
                    "RznSocRecep": (customer.name)[0:40],
                    "Contacto": "sincorreo@sincorreo.com",
                    "DirRecep": (customer.composite_address)[0:70],
                    "CmnaRecep":  services.first().best_location, # services.first().node.commune,
                    "CiudadRecep": services.first().best_location, #  services.first().node.commune,
                }
            ### Detalle ###
            detailList = []
            a = 1
            total_price_to_apply = 0
            for s in services:
                if s.price_override and s.price_override != 0:
                    price_to_apply = s.price_override
                    if s.plan.uf:
                        uf = (
                            Indicators.objects.filter(kind=1, company=s.operator.company)
                            .distinct("kind")
                            .values("kind", "created_at", "value")
                            .first()["value"]
                        )
                        price_to_apply = round(
                            s.price_override * uf * Decimal(1 + iva), 0
                        )
                else:
                    price_to_apply = s.plan.price
                total_price_to_apply = float(total_price_to_apply) + float(
                    price_to_apply
                )
                detalle = {
                    "NroLinDet": a,
                    "CdgItem": {"TpoCodigo": "INT1", "VlrCodigo": s.plan.id},
                    "NmbItem": s.plan.description_facturacion,
                    "QtyItem": 1,
                    "UnmdItem": "UN",
                    "PrcItem": int(round(float(price_to_apply) / (1 + iva), 0)),
                    "MontoItem": int(round(float(price_to_apply) / (1 + iva), 0)),
                }
                detailList.append(detalle)
                a += 1
            ### Cargos y Descuentos ###
            CDList = []
            a = 1
            for s in services:
                if s.price_override and s.price_override != 0:
                    price_to_applys = float(s.price_override)
                    if s.plan.uf:
                        uf = (
                            Indicators.objects.filter(kind=1, company=s.operator.company)
                            .distinct("kind")
                            .values("kind", "created_at", "value")
                            .first()["value"]
                        )
                        price_to_applys = float(round(
                            s.price_override * uf * Decimal(1 + iva), 0
                        ))
                else:
                    price_to_applys = float(s.plan.price)

                ### Cargos sin aplicar ###
                charge = Charge.objects.filter(
                    service_id=s.id, status_field=False, cleared_at__isnull=True
                ).annotate(
                    unit_price=Count("id"),
                    monto=Count("id"),
                    year=ExtractYear("created_at"),
                    month=ExtractMonth("created_at"),
                )
                if charge:
                    total = 0
                    for ch in charge:
                        cargoDescuento = {
                            "NroLinDR": a,
                            "TpoMov": "R",
                            "GlosaDR": (ch.reason.reason)[0:45],
                            "IndExeDR": 0,
                        }
                        if ch.reason.percent:
                            drValue = (price_to_applys / (1 + iva)) * (
                                ch.reason.valor / 100
                            )
                            cargoDescuento["ValorDR"] = int(round(drValue, 0))
                        else:
                            cargoDescuento["ValorDR"] = int(
                                round(ch.reason.valor, 0)
                            )
                        cargoDescuento["TpoValor"] = "$"
                        CDList.append(cargoDescuento)
                        a += 1
                        # Calculation
                        totalCharge = float(calculo(ch)) + float(total)
                        # print(totalCharge)
                        total = totalCharge
                ### Descuentos sin aplicar ###
                credit = Credit.objects.filter(
                    service_id=s.id, status_field=False, cleared_at__isnull=True, active=True
                ).annotate(
                    unit_price=Count("id"),
                    monto=Count("id"),
                    year=ExtractYear("created_at"),
                    month=ExtractMonth("created_at"),
                )
                if credit:
                    total = 0
                    for cr in credit:
                        cargoDescuento = {
                            "NroLinDR": a,
                            "TpoMov": "D",
                            "GlosaDR": (cr.reason.reason)[0:45],
                            "IndExeDR": 0,
                        }
                        if cr.reason.percent:
                            # print(calculo(cr))
                            cdrValue = (price_to_applys / (1 + iva)) * (
                                cr.reason.valor / 100
                            )
                            cargoDescuento["ValorDR"] = int(round(cdrValue, 0))
                        else:
                            cargoDescuento["ValorDR"] = int(
                                round(cr.reason.valor, 0)
                            )
                        # print(cargoDescuento)
                        cargoDescuento["TpoValor"] = "$"
                        CDList.append(cargoDescuento)
                        a += 1
                        # Calculation
                        totalCredit = float(calculo(cr)) + float(total)
                        # print(totalCredit)
                        total = totalCredit
            ### Totales ###
            if CDList:
                netAmount = (
                    (float(total_price_to_apply) / (1 + iva))
                    - float(totalCredit)
                    + float(totalCharge)
                )
                totalValue = int(round(netAmount * iva, 0)) + int(round(netAmount, 0))
                totales = {
                    "MntNeto": int(round(netAmount, 0)),
                    "IVA": int(round(netAmount * iva, 0)),
                    "MntTotal": totalValue,
                    "SaldoAnterior": 0,
                    "MontoNF": 0,
                    "TotalPeriodo": totalValue,
                    "SaldoAnterior": 0,
                    "VlrPagar": totalValue,
                }
            else:
                netAmount = float(total_price_to_apply) / (1 + iva)
                totalValue = int(round(netAmount * iva, 0)) + int(round(netAmount, 0))
                totales = {
                    "MntNeto": int(round(netAmount, 0)),
                    "IVA": int(round(netAmount * iva, 0)),
                    "MntTotal": totalValue,
                    "SaldoAnterior": 0,
                    "MontoNF": 0,
                    "TotalPeriodo": totalValue,
                    "SaldoAnterior": 0,
                    "VlrPagar": totalValue,
                }
            bill = {
                "Documento": {
                    "Encabezado": {
                        "IdDoc": idDoc,
                        "Emisor": emisor,
                        "Receptor": receptor,
                        "Totales": totales,
                    },
                    "Detalle": detailList,
                }
            }
            if CDList:
                bill["Documento"]["DscRcgGlobal"] = CDList
            #print(bill)
            billList.append(bill)
        # Factura
        if kind == 2:
            ### IdDoc ###
            idDoc = {
                "TipoDTE": 33,
                "Folio": folio,
                "FchEmis": str(datetime.now()).split(" ")[0],
                "FchVenc": (datetime.now() + timedelta(365 / 12)).strftime("%Y-%m-%d"),
            }
            ### Emisor ###
            emisor = {
                "RUTEmisor": operator.rut,
                "RznSoc": operator.name,
                "GiroEmis": "Servicio de telecomunicaciones e instalación de redes.",
            }
            try:
                emisor.update(
                    {
                        "DirOrigen": data["operator"].address.split(", ")[0],
                        "CmnaOrigen": data["operator"].address.split(", ")[1],
                        "CiudadOrigen": data["operator"].address.split(", ")[2],
                    }
                )
            except:
                emisor.update(
                    {
                        "DirOrigen": "Ernesto Pinto Lagarrigue DPTO 156 H",
                        "CmnaOrigen": "Recoleta",
                        "CiudadOrigen": "Santiago",
                    }
                )
            ### Receptor ###
            receptor = {
                "RUTRecep": str(customer.rut).replace(".", ""),
                "RznSocRecep": (customer.name)[0:40],
                "Contacto": customer.email[0:200],
                "DirRecep": (customer.composite_address)[0:70],
                "CmnaRecep":  services.first().best_location, # services.first().node.commune,
                "CiudadRecep":  services.first().best_location, # services.first().node.commune,
            }
            if customer.commercial_activity:
                receptor["GiroRecep"] = str(customer.commercial_activity.name)[0:40]
            else:
                receptor["GiroRecep"] = "No específico"
            ### Detalle ###
            detailList = []
            a = 1
            neto = 0
            for s in services:
                if s.price_override and s.price_override != 0:
                    price_to_apply = s.price_override
                    if s.plan.uf:
                        uf = (
                            Indicators.objects.filter(kind=1, company=s.operator.company)
                            .distinct("kind")
                            .values("kind", "created_at", "value")
                            .first()["value"]
                        )
                        price_to_apply = round(
                            s.price_override * uf * Decimal(1 + iva), 0
                        )
                else:
                    price_to_apply = s.plan.price
                montoItem = int(round(float(price_to_apply) / (1 + iva), 0))
                detalle = {
                    "NroLinDet": a,
                    "CdgItem": {"TpoCodigo": "INT1", "VlrCodigo": s.plan.id},
                    "NmbItem": s.plan.description_facturacion,
                    "QtyItem": 1,
                    "PrcItem": montoItem,
                }
                ### Cargos sin aplicar ###
                charge = Charge.objects.filter(
                    service_id=s.id, status_field=False, cleared_at__isnull=True
                ).annotate(
                    unit_price=Count("id"),
                    monto=Count("id"),
                    year=ExtractYear("created_at"),
                    month=ExtractMonth("created_at"),
                )
                if charge:
                    total = 0
                    for ch in charge:
                        totalCharge = float(calculo(ch)) + float(total)
                        total = totalCharge
                    detalle["RecargoMonto"] = totalCharge
                    montoItem += totalCharge
                ### Descuentos sin aplicar ###
                credit = Credit.objects.filter(
                    service_id=s.id, status_field=False, cleared_at__isnull=True, active=True
                ).annotate(
                    unit_price=Count("id"),
                    monto=Count("id"),
                    year=ExtractYear("created_at"),
                    month=ExtractMonth("created_at"),
                )
                if credit:
                    total = 0
                    for cr in credit:
                        totalCredit = float(calculo(cr)) + float(total)
                        total = totalCredit
                    detalle["DescuentoMonto"] = totalCredit
                    montoItem -= totalCredit
                detalle["MontoItem"] = montoItem
                detailList.append(detalle)
                a += 1
                neto += montoItem
            ### Totales ###
            totales = {
                "MntNeto": int(round(neto, 0)),
                "MntExe": 0,
                "TasaIVA": int(iva * 100),
                "IVA": int(round(neto * iva, 0)),
                "MntTotal": int(round(neto * (1 + iva), 0)),
            }
            bill = {
                "Documento": {
                    "Encabezado": {
                        "IdDoc": idDoc,
                        "Emisor": emisor,
                        "Receptor": receptor,
                        "Totales": totales,
                    },
                    "Detalle": detailList,
                }
            }
            #print(bill)
            billList.append(bill)
    return billList


def creditNote(data):
    iva = float(data["iva"])
    ### IdDoc ###
    idDoc = {
        "TipoDTE": 61,
        "Folio": data["folio"],
        "FchEmis": str(datetime.now()).split(" ")[0],
    }
    ### Emisor ###
    emisor = {
        "RUTEmisor": data["operator"].rut,
        "RznSoc": data["operator"].name,
        "GiroEmis": "Servicio de telecomunicaciones e instalación de redes.",
    }
    try:
        emisor.update(
            {
                "DirOrigen": data["operator"].address.split(", ")[0],
                "CmnaOrigen": data["operator"].address.split(", ")[1],
                "CiudadOrigen": data["operator"].address.split(", ")[2],
            }
        )
    except:
        emisor.update(
            {
                "DirOrigen": "Ernesto Pinto Lagarrigue DPTO 156 H",
                "CmnaOrigen": "Recoleta",
                "CiudadOrigen": "Santiago",
            }
        )
    
    ### Receptor ###
    receptor = {
        "RUTRecep": str(data["customer"].rut).replace(".", ""),
        "RznSocRecep": data["customer"].name,
        "Contacto": data["customer"].email,
        "DirRecep": (data["customer"].composite_address)[0:70],
        "CmnaRecep": data["service"].first().best_location, # data["service"].first().node.commune,
        "CiudadRecep": data["service"].first().best_location,  # data["service"].first().node.commune,
    }
    if data["customer"].commercial_activity:
        receptor["GiroRecep"] = str(data["customer"].commercial_activity.name)[0:40]
    else:
        receptor["GiroRecep"] = "No específico"
    ### Referencia ###
    referencia = {
        "NroLinRef": 1,
        "TpoDocRef": data["ref"]["TpoDocRef"],
        "FolioRef": data["ref"]["FolioRef"],
        "FchRef": data["ref"]["FchRef"],
        "CodRef": data["ref"]["CodRef"],
    }
    detailList = []
    ### ANULACIÓN ###
    if data["ref"]["CodRef"] == 1:
        referencia["RazonRef"] = "Anula Documento de Referencia"
        ### Detalle ###
        MntNeto = 0
        a = 1
        for s in data["service"]:
            MntNeto += round(float(s.plan.price) / (1 + iva), 2)
            detalle = {
                "NroLinDet": a,
                "CdgItem": {"TpoCodigo": "INT1", "VlrCodigo": s.plan.id},
                "NmbItem": s.plan.description_facturacion,
                "QtyItem": 1,
                "UnmdItem": "UN",
                "PrcItem": int(round(float(s.plan.price) / (1 + iva), 0)),
                "MontoItem": int(round(float(s.plan.price) / (1 + iva), 0)),
            }
            detailList.append(detalle)
            a += 1
        ### Totales ###
        mntIva = round(MntNeto * iva, 2)
        MntTotal = int(round(MntNeto + mntIva, 0))
        totales = {
            "MntNeto": MntNeto,
            "TasaIVA": iva * 100,
            "IVA": mntIva,
            "MntTotal": MntTotal,
        }
    ### CORRIGE TEXTO ###
    if data["ref"]["CodRef"] == 2:
        referencia["RazonRef"] = "Corrección de campos"
        ### Detalle ###
        a = 1
        for c in data["textCorrection"]:
            detalle = {
                "NroLinDet": a,
                "CdgItem": {"TpoCodigo": "INT1", "VlrCodigo": "CCV"},
                "NmbItem": "Donde dice "
                + c["previousDescription"]
                + " debe decir "
                + c["newDescription"],
                "QtyItem": 1,
                "UnmdItem": "UN",
                "PrcItem": 0,
                "MontoItem": 0,
            }
            detailList.append(detalle)
            a += 1
        ### Totales ###
        totales = {
            "MntNeto": 0,
            "TasaIVA": 0,
            "IVA": 0,
            "MntTotal": 0,
        }
    ### CORRIGE MONTO ###
    if data["ref"]["CodRef"] == 3:
        referencia["RazonRef"] = "Corrige Montos en Documento de Referencia"
        ### Detalle ###
        MntNeto = 0
        a = 1
        for c in data["amountCorrection"]:
            precio = round(float(c["PrcItem"]), 2)
            qty = c["QtyItem"]
            MontoItem = int(round(precio * qty, 0))
            MntNeto += MontoItem
            detalle = {
                "NroLinDet": a,
                "CdgItem": {"TpoCodigo": "INT1", "VlrCodigo": c["VlrCodigo"]},
                "NmbItem": c["NmbItem"],
                "QtyItem": qty,
                "UnmdItem": "UN",
                "PrcItem": precio,
                "MontoItem": MontoItem,
            }
            detailList.append(detalle)
            a += 1
        ### Totales ###
        mntIva = round(MntNeto * iva, 2)
        MntTotal = int(round(MntNeto + mntIva, 0))
        totales = {
            "MntNeto": MntNeto,
            "TasaIVA": iva * 100,
            "IVA": mntIva,
            "MntTotal": MntTotal,
        }
    ### INVOICE OBJECT ###
    documentsReady = {
        "user": getattr(config, "user"),
        "rut": getattr(config, "rut"),
        "password": getattr(config, "password"),
        "puerto": getattr(config, "port"),
        "documentList": json.dumps(
            [
                {
                    "Documento": {
                        "Encabezado": {
                            "IdDoc": idDoc,
                            "Emisor": emisor,
                            "Receptor": receptor,
                            "Totales": totales,
                        },
                        "Detalle": detailList,
                        "Referencia": referencia,
                    }
                }
            ]
        ),
        "celery": False,
        "endPoint": json.dumps(False),
        "order": json.dumps(False),
    }
    # Consume TaxationAPI
    writeTxt(str(documentsReady))
    # print(documentsReady)
    go = get_invoices(documentsReady)
    return go


# Función para obtener el código del invoice cuando esté disponible.
def getBarCode(data):
    service_id = (
        data.get("service")
        if data.get("service")
        else data.get("unified")["services"][0]
    )
    service = Service.objects.filter(id=service_id).first()
    kind = service.document_type
    TipoDTE = 39 if kind == 1 else 33
    return "ticket@" + str(TipoDTE) + "@" + str(list(data["folio"].values())[0])


def code64(data):
    return base64.b64encode(bytes(data, "UTF-8"))


def tbarcode():
    for documento in documentoUnificado:
        # print = getBarCode(documentoUnificado.get(documento))
        element = documentoUnificado.get(documento)
        data = {
            "parameters": {
                "login": {
                    "user": code64(getattr(config, "user")),
                    "rut": code64(getattr(config, "rut")),
                    "password": code64(getattr(config, "password")),
                    "puerto": code64(getattr(config, "port")),
                },
                "ticket": code64(getBarCode(element)),
            },
            "links": {"sendTo": "", "answerMe": "",},
        }
        print(data)


def invoiceNulling(data):
    batch = ProcessedBatch.objects.filter(status=True).order_by("-pk")
    for b in batch:
        if str(data) in b.processed and "barCode" in b.processed[str(data)]:
            dictionary = b.processed
            dictionary[str(data)].pop("barCode")
            dictionary[str(data)][
                "nulled"
            ] = "El documento se ha anulado mediante una nota de crédito."
            ProcessedBatch.objects.filter(pk=b.id).update(processed=dictionary)
            success = True
            break
