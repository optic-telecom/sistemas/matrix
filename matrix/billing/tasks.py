import requests, json, base64, pdfkit
from tasks.celery import celery_app
from customers.models import Invoice
from celery.schedules import crontab
from datetime import date, datetime, timedelta
from django.template.loader import get_template
from .views import barcode, invoiceDataAfter
from django.db.models.functions import Length
from constance import config
from django.utils import formats, timezone
from customers.models import Service, PlanOrder
from django.template import Context
from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string


@celery_app.task(name="invoice_to_iris", queue="queue1")
def InvoiceToIris():
    print("Iniciando tarea: Enviar documentos a IRIS.")
    daysAgo = date.today() - timedelta(30)  # From 30 days ago to now # Registrar
    # print(daysAgo) 55
    invoices = Invoice.objects.annotate(barcode_len=Length("barcode")).filter(
        barcode_len__gt=300, created_at__gte=daysAgo, sended=False, kind=1,
        customer__send_email=True
    )
    info = []
    invoiceSended = []
    print ('Length invoices',invoices.count())
    for invoice in invoices[:1]:  # Only 10 will be sended
        if invoice.customer.send_email == True:
            invoiceSended.append(invoice.id)
            data = invoiceDataAfter(invoice.pk)
            # Generate barcode pic
            bar = barcode(str(invoice.barcode))

            if invoice.kind in [3, 4, 5, 6]:
                template = get_template("bill_credit.html")
            else:
                template = get_template("bill_invoice_bolpdf.html")

            kind = data["invoice"].get_kind_display().upper()
            domain = getattr(config, "matrix_server") #.replace("https://", "").replace("http://", "")

            # Render the template
            html = template.render(
                {"barcode": bar, "data": data, "domain": domain, "kind": kind, "color": False}
            )
            # PDF options
            options = {"page-size": "letter", "encoding": "UTF-8"}

            # PDF
            pdf = pdfkit.from_string(html, False, options)
            pdf64 = base64.b64encode(pdf).decode("UTF-8")

            services = invoice.service.all()
            for service in services:
                if service.unified_billing:
                    # Update sended to True for invoices which where sended to iris
                    # Se coloca como enviado porque sino siempre va a tomar la misma boleta.
                    Invoice.objects.filter(id__in=invoiceSended).update(sended=True)
                    pass
                else:
                    url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_url").format(NUMBER=int(service.number))
                    token_sentinel = getattr(config, "sentinel_token")
                    headers = {'Authorization':token_sentinel}
                    node=None
                    network_mismatch=""
                    try:
                        r = requests.get(url,headers=headers,verify=False, timeout=30).json()
                        node=r["node"]
                        network_mismatch=r["network_mismatch"]
                    except Exception as e:
                        print(str(e))

                    if node is None:
                        node={
                            'alias': '',
                            'street': '',
                            'house_number': '',
                            'commune':'',
                            'expected_power':'',
                            'towers': '',
                            'apartments':'',
                            'phone':'',
                        }

                    # To get the network info
                    url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_network_url").format(NUMBER=int(service.number))
                    try:
                        r = requests.get(url,headers=headers,verify=False, timeout=30)
                        #print(r)
                        network = r.json()
                    except Exception as e:
                        print(str(e))
                        network={
                            "mac_eth1":"",
                            "kind_technology":""
                        }

                    info.append(
                        {
                            "number": str(service.number),
                            "street": str(service.street),
                            "house_number": str(service.house_number),
                            "apartment_number": str(service.apartment_number),
                            "tower": str(service.tower),
                            "location": str(service.location),
                            "price_override": str(service.price_override),
                            "due_day": str(service.due_day),
                            "activated_on": str(service.activated_on) if service.activated_on else "",
                            "installed_on": str(service.installed_on)  if service.installed_on else "",
                            "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                            "expired_on": str(service.expired_on)  if service.expired_on else "",
                            "status": str(service.get_status_display()),
                            "network_mismatch": "Sí" if network_mismatch else "No",
                            "ssid": "", # str(service.ssid), #####
                            "technology_kind":  str(network["kind_technology"]) if "kind_technology" in network else "", 
                            "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                            "seen_connected": str(service.seen_connected),
                            "mac_onu": str(network["mac_eth1"]) if "mac_eth1" in network else "", #str(service.mac_onu),
                            "composite_address": str(service.customer.composite_address),
                            "commune": str(node["commune"]),
                            "node__code": str(node["alias"]),
                            "node__commune": str(node["commune"]),
                            "node__towers":  str(node["towers"]) ,
                            "node__phone": str(node["phone"]),
                            "plan__name": str(service.plan.description_facturacion),
                            "plan__price": str(service.plan.price),
                            "plan__category": str(service.plan.category),
                            "plan__uf":  "Sí" if service.plan.uf else "No",
                            "plan__active": "Sí" if service.plan.active else "No", 
                            "customer__rut": str(service.customer.rut),
                            "customer__name": str(service.customer.name),
                            "customer__first_name": str(service.customer.first_name),
                            "customer__last_name": str(service.customer.last_name), ###NUEVO###
                            "customer__email": str(service.customer.email),
                            "customer__street": str(service.customer.street),
                            "customer__house_number": str(service.customer.house_number),
                            "customer__apartment_number": str(
                                service.customer.apartment_number
                            ),
                            "customer__tower": str(service.customer.tower),
                            "customer__location": str(service.customer.location),
                            "customer__phone": str(service.customer.phone),
                            "customer__default_due_day": str(
                                service.customer.default_due_day
                            ),
                            "customer__composite_address": str(
                                service.customer.composite_address
                            ),
                            "customer__commune": str(service.customer.commune),
                            "files": [pdf64],
                            "invoice__kind":str(kind),
                            "invoice__total_pagar":data["total_pagar"],
                            "customer__id": str(service.customer.id),
                        }
                    )
                #print ('info',info)


    data = {"event":  getattr(config, "evento_send_invoice"), "info": info}
    url = getattr(config, "iris_server") + getattr(config, "invoices_to_iris_task_url")
    print (url)
    # print(data)

    if invoices.count()!=0:
        try:
            send = requests.post(
                url=url,
                json=data,
                headers={"Authorization": getattr(config, "invoices_to_iris_task_token")},
                timeout=60,
            )
            print(send)
            print(send.text)
            if "Error" in send.json() or "error" in send.json():
                print("ERROR")
            else:
                # Update sended to True for invoices which where sended to iris
                Invoice.objects.filter(id__in=invoiceSended).update(sended=True)
        except Exception as e:
            print("e",e)
            print("invoices not sended", invoiceSended)

@celery_app.task(name="end_onboarding_email", queue="queue1")
def end_onboarding_email():

    services = Service.objects.all()
    for service in services:
        if service.activated_on:
            activated_on = service.activated_on
            for day in range(91,98):
                onboard_time = timezone.now() - timedelta(days=day)
                cen = 0
                if activated_on.day == onboard_time.day and activated_on.month == onboard_time.month and activated_on.year == onboard_time.year:
                    if service.flag:
                        if "onboarding_email" not in service.flag.keys():
                            service.flag["onboarding_email"] = "Por enviar"
                            service.save()
                            cen = 1
                        else:
                            if service.flag["onboarding_email"] != "Enviado":
                                service.flag["onboarding_email"] = "Por enviar"
                                service.save()
                                cen = 1
                            else:
                                print("Ya lo envié")
                    else:
                        json = {"onboarding_email": "Por enviar"}
                        service.flag = json
                        service.save()
                        cen = 1

                    if service.flag:
                        if "bloqueado_onboarding_email" in service.flag.keys():
                            if service.flag["bloqueado_onboarding_email"] == 'Si':
                                cen = 0
                    
                    nombre = ''
                    if service.customer.kind == 1:
                        nombre = service.customer.first_name.split(" ")[0]
                    else:
                        nombre = service.customer.name

                    if getattr(config, "allow_send_onboarding_email") == "yes" and service.status == 1:
                        if cen == 1:
                            url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_url").format(NUMBER=int(service.number))
                            token_sentinel = getattr(config, "sentinel_token")
                            headers = {'Authorization':token_sentinel}
                            node=None
                            network_mismatch=""
                            try:
                                r = requests.get(url,headers=headers,verify=False, timeout=30).json()
                                node=r["node"]
                                network_mismatch=r["network_mismatch"]
                            except Exception as e:
                                print(str(e))

                            if node is None:
                                node={
                                    'alias': '',
                                    'street': '',
                                    'house_number': '',
                                    'commune':'',
                                    'expected_power':'',
                                    'towers': '',
                                    'apartments':'',
                                    'phone':'',
                                }

                            # To get the network info
                            url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_network_url").format(NUMBER=int(service.number))
                            try:
                                r = requests.get(url,headers=headers,verify=False, timeout=30)
                                #print(r)
                                network = r.json()
                            except Exception as e:
                                print(str(e))
                                network={
                                    "mac_eth1":"",
                                    "kind_technology":""
                                }

                            info = []
                            info.append(
                            {
                                "number": str(service.number),
                                "street": str(service.street),
                                "house_number": str(service.house_number),
                                "apartment_number": str(service.apartment_number),
                                "tower": str(service.tower),
                                "location": str(service.location),
                                "price_override": str(service.price_override),
                                "due_day": str(service.due_day),
                                "activated_on": str(service.activated_on) if service.activated_on else "",
                                "installed_on": str(service.installed_on)  if service.installed_on else "",
                                "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                                "expired_on": str(service.expired_on)  if service.expired_on else "",
                                "status": str(service.get_status_display()),
                                "network_mismatch": "Sí" if network_mismatch else "No",
                                "ssid": "", # str(service.ssid), #####
                                "technology_kind":  str(network["kind_technology"]) if "kind_technology" in network else "", 
                                "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                                "seen_connected": str(service.seen_connected),
                                "mac_onu": str(network["mac_eth1"]) if "mac_eth1" in network else "", #str(service.mac_onu),
                                "composite_address": str(service.customer.composite_address),
                                "commune": str(node["commune"]),
                                "plan__name": str(service.plan.description_facturacion),
                                "plan__price": str(service.plan.price),
                                "plan__category": str(service.plan.category),
                                "plan__uf":  "Sí" if service.plan.uf else "No",
                                "plan__active": "Sí" if service.plan.active else "No", 
                                "customer__rut": str(service.customer.rut),
                                "customer__name": str(service.customer.name),
                                "customer__first_name": str(nombre),
                                "customer__last_name": str(service.customer.last_name), ###NUEVO###
                                "customer__email": str(service.customer.email),
                                "customer__street": str(service.customer.street),
                                "customer__house_number": str(service.customer.house_number),
                                "customer__apartment_number": str(
                                    service.customer.apartment_number
                                ),
                                "customer__tower": str(service.customer.tower),
                                "customer__location": str(service.customer.location),
                                "customer__phone": str(service.customer.phone),
                                "customer__default_due_day": str(
                                    service.customer.default_due_day
                                ),
                                "customer__composite_address": str(
                                    service.customer.composite_address
                                ),
                                "customer__commune": str(service.customer.commune),
                                "service__number": str(service.number),
                                "customer__id": str(service.customer.id),
                            })
                            
                            data = {"event": getattr(config, "evento_correo_onboarding"), "info": info}
                            url = getattr(config, "iris_server") + "api/v1/communications/trigger_event/active/"

                            try:
                                send = requests.post(
                                    url=url,
                                    json=data,
                                    headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                                    verify = False
                                )
                                if "Error" in send.json() or "error" in send.json():
                                    print("ERROR")
                                else:
                                    service.flag["onboarding_email"] = "Enviado"
                                    service.save()
                            except Exception as e:
                                print("e",e)
                            
                    
@celery_app.task(name = "four_days_whatsapp")  
def four_days_whatsapp():

    services = Service.objects.all()
    for service in services:
        if service.activated_on:
            activated_on = service.activated_on
            for day in range(4,9):
                whats_time = timezone.now() - timedelta(days=day)
                cen = 0
                if activated_on.day == whats_time.day and activated_on.month == whats_time.month and activated_on.year == whats_time.year:
                    if service.flag:
                        if "four_days_whatsapp" not in service.flag.keys():
                            service.flag['four_days_whatsapp'] = "Por enviar"
                            service.save()
                            cen = 1
                        else:
                            if service.flag["four_days_whatsapp"] != "Enviado":
                                service.flag["four_days_whatsapp"] = "Por enviar"
                                service.save()
                                cen = 1
                            else:
                                print("ya lo envié")
                    else:
                        json = {"four_days_whatsapp": "Enviado"}
                        service.flag = json
                        service.save()
                        cen = 1
                   
                    if getattr(config, "allow_send_whatsapp") == "yes":
                        if cen == 1:
                            url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_url").format(NUMBER=int(service.number))
                            token_sentinel = getattr(config, "sentinel_token")
                            headers = {'Authorization':token_sentinel}
                            node=None
                            network_mismatch=""
                            try:
                                r = requests.get(url,headers=headers,verify=False, timeout=30).json()
                                node=r["node"]
                                network_mismatch=r["network_mismatch"]
                            except Exception as e:
                                print(str(e))

                            if node is None:
                                node={
                                    'alias': '',
                                    'street': '',
                                    'house_number': '',
                                    'commune':'',
                                    'expected_power':'',
                                    'towers': '',
                                    'apartments':'',
                                    'phone':'',
                                }

                            # To get the network info
                            url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_network_url").format(NUMBER=int(service.number))
                            try:
                                r = requests.get(url,headers=headers,verify=False, timeout=30)
                                #print(r)
                                network = r.json()
                            except Exception as e:
                                print(str(e))
                                network={
                                    "mac_eth1":"",
                                    "kind_technology":""
                                }

                            info = []
                            info.append(
                            {
                                "number": str(service.number),
                                "street": str(service.street),
                                "house_number": str(service.house_number),
                                "apartment_number": str(service.apartment_number),
                                "tower": str(service.tower),
                                "location": str(service.location),
                                "price_override": str(service.price_override),
                                "due_day": str(service.due_day),
                                "activated_on": str(service.activated_on) if service.activated_on else "",
                                "installed_on": str(service.installed_on)  if service.installed_on else "",
                                "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                                "expired_on": str(service.expired_on)  if service.expired_on else "",
                                "status": str(service.get_status_display()),
                                "network_mismatch": "Sí" if network_mismatch else "No",
                                "ssid": "", # str(service.ssid), #####
                                "technology_kind":  str(network["kind_technology"]) if "kind_technology" in network else "", 
                                "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                                "seen_connected": str(service.seen_connected),
                                "mac_onu": str(network["mac_eth1"]) if "mac_eth1" in network else "", #str(service.mac_onu),
                                "composite_address": str(service.customer.composite_address),
                                "commune": str(node["commune"]),
                                "node__code": str(node["alias"]),
                                "node__commune": str(node["commune"]),
                                "node__towers":  str(node["towers"]) ,
                                "node__phone": str(node["phone"]),
                                "plan__name": str(service.plan.description_facturacion),
                                "plan__price": str(int(service.plan.price)),
                                "plan__category": str(service.plan.category),
                                "plan__uf":  "Sí" if service.plan.uf else "No",
                                "plan__active": "Sí" if service.plan.active else "No", 
                                "customer__rut": str(service.customer.rut),
                                "customer__name": str(service.customer.name),
                                "customer__first_name": str(service.customer.first_name),
                                "customer__last_name": str(service.customer.last_name), ###NUEVO###
                                "customer__email": str(service.customer.email),
                                "customer__street": str(service.customer.street),
                                "customer__house_number": str(service.customer.house_number),
                                "customer__apartment_number": str(
                                    service.customer.apartment_number
                                ),
                                "customer__tower": str(service.customer.tower),
                                "customer__location": str(service.customer.location),
                                "customer__phone": str(service.customer.phone),
                                "customer__default_due_day": str(
                                    service.customer.default_due_day
                                ),
                                "customer__composite_address": str(
                                    service.customer.composite_address
                                ),
                                "customer__commune": str(service.customer.commune),
                            })

                            data = {"event": "four_days_whatsapp", "info": info}
                            url = getattr(config, "iris_server") + "api/v1/communications/trigger_event/"

                            
                            try:
                                send = requests.post(
                                    url=url,
                                    json=data,
                                    headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                                    verify = False
                                )
                                if "Error" in send.json() or "error" in send.json():
                                    print("ERROR")
                                else:
                                    service.flag["four_days_whastapp"] = "Enviado"
                                    service.save()
                            except Exception as e:
                                print("e",e)


@celery_app.task(name = "nuevo_cliente_email", queue="queue1")  
def nuevo_cliente_email():
    services = Service.objects.all()
    for service in services:
        if service.created_at:
            created_at = service.created_at
            two_ago = timezone.now() - timedelta(hours=2)
            five_ago = timezone.now() - timedelta(hours=5)
            seven_ago = timezone.now() - timedelta(hours=7)
            cen = 0
            if seven_ago <= created_at and created_at <= two_ago:
                if service.customer.flag:
                    if "nuevo_cliente_email" not in service.customer.flag.keys():
                        service.customer.flag['nuevo_cliente_email'] = "Por enviar"
                        service.customer.save()
                        cen = 1
                    else:
                        if service.customer.flag["nuevo_cliente_email"] != "Enviado":
                            service.customer.flag["nuevo_cliente_email"] = "Por enviar"
                            service.customer.save()
                            cen = 1
                        else:
                            print("ya lo envié")
                else:
                    json = {"nuevo_cliente_email": "Por enviar"}
                    service.customer.flag = json
                    service.customer.save()
                    cen = 1

                if service.customer.flag:
                    if "bloqueado_nuevo_cliente_email" in service.customer.flag.keys():
                        if service.customer.flag["bloqueado_nuevo_cliente_email"] == 'Si':
                            cen = 0

                orders = PlanOrder.objects.all().filter(service = service.pk)
                index = 0
                installation_date = ''
                while (index < len(orders)):
                    if orders[index].cancelled == False and orders[index].start != None:
                        installation_date = orders[index].start
                        break
                    index += 1
                  
                if installation_date:
                    MES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
                        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
                    formatted_installation_date = str(installation_date.day) + ' de ' + str(MES[installation_date.month-1]) + ' del ' + str(installation_date.year)
                else:
                    if created_at <= five_ago:
                        formatted_installation_date = '(Por Definir)'
                    else:
                        formatted_installation_date = ''
                        cen = 0

                nombre = ''
                if service.customer.kind == 1:
                    nombre = service.customer.first_name.split(" ")[0]
                else:
                    nombre = service.customer.name

                costo_habilitacion = ''
                if service.technology_kind == 1:
                    costo_habilitacion = '<li>Debido a que tu dirección arroja un Servicio UTP se adicionara un costo de Habilitación, habitualmente el costo es de $50.000 pesos; sin embargo, por una promoción referente al COVID-19 obtendrá una rebaja y solo deberá cancelar <strong>$20.000 pesos</strong>.</li>'


                if getattr(config, "allow_send_nuevo_cliente_email") == "yes":
                    if cen == 1:
                        url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_url").format(NUMBER=int(service.number))
                        token_sentinel = getattr(config, "sentinel_token")
                        headers = {'Authorization':token_sentinel}
                        node=None
                        network_mismatch=""
                        try:
                            r = requests.get(url,headers=headers,verify=False, timeout=30).json()
                            node=r["node"]
                            network_mismatch=r["network_mismatch"]
                        except Exception as e:
                            print(str(e))
                        
                        if node is None:
                            node={
                                    'alias': '',
                                    'street': '',
                                    'house_number': '',
                                    'commune':'',
                                    'expected_power':'',
                                    'towers': '',
                                    'apartments':'',
                                    'phone':'',
                            }

                        # To get the network info
                        url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_network_url").format(NUMBER=int(service.number))
                        try:
                            r = requests.get(url,headers=headers,verify=False, timeout=30)
                            #print(r)
                            network = r.json()
                        except Exception as e:
                            print(str(e))
                            network={
                                "mac_eth1":"",
                                "kind_technology":""
                            }

                        info = []
                        info.append(
                        {
                            "number": str(service.number),
                            "street": str(service.street),
                            "house_number": str(service.house_number),
                            "apartment_number": str(service.apartment_number),
                            "tower": str(service.tower),
                            "location": str(service.location),
                            "price_override": str(service.price_override),
                            "due_day": str(service.due_day),
                            "activated_on": str(service.activated_on) if service.activated_on else "",
                            "installed_on": str(service.installed_on)  if service.installed_on else "",
                            "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                            "expired_on": str(service.expired_on)  if service.expired_on else "",
                            "status": str(service.get_status_display()),
                            "network_mismatch": "Sí" if network_mismatch else "No",
                            "ssid": "", # str(service.ssid), #####
                            "technology_kind":  str(network["kind_technology"]) if "kind_technology" in network else "", 
                            "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                            "seen_connected": str(service.seen_connected),
                            "mac_onu": str(network["mac_eth1"]) if "mac_eth1" in network else "", #str(service.mac_onu),
                            #"network_mismatch": "Sí" if service.network_mismatch else "No",
                            #"ssid": str(service.ssid),
                            #"technology_kind": str(service.get_technology_kind_display()),
                            #"allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                            #"seen_connected": str(service.seen_connected),
                            #"mac_onu": str(service.mac_onu),
                            "composite_address": str(service.customer.composite_address),
                            "commune": str(service.commune),
                            "plan__name": str(service.plan.description_facturacion),
                            "plan__price": str(int(service.plan.price)),
                            "plan__category": str(service.plan.category),
                            "plan__uf":  "Sí" if service.plan.uf else "No",
                            "plan__active": "Sí" if service.plan.active else "No", 
                            "customer__rut": str(service.customer.rut),
                            "customer__name": str(service.customer.name),
                            "customer__first_name": str(nombre),
                            "customer__last_name": str(service.customer.last_name), ###NUEVO###
                            "customer__email": str(service.customer.email),
                            "customer__street": str(service.customer.street),
                            "customer__house_number": str(service.customer.house_number),
                            "customer__apartment_number": str(
                                service.customer.apartment_number
                            ),
                            "customer__tower": str(service.customer.tower),
                            "customer__location": str(service.customer.location),
                            "customer__phone": str(service.customer.phone),
                            "customer__default_due_day": str(
                                service.customer.default_due_day
                            ),
                            "customer__composite_address": str(
                                service.customer.composite_address
                            ),
                            "customer__commune": str(service.customer.commune),
                            "installation_date": formatted_installation_date, 
                            "service__number": str(service.number),
                            "customer__id": str(service.customer.id),
                            "customer_html|safe": str(costo_habilitacion), 
                        })

                        data = {"event": getattr(config, "evento_cierre_de_ventas"), "info": info}
                        url = getattr(config, "iris_server") + "api/v1/communications/trigger_event/active/"
                        
                        try:
                            send = requests.post(
                                url=url,
                                json=data,
                                headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                                verify = False
                            )
                            if "Error" in send.json() or "error" in send.json():
                                print("ERROR")
                            else:
                                service.customer.flag["nuevo_cliente_email"] = "Enviado"
                                service.customer.save()
                        except Exception as e:
                            print("e",e)
                        

@celery_app.task(name = "post_activacion_email", queue="queue1")  
def post_activacion_email():
    services = Service.objects.all()
    for service in services:
        if service.activated_on:
            activated_on = service.activated_on
            two_ago = timezone.now() - timedelta(hours=2)
            five_ago = timezone.now() - timedelta(hours=5)
            cen = 0
            if five_ago <= activated_on and activated_on <= two_ago:
                if service.flag:
                    if "post_activacion_email" not in service.flag.keys():
                        service.flag['post_activacion_email'] = "Por enviar"
                        service.save()
                        cen = 1
                    else:
                        if service.flag["post_activacion_email"] != "Enviado":
                            service.flag["post_activacion_email"] = "Por enviar"
                            service.save()
                            cen = 1
                        else:
                            print("ya lo envié")
                else:
                    json = {"post_activacion_email": "Por enviar"}
                    service.flag = json
                    service.save()
                    cen = 1

                if service.flag:
                    if "bloqueado_post_activacion_email" in service.flag.keys():
                        if service.flag["bloqueado_post_activacion_email"] == 'Si':
                            cen = 0

                nombre = ''
                if service.customer.kind == 1:
                    nombre = service.customer.first_name.split(" ")[0]
                else:
                    nombre = service.customer.name
                
                if getattr(config, "allow_send_post_activacion_email") == "yes":
                    if cen == 1:
                        info = []
                        info.append(
                        {
                            "number": str(service.number),
                            "street": str(service.street),
                            "house_number": str(service.house_number),
                            "apartment_number": str(service.apartment_number),
                            "tower": str(service.tower),
                            "location": str(service.location),
                            "price_override": str(service.price_override),
                            "due_day": str(service.due_day),
                            "activated_on": str(service.activated_on) if service.activated_on else "",
                            "installed_on": str(service.installed_on)  if service.installed_on else "",
                            "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                            "expired_on": str(service.expired_on)  if service.expired_on else "",
                            "status": str(service.get_status_display()),
                            "network_mismatch": "Sí" if service.network_mismatch else "No",
                            "ssid": str(service.ssid),
                            "technology_kind": str(service.get_technology_kind_display()),
                            "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                            "seen_connected": str(service.seen_connected),
                            "mac_onu": str(service.mac_onu),
                            "composite_address": str(service.customer.composite_address),
                            "commune": str(service.commune),
                            "plan__name": str(service.plan.description_facturacion),
                            "plan__price": str(service.plan.price),
                            "plan__category": str(service.plan.category),
                            "plan__uf":  "Sí" if service.plan.uf else "No",
                            "plan__active": "Sí" if service.plan.active else "No", 
                            "customer__rut": str(service.customer.rut),
                            "customer__name": str(service.customer.name),
                            "customer__first_name": str(nombre),
                            "customer__last_name": str(service.customer.last_name), ###NUEVO###
                            "customer__email": str(service.customer.email),
                            "customer__street": str(service.customer.street),
                            "customer__house_number": str(service.customer.house_number),
                            "customer__apartment_number": str(
                                service.customer.apartment_number
                            ),
                            "customer__tower": str(service.customer.tower),
                            "customer__location": str(service.customer.location),
                            "customer__phone": str(service.customer.phone),
                            "customer__default_due_day": str(
                                service.customer.default_due_day
                            ),
                            "customer__composite_address": str(
                                service.customer.composite_address
                            ),
                            "customer__commune": str(service.customer.commune),
                            "service__number": str(service.number),
                            "customer__id": str(service.customer.id),
                        })

                        data = {"event": getattr(config, "evento_post_activacion"), "info": info}
                        url = getattr(config, "iris_server") + "api/v1/communications/trigger_event/active/"


                        try:
                            send = requests.post(
                                url=url,
                                json=data,
                                headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                                verify = False
                            )
                            if "Error" in send.json() or "error" in send.json():
                                print("ERROR")
                            else:
                                service.flag["post_activacion_email"] = "Enviado"
                                service.save()
                        except Exception as e:
                            print("e",e)
                        