from django.conf.urls import url
from .views import (
    ResponseViewSet,
    Bill,
    PreBill,
    PreBillUnified,
    BillPDF,
    unifiedBillPDF,
    service_ajax_1,
    service_ajax_2,
    service_unified_ajax,
    service_unified_ajax_2,
    for_billing,
    processed_ajax,
    processed,
    cycles_ajax,
    cycles_ajax_2,
    billing_cycle,
    folios_ajax,
    folios_ajax_2,
    folios,
    folio_delete,
    indicators_ajax,
    indicators_ajax_2,
    indicators,
    generateBarcode,
    report_invoices,
    reportInvoiceAjax,
    service_check_ajax,
    for_billing_check,
    calendario_cortes,
    calendario_cortes_ajax,
    calendario_cortes_create,
    delete_calendario_cortes,
)
from customers.urls import router

router.register(r"taxation-response", ResponseViewSet)

urlpatterns = [
    ################# BILLING ##################
    url(r"billing-view/(?P<pk>\d+)/$", Bill),
    url(r"pre-bill/(?P<pk>\d+)/$", PreBill),
    url(r"pre-bill-unified/(?P<pk>\d+)/$", PreBillUnified),
    url(r"billpdf/(?P<pk>\d+)/$", BillPDF),
    url(r"unified-billpdf/(?P<pk>\d+)/$", unifiedBillPDF),
    # For Billing
    url(
        r"finances/cycles/forbilling/(?P<pk>\d+)/ajax/$",
        service_ajax_1,
        name="service-ajax-1",
    ),
    url(
        r"finances/cycles/forbilling/(?P<pk>\d+)/ajax2/$",
        service_ajax_2,
        name="service-ajax-2",
    ),
    url(
        r"finances/cycles/forbilling/(?P<pk>\d+)/ajax-unified/$",
        service_unified_ajax,
        name="service-unified-ajax",
    ),
    url(
        r"finances/cycles/forbilling/(?P<pk>\d+)/ajax-unified-2/$",
        service_unified_ajax_2,
        name="service-unified-ajax-2",
    ),
    url(r"finances/cycles/forbilling/(?P<pk>\d+)/$", for_billing),
    url(r"finances/cycles/forbillingcheck/(?P<pk>\d+)/$", for_billing_check, name="for_billing_check"),
    url(
        r"finances/cycles/forbillingcheck/(?P<pk>\d+)/ajax/$",
        service_check_ajax,
        name="service-check-ajax",
    ),
    # Processed
    url(
        r"finances/cycles/processed/(?P<pk>\d+)/ajax/$",
        processed_ajax,
        name="processed-ajax",
    ),
    url(r"finances/cycles/processed/(?P<pk>\d+)/$", processed, name="processed"),
    # Cycles
    url(r"finances/cycles/ajax/$", cycles_ajax, name="cycles-ajax"),
    url(r"finances/cycles/ajax2/$", cycles_ajax_2, name="cycles-ajax-2"),
    url(r"finances/cycles/$", billing_cycle, name="billing-cycle"),
    # Folios
    url(r"finances/management/folios/ajax/$", folios_ajax, name="folios-ajax"),
    url(r"finances/management/folios/ajax2/$", folios_ajax_2, name="folios-ajax-2"),
    url(r"finances/management/folios/$", folios, name="folios"),
    url(r"finances/management/folios/delete/$", folio_delete, name="folio-delete"),
    # Indicators
    url(
        r"finances/management/indicators/ajax/$",
        indicators_ajax,
        name="indicators-ajax",
    ),
    url(
        r"finances/management/indicators/ajax2/$",
        indicators_ajax_2,
        name="indicators-ajax-2",
    ),
    url(r"finances/management/indicators/$", indicators, name="indicators"),
    url(r"finances/management/barcode/$", generateBarcode, name="generate_barcode"),
    url(r"finances/calendario_cortes/$", calendario_cortes, name="calendario-cortes"),
    url(r"finances/calendario_cortes/ajax/$",calendario_cortes_ajax, name="calendario-cortes-ajax"),
    url(r"finances/calendario_cortes/new/$", calendario_cortes_create, name="calendario-cortes-create"),
    url(r"^finances/calendario_cortes/(?P<slug>\d+)/delete/$", delete_calendario_cortes, name="delete-calendario-cortes"),
]
