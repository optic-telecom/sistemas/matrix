from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, SmartResize


class Pic(models.Model):
    photo = models.ImageField(_('photo'), upload_to='photos')
    photo_standard = ImageSpecField(source='photo',
                                    processors=[SmartResize(800, 600)],
                                    format='JPEG',
                                    options={'quality': 80})
    photo_thumbnail = ImageSpecField(source='photo',
                                     processors=[ResizeToFill(320, 240)],
                                     format='JPEG',
                                     options={'quality': 60})
    caption = models.CharField(_('caption'), max_length=255)
    created_at = models.DateTimeField(default=timezone.now)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.caption
