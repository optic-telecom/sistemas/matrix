from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import Pic


class PicForm(forms.ModelForm):
    class Meta:
        model = Pic
        fields = ['photo',
                  'caption']


class MassPicUploadForm(forms.Form):
    photos = forms.FileField(label=_('photos'))
    caption = forms.CharField(label=_('caption'), max_length=220)


class PicUpdateForm(forms.ModelForm):
    class Meta:
        model = Pic
        fields = ['caption']
