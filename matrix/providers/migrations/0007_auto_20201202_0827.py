# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2020-12-02 12:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('providers', '0006_auto_20201126_0925'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalpayment',
            name='kind_load',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Manual'), (2, 'No encontrado'), (3, 'Multiples proveedores')], default=1, verbose_name='kind_load'),
        ),
        migrations.AddField(
            model_name='historicalpayment',
            name='transfer_id',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='transfer_id'),
        ),
        migrations.AddField(
            model_name='payment',
            name='kind_load',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Manual'), (2, 'No encontrado'), (3, 'Multiples proveedores')], default=1, verbose_name='kind_load'),
        ),
        migrations.AddField(
            model_name='payment',
            name='transfer_id',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='transfer_id'),
        ),
    ]
