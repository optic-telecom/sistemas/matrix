import requests
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save,pre_save
from django.dispatch import receiver
from django.utils import timezone
from constance import config
from customers.models import Indicators
from .models import Provider, Payment, Invoice
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
import urllib3 
import json

@receiver(pre_save, sender=Provider)
def provider_currency_change(sender, instance, **kwargs):
    #print("jajajaj")
    try:
        old_instance = Provider.objects.get(pk = instance.pk)
        if old_instance.currency != instance.currency and old_instance.currency!=None: 
            # Se trae el indicador de la empresa del operador.
            indicadores=Indicators.objects.filter(company=old_instance.operator.company)
            # Si la moneda de la empresa coincide con la del servicio.
            # significa que la nueva moneda es el Dolar.
            if old_instance.currency==old_instance.operator.company.currency:
                tasa=1/indicadores.filter(kind=3).last().value     
            else:
                # Si la moneda de la empresa no coincide con la del servicio.
                # significa que la nueva moneda es la de la compania.
                tasa=indicadores.filter(kind=3).last().value
            #print(tasa)
            # Truncar en 5 decimales.
            tasa=round(tasa, 5)

            # Todos los pagos que son de la moneda anterior le tengo que colocar la tasa.
            Payment.objects.filter(
                provider=old_instance.id,
                bank_account__currency=old_instance.currency).update(tasa=tasa)

            Payment.objects.filter(
                provider=old_instance.id,
                bank_account__currency=instance.currency).update(tasa=1)
 
            # Todos los pagos que son de la moneda anterior le tengo que colocar la tasa.
            Invoice.objects.filter(
                provider=old_instance.id,
                currency=old_instance.currency).update(tasa=tasa)

            Invoice.objects.filter(
                provider=old_instance.id,
                currency=instance.currency).update(tasa=1)

        else:
            # No hago nada con los pagos y boletas
            pass
    except Provider.DoesNotExist:
        pass
