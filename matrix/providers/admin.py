from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from .models import (Provider, Invoice, Payment,
    FinancialRecord, Balance,ProviderPayorRut)
from import_export.admin import ImportExportModelAdmin
from .resources import (ProviderResource, InvoiceResource,
    PaymentResource, FinancialRecordResource, BalanceResource,
    ProviderPayorRutResource)


@admin.register(Provider)
class ProviderAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('rut', 'name', 'email', 'address', 'location', 'operator')
    search_fields = ('rut', 'name', 'email')
    resource_class = ProviderResource


@admin.register(Invoice)
class InvoiceAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('id', 'due_date', 'comment', 'total', 'paid', 'provider', 'operator')
    list_filter = ('paid', 'operator', 'due_date')
    search_fields = ['id', 'provider__name']
    resource_class = InvoiceResource


@admin.register(Payment)
class PaymentAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('deposited_on', 'id', 'provider', 'operator', 'paid_on', 'amount', 'kind')
    list_filter = ('operator', 'kind')
    search_fields = ['id', 'provider__name']
    resource_class = PaymentResource

@admin.register(FinancialRecord)
class FinancialRecordAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('record_id', 'ts', 'paid', 'charge', 'credit', 'comment', 'provider')
    search_fields = ('record_id', 'provider__name')
    resource_class = FinancialRecordResource

@admin.register(Balance)
class BalanceAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('provider', 'balance')
    search_fields = ('provider__name',)
    resource_class = BalanceResource

@admin.register(ProviderPayorRut)
class ProviderPayorRutAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('rut', 'name','provider')
    resource_class = ProviderPayorRutResource
