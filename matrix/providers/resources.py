from import_export import resources
from .models import (Provider, Invoice, Payment,
    FinancialRecord, Balance, ProviderPayorRut)

class ProviderResource(resources.ModelResource):

    class Meta:
        model = Provider


class InvoiceResource(resources.ModelResource):

    class Meta:
        model = Invoice

class PaymentResource(resources.ModelResource):

    class Meta:
        model = Payment

class FinancialRecordResource(resources.ModelResource):

    class Meta:
        model = FinancialRecord

class BalanceResource(resources.ModelResource):

    class Meta:
        model = Balance

class ProviderPayorRutResource(resources.ModelResource):

    class Meta:
        model= ProviderPayorRut