from django.conf import settings
from collections import namedtuple
from calendar import monthrange
from datetime import date
from itertools import tee, islice, chain
from math import ceil
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth import get_user_model
from django.db import models, transaction
from django.template.defaultfilters import truncatewords
from django.utils.translation import ugettext_lazy as _
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, SmartResize
from simple_history.models import HistoricalRecords
from customers.lefu.cl_territory import COMMUNE_CHOICES
from customers.models import *


def previous_and_next(some_iterable):
    prevs, items, nexts = tee(some_iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(prevs, items, nexts)


class BaseModel(models.Model):
    history = HistoricalRecords(inherit=True)
    created_at = models.DateTimeField(_('created at'),
                                      auto_now_add=True)

    def action_log(self, just=[]):
        fields = self.__class__._meta.fields
        filtered_fields = list(filter(lambda f: f.get_attname() in just, fields) if just else fields)
        field_names = [f.get_attname() for f in filtered_fields]
        historic_field_names = (just or field_names) + ['history_user_id', 'history_date']
        users = {None: 'matrix'}
        users.update({u.pk: u.username for u in get_user_model().objects.filter(id__in=self.history.values_list('history_user_id', flat=True).distinct())})
        rx = self.history.values(*historic_field_names)
        changes = []

        
        for prev, record, nxt in previous_and_next(rx):
            if nxt:
                changed_fields = [f for f in filtered_fields if record[f.get_attname()] != nxt[f.get_attname()]]
                verbose_changed_fields = [f.verbose_name for f in changed_fields]
                if changed_fields:
                    changes.append({'user': users[record['history_user_id']],
                                    'action': _('changed'),
                                    'changes': verbose_changed_fields,
                                    'current_values': {field.attname: record[field.attname] for field in changed_fields},
                                    'previous_values': {field.attname: nxt[field.attname] for field in changed_fields},
                                    'dt': record['history_date']})
            else:
                if not just:
                    changes.append({'user': users[record['history_user_id']],
                                    'action': _('created'),
                                    'dt': record['history_date']})
        return changes

    @property
    def agent(self):
        return self.history.earliest().history_user

    @property
    def last_edited_by(self):
        return self.history.latest().history_user

    class Meta:
        abstract = True


class Provider(BaseModel):
    rut = models.CharField(_('rut'), max_length=20, unique=True)
    name = models.CharField(_('name'), max_length=255)
    email = models.EmailField()
    street = models.CharField(_('street'), max_length=75, blank=True)
    house_number = models.CharField(_('house number'), max_length=75, blank=True)
    apartment_number = models.CharField(_('apartment number'), max_length=75, blank=True)
    tower = models.CharField(_('tower'), max_length=75, blank=True)
    location = models.CharField(_('commune'), max_length=5, choices=COMMUNE_CHOICES)
    phone = models.CharField(_('phone'), max_length=75)
    operator = models.ForeignKey('customers.Operator',verbose_name=_('operator'), default=2)
    notes = models.TextField(_('notes'), blank=True)
    currency = models.ForeignKey('customers.Currency',null=True, blank=True)

    @property
    def composite_address(self):
        partial_address = "{} {}".format(self.street, self.house_number)

        if self.apartment_number.strip():
            partial_address += " dpto {}".format(self.apartment_number)

        if self.tower.strip():
            partial_address += " torre {}".format(self.tower)

        return partial_address.strip()

    @property
    def address(self):
        return self.composite_address
    address.fget.short_description = u'Dirección'

    @property
    def truncated_name(self):
        return truncatewords(self.name, 5)


    def get_statement(self, limit=15, offset=0):
        records = self.financialrecord_set.all()
        previous_balance = 0
        current_balance = previous_balance

        field_names = [field.get_attname() for field in FinancialRecord._meta.fields] + ['balance', 'absolute_url']
        EnhancedRecord = namedtuple('EnhancedRecord', field_names)
        enhanced_results = []

        for record in reversed(records.values()):
            if record['record_id'].startswith('P'):
                record['absolute_url'] = reverse('provider-payment-detail', args=[record['record_id'][1:]])
            else:
                record['absolute_url'] = reverse('provider-invoice-detail', args=[record['record_id'][1:]])
            """
            if record['currency_id']!=None:
                record['symbol']=Currency.objects.get(id=record['currency_id']).symbol
            else:
                record['symbol']=""
            """
            current_balance -= record['charge'] or 0
            current_balance += record['credit'] or 0
            record['balance'] = current_balance
            enhanced_record = EnhancedRecord(**record)
            enhanced_results.insert(0, enhanced_record)

        return dict(initial_balance=previous_balance,
                    final_balance=current_balance,
                    records=enhanced_results)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('provider')
        verbose_name_plural = _('providers')
        permissions = (('view_provider', 'Can view provider data'),
                       ('list_provider', 'Can list provider data'),
                       ('search_provider', 'Can search provider data'))


class Invoice(BaseModel):

    MANUAL = 1  # Este es el default, incluye todos los de carga manual y los cargados por excel sin problema.
    NOT_FOUND = 2
    MULTIPLE_PROVIDERS = 3
    TYPE_OPTIONS = (
        (MANUAL, "Manual"),
        (NOT_FOUND, "No encontrado"),
        (MULTIPLE_PROVIDERS, "Multiples proveedores"),
    )

    provider = models.ForeignKey('Provider', verbose_name=_('provider'), null=True)
    folio = models.PositiveIntegerField()
    due_date = models.DateTimeField(_('due date'))
    neto = models.DecimalField(_("neto"), max_digits=12, decimal_places=2, default=0)
    iva = models.DecimalField(_("iva"), max_digits=12, decimal_places=2, default=0)
    total = models.DecimalField(_("Total"), max_digits=12, decimal_places=2, default=0)
    tax = models.DecimalField(_("impuesto adicional"), max_digits=12, decimal_places=2, default=0)
    exento = models.DecimalField(_("exento"), max_digits=12, decimal_places=2, default=0)
    #neto = models.PositiveIntegerField(_('neto'), default=0)
    #iva = models.PositiveIntegerField(_('iva'), default=0)
    #total = models.PositiveIntegerField(_('total'), default=0)
    #tax = models.PositiveIntegerField('impuesto adicional', default=0)
    #exento = models.PositiveIntegerField('exento', default=0)
    operator = models.ForeignKey('customers.Operator',verbose_name=_('operador'), default=2, related_name='%(class)s_operator')
    comment = models.CharField(_('comment'), max_length=255, default='')
    paid = models.BooleanField(_('paid'), default=True)
    paid_on = models.DateTimeField(_('paid on'),
                                   null=True)
    file = models.FileField(_('file'), upload_to='provider_invoices', null=True, blank=True)
    kind_load = models.PositiveSmallIntegerField(
        _("kind_load"), default=MANUAL, choices=TYPE_OPTIONS
    )
    tasa = models.DecimalField(_("tasa"), max_digits=20, decimal_places=5, null=True, blank=True)
    currency = models.ForeignKey('customers.Currency',null=True, blank=True, related_name='%(class)s_provider_currency')

    class Meta:
        #unique_together = ('provider', 'folio')
        permissions = (('list_invoice', 'Can list invoices'),
                       ('view_invoice', 'Can view invoices'))

    @property
    def price(self):
        if self.tasa:
            return self.tasa*self.total
        return ""

class Payment(BaseModel):
    CHEQUE = 1
    TRANSFER = 2
    CASH = 3
    PURCHASE_ORDER = 4
    OTHER = 5

    PAYMENT_OPTIONS = ((CHEQUE, 'Cheque'),
                       (TRANSFER, 'Transferencia'),
                       (CASH, 'Contado'),
                       (PURCHASE_ORDER, 'Orden de compra'),
                       (OTHER, 'Otro'))

    MANUAL = 1  # Este es el default, incluye todos los de carga manual y los cargados por excel sin problema.
    NOT_FOUND = 2
    MULTIPLE_PROVIDERS = 3
    TYPE_OPTIONS = (
        (MANUAL, "Manual"),
        (NOT_FOUND, "No encontrado"),
        (MULTIPLE_PROVIDERS, "Multiples proveedores"),
    )

    transfer_id = models.PositiveIntegerField(_('transfer_id'), null = True, blank = True)
    provider = models.ForeignKey('Provider', verbose_name=_('provider'), null=True)
    comment = models.CharField(_('comment'), max_length=255, default='')
    paid_on = models.DateTimeField(_('paid on'))
    kind = models.PositiveSmallIntegerField('tipo de pago', default=CHEQUE, choices=PAYMENT_OPTIONS)
    deposited_on = models.DateTimeField(_('deposited on'), null=True, blank=True)
    #amount = models.PositiveIntegerField(_('payment amount'), default=0)
    amount = models.DecimalField(_("payment amount"), max_digits=12, decimal_places=2, default=0)
    operator = models.ForeignKey('customers.Operator',verbose_name=_('operator'), default=2, related_name='%(class)s_operator')
    proof = models.ImageField(_('proof'), upload_to='provider_payments', null=True, blank=True)
    proof_standard = ImageSpecField(source='proof',
                                    processors=[SmartResize(800, 600)],
                                    format='JPEG',
                                    options={'quality': 80})
    proof_thumbnail = ImageSpecField(source='proof',
                                     processors=[ResizeToFill(320, 240)],
                                     format='JPEG',
                                     options={'quality': 60})
    file = models.FileField(_('file'), upload_to='provider_payments', null=True, blank=True)
    kind_load = models.PositiveSmallIntegerField(
        _("kind_load"), default=MANUAL, choices=TYPE_OPTIONS
    )
    tasa = models.DecimalField(_("tasa"), max_digits=20, decimal_places=5, null=True, blank=True)
    #currency = models.ForeignKey('customers.Currency',null=True, blank=True)
    bank_account = models.ForeignKey("customers.BankAccount", null=True, blank=True, related_name='%(class)s_bankaccount')
    number_document = models.BigIntegerField(null=True, blank=True)
    
    class Meta:
        permissions = (('list_payment', 'Can list payments'),
                       ('view_payment', 'Can view payments'))

    @property
    def price(self):
        if self.tasa:
            return self.tasa*self.amount
        return ""

class FinancialRecord(models.Model):
    record_id = models.CharField(max_length=255, primary_key=True)
    ts = models.DateTimeField()
    paid = models.BooleanField()
    original_charge = models.DecimalField(_("original charge"), max_digits=20, decimal_places=2, null=True, blank=True)
    original_credit = models.DecimalField(_("original credit"), max_digits=20, decimal_places=2, null=True, blank=True)
    charge = models.DecimalField(_("charge"), max_digits=20, decimal_places=2)
    credit = models.DecimalField(_("credit"), max_digits=20, decimal_places=2)
    tasa = models.DecimalField(_("tasa"), max_digits=20, decimal_places=5, null=True, blank=True)
    #charge = models.PositiveIntegerField()
    #credit = models.PositiveIntegerField()
    #currency = models.ForeignKey('customers.Currency', models.DO_NOTHING)
    comment = models.CharField(max_length=255)
    provider = models.ForeignKey('Provider', models.DO_NOTHING)

    class Meta:
        managed = False


class Balance(models.Model):
    provider = models.OneToOneField('Provider', models.DO_NOTHING)
    balance =  models.DecimalField(_("balance"), max_digits=20, decimal_places=2)

    class Meta:
        managed = False

class ProviderPayorRut(BaseModel):
    rut = models.CharField(_("rut"), max_length=20)
    name = models.CharField(_("name"), max_length=255, blank=True)
    provider = models.ForeignKey('Provider', verbose_name=_('provider'))

    class Meta:
        verbose_name = _("provider payor rut")
        verbose_name_plural = _("provider payor ruts")
        permissions = (("search_provider_ayorrut", "Can search provider payor ruts"),)
