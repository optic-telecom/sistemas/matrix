import json, io, os, re
import petl as etl
from tempfile import mkstemp
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls.base import reverse
from django.contrib.humanize.templatetags.humanize import intcomma
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q, Max, Count, Sum, Value
from .models import Invoice, Payment, Provider
from customers.models import Operator
from localflavor.cl.forms import CLRutField
from .forms import (ProviderForm, PaymentForm, InvoiceForm, PayorRutForm, 
                    BillingExcelForm, InvoiceUpdateForm, PaymentsExcelForm,
                    PaymentUpdateForm)
from .serializers import *
from django.contrib import messages
from customers.views import IsSystemInternalPermission
from django.db.models.functions import Concat
from django.template.defaultfilters import yesno
from django.contrib import messages
from rest_framework import viewsets, views, permissions, filters, status
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from url_filter.integrations.drf import DjangoFilterBackend
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from datetime import datetime
from datetime import date
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.auth.models import Group, User
from customers.models import UserOperator, Company, Operator, UserCompany

class ProviderStatement(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Provider
    permission_required = 'providers.view_provider'
    template_name = 'providers/provider_statement.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(ProviderStatement, self).get_context_data(*args, **kwargs)
        ctx["operators"] = Operator.objects.all()
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if op.company==self.object.operator.company.id:
                        can_show_detail=True
                else:
                    if op.operator==self.object.operator.id:
                        can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        return ctx
provider_statement = ProviderStatement.as_view()


class ProviderDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Provider
    permission_required = 'providers.view_provider'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if op.company==self.object.operator.company.id:
                        can_show_detail=True
                else:
                    if op.operator==self.object.operator.id:
                        can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        return ctx

provider_detail = ProviderDetail.as_view()


class InvoiceCreate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Invoice
    form_class = InvoiceForm
    success_message = 'Se ha creado una nueva factura.'
    permission_required = 'providers.add_invoice'

    def dispatch(self, *args, **kwargs):
        self.provider = get_object_or_404(Provider, pk=self.kwargs['pk'])
        return super(InvoiceCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(InvoiceCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(InvoiceCreate, self).get_context_data(*args, **kwargs)
        ctx['provider'] = self.provider
        ctx['cancel_url'] = reverse('provider-statement', args=[self.provider.pk])
        if self.request.user.is_authenticated():
            user=User.objects.get(username=self.request.user.username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    ctx["if_todos"]=True
                else:
                    ctx["if_todos"]=False
            except:
                pass
        return ctx

    def get_success_url(self, *args, **kwargs):
        return reverse('provider-statement', args=[self.provider.pk])

    def form_valid(self, form):
        invoice = form.save(commit=False)
        invoice.provider = self.provider
        return super(InvoiceCreate, self).form_valid(form)


invoice_create = InvoiceCreate.as_view()


class PaymentCreate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Payment
    form_class = PaymentForm
    success_message = 'Se ha creado un nuevo pago.'
    permission_required = 'providers.add_payment'

    def dispatch(self, *args, **kwargs):
        self.provider = get_object_or_404(Provider, pk=self.kwargs['pk'])
        return super(PaymentCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(PaymentCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentCreate, self).get_context_data(*args, **kwargs)
        ctx['provider'] = self.provider
        ctx['cancel_url'] = reverse('provider-statement', args=[self.provider.pk])
        return ctx

    def get_success_url(self, *args, **kwargs):
        return reverse('provider-statement', args=[self.provider.pk])

    def form_valid(self, form):
        payment = form.save(commit=False)
        payment.provider = self.provider
        return super(PaymentCreate, self).form_valid(form)

payment_create = PaymentCreate.as_view()


class ProviderCreate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Provider
    form_class = ProviderForm
    success_message = 'Se ha creado un nuevo proveedor.'
    permission_required = 'providers.add_provider'

    def get_success_url(self, *args, **kwargs):
        return reverse('provider-list')

    def get_form_kwargs(self):
        kwargs = super(ProviderCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        self.object = form.save()

        ProviderPayorRut.objects.create(provider=self.object, rut=form["rut"].value(), name=form["name"].value())
        messages.add_message(
            self.request, messages.SUCCESS, "Se ha creado un nuevo proveedor."
        )

        return super(ProviderCreate, self).form_valid(form)

provider_create = ProviderCreate.as_view()


class ProviderUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Provider
    form_class = ProviderForm
    success_message = 'Se ha actualizado el proveedor.'
    permission_required = 'providers.change_provider'

    def get_form_kwargs(self):
        kwargs = super(ProviderUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self, *args, **kwargs):
        return reverse('provider-list')


provider_update = ProviderUpdate.as_view()


class ProviderList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Provider
    queryset = Provider.objects.all()[:40]  # FIXME
    permission_required = 'providers.list_provider'

provider_list = ProviderList.as_view()


def price_str(number):
    value = ('%.6f' % number).rstrip('0').rstrip('.')
    return intcomma(value).replace(".", "*").replace(",", ".").replace("*", ",")

def prepare_provider(provider):
    
    return {'DT_RowId': provider.pk,
            'rut': provider.rut,
            'name': provider.truncated_name,
            'email': provider.email,
            'address': provider.address,
            'commune': provider.get_location_display(),
            'phone': provider.phone,
            'balance': str(provider.currency.symbol)+str(price_str(provider.balance.balance)) if provider.currency else "$"+price_str(provider.balance.balance),
            'operator': provider.operator.name
            }


@login_required
@permission_required('providers.list_provider')
def provider_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {'0': 'id',
                   '1': 'rut',
                   '2': 'name',
                   '3': 'email',
                   '4': 'db_composite_address',
                   '5': 'location',
                   '6': 'phone',
                   '7': 'balance__balance',
                   '8': 'operator'}
        orderings = {'asc': '',
                     'desc': '-'}
        order_column = request.GET.get('order[0][column]', None)
        order_dir = request.GET.get('order[0][dir]', None)
       
        # search
        search = request.GET.get('search[value]', '').replace('+', '')
        start = int(request.GET.get('start', 0))
        length = int(request.GET.get('length', 10))
        
    
        if order_column and order_dir:
            all_providers = Provider.objects.annotate(db_composite_address=Concat('street', Value(' '), 'house_number', Value(' dpto '), 'apartment_number', Value(' torre '), 'tower')).order_by(orderings[order_dir]+columns[order_column])
        else:
            all_providers = Provider.objects.all().annotate(db_composite_address=Concat('street', Value(' '), 'house_number', Value(' dpto '), 'apartment_number', Value(' torre '), 'tower'))
        all_count = all_providers.count()

        username = None
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    all_providers=all_providers.filter(operator__company__id=op.company)
                else:
                    all_providers=all_providers.filter(operator__id=op.operator)
            except:
                pass

        if search:
            rut_regex = '^' + '\.*'.join(list(search))

            q_objects = Q()
            q_objects |= Q(rut__iregex=rut_regex)
            q_objects |= Q(email__icontains=search)
            #q_objects |= Q(street__unaccent__icontains=search)
            #q_objects |= Q(commune__unaccent__icontains=search)

            splitted_name_queries = [Q(name__unaccent__icontains=term) for term in search.split(' ')]
            q_name_objects = Q()
            for q in splitted_name_queries:
                q_name_objects.add(q, Q.AND)

            q_objects |= q_name_objects

            filtered_providers = all_providers.filter(q_objects)
        else:
            filtered_providers = all_providers

        filtered_providers = filtered_providers.distinct()
            
        filtered_count = filtered_providers.count()

        if length == -1:
            sliced_providers = filtered_providers
        else:
            sliced_providers = filtered_providers[start:start+length]
        
        data = json.dumps({'draw': request.GET.get('draw', 1),
                           'recordsTotal': all_count,
                           'recordsFiltered': filtered_count,
                           'data': list(map(prepare_provider, sliced_providers))})
        
        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404


class PaymentDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Payment
    permission_required = 'providers.view_payment'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if op.company==self.object.operator.company.id:
                        can_show_detail=True
                else:
                    if op.operator==self.object.operator.id:
                        can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        return ctx

payment_detail = PaymentDetail.as_view()


class InvoiceDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Invoice
    permission_required = 'providers.view_invoice'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if op.company==self.object.operator.company.id:
                        can_show_detail=True
                else:
                    if op.operator==self.object.operator.id:
                        can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        return ctx

invoice_detail = InvoiceDetail.as_view()


class InvoiceUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Invoice
    form_class = InvoiceUpdateForm
    success_message = 'Se ha actualizado la factura.'
    permission_required = 'providers.change_invoice'
    template_name = "providers/invoice_form_update.html"


    def dispatch(self, *args, **kwargs):
        self.invoice = get_object_or_404(Invoice, pk=self.kwargs["pk"])
        try:
            self.provider = self.invoice.provider
            id_provider = self.invoice.provider.id
        except:
            if self.invoice.kind_load != 1:
                com = self.invoice.comment.split(" ")
                self.cleaned_rut = com[len(com) - 1]
        return super(InvoiceUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(InvoiceUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(InvoiceUpdate, self).get_context_data(*args, **kwargs)
        ctx['cancel_url'] = reverse('provider-invoice-detail', args=[self.object.pk])
        try:
            a=self.provider.name
            ctx["provider"] = self.provider
        except:
            cus = Provider.objects.filter(providerpayorrut__rut=self.cleaned_rut)
            if cus.count() != 0:
                ctx["providers"] = cus
        return ctx

    def get_success_url(self, *args, **kwargs):
        return reverse('provider-invoice-detail', args=[self.object.pk])

    def form_valid(self, form):
        invoice = form.save(commit=False)
        invoice_created = Invoice.objects.filter(
            folio=self.invoice.folio,
            provider=form.cleaned_data["provider"],
        ).exclude(id=self.invoice.id)
        if not invoice_created.exists():
            if (
                form["provider"].value()
                and self.invoice.kind_load == 2
                and (not self.invoice.provider)
            ):
                cus = Provider.objects.get(id=form["provider"].value())
                ProviderPayorRut.objects.create(
                    rut=self.cleaned_rut, name=cus.name, provider=cus
                )

            return super(InvoiceUpdate, self).form_valid(form)
        else:
            messages.error(
                self.request,
                "Ya existe una factura con ese folio y el mismo proveedor",
            )
            return HttpResponseRedirect(
                reverse_lazy("provider-invoice-update", kwargs={"pk": self.kwargs["pk"]})
            )
    def form_invalid(self, form):
        print(form.errors)
        return self.render_to_response(
            self.get_context_data(request=self.request, form=form))

invoice_update = InvoiceUpdate.as_view()

def default(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    if isinstance(obj, datetime):
        return obj.isoformat()
    raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)

@login_required
def ajax_provider(request):

    search = request.GET.get("search", False)

    if search != " ":
        provider = Provider.objects.filter(
            name__contains=search
        ) | Provider.objects.filter(rut__contains=search)
    else:
        provider = Provider.objects.none()

    provider = list(provider.values("id", "name"))
    context = {"provider": provider}
    data = json.dumps({"data": context}, default=default)
    return HttpResponse(data, content_type="application/json")


class PaymentUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Payment
    form_class = PaymentUpdateForm
    success_message = 'Se ha actualizado el pago.'
    permission_required = 'providers.change_payment'
    template_name = "providers/payment_form_update.html"

    def dispatch(self, *args, **kwargs):
        self.payment = get_object_or_404(Payment, pk=self.kwargs["pk"])
        try:
            self.provider = self.payment.provider
            id_provider = self.payment.provider.id
        except:
            if self.payment.kind_load != 1:
                com = self.payment.comment.split(" ")
                self.cleaned_rut = com[len(com) - 1]
        return super(PaymentUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(PaymentUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentUpdate, self).get_context_data(*args, **kwargs)
        ctx['cancel_url'] = reverse('provider-payment-detail', args=[self.object.pk])
        try:
            a=self.provider.name
            ctx["provider"] = self.provider
        except:
            cus = Provider.objects.filter(providerpayorrut__rut=self.cleaned_rut)
            if cus.count() != 0:
                ctx["providers"] = cus
        return ctx

    def get_success_url(self, *args, **kwargs):
        return reverse('provider-payment-detail', args=[self.object.pk])

    def form_valid(self, form):
        payment = form.save(commit=False)
        if (
            form["provider"].value()
            and self.payment.kind_load == 2
            and (not self.payment.provider)
        ):
            cus = Provider.objects.get(id=form["provider"].value())
            ProviderPayorRut.objects.create(rut=self.cleaned_rut, name=cus.name, provider=cus)

        return super(PaymentUpdate, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return self.render_to_response(
            self.get_context_data(request=self.request, form=form))

payment_update = PaymentUpdate.as_view()

@login_required
@permission_required("providers.view_provider")
def provider_payment_methods(request, provider_pk):
    provider = get_object_or_404(Provider, pk=provider_pk)
    payor_rut=ProviderPayorRut.objects.filter(provider=provider)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if op.company==provider.operator.company.id:
                    can_show_detail=True
            else:
                if op.operator==provider.operator.id:
                    can_show_detail=True
        except:
            pass
    context = {"provider": provider, "payor_rut": payor_rut, "can_show_detail":can_show_detail}
    return render(request, "providers/provider_payment_method.html", context)


@login_required
@permission_required("providers.change_provider")
def provider_payment_methods_add(request, provider_pk):
    provider = get_object_or_404(Provider, pk=provider_pk)
    if request.method == "POST":
        form = PayorRutForm(request.POST)
        if form.is_valid():
            payor_rut = form.save(commit=False)
            payor_rut.provider = provider
            payor_rut.save()
            messages.add_message(
                request, messages.SUCCESS, "Se ha creado nuevo RUT pagador."
            )
            return redirect(
                reverse("provider-payment-methods", kwargs={"provider_pk": provider.pk})
            )
    else:
        form = PayorRutForm()

    context = {
        "object": provider,
        "provider": provider,
        "form": form,
        "cancel_url": reverse(
            "provider-payment-methods", kwargs={"provider_pk": provider.pk}
        ),
    }
    return render(request, "providers/provider_payment_methods_add.html", context)


@login_required
@permission_required("providers.change_provider")
def provider_payment_methods_delete(request, provider_pk, pk):
    pr = get_object_or_404(ProviderPayorRut, pk=pk)
    pr.delete()

    messages.add_message(request, messages.SUCCESS, "Se ha eliminado el RUT pagador.")

    return redirect(
        reverse("provider-payment-methods", kwargs={"provider_pk": provider_pk})
    )



## manual invoicing
RUT_FIELD = CLRutField()

DATE_PARSER = etl.dateparser("%d-%m-%Y")

INVOICE_KINDS = {
    "BOLETA ELECTRONICA": 1,# Invoice.BOLETA,
    "FACTURA ELECTRONICA": 1,# Invoice.FACTURA,
    "NOTA DE CREDITO ELECTRONICA": 1,#Invoice.NOTA_DE_CREDITO,
    "NOTA DE DEBITO ELECTRONICA": 1,#Invoice.NOTA_DE_DEBITO,
}

CONSTRAINTS = [
    dict(name="folio_int", field="FOLIO", test=int),
    dict(name="due_date", field="FECHA DOCUMENTO", test=DATE_PARSER),
    dict(name="due_date", field="FECHA RECEPCION SII", test=DATE_PARSER),
    dict(name="rut_valid", field="RUT", test=RUT_FIELD.clean),
    dict(name="amount_int", field="TASA IVA", test=int),
]


def validate_data(table, constraints):
    t2 = etl.addrownumbers(table)
    problems = etl.validate(t2, constraints=constraints)
    rows_with_errors = set(problems.columns()["row"])
    invalid = etl.selectin(t2, "row", rows_with_errors)
    valid = etl.selectnotin(t2, "row", rows_with_errors)
    return valid, invalid


def process_manual_billing(excel_buffer, operator):
    raw_table = etl.fromxlsx(excel_buffer)
    valid, invalid = validate_data(raw_table, CONSTRAINTS)
    not_found = [raw_table.header()]
    multiple_services = [raw_table.header()]

    for t in valid.data():
        #print(t)
        #print(operator.company.currency)
        #aaaa
        try:
            print(t[29])
            #print(True if t[29]=="SI" else False)
            if t[11] != "0-0":
                cleaned_rut = RUT_FIELD.clean(t[11].lstrip("0"))
            else:
                cleaned_rut = t[11]

            provider = Provider.objects.get(providerpayorrut__rut=cleaned_rut)

            comment = "{} #{} - Ingreso Manual".format(t[3], t[4])
            
            ii, _ = Invoice.objects.update_or_create(
                    provider=provider,

                    folio=t[4],
                    defaults={
                        "due_date": DATE_PARSER(t[5]),
                        "comment": comment,
                        "exento": abs(t[18]),
                        "neto": abs(t[19]),
                        "iva": abs(t[20]),
                        "tax": abs(t[17]),
                        "total": float(t[26]), #abs(t[26]),
                        "paid": True if t[29]=="SI" else False,
                        "currency":provider.currency,
                    },
                    operator=operator,
                    kind_load=1,
            )

        except Provider.MultipleObjectsReturned:
            comment = "{} #{} - Ingreso Manual {}".format(t[3], t[4], cleaned_rut)

            invoice_created = Invoice.objects.filter(
                folio=t[4], provider__isnull=True
            )
            
            if  operator.company:
                currency=operator.company.currency
            else:
                currency=None

            if not invoice_created.exists():

                ii = Invoice.objects.create(
                    folio=t[4],
                    due_date=DATE_PARSER(t[5]),
                    comment=comment,
                    exento =  abs(t[18]),
                    neto = abs(t[19]),
                    iva = abs(t[20]),
                    tax = abs(t[17]),
                    total = float(t[26]), #abs(t[26]),
                    operator=operator,
                    paid = True if t[29]=="SI" else False,
                    kind_load=3,
                    currency = currency,
                )
            else:
                multiple_services.append(t[1:])

        except Provider.DoesNotExist:
            comment = "{} #{} - Ingreso Manual {}".format(t[3], t[4], cleaned_rut)

            invoice_created = Invoice.objects.filter(
                folio=t[4], provider__isnull=True
            )
            if  operator.company:
                currency=operator.company.currency
            else:
                currency=None
            if not invoice_created.exists():

                ii = Invoice.objects.create(
                    folio=t[4],
                    due_date=DATE_PARSER(t[5]),
                    comment=comment,
                    exento =  abs(t[18]),
                    neto = abs(t[19]),
                    iva = abs(t[20]),
                    tax = abs(t[17]),
                    total = float(t[26]), #abs(t[26]),
                    operator=operator,
                    paid = True if t[29]=="SI" else False,
                    kind_load=2,
                    currency =currency,
                )
                
            else:
                not_found.append(t[1:])

    excel_buffer.seek(0)

    valid_buffer = io.BytesIO()
    valid.cutout("row").toxlsx(valid_buffer)
    valid_buffer.seek(0)

    invalid_buffer = io.BytesIO()
    invalid.cutout("row").toxlsx(invalid_buffer)
    invalid_buffer.seek(0)

    not_found_buffer = io.BytesIO()
    etl.toxlsx(not_found, not_found_buffer)
    not_found_buffer.seek(0)

    multiple_services_buffer = io.BytesIO()
    etl.toxlsx(multiple_services, multiple_services_buffer)
    multiple_services_buffer.seek(0)

    return dict(
        processed_count=raw_table.nrows(),
        valid_count=valid.nrows(),
        invalid_count=invalid.nrows(),
        not_found_count=len(not_found) - 1,
        multiple_services_count=len(multiple_services) - 1,
        files=dict(
            processed=excel_buffer,
            valid=valid_buffer,
            invalid=invalid_buffer,
            not_found=not_found_buffer,
            multiple_services=multiple_services_buffer,
        ),
    )


@login_required
@permission_required("customers.change_service")
def manual_billing(request):
    context = {}

    if request.method == "POST":
        form = BillingExcelForm(request.POST, request.FILES)
        context["form"] = form
        if form.is_valid():
            excel_file = form.cleaned_data["excel_file"]
            operator = form.cleaned_data["operator"]
            processing_results = process_manual_billing(excel_file, operator)
            files = processing_results.pop("files")
            processing_results["created_count"] = (
                processing_results["valid_count"]
                - processing_results["not_found_count"]
            )
            context.update(processing_results)

            # File shenaningans
            old_file_paths = request.session.get("billing_files", {})
            for file_path in old_file_paths.values():
                try:
                    os.remove(file_path)
                except FileNotFoundError:
                    pass

            new_file_paths = {}

            for k, v in files.items():
                fd, temp_path = mkstemp()
                with open(temp_path, "wb") as f:
                    f.write(v.read())
                new_file_paths[k] = temp_path
                os.close(fd)

            request.session["billing_files"] = new_file_paths
    else:
        context["form"] = BillingExcelForm()

        asks_for_file = request.GET.get("file", None)
        if asks_for_file:
            file_paths = request.session.get("billing_files", {})
            path = file_paths.get(asks_for_file, None)
            if path:
                with open(path, "rb") as f:
                    response = HttpResponse(
                        f,
                        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    )
                    response[
                        "Content-Disposition"
                    ] = "attachment; filename='{}.xlsx'".format(asks_for_file)
                    return response

    return render(request, "providers/manual_billing.html", context)


def prepare_invoice(invoice):
    try:
        dictionary = {
                "DT_RowId": invoice.pk,
                "pk": invoice.pk,
                "created_at": invoice.created_at.strftime("%d-%m-%Y %H:%M")
                if invoice.created_at
                else None,
                "customer_name": invoice.provider.name,
                "total": str(invoice.currency.symbol) +" "+str(price_str(invoice.total)) if invoice.currency else "$ {}".format(price_str(invoice.total)),
                "comment": invoice.comment,
                "paid": yesno(invoice.paid, "Sí,No"),
                "operator": invoice.operator.name,
                "customer_operator": invoice.provider.operator.name,
                "kind_load": invoice.get_kind_load_display().capitalize(),
                "symbol": invoice.currency.symbol if invoice.currency else "$"
            }
    except:
        dictionary = {
                "DT_RowId": invoice.pk,
                "pk": invoice.pk,
                "created_at": invoice.created_at.strftime("%d-%m-%Y %H:%M")
                if invoice.created_at
                else None,
                "customer_name": "",
                "total": str(invoice.currency.symbol) +" "+str(price_str(invoice.total)) if invoice.currency else "$ {}".format(price_str(invoice.total)),
                "comment": invoice.comment,
                "paid": yesno(invoice.paid, "Sí,No"),
                "operator": invoice.operator.name,
                "customer_operator": "",
                "kind_load": invoice.get_kind_load_display().capitalize(),
                "symbol": invoice.currency.symbol if invoice.currency else "$"
            }

    return dictionary

@login_required
@permission_required("providers.list_invoice")
def provider_invoice_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "pk",
            "1": "created_at",
            "2": "due_date",
            "3": "paid_on",
            "4": "customer__name",
            "5": "comment",
            "6": "total",
            "7": "paid_on",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)
        paid = request.GET.get("paid_on", None)
        operator = request.GET.get("operator", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_invoices = Invoice.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_invoices = Invoice.objects.all()

        if paid == "True":
            all_invoices = all_invoices.filter(paid=True)
            #all_invoices = all_invoices.filter(paid_on__isnull=False)
        if paid == "False":
            all_invoices = all_invoices.filter(paid=False)
            #all_invoices = all_invoices.filter(paid_on__isnull=True)

        if operator and operator != "null":
            all_invoices = all_invoices.filter(operator=operator)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_invoices = all_invoices.filter(operator=op.operator)
                else:
                    all_invoices = all_invoices.filter(operator__company__id=op.company)
            except:
                pass

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            all_invoices = all_invoices.filter(
                created_at__gte=datetime(year, month, day)
            )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            all_invoices = all_invoices.filter(
                created_at__lte=datetime(year, month, day, 23, 59, 59)
            )
        all_count = all_invoices.count()

        if search:
            filtered_invoices = all_invoices.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(provider__name__unaccent__icontains=search)
                | Q(comment__unaccent__icontains=search)
                | Q(total=int(search) if search.isdigit() else None)
            )
        else:
            filtered_invoices = all_invoices

        filtered_invoices = filtered_invoices.distinct()

        filtered_count = filtered_invoices.count()

        if length == -1:
            sliced_invoices = filtered_invoices
        else:
            sliced_invoices = filtered_invoices[start : start + length]
        
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_invoice, sliced_invoices)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@permission_required("providers.list_invoice")
def provider_invoice_ajax2(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "pk",
            "1": "created_at",
            "2": "due_date",
            "3": "paid",
            "4": "customer__name",
            "5": "comment",
            "6": "total",
            "7": "paid_on",
            "8": "kind_load",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)
        paid = request.GET.get("paid_on", None)
        operator = request.GET.get("operator", None)
        kind_load = request.GET.get("kind_load", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_invoices = Invoice.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_invoices = Invoice.objects.all()

        all_invoices = all_invoices.filter(provider__isnull=True)

        if paid == "True":
            all_invoices = all_invoices.filter(paid=True)
            #all_invoices = all_invoices.filter(paid_on__isnull=False)
        if paid == "False":
            all_invoices = all_invoices.filter(paid=False)
            #all_invoices = all_invoices.filter(paid_on__isnull=True)

        if kind_load == "2":
            all_invoices = all_invoices.filter(kind_load=2)
        elif kind_load == "3":
            all_invoices = all_invoices.filter(kind_load=3)

        if operator and operator != "null":
            all_invoices = all_invoices.filter(operator=operator)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_invoices = all_invoices.filter(operator=op.operator)
                else:
                    all_invoices = all_invoices.filter(operator__company__id=op.company)
            except:
                pass
            
        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            all_invoices = all_invoices.filter(
                created_at__gte=datetime(year, month, day)
            )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            all_invoices = all_invoices.filter(
                created_at__lte=datetime(year, month, day, 23, 59, 59)
            )
        all_count = all_invoices.count()

        if search:
            filtered_invoices = all_invoices.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(provider__name__unaccent__icontains=search)
                | Q(comment__unaccent__icontains=search)
                | Q(total=int(search) if search.isdigit() else None)
            )
        else:
            filtered_invoices = all_invoices

        filtered_invoices = filtered_invoices.distinct()

        filtered_count = filtered_invoices.count()

        if length == -1:
            sliced_invoices = filtered_invoices
        else:
            sliced_invoices = filtered_invoices[start : start + length]
        
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_invoice, sliced_invoices)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

class providerInvoiceList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Invoice
    paginate_by = 8
    template_name = "providers/provider_invoice_list.html"
    permission_required = "providers.list_invoice"

    def get_context_data(self, *args, **kwargs):
        ctx = super(providerInvoiceList, self).get_context_data(*args, **kwargs)
        ctx["operators"] = Operator.objects.all()
        return ctx

provider_invoice_list = providerInvoiceList.as_view()

class ProviderInvoiceListWithoutCustomer(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Invoice
    paginate_by = 8
    template_name = "providers/provider_invoice_list_without_provider.html"
    permission_required = "providers.list_invoice"

    def get_context_data(self, *args, **kwargs):
        ctx = super(ProviderInvoiceListWithoutCustomer, self).get_context_data(*args, **kwargs)
        ctx["operators"] = Operator.objects.all()
        return ctx


provider_invoice_list_without_customer = ProviderInvoiceListWithoutCustomer.as_view()

@login_required
@permission_required("providers.change_service")
def mark_as_paid(request, pk):
    Invoice.objects.filter(pk=pk).update(paid_on=date.today())
    Invoice.objects.filter(pk=pk).update(paid=True)
    messages.add_message(request, messages.SUCCESS, "Se ha marcado como pagada.")
    return redirect(request.META.get("HTTP_REFERER", reverse("provider-list")))

@login_required
@permission_required("providers.change_service")
def mark_as_unpaid(request, pk):
    Invoice.objects.filter(pk=pk).update(paid_on=None)
    Invoice.objects.filter(pk=pk).update(paid=False)
    messages.add_message(request, messages.SUCCESS, "Se ha marcado como no pagada.")
    return redirect(request.META.get("HTTP_REFERER", reverse("provider-list")))

class PaymentList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Payment
    paginate_by = 50
    permission_required = "providers.list_payment"
    template_name = "providers/provider_payment_list.html"
    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentList, self).get_context_data(*args, **kwargs)
        ctx["agents"] = get_user_model().objects.filter(is_active=True)
        ctx["operators"] = Operator.objects.all()
        return ctx

provider_payment_list = PaymentList.as_view()

@login_required
@permission_required("providers.list_payment")
def provider_payment_ajax(request, to_excel=False):

    if request.is_ajax() or to_excel:

        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "provider__name",
            "3": "provider__operator",
            "4": "paid_on",
            "5": "amount",
            "6": "kind",
            "7": "operator",
            "8": "provider_pk"
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        kind_kind = request.GET.get("kind_kind", None)
        operator = request.GET.get("operator", None)
        operator_provider = request.GET.get("operator_cliente", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_payments = Payment.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_payments = Payment.objects.all()

        if operator_provider and operator_provider != "null":
            providers = (
                Payment.objects.filter(operator=operator_provider)
                .order_by("provider_id")
                .values_list("provider_id")
                .distinct("provider_id")
            )
            all_payments = all_payments.filter(provider_id__in=providers)

        if operator and operator != "null":
            all_payments = all_payments.filter(operator=operator)

        if kind_kind:
            all_payments = all_payments.filter(kind=kind_kind)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            all_payments = all_payments.filter(
                created_at__gte=datetime(year, month, day)
            )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            all_payments = all_payments.filter(
                created_at__lte=datetime(year, month, day, 23, 59, 59)
            )

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_payments = all_payments.filter(operator=op.operator)
                else:
                    all_payments = all_payments.filter(operator__company__id=op.company)
            except:
                pass

        all_count = all_payments.count()

        if search:
            filtered_payments = all_payments.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(provider__name__unaccent__icontains=search)
                | Q(amount=int(search) if search.isdigit() else None)
            )
        else:
            filtered_payments = all_payments

        filtered_payments = filtered_payments.distinct()

        filtered_count = filtered_payments.count()

        if length == -1:
            sliced_payments = filtered_payments
        else:
            sliced_payments = filtered_payments[start : start + length]

        if to_excel:
            return list(map(prepare_payment, filtered_payments))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_payment, sliced_payments)),
                }
            )

            return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_payment(payment):
    dictionary = {
        "DT_RowId": payment.pk,
        "created_at": payment.created_at.strftime("%Y-%m-%d %H:%M"),
        "pk": payment.pk,
        "provider_name": payment.provider.name if payment.provider else "",
        "provider_pk": payment.provider.pk if payment.provider else "",
        "paid_on": payment.paid_on.strftime("%Y-%m-%d %H:%M"),
        "formatted_amount": price_str(payment.amount),
        "operator": payment.operator.name,
        "proof": True if payment.proof else False,
        "provider_operator": payment.provider.operator.name if payment.provider else "",
        "kind": payment.get_kind_display().capitalize(),
        "kind_load": payment.get_kind_load_display().capitalize(),
        "comment": payment.comment,
        "symbol": payment.bank_account.currency.symbol if payment.bank_account else "$"
    }

    return dictionary

def price_tag(number):
    value = ('%.6f' % number).rstrip('0').rstrip('.')
    return intcomma(value).replace(".", "*").replace(",", ".").replace("*", ",")
    """
    result = intcomma(number)
    result = result.replace(".", "*").replace(",", ".").replace("*", ",")
    total = result.split(",")
    if len(total) == 2:
        if total[1] == "00":
            return total[0]
        else:
            return result
    else:
        return result
    """
    
def validate_payment_data(table, constraints):
    t2 = etl.addrownumbers(table)
    problems = etl.validate(t2, constraints=constraints)
    print(problems)
    rows_with_errors = set(problems.columns()["row"])
    invalid = etl.selectin(t2, "row", rows_with_errors)
    valid = etl.selectnotin(t2, "row", rows_with_errors)
    return valid, invalid

DATETIME_PARSER = etl.datetimeparser("%d/%m/%Y", strict=True)

PAYMENT_CONSTRAINTS = [
    dict(name="paid_on_valid", field="Fecha", test=DATETIME_PARSER),
    dict(name="rut_valid", field="Rut Origen/Destino", test=RUT_FIELD.clean),
    dict(name="amount_int", field="Monto $", test=lambda x: x and isinstance(x, float) or isinstance(x, int)), # test=int),
    dict(name="document_int", field="Numero Documento", test=lambda x: x and int(x) or None)
]

def process_manual_payments(table, operator, bank_account):
    raw_table = table
    # special RUT massage
    stripped_table = etl.convert(raw_table, "Rut Origen/Destino", str.strip)
    #int_table = stripped_table.convert("Monto $", lambda x: int(x.replace(".", "").replace(" ", "")))
    valid, invalid = validate_payment_data(stripped_table, PAYMENT_CONSTRAINTS)
    not_found = [stripped_table.header()]
    multiple_services = [stripped_table.header()]
    #aaa
    for t in valid.data():
        try:
            #print(t)
            if t[3] != "0-0":
                cleaned_rut = RUT_FIELD.clean(t[3].lstrip("0"))
            else:
                cleaned_rut = t[3]

            try:
                number_doc=int(t[9])
            except:
                number_doc=None

            provider = Provider.objects.get(providerpayorrut__rut=cleaned_rut)
            comment = "{} {} {} {}".format(t[4], t[5], t[8], cleaned_rut)
            ii, _ = Payment.objects.update_or_create(
                    provider=provider,
                    transfer_id = t[2],
                    defaults={
                        "provider": provider,
                        "paid_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "deposited_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "amount": float(t[6]), #abs(t[6]),
                        "comment": comment,
                        "operator": operator,
                        "number_document":number_doc,
                        "bank_account":bank_account,
                    },
                    operator=operator,
                    kind_load=1,
            )

        except Provider.MultipleObjectsReturned:
            comment = "{} {} {} {}".format(t[4], t[5], t[8], cleaned_rut)
            try:
                number_doc=int(t[9])
            except:
                number_doc=None
            """
            payment_created = Payment.objects.filter(transfer_id = t[2], provider__isnull = True)

            if not payment_created.exists():
            """

            pp, _ = Payment.objects.update_or_create(
                    transfer_id = t[2],
                    defaults={
                        "paid_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "deposited_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "amount": float(t[6]), #abs(t[6]),
                        "comment": comment,
                        "operator": operator,
                        "kind_load": 3,
                        "number_document":number_doc,
                        "bank_account":bank_account
                    },
            )
            multiple_services.append(t[1:])
        except Payment.MultipleObjectsReturned:
            multiple_services.append(t[1:])
        except Provider.DoesNotExist:
            comment = "{} {} {} {}".format(t[4], t[5], t[8], cleaned_rut)

            try:
                number_doc=int(t[9])
            except:
                number_doc=None

            pp, _ = Payment.objects.update_or_create(
                    transfer_id = t[2],
                    defaults={
                        "paid_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "deposited_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "amount": float(t[6]), #abs(t[6]),
                        "comment": comment,
                        "operator": operator,
                        "kind_load": 2,
                        "number_document":number_doc,
                        "bank_account":bank_account
                    },
            )
            not_found.append(t[1:])
            """
            payment_created = Payment.objects.filter(transfer_id = t[2], provider__isnull = True)
            
            if not payment_created.exists():
                pp, _ = Payment.objects.create(
                    transfer_id=t[2],
                    paid_on = DATETIME_PARSER(t[1]).replace(hour=21),
                    deposited_on = DATETIME_PARSER(t[1]).replace(hour=21),
                    amount =  abs(t[6]),
                    comment = comment,
                    operator = operator,
                    kind_load = 2
                )
            else:
                not_found.append(t[1:])
            """


    excel_buffer = io.BytesIO()
    stripped_table.toxlsx(excel_buffer)
    excel_buffer.seek(0)

    valid_buffer = io.BytesIO()
    valid.cutout("row").toxlsx(valid_buffer)
    valid_buffer.seek(0)

    invalid_buffer = io.BytesIO()
    invalid.cutout("row").toxlsx(invalid_buffer)
    invalid_buffer.seek(0)

    not_found_buffer = io.BytesIO()
    etl.toxlsx(not_found, not_found_buffer)
    not_found_buffer.seek(0)

    multiple_services_buffer = io.BytesIO()
    etl.toxlsx(multiple_services, multiple_services_buffer)
    multiple_services_buffer.seek(0)

    return dict(
        processed_count=stripped_table.nrows(),
        valid_count=valid.nrows(),
        invalid_count=invalid.nrows(),
        not_found_count=len(not_found) - 1,
        multiple_services_count=len(multiple_services) - 1,
        files=dict(
            processed=excel_buffer,
            valid=valid_buffer,
            invalid=invalid_buffer,
            not_found=not_found_buffer,
            multiple_services=multiple_services_buffer,
        ),
    )


@login_required
@permission_required("providers.change_service")
def manual_payments(request):
    context = {}
    if request.method == "POST":
        form = PaymentsExcelForm(request.POST, request.FILES)
        context["form"] = form
        if form.is_valid():
            table = form.cleaned_data["excel_file"]
            data = form.cleaned_data
            operator = data["operator"]
            bank_account = data["bank_account"]
            processing_results = process_manual_payments(table, operator, bank_account)
            files = processing_results.pop("files")
            processing_results["created_count"] = (
                processing_results["valid_count"]
                - processing_results["not_found_count"]
            )
            context.update(processing_results)

            # File shenaningans
            old_file_paths = request.session.get("payment_files", {})
            for file_path in old_file_paths.values():
                try:
                    os.remove(file_path)
                except FileNotFoundError:
                    pass

            new_file_paths = {}

            for k, v in files.items():
                fd, temp_path = mkstemp()
                with open(temp_path, "wb") as f:
                    f.write(v.read())
                new_file_paths[k] = temp_path
                os.close(fd)

            request.session["payment_files"] = new_file_paths
    else:
        context["form"] = PaymentsExcelForm()

        asks_for_file = request.GET.get("file", None)
        if asks_for_file:
            file_paths = request.session.get("payment_files", {})
            path = file_paths.get(asks_for_file, None)
            if path:
                with open(path, "rb") as f:
                    response = HttpResponse(
                        f,
                        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    )
                    response[
                        "Content-Disposition"
                    ] = "attachment; filename={}.xlsx".format(asks_for_file)
                    return response

    return render(request, "providers/providers_manual_payments.html", context)



class ProviderPaymentListWithoutCustomer(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Payment
    paginate_by = 8
    template_name = "providers/provider_payment_list_without_provider.html"
    permission_required = "providers.list_payment"

    def get_context_data(self, *args, **kwargs):
        ctx = super(ProviderPaymentListWithoutCustomer, self).get_context_data(*args, **kwargs)
        ctx["operators"] = Operator.objects.all()
        return ctx

provider_payment_list_without_customer = ProviderPaymentListWithoutCustomer.as_view()

@login_required
@permission_required("providers.list_payment")
def provider_payment_ajax2(request):

    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "paid_on",
            "3": "formatted_amount",
            "4": "kind",
            "5": "kind_load",
            "6": "operator",
            "7": "comment",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)
        operator = request.GET.get("operator", None)
        kind_load = request.GET.get("kind_load", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_payments = Payment.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_payments = Payment.objects.all()

        all_payments = all_payments.filter(provider__isnull=True)

        if kind_load == "2":
            all_payments = all_payments.filter(kind_load=2)
        elif kind_load == "3":
            all_payments = all_payments.filter(kind_load=3)

        if operator and operator != "null":
            all_payments = all_payments.filter(operator=operator)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_payments = all_payments.filter(operator=op.operator)
                else:
                    all_payments = all_payments.filter(operator__company__id=op.company)
            except:
                pass

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            all_payments = all_payments.filter(
                created_at__gte=datetime(year, month, day)
            )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            all_payments = all_payments.filter(
                created_at__lte=datetime(year, month, day, 23, 59, 59)
            )
        all_count = all_payments.count()

        if search:
            filtered_payments = all_payments.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(comment__unaccent__icontains=search)
                | Q(amount=int(search) if search.isdigit() else None)
            )
        else:
            filtered_payments = all_payments

        filtered_payments = filtered_payments.distinct()

        filtered_count = filtered_payments.count()

        if length == -1:
            sliced_payments = filtered_payments
        else:
            sliced_payments = filtered_payments[start : start + length]
        
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_payment, sliced_payments)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

###### API ###############



    
###############################    