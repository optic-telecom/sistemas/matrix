from localflavor.cl.forms import CLRutField
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django import forms
from easy_select2 import apply_select2, Select2
from .models import Provider, Invoice, Payment, ProviderPayorRut
import customers
import magic
import petl as etl
import csv
import xlrd
from customers.models import Operator, BankAccount, Bank
from django.core.exceptions import ValidationError
from django.contrib.auth.models import Group, User
from customers.models import UserCompany, UserOperator, Currency


class HTML5DateInput(forms.DateInput):
    input_type = 'date'

class PayorRutForm(forms.ModelForm):
    rut = CLRutField()

    class Meta:
        model = ProviderPayorRut
        fields = ["rut", "name"]

class InvoiceForm(forms.ModelForm):
    due_date = forms.DateField(label=_('due date'), input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format='%Y-%m-%d'))

    class Meta:
        model = Invoice
        fields = ['folio',
                  'due_date',
                  'neto',
                  'iva',
                  'total',
                  'tax',
                  'exento',
                  'operator',
                  'file',
                  'comment',
                  'currency',
                  'tasa']
        labels = {
        "currency": "Moneda"
        }

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(InvoiceForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
                #print(currency)
        currency=currency.distinct()
        self.fields["currency"].queryset = currency
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

class InvoiceUpdateForm(forms.ModelForm):
    due_date = forms.DateField(label=_('due date'), input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format='%Y-%m-%d'))

    class Meta:
        model = Invoice
        fields = ['provider',
                  'folio',
                  'due_date',
                  'neto',
                  'iva',
                  'total',
                  'tax',
                  'exento',
                  'operator',
                  'file',
                  'comment',
                  'currency',
                  'tasa']
        labels = {
        "currency": "Moneda"
        }
    
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(InvoiceUpdateForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
                #print(currency)
        currency=currency.distinct()
        self.fields["currency"].queryset = currency
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

    def clean_currency(self):
        currency = self.cleaned_data.get("currency", False)
        operator = self.cleaned_data.get("operator", False)

        if currency in operator.currency.all():
            return currency
        else:
            raise ValidationError("El operador que selecciono no acepta esa moneda")

class BillingExcelForm(forms.Form):
    excel_file = forms.FileField(label="Archivo Excel")
    operator = forms.ModelChoiceField(
        label="Operador", queryset=Operator.objects.all(), initial=1
    )

    def clean_excel_file(self):
        f = self.cleaned_data.get("excel_file", False)

        filetype = magic.from_buffer(f.read())
        #if not ("Microsoft Excel 2007+" in filetype or "Microsoft OOXML" in filetype):
        #    raise ValidationError("No es un archivo Excel")

        f.seek(0)
        raw_table = etl.fromxlsx(f)

        workbook = xlrd.open_workbook(file_contents=f.read())
        worksheet = workbook.sheet_by_index(0)

        table = []

        for rownum in range(worksheet.nrows):
            table.append([entry for entry in worksheet.row_values(rownum)])

        problems = etl.validate(
            table, [], (['ITEM', 'EMITIDO', 'DOCUMENTO', 'FOLIO', 'FECHA DOCUMENTO', 'FECHA RECEPCION SII', 'FECHA ACUSE', 'FORMA DE PAGO', 'TIPO DE COMPRA', 'TIPO DE INGRESO', 'RUT', 'RAZON SOCIAL', 'DIRECCION PROVEEDOR', 'SUCURSAL PROVEEDOR', 'DESCUENTO', 'IMPUESTO ESPECIFICO', 'IMPUESTOS', 'EXENTO', 'NETO', 'IVA', 'TASA IVA', 'IVA USO COMUN', 'IVA NO RECUPERABLE', 'IVA NO RETENIDO', 'IVA RETENIDO', 'TOTAL', 'OBSERVACION', 'MONTO ACTIVO FIJO', 'PAGADO', 'ENVIADOLIBRO', 'SUCURSAL', 'USUARIO', 'FECHA DE CREACION', 'NRO DOCUMENTO ANULADO', 'TIPO DOCUMENTO ANULADO', 'DECLARACION ACUSE MERCADERIA', 'EMISOR ACUSE', 'FECHA DE ACUSE', 'FECHA DE INGRESO AL LIBRO', 'FECHA DE ELIMINACION DEL LIBRO', 'USUARIO ELIMINACION'])
        )

        #print(problems)
        if problems.nrows() > 0:
            raise ValidationError("No tiene la estructura solicitada")

        return f

    def clean_currency(self):
        currency = self.cleaned_data.get("currency", False)
        operator = self.cleaned_data.get("operator", False)

        if currency in operator.currency.all():
            return currency
        else:
            raise ValidationError("El operador que selecciono no acepta esa moneda")

class PaymentForm(forms.ModelForm):
    paid_on = forms.DateField(label=_('paid on'), input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format='%Y-%m-%d'))
    deposited_on = forms.DateField(label=_('deposited on'), input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format='%Y-%m-%d'))
    class Meta:
        model = Payment
        fields = ['paid_on',
                  'deposited_on',
                  'amount',
                  'kind',
                  'operator',
                  'comment',
                  'proof',
                  'file',
                  #'currency',
                  'bank_account',
                  'tasa',
                  'number_document']
        labels = {
        "currency": "Moneda",
        "bank_account": "Cuenta Bancaria",
        "number_document": "Número de Documento"
        }
    
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(PaymentForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        banks=BankAccount.objects.none()
        for o in operators:
            banks= banks | BankAccount.objects.filter(operator=o)
        banks=banks.distinct()
        self.fields["bank_account"].queryset = banks
        # there's a `fields` property now
        self.fields['bank_account'].empty_label = None
        self.fields['bank_account'].required = True

class PaymentUpdateForm(forms.ModelForm):
    paid_on = forms.DateField(label=_('paid on'), input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format='%Y-%m-%d'))
    deposited_on = forms.DateField(label=_('deposited on'), input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format='%Y-%m-%d'))
    class Meta:
        model = Payment
        fields = ['provider',
                  'paid_on',
                  'deposited_on',
                  'amount',
                  'kind',
                  'operator',
                  'comment',
                  'proof',
                  'file',
                  #'currency',
                  'tasa',
                  'number_document',
                  "bank_account"]
        labels = {
        "currency": "Moneda",
        "bank_account": "Cuenta Bancaria",
        "number_document": "Número de Documento"
        }

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(PaymentUpdateForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        banks=BankAccount.objects.none()
        for o in operators:
            banks= banks | BankAccount.objects.filter(operator=o)
        banks=banks.distinct()
        self.fields["bank_account"].queryset = banks
        # there's a `fields` property now
        self.fields['bank_account'].empty_label = None
        self.fields['bank_account'].required = True

class ProviderForm(forms.ModelForm):
    rut = CLRutField()

    class Meta:
        model = Provider
        fields = ['rut',
                  'name',
                  'email',
                  'street',
                  'house_number',
                  'apartment_number',
                  'tower',
                  'location',
                  'phone',
                  'operator',
                  'notes',
                  'currency']
        widgets = {'location': apply_select2(forms.Select)}
        labels = {
        "currency": "Moneda predeterminada"
        }

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(ProviderForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
                #print(currency)
        currency=currency.distinct()
        self.fields["currency"].queryset = currency
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

    def clean_currency(self):
        currency = self.cleaned_data.get("currency", False)
        operator = self.cleaned_data.get("operator", False)

        if currency in operator.currency.all():
            return currency
        else:
            raise ValidationError("El operador que selecciono no acepta esa moneda")

class PaymentsExcelForm(forms.Form):
    excel_file = forms.FileField(label="Archivo Excel")
    operator = forms.ModelChoiceField(
        queryset=Operator.objects.all(), initial=1, label="Operador"
    )
    #bank = forms.ModelChoiceField(queryset=Bank.objects.all(), label="Banco")
    bank_account = forms.ModelChoiceField(
        queryset=BankAccount.objects.all(), label="Cuenta bancaria"
    )


    def clean_bank(self):
        bank = self.cleaned_data.get("bank", False)

        if bank.id != 1:
            raise ValidationError("Actualmente no existe soporte para este banco")
        return bank

    def clean_excel_file(self):
        f = self.cleaned_data.get("excel_file", False)

        # filetype = magic.from_buffer(f.read())
        # if not (b'CDFV2 Microsoft Excel' in filetype):
        #     raise ValidationError("No es un archivo Excel antiguo")

        # f.seek(0)

        # new code for old xls
        workbook = xlrd.open_workbook(file_contents=f.read())
        worksheet = workbook.sheet_by_index(0)

        table = []

        for rownum in range(worksheet.nrows):
            table.append([entry for entry in worksheet.row_values(rownum)])

        problems = etl.validate(
            table,
            [],
            (
                "Fecha",
                "ID Transferencia",
                "Rut Origen/Destino",
                "Banco Origen/Destino",
                "Cuenta Origen/Destino",
                "Monto $",
                "Estado",
                "Nombre Destino/Origen",
                #"Mensaje Destino"
                "Numero Documento",
            ),
        )

        #print(problems)
        if problems.nrows() > 0:
            raise ValidationError("No tiene la estructura solicitada")

        return table


PAYMENT_OPTIONS = (
    (1, _("transbank")),
    (2, _("cash")),
    (3, _("cheque")),
    (5, _("servipag")),
    (6, _("BCI deposit")),
    (7, _("manual transfer")),
    (8, _("webpay")),
    (9, _("other")),
    (10, "NO registrado en banco"),
    (11, "Condonación"),
    (13, "paypal"),
    (14, "multicaja"),
    (15, "caja vecina"),
)
