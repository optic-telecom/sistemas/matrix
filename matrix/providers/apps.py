from django.apps import AppConfig


class ProvidersConfig(AppConfig):
    name = 'providers'

    def ready(self):
        from . import signals
