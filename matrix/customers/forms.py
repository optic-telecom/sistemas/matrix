import operator
import magic
import petl as etl
import csv
import xlrd
from django.contrib.auth.models import Group
from io import StringIO
from django import forms
from django.db.models import Q
from django.conf import settings
from django.db.models import Value, Q, F, Max, Count, Sum, Min
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.contrib.auth import get_user_model
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext_lazy as _
from localflavor.cl.forms import CLRutField
from easy_select2 import apply_select2, Select2
from sentry_sdk import capture_message
from hr.models import Technician, Agent
from django.contrib.auth.models import Group, User
from .widgets import SplitJSONWidget
from django.contrib.auth.models import Group, User
from .models import (
    Customer,
    ExpenseType,
    Prospect,
    Promotions,
    Promo, 
    Call,
    CallQA,
    QAIndicator,
    QAMonitoring,
    QATracing,
    QAChannel,
    Service,
    Contact,
    Plan,
    Payment,
    Invoice,
    PayorRut,
    Email,
    PlanOrder,
    TagColor,
    Operator,
    CommercialActivity,
    TypeCommercialActivity,
    Bank,
    BankAccount,
    MainReason,
    Credit,
    Charge,
    CartolaMovement,
    CalendarioCortes,
    UserOperator,
    Company,
    UserCompany,
    GeneralExpense,
    Currency
)
from django.contrib.admin.widgets import (
    FilteredSelectMultiple,
    RelatedFieldWidgetWrapper,
    AdminSplitDateTime,
)
from django.contrib import admin
from django.contrib.admin import widgets

from django.utils import timezone
from constance import config
import requests


def get_typing(search):
    data_return = []
    try:   
        url = config.iris_server + config.iris_tipificacion.format(SEARCH=search)
        r = requests.get(url, headers={"Authorization": config.iris_tipificaciones_token}, timeout=20)
        response = r.json()
        if not isinstance(response, list):
            e = f'Error Response {response} get_typing'
            capture_message(e)
        else:
            return response


    except requests.exceptions.Timeout as err:
        e = f'Error Timeout {config.iris_tipificacion} get_typing'
        capture_message(e)
        print (err)
    except requests.exceptions.ConnectionError as err:
        e = f'Error ConnectionError {config.iris_tipificacion} get_typing'
        capture_message(e)
        print (err)

    return data_return


class CallFilterForm(forms.Form):
    SUBJECT_EMPTY = (("", "---------"),) + Call.SUBJECT_CHOICES
    agent = forms.ModelChoiceField(
        label=_("agent"), queryset=get_user_model().objects.filter(is_active=True)
    )
    subject = forms.ChoiceField(label=_("subject"), choices=SUBJECT_EMPTY)


class CallQAFilterForm(forms.Form):
    agent = forms.ModelChoiceField(label=_("agent"), queryset=Agent.objects.all())


class HTML5DateInput(forms.DateInput):
    input_type = "date"


class HTML5TimeInput(forms.TimeInput):
    input_type = "time"


class ProspectForm(forms.ModelForm):
    rut = CLRutField(required=False)

    class Meta:
        model = Prospect
        fields = [
            "rut",
            "name",
            "kind",
            "location",
            "address",
            "street",
            "house_number",
            "apartment_number",
            "tower",
            "phone",
            "email",
            "current_company",
            "notes",
        ]
        # widgets = {'address': SplitJSONWidget()}
        widgets = {"location": apply_select2(forms.Select)}


class CustomerUpdateForm(forms.ModelForm):
    commercial_activity = forms.ModelChoiceField(
        queryset=CommercialActivity.objects.all(), required=False
    )
    phone = forms.RegexField(regex=r'(^[+][5][6|1|7|8]\d{9,10})', error_messages = {'invalid': "Phone number must be entered in the format: '+xx999999999'. Up to 15 digits allowed."})
    
    class Meta:
        model = Customer
        fields = [
            "rut",
            "kind",
            "send_email",
            "send_snail_mail",
            "first_name",
            "last_name",
            "commercial_activity",
            "birth_date",
            "email",
            "type_of_document",
            "location",
            'id_pulso',
            'street_location_id',
            "phone",
            "unified_billing",
            "notes",
            "first_category_activity",
        ]
        widgets = {"location": apply_select2(forms.Select)}

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(CustomerUpdateForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserOperator.objects.get(user=user1).company
            try:
                comp=Company.objects.get(id=companies)
                # Chile 
                if comp.country_id==1:
                    self.fields["type_of_document"].choices = Customer.CHILE_CHOICES
                    self.fields["phone"].regex=r'(^[+][5][6]\d{9,10})'
                else:
                    self.fields["phone"].regex=r'(^[+][5][1|7|8]\d{9,10})'
                    self.fields["type_of_document"].choices = Customer.PERU_CHOICES
            except:
                pass
        self.fields['id_pulso'].required = True

    def clean(self):
        cleaned_data = super(CustomerUpdateForm, self).clean()
        type_doc = self.cleaned_data.get("type_of_document", None)
        rut_number =  self.cleaned_data.get("rut", None)
        if type_doc==1:
            ff = CLRutField()
            rut, verificador = ff._canonify(rut_number)
            if ff._algorithm(rut) == verificador:
                pass
            else:
                #raise ValidationError(ff.error_messages['checksum'])
                raise ValidationError("Valor de RUT no válido") 

        id_pulso = self.cleaned_data.get("id_pulso", None)
        if id_pulso==None or id_pulso=="":
            raise ValidationError("Necesita actualizar la dirección. \n Busque la dirección con los filtros de Region, Comuna, Calle, Nro de Calle y Nro de Casa o Dpto")
        else:
            return 
        
        return cleaned_data
 
class CustomerCreateForm(forms.ModelForm):
    #rut = CLRutField()
    auto_rut = forms.BooleanField(required=False)
    commercial_activity = forms.ModelChoiceField(
        queryset=CommercialActivity.objects.all(), required=False
    )
    # Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.
    phone = forms.RegexField(regex=r'(^[+][5][6|1|7|8]\d{9,10})', error_messages = {'invalid': "Phone number must be entered in the format: '+xx999999999'. Up to 15 digits allowed."})
    
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(CustomerCreateForm, self).__init__(*args, **kwargs)
        
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserOperator.objects.get(user=user1).company
            try:
                comp=Company.objects.get(id=companies)
                # Chile 
                if comp.country_id==1:
                    self.fields["type_of_document"].choices = Customer.CHILE_CHOICES
                    self.fields["phone"].regex=r'(^[+][5][6]\d{9,10})'
                else:
                    self.fields["phone"].regex=r'(^[+][5][1|7|8]\d{9,10})'
                    self.fields["type_of_document"].choices = Customer.PERU_CHOICES
            except:
                pass
        
        self.fields['id_pulso'].required = True

    def clean(self):
        cleaned_data = super(CustomerCreateForm, self).clean()
        type_doc = self.cleaned_data.get("type_of_document", None)
        rut_number =  self.cleaned_data.get("rut", None)
        if rut_number is None and "auto_rut" in cleaned_data:
            new_rut_as_int = Customer.objects.latest_rut_as_int() + 1
            ff = CLRutField()
            verifier_digit = ff._algorithm(str(new_rut_as_int))
            cleaned_data["rut"] = "-".join([str(new_rut_as_int), str(verifier_digit)])
            self.data = self.data.copy()
            self.data['rut'] = "-".join([str(new_rut_as_int), str(verifier_digit)])
            rut_number=cleaned_data["rut"]
        if type_doc==1:
            ff = CLRutField()
            rut, verificador = ff._canonify(rut_number)
            if ff._algorithm(rut) == verificador:
                pass
            else:
                #raise ValidationError(ff.error_messages['checksum'])
                raise ValidationError("Valor de RUT no válido") 
        
        id_pulso = self.cleaned_data.get("id_pulso", None)
        if id_pulso==None or id_pulso=="":
            raise ValidationError("Necesita actualizar la dirección. \n Busque la dirección con los filtros de Region, Comuna, Calle, Nro de Calle y Nro de Casa o Dpto")
        #print(cleaned_data)
        return cleaned_data

    class Meta:
        model = Customer
        fields = [
            "kind",
            "send_email",
            "send_snail_mail",
            "type_of_document",
            "rut",
            "auto_rut",
            "first_name",
            "last_name",
            "commercial_activity",
            "birth_date",
            "email",
            #"street",
            #"house_number",
            #"apartment_number",
            #"tower",
            "location",
            'id_pulso',
            'street_location_id',
            "phone",
            "unified_billing",
            "notes",
            "first_category_activity",
            "change_to_service",
        ]
        widgets = {"location": apply_select2(forms.Select)}


ContactFormSet = inlineformset_factory(
    Customer, Contact, fields=["kind", "name", "phone", "email"], max_num=3
)


class ServiceCreateForm(forms.ModelForm):

    customer_type = forms.ChoiceField(choices=Service.CUSTOMER_CHOICES)
    document_type = forms.ChoiceField(choices=Service.DOCUMENT_CHOICES)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(ServiceCreateForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
            if operators.count()==1:
                try:
                    self.fields["number"].initial = (
                        Service.objects.filter(number__lt=99000, operator=operators[0]).aggregate(Max("number"))[
                            "number__max"
                        ]
                        + 1
                    )
                except TypeError as e:
                    self.fields["number"].initial = 0
        self.fields["technician"].queryset = Technician.objects.filter(active=True)
        self.fields["plan"].queryset = Plan.objects.filter(active=True)

    def clean(self):            
        operator = self.cleaned_data['operator']
        number = self.cleaned_data.get("number", False)
        service=Service.objects.filter(number=number,operator=operator)
        if service.count()==0:
            return 
        else:
            raise ValidationError("El operador que selecciono ya tiene un servicio con ese número")
   
    class Meta:
        model = Service
        fields = [
            "customer_type",
            "document_type",
            "number",
            "street",
            "house_number",
            "apartment_number",
            "tower",
            "location",
            "plan",
            "node",
            "technology_kind",
            "price_override",
            "due_day",
            "activated_on",
            "installed_on",
            "uninstalled_on",
            "expired_on",
            "activation_fee",
            "billing_notes",
            "internal_notes",
            "seller",
            "technician",
            "allow_auto_cut",
            "operator",
            "notes",
            "unified_billing",
        ]
        widgets = {
            "seller": apply_select2(forms.Select),
            "technician": apply_select2(forms.Select),
            "node": apply_select2(forms.Select),
            "location": apply_select2(forms.Select),
        }


class ServiceUpdateForm(forms.ModelForm):

    customer_type = forms.ChoiceField(choices=Service.CUSTOMER_CHOICES)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        self.id = kwargs.pop('id')
        super(ServiceUpdateForm, self).__init__(*args, **kwargs)
        self.fields["technician"].queryset = Technician.objects.filter(active=True)
        active_plans = Plan.objects.filter(Q(active=True) | Q(id=self.instance.plan.pk))
        self.fields["plan"].queryset = active_plans
        if self.instance.status == Service.ACTIVE:
            try:
                group =  Group.objects.get(name="Coordinador de facturación")
                group1 =  Group.objects.get(name="Head Finanzas")
                if group in user.groups.all() or group1 in user.groups.all():
                    self.fields["status"].choices = Service.STATUS_CHOICES_IF_ACTIVE_SPECIAL
                else:
                    self.fields["status"].choices = Service.STATUS_CHOICES_IF_ACTIVE
            except Group.DoesNotExist:
                self.fields["status"].choices = Service.STATUS_CHOICES_IF_ACTIVE
        elif self.instance.status == Service.PENDING:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_PENDING
        elif self.instance.status == Service.INACTIVE:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_INACTIVE
        elif self.instance.status == Service.NOT_INSTALLED:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_NOT_INSTALLED
        elif self.instance.status == Service.FREE:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_FREE
        elif self.instance.status == Service.INSTALL_REJECTED:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_INSTALL_REJECTED
        elif self.instance.status == Service.DELINQUENT:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_DELINQUENT
        elif self.instance.status == Service.HOLDER_CHANGE:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_HOLDER_CHANGE
        elif self.instance.status == Service.DISCARDED:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_DISCARDED
        elif self.instance.status == Service.ON_HOLD:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_ON_HOLD
        elif self.instance.status == Service.LOST:
            self.fields["status"].choices = Service.STATUS_CHOICES_IF_LOST

    def clean(self):
        cleaned_data = super(ServiceUpdateForm, self).clean()
        former_status = self.instance.status
        new_status = cleaned_data["status"]
        was_not_installed = former_status == Service.NOT_INSTALLED
        status_changed = former_status != new_status
        technician_involved = was_not_installed and status_changed
        technician_unknown = cleaned_data.get("technician", None) is None
        
        if technician_involved and technician_unknown:
            
            raise forms.ValidationError(
                "Debe especificar un técnico si va a cambiar el estado."
            )
        operator = self.cleaned_data['operator']
        number = self.cleaned_data.get("number", False)
        service=Service.objects.filter(number=number,operator=operator).exclude(id=self.id)
        if service.count()!=0:
            raise ValidationError("El operador que selecciono ya tiene un servicio con ese número")
        return cleaned_data

    class Meta:
        model = Service
        fields = [
            "customer_type",
            "document_type",
            "number",
            #"street",
            #"house_number",
            #"apartment_number",
            #"tower",
            'id_pulso',
            'street_location_id',
            "location",
            "plan",
            "node",
            #"promos",
            "promotions",
            "price_override",
            "due_day",
            "activation_fee",
            "billing_notes",
            "internal_notes",
            "status",
            "seller",
            "technician",
            "allow_auto_cut",
            "operator",
            "notes",
            "tags",
            "unified_billing",
        ]
        widgets = {
            "seller": apply_select2(forms.Select),
            "technician": apply_select2(forms.Select),
            "location": apply_select2(forms.Select),
            "node": apply_select2(forms.Select),
        }


class ServiceDatesForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = ["activated_on", "installed_on", "uninstalled_on", "expired_on"]


class ServiceNetworkForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = [
            "node",
            "original_node",
            "technology_kind",
            "mac_onu",
            "ssid",
            "password",
            "network_notes",
        ]
        widgets = {
            "node": apply_select2(forms.Select),
            "original_node": apply_select2(forms.Select),
        }


class MainReasonCreateForm(forms.ModelForm):
    class Meta:
        model = MainReason
        fields = [
            "reason",
            "valor",
            "kind",
            "operator",
            "calculate_by_days",
            "percent",
            "active",
            "uf",
        ]


class MainReasonUpdateForm(forms.ModelForm):
    class Meta:
        model = MainReason
        fields = [
            "reason",
            "valor",
            "kind",
            "calculate_by_days",
            "percent",
            "active",
            "uf",
        ]


class CreateCreditForm(forms.Form):
    reason = forms.ModelChoiceField(
        label="Motivo",
        queryset=MainReason.objects.filter(status_field=False, kind=1).exclude(kind=3),
        initial=1,
    )
    number_billing = forms.IntegerField(
        label="Número de facturas a aplicar", min_value=1, required=False
    )
    days_count = forms.IntegerField(
        label="Número de días sin servicio", min_value=1, required=False
    )
    permanent = forms.BooleanField(label="Fijo", required=False)


class CreateChargeForm(forms.Form):
    reason = forms.ModelChoiceField(
        label="Motivo",
        queryset=MainReason.objects.filter(status_field=False, kind=2).exclude(kind=3),
        initial=1,
    )
    number_billing = forms.IntegerField(
        label="Número de facturas a aplicar", min_value=1, required=False
    )
    quantity = forms.IntegerField(label="Cantidad", min_value=1, required=False)
    days_count = forms.IntegerField(
        label="Número de días sin servicio", min_value=1, required=False
    )
    permanent = forms.BooleanField(label="Fijo", required=False)


class CreditUpdateForm(forms.Form):
    reason = forms.ModelChoiceField(
        label="Motivo",
        queryset=MainReason.objects.filter(status_field=False, kind=1).exclude(kind=3),
        initial=1,
    )
    number_billing = forms.IntegerField(
        label="Número de facturas a aplicar", min_value=1, required=False
    )
    days_count = forms.IntegerField(
        label="Número de días sin servicio", min_value=1, required=False
    )
    permanent = forms.BooleanField(label="Fijo", required=False)


class ChargeUpdateForm(forms.Form):
    reason = forms.ModelChoiceField(
        label="Motivo",
        queryset=MainReason.objects.filter(status_field=False, kind=2).exclude(kind=3),
        initial=1,
    )
    number_billing = forms.IntegerField(
        label="Número de facturas a aplicar", min_value=1, required=False
    )
    quantity = forms.IntegerField(label="Cantidad", min_value=1, required=False)
    days_count = forms.IntegerField(
        label="Número de días sin servicio", min_value=1, required=False
    )
    permanent = forms.BooleanField(label="Fijo", required=False)


class SearchNumberForm(forms.Form):
    number = forms.IntegerField(label="Número de servicio", min_value=0)


class ManualPaymentForm(forms.Form):
    amount = forms.IntegerField(label="Monto", min_value=1)
    description = forms.CharField(label="Descripción", max_length=75)


class MassPaymentForm(forms.Form):
    excel_file = forms.FileField(label="Archivo Excel")


class PlanForm(forms.ModelForm):
    class Meta:
        model = Plan
        fields = ["name", "description_facturacion","price", "type_plan", 
                  "category", "operator", "uf","currency"]
        labels = {
        "type_plan": "Tipo",
        "currency": "Moneda"
        }
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(PlanForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
                #print(currency)
        currency=currency.distinct()
        self.fields["currency"].queryset = currency
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

class EmailForm(forms.ModelForm):
    class Meta:
        model = Email
        fields = ["subject", "body"]


class CallForm(forms.ModelForm):
    SUBJECT_EMPTY = (("", "---------"),) + Call.SUBJECT_CHOICES
    subject = forms.ChoiceField(label=_("subject"), choices=SUBJECT_EMPTY)

    def __init__(self, *args, **kwargs):
        super(CallForm, self).__init__(*args, **kwargs)
        self.base_fields["subject"].initial = ""

    class Meta:
        model = Call
        fields = ["subject", "notes", "phone_number", "recording", "qa_comment"]


class CallQAForm(forms.ModelForm):
    SERVICE_HIRING_EMPTY = (("", "N/A"),) + CallQA.SERVICE_HIRING_CHOICES
    call_date = forms.DateField(
        label="Fecha de contacto",
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=HTML5DateInput(format="%Y-%m-%d"),
    )
    call_time = forms.TimeField(
        label="Hora de contacto", widget=HTML5TimeInput(format="%H:%M")
    )
    customer_phone = forms.CharField(
        label="Número de teléfono del Cliente",
        max_length=11,
        required=False,
        validators=[
            RegexValidator(
                "^(56)?(9|2)[0-9]{8}$",
                message="Formato permitido 569######## ó 562########",
            )
        ],
    )
    service = forms.ModelMultipleChoiceField(
        label=_("Service Number"), queryset=Service.objects.all(), required=False
    )
    service_hiring = forms.ChoiceField(
        label=_("Service Hiring"), choices=SERVICE_HIRING_EMPTY, required=False
    )
    indicators = forms.ModelMultipleChoiceField(
        queryset=QAIndicator.objects.all(), required=False
    )
    address = forms.CharField(label="Dirección")
    channel = forms.ModelChoiceField(label="Canal", queryset=QAChannel.objects.all(), required=False)
    
    def __init__(self, *args, **kwargs):
        super(CallQAForm, self).__init__(*args, **kwargs)
        self.base_fields["typing"].initial = ""
        self.base_fields["category"].initial = ""
        self.base_fields["subcategory"].initial = ""
        SERVICE_HIRING_EMPTY = (("", "N/A"),) + CallQA.SERVICE_HIRING_CHOICES
        TYPING_EMPTY = (("", "---------"),)
        for record in get_typing("classification=1"):
            if record["operator"] == 2:
                TYPING_EMPTY = TYPING_EMPTY + ((record["ID"], record["name"]),)
        TYPING_EMPTY = sorted(TYPING_EMPTY, key=lambda tup: tup[1])
        CATEGORY_EMPTY = (("", "---------"),)
        for record in get_typing("classification=2"):
            if record["operator"] == 2:
                CATEGORY_EMPTY = CATEGORY_EMPTY + ((record["ID"], record["name"]),)
        SUBCATEGORY_EMPTY = (("", "---------"),)
        for record in get_typing("classification=3"):
            if record["operator"] == 2:
                SUBCATEGORY_EMPTY = SUBCATEGORY_EMPTY + ((record["ID"], record["name"]),)
        
        self.fields["typing"] = forms.ChoiceField(label=_("Typing"), choices=TYPING_EMPTY)
        self.fields["category"]  = forms.ChoiceField(label=_("Category"), choices=CATEGORY_EMPTY)
        self.fields["subcategory"] = forms.ChoiceField(label=_("Subcategory"), choices=SUBCATEGORY_EMPTY)
        
    class Meta:
        model = CallQA
        fields = [
            "call_date",
            "call_time",
            "agent",
            "customer_phone",
            "rut",
            "service",
            "service_hiring",
            "typing",
            "category",
            "subcategory",
            "indicators",
            "qa_comment",
            "address",
            "email",
            "indicators_apply",
            "kind",
            "channel",
        ]


class QAIndicatorForm(forms.ModelForm):
    parent = forms.ModelChoiceField(
        label=_("Parent"),
        queryset=QAIndicator.objects.all().filter(parent=None),
        required=False,
    )

    class Meta:
        model = QAIndicator
        fields = ["description", "parent"]


class QAMonitoringForm(forms.ModelForm):
    date = forms.DateField(
        label=_("Date"),
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=HTML5DateInput(format="%Y-%m-%d"),
    )
    phone = forms.CharField(
        label="Número de teléfono del Cliente",
        max_length=11,
        validators=[
            RegexValidator(
                "^(56)?(9|2)[0-9]{8}$",
                message="Formato permitido 569######## ó 562########",
            )
        ],
    )
    service = forms.ModelChoiceField(
        label=_("Service Number"), queryset=Service.objects.all(), required=False
    )

    def __init__(self, *args, **kwargs):
        super(QAMonitoringForm, self).__init__(*args, **kwargs)
        SUBCATEGORY_EMPTY = (("", "---------"),)
        for record in get_typing("classification=3"):
            if record["operator"] == 2:
                SUBCATEGORY_EMPTY = SUBCATEGORY_EMPTY + ((record["ID"], record["name"]),)
        SUBCATEGORY_EMPTY = sorted(SUBCATEGORY_EMPTY, key=lambda tup: tup[1])

        self.fields["subcategory"] = forms.ChoiceField(label=_("Subcategory"), choices=SUBCATEGORY_EMPTY)

    class Meta:
        model = QAMonitoring
        fields = [
            "date",
            "agent",
            "rut",
            "reason",
            "address",
            "phone",
            "comment",
            "service",
            "email",
            "subcategory",
        ]



class QATracingForm(forms.ModelForm):
    class Meta:
        model = QATracing
        fields = ["solved", "reason", "comment", "monitoring"]


class QAChannelForm(forms.ModelForm):
    class Meta:
        model = QAChannel
        fields = ["name"]


class BillingExcelForm(forms.Form):
    excel_file = forms.FileField(label="Archivo Excel")
    operator = forms.ModelChoiceField(
        label="Operador", queryset=Operator.objects.all(), initial=1
    )

    def clean_excel_file(self):
        f = self.cleaned_data.get("excel_file", False)

        # Comment this if not work 
        filetype = magic.from_buffer(f.read())
        if not ("Microsoft Excel 2007+" in filetype or "Microsoft OOXML" in filetype):
            raise ValidationError("No es un archivo Excel")

        f.seek(0)
        raw_table = etl.fromxlsx(f)

        workbook = xlrd.open_workbook(file_contents=f.read())
        worksheet = workbook.sheet_by_index(0)

        table = []

        for rownum in range(worksheet.nrows):
            table.append([entry for entry in worksheet.row_values(rownum)])

        problems = etl.validate(
            table, [], ("DOCUMENTO", "FOLIO", "FECHA", "RUT", "RAZON SOCIAL", "TOTAL")
        )
        print(problems)
        if problems.nrows() > 0:
            raise ValidationError("No tiene la estructura solicitada")

        return f


class PaymentsExcelForm(forms.Form):
    excel_file = forms.FileField(label="Archivo Excel")
    operator = forms.ModelChoiceField(
        queryset=Operator.objects.all(), initial=1, label="Operador"
    )
    bank = forms.ModelChoiceField(queryset=Bank.objects.all(), label="Banco")
    bank_account = forms.ModelChoiceField(
        queryset=BankAccount.objects.all(), label="Cuenta bancaria"
    )

    def clean_bank(self):
        bank = self.cleaned_data.get("bank", False)

        if bank.id != 1:
            raise ValidationError("Actualmente no existe soporte para este banco")
        return bank

    def clean_excel_file(self):
        f = self.cleaned_data.get("excel_file", False)

        # filetype = magic.from_buffer(f.read())
        # if not (b'CDFV2 Microsoft Excel' in filetype):
        #     raise ValidationError("No es un archivo Excel antiguo")

        # f.seek(0)

        # new code for old xls
        workbook = xlrd.open_workbook(file_contents=f.read())
        worksheet = workbook.sheet_by_index(0)

        table = []

        for rownum in range(worksheet.nrows):
            table.append([entry for entry in worksheet.row_values(rownum)])

        problems = etl.validate(
            table,
            [],
            (
                "Fecha",
                "ID Transferencia",
                "Rut Origen/Destino",
                "Banco Origen/Destino",
                "Cuenta Origen/Destino",
                "Monto $",
                "Estado",
                "Nombre Destino/Origen",
                "Numero Documento",
            ),
        )

        if problems.nrows() > 0:
            raise ValidationError("No tiene la estructura solicitada")

        return table


PAYMENT_OPTIONS = (
    (1, _("transbank")),
    (2, _("cash")),
    (3, _("cheque")),
    (5, _("servipag")),
    (6, _("BCI deposit")),
    (7, _("manual transfer")),
    (8, _("webpay")),
    (9, _("other")),
    (10, "NO registrado en banco"),
    (11, "Condonación"),
    (13, "paypal"),
    (14, "multicaja"),
    (15, "caja vecina"),
)


class PaymentCreateForm(forms.ModelForm):

    kind = forms.ChoiceField(choices=PAYMENT_OPTIONS)
    bank_account = forms.ModelChoiceField(
        queryset=BankAccount.objects.all(), required=False, label=_("Bank account")
    )

    def __init__(self, *args, **kwargs):
        super(PaymentCreateForm, self).__init__(*args, **kwargs)
        limited_choices = filter(
            lambda c: c[0] != Payment.MASS_TRANSFER, Payment.PAYMENT_OPTIONS
        )
        self.fields["kind"] = forms.ChoiceField(
            choices=limited_choices, label=_("Kind")
        )
        self.fields['tasa'].widget.attrs['min'] = 0

    def clean_paid_on(self):
        paid_on = self.cleaned_data["paid_on"]
        now = timezone.now()
        if paid_on > now:
            raise ValidationError(
                "La fecha de pago no puede ser mayor a la fecha de hoy"
            )
        return paid_on

    class Meta:
        model = Payment
        fields = [
            "paid_on",
            "deposited_on",
            "amount",
            "kind",
            "operation_number",
            "proof",
            "proof2",
            "proof3",
            "proof4",
            "comment",
            "operator",
            "bank_account",
            "service",
            "invoice",
            "number_document",
            "tasa"
        ]
        widgets = {
            "paid_on": HTML5DateInput,
            "deposited_on": HTML5DateInput,
        }


class PaymentUpdateForm(forms.ModelForm):

    bank_account = forms.ModelChoiceField(
        queryset=BankAccount.objects.all(), required=False
    )

    class Meta:
        model = Payment
        fields = [
            "paid_on",
            "deposited_on",
            "proof",
            "proof2",
            "proof3",
            "proof4",
            "amount",
            "kind",
            "operation_number",
            "comment",
            "operator",
            "bank_account",
            "service",
            "customer",
            "invoice",
            "number_document",
            "tasa"
        ]

    def __init__(self, *args, **kwargs):
        super(PaymentUpdateForm, self).__init__(*args, **kwargs)
        self.fields['tasa'].widget.attrs['min'] = 0

INVOICE_CHOICES = (
    (1, "Boleta"),
    (2, "Factura"),
    (4, "Ajuste por cargo"),
    (5, "Devolución"),
    (9, "Ajuste incobrable"),
)


class InvoiceCreateForm(forms.ModelForm):
    kind = forms.ChoiceField(choices=INVOICE_CHOICES)
    operator = forms.ModelChoiceField(
        label="Operador", queryset=Operator.objects.all(), initial=1
    )

    class Meta:
        model = Invoice
        fields = [
            "due_date",
            "paid_on",
            "total",
            "comment",
            "file",
            "operator",
            "service",
            "credit",
            "charge",
            "currency",
            "tasa"]
        labels = {
        "type_plan": "Tipo",
        "currency": "Moneda"
        }
        widgets = {
            "due_date": HTML5DateInput,
            "paid_on": HTML5DateInput,
        }

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(InvoiceCreateForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
                #print(currency)
        currency=currency.distinct()
        self.fields["currency"].queryset = currency
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

    def clean_currency(self):
        currency = self.cleaned_data.get("currency", False)
        operator = self.cleaned_data.get("operator", False)

        if currency in operator.currency.all():
            return currency
        else:
            raise ValidationError("El operador que selecciono no acepta esa moneda")

class InvoiceNotaVentaCreateForm(forms.ModelForm):
    paid_on = forms.DateTimeField(
        label="pagado at", widget=HTML5DateInput, required=False
    )
    operator = forms.ModelChoiceField(
        label="Operador", queryset=Operator.objects.all(), initial=1
    )

    class Meta:
        model = Invoice
        fields = [
            "kind",
            "due_date",
            "paid_on",
            "total",
            "comment",
            "file",
            "operator",
            "service",
            "credit",
            "charge",
            "currency",
            "tasa"]
        labels = {
        "type_plan": "Tipo",
        "currency": "Moneda"
        }
        widgets = {
            "due_date": HTML5DateInput,
            "paid_on": HTML5DateInput,
        }

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(InvoiceNotaVentaCreateForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
                #print(currency)
        currency=currency.distinct()
        self.fields["currency"].queryset = currency
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

    def clean_currency(self):
        currency = self.cleaned_data.get("currency", False)
        operator = self.cleaned_data.get("operator", False)

        if currency in operator.currency.all():
            return currency
        else:
            raise ValidationError("El operador que selecciono no acepta esa moneda")

class MultipleIntegersField(forms.TypedMultipleChoiceField):
    def __init__(self, *args, **kwargs):
        super(MultipleIntegersField, self).__init__(*args, **kwargs)
        self.coerce = int

    def valid_value(self, value):
        return True


class SearchForm(forms.Form):
    shop = MultipleIntegersField()


class InvoiceNotaCreditoCreateForm(forms.ModelForm):
    CODE_CHOICES = (
        (1, "Anular documento de referencia"),
        (2, "Corregir texto"),
        (3, "Corregir monto"),
    )

    """
    ATTR_CHOICES = ((1, 'Giro'),
                    (2, 'Nombre'),
                    (3,'Dirección'))"""
    code = forms.ChoiceField(
        label=_("Motivo de la nota de crédito"), choices=CODE_CHOICES
    )
    # fix_attr = forms.ChoiceField(label=_('Campo a modificar'), choices=ATTR_CHOICES)
    previous_text = forms.CharField(label=_("Texto a modificar"), required=False)
    paid_on = forms.DateTimeField(
        label="pagado at", widget=HTML5DateInput, required=False
    )
    new_text = forms.CharField(label=_("Nuevo texto"), required=False)
    # quantity=forms.MultiValueField(label='Cantidad del servicio',  min_value=1, required=False)
    # price=forms.IntegerField(label='Precio del servicio', min_value=1, required=False)
    # operator = forms.ModelChoiceField(label='Operador',queryset=Operator.objects.all(), initial=1, required=False)
    # folio = forms.IntegerField(required=False)

    class Meta:
        model = Invoice
        fields = ["kind", "paid_on", "comment", "file", "total", "service", "currency"]
        widgets = {
            "paid_on": HTML5DateInput,
        }
        labels = {
        "currency": "Moneda"
        }
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(InvoiceNotaCreditoCreateForm, self).__init__(*args, **kwargs)
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

class NotaCreditoForm(forms.Form):
    planCode = forms.IntegerField(label="Codigo del plan", min_value=1, required=False)
    quantity = forms.IntegerField(
        label="Cantidad del servicio", min_value=1, required=False
    )
    price = forms.DecimalField(
        label="Precio del servicio", min_value=1, required=False, decimal_places=2
    )


class InvoiceNotaCobroCreateForm(forms.ModelForm):
    paid_on = forms.DateTimeField(
        label="pagado at", widget=HTML5DateInput, required=False
    )
    # operator = forms.ModelChoiceField(label='Operador',queryset=Operator.objects.all(), initial=1, required=False)
    # folio = forms.IntegerField(required=False)
    class Meta:
        model = Invoice
        fields = ["kind", "paid_on", "total", "comment", "file", "service", "currency",
            "tasa"]
        labels = {
        "type_plan": "Tipo",
        "currency": "Moneda"
        }
        widgets = {
            "due_date": HTML5DateInput,
            "paid_on": HTML5DateInput,
        }

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(InvoiceNotaCobroCreateForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
                #print(currency)
        currency=currency.distinct()
        self.fields["currency"].queryset = currency
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

class InvoiceNotaDebitoCreateForm(forms.ModelForm):
    paid_on = forms.DateTimeField(
        label="pagado at", widget=HTML5DateInput, required=False
    )

    class Meta:
        model = Invoice
        fields = ["kind", "due_date", "paid_on", "total", "comment", "file", "service","currency",
            "tasa"]
        labels = {
        "type_plan": "Tipo",
        "currency": "Moneda"
        }
        widgets = {
            "due_date": HTML5DateInput,
            "paid_on": HTML5DateInput,
        }

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        user = kwargs.pop('user')
        super(InvoiceNotaDebitoCreateForm, self).__init__(*args, **kwargs)
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
                #print(currency)
        currency=currency.distinct()
        self.fields["currency"].queryset = currency
        # there's a `fields` property now
        self.fields['currency'].empty_label = None
        self.fields['currency'].required = True

class InvoiceUpdateForm(forms.ModelForm):
    payment = forms.ModelMultipleChoiceField(
        label=_("Pago asociado"), queryset=Payment.objects.all(), required=False
    )
    class Meta:
        model = Invoice
        fields = ["comment", "operator", "service", "customer", "pdf_date", "currency", "tasa"]
        labels = {
        "currency": "Moneda"
        }
    
    def clean_currency(self):
        currency = self.cleaned_data.get("currency", False)
        operator = self.cleaned_data.get("operator", False)

        if currency in operator.currency.all():
            return currency
        else:
            raise ValidationError("El operador que selecciono no acepta esa moneda")

        
class InvoiceNoteUpdateForm(forms.ModelForm):
    payment = forms.ModelMultipleChoiceField(
        label=_("Pago asociado"), queryset=Payment.objects.all(), required=False
    )
    class Meta:
        model = Invoice
        fields = ["paid_on", "comment", "operator", "service", "customer", "currency"]
        widgets = {
            "paid_on": HTML5DateInput,
        }
        labels = {
        "currency": "Moneda"
        }

    def clean_currency(self):
        currency = self.cleaned_data.get("currency", False)
        operator = self.cleaned_data.get("operator", False)

        if currency in operator.currency.all():
            return currency
        else:
            raise ValidationError("El operador que selecciono no acepta esa moneda")


class PayorRutForm(forms.ModelForm):
    rut = CLRutField()

    class Meta:
        model = PayorRut
        fields = ["rut", "name"]


class InvoiceAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(InvoiceAdminForm, self).__init__(*args, **kwargs)
        self.fields["json"].required = False

    class Meta:
        widgets = {
            "customer": apply_select2(forms.Select),
        }


def add_related_field_wrapper(form, col_name):
    rel_model = form.Meta.model
    rel = rel_model._meta.get_field(col_name).rel
    form.fields[col_name].widget = RelatedFieldWidgetWrapper(
        form.fields[col_name].widget,
        rel,
        admin.site,
        can_add_related=True,
        can_change_related=True,
    )


class PaymentAdminForm(forms.ModelForm):
    bank_account = forms.ModelChoiceField(
        queryset=BankAccount.objects.all(), required=False
    )

    def __init__(self, *args, **kwargs):
        super(PaymentAdminForm, self).__init__(*args, **kwargs)
        add_related_field_wrapper(self, "bank_account")

    class Meta:
        widgets = {
            "customer": apply_select2(forms.Select),
        }


class TechnicalVisitForm(forms.Form):
    visit_details = forms.CharField(label=_("Detalles de Visita"))


class ServiceAnnexForm(forms.Form):
    notes = forms.CharField(label=_("notes"), widget=forms.Textarea)


class ServiceTermForm(forms.Form):
    equipment_removal = forms.CharField(label=_("Equipos por retirar"))
    additional_terms = forms.CharField(label=_("Motivo de baja de servicio"))


class EquipmentRemovalForm(forms.Form):
    person = forms.CharField(label=_("Persona encargada de la entrega"))
    status = forms.CharField(label=_("Condiciones del equipo a retirar"))
    pick_up_date = forms.CharField(label=_("Fecha de retiro"))
    pick_up_time = forms.CharField(label=_("Hora de retiro"))


class PlanServiceForm(forms.Form):
    pass


class DiffForm(forms.Form):
    beginning = forms.DateField(label=_("begining"))
    end = forms.DateField(label=_("end"))
    transfers = forms.FileField(label=_("transfers"))


class PlanOrderCreateForm(forms.ModelForm):
    class Meta:
        model = PlanOrder
        fields = ["kind", "subject"]


class PlanOrderDeleteForm(forms.Form):
    SUBJECT_CHOICES = (
        (
            "Cierre Matrix - Cliente rechaza inst",
            "Cierre Matrix - Cliente rechaza inst",
        ),
        (
            "Cierre Matrix - Ausencia Laboral Tecnico",
            "Cierre Matrix - Ausencia Laboral Tecnico",
        ),
        ("Cierre Matrix - Prueba Interna", "Cierre Matrix - Prueba Interna"),
        ("Cierre Matrix - Otro", "Cierre Matrix - Otro"),
    )

    subject = forms.ChoiceField(label=_("Motivo de cierre"), choices=SUBJECT_CHOICES)


class PlanOrderScheduleForm(forms.Form):
    start = forms.CharField(label="", widget=forms.HiddenInput())
    end = forms.CharField(label="", widget=forms.HiddenInput())

    def __init__(self, choices, *args, **kwargs):
        super(PlanOrderScheduleForm, self).__init__(*args, **kwargs)


class TagColorForm(forms.ModelForm):
    class Meta:
        model = TagColor
        fields = "__all__"
        widgets = {
            "html_color": forms.widgets.TextInput(attrs={"type": "color"}),
        }


class TagCreateForm(forms.Form):
    tag_name = forms.CharField(
        label="etiqueta",
        widget=forms.TextInput(attrs={"list": "tag-list", "autocomplete": "off"}),
    )


class CommercialActivityAdminForm(forms.ModelForm):
    type = forms.ModelChoiceField(
        queryset=TypeCommercialActivity.objects.all(), required=False
    )

    class Meta:
        model = CommercialActivity
        fields = ["name", "code", "type"]


class BankAdminForm(forms.ModelForm):
    code = forms.IntegerField(required=False)

    class Meta:
        model = Bank
        fields = ["name", "code"]


class OperatorAdminForm(forms.ModelForm):
    code = forms.CharField(required=False)

    class Meta:
        model = Operator
        fields = ["name", "code", "company", "currency"]


class CreditAdminForm(forms.ModelForm):
    reason = forms.ModelChoiceField(
        label="Motivo",
        queryset=MainReason.objects.filter(status_field=False, kind=1).exclude(kind=3),
        initial=1,
    )
    number_billing = forms.IntegerField(
        label="Número de facturas a aplicar", min_value=1
    )
    days_count = forms.IntegerField(
        label="Número de días sin servicio", min_value=1, required=False
    )
    cleared_at = forms.SplitDateTimeField(
        label="Cleared at", widget=AdminSplitDateTime(), localize=True, required=False
    )
    permanent = forms.BooleanField(label="Fijo", required=False)
    class Meta:
        model = Credit
        fields = ["service", "times_applied", "status_field", "active"]


class ChargeAdminForm(forms.ModelForm):
    reason = forms.ModelChoiceField(
        label="Motivo",
        queryset=MainReason.objects.filter(status_field=False, kind=2).exclude(kind=3),
        initial=1,
    )
    number_billing = forms.IntegerField(
        label="Número de facturas a aplicar", min_value=1
    )
    quantity = forms.IntegerField(label="Cantidad", min_value=1, required=False)
    cleared_at = forms.SplitDateTimeField(
        label="Cleared at", widget=AdminSplitDateTime(), localize=True, required=False
    )
    days_count = forms.IntegerField(
        label="Número de días sin servicio", min_value=1, required=False
    )
    permanent = forms.BooleanField(label="Fijo", required=False)

    class Meta:
        model = Charge
        fields = ["service", "times_applied", "status_field"]


class BankAccountModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
         return str(obj.operator) +" - "+ str(obj.tag) 

class CartolaLoadForm(forms.Form):
    number = forms.IntegerField(label="Numero de Cartola")
    start = forms.DateField(
        label="Fecha inicio de periodo",
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=HTML5DateInput(format="%Y-%m-%d"),
    )
    end = forms.DateField(
        label="Fecha final de periodo",
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=HTML5DateInput(format="%Y-%m-%d"),
    )
    saldo_inicial=forms.DecimalField(label="Saldo inicial")
    saldo_final=forms.DecimalField(label="Saldo final")  
    total_cargo=forms.DecimalField(label="Total Cargos y Cheques", min_value=0, required=True)
    total_abono=forms.DecimalField(label="Total Abonos y Depositos", min_value=0, required=True)
    bank_account = BankAccountModelChoiceField(
        queryset=BankAccount.objects.all(), label="Cuenta bancaria"
    )
    excel_file = forms.FileField(label="Archivo Excel")
    pdf_file = forms.FileField(label="Archivo PDF, Cartola del banco")

    def clean_excel_file(self):
        f = self.cleaned_data.get("excel_file", False)

        # filetype = magic.from_buffer(f.read())
        # if not (b'CDFV2 Microsoft Excel' in filetype):
        #     raise ValidationError("No es un archivo Excel antiguo")

        # f.seek(0)

        # new code for old xls
        workbook = xlrd.open_workbook(file_contents=f.read())
        worksheet = workbook.sheet_by_index(0)

        table = []

        for rownum in range(worksheet.nrows):
            table.append([entry for entry in worksheet.row_values(rownum)])

        problems = etl.validate(
            table,
            [],
            (
                "Fecha",
                "Sucursal",
                "Descripcion",
                "Num. Documento",
                "Cargo",
                "Abono",
                "Saldo diario"
            ),
        )

        if problems.nrows() > 0:
            print(problems)
            raise ValidationError("No tiene la estructura solicitada")

        return table, f


class CartolaMovementUpdateForm(forms.ModelForm):

    class Meta:
        model = CartolaMovement
        fields = [
            "payment", "payment_provider"
        ]

class PromotionUpdateForm(forms.ModelForm):

    class Meta:
        model = Promotions
        fields = [
            "name", "description", "initial_date", "end_date"
        ]

class PromoCreateForm(forms.Form):
    operator = forms.ModelChoiceField(
        label="Operador", queryset=Operator.objects.all(), initial=1
    )
    name = forms.CharField(label="nombre", max_length=255)
    description = forms.CharField(label="Descripción",max_length=300)
    plan = forms.ModelMultipleChoiceField(
        queryset=Plan.objects.all(),
        widget=forms.CheckboxSelectMultiple, required=False
    )
    number_periods = forms.IntegerField(label="Cantidad de periodos", min_value=0, required=True)
    initial_date = forms.DateField(
        label="Fecha inicio de periodo",
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=HTML5DateInput(format="%d/%m/%Y"),
    )
    end_date = forms.DateField(
        label="Fecha inicio de periodo",
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=HTML5DateInput(format="%d/%m/%Y"),
        required=False
    )
    permanent = forms.BooleanField(label="Permanente", required=False)

    def __init__(self, *args, **kwargs):

        extra_fields = kwargs.pop('extra', 0)

        # check if extra_fields exist. If they don't exist assign 0 to them
        if not extra_fields:
            extra_fields = 1

        super(PromoCreateForm, self).__init__(*args, **kwargs)
        self.fields['number_periods'].initial = extra_fields

        for index in range(int(extra_fields)):
            # generate extra fields in the number specified via extra_fields
            self.fields['tipo_descuento{index}'.format(index=index+1)] = forms.CharField()
            self.fields['valor_descuento{index}'.format(index=index+1)] = forms.CharField()

class CalendarioCortesCreateForm(forms.ModelForm):
    billing_date = forms.DateField( label="Fecha de contacto", input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format="%Y-%m-%d") )

    def clean(self):
        cleaned_data = super(CalendarioCortesCreateForm, self).clean()
        billing_date = self.data['billing_date']
        if not billing_date:
            self._errors['billing_date'] = ['Debe seleccionar una fecha']
            return self.cleaned_data
        else:
            billing_date = billing_date.split('-')

        dates = CalendarioCortes.objects.all()
        for x in dates:
            if int(billing_date[1]) == x.billing_date.month and int(billing_date[0]) == x.billing_date.year:
                self._errors['billing_date'] = ['Ya existe una fecha con este mes y este año']
        return self.cleaned_data

    class Meta:
        model = CalendarioCortes
        fields = [ "billing_date", ]

class GeneralExpenseCreateForm(forms.ModelForm):
    issue_date = forms.DateField( label="Fecha de Emisión", input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format="%Y-%m-%d") )
    type = forms.ModelChoiceField(
        label=_("Tipo de Gasto"), queryset=ExpenseType.objects.all(), required=False
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(GeneralExpenseCreateForm, self).__init__(*args, **kwargs)
        username = self.user.username
        user=User.objects.get(username=username)
        
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                self.fields["operator"].queryset=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            else:
                self.fields["operator"].queryset = Operator.objects.filter(id=op.operator)
                self.fields["currency"].queryset = Operator.objects.get(id=op.operator).currency.all()
        except:
            pass

    def clean(self):
        cleaned_data = super(GeneralExpenseCreateForm, self).clean()
        currency_id = self.data["currency"]
        operator_id = self.data["operator"]
        operator = Operator.objects.get(id=int(operator_id))

        for currency in operator.currency.all():
            if currency.pk == int(currency_id):
                return self.cleaned_data
        
        self._errors["currency"] = ["Operador seleccionado no trabaja con esta moneda"]
        return self.cleaned_data


    class Meta:
        model = GeneralExpense
        fields = [
            "type",
            "description",
            "issue_date",
            "amount",
            "currency",
            "operator",
            "bank_account",
        ]

class GeneralExpenseUpdateForm(forms.ModelForm):
    issue_date = forms.DateField( label="Fecha de Emisión", input_formats=settings.DATE_INPUT_FORMATS, widget=HTML5DateInput(format="%Y-%m-%d") )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(GeneralExpenseUpdateForm, self).__init__(*args, **kwargs)
        username = self.user.username
        user=User.objects.get(username=username)
        
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                self.fields["operator"].queryset=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            else:
                self.fields["operator"].queryset = Operator.objects.filter(id=op.operator)
                self.fields["currency"].queryset = Operator.objects.get(id=op.operator).currency.all()
        except:
            pass

    def clean(self):
        cleaned_data = super(GeneralExpenseUpdateForm, self).clean()
        currency_id = self.data["currency"]
        operator_id = self.data["operator"]
        operator = Operator.objects.get(id=int(operator_id))

        for currency in operator.currency.all():
            if currency.pk == int(currency_id):
                return self.cleaned_data
        
        self._errors["currency"] = ["Operador seleccionado no trabaja con esta moneda"]
        return self.cleaned_data

    class Meta:
        model = GeneralExpense
        fields = [
            "type",
            "description",
            "issue_date",
            "amount",
            "currency",
            "operator",
            "bank_account",
        ]

