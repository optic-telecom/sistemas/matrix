from rest_framework import routers
from django.conf.urls import url
from .views import *

def trigger_error(request):
    division_by_zero = 1 / 0

    
router = routers.DefaultRouter()
router.register(r"configoperator", ConfigOperatorViewSet,base_name="config_operator")
router.register(r"services_ips", ServiceIpsViewSet,base_name="service_ips")
router.register(r"plans", PlanViewSet)
router.register(r"planorders", PlanOrderViewSet)
router.register(r"sellers", SellerViewSet)
router.register(r"users", UserViewSet)
router.register(r"promos", PromoViewSet)
router.register(r"commercial-activities", CommercialActivityViewSet)
router.register(r"customers", CustomerViewSet)
router.register(r"services", ServiceViewSet, base_name="service")
router.register(r"invoices", InvoiceViewSet)
router.register(r"contacts", ContactViewSet)
router.register(r"calls", CallViewSet)
router.register(r"streets", CalleViewSet)
router.register(r"addresses", DireccionViewSet)
router.register(r"payments", PaymentViewSet)
router.register(r"balances", BalanceViewSet)
router.register(r"communes", ComunaViewSet)
router.register(r"regions", RegionViewSet)
router.register(r"operators", OperatorViewSet)
router.register(r"payorruts", PayorRutViewSet)
router.register(r"bankaccount", BankAccountViewSet)
router.register(r"bank", BankViewSet)
# nulas
router.register(r"invoice-items", InvoiceItemViewSet)


urlpatterns = [
    url(r"^$", home, name="home"),
    url('sentry-debug/', trigger_error),
    url(r"iframe_plan_order_iris/(?P<slug>\d+)/$", iframe_planorder_matrix, name="iframe-planorder-matrix"),
    url(r"^api/v1/update_info/", customers_updateinfo_api, name='update-info-view'),
    url(r"^api/v1/services-filters/", service_filters_view, name='services-filters-view'),
    url(r"^api/v1/services-filters-count/", service_filters_count, name='services-filters-count'),
    url(r"^api/v1/services-filters-dict/", service_filters_dict, name='services-filters-dict'),
    url(r"^api/v1/create_customerservice/", create_customerservice_view, name='create-customerservice-view'),
    url(r"^api/v1/register_payments/", create_payments_iris_view, name='create-paymentsiris-view'),
    url(r"^api/v1/register_prorroga/", create_prorroga_iris_view, name='create-prorroga-view'),
    url(r"^api/v1/prorroga/(?P<pk>\d+)/$", get_prorroga, name='get-prorroga'),
    url(r"^api/v1/prorroga_service/(?P<pk>\d+)/$", get_prorroga_service, name='get-prorroga-service'),
    url(r"^api/v1/load_proof_payments/(?P<pk>\d+)/$", load_proof_payment, name='load-proof-paymentiris-view'),
    url(r"^api/v1/network_active_drop/(?P<slug>\d+)/$", activate_drop_service_iris_view, name='activate-drop-service-iris-view'),
    url(r"^api/v1/create_payments_pome/", create_payments_pomaire_view, name='create-paymentspome-view'),
    url(r"^api/v1/retire_equiment/", retire_equipment_service_view, name='retire-equipment-service-view'),
    url(r"^api/v1/update_indicators/", indicators_pomaire_view, name='indicators-pomaire-view'),
    url(r"^bankaccount_ajax/$", bankaccount_ajax, name="bankaccount-ajax"),
    url(r"^accounts/required-login/$", required_login_view, name="required-login"),
    url(r"^dashboard/$", dashboard, name="dashboard"),
    url(r"^dashboard_ajax/$", dashboard_ajax, name="dashboard_ajax"),
    url(r"^dashboard_ajax2/$", dashboard_ajax2, name="dashboard_ajax_2"),
    url(r"^operator_ajax/$", operator_ajax, name="operator_ajax"),
    url(r"^operators_ajax/$", operators_ajax, name="operators_ajax"),
    url(r"^clear_payment_ajax/$", clear_payment_ajax, name="clear_payment_ajax"),
    url(r"^quick-drop/$", quick_drop, name="quick-drop"),
    url(r"^lookup/$", lookup_from_liveagent, name="lookup-from-liveagent"),
    url(r"^export-drops/$", export_network_drops, name="export-drops"),
    url(r"^export-txt/$", export_txt, name="export-txt"),
    url(r"^export-customers/$", export_customers, name="export-customers"),
    url(r"^export-facturacion/$", export_facturacion, name="export-facturacion"),
    url(r"^customers/$", customer_list, name="customer-list"),
    url(r"^prospects/$", prospect_list, name="prospect-list"),
    url(r"^prospects/new/$", prospect_create, name="prospect-create"),
    url(r"^prospects/(?P<pk>\d+)/$", prospect_detail, name="prospect-detail"),
    url(r"^prospects/(?P<pk>\d+)/edit/$", prospect_update, name="prospect-update"),
    url(r"^customers/how-much/$", how_much, name="how-much"),
    url(r"^customers/payor-search/$", payor_search, name="payor-search"),
    url(r"^customers/invoice-search/$", invoice_search, name="invoice-search"),
    url(r"^customers/export/$", export_services, name="export-services"),
    url(
        r"^customers/export-one-more-time/$",
        export_services_one_more_time,
        name="export-services-one-more-time",
    ),
    url(r"^customers/new/$", customer_create, name="customer-create"),
    url(r"^customers/(?P<pk>\d+)/$", customer_detail, name="customer-detail"),
    url(r"^customers/(?P<pk>\d+)/edit/$", customer_update, name="customer-update"),
    url(r"^customers/ajax/$", customer_ajax, name="customer-ajax"),
    url(r"^customersupdate/ajax/$", customerinfo_ajax, name="customerinfo-ajax"),
    url(r"^customers/update/$", customersinfo, name="customer-info"),
    url(r"^customers/update/(?P<pk>\d+)/$", customers_detail_info, name="customerdetail-info"),
    url(r"^customers/update_accept/(?P<pk>\d+)/$", accept_changes, name="accept_changes_info"),
    url(r"^customers/update_mark/(?P<pk>\d+)/$", mark_as_validado, name="mark_as_validado"),
    url(r"^customers/update/(?P<pk>\d+)/(?P<street>\d+)/(?P<complete>\d+)/$", customers_address_save, name="customer-save"),
    url(r"^customers/update_service/(?P<pk>\d+)/(?P<street>\d+)/(?P<complete>\d+)/$", services_address_save, name="service-save"),

    url(r"^prospects/ajax/$", prospect_ajax, name="prospect-ajax"),
    url(r"^customers/invoices/$", global_invoice_list, name="global-invoice-list"),
    url(
        r"^customers/invoices/new/$",
        global_invoice_list_new,
        name="global-invoice-list-new",
    ),
    url(
        r"^customers/invoices-no-identificados/$",
        invoice_list_without_customer,
        name="invoice-list-without-customer",
    ),
    url(
        r"^customers/invoices/(?P<pk>\d+)/mark-as-paid/$",
        mark_as_paid,
        name="mark-as-paid",
    ),
    url(
        r"^customers/invoices/(?P<pk>\d+)/mark-as-unpaid/$",
        mark_as_unpaid,
        name="mark-as-unpaid",
    ),
    url(r"^customers/bank-accounts/$", bank_account_list, name="bank-account-list"),
    url(r"^region-ac/$", region_ajax, name="region-ajax"),
    url(r"^commune-ac/(?P<region>\d+)/$", commune_ajax, name="commune-ajax"),
    url(r"^street-ac/(?P<commune>\d+)/$", street_ajax, name="street-ajax"),
    url(r"^street-location-ac/(?P<street>\d+)/$", street_location_ajax, name="street_location-ajax"),
    url(r"^complete-location-ac/(?P<street_location>\d+)/$", complete_location_ajax, name="complete_location-ajax"),
    url(r"^customers/bank-accounts/ajax/$", bank_accounts_ajax, name="bank-accounts-ajax"),
    url(r"^customers/bank-accounts/(?P<bank_account>\d+)/$", cartolas_list, name="cartolas-list"),
    url(r"^customers/bank-accounts/(?P<bank_account>\d+)/ajax/$", cartolas_ajax, name="cartolas-ajax"),
    url(r"^customers/bank-accounts/cartola/(?P<pk>\d+)/$", cartola_movements_list, name="cartolas-movements-list"),
    url(r"^customers/bank-accounts/cartola/(?P<pk>\d+)/ajax$", cartola_movements_ajax, name="cartolas-movements-ajax"),
    url(r"^customers/bank-acccounts/cartola/movements/(?P<pk>\d+)/$", cartola_movements_update, name="cartolas-movements-update"),
    url(r"^customers/bank-accounts/conciliar/(?P<pk>\d+)/$", conciliar_movimientos, name="conciliar-movimientos"),
    url(r"^promotions/new/$", promotions_create, name="promotions-create"),
    url(r"^plans/ajax/$", plans_ajax, name="plans-ajax"),
    url(r"^promotions/$", promotions_list, name="promotions-list"),
    url(r"^promotions/ajax/$", promotions_ajax, name="promotions-ajax"),
    url(r"^promotions/(?P<pk>\d+)/$", promotions_update, name="promotions-update"),
    url(r"^plans/$", plans_list, name="plans-list"),
    url(r"^plans/ajax2/$", plans_list_ajax, name="plans-list-ajax"),
    url(
        r"^customers/invoices/(?P<pk>\d+)/delete/$",
        delete_invoice,
        name="delete-invoice",
    ),
    url(r"^customers/invoices/ajax/$", invoice_ajax, name="invoice-ajax"),
    url(r"^customers/invoices/ajax2/$", invoice_ajax2, name="invoice-ajax2"),
    # Billing history
    url(
        r"^customers/(?P<customer_pk>\d+)/billing/$", invoice_list, name="invoice-list"
    ),
    url(r"^customers/invoices/(?P<pk>\d+)/$", invoice_detail, name="invoice-detail"),
    url(
        r"^customers/invoices/(?P<pk>\d+)/edit/$", invoice_update, name="invoice-update"
    ),
    # Actual billing
    url(r"^customers/bforilling/$", billing, name="billing"),
    # Debt collection
    url(r"^customers/collect/$", collect, name="collect"),
    url(
        r"^customers/collect/(?P<service_number>\d+)/$",
        collect_by_number,
        name="collect-by-number",
    ),
    url(r"^customers/collect/excel/$", collect_by_excel, name="collect-by-excel"),
    # Reason for charge and credit
    url(r"^finances/management/reasons/$", mainreason_list, name="mainreason-list"),
    url(
        r"^finances/management/reasons/new/$",
        mainreason_create,
        name="mainreason-create",
    ),
    url(
        r"^finances/management/reasons/(?P<pk>\d+)/edit/$",
        mainreason_update,
        name="mainreason-update",
    ),
    url(
        r"^finances/management/mainreason_ajax/$",
        mainreason_ajax,
        name="mainreason-ajax",
    ),
    url(
        r"^finances/management/mainreason/(?P<pk>\d+)/delete/$",
        delete_reason,
        name="delete-mainreason",
    ),
    url(r"^reports/reasons_applied/$", creditChargeList, name="creditcharge-list"),
    url(
        r"^customers/reasons_applied_ajax/$",
        reasons_applied_ajax,
        name="reasons_applied-ajax",
    ),
    url(
        r"^customers/credits_applied_ajax/$",
        credits_applied_ajax,
        name="credits_applied-ajax",
    ),
    url(
        r"^customers/charges_applied_ajax/$",
        charges_applied_ajax,
        name="charges_applied-ajax",
    ),
    # Same for services
    url(
        r"^customers/(?P<customer_pk>\d+)/services/$", service_list, name="service-list"
    ),
    url(
        r"^customers/(?P<customer_pk>\d+)/services/new/$",
        service_create,
        name="service-create",
    ),
    url(
        r"^customers/(?P<customer_pk>\d+)/payment-methods/$",
        customer_payment_methods,
        name="customer-payment-methods",
    ),
    url(
        r"^customers/(?P<customer_pk>\d+)/payment-methods/add/$",
        customer_payment_methods_add,
        name="customer-payment-methods-add",
    ),
    url(
        r"^customers/(?P<customer_pk>\d+)/payment-methods/(?P<pk>\d+)/delete/$",
        customer_payment_methods_delete,
        name="customer-payment-methods-delete",
    ),
    url(r"^services/(?P<slug>\d+)/$", service_detail, name="service-detail"),
    url(r"^services/(?P<slug>\d+)/promotions/$", service_promotions_list, name="service-promotions"),
    url(r"^services/(?P<slug>\d+)/promotions/ajax/$", service_promotions_ajax, name="service-promotions-ajax"),
    url(r"^services/(?P<slug>\d+)/promotions/ajax2/$", service_promotions_hist_ajax, name="service-promotions-hist-ajax"),
    url(r"^services/custom/$", services_custom_list, name="services-custom-list"),
    url(r"^services/custom/ajax/$", services_custom_ajax, name="services-custom-ajax"),
    url(r"^services/(?P<slug>\d+)/billing/$", service_billing, name="service-billing"),
    url(
        r"^services/(?P<slug>\d+)/mark-all-invoice-as-paid/$",
        mark_all_invoice_as_paid,
        name="mark-all-invoice-as-paid",
    ),
    url(
        r"^services/(?P<slug>\d+)/technical-visit-form/$",
        technical_visit_form,
        name="technical-visit-form",
    ),
    url(
        r"^services/(?P<slug>\d+)/service-annex-form/$",
        service_annex_form,
        name="service-annex-form",
    ),
    url(
        r"^services/(?P<slug>\d+)/service-term-form/$",
        service_term_form,
        name="service-term-form",
    ),
    url(
        r"^services/(?P<slug>\d+)/team-retreat-form/$",
        equipment_removal,
        name="equipment-removal-form",
    ),
    url(r"^services/(?P<slug>\d+)/plan-service/$", plan_service, name="plan-service"),
    url(r"^services/(?P<slug>\d+)/edit/$", service_update, name="service-update"),
    url(r"^services/(?P<slug>\d+)/extras/$", service_extras, name="service-extras"),
    url(
        r"^services/(?P<slug>\d+)/extras/history/$",
        service_extras_history,
        name="service-extras-history",
    ),
    url(
        r"^services/(?P<slug>\d+)/credit/new/$",
        create_service_extra_credit,
        name="create-service-credit",
    ),
    url(
        r"^services/(?P<slug>\d+)/charge/new/$",
        create_service_extra_charge,
        name="create-service-charge",
    ),
    url(
        r"^services/(?P<slug>\d+)/credit/(?P<credit_pk>\d+)/edit/$",
        update_service_extra_credit,
        name="update-service-credit",
    ),
    url(
        r"^services/(?P<slug>\d+)/charge/(?P<charge_pk>\d+)/edit/$",
        update_service_extra_charge,
        name="update-service-charge",
    ),
    url(
        r"^credits/(?P<credit_pk>\d+)/delete/$",
        delete_service_credit,
        name="delete-service-credit",
    ),
    url(
        r"^charges/(?P<charge_pk>\d+)/delete/$",
        delete_service_charge,
        name="delete-service-charge",
    ),
    url(r"^services/(?P<slug>\d+)/gallery/$", service_gallery, name="service-gallery"),
    url(
        r"^services/(?P<slug>\d+)/gallery/new/$",
        service_gallery_add,
        name="service-gallery-add",
    ),
    url(
        r"^services/(?P<slug>\d+)/gallery/(?P<pic_pk>\d+)/edit/$",
        service_gallery_update,
        name="service-gallery-update",
    ),
    url(
        r"^services/(?P<slug>\d+)/gallery/(?P<pic_pk>\d+)/delete/$",
        service_gallery_delete,
        name="service-gallery-delete",
    ),
    url(
        r"^services/(?P<slug>\d+)/documents/$",
        service_documents,
        name="service-documents",
    ),
    url(
        r"^services/(?P<slug>\d+)/documents/new/$",
        service_documents_add,
        name="service-documents-add",
    ),
    url(
        r"^services/(?P<slug>\d+)/documents/(?P<document_pk>\d+)/edit/$",
        service_documents_update,
        name="service-documents-update",
    ),
    url(
        r"^services/(?P<slug>\d+)/documents/(?P<document_pk>\d+)/delete/$",
        service_documents_delete,
        name="service-documents-delete",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/$",
        service_network_detail,
        name="service-network-detail",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/edit/$",
        service_network_update,
        name="service-network-update",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/activate/$",
        service_network_activate,
        name="service-network-activate",
    ),
    url(
        r"^services/(?P<slug>\d+)/activate_pome/$",
        activate_service_pomaire_view,
        name="service-pomaire-activate",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/drop/$",
        service_network_drop,
        name="service-network-drop",
    ),
    url(
        r"^services/(?P<slug>\d+)/dates/edit/$",
        service_dates_update,
        name="service-dates-update",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/choose/(?P<pk>\d+)/$",
        service_network_choose,
        name="service-network-choose",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/reset-ssid/$",
        service_network_reset_ssid,
        name="service-network-reset-ssid",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/generate-password/$",
        service_network_generate_password,
        name="service-network-generate-password",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/add/$",
        service_network_item_add,
        name="service-network-item-add",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/(?P<pk>\d+)/edit/$",
        service_network_item_update,
        name="service-network-item-update",
    ),
    url(
        r"^services/(?P<slug>\d+)/network/(?P<pk>\d+)/delete/$",
        service_network_item_delete,
        name="service-network-item-delete",
    ),
    url(r"^services/(?P<slug>\d+)/support/$", service_support, name="service-support"),
    url(r"^services/(?P<slug>\d+)/typify/$", service_typify, name="service-typify"),
    url(r"^services/(?P<slug>\d+)/calls/$", service_calls, name="service-calls"),
    url(
        r"^services/(?P<slug>\d+)/plan-orders/$",
        service_plan_orders,
        name="service-plan-orders",
    ),
    url(
        r"^services/(?P<slug>\d+)/plan-orders/new/$",
        plan_order_create,
        name="service-plan-orders-create",
    ),
    url(
        r"^services/(?P<slug>\d+)/plan-orders/(?P<pk>\d+)/schedule/$",
        plan_order_schedule,
        name="plan-order-schedule",
    ),
    url(
        r"^services/(?P<slug>\d+)/plan-orders/(?P<pk>\d+)/delete/$",
        plan_order_delete,
        name="plan-order-delete",
    ),
    url(
        r"^services/(?P<slug>\d+)/plan-orders/(?P<pk>\d+)/$",
        plan_order_detail,
        name="plan-order-detail",
    ),
    url(r"^services/ajax/$", service_ajax, name="service-ajax"),
    url(r"^services/(?P<slug>\d+)/configuration/$", service_configuration, name="service-configuration"),
    url(r"^services/(?P<slug>\d+)/configuration/ajax/$", service_configuration_ajax, name="service-configurationa-ajax"),
    url(r"^customers/(?P<pk>\d+)/configuration/$", customer_configuration, name="customer-configuration"),
    url(r"^customers/(?P<pk>\d+)/widget/$", customer_widget, name="customer-widget"),
    url(r"^customers/(?P<pk>\d+)/configuration/ajax/$", customer_configuration_ajax, name="customer-configurationa-ajax"),
    url(r"^customers/register-support-call/new/$", call_create, name="call-create"),
    url(
        r"^customers/register-support-call/new-empty/$",
        call_empty_create,
        name="call-empty-create",
    ),
    url(r"^customers/register-support-call/$", call_list, name="call-list"),
    url(
        r"^customers/register-support-call/(?P<call_pk>\d+)/$",
        call_detail,
        name="call-detail",
    ),
    url(
        r"^customers/register-support-call/(?P<pk>\d+)/edit/$",
        call_update,
        name="call-update",
    ),
    url(r"^customers/register-support-call/ajax/$", call_ajax, name="call-ajax"),
    url(
        r"^customers/register-support-call-qa/new/$",
        call_qa_create,
        name="call-qa-create",
    ),
    url(r"^customers/register-support-call-qa/$", call_qa_list, name="call-qa-list"),
    url(
        r"^customers/register-support-call-qa/(?P<pk>\d+)/$",
        call_qa_detail,
        name="call-qa-detail",
    ),
    url(
        r"^customers/register-support-call-qa/ajax/$", call_qa_ajax, name="call-qa-ajax"
    ),
    url(
        r"^customers/register-support-call-qa/(?P<pk>\d+)/edit/$",
        call_qa_update,
        name="call-qa-update",
    ),
    url(
        r"^customers/register-support-call-qa/(?P<pk>\d+)/delete/$",
        call_qa_delete,
        name="call-qa-delete",
    ),
    url(r"^customers/by-phones/ajax/$", data_by_phone_ajax, name="data-by-phone"),
    url(r"^customers/by-rut/ajax/$", data_by_rut_ajax, name="data-by-rut"),
    url(r"^customers/typing/childs/$", get_typing_childs, name="get-typing-childs"),
    url(
        r"^customers/calls-monitoring/new/$",
        qa_monitoring_create,
        name="qa-monitoring-create",
    ),
    url(
        r"^customers/calls-monitoring/$", qa_monitoring_list, name="qa-monitoring-list"
    ),
    url(
        r"^customers/calls-monitoring/ajax/$",
        qa_monitoring_ajax,
        name="qa-monitoring-ajax",
    ),
    url(
        r"^customers/calls-monitoring/(?P<pk>\d+)/$",
        qa_monitoring_detail,
        name="qa-monitoring-detail",
    ),
    url(
        r"^customers/calls-monitoring/(?P<pk>\d+)/edit/$",
        qa_monitoring_update,
        name="qa-monitoring-update",
    ),
    url(
        r"^customers/calls-monitoring/(?P<pk>\d+)/delete/$",
        qa_monitoring_delete,
        name="qa-monitoring-delete",
    ),
    url(
        r"^customers/calls-monitoring/tracing/ajax/$",
        qa_monitoring_tracing_ajax,
        name="qa-monitoring-tracing-ajax",
    ),
    url(
        r"^customers/calls-monitoring/tracing/new/$",
        qa_monitoring_tracing_create,
        name="qa-monitoring-tracing-create",
    ),
    url(
        r"^customers/calls-monitoring/tracing/edit/$",
        qa_monitoring_tracing_edit,
        name="qa-monitoring-tracing-edit",
    ),
    url(
        r"^customers/calls-monitoring/tracing/(?P<pk>\d+)/delete/$",
        qa_monitoring_tracing_delete,
        name="qa-monitoring-tracing-delete",
    ),
    url(r"^qa-indicators/$", qa_indicator_list, name="qa-indicator-list"),
    url(r"^qa-indicators/ajax/$", qa_indicator_ajax, name="qa-indicator-ajax"),
    url(
        r"^qa-indicators/(?P<pk>\d+)/$", qa_indicator_detail, name="qa-indicator-detail"
    ),
    url(r"^qa-indicators/new$", qa_indicator_create, name="qa-indicator-create"),
    url(
        r"^qa-indicators/(?P<pk>\d+)/edit/$",
        qa_indicator_update,
        name="qa-indicator-update",
    ),
    url(r"^reports/call-qa/$", call_qa_report, name="call-qa-report"),
    url(
        r"^reports/call-qa-table-ajax/$", call_qa_table_ajax, name="call-qa-table-ajax"
    ),
    url(r"^export-call-qa-excel/$", export_call_qa_excel, name="export-call-qa-excel"),
    url(r"^export-call-qa-pdf/$", export_call_qa_pdf, name="export-call-qa-pdf"),
    url(
        r"^customers/register-support-call-qa/new/$",
        call_qa_create,
        name="call-qa-create",
    ),
    url(r"^customers/register-support-call-qa/$", call_qa_list, name="call-qa-list"),
    url(
        r"^customers/register-support-call-qa/(?P<pk>\d+)/$",
        call_qa_detail,
        name="call-qa-detail",
    ),
    url(
        r"^customers/register-support-call-qa/ajax/$", call_qa_ajax, name="call-qa-ajax"
    ),
    url(
        r"^customers/register-support-call-qa/(?P<pk>\d+)/edit/$",
        call_qa_update,
        name="call-qa-update",
    ),
    url(
        r"^customers/register-support-call-qa/(?P<pk>\d+)/delete/$",
        call_qa_delete,
        name="call-qa-delete",
    ),
    url(r"^customers/by-phones/ajax/$", data_by_phone_ajax, name="data-by-phone"),
    url(r"^customers/by-email/ajax/$", data_by_email_ajax, name="data-by-email"),
    url(r"^customers/by-rut/ajax/$", data_by_rut_ajax, name="data-by-rut"),
    url(r"^customers/typing/childs/$", get_typing_childs, name="get-typing-childs"),
    url(
        r"^customers/qa-monitoring/new/$",
        qa_monitoring_create,
        name="qa-monitoring-create",
    ),
    url(r"^customers/qa-monitoring/$", qa_monitoring_list, name="qa-monitoring-list"),
    url(
        r"^customers/qa-monitoring/ajax/$",
        qa_monitoring_ajax,
        name="qa-monitoring-ajax",
    ),
    url(
        r"^customers/qa-monitoring/(?P<pk>\d+)/$",
        qa_monitoring_detail,
        name="qa-monitoring-detail",
    ),
    url(
        r"^customers/qa-monitoring/(?P<pk>\d+)/edit/$",
        qa_monitoring_update,
        name="qa-monitoring-update",
    ),
    url(
        r"^customers/qa-monitoring/(?P<pk>\d+)/delete/$",
        qa_monitoring_delete,
        name="qa-monitoring-delete",
    ),
    url(
        r"^customers/qa-monitoring/tracing/ajax/$",
        qa_monitoring_tracing_ajax,
        name="qa-monitoring-tracing-ajax",
    ),
    url(
        r"^customers/qa-monitoring/tracing/new/$",
        qa_monitoring_tracing_create,
        name="qa-monitoring-tracing-create",
    ),
    url(
        r"^customers/qa-monitoring/tracing/edit/$",
        qa_monitoring_tracing_edit,
        name="qa-monitoring-tracing-edit",
    ),
    url(
        r"^customers/qa-monitoring/tracing/(?P<pk>\d+)/delete/$",
        qa_monitoring_tracing_delete,
        name="qa-monitoring-tracing-delete",
    ),
    url(
        r"^customers/qa-monitoring/export/$",
        qa_monitoring_export,
        name="qa-monitoring-export",
    ),
    url(r"^qa-indicators/$", qa_indicator_list, name="qa-indicator-list"),
    url(r"^qa-indicators/ajax/$", qa_indicator_ajax, name="qa-indicator-ajax"),
    url(
        r"^qa-indicators/(?P<pk>\d+)/$", qa_indicator_detail, name="qa-indicator-detail"
    ),
    url(r"^qa-indicators/new$", qa_indicator_create, name="qa-indicator-create"),
    url(
        r"^qa-indicators/(?P<pk>\d+)/edit/$",
        qa_indicator_update,
        name="qa-indicator-update",
    ),
    url(
        r"^qa-indicators/(?P<pk>\d+)/delete/$",
        qa_indicator_delete,
        name="qa-indicator-delete",
    ),
    url(r"^qa-channels/$", qa_channel_list, name="qa-channel-list"),
    url(r"^qa-channels/ajax/$", qa_channel_ajax, name="qa-channel-ajax"),
    url(r"^qa-channels/new$", qa_channel_create, name="qa-channel-create"),
    url(
        r"^qa-channels/(?P<pk>\d+)/edit/$", qa_channel_update, name="qa-channel-update"
    ),
    url(
        r"^qa-channels/(?P<pk>\d+)/delete/$",
        qa_channel_delete,
        name="qa-channel-delete",
    ),
    url(r"^reports/call-qa/$", call_qa_report, name="call-qa-report"),
    url(
        r"^reports/call-qa-table-ajax/$", call_qa_table_ajax, name="call-qa-table-ajax"
    ),
    url(r"^export-call-qa-excel/$", export_call_qa_excel, name="export-call-qa-excel"),
    url(r"^export-call-qa-pdf/$", export_call_qa_pdf, name="export-call-qa-pdf"),
    url(r"^customers/manual-billing/$", manual_billing, name="manual-billing"),
    url(r"^customers/manual-payments/$", manual_payments, name="manual-payments"),
    url(r"^customers/manual-cartola/$", manual_cartolas, name="manual-cartolas"),
    url(
        r"^customers/manual-payments/new/(?P<customer_pk>\d+)/$",
        payment_create,
        name="payment-create",
    ),
    url(
        r"^customers/manual-payments/new/(?P<customer_pk>\d+)/(?P<slug>\d+)/$",
        payment_create,
        name="payment-create-service",
    ),
    url(
        r"^manual_payment_new_ajax/$",
        manual_payment_new_ajax,
        name="manual_payment_new_ajax",
    ),
    url(
        r"^user_operador_new_ajax/$",
        user_operador_new_ajax,
        name="user_operador_new_ajax",
    ),
    url(
        r"^user_company_new_ajax/$",
        user_company_new_ajax,
        name="user_company_new_ajax",
    ),
    url(
        r"^manual_new_ajax_client/$",
        manual_new_ajax_client,
        name="manual_new_ajax_client",
    ),
    url(
        r"^manual_new_ajax_service/$",
        manual_new_ajax_service,
        name="manual_new_ajax_service",
    ),
    url(r"^ajax_user/$", ajax_user, name="ajax_user"),
    url(
        r"^customers/manual-invoices/new/(?P<customer_pk>\d+)/$",
        invoice_create,
        name="invoice-create",
    ),
    url(
        r"^customers/manual-invoices/new/(?P<customer_pk>\d+)/(?P<slug>\d+)/$",
        invoice_create,
        name="invoice-create-service",
    ),
    url(
        r"^customers/manual-invoices/new-note/(?P<customer_pk>\d+)/(?P<slug>\d+)/$",
        invoice_create_nota_venta,
        name="invoice-create-service-nota",
    ),
    url(
        r"^customers/manual-invoices/new-credit-note/(?P<customer_pk>\d+)/(?P<slug>\d+)/$",
        invoice_create_nota_credito,
        name="invoice-create-service-nota-credito",
    ),
    url(
        r"^customers/manual-invoices/new-cobro-note/(?P<customer_pk>\d+)/(?P<slug>\d+)/$",
        invoice_create_nota_cobro,
        name="invoice-create-service-nota-cobro",
    ),
    url(
        r"^customers/manual-invoices/new-debit-note/(?P<customer_pk>\d+)/(?P<slug>\d+)/$",
        invoice_create_nota_debito,
        name="invoice-create-service-nota-debito",
    ),
    url(r"^customers/payments/$", payment_list, name="payment-list"),
    url(
        r"^customers/payments-no-identificados/$",
        payment_list_without_customer,
        name="payment-list-without-customer",
    ),
    url(r"^customers/invoices/excel/$", invoice_excel, name="invoice_excel"),
    url(r"^customers/payments/excel/$", payments_excel, name="payments_excel"),
    url(r"^customers/payments/(?P<pk>\d+)/$", payment_detail, name="payment-detail"),
    url(
        r"^customers/payments/(?P<pk>\d+)/edit/$", payment_update, name="payment-update"
    ),
    url(r"^customers/payments/ajax/$", payment_ajax, name="payment-ajax"),
    url(r"^customers/payments/ajax2/$", payment_ajax2, name="payment-ajax2"),
    # RUT verifier
    url(r"^customers/generate-verifier/$", generate_verifier, name="generate-verifier"),
    url(
        r"^customers/verifier-digit/$", verifier_digit_ajax, name="verifier-digit-ajax"
    ),
    # Reports
    url(r"^reports/docymentsByCycle/(?P<pk>\d+)/export/$", reportInvoiceExport,),
    url(r"^reports/docymentsByCycle/(?P<pk>\d+)/ajax/$", reportInvoiceAjax,),
    url(
        r"^reports/docymentsByCycle/(?P<pk>\d+)/$",
        reportInvoices,
        name="invoiceReport",
    ),
    url(r"^reports/docymentsByCycle/ajax/$", cyclesAjax, name="cyclesAjax",),
    url(r"^reports/docymentsByCycle/$", billingCycle, name="billingCycle"),
    url(r"^reports/duration/ajax/$", durationReportAjax, name="duration-report-ajax"),
    url(
        r"^reports/duration/export/(?P<status>\d+)/(?P<interval>\w+)/$",
        durationExport,
        name="duration-report-export",
    ),
    url(r"^reports/duration/$", duration_report, name="duration-report"),
    url(r"^reports/node/$", node_report, name="node-report"),
    url(r"^reports/reincorporacion/$", reincorporacion_report, name="reincorporacion_report"),
    url(r"^reports/reincorporacion-mes/(?P<mes>\d+)/(?P<year>\d+)/$", reincorporacion_report_month, name="reincorporacion_report_month"),
    url(r"^reports/reincorporacion-mes/(?P<mes>\d+)/(?P<year>\d+)/ajax/$", reincorporacion_month_table_ajax, name="reincorporacion_ajax_report_month"),
    url(r"^reports/reincorporacion-mes/excel/$", reincorporacion_report_month_excel, name="reincorporacion_report_month_excel"),
    url(r"^reports/ventas/$", ventas_report, name="ventas_report"),
    url(r"^reports/ventas-mes/(?P<mes>\d+)/(?P<year>\d+)/$", ventas_report_month, name="ventas_report_month"),
    url(r"^reports/ventas-mes/(?P<mes>\d+)/(?P<year>\d+)/ajax/$", ventas_month_table_ajax, name="ventas_ajax_report_month"),
    url(r"^reports/ventas-mes/excel/$", ventas_report_month_excel, name="ventas_report_month_excel"),
    url(r"^reports/morosos/$", morosos_report, name="morosos_report"),
    url(r"^reports/morosos-mes/(?P<mes>\d+)/(?P<year>\d+)/$", morosos_report_month, name="morosos_report_month"),
    url(r"^reports/morosos-mes/(?P<mes>\d+)/(?P<year>\d+)/ajax/$", morosos_month_table_ajax, name="morosos_ajax_report_month"),
    url(r"^reports/morosos-mes/excel/$", morosos_report_month_excel, name="morosos_report_month_excel"),
    url(r"^reports/porretirar/$", porretirar_report, name="porretirar_report"),
    url(r"^reports/porretirar-mes/(?P<mes>\d+)/(?P<year>\d+)/$", porretirar_report_month, name="porretirar_report_month"),
    url(r"^reports/porretirar-mes/(?P<mes>\d+)/(?P<year>\d+)/ajax/$", porretirar_month_table_ajax, name="porretirar_ajax_report_month"),
    url(r"^reports/porretirar-mes/excel/$", porretirar_report_month_excel, name="porretirar_report_month_excel"),
    url(r"^reports/retiros/$", retiros_report, name="retiros_report"),
    url(r"^reports/retiros_excel/$", retiros_excel, name="retiros_excel"),
    url(r"^reports/newinstalled/$", newinstalled_report, name="newinstalled_report"),
    url(r"^reports/newinstalled_excel/$", newinstalled_excel, name="newinstalled_excel"),
    url(r"^reports/payments_plan/$", payments_report, name="payments_report"),
    url(r"^reports/payments_plan_excel/$", payments_plan_excel, name="payments_plan_excel"),
    url(r"^reports/count/$", count_report, name="count-report"),
    url(r"^reports/network/$", network_report, name="network-report"),
    url(r"^reports/payments/$", payment_report, name="payment-report"),
    url(
        r"^reports/payments_other_account/excel/$",
        payments_other_account_report_excel,
        name="payments_other_account-report_excel",
    ),
    url(
        r"^reports/payments_other_account/ajax/$",
        payments_other_account_report_ajax,
        name="payments_other_account-report_ajax",
    ),
    url(
        r"^reports/payments_other_account/$",
        payments_other_account,
        name="payments_other_account-report",
    ),
    url(r"^reports/plans/$", plan_report, name="plan-report"),
    url(r"^reports/calls/$", call_report, name="call-report"),
    url(r"^customers/table-ajax/$", table_ajax, name="table-ajax"),
    url(r"^customers/call-table-ajax/$", call_table_ajax, name="call-table-ajax"),
    url(r"^customers/count-table-ajax/$", count_table_ajax, name="count-table-ajax"),
    url(
        r"^customers/payments-table-ajax/$",
        payments_table_ajax,
        name="payments-table-ajax",
    ),
    url(
        r"^customers/network-table-ajax/$",
        network_table_ajax,
        name="network-table-ajax",
    ),
    url(r"^customers/node-table-ajax/$", node_table_ajax, name="node-table-ajax"),
    url(r"^customers/reincorporacion-table-ajax/$", reincorporacion_table_ajax, name="reincorporacion-table-ajax"),
    url(r"^customers/ventas-table-ajax/$", ventas_table_ajax, name="ventas-table-ajax"),
    url(r"^customers/morosos-table-ajax/$", morosos_table_ajax, name="morosos-table-ajax"),
    url(r"^customers/porretirar-table-ajax/$", porretirar_table_ajax, name="porretirar-table-ajax"),
    url(r"^customers/retiros-table-ajax/$", retiros_table_ajax, name="retiros-table-ajax"),
    url(r"^customers/newinstalled-table-ajax/$", newinstalled_table_ajax, name="newinstalled-table-ajax"),
    url(r"^customers/payments-plan-table-ajax/$", payments_report_table_ajax, name="payments_report_table_ajax"),
    # FIND
    url(r"^excel-diff/$", find_excel_differences, name="find-excel-differences"),
    # Emails
    url(r"^emails/$", email_list, name="email-list"),
    url(r"^emails/new/$", email_create, name="email-create"),
    # Plans
    #url(r"^plans/$", plan_list, name="plan-list"),
    url(r"^plans/new/$", plan_create, name="plan-create"),
    url(r"^plans/(?P<pk>\d+)/edit/$", plan_update, name="plan-update"),
    url(
        r"^admin/customers/service/(?P<pk>\d+)/pdf/$",
        generate_service_pdf,
        name="generate-service-pdf",
    ),
    url(
        r"^admin/customers/service/(?P<pk>\d+)/pdf-preview/$",
        generate_service_preview,
        name="generate-service-preview",
    ),  # TODO: disable this view
    url(r"^api/v1/find-factibility/", FactibilityView.as_view(), name="factibility"),
    url(
        r"^api/v1/onu-provisioning/",
        ONUProvisioningView.as_view({"post": "create"}),
        name="onu-provisioning",
    ),
    url(r"^api/v1/customer_filter/$", customer_filter, name='customer-filter'),
    url(r"^update_service_configuration/$", update_service_configuration, name='update-service-configuration'),
    url(r"^update_customer_configuration/$", update_customer_configuration, name='update-customer-configuration'),
    url(r"^api/v1/contract_acceptance/$", contract_acceptance, name = "contract-acceptance"),
    url(r"^api/v1/service_retire/$", service_retire, name = "service-retire"),
    url(r"^constance_list/$", constance_list, name ="constance-list"),
    url(r"^constance_update/$", constance_update, name="constance-update"),
    url(r"^manual_cierre_ventas/$", manual_cierre_ventas, name='manual-cierre-ventas'),
    url(r"^manual_post_instalacion/$", manual_post_instalacion, name='manual-post-instalacion'),
    url(r"^general_expense/$", general_expense_list, name = 'general-expense-list'),
    url(r"^general_expense/ajax/$", general_expense_ajax, name = 'general-expense-ajax'),
    url(r"^general_expense/(?P<pk>\d+)/$", general_expense_detail, name ='general-expense-detail'),
    url(r"^general_expense/new/$", general_expense_create, name = 'general-expense-create'),
    url(r"^general_expense/(?P<pk>\d+)/edit/$", general_expense_update, name ='general-expense-update'),
    url(r"^general_expense/(?P<pk>\d+)/delete/$", general_expense_delete, name ='general-expense-delete'),
    url(r"^general_expense/type_update/$", expense_type_update, name = 'expense-type-update'),
    url(r"^general_expense_prueba/$", pruebage, name='general-expense')
]
