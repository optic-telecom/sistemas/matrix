#!/usr/bin/python3
import logging
import sys
try:
    import posix
except Exception as e:
    pass
import time
import binascii
import socket
import select
import hashlib

from django.conf import settings
from customers.signals import notify_slack
from django.conf import settings

logger = logging.getLogger(__name__)

TECHNICAL_SCALING = "https://hooks.slack.com/services/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"

class ApiRos:
    "Routeros api"
    def __init__(self, sk):
        self.sk = sk
        self.currenttag = 0

    def login(self, username, pwd):
        md = hashlib.md5()
        md.update(b'\x00')
        md.update(pwd.encode('UTF-8'))
        respuesta = self.talk(["/login", "=name=" + username,
                   "=password=" + pwd])

        if respuesta[0][1]!={}:
            for repl, attrs in self.talk(["/login"]):
                chal = binascii.unhexlify((attrs['=ret']).encode('UTF-8'))
            md = hashlib.md5()
            md.update(b'\x00')
            md.update(pwd.encode('UTF-8'))
            md.update(chal)
            self.talk(["/login", "=name=" + username,
                   "=response=00" + binascii.hexlify(md.digest()).decode('UTF-8')])

    def talk(self, words):
        if self.writeSentence(words) == 0: return
        r = []
        while 1:
            i = self.readSentence();
            if len(i) == 0: continue
            reply = i[0]
            attrs = {}
            for w in i[1:]:
                j = w.find('=', 1)
                if (j == -1):
                    attrs[w] = ''
                else:
                    attrs[w[:j]] = w[j+1:]
            r.append((reply, attrs))
            if reply == '!done': return r

    def writeSentence(self, words):
        ret = 0
        for w in words:
            self.writeWord(w)
            ret += 1
        self.writeWord('')
        return ret

    def readSentence(self):
        r = []
        while 1:
            w = self.readWord()
            if w == '':
                return r
            r.append(w)

    def writeWord(self, w):
        # print(("<<< " + w)) # this is really ugly, must upgrade to a logger
        self.writeLen(len(w))
        self.writeStr(w)

    def readWord(self):
        ret = self.readStr(self.readLen())
        # print((">>> " + ret)) # upgrade to proper logging
        return ret

    def writeLen(self, l):
        if l < 0x80:
            self.writeStr(chr(l))
        elif l < 0x4000:
            l |= 0x8000
            self.writeStr(chr((l >> 8) & 0xFF))
            self.writeStr(chr(l & 0xFF))
        elif l < 0x200000:
            l |= 0xC00000
            self.writeStr(chr((l >> 16) & 0xFF))
            self.writeStr(chr((l >> 8) & 0xFF))
            self.writeStr(chr(l & 0xFF))
        elif l < 0x10000000:
            l |= 0xE0000000
            self.writeStr(chr((l >> 24) & 0xFF))
            self.writeStr(chr((l >> 16) & 0xFF))
            self.writeStr(chr((l >> 8) & 0xFF))
            self.writeStr(chr(l & 0xFF))
        else:
            self.writeStr(chr(0xF0))
            self.writeStr(chr((l >> 24) & 0xFF))
            self.writeStr(chr((l >> 16) & 0xFF))
            self.writeStr(chr((l >> 8) & 0xFF))
            self.writeStr(chr(l & 0xFF))

    def readLen(self):
        c = ord(self.readStr(1))
        if (c & 0x80) == 0x00:
            pass
        elif (c & 0xC0) == 0x80:
            c &= ~0xC0
            c <<= 8
            c += ord(self.readStr(1))
        elif (c & 0xE0) == 0xC0:
            c &= ~0xE0
            c <<= 8
            c += ord(self.readStr(1))
            c <<= 8
            c += ord(self.readStr(1))
        elif (c & 0xF0) == 0xE0:
            c &= ~0xF0
            c <<= 8
            c += ord(self.readStr(1))
            c <<= 8
            c += ord(self.readStr(1))
            c <<= 8                 
            c += ord(self.readStr(1))    
        elif (c & 0xF8) == 0xF0:    
            c = ord(self.readStr(1))     
            c <<= 8                 
            c += ord(self.readStr(1))    
            c <<= 8                 
            c += ord(self.readStr(1))    
            c <<= 8                 
            c += ord(self.readStr(1))    
        return c                    

    def writeStr(self, str):        
        n = 0;                      
        while n < len(str):         
            r = self.sk.send(bytes(str[n:], 'UTF-8'))
            if r == 0:
                raise RuntimeError("connection closed by remote end")
            n += r                  

    def readStr(self, length):      
        ret = ''
        while len(ret) < length:
            s = self.sk.recv(length - len(ret))
            if s == '': 
                raise RuntimeError("connection closed by remote end")
            ret += s.decode('UTF-8', 'replace')
        return ret


def drop_client(contract):
    if not contract.node:
        logger.error("Tried to DROP contract #%s without a node", contract.number)
        return

    origin = "Al tratar de desconectar el servicio"
    mikrotik_ip = contract.node.mikrotik_ip
    user = contract.node.mikrotik_username
    password = contract.node.mikrotik_password
    primary = contract.primary_equipment
    slug = contract.slug

    if primary:
        s = None
        for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC, socket.SOCK_STREAM):
            af, socktype, proto, canonname, sa = res
            try:
                 s = socket.socket(af, socktype, proto)
                 s.settimeout(1)
            except socket.error as msg:
                s = None
                continue
            try:
                s.connect(sa)
            except socket.error as msg:
                s.close()
                s = None
                continue
            break
        if s is None:
            # print('could not open socket')
            notify_service_error_mikrotik(mikrotik_ip,user,contract, origin)
            logger.error("Can't open socket to Node %s to DROP contract #%s (Mikrotik: %s)", contract.node.code, contract.number, mikrotik_ip)
            return
            # sys.exit(1) # WTH

        apiros = ApiRos(s)
        try:
            apiros.login(user, password)
        except:
            notify_service_error_mikrotik(mikrotik_ip,user,contract, origin)
            logger.error("Can't login with user %s to Drop node %s",user,mikrotik_ip)
            return


        apiros.talk(["/ip/firewall/address-list/add",
                     "=list=CORTES CLIENTES",
                     "=address={}".format(primary.ip),
                     "=comment=servicio #{}".format(slug)])
        logger.info("DROPPED %s on Node %s for contract #%s", primary.ip, contract.node.code, contract.number)
    else:
        logger.error("Tried to DROP contract #%s without a primary")
        

def activate_client(contract):
    if not contract.node:
        logger.error("Tried to ACTIVATE contract #%s without a node", contract.number)
        return

    origin = "Al tratar de conectar el servicio"
    mikrotik_ip = contract.node.mikrotik_ip
    user = contract.node.mikrotik_username
    password = contract.node.mikrotik_password
    primary = contract.primary_equipment


    if primary:
        s = None
        for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC, socket.SOCK_STREAM):
            af, socktype, proto, canonname, sa = res
            try:
                 s = socket.socket(af, socktype, proto)
                 s.settimeout(1)
            except socket.error as msg:
                s = None
                continue
            try:
                s.connect(sa)
            except socket.error as msg:
                s.close()
                s = None
                continue
            break
        if s is None:
            # print('could not open socket')
            notify_service_error_mikrotik(mikrotik_ip,user,contract,origin)
            logger.error("Can't open socket to Node %s to ACTIVATE contract #%s (Mikrotik: %s)", contract.node.code, contract.number, mikrotik_ip)
            return
            # sys.exit(1) # why this was here, I can't comprehend

        apiros = ApiRos(s)
        try:
            apiros.login(user, password)
        except:
            notify_service_error_mikrotik(mikrotik_ip,user,contract,origin)
            logger.error("Can't login with user %s to Drop node %s",user,mikrotik_ip)
            return

        response = apiros.talk(["/ip/firewall/address-list/print",
                                "?list=CORTES CLIENTES",
                                "?address={}".format(primary.ip),
                                "=.proplist=.id"])

        for reply, data in response:
            if reply == '!re':
                try:
                    client_id = data['=.id']
                    apiros.talk(["/ip/firewall/address-list/remove", "=.id={}".format(client_id)])
                    logger.info("ACTIVATED %s on Node %s for contract #%s", primary.ip, contract.node.code, contract.number)
                except KeyError:
                    pass
    else:
        logger.error("Tried to ACTIVATE contract #%s without a primary", contract.number)
                    


def get_client_status(mikrotik_ip, user, password, client_ip):
    return False
    s = None
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC, socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
             s = socket.socket(af, socktype, proto)
             s.settimeout(1)
        except socket.error as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)


    apiros = ApiRos(s)
    try:
        apiros.login(user, password)
    except:
        notify_error_mikrotik(mikrotik_ip,user)
        logger.error("Can't login with user %s to Drop node %s",user,mikrotik_ip)
        return

    response = apiros.talk(["/ip/firewall/address-list/print",
                            "?list=CORTES CLIENTES",
                            ])

    is_there = False
    for reply, data in response:
        if reply == '!re':
            try:
                if client_ip == data['=address']:
                    is_there = True
            except KeyError:
                pass
    return is_there


def get_dropped(mikrotik_ip, user, password):
    s = None
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC, socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
             s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)

    s.settimeout(1)
    apiros = ApiRos(s)
    try:
        apiros.login(user, password)
    except:
        notify_error_mikrotik(mikrotik_ip,user)
        logger.error("Can't login with user %s to Drop node %s",user,mikrotik_ip)
        return

    response = apiros.talk(["/ip/firewall/address-list/print",
                            "?list=CORTES CLIENTES",
                            ])
    raw_results = []

    for reply, data in response:
        if reply == '!re':
            try:
                raw_results.append(data['=comment'])
            except KeyError:
                pass

    results = []
    for item in raw_results:
        try:
            contract_number = item.split('#')[1].split(' ')[0]
            results.append(int(contract_number))
        except:
            pass
    return results



def get_leases(mikrotik_ip, user, password):
    s = None
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC, socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
             s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)

    s.settimeout(1)
    apiros = ApiRos(s)
    try:
        apiros.login(user, password)
    except:
        notify_error_mikrotik(mikrotik_ip,user)
        logger.error("Can't login with user %s to Drop node %s",user,mikrotik_ip)
        return

    response = apiros.talk(["/ip/dhcp-server/lease/print",
                            "=.proplist=.id,address,mac-address,status,host-name",])

    results = []

    for reply, data in response:
        if reply == '!re':
            try:
                results.append(dict(address=data['=address'],
                                    mac_address=data['=mac-address'],
                                    status=data['=status'],
                                    host_name=data.get('=host-name', ''),))
            except KeyError:
                pass
    return results


def main():
    s = None
    for res in socket.getaddrinfo(sys.argv[1], "8728", socket.AF_UNSPEC, socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
             s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print ('could not open socket')
        sys.exit(1)

    s.settimeout(1)
    apiros = ApiRos(s);
    apiros.login(sys.argv[2], sys.argv[3]);

    inputsentence = []

    while 1:
        r = select.select([s, sys.stdin], [], [], None)
        if s in r[0]:
            # something to read in socket, read sentence
            x = apiros.readSentence()

        if sys.stdin in r[0]:
            # read line from input and strip off newline
            l = sys.stdin.readline()
            l = l[:-1]

            # if empty line, send sentence and start with new
            # otherwise append to input sentence
            if l == '':
                apiros.writeSentence(inputsentence)
                inputsentence = []
            else:
                inputsentence.append(l)

def notify_error_mikrotik(nodo_ip, user):
    if (settings.ENVIROMENT == 'PROD'):
        notify_slack(TECHNICAL_SCALING, 
            {
                'text': "Error conectando nodo de Ip {} con el usuario {}".format(nodo_ip, user),
                'attachments': [
                    {
                        'fallback': "Nodo Ip {}".format(nodo_ip),
                        'color': "danger",
                        'author_name': user,
                        'title': "Error conectar: Nodo {}".format(nodo_ip),
                        'text': "{}".format(nodo_ip),
                        'fields': [
                            {
                                'title': "Nodo Ip",
                                'value': nodo_ip,
                                'short': True
                            },
                            {
                                'title': "Usuario",
                                'value': user,
                                'short': True
                            },
                        ]
                    }
                ]
            }
        )

def notify_service_error_mikrotik(nodo_ip, user, service,origin):
    if (settings.ENVIROMENT == 'PROD'):
        notify_slack(TECHNICAL_SCALING, 
            {
                'text': "Fallo en conexión matrix-mikrotik",
                'attachments': [
                    {   
                        'fallback': "Error al conectar matrix a nodo {}".format(nodo_ip),
                        'color': "danger",
                        'title': "Error al conectar matrix a nodo {}".format(nodo_ip),
                        'fields': [
                            {
                                'title': "Nodo Ip",
                                'value': nodo_ip,
                                'short': True
                            },
                            {
                                'title': "Usuario",
                                'value': user,
                                'short': True
                            },
                            {
                                'title': "Servicio",
                                'value': "https://optic.matrix2.cl/services/" + str(service.number) + "/network/",
                                'short': True
                            },
                            {
                                'title': "Origen",
                                'value': origin,
                                'short': True
                            },
                        ],
                        "footer": "Alerta enviada a usuarios activos del canal <!here>",
                    }
                ]
            }
        )

if __name__ == '__main__':
    main()

