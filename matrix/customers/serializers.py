from django.contrib.auth import get_user_model
from .models import *
from infra.models import NetworkEquipment
from hr.models import Technician
from datetime import datetime

from rest_framework import serializers
from drf_queryfields import QueryFieldsMixin

class PlanSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Plan
        fields = ('id', 'name', 'price', 'category', 'get_category_display', 'uf', 'active')


class SellerSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Seller
        fields = ('id', 'name')


class UserSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name', 'last_name', 'username', 'email')

class PromoSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Promotions
        fields = ('id','name','description')

class ConfigOperatorSerializer1(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    operator = serializers.HyperlinkedRelatedField(view_name='api:operator-detail', read_only=True)
    #operator_id = serializers.ReadOnlyField()
    class Meta:
        model = ConfigOperator
        fields = ('id','operator','name','flag')

class ConfigOperatorSerializer(serializers.Serializer):
    name = serializers.CharField()
    operator = serializers.IntegerField(default=2)
    flag = serializers.JSONField()

class CustomerServiceSerializer(serializers.Serializer):
    # Customers fields
    send_email = serializers.BooleanField(default=True)
    send_snail_mail = serializers.BooleanField(default=False)
    rut = serializers.CharField(max_length=20)
    first_name = serializers.CharField( max_length=255)
    last_name = serializers.CharField( max_length=255)
    kind = serializers.IntegerField(default=1)
    commercial_activity = serializers.IntegerField(allow_null=True)
    email = serializers.EmailField()
    street = serializers.CharField( max_length=75)
    house_number = serializers.CharField(max_length=75)
    apartment_number = serializers.CharField(max_length=75)
    tower = serializers.CharField( max_length=75)
    location = serializers.CharField( max_length=5)
    phone = serializers.CharField( max_length=75)
    default_due_day = serializers.IntegerField(default=5)
    unified_billing = serializers.BooleanField(default=False)
    full_process = serializers.BooleanField( default=True)
    notes = serializers.CharField(style={'base_template': 'textarea.html'})
    birth_date = serializers.DateField(allow_null=True)
    first_category_activity = serializers.BooleanField(default=False)
    # Service fields
    document_type = serializers.IntegerField(default=1)
    plan = serializers.IntegerField()  
    technology_kind = serializers.IntegerField(default=1)
    operator = serializers.IntegerField(default=2)
    
class ServiceIpsSerializer(serializers.Serializer):
    ip = serializers.CharField()
    service = serializers.IntegerField()

class ProrrogaIrisSerializer(serializers.Serializer):
    invoices = serializers.ListField()
    date = serializers.DateTimeField()

class ServiceSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    customer = serializers.HyperlinkedRelatedField(view_name='api:customer-detail', queryset = Customer.objects.all())
    seller = serializers.HyperlinkedRelatedField(view_name='api:seller-detail', read_only=True)
    node = serializers.HyperlinkedRelatedField(view_name='node-detail', read_only=True)
    plan = serializers.HyperlinkedRelatedField(view_name='api:plan-detail', queryset = Plan.objects.all())
    #promos = serializers.HyperlinkedRelatedField(view_name='api:promo-detail', read_only=True)
    technician = serializers.HyperlinkedRelatedField(view_name='api:technician-detail', read_only=True)
    document_type = serializers.CharField(source='get_document_type_display')
    get_statement_actual = serializers.ReadOnlyField()
    class Meta:
        model = Service
        fields = ('id',
                  'number',
                  'customer',
                  'document_type',
                  'street',
                  'house_number',
                  'apartment_number',
                  'tower',
                  'location',
                  #'promos',
                  'get_statement_actual',
                  'plan',
                  'price_override',
                  'due_day',                  
                  'activated_on',
                  'installed_on',
                  'uninstalled_on',
                  'expired_on',
                  'status',
                  'seller',
                  'technician',
                  'node',
                  'original_node',
                  'network_mismatch',
                  'node_mismatch',
                  'billing_notes',
                  'internal_notes',
                  'ssid',
                  'password',    
                  'technology_kind',
                  'allow_auto_cut',
                  'seen_connected',
                  'mac_onu',
                  'get_status_display',
                  'get_technology_kind_display',
                  'composite_address',
                  'network_status',
                  'commune',
                  'best_location',)  # node, tags
        

class CommercialActivitySerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = CommercialActivity
        fields = ('id', 'code', 'name')

class CustomerSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    service_set = serializers.HyperlinkedRelatedField(view_name='api:service-detail', read_only=True, many=True)
    kind = serializers.IntegerField(min_value=1, write_only=True)
    kind_name = serializers.CharField(source='get_kind_display', read_only=True)
    commercial_activity = serializers.HyperlinkedRelatedField(view_name='api:commercialactivity-detail', read_only=True)
    name = serializers.ReadOnlyField()
    class Meta:
        model = Customer
        fields = ('id',
                  'rut',
                  'first_name',
                  'last_name',
                  'name',
                  'kind',
                  'kind_name',
                  'commercial_activity',
                  'email',
                  'street', 
                  'house_number',  
                  'apartment_number',  
                  'tower', 
                  'location',           
                  'phone',
                  'default_due_day',
                  'composite_address',
                  'commune',
                  'notes',
                  'service_set')


class ContactSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Contact
        fields = ('customer',
                  'kind',
                  'name',
                  'phone',
                  'email')

class CallSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    answered_by = serializers.HyperlinkedRelatedField(view_name='api:user-detail', read_only=True)

    class Meta:
        model = Call
        fields = ('answered_by',
                  'subject',
                  'phone_number',
                  'notes',
                  'recording',
                  'qa_comment')
class ComunaSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    region = serializers.HyperlinkedRelatedField(view_name='api:region-detail', read_only=True)

    class Meta:
        model = Comuna
        fields = ('entel_id','nombre','region')


class CalleSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    comuna = serializers.HyperlinkedRelatedField(view_name='api:comuna-detail', read_only=True)

    class Meta:
        model = Calle
        fields = ('entel_id','nombre','comuna')

class PaymentPomaireSerializer(serializers.Serializer):
    kind = serializers.IntegerField() #serializers.CharField(source='get_kind_display')
    operation_number = serializers.CharField()
    transfer_id = serializers.CharField()
    cleared = serializers.BooleanField()
    comment = serializers.CharField()
    customer_rut = serializers.CharField()
    invoices = serializers.CharField()
    paid_on = serializers.DateTimeField()
    deposited_on = serializers.DateTimeField()
    amount = serializers.IntegerField()

class PaymentIrisSerializer(serializers.Serializer):
    kind = serializers.IntegerField() #serializers.CharField(source='get_kind_display')
    operation_number = serializers.CharField(required=False)
    transfer_id = serializers.CharField(required=False)
    cleared = serializers.BooleanField(required=False)
    comment = serializers.CharField()
    customer_rut = serializers.CharField()
    invoices = serializers.ListField(required=False)
    paid_on = serializers.DateTimeField()
    deposited_on = serializers.DateTimeField()
    amount = serializers.IntegerField()
    operator = serializers.IntegerField()
    bank_account = serializers.IntegerField()
    service = serializers.ListField(required=False)
    comprobante = serializers.ListField(required=False)


class PaymentSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    kind = serializers.CharField(source='get_kind_display')
    service = ServiceSerializer(read_only=True, many=True)

    class Meta:
        model = Payment
        fields = ('paid_on', 'deposited_on','amount',
                  'manual','kind','operation_number', 'transfer_id', 
                  'cleared', 'comment', 'customer', 'service')

class PaymentIrisSerializer(serializers.Serializer):
    kind = serializers.IntegerField() #serializers.CharField(source='get_kind_display')
    operation_number = serializers.CharField(required=False)
    transfer_id = serializers.CharField(required=False)
    cleared = serializers.BooleanField(required=False)
    comment = serializers.CharField()
    customer_rut = serializers.CharField()
    invoices = serializers.CharField(required=False)
    paid_on = serializers.DateTimeField()
    deposited_on = serializers.DateTimeField()
    amount = serializers.IntegerField()
    operator = serializers.IntegerField()
    bank_account = serializers.IntegerField()

class PlanOrderSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    service = serializers.HyperlinkedRelatedField(view_name='api:service-detail', queryset = Service.objects.all())
    street_address = serializers.ReadOnlyField()
    microarea = serializers.ReadOnlyField()
    city = serializers.ReadOnlyField()
    id = serializers.ReadOnlyField()

    class Meta:
        model = PlanOrder
        fields =  ('id','service',  'kind', 'subject', 'start', 'end', 'team', 'cancelled','street_address', 'microarea','city')

class EquipmentRemovalSerializer(serializers.Serializer):
    service_id = serializers.CharField()
    person = serializers.CharField()
    status = serializers.CharField()
    pick_up_date = serializers.CharField()
    pick_up_time = serializers.CharField()


class IndicatorsSerializer(serializers.Serializer):
    uf = serializers.DecimalField(max_digits=20, decimal_places=2, allow_null=True)
    dolar = serializers.DecimalField(max_digits=20, decimal_places=2, allow_null=True)


class BalanceSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Balance
        fields = ('customer', 'balance')


class RegionSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Region
        fields = ('entel_id', 'nombre')

class InvoiceSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    kind = serializers.CharField(source='get_kind_display')
    service = ServiceSerializer(read_only=True, many=True)
    # operator = serializers.HyperlinkedRelatedField(view_name='api:operator-detail', read_only=True)

    class Meta:
        model = Invoice
        fields = ('id','kind','folio','customer','due_date','total',
                  'comment','paid_on','file','status', 'service')

class InvoiceItemSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = InvoiceItem
        fields = ('invoice','description','price')

class DireccionSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    
    calle = serializers.HyperlinkedRelatedField(view_name='api:calle-detail', read_only=True)

    class Meta:
        model = Direccion
        fields = ('entel_id','numeroMunicipal','lat','lon','calle')

class FactibilitySerializer(serializers.Serializer):
    
    region = serializers.IntegerField(required=False)
    comuna = serializers.IntegerField(required=False)
    calle = serializers.IntegerField(required=False)
    lugar = serializers.IntegerField(required=False)

class BankAccountSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    operator = serializers.HyperlinkedRelatedField(view_name='api:operator-detail', read_only=True)
    bank = serializers.HyperlinkedRelatedField(view_name='api:bank-detail', read_only=True)

    class Meta:
        model = BankAccount
        fields = ('id', 'tag', 'account_number', 'operator','bank')

class BankSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
   
    class Meta:
        model = Bank
        fields = ('id', 'name', 'code')

class ONUProvisioningSerializer(serializers.Serializer):

    technician = serializers.IntegerField()
    password = serializers.CharField()
    ssid_2g = serializers.CharField()
    ssid_5g = serializers.CharField()
    technology_kind = serializers.IntegerField()
    brand = serializers.CharField()
    model = serializers.CharField()
    ip = serializers.CharField(required=False)
    serial = serializers.CharField()
    mac = serializers.CharField()
    note = serializers.CharField()
    mac = serializers.CharField(required=False)
    service = serializers.IntegerField()
    caso = serializers.IntegerField()

    class Meta:
        model = NetworkEquipment
        fields = ('technician','password','ssid_2g','ssid_5g','technology_kind','brand','model','ip','serial','caso','mac','note')

    def create(self, validated_data):
        technician = Technician.objects.get(id= validated_data['technician'])
        password = validated_data['password']
        brand = validated_data['brand']
        model = validated_data['model']
        ssid_2g = validated_data['ssid_2g']
        ssid_5g = validated_data['ssid_5g']
        serial = validated_data['serial']
        note = validated_data['note']
        caso = int(validated_data['caso'])
        technology_kind = validated_data['technology_kind']
        if validated_data.get('ip'):
            ip = validated_data['ip']
        else:
            ip = '4.4.4.4'
        if validated_data.get('mac'):
            mac = validated_data['mac']
        else:
            mac = '44:44:44:44:44:44'
        service = Service.objects.get(id=validated_data['service'])
        old_primary_network_equipment = NetworkEquipment.objects.filter(service=service, primary=True)
        if old_primary_network_equipment.exists():
            for network_equipment in old_primary_network_equipment:
                network_equipment.primary = False
                network_equipment.ip = '2.2.2.2'
                network_equipment.save()
        network_equipment_obj = NetworkEquipment(
            brand = brand,
            model = model,
            ip = ip,
            mac = mac,
            serial_number = serial,
            notes = note,
            service = service,
            primary= True)
        network_equipment_obj.save()

        if caso == 1:
            service.installed_on = datetime.now()
            service.save()

        elif caso == 2:
            service.technician = technician
            service.password = password
            service.ssid = ssid_2g
            service.ssid_5g = ssid_5g
            service.technology_kind = technology_kind
            service.billing_notes = 'Posible fecha visita tecnica o migracion de plan (revisar el caso) = ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S")
            service.save()
        elif caso == 3:
            service.technician = technician
            service.password = password
            service.ssid = ssid_2g
            service.ssid_5g = ssid_5g
            service.technology_kind = technology_kind
            service.billing_notes = 'Fecha de reincorporacion = ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        
        elif caso == 4:
            pass
        else:
            raise ValidationError('This case is not correct')
        


        return validated_data

class PayorRutSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    customer = serializers.HyperlinkedRelatedField(view_name='api:customer-detail', read_only=True)
    class Meta:
        model = PayorRut
        fields = ('rut', 'name', 'customer')


class OperatorSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = Operator
        fields = ('id', 'name', 'code')

    
