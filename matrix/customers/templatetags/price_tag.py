from django import template
from django.contrib.humanize.templatetags.humanize import intcomma

register = template.Library()

@register.filter
def price_tag(number, decimal_places=2, decimal=','):
    #result = intcomma(number)
    value = ('%.6f' % number).rstrip('0').rstrip('.')
    return intcomma(value).replace(".", "*").replace(",", ".").replace("*", ",")
    """
    #return '%g'%(number)
    #print('%g'%(number))
    #print(result)
    result=result.replace(".", "*").replace(",", ".").replace("*", ",")
    total=result.split(".")
    #print(total)
    if len(total)==2:
        if total[1]=="00" or int(total[1])==0:
            return total[0]
        else:
            return '%g'%(number)
            #print(f'{result:g}')
            return result
    else:
        return result
    """
