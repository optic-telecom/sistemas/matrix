from django import template
from customers.models import TagColor

register = template.Library()


@register.simple_tag
def tag_color(name):
    try:
        return TagColor.objects.get(tag=name).html_color
    except TagColor.DoesNotExist:
        return '#9a9a9a'
