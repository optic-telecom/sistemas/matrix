from django import template
from ..models import Service

register = template.Library()


@register.simple_tag
def get_service_status_class(status):
    return Service.STATUS_CLASSES.get(status, 'default')
