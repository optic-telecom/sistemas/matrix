from django import template

register = template.Library()


@register.simple_tag
def add_active(request, name):
    if request.resolver_match.url_name == name:
        return ' active '

    return ''
