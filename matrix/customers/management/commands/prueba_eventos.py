from django.core.management import BaseCommand
from customers.models import *
from billing.views import barcode, invoiceDataAfter
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta
import pandas as pd
pd.set_option('mode.chained_assignment', None)
from providers.models import Provider
from reports.models import ServiceStatusLog
from constance import config
import requests
from django.template.loader import get_template
import base64, pdfkit

class Command(BaseCommand):
    def add_arguments(self, parser):
            parser.add_argument('--arg1', help="numero de evento", type=str)
            parser.add_argument('--arg2', help="id cliente o servicio", type=str)
            parser.add_argument('--arg3', help="shortcut", type=str)

    def handle(self, *args, **options):
    
        if options['arg1'] is None or options['arg2'] is None:
            print("Error falta un dato")
            print(options)
            exit()

        if options['arg1'] == '1':
            cen = 0
            service = Service.objects.get(number=int(options['arg2']))
            if service.customer.flag:
                if "nuevo_cliente_email" not in service.customer.flag.keys():
                    service.customer.flag['nuevo_cliente_email'] = "Por enviar"
                    service.customer.save()
                    cen = 1
                else:
                    if service.customer.flag["nuevo_cliente_email"] != "Enviado":
                        service.customer.flag["nuevo_cliente_email"] = "Por enviar"
                        service.customer.save()
                        cen = 1
                    else:
                        print("ya lo envié")
            else:
                json = {"nuevo_cliente_email": "Por enviar"}
                service.customer.flag = json
                service.customer.save()
                cen = 1

            if service.customer.flag:
                if "bloqueado_nuevo_cliente_email" in service.customer.flag.keys():
                    if service.customer.flag["bloqueado_nuevo_cliente_email"] == 'Si':
                        cen = 0

            orders = PlanOrder.objects.all().filter(service = service.pk)
            index = 0
            installation_date = ''
            formatted_installation_date = '29 de Junio del 2021'
            if options['arg3'] == '1':
                while (index < len(orders)):
                    if orders[index].cancelled == False and orders[index].start != None:
                        installation_date = orders[index].start
                        break
                    index += 1
                    
                if installation_date:
                    MES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
                        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
                    formatted_installation_date = str(installation_date.day) + ' de ' + str(MES[installation_date.month-1]) + ' del ' + str(installation_date.year)
                else:
                    formatted_installation_date = '(Por definir)'
                    #cen = 0

            nombre = ''
            if service.customer.kind == 1:
                nombre = service.customer.first_name.split(" ")[0]
            else:
                nombre = service.customer.name

            costo_habilitacion = ''
            print(service.technology_kind)
            if service.technology_kind == 1:
                costo_habilitacion = '<li>Debido a que tu dirección arroja un Servicio UTP se adicionara un costo de Habilitación, habitualmente el costo es de $50.000 pesos; sin embargo, por una promoción referente al COVID-19 obtendrá una rebaja y solo deberá cancelar <strong>$20.000 pesos</strong>.</li>'

            
            if cen == 1:
                info = []
                info.append(
                {
                    "number": str(service.number),
                    "street": str(service.street),
                    "house_number": str(service.house_number),
                    "apartment_number": str(service.apartment_number),
                    "tower": str(service.tower),
                    "location": str(service.location),
                    "price_override": str(service.price_override),
                    "due_day": str(service.due_day),
                    "activated_on": str(service.activated_on) if service.activated_on else "",
                    "installed_on": str(service.installed_on)  if service.installed_on else "",
                    "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                    "expired_on": str(service.expired_on)  if service.expired_on else "",
                    "status": str(service.get_status_display()),
                    "network_mismatch": "Sí" if service.network_mismatch else "No",
                    "ssid": str(service.ssid),
                    "technology_kind": str(service.get_technology_kind_display()),
                    "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                    "seen_connected": str(service.seen_connected),
                    "mac_onu": str(service.mac_onu),
                    "composite_address": str(service.customer.composite_address),
                    "commune": str(service.node.commune),
                    "plan__name": str(service.plan.description_facturacion),
                    "plan__price": str(int(service.plan.price)),
                    "plan__category": str(service.plan.category),
                    "plan__uf":  "Sí" if service.plan.uf else "No",
                    "plan__active": "Sí" if service.plan.active else "No", 
                    "customer__rut": str(service.customer.rut),
                    "customer__name": str(service.customer.name),
                    "customer__first_name": str(nombre),
                    "customer__last_name": str(service.customer.last_name), ###NUEVO###
                    "customer__email": str(service.customer.email),
                    "customer__street": str(service.customer.street),
                    "customer__house_number": str(service.customer.house_number),
                    "customer__apartment_number": str(
                        service.customer.apartment_number
                    ),
                    "customer__tower": str(service.customer.tower),
                    "customer__location": str(service.customer.location),
                    "customer__phone": str(service.customer.phone),
                    "customer__default_due_day": str(
                        service.customer.default_due_day
                    ),
                    "customer__composite_address": str(
                        service.customer.composite_address
                    ),
                    "customer__commune": str(service.customer.commune),
                    "installation_date": formatted_installation_date, 
                    "service__number": str(service.number),
                    "customer__id": str(service.customer.id),
                    "customer_html": str(costo_habilitacion),
                })
                
                data = {"event": getattr(config, "evento_cierre_de_ventas"), "info": info}
                url = "https://Backend.iris7.cl/api/v1/communications/trigger_event/active/"
                
                try:
                    send = requests.post(
                        url=url,
                        json=data,
                        headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                        verify = False
                    )
                    if "Error" in send.json() or "error" in send.json():
                        print("ERROR")
                    else:
                        service.customer.flag["nuevo_cliente_email"] = "Enviado"
                        service.customer.save()
                except Exception as e:
                    print("e",e)     

        if options['arg1'] == '2':
            service = Service.objects.get(number=int(options['arg2']))
            if service.flag:
                if "post_activacion_email" not in service.flag.keys():
                    service.flag['post_activacion_email'] = "Por enviar"
                    service.save()
                    cen = 1
                else:
                    if service.flag["post_activacion_email"] != "Enviado":
                        service.flag["post_activacion_email"] = "Por enviar"
                        service.save()
                        cen = 1
                    else:
                        print("ya lo envié")
            else:
                json = {"post_activacion_email": "Por enviar"}
                service.flag = json
                service.save()
                cen = 1

            if service.flag:
                if "bloqueado_post_activacion_email" in service.flag.keys():
                    if service.flag["bloqueado_post_activacion_email"] == 'Si':
                        cen = 0

            nombre = ''
            if service.customer.kind == 1:
                nombre = service.customer.first_name.split(" ")[0]
            else:
                nombre = service.customer.name
            
            
            if cen == 1:
                info = []
                info.append(
                {
                    "number": str(service.number),
                    "street": str(service.street),
                    "house_number": str(service.house_number),
                    "apartment_number": str(service.apartment_number),
                    "tower": str(service.tower),
                    "location": str(service.location),
                    "price_override": str(service.price_override),
                    "due_day": str(service.due_day),
                    "activated_on": str(service.activated_on) if service.activated_on else "",
                    "installed_on": str(service.installed_on)  if service.installed_on else "",
                    "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                    "expired_on": str(service.expired_on)  if service.expired_on else "",
                    "status": str(service.get_status_display()),
                    "network_mismatch": "Sí" if service.network_mismatch else "No",
                    "ssid": str(service.ssid),
                    "technology_kind": str(service.get_technology_kind_display()),
                    "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                    "seen_connected": str(service.seen_connected),
                    "mac_onu": str(service.mac_onu),
                    "composite_address": str(service.customer.composite_address),
                    "commune": str(service.node.commune),
                    "plan__name": str(service.plan.description_facturacion),
                    "plan__price": str(service.plan.price),
                    "plan__category": str(service.plan.category),
                    "plan__uf":  "Sí" if service.plan.uf else "No",
                    "plan__active": "Sí" if service.plan.active else "No", 
                    "customer__rut": str(service.customer.rut),
                    "customer__name": str(service.customer.name),
                    "customer__first_name": str(nombre),
                    "customer__last_name": str(service.customer.last_name), ###NUEVO###
                    "customer__email": str(service.customer.email),
                    "customer__street": str(service.customer.street),
                    "customer__house_number": str(service.customer.house_number),
                    "customer__apartment_number": str(
                        service.customer.apartment_number
                    ),
                    "customer__tower": str(service.customer.tower),
                    "customer__location": str(service.customer.location),
                    "customer__phone": str(service.customer.phone),
                    "customer__default_due_day": str(
                        service.customer.default_due_day
                    ),
                    "customer__composite_address": str(
                        service.customer.composite_address
                    ),
                    "customer__commune": str(service.customer.commune),
                })

                data = {"event": getattr(config, "evento_post_activacion"), "info": info}
                url = "https://Backend.iris7.cl/api/v1/communications/trigger_event/active/"


                try:
                    send = requests.post(
                        url=url,
                        json=data,
                        headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                        verify = False
                    )
                    if "Error" in send.json() or "error" in send.json():
                        print("ERROR")
                    else:
                        service.flag["post_activacion_email"] = "Enviado"
                        service.save()
                except Exception as e:
                    print("e",e)
        if options['arg1'] == '3':
            service = Service.objects.get(number=int(options['arg2']))
            if service.flag:
                if "onboarding_email" not in service.flag.keys():
                    service.flag["onboarding_email"] = "Por enviar"
                    service.save()
                    cen = 1
                else:
                    if service.flag["onboarding_email"] != "Enviado":
                        service.flag["onboarding_email"] = "Por enviar"
                        service.save()
                        cen = 1
                    else:
                        print("Ya lo envié")
            else:
                json = {"onboarding_email": "Por enviar"}
                service.flag = json
                service.save()
                cen = 1

            if service.flag:
                if "bloqueado_onboarding_email" in service.flag.keys():
                    if service.flag["bloqueado_onboarding_email"] == 'Si':
                        cen = 0
            
            nombre = ''
            if service.customer.kind == 1:
                nombre = service.customer.first_name.split(" ")[0]
            else:
                nombre = service.customer.name

            
            if cen == 1:
                info = []
                info.append(
                {
                    "number": str(service.number),
                    "street": str(service.street),
                    "house_number": str(service.house_number),
                    "apartment_number": str(service.apartment_number),
                    "tower": str(service.tower),
                    "location": str(service.location),
                    "price_override": str(service.price_override),
                    "due_day": str(service.due_day),
                    "activated_on": str(service.activated_on) if service.activated_on else "",
                    "installed_on": str(service.installed_on)  if service.installed_on else "",
                    "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                    "expired_on": str(service.expired_on)  if service.expired_on else "",
                    "status": str(service.get_status_display()),
                    "network_mismatch": "Sí" if service.network_mismatch else "No",
                    "ssid": str(service.ssid),
                    "technology_kind": str(service.get_technology_kind_display()),
                    "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                    "seen_connected": str(service.seen_connected),
                    "mac_onu": str(service.mac_onu),
                    "composite_address": str(service.customer.composite_address),
                    "commune": str(service.node.commune),
                    "plan__name": str(service.plan.description_facturacion),
                    "plan__price": str(service.plan.price),
                    "plan__category": str(service.plan.category),
                    "plan__uf":  "Sí" if service.plan.uf else "No",
                    "plan__active": "Sí" if service.plan.active else "No", 
                    "customer__rut": str(service.customer.rut),
                    "customer__name": str(service.customer.name),
                    "customer__first_name": str(nombre),
                    "customer__last_name": str(service.customer.last_name), ###NUEVO###
                    "customer__email": str(service.customer.email),
                    "customer__street": str(service.customer.street),
                    "customer__house_number": str(service.customer.house_number),
                    "customer__apartment_number": str(
                        service.customer.apartment_number
                    ),
                    "customer__tower": str(service.customer.tower),
                    "customer__location": str(service.customer.location),
                    "customer__phone": str(service.customer.phone),
                    "customer__default_due_day": str(
                        service.customer.default_due_day
                    ),
                    "customer__composite_address": str(
                        service.customer.composite_address
                    ),
                    "customer__commune": str(service.customer.commune),
                })
                
                data = {"event": getattr(config, "evento_correo_onboarding"), "info": info}
                url = "https://Backend.iris7.cl/api/v1/communications/trigger_event/active/"

                try:
                    send = requests.post(
                        url=url,
                        json=data,
                        headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                        verify = False
                    )
                    if "Error" in send.json() or "error" in send.json():
                        print("ERROR")
                    else:
                        service.flag["onboarding_email"] = "Enviado"
                        service.save()
                except Exception as e:
                    print("e",e)
        if options['arg1'] == '4':
            invoice = Invoice.objects.get(id=int(options['arg2']))
            data = invoiceDataAfter(invoice.pk)
            # Generate barcode pic
            bar = barcode(str(invoice.barcode))

            if invoice.kind in [3, 4, 5, 6]:
                template = get_template("bill_credit.html")
            else:
                template = get_template("bill_invoice_bolpdf.html")

            kind = data["invoice"].get_kind_display().upper()
            domain = getattr(config, "matrix_server") #.replace("https://", "").replace("http://", "")

            # Render the template
            html = template.render(
                {"barcode": bar, "data": data, "domain": domain, "kind": kind, "color": False}
            )
            # PDF options
            options = {"page-size": "letter", "encoding": "UTF-8"}

            # PDF
            pdf = pdfkit.from_string(html, False, options)
            pdf64 = base64.b64encode(pdf).decode("UTF-8")

            service = invoice.service.all()[0]
            info = []
            info.append(
                {
                    "number": str(service.number),
                    "street": str(service.street),
                    "house_number": str(service.house_number),
                    "apartment_number": str(service.apartment_number),
                    "tower": str(service.tower),
                    "location": str(service.location),
                    "price_override": str(service.price_override),
                    "due_day": str(service.due_day),
                    "activated_on": str(service.activated_on) if service.activated_on else "",
                    "installed_on": str(service.installed_on)  if service.installed_on else "",
                    "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                    "expired_on": str(service.expired_on)  if service.expired_on else "",
                    "status": str(service.get_status_display()),
                    "network_mismatch": "Sí" if service.network_mismatch else "No",
                    "ssid": str(service.ssid),
                    "technology_kind": str(service.get_technology_kind_display()),
                    "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                    "seen_connected": str(service.seen_connected),
                    "mac_onu": str(service.mac_onu),
                    "composite_address": str(service.customer.composite_address),
                    "commune": str(service.node.commune),
                    "node__code": str(service.node.code),
                    "node__address": str(service.node.address),
                    "node__street": str(service.node.street),
                    "node__house_number": str(service.node.house_number),
                    "node__commune": str(service.node.commune),
                    "node__olt_config": str(service.node.olt_config)  if service.node.olt_config else "",
                    "node__is_building": "Sí" if service.node.is_building else "No", 
                    "node__is_optical_fiber": "Sí" if service.node.is_optical_fiber else "No",
                    "node__gpon":  "Sí" if service.node.gpon else "No",
                    "node__gepon":  "Sí" if service.node.gepon else "No",
                    "node__pon_port": str(service.node.pon_port),
                    "node__box_location": str(service.node.box_location),
                    "node__splitter_location": str(service.node.splitter_location),
                    "node__expected_power": str(service.node.expected_power),
                    "node__towers": str(service.node.towers),
                    "node__apartments": str(service.node.apartments),
                    "node__phone": str(service.node.phone),
                    "plan__name": str(service.plan.description_facturacion),
                    "plan__price": str(service.plan.price),
                    "plan__category": str(service.plan.category),
                    "plan__uf":  "Sí" if service.plan.uf else "No",
                    "plan__active": "Sí" if service.plan.active else "No", 
                    "customer__rut": str(service.customer.rut),
                    "customer__name": str(service.customer.name),
                    "customer__first_name": str(service.customer.first_name),
                    "customer__last_name": str(service.customer.last_name), ###NUEVO###
                    "customer__email": str(service.customer.email),
                    "customer__street": str(service.customer.street),
                    "customer__house_number": str(service.customer.house_number),
                    "customer__apartment_number": str(
                        service.customer.apartment_number
                    ),
                    "customer__tower": str(service.customer.tower),
                    "customer__location": str(service.customer.location),
                    "customer__phone": str(service.customer.phone),
                    "customer__default_due_day": str(
                        service.customer.default_due_day
                    ),
                    "customer__composite_address": str(
                        service.customer.composite_address
                    ),
                    "customer__commune": str(service.customer.commune),
                    "files": [pdf64],
                    "invoice__kind":str(kind),
                    "invoice__total_pagar": data["total_pagar"],
                    "customer__id": str(service.customer.id),
                    "customer_html": str("<strong>hola</strong>"),
                }
            )
                #print ('info',info)


            data = {"event":  getattr(config, "evento_send_invoice"), "info": info}
            url = "https://Backend.iris7.cl/api/v1/communications/trigger_event/active/"
            print (url)
            print(data, url, invoice.customer.email)
            
            try:
                send = requests.post(
                    url,
                    json=data,
                    headers={"Authorization": config.invoices_to_iris_task_token,},
                    timeout=30,
                ).json()
                print(send)
            except Exception as e:
                print("invoices not sended", e)

   