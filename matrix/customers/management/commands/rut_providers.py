from django.core.management import BaseCommand
from django.utils.translation import ugettext_lazy as _
from datetime import date, datetime, timedelta
from providers.models import Provider, ProviderPayorRut

class Command(BaseCommand):
    def handle(self, *args, **options):

        # Get all the providers  
        providers = Provider.objects.all()
        for i in providers:
            ProviderPayorRut.objects.create(provider=i, rut=i.rut, name=i.name)

        print("Listo")

