# coding: utf-8
from io import BytesIO
from django.core.management import BaseCommand
import petl as etl
from customers.models import Customer
from customers.models import Invoice
from localflavor.cl.forms import CLRutField

RUT_FIELD = CLRutField()

DATE_PARSER = etl.dateparser('%d-%m-%Y')

INVOICE_KINDS = {'BOLETA ELECTRONICA': Invoice.BOLETA,
                 'FACTURA ELECTRONICA': Invoice.FACTURA}

CONSTRAINTS = [
    dict(name='kind_enum',
         field='DOCUMENTO',
         assertion=lambda v: v in INVOICE_KINDS.keys()),
    dict(name='folio_int',
         field='FOLIO',
         test=int),
    dict(name='due_date',
         field='FECHA',
         test=DATE_PARSER),
    dict(name='rut_valid',
         field='RUT',
         test=RUT_FIELD.clean),
    dict(name='amount_int',
         field='TOTAL',
         test=int)
]


def validate_data(table, constraints):
    t2 = etl.addrownumbers(table)
    problems = etl.validate(t2, constraints=constraints)
    rows_with_errors = set(problems.columns()['row'])
    invalid = etl.selectin(t2, 'row', rows_with_errors)
    valid = etl.selectnotin(t2, 'row', rows_with_errors)
    return valid, invalid


class Command(BaseCommand):
    def handle(self, *args, **options):
        raw_table = etl.io.xlsx.fromxlsx('sample_data/ciclo_facturacion_septiembre.xlsx')
        valid, invalid = validate_data(raw_table, CONSTRAINTS)
        not_found = [raw_table.header()]

        for t in valid.data():
            try:
                cc = Customer.objects.get(rut=t[4])
                comment = "{} #{} - Ingreso Manual".format(t[1], t[2])
                ii, _ = Invoice.objects.update_or_create(customer=cc,
                                                         kind=INVOICE_KINDS[t[1]],
                                                         folio=t[2],
                                                         defaults={'due_date': DATE_PARSER(t[3]),
                                                                   'comment': comment,
                                                                   'total': t[6]})
            except Customer.DoesNotExist:
                not_found.append(t[1:])

        invalid_buffer = BytesIO()
        valid.cutout('row').toxlsx(invalid_buffer)
        invalid_buffer.seek(0)

        not_found_buffer = BytesIO()
        etl.fromcolumns(not_found).toxlsx(not_found_buffer)
        not_found_buffer.seek(0)
