# coding: utf-8
from pdb import set_trace
from django.core.management import BaseCommand
import petl as etl
from model_mommy import mommy
from lepl.apps.rfc3696 import Email
from localflavor.cl.forms import CLRutField
from django.contrib.auth.models import User
from customers.models import Service
from customers.models import Customer
from customers.models import Service
from infra.models import Node


rut_field = CLRutField()


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Clearing things up
        Service.objects.all().delete()
        Service.objects.all().delete()
        Customer.objects.all().delete()
        Node.objects.all().delete()
        user = User.objects.get(pk=1)
        node = mommy.make('infra.Node')

        email_validator = Email()
        customers = etl.fromcsv('active_customers.csv')
        t1 = etl.cut(customers,
                     'Número',
                     'Cliente',
                     'RUT',
                     'Direccion',
                     'Comuna',
                     'Mail',
                     'Teléfono',
                     ' Vencimiento',
                     'Plan ')
        t2 = etl.rename(t1, {'Número': 'number',
                             'Cliente': 'name',
                             'RUT': 'rut',
                             'Direccion': 'address',
                             'Comuna': 'commune',
                             'Mail': 'email',
                             'Teléfono': 'phone',
                             ' Vencimiento': 'due_day',
                             'Plan ': 'service'})
        t3 = etl.convert(t2, 'rut', str.strip)
        t4 = etl.convert(t3, 'due_day', int)
        t5 = etl.convert(t4, 'number', int)
        t6 = etl.convert(t5, 'due_day', lambda v: 5 if not v else v)
        t7 = etl.convert(t6, 'rut', rut_field.clean)
        t8 = etl.selectnotnone(t6, 'rut')
        print(t8.head())

        for row in t8.dicts():
            # Get or create service
            service, created = Service.objects.get_or_create(name=row['service'],
                                                             price=0)  # FIXME

            # Update or create customer
            customer_data = {k: row[k] for k in ('name',
                                                 'address',
                                                 'email',
                                                 'commune',
                                                 'phone')}
            customer, created = Customer.objects.update_or_create(rut=row['rut'],
                                                                  defaults=customer_data)

            # Get or create service (by number)
            # need: number, customer, service, due_day, seller, node
            service_data = {'service': service,
                             'customer': customer,
                             'due_day': row['due_day'],
                             'seller': user,
                             'node': node}
            Service.objects.update_or_create(number=row['number'],
                                              defaults=service_data)
