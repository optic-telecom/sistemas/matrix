# coding: utf-8
import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        #Busca los usuarios que no tienen el cambio de servicios.
        clientes=Customer.objects.all()

        #Crea el archivo del reporte
        workbook = xlsxwriter.Workbook('customers_sin_servicio.xlsx')
        worksheet = workbook.add_worksheet()

        count_row=1
        #Crea los titulos de las columnas.

        worksheet.write(0,0,"ID cliente")
        worksheet.write(0,1,"Nombre")
        worksheet.write(0,2,"Balance final")
        worksheet.write(0,3, "Email")
        
        for c in clientes:
            #Si no tiene servicios
            if c.service_set.all().count()==0:
                #Si tiene pagos o boletas.
                if c.payment_set.all().count()!=0 or c.invoice_set.all().count()!=0:
                    #Guarda los valores en el excel.
                    
                    worksheet.write(count_row,0,c.id)
                    worksheet.write(count_row,1, c.name)
                    worksheet.write(count_row,2, c.get_statement()['final_balance'])
                    worksheet.write(count_row,3, c.email)
                    count_row=count_row+1
                    
        workbook.close()
        print("Listo")