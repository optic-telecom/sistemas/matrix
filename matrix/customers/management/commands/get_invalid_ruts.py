# coding: utf-8
from django.core.exceptions import ValidationError
from django.core.management import BaseCommand
from customers.models import Customer
from localflavor.cl.forms import CLRutField

rut_field = CLRutField()


class Command(BaseCommand):
    def handle(self, *args, **options):
        for customer in Customer.objects.all():
            try:
                rut_field.clean(customer.rut)
            except ValidationError:
                print("https://optic.matrix2.cl/customers/{}/edit/".format(customer.pk))
