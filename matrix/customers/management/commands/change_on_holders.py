from django.core.management import BaseCommand
from customers.models import *
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta
import pandas as pd
pd.set_option('mode.chained_assignment', None)
from providers.models import Provider
from reports.models import ServiceStatusLog

class Command(BaseCommand):
    def add_arguments(self, parser):
            parser.add_argument('--arg1', help="Dirección del documento", type=str)

    def handle(self, *args, **options):
    
        if options['arg1'] is None:
            print("El comando se debe ejecutar con python manage.py update_providers --arg1 [excel_path]")
            print("Help: [excel_path] es un str, que indica la direccion del archivo excel")
            exit()

        path=str(options['arg1'])

        dfBeta = pd.read_excel(path, sheet_name = 'Beta')
        #print(dfBeta)
        to_change_beta =[]
        for x in dfBeta["Numero"]:
            to_change_beta.append(x)
        
        for x in to_change_beta:
            hist = HistoricalService.objects.get(pk = x)
            #print(hist)
            #print(hist.customer)
            #print(hist.status)
            if hist:
                if hist.status == 8:
                    hist.status = 10
                elif hist.status == 9:
                    hist.status = 11
                hist.save()


        dfAlfa = pd.read_excel(path, sheet_name = 'Alfa')

        #print(dfAlfa)
        to_change = []
        for x in dfAlfa["Numero"]:
            to_change.append(x)
        
        for x in to_change:
            ser = Service.objects.get(number = int(x))
            if ser:
                hh = ser.history.order_by("history_date")
                if ser.status == 8:
                    if hh:
                        count = len(hh)-1
                        while hh[count].status == 8:
                            #print(ser, hh[count].status)
                            hh[count].status = 10
                            hh[count].save()
                            count = count - 1
                    ser.status = 10
                elif ser.status == 9:
                    if hh:
                        count = len(hh)-1
                        while hh[count].status == 9:
                            #print(ser, hh[count].status)
                            hh[count].status = 11
                            hh[count].save()
                            count = count - 1
                    ser.status = 11
                ser.save()
            #print(ser.customer.name)
            #print(ser.number)
            #print(ser.status)

        dfGamma = pd.read_excel(path, sheet_name = 'Gamma')
        #print(dfGamma)
        to_change_gamma = []
        for x in dfGamma["Numero"]:
            to_change_gamma.append(x)

        for x in to_change_gamma:
            q = ServiceStatusLog.objects.get(pk = x)
            #print(q)
            #print(q.service.customer.name)
            #print(q.status)
            if q:
                if q.status == 8:
                    q.status = 10
                elif q.status == 9: 
                    q.status = 11
                q.save()

        print("ready")

        
        