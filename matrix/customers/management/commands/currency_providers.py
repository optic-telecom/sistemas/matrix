# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Payment, Plan, Service, Currency
from providers.models import Provider
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Obtiene todos los planes.

        providers=Provider.objects.all()
        print("Total de proveedores:")
        print(providers.count())
        count=0
        for p in providers:
            # Si la compania del operador tiene default, se lo asocia.
            if p.operator.company.currency and p.currency==None:
                p.currency=p.operator.company.currency
                #print(p.currency)
                count=count+1
                p.save()
        print("Cuenta de cuantos se asociaron:")
        print(count)
