# coding: utf-8
import requests
from django.core.management import BaseCommand
from constance import config
from django.utils.translation import ugettext_lazy as _
import json, datetime
from datetime import datetime
from customers.models import *
from django.contrib.auth.models import Group, User

class Command(BaseCommand):
    def handle(self, *args, **options):
        usuarios=User.objects.filter(is_active=True)
        company=Company.objects.get(id=1)
        operadores=Operator.objects.all()
        for o in operadores:
            #print(usuarios)
            if o.company==None:
                o.company=company
                o.save()
            else:
                # Ya lo tiene asociado.
                print(o)
        for u in usuarios:
            #print(usuarios)
            if UserCompany.objects.filter(user=u,company=company).count()==0:
                usercomp=UserCompany.objects.create(user=u)
                usercomp.company.add(company)
                usercomp.save()
                if UserOperator.objects.filter(user=u).count()==0:
                    UserOperator.objects.create(user=u,company=1,operator=0)
            else:
                # Ya lo tiene asociado.
                print(u)

            