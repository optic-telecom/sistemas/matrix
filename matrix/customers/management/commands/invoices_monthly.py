# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Invoice
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # WORKBOOK CREATION AND WORKSHEET ADDING
        workbook = xlsxwriter.Workbook("reporte de documentos emitidos por matrix.xlsx")
        worksheet = workbook.add_worksheet()

        # XLS HEADER
        worksheet.write(0, 0, "DOCUMENTO")
        worksheet.write(0, 1, "FECHA-Y/M/D")
        worksheet.write(0, 2, "HORA")
        worksheet.write(0, 3, "CLIENTE")
        worksheet.write(0, 4, "FOLIO")
        worksheet.write(0, 5, "SERVICIOS")
        worksheet.write(0, 6, "TOTAL")

        # Ciclos de facturación
        start = "2020-07-20"
        end = "2020-08-20"

        # REPORT
        invoices = (
            Invoice.objects.filter(Q(created_at__gte=start, created_at__lt=end))
            .filter(Q(kind=1) | Q(kind=2))
            .order_by("kind", "-created_at")
        )

        r = 1
        for invoice in invoices:
            worksheet.autofilter("A1:G" + str(r))

            kind = dict(invoice.INVOICE_CHOICES)[invoice.kind]
            date = invoice.created_at.strftime("%Y/%m/%d")
            time = invoice.created_at.strftime("%H:%M:%S")
            customer = invoice.customer.name
            folio = invoice.folio
            total = invoice.total

            allServices = []
            for service in invoice.service.all():
                allServices.append(service.number)

            serviceString = str(allServices).replace("[", "").replace("]", "")

            print(r, kind, date, time, customer, folio, total, allServices)

            # Writing data row by row
            worksheet.write(r, 0, kind)
            worksheet.write(r, 1, date)
            worksheet.write(r, 2, time)
            worksheet.write(r, 3, customer)
            worksheet.write(r, 4, folio)
            worksheet.write(r, 5, serviceString)
            worksheet.write(r, 6, total)

            r += 1

        workbook.close()
