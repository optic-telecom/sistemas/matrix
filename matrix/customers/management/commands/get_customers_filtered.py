# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Customer, Service
from django.utils.translation import ugettext_lazy as _


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Create a workbook and add a worksheet.
        workbook = xlsxwriter.Workbook("get_customers_filtered.xlsx")
        worksheet = workbook.add_worksheet()
        services = Service.objects.all().order_by("-id")
        r = 1
        col = 0
        # Header
        worksheet.write(0, col, "ID Cliente")
        worksheet.write(0, col + 1, "Nombre")
        worksheet.write(0, col + 2, "ID Servicio")
        worksheet.write(0, col + 3, "Número")
        worksheet.write(0, col + 4, "Calle")
        worksheet.write(0, col + 5, "Torre")
        worksheet.write(0, col + 6, "N. Apartamento")
        worksheet.write(0, col + 7, "N. Casa")
        worksheet.write(0, col + 8, "Ubicación")
        worksheet.write(0, col + 9, "Estatus")
        for s in services:
            # Filter
            worksheet.autofilter("A1:J" + str(r))
            status = str(_(dict(s.STATUS_CHOICES)[s.status]))
            name = s.customer.name if s.customer.name != "" else "Sin nombre"
            number = s.number if s.number != "" else "Sin número de servicio"
            street = s.best_street if s.best_street != "" else "Sin calle"
            tower = s.best_tower if s.best_tower != "" else "Sin torre"
            if s.best_apartment_number == "sin depto." or s.best_apartment_number == "":
                apartment_number = "Sin Apartamento"
            else:
                apartment_number = s.best_apartment_number
            house_number = (
                s.best_house_number if s.best_house_number != "" else "Sin N. Casa"
            )
            location = s.best_location if s.best_location != "" else "Sin ubicación"
            row = [
                s.customer.id,
                name,
                s.id,
                number,
                street,
                tower,
                apartment_number,
                house_number,
                location,
                status,
            ]
            # Write data out row by row
            worksheet.write(r, col, row[0])
            worksheet.write(r, col + 1, row[1])
            worksheet.write(r, col + 2, row[2])
            worksheet.write(r, col + 3, row[3])
            worksheet.write(r, col + 4, row[4])
            worksheet.write(r, col + 5, row[5])
            worksheet.write(r, col + 6, row[6])
            worksheet.write(r, col + 7, row[7])
            worksheet.write(r, col + 8, row[8])
            worksheet.write(r, col + 9, row[9])
            r += 1
        workbook.close()
