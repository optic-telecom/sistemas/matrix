# coding: utf-8
import magic
from os import unlink
from pydub import AudioSegment
from django.core.management import BaseCommand
from customers.models import Call


class Command(BaseCommand):
    def handle(self, *args, **options):
        # clear calls that don't show up as files
        calls = Call.objects.exclude(recording='')
        for call in calls:
            try:
                call.recording._get_size_from_underlying_file()
            except (FileNotFoundError, ValueError):
                call.recording = None
                call.save()
                print("cleared missing file on call #{}".format(call.pk))

        # convert wavs to webm
        calls = Call.objects.filter(recording__icontains='wav')
        for call in calls:
            call.recording.file.seek(0)
            segment = AudioSegment.from_file(call.recording)
            old_path = call.recording.file.name
            new_name = "{}.{}".format(call.pk, 'webm')
            call.recording.save(new_name, segment.export(format='webm'))
            #unlink(old_path)
            print("converted {} to {}".format(old_path, new_name))
