import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import *
from reports.models import ServiceStatusLog

class Command(BaseCommand):
    def handle(self, *args, **options):
        services = Service.objects.all()
        
        on_holders = []
        for service in services:
            if service.status == 12 or service.status == 13:
                on_holders.append(service)


        workbook = xlsxwriter.Workbook('on_holders_stage.xlsx')
        worksheet = workbook.add_worksheet("Alfa")
        worksheet.write(0,0,"Nombre")
        worksheet.write(0,1,"Numero")
        worksheet.write(0,2,"Status")
        row = 1
        for x in on_holders:
            if x.customer.name:
                worksheet.write(row,0,x.customer.name)
            if x.number:
                worksheet.write(row,1,x.number)
            if x.status:
                worksheet.write(row,2,x.status)
            row = row+1
        

        worksheetBeta = workbook.add_worksheet("Beta")
        worksheetBeta.write(0,0,"Nombre")
        worksheetBeta.write(0,1,"Numero")
        worksheetBeta.write(0,2,"Status")
        row = 1
        for service in services:
            for hh in service.history.order_by("history_date"):
                if hh.status == 12 or hh.status == 13:
                    worksheetBeta.write(row,0,hh.customer.name)
                    worksheetBeta.write(row,1,hh.pk)
                    worksheetBeta.write(row,2,hh.status)
                    row = row+1
        
        worksheetGamma = workbook.add_worksheet("Gamma")
        worksheetGamma.write(0,0,"Nombre")
        worksheetGamma.write(0,1,"Numero")
        worksheetGamma.write(0,2,"Status")
        row = 1

        qs = ServiceStatusLog.objects.all()
        for q in qs:
            if q.status == 12 or q.status == 13:
                if q.service:
                    worksheetGamma.write(row,0,q.service.customer.name)
                    worksheetGamma.write(row,1,q.pk)
                    worksheetGamma.write(row,2,q.status)
                    row = row+1