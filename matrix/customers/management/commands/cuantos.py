# coding: utf-8
import requests
from django.utils import timezone
from django.core.management import BaseCommand
from customers.models import Service
from reports.models import ServiceSumSnapshot


class Command(BaseCommand):
    def handle(self, *args, **options):
        sad_gorilla = '🦍'
        happy_monkey = '🐒'
        muscle = '💪'
        entero_pollo = '🐤'
        happy_face = '😃'
        cake = '🎂'
        corneta = '🎉'
        plus = '➕'
        minus = '➖'

        yesterday = ServiceSumSnapshot.objects.filter(status=Service.ACTIVE).latest('created_at')

        try:
            before_yesterday = ServiceSumSnapshot.objects.get(created_at=yesterday.created_at-timezone.timedelta(days=1),
                                                              status=Service.ACTIVE)
            delta = yesterday.count - before_yesterday.count
        except ServiceSumSnapshot.DoesNotExist:
            before_yesterday = None
            delta = yesterday.count
        
        active_count = yesterday.count

        if delta > 0:
            if active_count >= 5000:
                text = "Servicios activos >> {} {} {} {} ({} {})".format(active_count, happy_face, cake, corneta, plus, delta)
            else:
                text = "Servicios activos >> {} ({} {} {})".format(active_count, plus, delta, happy_monkey)
        elif delta == 0:
            text = "Servicios activos >> {} (igual que ayer {})".format(active_count, entero_pollo)
        else: # delta < 0
            text = "Servicios activos >> {} ({} {} {}, vamos que se puede {})".format(active_count, minus, abs(delta), sad_gorilla, muscle)

        requests.post('https://slack.com/api/chat.postMessage',
                      data={'token': 'xoxp-381128721780-474588365840-490282789413-bf1f81420408327912250c85ada60c34', 
                            'channel': 'los5000', 'text': text, 'as_user': True})
