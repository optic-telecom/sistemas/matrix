# coding: utf-8
import csv
import collections
from django.core.management import BaseCommand
from customers.models import Service
from infra.models import Node, NetworkEquipment

class Command(BaseCommand):
    def handle(self, *args, **options):
      

        nodos = Node.objects.filter().order_by('mikrotik_ip')
        ips = []
        ips_repetidas = []

        for nodo in nodos:
            #nombre del nodo, dirección, comuna, tecnología y planes disponibles

            ips_repetidas.append([str(nodo.code),
                                 str(nodo.address),
                                 str(nodo.commune),
                                 str('FIBRA' if nodo.is_optical_fiber else 'UTP'),
                                 str(" | ".join([x.name + " (precio -> " + str(x.price) + ')' for x in nodo.plans.all()])),
                                 str(nodo.get_status_display()),
                                 str(nodo.parent.code if nodo.parent else 'Sin padre')
                                 ])

        with open('Planes por nodos.csv', 'w') as myFile:
            writer = csv.writer(myFile, delimiter=';')
            writer.writerows(ips_repetidas)

        exit()


        ips_repetidas = []
        for nodo in nodos:
            if nodo.mikrotik_ip not in ips:
                ips.append(nodo.mikrotik_ip)
            else:
                ips_repetidas.append([str(nodo.mikrotik_ip),str(nodo.code)])
        
        print('ips repetidas',str(len(ips_repetidas)))

        with open('nodos_ips_duplicadas.csv', 'w') as myFile:
            writer = csv.writer(myFile, delimiter=';')
            writer.writerows(ips_repetidas)


        
        qs_all = NetworkEquipment.objects.filter().exclude(service__isnull=True, ip='1.1.1.1')
        ips_qs = NetworkEquipment.objects.exclude(ip='1.1.1.1').values_list('ip')
        lista_ips = [x[0] for x in list(ips_qs)]
        duplicadas = [x for x, y in collections.Counter(lista_ips).items() if y > 1]
        data_reporte = []

        for equipo in qs_all:
            print ('equipo ',equipo.id)
            s = equipo.service

            if s.status == 2 or s.status == 3:
                #retirados y por retirar eliminar
                try:
                    #print ("borrar:", equipo.ip)
                    data_reporte.append([s.number, 
                        s.get_status_display(),
                        'Equipo borrado por status del servicio',
                        equipo.serial_number,
                        equipo.ip,
                    ])                    
                    equipo.delete()
                except Exception as e:
                    print ('Equipo no borrado status, e->',str(e), equipo.id, equipo.ip,equipo.serial_number)
                

            if s.network_status[0] == 'danger' and not s.status == 1:
                #cortados 1.1.1.1
                #print ('aqui buscar duplicados')
                if equipo.ip in duplicadas:
                    #print (equipo.ip)
                    
                    data_reporte.append([s.number, 
                        "%s y cortado en MK" % s.get_status_display(),
                        'Equipo actualizado a 1.1.1.1 por ip duplicada',
                        equipo.serial_number,
                        equipo.ip,
                    ])                    

                    equipo.ip = '1.1.1.1'
                    equipo.save()

            if equipo.ip == '0.0.0.0':
                # equipos ya no usados, borrar
                try:
                    data_reporte.append([s.number, 
                        s.get_status_display(),
                        'Equipo borrado por tener ip 0.0.0.0',
                        equipo.serial_number,
                        equipo.ip,
                    ])                    
                    equipo.delete()
                except Exception as e:
                    print ('Equipo no borrado ceros, e->',str(e), equipo.id, equipo.ip,equipo.serial_number)

        with open('reporte_ips_duplicadas.csv', 'w') as myFile:
            writer = csv.writer(myFile,delimiter=';')
            writer.writerows(data_reporte)                