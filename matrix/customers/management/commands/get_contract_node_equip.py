# coding: utf-8
import csv
from django.core.management import BaseCommand
from infra.models import NetworkEquipment as E, Gear
from customers.models import Service


class Command(BaseCommand):
    def handle(self, *args, **options):
        services = Service.objects.all()
        data = []
        for service in services[:100]:
            eq = E.objects.filter(service=service, primary=True).reverse()
            equipo = None
            if eq.exists():
                equipo = eq[0]

            ip = equipo.ip if equipo and equipo.ip else "sin_ip"
            mac = equipo.mac if equipo and equipo.mac else "sin_mac"
            row = [service.number, service.customer.name, service.node, ip, mac]
            print(row)
            data.append(row)

        myFile = open("get_contract_node_equip.csv", "w")
        with myFile:
            writer = csv.writer(myFile)
            writer.writerows(data)
