# coding: utf-8
import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from django.contrib.auth.models import Group, Permission
from datetime import date


class Command(BaseCommand):
    def handle(self, *args, **options):
        json_final={}
        for group in Group.objects.all():
            #print(group)
            permissions = group.permissions.all()
            perms_name=[]
            for perm in permissions:
                perms_name.append(perm.name)
            #print(perms_name)
            json_final[group.name]=perms_name
        print(json_final)
        print("Listo")

        