from django.core.management import BaseCommand
from customers.models import Payment, Plan, Service, Operator
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta
import pandas as pd
pd.set_option('mode.chained_assignment', None)
from providers.models import Provider

class Command(BaseCommand):
    def add_arguments(self, parser):
            parser.add_argument('--arg1', help="Dirección del documento", type=str)

    def handle(self, *args, **options):
    
        if options['arg1'] is None:
            print("El comando se debe ejecutar con python manage.py update_providers --arg1 [excel_path]")
            print("Help: [excel_path] es un str, que indica la direccion del archivo excel")
            exit()

        path=str(options['arg1'])
    
        df = pd.read_excel(path, sheet_name = 'DETALLE')

        providers = Provider.objects.all()
        for idx in range(0,len(df)):
            if (len(df['RUT'][idx]) == 10 ):
                df['RUT'][idx] = df['RUT'][idx][0:2] + '.' + df['RUT'][idx][2:5] + '.' + df['RUT'][idx][5:]
    
        
        for provider in providers:
            provider.operator = Operator.objects.get(id = 1)
            provider.save()

        for idx in range(0,len(df)):
            if not providers.filter(name=df['RAZON SOCIAL'][idx]).exists():
                if not providers.filter(rut=df['RUT'][idx]).exists():
                    new_provider = Provider(name = df['RAZON SOCIAL'][idx],
                    rut = df['RUT'][idx],
                    email = '',
                    street = '',
                    house_number = '',
                    apartment_number = '',
                    tower = '',
                    location = '',
                    phone = '',
                    operator_id = 2,
                    notes = 'Direccion: ' + df['DIRECCION PROVEEDOR'][idx], 
                    )
                    new_provider.save()

