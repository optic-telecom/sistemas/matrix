# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Customer, Service
from infra.models import NetworkEquipment


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Create a workbook and add a worksheet.
        workbook = xlsxwriter.Workbook("clientes_gpon.xlsx")
        worksheet = workbook.add_worksheet()
        r = 1
        col = 0
        # Header
        worksheet.write(0, col, "ID SERVICIO")
        worksheet.write(0, col + 1, "CLIENTE")
        worksheet.write(0, col + 2, "N. SERVICIO")
        worksheet.write(0, col + 3, "PLAN")
        worksheet.write(0, col + 4, "NODO")
        worksheet.write(0, col + 5, "IP MIKROTIK")
        worksheet.write(0, col + 6, "NODO DE INSTALACIÓN")
        worksheet.write(0, col + 7, "SSID")
        worksheet.write(0, col + 8, "CLAVE")
        worksheet.write(0, col + 9, "NOTAS DE INSTALACIÓN")
        worksheet.write(0, col + 10, "IP")
        worksheet.write(0, col + 11, "MARCA")
        worksheet.write(0, col + 12, "MODELO")
        worksheet.write(0, col + 13, "MAC")
        worksheet.write(0, col + 14, "NOTAS")
        for s in Service.objects.filter(technology_kind=2):
            # Filter
            worksheet.autofilter("A1:O" + str(r))
            # Rows
            number = s.number if s.number else "Sin número de servicio"
            name = s.customer.name if s.customer.name else "Sin nombre"
            plan = str(s.plan) if s.plan else "Sin plan"
            node = str(s.node) if s.node else "Sin nodo"
            mikrotik_ip = (
                s.node.mikrotik_ip if s.node.mikrotik_ip else "Sin ip mikrotik"
            )
            original_node = (
                str(s.original_node) if s.original_node else "Sin nodo de instalación"
            )
            ssid = s.ssid if s.ssid else "Sin SSID"
            password = s.password if s.password else "Sin clave"
            network_notes = (
                s.network_notes if s.network_notes else "Sin notas de instalación"
            )
            if s.primary_equipment:
                ip = s.primary_equipment.ip
                brand = s.primary_equipment.brand
                model = s.primary_equipment.model
                mac = s.primary_equipment.mac
                notes = s.primary_equipment.notes
            else:
                ip = "Sin IP"
                brand = "Sin marca"
                model = "Sin modelo"
                mac = "Sin MAC"
                notes = "Sin notas"
            # Writing rows
            worksheet.write(r, col, s.id)
            worksheet.write(r, col + 1, name)
            worksheet.write(r, col + 2, number)
            worksheet.write(r, col + 3, plan)
            worksheet.write(r, col + 4, node)
            worksheet.write(r, col + 5, mikrotik_ip)
            worksheet.write(r, col + 6, original_node)
            worksheet.write(r, col + 7, ssid)
            worksheet.write(r, col + 8, password)
            worksheet.write(r, col + 9, network_notes)
            worksheet.write(r, col + 10, ip)
            worksheet.write(r, col + 11, brand)
            worksheet.write(r, col + 12, model)
            worksheet.write(r, col + 13, mac)
            worksheet.write(r, col + 14, notes)
            r += 1
        workbook.close()
