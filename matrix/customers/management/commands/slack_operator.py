# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Payment, Plan, Service, Currency, Invoice, Operator, Company, ConfigOperator
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Obtiene todos los invoices.
        ops=Operator.objects.all()
        print("Total de operadores:")
        print(ops.count())
        count=0
        for o in ops:
            configOp=ConfigOperator.objects.filter(operator=o, name="Slack")
            if configOp.count()==0:
                ConfigOperator.objects.create(operator=o, name="Slack", flag={})
                count=count+1
        print("Cuenta de cuantos se asociaron:")
        print(count)