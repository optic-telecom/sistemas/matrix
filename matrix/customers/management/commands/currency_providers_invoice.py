# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Currency
from providers.models import Provider, Invoice
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Obtiene todos los invoices.

        invoices=Invoice.objects.all()
        print("Total de invoices:")
        print(invoices.count())
        invoices=invoices.filter(currency__isnull=True)
        print("Total de invoices sin moneda:")
        print(invoices.count())
        count=0
        for i in invoices:
            if i.provider:
                # Si el proveedor tiene default, se lo asocia.
                if i.provider.currency and i.currency==None:
                    i.currency=i.provider.currency
                    #print(i.currency)
                    count=count+1
                    i.save()
            else:
                # Si la compania tiene default, se lo asocia.
                if i.operator.company.currency and i.currency==None:
                    i.currency=i.operator.company.currency
                    #print(i.currency)
                    count=count+1
                    i.save()

        print("Cuenta de cuantos se asociaron:")
        print(count)
