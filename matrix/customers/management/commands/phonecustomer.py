# coding: utf-8
import xlsxwriter
import io, datetime, re, sys
from django.core.management import BaseCommand
from customers.models import Customer
from datetime import date
from django.utils.translation import activate

class Command(BaseCommand):
    def handle(self, *args, **options):
        print ('Inicio')
        #Busca todos los usuarios
        customers=Customer.objects.all()

        row = 1

        workbook = xlsxwriter.Workbook('formatos_telefono_para_revisar.xlsx')
        worksheet = workbook.add_worksheet()

        worksheet.write(0, 0, "ID cliente")
        worksheet.set_column(1, 1, 35)
        worksheet.write(0, 1, "Nombre")
        worksheet.set_column(2, 5, 20)
        worksheet.write(0, 2, "Teléfono")
        worksheet.write(0, 3, "Correo")
        worksheet.write(0, 4, "Número de servicio")
        worksheet.write(0, 5, "Status del servicio")
        total = customers.count()
        total_restante = total
        for c in customers:
            error = False
            #print(c.phone)
            phone = c.phone.replace(' ', '').replace('/', '').replace('+', '').replace('-', '').replace('.', '')
            #print(phone)
            if len(phone) <= 11:
                if re.match('^(56)(9|2)[0-9]{8}$', phone) is None:
                    if re.match('^(9|2)[0-9]{9}$', phone) is None:
                        error = True
                    else:
                        if len(phone)==11 and phone[:2]=="56":
                            phone = '+' + phone
                        else:
                            phone = '+56' + phone 
                else:
                    if len(phone)==11:
                        phone = '+' + phone
            else:
                error = True
            #print(phone)
            if error:
                for service in c.service_set.all().order_by('pk'):
                    activate('es')
                    worksheet.write(row, 0, c.pk)
                    worksheet.write(row, 1, c.name)
                    worksheet.write(row, 2, c.phone)
                    worksheet.write(row, 3, c.email)
                    worksheet.write(row, 4, service.number)
                    worksheet.write(row, 5, service.get_status_display())
                    row += 1      
            else:
                c.phone = phone
                c.save()
            
            total_restante = total_restante - 1
            print('{}-{}'.format(total_restante,total))
            
        workbook.close()
        print("Formateo aplicado")
