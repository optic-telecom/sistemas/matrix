# coding: utf-8
import pandas as pd
import xlrd
import xlsxwriter
import io, datetime, json
from constance import config
from datetime import date, datetime, timedelta
from django.core.management import BaseCommand
from customers.models import (Customer, Balance, Service, Invoice,
                             BalanceService, FinancialRecord,
                             BillingCycle, ProcessedBatch, InvoiceBatch)
from infra.models import Node
import xlrd 
import dateutil.parser
import pytz
from reports.models import ServiceStatusLog, ServiceChangeStatusLog
utc=pytz.UTC
#archivo = 'C:/Users/rvaquerizo/Documents/ejemplo.xlsx'


class Command(BaseCommand):
    #def add_arguments(self, parser):
    #    parser.add_argument('--arg1', help="Something helpful", type=int)

    def handle(self, *args, **options):
        data = []
        # Valores para el excel de los invoice sin cargar en Matrix.
        output_file = io.BytesIO()
        workbook = xlsxwriter.Workbook(output_file)
        worksheet = workbook.add_worksheet()

        header = ('ID','Servicio','Fecha','Status anterior')
        bold = workbook.add_format({'bold': True})
        worksheet.set_row(0, None, bold)
        worksheet.write_row(0, 0, header)
        row = 0

        services=Service.objects.all()
        #paso=False
        for service in services:
            fecha =  datetime(2020,1,1).replace(tzinfo=utc) #dateutil.parser.parse(xlrd.xldate.xldate_as_datetime(hoja.cell_value(i, 2), 0))
            delta = datetime(2021,5,25).replace(tzinfo=utc)
            try:
                logs = service.action_log()
                for hh in logs:
                    if 'changes' in hh.keys():
                        if 'status' in hh['changes']:
                            if hh['dt']>fecha and hh['dt'] < delta:
                                #paso=True
                                print(hh)
                                kind=1
                                try:
                                    if service.node.is_optical_fiber:
                                        kind=2
                                except:
                                    kind=1
                                    pass

                                if service.plan.type_plan==2:
                                    kind_plan=True
                                else:
                                    kind_plan=False

                                # Creando registro
                                ServiceChangeStatusLog.objects.create(created_at=hh['dt'],
                                                                    status=hh['current_values']['status'],
                                                                    prev_status=hh['previous_values']['status'],
                                                                    address=service.best_address,
                                                                    plan=service.plan,
                                                                    empresa=kind_plan,
                                                                    kind=kind,
                                                                    service=service)
            except Exception as e: 
                print(e)
                print("No encontro el servicio")

        print("Listo")