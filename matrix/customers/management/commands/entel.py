import json
import requests
from time import sleep
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from django.core.management import BaseCommand
from customers.models import Region, Comuna, Calle, Direccion


def requests_retry_session(
    retries=7,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session



URL_BASE = "http://personas.entel.cl/PortalPersonas/portlet/factibilidadTecnica3Play/"
URL_REGIONES = URL_BASE + "listaRegiones.do?paisID=56"
URL_COMUNAS = URL_BASE + "listaComunas.do?paisID=56&regionID={}"
URL_CALLES = URL_BASE + "listaCalles.do?paisID=56&regionID={}&comunaID={}"
URL_DIRECCIONES = URL_BASE + "listaDirecciones.do?paisID=56&calleID={}"


class Command(BaseCommand):
    def handle(self, *args, **options):
        regiones_request = requests_retry_session().post(URL_REGIONES)
        regiones_json = json.loads(regiones_request.content)

        # region sample
        # {'iD': '1', 'nombreRegion': 'TARAPACA', 'resultado': {'codigo': '1', 'glosa': 'Ok'}}

        regiones_sorted = [regiones_json[-1]] + [regiones_json[-3]] + [regiones_json[-2]] + regiones_json[0:-3]
        print(regiones_sorted)

        for rr in regiones_sorted:
            region, _ = Region.objects.update_or_create(entel_id=rr['iD'], defaults=dict(nombre=rr['nombreRegion']))
            print(region)

            while True:
                try:
                    comunas_request = requests_retry_session().post(URL_COMUNAS.format(rr['iD']))
                    comunas_json = json.loads(comunas_request.content)  # FIXME

                    # comunas sample
                    # {'iD': '20', 'nombreComuna': 'IQUIQUE'}

                    for cc in comunas_json:
                        comuna, _ = Comuna.objects.update_or_create(entel_id=cc['iD'], region=region, defaults=dict(nombre=cc['nombreComuna']))
                        print(comuna)

                        while True:
                            try:
                                calles_request = requests_retry_session().post(URL_CALLES.format(rr['iD'], cc['iD']))
                                calles_json = json.loads(calles_request.content)  # FIXME

                                # calle sample
                                # {'iD': '34719', 'nombreCalle': 'SARA', 'resultado': {'codigo': '1', 'glosa': 'Ok'}}

                                for ll in calles_json:
                                    try:
                                        ll['iD']
                                    except KeyError:
                                        continue

                                    calle, _ = Calle.objects.update_or_create(entel_id=ll['iD'], comuna=comuna, defaults=dict(nombre=ll['nombreCalle']))
                                    print(calle)
                                    while True:
                                        try:
                                            direcciones_request = requests_retry_session().post(URL_DIRECCIONES.format(ll['iD']), timeout=20)
                                            direcciones_json = json.loads(direcciones_request.content)

                                            # direccion sample
                                            # {'iDDireccion': '1348734', 'can': '', 'numeroMunicipal': '1704', 'cpn': '', 'iDSegmento': '160367', 'iDBlock': '2000561', 'iDTipoEdificacion': '1', 'lat': '-20.22522650', 'lon': '-70.13426732', 'resultado': {'codigo': '1', 'glosa': 'Ok'}}

                                            for dd in direcciones_json:
                                                direccion, _ = Direccion.objects.update_or_create(entel_id=dd['iDDireccion'], calle=calle, defaults=dict(numeroMunicipal=dd['numeroMunicipal'], lat=dd['lat'], lon=dd['lon']))
                                                print("R:{} L:{} C:{} {}".format(region.nombre, comuna.nombre, calle.nombre, direccion))

                                        except json.decoder.JSONDecodeError:
                                            sleep(5)
                                            continue
                                        except KeyError:
                                            pass
                                        break
                            except json.decoder.JSONDecodeError:
                                sleep(5)
                                continue
                            except KeyError:
                                pass
                            break
                except json.decoder.JSONDecodeError:
                    sleep(5)
                    continue
                except KeyError:
                    pass
                break
