# coding: utf-8
import xlsxwriter
import io
from django.core.management import BaseCommand
from customers.models import Plan, Service 



class Command(BaseCommand):
    def handle(self, *args, **options):
        planes = Plan.objects.all()
        data = []

        output_file = io.BytesIO()
        workbook = xlsxwriter.Workbook(output_file)
        worksheet = workbook.add_worksheet()


        header = ('Nombre plan', 'Nro servicios', 'Monto del plan','Plan activo')
        bold = workbook.add_format({'bold': True})
        #red = workbook.add_format({'font_color': 'red'})
        worksheet.set_row(0, None, bold)
        worksheet.write_row(0, 0, header)
        row = 1

        for p in planes:
            servicios = Service.objects.filter(plan=p, status=1)
            worksheet.write(row,0,str(p.name))
            worksheet.write(row,1,str(servicios.count()))
            moneda = 'UF ' if p.uf else '$'
            worksheet.write(row,2,moneda + str(p.price))
            worksheet.write(row,3,'Si' if p.active else 'No')
                
            row += 1

        workbook.close()
        output_file.seek(0)

        myFile = open('servicios_por_planes.xlsx', 'wb')
        with myFile:
            myFile.write(output_file.read())
