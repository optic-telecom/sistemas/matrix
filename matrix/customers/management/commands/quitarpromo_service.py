# coding: utf-8
import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import Customer, Balance, Service, Invoice, BalanceService, FinancialRecord, Credit, Promotions
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # From 7 days ago to now 
        daysAgo = date.today() - timedelta(7)  
        # print(daysAgo) 
        # Parte para desasociar promo de un servicio.
        promociones_completas=Credit.objects.filter(
            status_field=False, cleared_at__isnull=False,
            cleared_at__gte=daysAgo,
            reason__kind=3).values('service').distinct()

        for i in promociones_completas:
            service=Service.objects.get(id=i['service'])
            service_promo=service.promotions.all()
            promociones=Credit.objects.filter(status_field=False, cleared_at__isnull=True, reason__kind=3,service=service)
            promo_qs=Promotions.objects.none()
            for promos in promociones:
                # caso en que le falte la asociacion.
                if not (promos.reason.promo in service_promo):
                    service.add(promos.reason.promo)
                    service.save()
                # Para obtener listado de promos.
                promo_qs=promo_qs | Promotions.objects.filter(id=promos.reason.promo.id)

            # Elimino los repetidos.
            result_qs=promo_qs.distinct()

            # Obteniendo la diferencia para ver si tiene promo asociada de mas.
            qs_diff = service_promo.difference(result_qs) 
            # Elimino la relacion
            for result in qs_diff:
                service.promotions.remove(result)
                service.save()

        print("Listo")