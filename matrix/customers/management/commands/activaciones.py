from django.core.management import BaseCommand
from customers.models import *
from billing.views import barcode, invoiceDataAfter
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta
import pandas as pd
pd.set_option('mode.chained_assignment', None)
from providers.models import Provider
from reports.models import ServiceStatusLog
from constance import config
import requests
from django.template.loader import get_template
import base64, pdfkit
from django.utils import formats, timezone
import xlsxwriter

class Command(BaseCommand):
    def handle(self, *args, **options):
        workbook = xlsxwriter.Workbook('activated_later.xlsx')
        worksheet = workbook.add_worksheet("Alfa")
        worksheet.write(0,0,"Nombre")
        worksheet.write(0,1,"Numero")
        worksheet.write(0,2,"Fecha de Activacion")
        worksheet.write(0,3,"Fecha de Activacion Status")
        row = 1
        services= Service.objects.all()
        for service in services:
            alfa = service.activated_on
            beta = ''
            first, *hx = service.history.order_by("history_date")
            history = [first]
            for hh in hx:
                if history[-1].status != hh.status:
                    history.append(hh)
            for h in history:
                if h.status == 1:
                    beta = h.history_date
                    break

            if alfa and beta:
                auxAlfa = alfa + timedelta(days = 180)
                now = timezone.now()
                alfa = alfa.replace(tzinfo=None) - timedelta(days = 3)
                if alfa.replace(tzinfo=None) > beta.replace(tzinfo=None) and auxAlfa.replace(tzinfo=None) > now.replace(tzinfo=None):
                    print(alfa, beta, service.number)
                    worksheet.write(row,0,service.customer.name)
                    worksheet.write(row,1,service.number)
                    worksheet.write(row,2,str(service.activated_on))
                    worksheet.write(row,3,str(beta))
                    row += 1
