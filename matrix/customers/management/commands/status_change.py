import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import *
from reports.models import ServiceStatusLog

def relation(x):
    if x == 7 or x == 11 or x == 13:
        return 9
    elif x == 8:
        return 7
    elif x == 9:
        return 4
    elif x == 10 or x == 12:
        return 8
    else:
        return x

class Command(BaseCommand):
    def handle(self, *args, **options):
        services = Service.objects.all()
        
        for service in services:
            for hh in service.history.order_by("history_date"): 
                if hh.status >= 7:
                    hh.status = relation(hh.status)
                    hh.save()
            
            if service.status >= 7:
                service.status = relation(service.status)
                service.save()
        
        qs = ServiceStatusLog.objects.all()
        for q in qs:
            q.status = relation(q.status)
            q.save()
        print('Listo')
        

         
        