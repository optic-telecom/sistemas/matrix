import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import *
from reports.models import ServiceSumSnapshot

def mixStatus(date):
    print(date)
    snaps = ServiceSumSnapshot.objects.filter(created_at__day = date.day).filter(created_at__month = date.month).filter(created_at__year = date.year)
    dicc = dict()
    for x in snaps:
        key = str(x.status)
        dicc[key] = x 

    fourCount = 0
    if "4" in dicc.keys():
        fourCount = fourCount + dicc["4"].count 
    if "9" in dicc.keys():
        fourCount = fourCount + dicc["9"].count
    
    sevenCount = 0
    if "8" in dicc.keys():
        sevenCount = sevenCount + dicc["8"].count
    
    eightCount = 0
    if "10" in dicc.keys():
        eightCount = eightCount + dicc["10"].count

    nineCount = 0
    if "7" in dicc.keys():
        nineCount = nineCount + dicc["7"].count
    if "11" in dicc.keys():
        nineCount = nineCount + dicc["11"].count

    tenCount = 0
    if "12" in dicc.keys():
        tenCount = tenCount + dicc["12"].count

    elevenCount = 0
    if "13" in dicc.keys():
        elevenCount = elevenCount + dicc["13"].count

    
    if "4" in dicc.keys():
        dicc["4"].count = fourCount
        dicc["4"].save()
    else:
        ServiceSumSnapshot.objects.update_or_create(created_at=date, status=4, count=fourCount)

    if "7" in dicc.keys():
        dicc["7"].count = sevenCount
        dicc["7"].save()
    else:
        ServiceSumSnapshot.objects.update_or_create(created_at=date, status=7, count=sevenCount)
    
    if "8" in dicc.keys():
        dicc["8"].count = eightCount
        dicc["8"].save()
    else:
        ServiceSumSnapshot.objects.update_or_create(created_at=date, status=8, count=eightCount)

    if "9" in dicc.keys():
        dicc["9"].count = nineCount
        dicc["9"].save()
    else:
        ServiceSumSnapshot.objects.update_or_create(created_at=date, status=9, count=nineCount)

    if "10" in dicc.keys():
        dicc["10"].count = tenCount
        dicc["10"].save()
    else:
        ServiceSumSnapshot.objects.update_or_create(created_at=date, status=10, count=tenCount)

    if "11" in dicc.keys():
        dicc["11"].count = elevenCount
        dicc["11"].save()
    else:
        ServiceSumSnapshot.objects.update_or_create(created_at=date, status=11, count=elevenCount)

    if "12" in dicc.keys():
        dicc["12"].delete()
    
    if "13" in dicc.keys():
        dicc["13"].delete()

class Command(BaseCommand):
    def handle(self, *args, **options):
        
        snaps = ServiceSumSnapshot.objects.all()
        #tuplas = []
        first = 0
        for i in range(1,len(snaps)):
            cen = 0
            if snaps[i].created_at.day == snaps[i-1].created_at.day:
               if snaps[i].created_at.month == snaps[i-1].created_at.month:
                   if snaps[i].created_at.year == snaps[i-1].created_at.year:
                       cen = 1
            
            if cen == 0:
                mixStatus(snaps[i-1].created_at)
                tuplas = []

            #tuplas.append((snaps[i].status,snaps[i].count))
        mixStatus(snaps[len(snaps)-1].created_at)
            
                

            
        