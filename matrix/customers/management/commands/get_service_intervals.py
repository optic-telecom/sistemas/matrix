# coding: utf-8
import math, pytz, xlsxwriter
from customers.models import *
from datetime import date, datetime
from django.core.management import BaseCommand
from django.db.models import Value, Q, F, Max, Count, Sum
from django.utils.translation import ugettext_lazy as _


class Command(BaseCommand):
    def handle(self, *args, **options):
        workbook = xlsxwriter.Workbook("periodos_de_servicios.xlsx")
        worksheet = workbook.add_worksheet()
        r = 1
        col = 0
        # Header
        worksheet.write(0, col, "RUT")
        worksheet.write(0, col + 1, "N.SERVICIO")
        worksheet.write(0, col + 2, "ESTATUS")
        worksheet.write(0, col + 3, "PERÍODOS ACTIVOS")
        worksheet.write(0, col + 4, "DÍAS ACTIVO")
        worksheet.write(0, col + 5, "TOTAL ACTIVO")
        for s in Service.objects.all().order_by("id"):
            # Filter
            worksheet.autofilter("A1:F" + str(r))
            # Rows
            traductor = {
                str(1): "Activo",
                str(2): "Por retirar",
                str(3): "Retirado",
                str(4): "No instalado",
                str(5): "Canje",
                str(6): "Instalación rechazada",
                str(7): "Moroso",
                str(8): "Cambio de Titular",
                str(9): "Descartado",
                str(10): "Baja Temporal",
                str(11): "Perdido, no se pudo retirar"
            }
            history = HistoricalService.objects.filter(id=s.id).order_by("history_date")
            daysActive = 0
            intervals = []
            active = 0
            anterior = 0
            for h in history:
                date = h.history_date
                if anterior == 0 and h.status != 1:
                    inactive = date
                    anterior = h.status
                    continue
                if anterior == 0 and h.status == 1:
                    active = date
                    anterior = h.status
                    continue
                if anterior == 1 and h.status == 1:
                    continue
                if anterior != 1 and h.status == 1:
                    active = date
                    anterior = h.status
                    continue
                if anterior == 1 and h.status != 1:
                    anterior = h.status
                    inactive = date
                    daysActive += abs((active - inactive).days)
                    intervals.append(
                        [active.strftime("%d/%m/%Y"), inactive.strftime("%d/%m/%Y")]
                    )
            if anterior == 1:
                intervals.append([active.strftime("%d/%m/%Y"), "Actualmente"])
                daysActive += abs(
                    (
                        active.replace(tzinfo=None)
                        - datetime.now().replace(tzinfo=None)
                    ).days
                )
            if active == 0:
                intervals = False
            if intervals:
                period = ""
                for i in intervals:
                    if len(intervals) > 1:
                        period += i[0] + " - " + i[1] + ", "
                    else:
                        period = i[0] + " - " + i[1]
            else:
                period = "Nunca activado."
            if daysActive > 0:
                rest, year = math.modf(daysActive / 365)
                days, month = math.modf(rest * 365 / (365 / 12))
                year = str(int(year))
                month = str(int(month))
                day = str(int(round(days * (365 / 12), 0)))
                totalTime = year + " años, " + month + " meses, " + day + " días."
            else:
                totalTime = "Nunca activado."
            rut = s.customer.rut if s.customer.rut else "Cliente sin RUT."
            number = s.number if s.number else "Sin número de servicio."
            status = traductor[str(s.status)]
            data = [rut, number, status, period, daysActive, totalTime]
            # Writing rows
            worksheet.write(r, col, data[0])
            worksheet.write(r, col + 1, data[1])
            worksheet.write(r, col + 2, data[2])
            worksheet.write(r, col + 3, data[3])
            worksheet.write(r, col + 4, data[4])
            worksheet.write(r, col + 5, data[5])
            r += 1
        workbook.close()
