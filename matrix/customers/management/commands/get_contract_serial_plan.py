# coding: utf-8
import csv
from django.core.management import BaseCommand
from infra.models import NetworkEquipment as E

class Command(BaseCommand):
    def handle(self, *args, **options):
        equipos = E.objects.filter(contract__status=1, primary=True)
        data = []
        for equipo in equipos:
           serial = equipo.serial_number if equipo.serial_number else 'sin_serial'
           row = [equipo.contract.number, serial, equipo.contract.service.name]
           data.append(row)

        myFile = open('contrato_plan.csv', 'w')
        with myFile:
            writer = csv.writer(myFile)
            writer.writerows(data)
