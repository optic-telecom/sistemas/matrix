import json
import time
import requests
from django.db.models import Q
from infra.models import Group, Node, PlanOrderKind, Gear, NetworkEquipment, BuildingContact, Availability
from customers.models import Service, Plan, Operator, NetworkStatusChange
from django.core.management.base import BaseCommand
from django.core.files import File
from threading import Thread

import re
from unicodedata import normalize

import pprint
from customers.lefu.cl_territory import TERRITORIAL_ORGANIZATION

import sys
import multiprocessing

import logging

migration_logger = logging.getLogger('migration')
migration_logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(levelname)s -  in %(pathname)s at line %(lineno)d: %(message)s in function %(funcName)s'
)
file_handler = logging.FileHandler('../logs/migration.log')
file_handler.setLevel(logging.INFO)
#file_handler.setFormatter(formatter)
migration_logger.addHandler(file_handler)


def normalize_names(s):

    s = re.sub(
        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+",
        r"\1", normalize("NFD", s), 0, re.I)

    s = normalize('NFC', s)

    return s


def check_commune(provinces, commune_id):

    if commune_id in provinces['communes']:
        return True
    else:
        False


def check_provinces(region, commune_id):

    provinces = list(
        filter(lambda x: check_commune(x[1], commune_id),
               region['provinces'].items()))

    if len(provinces) == 1:
        return True
    else:
        return False


def find_region_name_by_location(commune_id='14204'):

    te = list(
        filter(lambda x: check_provinces(x[1], commune_id),
               TERRITORIAL_ORGANIZATION.items()))

    return te[0][1]['name']


def fixes_street_name(street):
    street = street.lower()
    street = street.replace('av. ', 'AVENIDA ')
    street = street.replace('av ', 'AVENIDA ')
    street = street.replace('\'', '')
    street = street.replace('el quijote', 'DON QUIJOTE')
    street = street.replace(' de arica', '')
    street = re.sub('^los ', '', street)
    street = street.replace('ñ', 'n')
    street = street.replace('egypto', 'egipto')
    street = street.replace('antartida', 'antartica')
    street = street.replace('ignacio loyola', 'SAN IGNACIO DE LOYOLA')
    street = street.replace('amador neghme & linderos', 'amador neghme')
    street = street.replace('7', 'siete')
    street = street.replace('3', 'tres')
    street = street.replace('6', 'seis')
    street = street.replace('5', 'cinco')
    street = street.replace('10', 'diez')
    street = street.replace('beachef', 'beaucheff')
    street = street.replace('monsenor muller', 'monsenor miller')
    # street = street.replace('AVENIDA alcalde manuel castillo ibaceta', 'monsenor miller')

    return street


def fixes_number(house_number):
    house_number = re.sub('/.*', '', house_number)
    house_number = re.sub('-.*', '', house_number)
    house_number = re.sub('^0', '0.', house_number)
    return house_number


def fixes_commune_name(commune):
    commune = commune.replace('.', '')
    commune = commune.replace('Santiago Centro', 'santiago')
    commune = commune.replace('ñ', 'n')
    commune = commune.replace('Ñ', 'N')
    return commune


# street
# av. || av -> AVENIDA
# ' -> nada
#El Quijote -> DON QUIJOTE
# empiece por los -> nada
# delete 'de arica'

# number
# contiene / -> quitar todo despues del /


class Command(BaseCommand):

    url = 'http://127.0.0.1:8000/api/v1'
    headers = {
        'Authorization':
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    }
    #url_pulso = 'https://pulso.multifiber.cl/api/v1/search-location/'
    url_pulso = 'http://127.0.0.1:9001/api/v1/search-location/'

    def add_arguments(self, parser):
        # parser.add_argument('url', type=str, help='url base del servidor de sentinel. ej: "http://localhost:8000/api/v1"',)
        parser.add_argument(
            '-u',
            '--url',
            nargs='+',
            type=str,
            help=
            'url base del servidor de sentinel. ej: "http://localhost:8000/api/v1"',
        )
        parser.add_argument(
            '-up',
            '--urlpulso',
            nargs='+',
            type=str,
            help='url base del servidor pulso.',
        )

    def handle(self, *args, **options):
        url = options['url']
        url_pulso = options['urlpulso']

        if (url):
            self.url = url[0]

        print('URL = ', self.url, ' | ', url)

        if (url_pulso):
            self.url_pulso = url_pulso[0]

        print('URL = ', self.url_pulso, ' | ', url_pulso)

        self.groups()
        self.operators()
        self.plans()
        self.plan_order_kinds()

        self.nodes()

        self.gears()

        # # # print(" ======================= ")
        # # # print(" ======================= ")
        # # # print(" SERVICIOS ")
        # # # print(" ======================= ")
        # # # print(" ======================= ")
        self.services()
        # # # # print(" ======================= ")
        # # # # print(" ======================= ")
        # # # # print(" CPE ")
        # # # # print(" ======================= ")
        # # # # print(" ======================= ")
        self.cpes()

        # # # print(" ======================= ")
        # # # print(" ======================= ")
        # # # print(" ONUS ")
        # # # print(" ======================= ")
        # # # print(" ======================= ")
        self.onus()
        self.network_status_changes()
        self.building_contacts()
        self.where_service()

        # self.availabilities()
        # self.read_all_dir()

    def groups(self):
        for g in Group.objects.all():
            # print(g)
            r = requests.post("{}/group/".format(self.url),
                              data={
                                  'id': g.id,
                                  'name': g.name
                              },
                              headers=self.headers,
                              verify=False)
            migration_logger.debug('Grupo creado')
            # print(r.text)
            # print('\n\n')

        migration_logger.info('Grupos terminados')

    def operators(self):
        for o in Operator.objects.all():
            payload = {"id": o.id, "name": o.name, "code": o.code}
            r = requests.post('{}/operator/'.format(self.url),
                              data=payload,
                              headers=self.headers,
                              verify=False)
            # print(r.text)
            migration_logger.debug('Operador creado')

        migration_logger.info('Operadores terminados')

    def insertCateogrPlan(self):
        r = requests.post('{}/type_plan/'.format(self.url),
                          data={
                              "id": 1,
                              "name": "Internet hogar"
                          },
                          headers=self.headers,
                          verify=False)
        # print(r.text)
        r = requests.post('{}/type_plan/'.format(self.url),
                          data={
                              "id": 2,
                              "name": "Internet empresas"
                          },
                          headers=self.headers,
                          verify=False)
        # print(r.text)
        r = requests.post('{}/type_plan/'.format(self.url),
                          data={
                              "id": 3,
                              "name": "Telefonía empresas"
                          },
                          headers=self.headers,
                          verify=False)
        # print(r.text)
        r = requests.post('{}/type_plan/'.format(self.url),
                          data={
                              "id": 4,
                              "name": "Red empresas"
                          },
                          headers=self.headers,
                          verify=False)
        # print(r.text)
        r = requests.post('{}/type_plan/'.format(self.url),
                          data={
                              "id": 5,
                              "name": "Televisión Hogar"
                          },
                          headers=self.headers,
                          verify=False)
        # print(r.text)

    def plans(self):
        # insert category plan
        self.insertCateogrPlan()

        # print('\n\n')
        """ OPTIMIZAR CON EL OBJ PREFETCH """
        plans = Plan.objects.select_related("operator").all()
        for p in plans:
            # # print(p, p.id)
            payload = {
                "id": p.id,
                "operator": p.operator.id,
                "name": p.name,
                "matrix_plan": p.id,
                "type_plan": p.category,
            }

            try:
                r = requests.post('{}/plan/'.format(self.url),
                                  data=payload,
                                  headers=self.headers,
                                  verify=False)
                time.sleep(0.2)
            except Exception as e:
                try:
                    r = requests.post('{}/plan/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.2)
                except Exception as e:
                    r = requests.post('{}/plan/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)

            if r.status_code == 201:
                migration_logger.debug('Plan creado')
            elif r.status_code == 400 or r.status_code == 401:
                migration_logger.warning('Error al crear plan')
                migration_logger.warning(r.json())

            # print(r.text)
            # print('\n')

            for h in p.history.all():

                # print(h)
                payload = {
                    'id': h.id,
                    'created': h.created_at,
                    'name': h.name,
                    'history_date': h.history_date,
                    'history_type': h.history_type,
                    'operator_id': h.operator.id if h.operator else None,
                    'type_plan_id': h.category
                }

                try:
                    r = requests.post('{}/plan/{}/add_history/'.format(
                        self.url, p.id),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.2)
                except Exception as e:
                    try:
                        r = requests.post('{}/plan/{}/add_history/'.format(
                            self.url, p.id),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                        time.sleep(0.2)
                    except Exception as e:
                        try:
                            r = requests.post('{}/plan/{}/add_history/'.format(
                                self.url, p.id),
                                              data=payload,
                                              headers=self.headers,
                                              verify=False)
                            time.sleep(0.2)
                        except Exception as e:
                            pass
                # print(r.text)
        migration_logger.info('Planes terminados')

    def plan_order_kinds(self):

        for pok in PlanOrderKind.objects.all():
            payload = {"id": pok.id, "name": pok.name}
            try:
                r = requests.post('{}/planorderkind/'.format(self.url),
                                  data=payload,
                                  headers=self.headers,
                                  verify=False)

            except Exception as e:
                try:
                    r = requests.post('{}/planorderkind/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)

                except Exception as e:
                    r = requests.post('{}/planorderkind/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
            if r.status_code == 201:
                migration_logger.debug('Plan Order Kind creado')
            elif r.status_code == 400 or r.status_code == 401:
                migration_logger.warning('Error al crear Plan Order Kind')
                migration_logger.warning(r.json())
            # print(r.text)
        migration_logger.info('Plan Order Kinds terminados')

    ANTENAS = [
        21,
        42,
        78,
        79,
        99,
        102,
        120,
        125,
        127,
        128,
        129,
        130,
        132,
        134,
        135,
        136,
        137,
        138,
        139,
        140,
        141,
        142,
        143,
        145,
        146,
        147,
        150,
        151,
        159,
        160,
        162,
        163,
        164,
        166,
        167,
        168,
        169,
        170,
        171,
        176,
        177,
        178,
        188,
        189,
        193,
        204,
        207,
        209,
        220,
        225,
        241,
        242,
        243,
    ]

    i = 0

    def nodes(self):
        nodes_uuid = []
        nodes_initial = Node.objects.prefetch_related("plan_order_kinds",
                                                      "plans")
        migration_logger.info(len(nodes_initial))
        migration_logger.info(
            f'CANTIDAD EXCLUDE IN ANTENAS {len(nodes_initial.exclude(id__in=self.ANTENAS))}'
        )
        for n in nodes_initial.exclude(id__in=self.ANTENAS):
            node = self.request_nodes(n, 'utp', 1)
            if node:
                nodes_uuid.append(node)
            else:
                migration_logger.info('NOT NODE')

        migration_logger.info(f'PRIMERA CANTIDAD {len(nodes_uuid)}')
        for n in nodes_uuid:
            self.request_update_nodes(n, 'utp')

        # self.request_history_node('utp')

        nodes_uuid = []
        migration_logger.info(
            f'CANTIDAD FILTER IN ANTENAS {len(nodes_initial.filter(id__in=self.ANTENAS))}'
        )
        for n in nodes_initial.filter(id__in=self.ANTENAS):
            node = self.request_nodes(n, 'utp', 2)
            if node:
                nodes_uuid.append(node)
            else:
                migration_logger.info('NOT NODE')
        migration_logger.info(f'SEGUNDA CANTIDAD {len(nodes_uuid)}')

        for n in nodes_uuid:
            self.request_update_nodes(n, 'utp')

        # self.request_history_node('utp')

        # print(self.i)
        migration_logger.info('Nodos terminados')

    def request_nodes(self, n, model, tipo):

        plan_order_kinds = []
        plans = []

        for pok in n.plan_order_kinds.all():
            plan_order_kinds.append(pok.id)
        for plan in n.plans.all():
            plans.append(plan.id)

        dire_id = self.request_direction(
            fixes_commune_name(normalize_names(n.commune)),
            fixes_street_name(normalize_names(n.street)),
            house_number=fixes_number(n.house_number)
            if n.house_number else None,
            region=normalize_names(
                find_region_name_by_location(str(n.location)).replace(
                    'Región ', '')))

        # print('id_pulso: ', dire_id)
        # print(model)

        if dire_id:
            payload = {
                "id": n.id,
                "group": n.group.id if n.group else None,
                "alias": n.code,
                "status": n.status,
                "address_pulso": dire_id,
                # "parent": n.parent.id if n.parent else None,
                # "default_connection_node": n.default_connection_node.id if n.default_connection_node else None,
                # "is_building": n.is_building  ,
                "towers": n.towers,
                "apartments": n.apartments,
                "phone": n.phone,
                # "antennas_left": n.antennas_left  ,
                "ip": n.mikrotik_ip,
                "ip_range": n.ip_range,
                "comment":
                (n.notes[:200] + '...') if len(n.notes) > 200 else n.notes,
                "no_address_binding": n.no_address_binding,
                'plans': plans,
                'plan_order_kinds': plan_order_kinds,
                'matrix_seller': n.seller.id,
                'user': n.mikrotik_username,
                'password': n.mikrotik_password,
                'description': 'None',
                'type': tipo
            }
            try:
                r = requests.post('{}/{}/'.format(self.url, model),
                                  data=payload,
                                  verify=False)
                time.sleep(0.2)
            except Exception as e:
                try:
                    migration_logger.info(
                        f'EXCEPCIÓN AL MOMENTO DE CREAR NODO: {e}')
                    r = requests.post('{}/{}/'.format(self.url, model),
                                      data=payload,
                                      verify=False)
                    time.sleep(0.2)
                except Exception as e:
                    migration_logger.info(
                        f'EXCEPCIÓN AL MOMENTO DE CREAR NODO: {e}')
                    r = requests.post('{}/{}/'.format(self.url, model),
                                      data=payload,
                                      verify=False)
                    time.sleep(0.2)

            migration_logger.info(f'NODE REQUEST STATUS {r.status_code}')

            if r.status_code == 201:
                migration_logger.debug('Nodo creado')
            elif r.status_code == 400 or r.status_code == 401:
                migration_logger.warning('\nError al crear el nodo')
                migration_logger.warning(r.json())
                migration_logger.warning('Parámetros usados')
                migration_logger.warning(payload)
            elif r.status_code == 500:
                migration_logger.warning(
                    '\nError del servidor 500 al crear el nodo')
                migration_logger.warning(r.json())
            # import pprint
            # print('')
            # pprint.pprint(payload)
            # print('{}/{}/'.format(self.url, model))
            # print(r.status_code)
            # print(r.text)
            # print('')
            # # print(res, '\n')

            if (r.status_code == 201):
                res = r.json()
                uuid = res['uuid']
                # print(res['id'])
                # print()

                # for p in n.pics.all():
                #     file = {'pic': open(p.photo.path, 'r')}
                # #     # # print(p.photo.path)
                #     # f = open(p.photo.path, 'r')
                # #     # print(f)
                #     r = requests.post('{}/{}/{}/add_gallery/'.format(self.url, model, uuid), files=file, data={'caption':p.caption}, headers=self.headers, verify=False )
                # #     print(r.text)

                # for d in n.documents.all():
                #     file = {'file': open(d.file.path, 'r')}
                # #     print(d.file.path)
                #     # f = open(d.file.path, 'r')
                # #     # print(f)
                #     payload = {
                #         'description': p.description,
                #         'tag': p.tag
                #     }
                #     r = requests.post('{}/{}/{}/add_document/'.format(self.url, model, uuid), files=file, data=payload, headers=self.headers, verify=False )
                # #     print(r.text)

                return (uuid, n.parent.id if n.parent else None,
                        n.default_connection_node.id
                        if n.default_connection_node else None, n)
            else:
                return None
            # pass
        else:
            payload = {
                "id": n.id,
                "group": n.group.id if n.group else None,
                "alias": n.code,
                "status": n.status,
                "address_pulso": None,
                # "parent": n.parent.id if n.parent else None,
                # "default_connection_node": n.default_connection_node.id if n.default_connection_node else None,
                # "is_building": n.is_building  ,
                "towers": n.towers,
                "apartments": n.apartments,
                "phone": n.phone,
                # "antennas_left": n.antennas_left  ,
                "ip": n.mikrotik_ip,
                "ip_range": n.ip_range,
                "comment":
                (n.notes[:200] + '...') if len(n.notes) > 200 else n.notes,
                "no_address_binding": n.no_address_binding,
                'plans': plans,
                'plan_order_kinds': plan_order_kinds,
                'matrix_seller': n.seller.id,
                'user': n.mikrotik_username,
                'password': n.mikrotik_password,
                'description': 'None',
                'type': tipo
            }
            migration_logger.info('NO EXISTE DIRE ID')
            migration_logger.info('PAYLOAD DEL NODO')
            migration_logger.info(payload)
            self.i = self.i + 1
            # if (normalize_names(find_region_name_by_location(str(n.location)).replace('Región ', '')).lower() == 'metropolitana de santiago' or \
            #     normalize_names(find_region_name_by_location(str(n.location)).replace('Región ', '')).lower() == 'arica y parinacota'):
            # print(
            #     'region: ', normalize_names(find_region_name_by_location(str(n.location)).replace('Región ', '')),
            #     ', comuna: ', fixes_commune_name(normalize_names(n.commune)),
            #     ', calle: ', fixes_street_name(normalize_names(n.street)),
            #     ', numero: ', fixes_number(n.house_number) if n.house_number else None,
            # )
            # print('false')
            # print()
            # print()

            return None

    def request_update_nodes(self, nodes_uuid, model):
        # print(nodes_uuid,'\nrequest update')
        if (not nodes_uuid is None):
            if (not nodes_uuid[1] is None or not nodes_uuid[2] is None):
                payload = {
                    'parent': nodes_uuid[1],
                    'default_connection_node': nodes_uuid[2]
                }

                try:
                    r = requests.patch('{}/{}/{}/'.format(
                        self.url, model, nodes_uuid[0]),
                                       data=payload,
                                       headers=self.headers,
                                       verify=False)
                except Exception as e:
                    try:
                        r = requests.patch('{}/{}/{}/'.format(
                            self.url, model, nodes_uuid[0]),
                                           data=payload,
                                           headers=self.headers,
                                           verify=False)
                    except Exception as e:
                        try:
                            r = requests.patch('{}/{}/{}/'.format(
                                self.url, model, nodes_uuid[0]),
                                               data=payload,
                                               headers=self.headers,
                                               verify=False)
                        except Exception as e:
                            pass
                # print(r.text)

    def request_history_node(self, model):
        # print('uno')
        r = requests.get('{}/{}/'.format(self.url, model),
                         headers=self.headers,
                         verify=False)

        if r.status_code == 201 or r.status_code == 200:
            nodes = r.json()
            # print('dos')
            i = 0
            for node in nodes:
                i = i + 1
                # print(i)
                n = Node.objects.get(id=node['id'])
                for h in n.history.all():
                    # print(h)

                    dire_id = self.request_direction(
                        fixes_commune_name(normalize_names(h.commune)),
                        fixes_street_name(normalize_names(h.street)),
                        house_number=fixes_number(h.house_number)
                        if h.house_number else None,
                        region=normalize_names(
                            find_region_name_by_location(str(
                                h.location)).replace('Región ', '')))

                    # print('id_pulso: ', dire_id)

                    if dire_id:
                        payload = {
                            'id':
                            h.id,
                            'created':
                            h.history_date,
                            'history_date':
                            h.history_date,
                            'history_type':
                            h.history_type,
                            'history_id':
                            h.history_id,
                            "group":
                            h.group.id if h.group else None,
                            "alias":
                            h.code,
                            "status":
                            h.status,
                            "address_pulso":
                            dire_id,
                            # "address": h.address,
                            # "street": h.street,
                            # "house_number": h.house_number,
                            # "commune": h.commune,
                            # "location": h.location,
                            "parent_id":
                            h.parent.id if h.parent else None,
                            "default_connection_node_id":
                            h.default_connection_node.id
                            if h.default_connection_node else None,
                            # "is_building": h.is_building  ,
                            "towers":
                            h.towers,
                            "apartments":
                            h.apartments,
                            "phone":
                            h.phone,
                            # "antennas_left": h.antennas_left  ,
                            "ip":
                            h.mikrotik_ip,
                            "ip_range":
                            h.ip_range,
                            "comment":
                            (h.notes[:200] +
                             '...') if len(h.notes) > 200 else h.notes,
                            "no_address_binding":
                            h.no_address_binding,
                            'matrix_seller':
                            h.seller.id if h.seller else None,
                            'description':
                            'None',
                        }

                        try:
                            r = requests.post('{}/{}/{}/add_history/'.format(
                                self.url, model, node["uuid"]),
                                              data=payload,
                                              headers=self.headers,
                                              verify=False)
                            time.sleep(0.2)
                        except Exception as e:
                            try:
                                r = requests.post(
                                    '{}/{}/{}/add_history/'.format(
                                        self.url, model, node["uuid"]),
                                    data=payload,
                                    headers=self.headers,
                                    verify=False)
                                time.sleep(0.2)
                            except Exception as e:
                                pass

                        # print(r.text)
                # print('\n\n')

    def request_direction(self,
                          commune,
                          street,
                          house_number=None,
                          region=None):
        payload = {'commune': commune, 'street': street}

        if region:
            payload['region'] = region

        if house_number:
            payload['house_number'] = house_number

        migration_logger.info('PAYLOAD DIRECCION')
        migration_logger.info(payload)
        # pprint.pprint(payload)
        # print('')
        try:
            r = requests.get(self.url_pulso,
                             params=payload,
                             headers=self.headers,
                             verify=False)
            time.sleep(0.2)
        except Exception as e:
            r = requests.get(self.url_pulso,
                             params=payload,
                             headers=self.headers,
                             verify=False)

        # print(r.status_code)
        if r.status_code == 404:
            migration_logger.warning('\nError en la direccion del nodo')
            migration_logger.warning(r.json())
            migration_logger.warning('Payload usado:')
            migration_logger.warning(payload)

            # return self.request_direction(commune, street, house_number,
            #                               region)
        print('\n\n\n')

        #return r.json()['street_location']

        if len(r.json()['results']) > 0:
            return r.json()['results'][0]['id']
        else:
            return None

    def get_model_id(self, model, brand, kind):
        model_id = None

        try:
            r = requests.get('{}/model_device/'.format(self.url),
                             headers=self.headers,
                             verify=False)
            time.sleep(0.2)
        except Exception as e:
            try:
                r = requests.get('{}/model_device/'.format(self.url),
                                 headers=self.headers,
                                 verify=False)
                time.sleep(0.2)
            except Exception as e:
                try:
                    r = requests.get('{}/model_device/'.format(self.url),
                                     headers=self.headers,
                                     verify=False)
                    time.sleep(0.2)
                except Exception as e:
                    r = None

        if r:

            for m in r.json():
                if (m['model'] == model and m['brand'] == brand
                        and m['kind'] == kind):
                    model_id = m['id']
                    break
        else:
            model_id = None

        # print(model_id)
        if (not model_id):
            # print(model, brand)
            payload = {'model': model, 'brand': brand, 'kind': kind}

            try:
                r = requests.post('{}/model_device/'.format(self.url),
                                  data=payload,
                                  headers=self.headers,
                                  verify=False)
                print(r.status_code, r.json())
                model_id = r.json()['id']
            except json.decoder.JSONDecodeError as e:
                model_id = None

        return model_id

    NODES_GEARS = [
        254,
        255,
        69,
        55,
        205,
        185,
        175,
        219,
        96,
        126,
        83,
        84,
        85,
        47,
        123,
        186,
        51,
        15,
        227,
        118,
        68,
        117,
        115,
        92,
        88,
        87,
        149,
        93,
        238,
        31,
        52,
        23,
        116,
        19,
        56,
        77,
        2,
        34,
        174,
        10,
        223,
        194,
        89,
        236,
        201,
        224,
        25,
        165,
        222,
        97,
        1,
        208,
        27,
        101,
        59,
        63,
        67,
        26,
        17,
        30,
        24,
        200,
        3,
        18,
        119,
        62,
        98,
        32,
        124,
        14,
        229,
        70,
        95,
        38,
        57,
        100,
        80,
        71,
        8,
        203,
        58,
        16,
        12,
        64,
        4,
        234,
        211,
        212,
        213,
        20,
        65,
        73,
        182,
        215,
        86,
        206,
        233,
        218,
        45,
        103,
        187,
        173,
        133,
        74,
        13,
        76,
        54,
        66,
        155,
        37,
        109,
        112,
        113,
        107,
        105,
        108,
        104,
        106,
        110,
        180,
        184,
        195,
        111,
        198,
        197,
        199,
        239,
        179,
        131,
        82,
        153,
        161,
        190,
        81,
        152,
        11,
        72,
        50,
        53,
        48,
        49,
        172,
        9,
        122,
        156,
        33,
        121,
        60,
        28,
        5,
        6,
        75,
        39,
        158,
        40,
        43,
        196,
        91,
        22,
        35,
        36,
        46,
        181,
        240,
        202,
    ]

    def gears(self):
        gears = Gear.objects.select_related("node").all()
        print(Gear.objects.all())
        for g in gears:
            # for g in Gear.objects.filter(node__id__in=self.NODES_GEARS):
            model_id = self.get_model_id(g.kind, g.brand, 5)

            # print(p, p.id)
            payload = {
                "id":
                g.id,
                'description':
                g.description,
                'ip':
                g.ip,
                'mac':
                g.mac,
                'vlan':
                'None'
                if str(g.vlan).strip() == '' or g.vlan is None else g.vlan,
                'node':
                g.node.id,
                'firmware_version':
                'None' if str(g.firmware_version).strip() == ''
                or g.firmware_version is None else g.firmware_version,
                'notes':
                g.notes,
                'latest_backup_date_cache':
                g.latest_backup_date_cache,
                'model':
                model_id
            }
            # print(payload)

            try:
                r = requests.post('{}/gear/'.format(self.url),
                                  data=payload,
                                  headers=self.headers,
                                  verify=False)
                time.sleep(0.2)
            except Exception as e:
                try:
                    r = requests.post('{}/gear/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.2)
                except Exception as e:
                    r = requests.post('{}/gear/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.2)

            if r.status_code == 400 or r.status_code == 401:
                migration_logger.warning('\nError al crear Gear')
                migration_logger.warning(r.json())
                migration_logger.warning('Parámetros usados')
                migration_logger.warning(payload)
            # print(r.text)
            if r.status_code == 201 or r.status_code == 200:
                migration_logger.debug('Gear creado')
                res = r.json()
                # print(res)
                # print('\n')

                # for p in g.pics.all():
                #     file = {'pic': open(p.photo.path, 'r')}
                #     # # print(p.photo.path)
                #     # f = open(p.photo.path, 'r')
                #     # print(f)
                #     r = requests.post('{}/gear/{}/add_gallery/'.format(self.url, g.id), files=file, data={'caption':p.caption}, headers=self.headers , verify=False)
                #     print(r.text)

                # for d in g.documents.all():
                #     file = {'file': open(d.file.path, 'r')}
                #     print(d.file.path)
                #     # f = open(d.file.path, 'r')
                #     # print(f)
                #     payload = {
                #         'description': p.description,
                #         'tag': p.tag
                #     }
                #     r = requests.post('{}/gear/{}/add_document/'.format(self.url, g.id), files=file, data=payload, headers=self.headers, verify=False )
                #     print(r.text)

                for h in g.history.all():

                    model_id = self.get_model_id(h.kind, h.brand, 5)

                    # print(h)
                    payload = {
                        'id':
                        h.id,
                        'created':
                        h.history_date,
                        'history_date':
                        h.history_date,
                        'history_type':
                        h.history_type,
                        'history_id':
                        h.history_id,
                        'description':
                        h.description,
                        'ip':
                        h.ip,
                        'mac':
                        h.mac,
                        'vlan':
                        'None' if str(h.vlan).strip() == '' or h.vlan is None
                        else h.vlan,
                        'node':
                        h.node.id if h.node else None,
                        'firmware_version':
                        'None' if str(h.firmware_version).strip() == ''
                        or h.firmware_version is None else h.firmware_version,
                        'notes':
                        h.notes,
                        'latest_backup_date_cache':
                        h.latest_backup_date_cache,
                        'model':
                        model_id
                    }

                    try:
                        r = requests.post('{}/gear/{}/add_history/'.format(
                            self.url, g.id),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                    except Exception as e:
                        pass

                #     print(r.text)
        migration_logger.info('Gears terminados')

    def service_thread(self, services, thread_num):

        try:

            print('Inicio HIlo {}'.format(thread_num))

            filter_nodes = []
            j = 0
            k = 0
            for s in services:
                if (not s.node.id in filter_nodes):
                    # print(p, p.id)

                    payload = {
                        'id':
                        s.id,
                        'number':
                        s.number,
                        'seen_connected':
                        s.seen_connected,
                        'network_mismatch':
                        s.network_mismatch,
                        'node_mismatch':
                        s.node_mismatch.id if s.node_mismatch else None,
                        'node':
                        s.node.id
                    }
                    #print(payload)

                    try:
                        r = requests.post('{}/service-migrate/'.format(
                            self.url),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                        time.sleep(0.2)
                    except Exception as e:
                        try:
                            r = requests.post('{}/service-migrate/'.format(
                                self.url),
                                              data=payload,
                                              headers=self.headers,
                                              verify=False)
                            time.sleep(0.2)
                        except Exception as e:
                            print(1, e)
                            r = requests.post('{}/service-migrate/'.format(
                                self.url),
                                              data=payload,
                                              headers=self.headers,
                                              verify=False)
                            time.sleep(0.2)
                    if r.status_code == 201 or r.status_code == 200:
                        migration_logger.debug('Servicio creado')
                    elif r.status_code == 400 or r.status_code == 401:
                        migration_logger.warning('\nError al crear servicio')
                        migration_logger.warning(r.json())
                        migration_logger.warning('Parámetros usados:')
                        migration_logger.warning(payload)

                    # print(r.status_code)
                    if (not r.status_code == 201):
                        k = k + 1
                        #print(f'salida: {r.json()}')
                        #migration_logger.info(f'salida: {r.json()}')
                        filter_nodes.append(s.node.id)
                        print('\n\n')
                    else:
                        j = j + 1
                        for h in s.history.all():
                            try:
                                # print(h)
                                payload = {
                                    'id':
                                    h.id,
                                    'created':
                                    h.created_at,
                                    'history_date':
                                    h.history_date,
                                    'history_type':
                                    h.history_type,
                                    'history_id':
                                    h.history_id,
                                    'number':
                                    h.number,
                                    'seen_connected':
                                    h.seen_connected,
                                    'network_mismatch':
                                    h.network_mismatch,
                                    'node_id':
                                    h.node.id if h.node else None,
                                    'node_mismatch_id':
                                    h.node_mismatch.id
                                    if h.node_mismatch else None
                                }

                                try:
                                    r = requests.post(
                                        '{}/service/{}/add_history/'.format(
                                            self.url, s.id),
                                        data=payload,
                                        headers=self.headers,
                                        verify=False)
                                    time.sleep(0.2)
                                except Exception as e:
                                    try:
                                        r = requests.post(
                                            '{}/service/{}/add_history/'.
                                            format(self.url, s.id),
                                            data=payload,
                                            headers=self.headers,
                                            verify=False)
                                        time.sleep(0.2)
                                    except Exception as e:
                                        pass
                                # print(r.text)
                            except Node.DoesNotExist as e:
                                print(e)
                                pass
            migration_logger.info(f"CANTIDAD DE ERRORES = {k}")
            print('hilo =', thread_num, ' | Servicios =', len(services),
                  ' | Registrados =', j)
            migration_logger.info(
                f"hilo={thread_num} | Servicios = {len(services)} | Registrados={j}"
            )
        except Exception as e:
            migration_logger.warning(e)
            #print(e)

    def services(self):
        thread_num = multiprocessing.cpu_count()
        services = Service.objects.select_related("node", "operator",
                                                  "node_mismatch").all()
        migration_logger.info(f"SERVICIOS CUENTA = {services.count()}")
        #print(len(Service.objects.all()))
        #print(services.count())
        lng = int(services.count() / thread_num)
        # lng = int((10764-9415)/8)
        #print(lng)

        threads = []
        for i in range(thread_num):
            end = (services.count() -
                   1) if i == thread_num - 1 else int((i * lng) + lng) - 1
            # end = 10764-1 if i == thread_num-1 else int((i * lng) + lng) + 9415 - 1
            start = int(i * lng)  #+9415
            # print(len(services[start:end]))
            # print( start ,  end )
            t = Thread(target=self.service_thread,
                       args=(services[start:end], i))
            t.start()
            threads.append(t)

        for thread in threads:
            thread.join()
        migration_logger.info('Servicios terminados')

    def cpes_thread(self, cpes, thread_num):
        import uuid
        count = 0
        filter_nodes = []
        uuids = []
        for c in cpes:
            # for c in NetworkEquipment.objects.filter(Q(service__technology_kind=2) | Q(service__technology_kind=3),
            #                                             service__id__in=self.status_services):

            if (not c.service.node.id in filter_nodes):

                model_id = self.get_model_id(c.model, c.brand, 4)

                # print(p, p.id)
                payload = {
                    'alias':
                    str(uuid.uuid4()),
                    'id':
                    c.id,
                    'serial':
                    'None Serial' if str(c.serial_number).strip() == ''
                    or c.serial_number is None else c.serial_number,
                    'nodo_utp':
                    c.service.node.id,
                    'description':
                    'None Description' if str(c.notes).strip() == ''
                    or c.notes is None else c.notes,
                    'model':
                    model_id,
                    'service':
                    c.service.id if c.service else None,
                    'service_number_verified':
                    False,
                    'mac':
                    c.mac,
                    'ip':
                    c.ip,
                    'primary':
                    c.primary,
                    'ssid': (c.service.ssid if c.service.ssid else None)
                    if c.service else None,
                    'password': (c.service.ssid if c.service.ssid else None)
                    if c.service else None,
                    'ssid_5g':
                    (c.service.ssid_5g if c.service.ssid_5g else None)
                    if c.service else None
                }
                # print(payload)
                try:
                    r = requests.post('{}/cpeutp/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.3)
                except Exception as e:
                    try:
                        r = requests.post('{}/cpeutp/'.format(self.url),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                        time.sleep(0.3)
                    except Exception as e:
                        r = requests.post('{}/cpeutp/'.format(self.url),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                        time.sleep(0.3)

                if r.status_code == 201:
                    migration_logger.debug('CPEUTP Creado')

                elif r.status_code == 400 or r.status_code == 401:
                    migration_logger.warning('\nError al crear CPEUTP')
                    migration_logger.warning(r.json())
                    migration_logger.warning('Parámetros usados:')
                    migration_logger.warning(payload)
                # print('cpe', r.text)
                # exit()
                # res = r.json()
                # print(r.status_code)
                if (not r.status_code == 201):
                    #print(r.text)
                    filter_nodes.append(c.service.node.id)
                    # print('\n')
                else:
                    # print(c.service.node.id)
                    uuids.append((c.id, r.json()['uuid']))
                    count = count + 1
                    pass
                # print('\n')

        print('hilo =', thread_num, 'TOTAL CPES=', len(cpes),
              '- Registrados CPES=', count)

        self.request_history_cpes_thread(uuids)

    def cpes(self):

        thread_num = multiprocessing.cpu_count()
        cpes = NetworkEquipment.objects.select_related("service").filter(
            Q(service__technology_kind=1) | Q(service__technology_kind=4))
        # print(len(cpes))
        lng = int(cpes.count() / thread_num)

        threads = []
        for i in range(thread_num):
            end = (cpes.count() -
                   1) if i == thread_num - 1 else int((i * lng) + lng) - 1
            start = int(i * lng)
            t = Thread(target=self.cpes_thread, args=(cpes[start:end], i))
            t.start()
            threads.append(t)

        for thread in threads:
            thread.join()

        migration_logger.info("CPEUTP's Terminados")

    def onu_thread(self, onus, thread_num):
        import uuid

        filter_nodes = []
        count = 0
        no_re = 0
        for c in onus:
            # print(len(list(NetworkEquipment.objects.filter(Q(service__technology_kind=2) | Q(service__technology_kind=3),
            #                                             service__id__in=self.status_services))))
            # for c in NetworkEquipment.objects.filter(Q(service__technology_kind=2) | Q(service__technology_kind=3),
            #                                             service__id__in=self.status_services):

            if c.service.node.id not in filter_nodes:

                model_id = self.get_model_id(c.model, c.brand, 4)
                #print(c.serial_number)
                # print(p, p.id)
                payload = {
                    'alias':
                    str(uuid.uuid4()),
                    'id':
                    c.id,
                    'serial':
                    'None Serial' if str(c.serial_number).strip() == ''
                    or c.serial_number is None else c.serial_number,
                    'description':
                    'None Description' if str(c.notes).strip() == ''
                    or c.notes is None else c.notes,
                    'model':
                    model_id,
                    'service':
                    c.service.id if c.service else None,
                    'mac':
                    c.mac,
                    'ip':
                    c.ip,
                    'primary':
                    c.primary,
                    'ssid': (c.service.ssid if c.service.ssid else None)
                    if c.service else None,
                    'password': (c.service.ssid if c.service.ssid else None)
                    if c.service else None,
                    'ssid_5g':
                    (c.service.ssid_5g if c.service.ssid_5g else None)
                    if c.service else None,
                    'onu_id':
                    1,
                    'frame':
                    1,
                    'slot':
                    1,
                    'port':
                    1
                }

                try:
                    r = requests.post('{}/onu/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.3)
                except Exception as e:
                    try:
                        r = requests.post('{}/onu/'.format(self.url),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                        time.sleep(0.3)
                    except Exception as e:
                        r = requests.post('{}/onu/'.format(self.url),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                        time.sleep(0.3)

                if r.status_code == 201:
                    migration_logger.debug('ONU creado')

                elif r.status_code == 400 or r.status_code == 401:
                    migration_logger.warning('Error al crear ONU')
                    migration_logger.warning(r.json())
                    migration_logger.warning('Parámetros usados:')
                    migration_logger.warning(payload)
                # print('onu', r.text)
                # res = r.json()
                # print(r.status_code)
                if (not r.status_code == 201):
                    filter_nodes.append(c.service.node.id)
                    no_re = no_re + 1
                    # print('\n')
                else:
                    # print(c.service.node.id)
                    count = count + 1
                    pass
                # print('\n')

        print('hilo =', thread_num, 'TOTAL ONUS=', len(onus),
              '- Registrados ONUS=', count)
        print(f'no registrados = {no_re}')

    def onus(self):
        thread_num = multiprocessing.cpu_count()
        onus = NetworkEquipment.objects.select_related("service").filter(
            Q(service__technology_kind=2) | Q(service__technology_kind=3))
        #print(len(onus))
        lng = int(onus.count() / thread_num)

        threads = []
        for i in range(thread_num):
            end = (onus.count() -
                   1) if i == thread_num - 1 else int((i * lng) + lng) - 1
            start = int(i * lng)
            t = Thread(target=self.onu_thread, args=(onus[start:end], i))
            t.start()
            threads.append(t)

        for thread in threads:
            thread.join()
        migration_logger.info('ONUS terminados')

    def request_history_cpes_thread(self, uuids):
        # r = requests.get('{}/cpeutp/'.format(self.url), verify=False)
        # cpes = r.json()

        for cpe in uuids:
            try:

                print(cpe)
                print('cpe')
                c = NetworkEquipment.objects.get(id=cpe[0])
                for h in c.history.all():
                    # print(h)
                    model_id = self.get_model_id(h.model, h.brand, 4)

                    payload = {
                        'id':
                        h.id,
                        'created':
                        h.history_date,
                        'history_date':
                        h.history_date,
                        'history_type':
                        h.history_type,
                        'history_id':
                        h.history_id,
                        'alias':
                        h.ip,
                        'serial':
                        'None Serial' if str(c.serial_number).strip() == ''
                        or c.serial_number is None else c.serial_number,
                        'nodo_utp':
                        h.service.node.id if h.service.node else None,
                        'description':
                        'None Description' if str(c.notes).strip() == ''
                        or c.notes is None else c.notes,
                        'model':
                        model_id,
                        'service':
                        h.service.id if h.service else None,
                        'service_number_verified':
                        False,
                        'mac':
                        h.mac,
                        'ip':
                        h.ip,
                        'primary':
                        h.primary
                    }

                    try:
                        r = requests.post('{}/cpeutp/{}/add_history/'.format(
                            self.url, cpe[1]),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                        time.sleep(0.2)
                    except Exception as e:
                        try:
                            r = requests.post(
                                '{}/cpeutp/{}/add_history/'.format(
                                    self.url, cpe[1]),
                                data=payload,
                                headers=self.headers,
                                verify=False)
                            time.sleep(0.2)
                        except Exception as e:
                            r = None

                    # if r:
                    #     if(not r.status_code == 201 ):
                    #         # print(r.text)
                    #         pass
                    # print('\n')
                #     print(r.text)
            except Exception:
                pass

        print('\n\n')

    # def request_history_cpes(self, uuids):
    #     thread_num = multiprocessing.cpu_count()
    #     # print(len(uuids))
    #     lng = int(len(uuids) / thread_num)

    #     threads = []
    #     for i in range(thread_num):
    #         end = len(uuids)-1 if i == thread_num-1 else int((i * lng) + lng) - 1
    #         start = int(i * lng)
    #         t = Thread(target=self.request_history_cpes_thread, args=(uuids[start:end]) )
    #         t.start()
    #         threads.append(t)

    #     for thread in threads:
    #         thread.join()

    def building_contacts(self):
        buildingContact = BuildingContact.objects.select_related("node").all()
        for b in buildingContact:

            # print(p, p.id)
            payload = {
                'id': b.id,
                'kind': b.kind,
                'name': b.name,
                'phone': b.phone,
                'email': b.email,
                'node': b.node.id,
            }
            # print(payload)
            try:
                r = requests.post('{}/building_contact/'.format(self.url),
                                  data=payload,
                                  headers=self.headers,
                                  verify=False)
                time.sleep(0.2)
            except Exception as e:
                try:
                    r = requests.post('{}/building_contact/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.2)
                except Exception as e:
                    r = requests.post('{}/building_contact/'.format(self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.2)

            if r.status_code == 201:
                migration_logger.debug('building contact creado')
            elif r.status_code == 401 or r.status_code == 400:
                migration_logger.warning('\nError al crear building contact')
                migration_logger.warning(r.json())
                migration_logger.warning('Parámetros usados:')
                migration_logger.warning(payload)
            # print(r.text)
            # print('\n')

            for h in b.history.all():

                try:

                    # print(h)
                    payload = {
                        'id': h.id,
                        'created': h.history_date,
                        'history_date': h.history_date,
                        'history_type': h.history_type,
                        'history_id': h.history_id,
                        'kind': h.kind,
                        'name': h.name,
                        'phone': h.phone,
                        'email': h.email,
                        'node': h.node.id if h.node else None,
                    }

                    r = requests.post(
                        '{}/building_contact/{}/add_history/'.format(
                            self.url, b.id),
                        data=payload,
                        headers=self.headers,
                        verify=False)
                    # print(r.text)
                except Node.DoesNotExist as e:
                    print(e)
                    pass

                except:
                    pass
            # print('\n\n')
        migration_logger.info('BuildingContact terminados')

    def network_status_changes(self):

        for change in NetworkStatusChange.objects.select_related(
                "service", "agent").all():
            # for change in NetworkStatusChange.objects.filter(service__id__in=self.status_services):

            payload = {
                'service': change.service.id,
                'new_state': change.new_state,
                'agent': change.agent.username,
                'created_at': change.created_at
            }
            # print(payload)
            try:
                r = requests.post('{}/networkstatuschange/'.format(self.url),
                                  data=payload,
                                  headers=self.headers,
                                  verify=False)
                time.sleep(0.3)
            except Exception as e:
                try:
                    r = requests.post('{}/networkstatuschange/'.format(
                        self.url),
                                      data=payload,
                                      headers=self.headers,
                                      verify=False)
                    time.sleep(0.3)
                except Exception as e:
                    try:
                        r = requests.post('{}/networkstatuschange/'.format(
                            self.url),
                                          data=payload,
                                          headers=self.headers,
                                          verify=False)
                        time.sleep(0.3)
                    except Exception:
                        pass
            if r.status_code == 201:
                migration_logger.debug('networkstatuschange creado')
            # print(r.text)
            # print('\n')
        migration_logger.info('networkstatuschange terminados')

    status_services = [
        7636,
        7529,
        7524,
        7483,
        7482,
        7453,
        7452,
        7444,
        7443,
        7442,
        7441,
        7440,
        7439,
        7261,
        7207,
        7138,
        7012,
        7009,
        6946,
        6927,
        6926,
        6921,
        6919,
        6908,
        6881,
        6833,
        6525,
        6343,
        6278,
        6260,
        6096,
        6088,
        6083,
        6079,
        6076,
        6072,
        6061,
        6059,
        6057,
        6051,
        6043,
        6041,
        6038,
        6036,
        6029,
        6023,
        6012,
        6009,
        6007,
        5973,
        5972,
        5819,
        5667,
        5666,
        5433,
        5432,
        5431,
        5427,
        5423,
        5422,
        5416,
        5414,
        5411,
        5408,
        5404,
        5403,
        5402,
        5385,
        5384,
        5382,
        5375,
        5374,
        5373,
        5371,
        5368,
        5367,
        5363,
        5362,
        5357,
        5355,
        5354,
        5352,
        5318,
        5209,
        10799,
        10792,
        10789,
        10786,
        10782,
        10779,
        10778,
        10777,
        10776,
        10775,
        10774,
        10773,
        10772,
        10771,
        10770,
        10767,
        10764,
        10763,
        10762,
        10761,
        10760,
        10759,
        10758,
        10756,
        10755,
        10754,
        10753,
        10752,
        10751,
        10747,
        10746,
        10745,
        10738,
        10737,
        10736,
        10735,
        10732,
        10729,
        10728,
        10726,
        10725,
        10709,
        10708,
        10707,
        10706,
        10705,
        10704,
        10703,
        10702,
        10699,
        10698,
        10695,
        10691,
        10690,
        10689,
        10688,
        10687,
        10686,
        10682,
        10681,
        10680,
        10679,
        10678,
        10676,
        10675,
        10674,
        10673,
        10671,
        10670,
        10669,
        10668,
        10665,
        10663,
        10662,
        10657,
        10653,
        10652,
        10650,
        10649,
        10648,
        10647,
        10646,
        10645,
        10643,
        10642,
        10638,
        10637,
        10636,
        10635,
        10634,
        10633,
        10632,
        10631,
        10630,
        10629,
        10628,
        10624,
        10623,
        10622,
        10621,
        10620,
        10619,
        10618,
        10617,
        10616,
        10615,
        10614,
        10613,
        10612,
        10610,
        10609,
        10607,
        10606,
        10602,
        10601,
        10600,
        10598,
        10597,
        10596,
        10595,
        10594,
        10593,
    ]

    nodes_in_sent = [
        54,
        85,
        47,
        123,
        186,
        51,
        15,
        227,
        68,
        117,
        115,
        92,
        88,
        87,
        149,
        93,
        238,
        31,
        23,
        69,
        205,
        175,
        219,
        96,
        126,
        83,
        84,
        48,
        89,
        236,
        201,
        224,
        25,
        165,
        222,
        97,
        208,
        27,
        101,
        59,
        63,
        67,
        26,
        17,
        30,
        24,
        200,
        3,
        18,
        119,
        52,
        56,
        77,
        2,
        34,
        174,
        10,
        223,
        194,
        60,
        100,
        98,
        32,
        124,
        14,
        229,
        95,
        38,
        80,
        57,
        8,
        233,
        133,
        71,
        203,
        58,
        16,
        12,
        64,
        4,
        234,
        211,
        212,
        213,
        20,
        65,
        73,
        182,
        215,
        206,
        218,
        45,
        103,
        187,
        173,
        74,
        66,
        155,
        159,
        220,
        166,
        198,
        239,
        152,
        76,
        37,
        109,
        112,
        113,
        107,
        105,
        108,
        104,
        106,
        110,
        180,
        184,
        195,
        111,
        197,
        199,
        179,
        131,
        82,
        153,
        161,
        190,
        81,
        11,
        72,
        50,
        53,
        49,
        172,
        9,
        116,
        1,
        62,
        55,
        118,
        158,
        91,
        202,
        86,
        13,
        122,
        156,
        33,
        121,
        28,
        5,
        6,
        75,
        39,
        40,
        43,
        196,
        22,
        35,
        36,
        46,
        181,
        240,
        185,
        120,
        241,
        19,
        70,
        99,
        125,
        193,
        242,
        127,
        78,
        207,
        204,
        102,
        129,
        177,
        176,
        132,
        189,
        151,
        135,
        160,
        162,
        163,
        150,
        170,
        130,
        225,
        128,
        21,
        79,
        178,
        42,
        169,
        167,
        168,
        209,
        164,
        141,
        145,
        137,
        138,
        140,
        143,
        171,
    ]

    def where_service(self):
        print('CPE')
        networkEquipment = NetworkEquipment.objects.select_related("service")
        print(
            'actuales = ',
            len(
                list(
                    networkEquipment.filter(
                        Q(service__technology_kind=1)
                        | Q(service__technology_kind=4),
                        service__node__id__in=self.nodes_in_sent))))
        print(
            'faltantes = ',
            len(
                list(
                    networkEquipment.filter(
                        Q(service__technology_kind=1)
                        | Q(service__technology_kind=4)).exclude(
                            service__node__id__in=self.nodes_in_sent))))

        print('')
        print('ONU')
        print(
            'actuales = ',
            len(
                list(
                    networkEquipment.filter(
                        Q(service__technology_kind=2)
                        | Q(service__technology_kind=3),
                        service__node__id__in=self.nodes_in_sent))))
        print(
            'faltantes = ',
            len(
                list(
                    networkEquipment.filter(
                        Q(service__technology_kind=2)
                        | Q(service__technology_kind=3)).exclude(
                            service__node__id__in=self.nodes_in_sent))))

    # def availabilities(self):
    #     for a in Availability.objects.all():
    #         id_pulso = self.request_direction(
    #             normalize_names(a.direccion.calle.comuna.nombre),
    #             normalize_names(a.direccion.calle.nombre),
    #             house_number = fixes_number(a.direccion.numeroMunicipal) if n.house_number else None,
    #             region = normalize_names(a.direccion.calle.comuna.region.nombre)
    #         )

    #         if id_pulso:
    #             payload = {
    #                 'id': a.id,
    #                 'node': a.node.id,
    #                 'address': id_pulso
    #             }
    #             print(payload)
    #             r = requests.post('{}/availability/'.format(b.id), data=payload, headers=self.headers, verify=False )
    #             print(r.text)
    #             print('\n')

    #         print('\n\n')

    # def read_all_dir(self):
    #     import os

    #     with open("C:\\Users\\francisco alvarado\\Desktop\\direciones.json", "w") as file:
    #         file.write(f'"direciones": [')

    #         for n in Node.objects.all():
    #             file.write(f"""
    # {'{'}
    #     "region": "{normalize_names(find_region_name_by_location(str(n.location)).replace("Región", ""))}",
    #     "comunne": "{normalize_names(n.commune)}",
    #     "street": "{normalize_names(n.street)}",
    #     "house_numer": {n.house_number if n.house_number else "null"}
    # {'}'},

    #             """)

    #         for a in Availability.objects.all():
    #             file.write(f"""
    # {'{'}
    #     "region": "{normalize_names(a.direccion.calle.comuna.region.nombre)}",
    #     "comunne": "{normalize_names(a.direccion.calle.comuna.nombre)}",
    #     "street": "{normalize_names(a.direccion.calle.nombre)}",
    # {'}'},

    # """)

    # file.write(f']')
