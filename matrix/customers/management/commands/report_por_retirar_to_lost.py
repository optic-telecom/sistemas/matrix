# coding: utf-8
import pandas as pd
import xlrd
import xlsxwriter
import io, datetime, json
from constance import config
from datetime import date, datetime, timedelta
from django.core.management import BaseCommand
from customers.models import (Customer, Balance, Service)
from datetime import date
from infra.models import Node
import xlrd 
import dateutil.parser
import pytz
utc=pytz.UTC

class Command(BaseCommand):
    def handle(self, *args, **options):
        data = []
        # Valores para el excel de los invoice sin cargar en Matrix.
        output_file = io.BytesIO()
        workbook = xlsxwriter.Workbook(output_file)
        worksheet = workbook.add_worksheet()

        header = ('ID','Servicio','Cliente','RUT', 'Correo','Plan', 'Comuna', 'Dirección','Fecha del Cambio')
        bold = workbook.add_format({'bold': True})
        worksheet.set_row(0, None, bold)
        worksheet.write_row(0, 0, header)
        row = 1

        services=Service.objects.filter(status=11)
        print(services.count())
        count=0
        # Valores inciales
        fecha_limite=  datetime(2021,7,25).replace(tzinfo=utc)
        delta=  datetime(2021,7,27).replace(tzinfo=utc)
        for service in services:
            try:
                logs = service.action_log()
                for hh in logs:
                    if 'changes' in hh.keys():
                        if 'status' in hh['changes']:
                            
                            if hh['current_values']['status']==11 and  hh['previous_values']['status']==2 and hh['dt']>fecha_limite and hh['dt'] < delta:
                                worksheet.write(row,0, str(service.id))
                                worksheet.write(row,1,str(service.number))
                                worksheet.write(row,2,str(service.customer))
                                worksheet.write(row,3,str(service.customer.rut))
                                worksheet.write(row,4,str(service.customer.email))
                                worksheet.write(row,5,str(service.plan))
                                worksheet.write(row,6,str(service.get_location_display().capitalize()))
                                worksheet.write(row,7,str(service.address))
                                worksheet.write(row,8,str(hh['dt']))
                                row += 1
            except Exception as e:
                print(e) 
                print("No encontro el servicio")

        # Escribe los valores en el excel.
        workbook.close()
        output_file.seek(0)

        myFile = open('list_new_lost.xlsx', 'wb')
        with myFile:
            myFile.write(output_file.read())
        
        print("Listo")
                            