# coding: utf-8
import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import Customer, Balance, Service, Invoice, BalanceService, FinancialRecord, Credit, Promotions
from datetime import date


class Command(BaseCommand):
    def handle(self, *args, **options):
        
        # Activo las promociones 
        promociones_descuentos=Credit.objects.filter(
            status_field=False, cleared_at__isnull=True,
            reason__kind=3, active=False, 
            date_to_set_active__lte=date.today()).update(active=True)

        # Promociones activar.
        # Se activan las que tienen fecha mayor o igual a hoy.
        Promotions.objects.filter(initial_date__lte=date.today(), active=False).update(active=True)

        # Promociones desactivar.
        # Se desactivan las que tienen fecha de finalizar, si ya tenian descuentos creados igual se dejan.
        Promotions.objects.filter(end_date__lt=date.today(), active=True).update(active=False)

        print("Listo")