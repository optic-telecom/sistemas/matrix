# coding: utf-8
import xlsxwriter
import io
from django.core.management import BaseCommand
from infra.models import NetworkEquipment



class Command(BaseCommand):
    def handle(self, *args, **options):
        equipos = NetworkEquipment.objects.filter(
            service__technology_kind=1, 
            service__status=1, 
            service__node__is_optical_fiber=True,
            service__node__is_building=True,
            primary=True)
        data = []

        output_file = io.BytesIO()
        workbook = xlsxwriter.Workbook(output_file)
        worksheet = workbook.add_worksheet()

        header = ('Nro servicio','Direccion Cliente', 'Nombre plan', 'Direccion IP','MAC address')
        bold = workbook.add_format({'bold': True})
        worksheet.set_row(0, None, bold)
        worksheet.write_row(0, 0, header)
        row = 1

        for e in equipos:
            worksheet.write(row,0,str(e.service.number))
            worksheet.write(row,1,str(e.service.customer.address))
            worksheet.write(row,2,str(e.service.plan.name))
            worksheet.write(row,3,str(e.ip))
            worksheet.write(row,4,str(e.mac))
            row += 1

        workbook.close()
        output_file.seek(0)

        myFile = open('clientes_utp_fibra.xlsx', 'wb')
        with myFile:
            myFile.write(output_file.read())
