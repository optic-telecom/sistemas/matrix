import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import *
from customers.lefu.cl_territory import TERRITORIAL_ORGANIZATION


class Command(BaseCommand):
    def handle(self, *args, **options):
        
        workbook = xlsxwriter.Workbook('status_report.xlsx')
        worksheet = workbook.add_worksheet()
        
        worksheet.write(0,0,"Nombre/Razón Social")
        worksheet.write(0,1,"RUT")
        worksheet.write(0,2, "Dirección")
        worksheet.write(0,3, "Comuna")
        worksheet.write(0,4, "Estado de Red")
        worksheet.write(0,5, "Numero de Servicio")
        worksheet.write(0,6, "Fecha de Instalación")
        worksheet.write(0,7, "Fecha de Baja")
        worksheet.write(0,8, "Estado")
        row = 1

        service = Service.objects.filter(status__in=[2,3])
        for s in service:
            
            hist = []
            for hh in s.status_history:
                hist.append(hh)

            date_change = hist[0]
            hist.reverse()
            p_date = ''

            for x in hist:
                if x.status == 1:
                    p_date = x
                    break

            if date_change.history_date.year == 2021 and date_change.history_date.month == 1:
                worksheet.write(row,0,s.customer.name)
                worksheet.write(row,1,s.customer.rut)
                worksheet.write(row,2,s.address)

                location = s.customer.location
                region_id = location[0:2]
                province_id = location[0:3]
                commune_id = location
                commune = TERRITORIAL_ORGANIZATION[region_id]['provinces'][province_id]['communes'][commune_id]
                worksheet.write(row,3,commune)

                worksheet.write(row,4,s.network_status[1])
                worksheet.write(row,5,s.number)
                if p_date:
                    ins_date = p_date.history_date.date()
                elif s.activated_on:
                    ins_date = s.activated_on.date()
                elif s.installation_on:
                    ins_date = s.installation_on.date()

                worksheet.write(row,6,str(ins_date))
                worksheet.write(row,7,str(date_change.history_date.date()))

                if s.status == 2:
                    worksheet.write(row,8,'Por retirar')
                else:
                    worksheet.write(row,8,'Retirado')
                row = row+1

        workbook.close()
        print('Listo')   
        