import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import *
from reports.models import ServiceStatusLog

class Command(BaseCommand):
    def handle(self, *args, **options):
        workbook = xlsxwriter.Workbook('cambios numeros de servicio.xlsx')
        worksheet = workbook.add_worksheet("Numeros")
        worksheet.write(0,0,"Nombre")
        worksheet.write(0,1,"Numero Antiguo")
        worksheet.write(0,2,"Numero Nuevo")

        services = Service.objects.filter(number__gte = 15000, number__lte=51000)
        services = services.reverse()
        for s in services:
            print(s.customer, s.number)
        
        base = 13890
        row = 1
        for s in services:
            worksheet.write(row,0,s.customer.name)
            worksheet.write(row,1,s.number)
            worksheet.write(row,2,base)
            s.number = base
            s.save()
            base = base + 1
            row = row + 1


        for s in services:
            print(s.customer, s.number)
