# coding: utf-8
from django.core.management import BaseCommand
from customers.models import Service
from django.utils import timezone
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags


class Command(BaseCommand):
    def handle(self, *args, **options):
        expiring = Service.objects.filter(status=Service.ACTIVE,
                                           expired_on__lte=timezone.now())
        for c in expiring:
            c.status = Service.PENDING
            c.save()
