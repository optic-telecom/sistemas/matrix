# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Payment, Plan, Service, Currency, Invoice
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Obtiene todos los invoices.
        invoices=Invoice.objects.all()
        print("Total de invoices:")
        print(invoices.count())
        invoices=invoices.filter(currency=None)
        print(invoices.count())
        count=0
        for i in invoices:
            # Si la compania del operador tiene default, se lo asocia.
            if i.operator.company:
                if i.operator.company.currency and i.currency==None:
                    i.currency=i.operator.company.currency
                    i.save()
                    count=count+1
        print("Cuenta de cuantos se asociaron:")
        print(count)