import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import *
from reports.models import ServiceStatusLog

class Command(BaseCommand):
    def handle(self, *args, **options):
        customers = Customer.objects.all()
        first_names = []
        for c in customers:
            sepName = c.name.split()
            first_names.append(sepName[0])

        for c in customers:
            if c.kind == 1:
                sepName = c.name.split()
                sepIndex = 1
                for index in range(1, len(sepName)):
                    if sepName[index] in first_names:
                        sepIndex = index+1
                    else:
                        break

                name = ' '
                name = name.join(sepName[0:sepIndex])
                last_name = ' '
                last_name = last_name.join(sepName[sepIndex:])
                
                c.first_name = name
                c.last_name = last_name
                c.save()
            

        print('Listo')
