# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Payment, Plan, Service, Currency, Invoice, Operator, Company
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Obtiene todos los invoices.
        ops=Operator.objects.all()
        print("Total de operadores:")
        print(ops.count())
        ops=ops.filter(company__isnull=False).filter(company__currency__isnull=False)
        count=0
        for o in ops:
            # Si la compania del operador tiene default, se lo asocia.
            monedas=o.currency.all()
            if monedas.filter(id=o.company.currency.id).count()==0:
                o.currency.add(o.company.currency)
                #o.save()
                count=count+1
        print("Cuenta de cuantos se asociaron:")
        print(count)