# coding: utf-8
import petl as etl
from django.core.exceptions import ValidationError
from django.core.management import BaseCommand
from customers.models import Customer
from localflavor.cl.forms import CLRutField

rut_field = CLRutField()


class Command(BaseCommand):
    def handle(self, *args, **options):
        t1 = etl.fromxlsx('cliente-pagador.xlsx')
        t2 = etl.convert(t1, 'RUT CLIENTE', str.strip)
        t3 = etl.convert(t2, 'RUT CLIENTE', rut_field.clean)
        t4 = etl.convert(t3, 'RUT PAGADOR', str.strip)
        t5 = etl.convert(t4, 'RUT PAGADOR', rut_field.clean)

        relationships = t5

        for rel in relationships:
            try:
                customer = Customer.objects.get(rut=rel[1])
                customer.payorrut_set.update_or_create(rut=rel[3],
                                                       defaults={'name': rel[2]})
            except Customer.DoesNotExist:
                pass
