# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Payment, Plan, Service, Currency, Payment, Operator, Company
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Obtiene todos los invoices.
        pagos=Payment.objects.all()
        print("Total de operadores:")
        print(pagos.count())
        pagos=pagos.filter(bank_account__isnull=True)
        #Crea el archivo del reporte
        workbook = xlsxwriter.Workbook('pagos_sin_cuaentabancaria.xlsx')
        worksheet = workbook.add_worksheet()

        count_row=1
        #Crea los titulos de las columnas.

        worksheet.write(0,0,"ID pago")
        worksheet.write(0,1,"Comentario")
        worksheet.write(0,2,"Monto")
        worksheet.write(0,3,"Nombre Cliente")

        for p in pagos:
            #Si no tiene cuenta, guarda los valores en el excel.
        
            worksheet.write(count_row,0,p.id)
            worksheet.write(count_row,1, p.comment)
            worksheet.write(count_row,2, p.amount)
            worksheet.write(count_row,3, p.customer.name if p.customer else "")
            count_row=count_row+1
                    
        workbook.close()
        print("Listo")