# coding: utf-8
import xlsxwriter
import io
import requests
from django.core.management import BaseCommand
from customers.models import Plan, Service 
from constance import config


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Trae todos los servicios que tienen estado de por retirar.
        services = Service.objects.filter(status=2)

        error=[]
        for service in services:
            data ={
                    "operator": service.operator.id,
                    "rut": str(service.customer.rut),
                    "service": int(service.number),
                    "serial": None,
                    "iclass_id": None,
                    "status": 0,
                    "status_agendamiento": None,
                    "status_storage": None,
                    "status_ti": None,
                    "creator": None,
                    "updater": None,
                    "agent_ti": None,
                    "agent_sac": None,
                    "agent_storage": None
                }

            url = getattr(config, "iris_server") + getattr(config, "iris_notify_remove_equipament")
            # print (url)
            # print(data)

            try:
                send = requests.post(
                        url=url,
                        json=data,
                        headers={"Authorization": getattr(config, "invoices_to_iris_task_token")},
                        timeout=60,
                )
                print(send)
                print(send.text)
                if "Error" in send.json() or "error" in send.json():
                    print("ERROR")
            except Exception as e:
                error.append(service.number)
                print("e",e)

        if len(error)>0:
            # En caso de faltar
            print("Listado de las que no puedieron procesar")
            print(error)