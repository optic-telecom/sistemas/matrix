# coding: utf-8
import pandas as pd
import xlrd
import xlsxwriter
import io, datetime, json
from constance import config
from datetime import date, datetime, timedelta
from django.core.management import BaseCommand
from customers.models import (Customer, Balance, Service)
from datetime import date
from infra.models import Node
import xlrd 
import dateutil.parser
import pytz
utc=pytz.UTC

class Command(BaseCommand):
    def handle(self, *args, **options):
        data = []
        # Valores para el excel de los invoice sin cargar en Matrix.
        output_file = io.BytesIO()
        workbook = xlsxwriter.Workbook(output_file)
        worksheet = workbook.add_worksheet()

        header = ('ID','Servicio','Fecha','Status anterior')
        bold = workbook.add_format({'bold': True})
        worksheet.set_row(0, None, bold)
        worksheet.write_row(0, 0, header)
        row = 0

        services=Service.objects.filter(status=2)
        print(services.count())
        count=0
        # Valores inciales
        fecha_limite=  datetime(2020,12,31).replace(tzinfo=utc)
        for service in services:
            first=False
            try:
                logs = service.action_log()
                for hh in logs:
                    if 'changes' in hh.keys():
                        if 'status' in hh['changes'] and first==False:
                            
                            # Si el primero es mayor a fecha limite, no se cambia el estado.
                            if hh['dt']>fecha_limite:
                                #print(hh)
                                first=True
                            if hh['dt'] < fecha_limite:
                                #print(hh)
                                first=True
                                count=count+1
                                service.status=11
                                service.save()
                            #print(first)
            except Exception as e: 
                print(e)
                print("No encontro el servicio")
        
        print("Total cambiados")
        print(count)