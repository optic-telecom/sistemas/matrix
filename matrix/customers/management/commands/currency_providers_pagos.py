# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from customers.models import Currency, BankAccount
from providers.models import Provider, Invoice, Payment
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count, Sum, Min, Max
from datetime import date, datetime, timedelta


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Obtiene todos los invoices.

        pagos=Payment.objects.all()
        print("Total de pagos:")
        print(pagos.count())
        pagos=pagos.filter(bank_account__isnull=True, operator__id=2)
        print("Total de pagos sin cuenta bancaria:")
        print(pagos.count())
        count=0
        for p in pagos:
            p.bank_account=BankAccount.objects.get(operator__id=2)
            count=count+1
            p.save()
        print("Cuenta de cuantos se asociaron:")
        print(count)
