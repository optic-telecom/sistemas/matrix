# coding: utf-8
import xlsxwriter
from django.core.management import BaseCommand
from infra.models import Node


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Create a workbook and add a worksheet.
        workbook = xlsxwriter.Workbook("node_address.xlsx")
        worksheet = workbook.add_worksheet()
        r = 1
        col = 0
        # Header
        worksheet.write(0, col, "ID")
        worksheet.write(0, col + 1, "COMUNA")
        worksheet.write(0, col + 2, "CALLE")
        worksheet.write(0, col + 3, "CASA")
        worksheet.write(0, col + 4, "APTO")
        for n in Node.objects.all().order_by("commune"):
            # Filter
            worksheet.autofilter("A1:E" + str(r))
            # Rows
            idNode = n.id if n.id else "NO DATA"
            comuna = n.commune if n.commune else "NO DATA"
            calle = n.street if n.street else "NO DATA"
            casa = n.house_number if n.house_number else "NO DATA"
            apto = n.apartments if n.apartments else "NO DATA"
            # Writing rows
            worksheet.write(r, col, idNode)
            worksheet.write(r, col + 1, comuna)
            worksheet.write(r, col + 2, calle)
            worksheet.write(r, col + 3, casa)
            worksheet.write(r, col + 4, apto)
            r += 1
        workbook.close()
