# coding: utf-8
import xlsxwriter
import io, datetime
from django.core.management import BaseCommand
from customers.models import Plan
from datetime import date


class Command(BaseCommand):
    def handle(self, *args, **options):
        
        plans=Plan.objects.filter(category=2)
        print("Total de planes a cambiar:")
        print(plans.count())

        for i in plans:
            i.category=1
            i.type_plan=2
            i.save()

        print("Listo el cambio")