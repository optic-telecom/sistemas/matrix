import io
import xlsxwriter
from django.db import connection
from django.db.models import Count
from django.http import HttpResponse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from .forms import (TagColorForm, CommercialActivityAdminForm, PaymentAdminForm,
    InvoiceAdminForm, BankAdminForm, OperatorAdminForm, CreditAdminForm, ChargeAdminForm)
from .models import (Contact, BillingCycle, InvoiceBatch, TaxationResponse,
    ProcessedBatch, InvoiceItem, DurationRecord, Customer, Prospect, Service, 
    Plan, Invoice, Seller, Payment, Promotions, NetworkStatusChange, PlanOrder,
    Region, Comuna, Calle, Direccion, TagColor, CategoryCommercialActivity, 
    TypeCommercialActivity, Operator, OperatorInformation, MainReason, Credit,
    Charge,CreditApplied, ChargeApplied, CartolaMovement, Cartola, UserOperator,
    UserCompany, Company, Currency)
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from import_export.admin import ImportExportModelAdmin
from .resources import *

class ContactInline(admin.TabularInline):
    model = Contact
    extra = 1

class BankAccountInline(admin.TabularInline):
    model = BankAccount
    extra = 1

def export_empty_address_prospect(modeladmin, request, queryset):
    output_file = io.BytesIO()
    qs = queryset.filter(street='')
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('RUT', 'Nombre', 'Email', 'Dirección vieja', 'Dirección nueva', 'Comuna', 'Teléfono', 'Compañía actual')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for prospect in qs:
        worksheet.write(row, 0, prospect.rut)
        worksheet.write(row, 1, prospect.name)
        worksheet.write(row, 2, prospect.email)
        worksheet.write(row, 3, prospect.address)
        worksheet.write(row, 4, prospect.composite_address)
        worksheet.write(row, 5, prospect.get_location_display())
        worksheet.write(row, 6, prospect.phone)
        worksheet.write(row, 7, prospect.current_company)
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="prospectos_direccion_en_blanco.xlsx"'
    return response
export_empty_address_prospect.short_description = "Exportar prospectos con dirección en blanco"


@admin.register(Prospect)
class ProspectAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('rut', 'name', 'email', 'address', 'location', 'phone', 'current_company')
    list_filter = ('current_company',)
    search_fields = ('rut', 'name', 'email')
    actions = [export_empty_address_prospect,]
 

@admin.register(Customer)
class CustomerAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('rut', 'first_name', 'last_name', 'email', 'address', 'location', 'kind')
    list_filter = ('kind', 'default_due_day', 'full_process')
    search_fields = ('rut', 'first_name', 'last_name', 'email')
    inlines = (ContactInline,)
    resource_class = CustomerResource
 

def export_numbers(modeladmin, request, queryset):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('Número de servicio', 'Nodo Padre', 'Nombre cliente', 'Dirección IP principal')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for service in queryset:
        worksheet.write(row, 0, service.number)
        worksheet.write(row, 1, service.node.code)
        worksheet.write(row, 2, service.customer.name)
        worksheet.write(row, 3, service.primary_equipment.ip if service.primary_equipment else '')
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="numeros_de_servicio.xlsx"'
    return response
export_numbers.short_description = "Exportar números de servicio"


def export_empty_address(modeladmin, request, queryset):
    output_file = io.BytesIO()
    qs = queryset.filter(customer__street='', customer__house_number='', customer__apartment_number='', customer__tower='')
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('Número de servicio',)
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for service in qs:
        worksheet.write(row, 0, service.number)
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="servicio_direccion_en_blanco.xlsx"'
    return response
export_empty_address.short_description = "Exportar servicios con dirección en blanco"


def find_missing_numbers(modeladmin, request, queryset):
    ordered_qs = queryset.order_by('number')

    if ordered_qs.exists():
        start = ordered_qs.first().number
        end = ordered_qs.filter(number__lt=99000).last().number

        cursor = connection.cursor()
        cursor.execute("SELECT G.number FROM (SELECT generate_series(%s,%s) AS number) G LEFT JOIN customers_service I ON G.number = I.number WHERE I.number IS NULL", [start, end])
        missing_numbers = (row[0] for row in cursor.fetchall())
    else:
        missing_numbers = []

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('Número faltante',)
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for number in missing_numbers:
        worksheet.write(row, 0, number)
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="numeros_faltantes.xlsx"'
    return response
find_missing_numbers.short_description = "Encontrar números faltantes"


@admin.register(Service)
class ServiceAdmin(SimpleHistoryAdmin,ImportExportModelAdmin):
    list_display = ('number', 'customer', 'due_day', 'activated_on', 'expired_on', 'generate_pdf_link')
    list_filter = ('document_type', 'network_mismatch', 'due_day', 'status','operator', 'node')
    list_select_related = ('customer',)
    search_fields = ('customer__first_name', 'customer__last_name', 'customer__email', 'customer__rut', 'number', 'node__code')
    raw_id_fields = ('customer', 'plan', 'seller', 'node')
    date_hierarchy = 'activated_on'
    actions = [find_missing_numbers, export_numbers, export_empty_address]
    resource_class = ServiceResource

    def generate_pdf_link(self, obj):
        return "<a href=\"%s\" target=\"_blank\">Descargar</a>" % obj.pdf_url
    generate_pdf_link.allow_tags = True
    generate_pdf_link.short_description = _('generate PDF')


@admin.register(Plan)
class PlanAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('name', 'price', 'category', 'type_plan', 'uf', 'active')
    list_filter = ('active','type_plan',)
    list_editable = ('active',)
    resource_class = PlanResource


def find_missing_folios(modeladmin, request, queryset):
    ordered_qs = queryset.order_by('folio')

    if ordered_qs.exists():
        start = ordered_qs.first().folio
        end = ordered_qs.last().folio

        cursor = connection.cursor()
        cursor.execute("SELECT G.folio FROM (SELECT generate_series(%s,%s) AS folio) G LEFT JOIN customers_invoice I ON G.folio = I.folio WHERE I.folio IS NULL", [start, end])
        missing_folios = (row[0] for row in cursor.fetchall())
    else:
        missing_folios = []

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('Folio faltante',)
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for folio in missing_folios:
        worksheet.write(row, 0, folio)
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="folios_faltantes.xlsx"'
    return response
find_missing_folios.short_description = "Encontrar folios faltantes"


def find_happy_customers(modeladmin, request, queryset):
    today = timezone.now().date()
    month = request.GET.get('due_date__month', today.month)
    year = request.GET.get('due_date__year', today.year)

    customers_active = Service.objects.order_by().filter(status=Service.ACTIVE).values_list('customer_id', flat=True).distinct()
    customers_billed = Invoice.objects.order_by().filter(due_date__month=month, due_date__year=year).values_list('customer_id', flat=True).distinct()

    customers_not_billed = Customer.objects.filter(id__in=set(customers_active)-set(customers_billed))

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('ID Cliente', 'RUT', 'Nombre', 'URL')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0
    
    for customer in customers_not_billed:
        worksheet.write(row, 0, customer.pk)
        worksheet.write(row, 1, customer.rut)
        worksheet.write(row, 2, customer.name)
        worksheet.write(row, 3, "https://optic.matrix2.cl/customers/{}/billing/".format(customer.pk))
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="clientes_sin_facturar.xlsx"'
    return response
find_happy_customers.short_description = "Encontrar clientes sin facturar"


@admin.register(Invoice)
class InvoiceAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('kind', 'folio', 'customer', 'due_date', 'total','operator', 'sended')
    list_filter = ('kind','operator', 'sended')
    list_select_related = ('customer',)
    date_hierarchy = 'due_date'
    search_fields = ['folio', 'total']
    raw_id_fields = ('customer','service','credit', 'charge','parent_id')
    actions = [find_missing_folios,
               find_happy_customers]
    form = InvoiceAdminForm
    resource_class = InvoiceResource
    readonly_fields = ('created_at',)
    

@admin.register(InvoiceItem)
class InvoiceItemAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('invoice', 'description','price')
    
@admin.register(CategoryCommercialActivity)
class CategoryCommercialActivityAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('name',)

@admin.register(TypeCommercialActivity)
class TypeCommercialActivityAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('name','category')

@admin.register(Seller)
class SellerAdmin(SimpleHistoryAdmin,ImportExportModelAdmin):
    list_display = ('name', 'active')
    resource_class = SellerResource

def export_payments(modeladmin, request, queryset):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = ('ID Pago', 'Cliente', 'Fecha de Pago', 'Monto de pago', 'Es Manual', 'Tipo', 'Agente')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0

    from django.utils import formats

    for payment in queryset:
        worksheet.write(row, 0, payment.pk)
        worksheet.write(row, 1, str(payment.customer))
        worksheet.write(row, 2, formats.date_format(payment.paid_on.replace(tzinfo=None), "SHORT_DATETIME_FORMAT"))
        worksheet.write(row, 3, payment.amount)
        worksheet.write(row, 4, 'Sí' if payment.manual else 'No')
        worksheet.write(row, 5, payment.get_kind_display())
        worksheet.write(row, 6, str(payment.history.earliest().history_user))
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="pagos.xlsx"'
    return response
export_payments.short_description = "Exportar pagos"


def export_edited_payments(modeladmin, request, queryset):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    all_ids = queryset.values_list('id', flat=True)
    edited_ids = Payment.history.values('id').order_by().annotate(Count('pk')).filter(pk__count__gt=1).filter(id__in=all_ids).values_list('id', flat=True)
    edited_qs = Payment.objects.filter(id__in=edited_ids)

    header = ('ID Pago', 'Cliente', 'Fecha de Pago', 'Monto de pago', 'Es Manual', 'Tipo', 'Agente', 'URL')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0

    from django.utils import formats

    for payment in edited_qs:
        worksheet.write(row, 0, payment.pk)
        worksheet.write(row, 1, str(payment.customer))
        worksheet.write(row, 2, formats.date_format(payment.paid_on.replace(tzinfo=None), "SHORT_DATETIME_FORMAT"))
        worksheet.write(row, 3, payment.amount)
        worksheet.write(row, 4, 'Sí' if payment.manual else 'No')
        worksheet.write(row, 5, payment.get_kind_display())
        worksheet.write(row, 6, str(payment.history.earliest().history_user))
        worksheet.write(row, 7, "https://optic.matrix2.cl"+payment.get_absolute_url())
        row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="pagos_modificados.xlsx"'
    return response
export_edited_payments.short_description = "Encontrar pagos modificados"


def export_changed_customer_payments(modeladmin, request, queryset):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    all_ids = queryset.values_list('id', flat=True)
    edited_ids = Payment.history.values('id').order_by().annotate(Count('pk')).filter(pk__count__gt=1).filter(id__in=all_ids).values_list('id', flat=True)
    edited_qs = Payment.objects.filter(id__in=edited_ids)

    header = ('ID Pago', 'Cliente', 'Fecha de Pago', 'Monto de pago', 'Es Manual', 'Tipo', 'Agente', 'URL')
    bold = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0

    from django.utils import formats

    for payment in edited_qs:
        if payment.previous_customer:
            worksheet.write(row, 0, payment.pk)
            worksheet.write(row, 1, str(payment.customer))
            worksheet.write(row, 2, formats.date_format(payment.paid_on.replace(tzinfo=None), "SHORT_DATETIME_FORMAT"))
            worksheet.write(row, 3, payment.amount)
            worksheet.write(row, 4, 'Sí' if payment.manual else 'No')
            worksheet.write(row, 5, payment.get_kind_display())
            worksheet.write(row, 6, str(payment.history.earliest().history_user))
            worksheet.write(row, 7, "https://optic.matrix2.cl"+payment.get_absolute_url())
            row += 1
        
    workbook.close()
    output_file.seek(0)
    response = HttpResponse(output_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="pagos_cambiados_de_cliente.xlsx"'
    return response
export_changed_customer_payments.short_description = "Encontrar cambios de cliente"


@admin.register(Payment)
class PaymentAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('id', 'customer', 'paid_on', 'amount', 'manual', 'kind', 'cleared', 'operator')
    list_filter = ('kind', 'manual', 'cleared','operator')
    list_editable = ('cleared',)
    list_select_related = ('customer',)
    defer = ('proof', 'proof2', 'proof3', 'comment')
    search_fields = ['id','customer__first_name', 'customer__last_name', 'customer__rut']
    raw_id_fields = ('customer',)
    date_hierarchy = 'paid_on'
    actions = [export_payments, export_edited_payments, export_changed_customer_payments]
    form = PaymentAdminForm
    resource_class = PaymentResource


@admin.register(Promotions)
class PromoAdmin(ImportExportModelAdmin):
    list_display = ('name', 'description')
    resource_class = PromoResource

@admin.register(NetworkStatusChange)
class NetworkStatusChangeAdmin(ImportExportModelAdmin):
    list_display = ('service', 'new_state', 'created_at', 'agent')
    list_filter = ('agent', 'new_state')
    date_hierarchy = 'created_at'
    search_fields = ['service__number',]
    raw_id_fields = ('service',)
    resource_class = NetworkStatusChangeResource


@admin.register(PlanOrder)
class PlanOrderAdmin(ImportExportModelAdmin):
    list_display = ('pk', 'created_at', 'start', 'end', 'team', 'cancelled')
    list_filter = ('cancelled',)
    raw_id_fields = ('service',)
    readonly_fields = ('start', 'end', 'team')
    resource_class = PlanOrderResource


@admin.register(Region)
class RegionAdmin(ImportExportModelAdmin):
    resource_class = RegionResource
    pass

@admin.register(Comuna)
class ComunaAdmin(ImportExportModelAdmin):
    list_display = ('__str__', 'nombre', 'region')
    resource_class = ComunaResource


@admin.register(Calle)
class CalleAdmin(ImportExportModelAdmin):
    list_display = ('__str__', 'nombre', 'comuna')
    search_fields = ['nombre']
    raw_id_fields = ('comuna',)
    resource_class = CalleResource


@admin.register(Direccion)
class DireccionAdmin(ImportExportModelAdmin):
    list_display = ('__str__', 'numeroMunicipal', 'calle', 'lat', 'lon')
    list_filter = ['calle__comuna']
    raw_id_fields = ('calle',)
    resources_class = DireccionResource


class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'is_active')

admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), CustomUserAdmin)


@admin.register(TagColor)
class TagColorAdmin(ImportExportModelAdmin):
    list_display = ('tag', 'html_color', 'visual_color')
    form = TagColorForm

    def visual_color(self, obj):
        return "<div style='width: 100px; height: 20px; background: {};'></div>".format(obj.html_color)
    visual_color.allow_tags = True
    visual_color.short_description = 'Color visual'
    resource_class = TagColorResource

@admin.register(Operator)
class OperatorAdmin(ImportExportModelAdmin):
    form = OperatorAdminForm
    resources_class = OperatorResource
    inlines = (BankAccountInline,)

@admin.register(OperatorInformation)
class OperatorAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('operator', 'rut','name','email','phone',)

@admin.register(ConfigOperator)
class ConfigOperatorAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('name', 'operator')

@admin.register(CommercialActivity)
class CommercialActivityAdmin(ImportExportModelAdmin):
    form = CommercialActivityAdminForm
    resources_class = CommercialActivityResource

@admin.register(Bank)
class BankAdmin(ImportExportModelAdmin):
    form = BankAdminForm
    resources_class = BankResource

@admin.register(BankAccount)
class BankAccountAdmin(ImportExportModelAdmin):
    resources_class = BankAccountResource

class ContactAdminImportExport(ImportExportModelAdmin):
    resource_class = ContactResource

admin.site.register(Contact, ContactAdminImportExport)


class CallAdminImportExport(ImportExportModelAdmin):
    resource_class = CallResource

admin.site.register(Call, CallAdminImportExport)

@admin.register(CallQA)
class CallQAAdminImportExport(ImportExportModelAdmin):
    raw_id_fields = ('service',)
    resource_class = CallQAResource

@admin.register(QAIndicator)
class QAIndicatorAdminImportExport(ImportExportModelAdmin):
    resource_class = QAIndicatorResource

@admin.register(QAMonitoring)
class QAMonitoringAdminImportExport(ImportExportModelAdmin):
    raw_id_fields = ('service',)
    resource_class = QAMonitoringResource

@admin.register(QATracing)
class QATracingAdminImportExport(ImportExportModelAdmin):
    resource_class = QATracingResource

@admin.register(QAChannel)
class QAChannelAdminImportExport(ImportExportModelAdmin):
    resource_class = QAChannelResource

class PayorRutAdminImportExport(ImportExportModelAdmin):
    resource_class = PayorRutResource

admin.site.register(PayorRut, PayorRutAdminImportExport)

admin.site.register(DurationRecord)

class FinancialRecordAdminImportExport(ImportExportModelAdmin):
    resource_class = FinancialRecordResource

admin.site.register(FinancialRecord, FinancialRecordAdminImportExport)

class FinancialRecordServiceAdminImportExport(ImportExportModelAdmin):
    resource_class = FinancialRecordServiceResource

admin.site.register(FinancialRecordService, FinancialRecordServiceAdminImportExport)


class BalanceServiceAdminImportExport(ImportExportModelAdmin):
    resource_class = BalanceServiceResource

admin.site.register(BalanceService, BalanceServiceAdminImportExport)

class EmailAdminImportExport(ImportExportModelAdmin):
    resource_class = EmailResource

admin.site.register(Email, EmailAdminImportExport)

class MainReasonAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('reason', 'valor','kind','percent','calculate_by_days','active','status_field')
    list_filter = ('active', 'percent', 'kind', 'status_field')
    search_fields = ['reason',]
admin.site.register(MainReason, MainReasonAdmin)

class CreditAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('reason', 'service','times_applied','number_billing','days_count','created_at','active','cleared_at','status_field','quantity')
    list_filter = ('status_field',)
    search_fields = ['reason','service']
    form = CreditAdminForm
admin.site.register(Credit,CreditAdmin)

class CreditAppliedAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('credit_applied', 'amount','created_at')
    search_fields = [ 'credit_applied__reason__reason','credit_applied__service__number']

admin.site.register(CreditApplied, CreditAppliedAdmin)
class ChargeAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('reason', 'service','times_applied','number_billing','days_count','created_at','cleared_at','status_field','quantity')
    list_filter = ('status_field',)
    search_fields = ['reason','service']
    form = ChargeAdminForm
admin.site.register(Charge, ChargeAdmin)

class ChargeAppliedAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('charge_applied', 'amount','created_at')
    search_fields = ['charge_applied__reason__reason', 'charge_applied__service__number']
admin.site.register(ChargeApplied, ChargeAppliedAdmin)

class IndicatorsAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('kind','value','created_at')

admin.site.register(Indicators, IndicatorsAdmin)

class CycleAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('processed','start','end')

admin.site.register(BillingCycle, CycleAdmin)

class InvoiceBatchAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('batch','created_on', 'expiration','stock')
    list_filter = ('kind',)

admin.site.register(InvoiceBatch, InvoiceBatchAdmin)

class ProcessedBatchAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('batch','processed','processed_on', 'status')
    list_filter = ('status','batch__kind')

admin.site.register(ProcessedBatch, ProcessedBatchAdmin)

class TaxationResponseAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('response','date')

admin.site.register(TaxationResponse, TaxationResponseAdmin)

class CartolaAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('number', 'start', 'end','saldo_final','saldo_inicial','bank_account')

admin.site.register(Cartola, CartolaAdmin)

class CartolaMovementAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('date', 'sucursal', 'number_document', 'description', 'total', 'kind', 'saldo_diario')

admin.site.register(CartolaMovement, CartolaMovementAdmin)

@admin.register(Balance)
class BalanceAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('nombre', 'rut', 'balance')
    resource_class = BalanceResource

    def nombre(self, obj):
        return obj.customer.name

    def rut(self, obj):
        return obj.customer.rut

class UpdateInfoTempAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('customer', 'email', 'phone', 'id_pulso', 'street_location_id', 'services')

admin.site.register(UpdateInfoTemp, UpdateInfoTempAdmin)

class UserOperatorAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('user', 'operator', 'company')
admin.site.register(UserOperator, UserOperatorAdmin)

class CompanyAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('id', 'name', 'country_id', 'currency')
admin.site.register(Company, CompanyAdmin)

class UserCompanyAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('user',)

admin.site.register(UserCompany, UserCompanyAdmin)

class CurrencyAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('id', 'name', 'symbol')
admin.site.register(Currency, CurrencyAdmin)

