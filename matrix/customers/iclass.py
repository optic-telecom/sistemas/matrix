from collections import defaultdict
from constance import config
from operator import attrgetter, itemgetter
from itertools import chain, groupby, starmap
from django.utils import timezone
from requests import Session
from zeep import Client
from zeep.cache import InMemoryCache
from zeep.transports import Transport
from zeep.plugins import HistoryPlugin

session = Session()
session.verify = False
transport = Transport(session=session, cache=InMemoryCache())


wsdl_v2 = 'http://fs2.iclass.com.br:80/WS/v2/OrdemServicoWS?wsdl'
wsdl_v1 = 'http://fs2.iclass.com.br:80/WS/v1/OrdemServicoWS?wsdl'

def create_order(service_order, start, end):
    AuthIclass={'login': config.ICLASS_LOGIN,'password': config.ICLASS_PASSWORD}
    client_v2 = Client(wsdl_v2, transport=transport)
    customer_name = service_order.customer_name
    if len(customer_name) > 80:
        words = customer_name.upper().split(' ')
        customer_name = ''
        for word in words:
           customer_name += "%d." % word[0]


    criarOSIn = {
        'codigoOS': service_order.code,
        'codigoCliente': service_order.customer_code,
        'codigoEndereco': service_order.address_code,
        'codigoTipoOS': service_order.kind,
        'detalhesAgendamento': dict(limitesAgendamento=dict(inicio=start, fim=end)),
        'informacoesSigilosas': service_order.subject
    }
    enderecoIn = {
        'codigoEndereco': service_order.address_code,
        'codigoCliente': service_order.customer_code,
        'codigoMicroarea': service_order.microarea, 
        'logradouro': service_order.street_address, 
        'bairro': service_order.commune, 
        'cidade': service_order.city, 
        'estado': service_order.estado
    }

    result = client_v2.service.criarOS(
        credentials=AuthIclass,
        criarOSIn=criarOSIn,
        clienteIn={'codigoCliente': service_order.customer_code,'nomeTitular': customer_name},
        enderecoIn=enderecoIn
    )
    # from lxml import etree
    # print(etree.tostring(history.last_sent['envelope']))
    # print(result)
    return result

def reschedule_order(service_order, start, end):
    AuthIclass={'login': config.ICLASS_LOGIN,'password': config.ICLASS_PASSWORD}
    client_v1 = Client(wsdl_v1, transport=transport)
    
    reagendarOSIn={
        'codigoOS': service_order.code,
        'comentarioCO': 'Reagendado desde Matrix', 
        'detalhesAgendamento': dict(limitesAgendamento=dict(inicio=start, fim=end))
    }
    result = client_v1.service.reagendarOS(
    credentials=AuthIclass,
    reagendarOSIn=reagendarOSIn)
    # from lxml import etree
    # print(etree.tostring(history.last_sent['envelope']))
    # print(result)
    return result


def prepare_order(service_order):
    AuthIclass={'login': config.ICLASS_LOGIN,'password': config.ICLASS_PASSWORD}
    client_v1 = Client(wsdl_v1, transport=transport)
    # create or update cliente
    result = client_v1.service.manterCliente(credentials=AuthIclass,
            clienteIn={'codigoCliente': service_order.customer_code,
                       'nomeTitular': service_order.customer_name, 
                       'telefoneResidencial': service_order.customer_phone}
            )
    print(result)
    # create or update endereco
    result = client_v1.service.manterEndereco(credentials=AuthIclass,
        enderecoIn={'codigoEndereco': service_order.address_code,
                    'codigoCliente': service_order.customer_code, 
                    'codigoMicroarea': service_order.microarea, 
                    'logradouro': service_order.street_address, 
                    'bairro': service_order.commune, 
                    'cidade': service_order.city, 
                    'estado': service_order.estado})
    print(result)


def auto_schedule(service_order):
    AuthIclass={'login': config.ICLASS_LOGIN,'password': config.ICLASS_PASSWORD}
    client_v2 = Client(wsdl_v2, transport=transport)
    result = client_v2.service.criarOS(credentials=AuthIclass,
    criarOSIn={
        'codigoOS': service_order.code,
        'codigoCliente': service_order.customer_code,
        'codigoEndereco': service_order.address_code,
        'codigoTipoOS': service_order.kind,
        'detalhesAgendamento': dict(limitesAgendamento=dict(inicio=timezone.now(), fim=timezone.now()+timezone.timedelta(days=1))),
        'informacoesSigilosas': service_order.subject
    },
    clienteIn={
        'codigoCliente': service_order.customer_code, 
        'nomeTitular': service_order.customer_name
    },
    enderecoIn={
        'codigoEndereco': service_order.address_code, 
        'codigoCliente': service_order.customer_code, 
        'codigoMicroarea': service_order.microarea, 
        'logradouro': service_order.street_address, 
        'bairro': service_order.commune, 
        'cidade': service_order.city, 
        'estado': service_order.estado
    })
    return result


def anon_tuple(start, end):
    AM = (start.replace(hour=9, minute=0), end.replace(hour=13, minute=0))
    PM = (start.replace(hour=13, minute=1), end.replace(hour=17, minute=0))
    AFTER = (start.replace(hour=17, minute=1), end.replace(hour=21, minute=0))
    if AM[0] <= start < PM[0]:
        return AM
    elif PM[0] <= start < AFTER[0]:
        return PM
    else:
        return AFTER


def merge_day(event_tuples):
    """this needs testing, it's ok to rely on events being sorted, but can break easily"""
    events = []
    sorted_et = sorted(event_tuples, key=itemgetter(0))
    latest_event, *rest = sorted_et

    for start, end in rest:
        # they overlap
        latest_start, latest_end = latest_event
        if latest_end >= start:
            if end >= latest_end:
                latest_event = (latest_start, end)
            else:
                latest_event = (latest_start, latest_end)
        # they don't
        else:
            events.append(latest_event)
            latest_event = (start, end)
    events.append(latest_event)
    return events

def slice_day(event_tuples, size=timezone.timedelta(minutes=60)):
    events = []
    for start, end in event_tuples:
        new_start = start
        new_end = new_start + size

        while new_end <= end:
            events.append((new_start, new_end))
            new_start = new_end
            new_end = new_start + size
    return events

def find_slice_size(service_order):
    KIND_SIZES = (
        ('Instalacion Hogar - UTP 1h', 60),
        ('Instalacion Hogar - UTP 2h', 120),
        ('Instalacion Hogar - Fibra 1h', 60),
        ('Instalacion Hogar - Fibra 2h', 120),
        ('Instalacion Hogar - Antena 2h', 120),
        ('Instalacion Hogar - Apoyo Instalacion 1h', 60),
        ('Instalacion Hogar - Apoyo Instalacion 2h', 120),
        ('Instalacion Hogar - Migracion a fibra 1h', 60),
        ('Instalacion Hogar - Migracion a fibra 2h', 120),
        ('Visita Tecnica - Cliente sin servicio', 60),
        ('Visita Tecnica - Cambio de equipo', 30),
        ('Visita Tecnica - Cablear SmarTV', 60),
        ('Visita Tecnica - Otros 1h', 60),
        ('Visita Tecnica - Otros 2h', 120),
        ('Retiro de equipo', 30),
        ('Mantencion red Fibra - 1 Hora', 60),
        ('Mantencion red Fibra - 2 Horas', 120),
        ('Mantencion red UTP - 1 Hora', 60),
        ('Mantencion red UTP - 2 Horas', 120),
        ('Mantencion Electrica - 1 Hora', 60),
        ('Mantencion Electrica - 2 Horas', 120),
        ('Mantencion Alineacion antena 1h', 60),
        ('Mantencion red Cambio antena 2h', 120),
    )

    sizes = defaultdict(lambda: 60, KIND_SIZES)

    return timezone.timedelta(minutes=sizes[service_order.kind])
    
    
def find_availability(service_order):
    AuthIclass={'login': config.ICLASS_LOGIN,'password': config.ICLASS_PASSWORD}
    client_v1 = Client(wsdl_v1, transport=transport)
    result = client_v1.service.buscarDisponibilidade(credentials=AuthIclass,
                               buscarDisponibilidadeIn={
                                                        'endereco':{
                                                            'codigoEndereco':service_order.address_code,
                                                            'codigoCliente':service_order.customer_code
                                                        },
                                                        'codigoTipoOS':service_order.kind
                                                        })
    data_chain = []
    try:
        for data in chain(*map(attrgetter('periodoDisponibilidade'), result['disponibilidades'])):
            data_chain.append((data['dataInicio'], data['dataFim']))
    except Exception as e:
        print(e)
    dx = sorted(data_chain,key=itemgetter(0))
    days = groupby(dx, key=lambda dd: dd[0].date())
    data_return = [(key, slice_day(merge_day(group), size=find_slice_size(service_order))) for key, group in days]
    return data_return


def delete_order(service_order, subject_override=None):
    AuthIclass={'login': config.ICLASS_LOGIN,'password': config.ICLASS_PASSWORD}
    client_v1 = Client(wsdl_v1, transport=transport)
    motivo = subject_override or "CANCELADO MTX"
    encerrarOSIn = {
        'codigoOS':service_order.code,
        'motivo':motivo,
        'dataEncerramento':timezone.now()
    }
    result = client_v1.service.encerrarOS(credentials=AuthIclass, encerrarOSIn=encerrarOSIn)
    return result
