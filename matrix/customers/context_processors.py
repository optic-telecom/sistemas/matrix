import requests
from django.conf import settings
from random import randint
from django.contrib.auth.models import Group, User
from .models import *
from constance import config

url = "{}{}".format(getattr(config, "iris_server_contact"), "api/v1/drives/register/10/datatables/")

payload={
'order_field': 'valor',
'order_type': 'desc',
'start': '0',
'offset': '10',
'filters': '[]',
'operator': '2'}

headers = {
  'Authorization': getattr(config, "invoices_to_iris_task_token"),
}


def enviroment(request):

    try:
        response = requests.request("POST", url, headers=headers, data=payload, verify=False)
        data_iris = response.json()['result']
        mostrar_notificaciones = True if data_iris[2]['valor']['value'] == 'si' else False
        mensaje_notificacion = data_iris[0]['valor']['value']
    except Exception as e:
        mostrar_notificaciones = None
        mensaje_notificacion = str(e)

    ti = request.GET.get('to_iframe', False)
    if ti:
        template_base = 'customers/base_iframe.html'
    else:
        template_base = 'customers/base.html'

    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        try:
            companies=UserCompany.objects.get(user=user).company.all()
        except:
            pass
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                name_operator="Todos"
                todos=True
            else:
                name_operator=Operator.objects.get(id=op.operator).name
                todos=False
            context={"companies":companies.exclude(id=op.company),
                    "current_company":op.company,
                    "current_company_name":Company.objects.get(id=op.company).name,
                    "current_operator":op.operator,
                    "current_operator_name":name_operator,
                    "operadores": Operator.objects.filter(company__id=op.company).exclude(id=op.operator),
                    "if_todos":todos,
                    'ENVIROMENT': settings.ENVIROMENT,
                    'no_cache':    randint(10000, 99000),
                    'template_base':template_base
                }
            return context
        except:
            pass
    return {'ENVIROMENT': settings.ENVIROMENT,
           'no_cache':    randint(10000, 99000),
           'template_base':template_base,
           'mostrar_notificaciones':mostrar_notificaciones,
           'mensaje_notificacion':mensaje_notificacion}