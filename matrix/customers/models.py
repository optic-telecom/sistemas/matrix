from django.conf import settings
from collections import namedtuple
from calendar import monthrange
from datetime import date
from itertools import tee, islice, chain
from math import ceil
from django.core.cache import cache
from django.urls import reverse, reverse_lazy
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import JSONField
from django.db import models,transaction
from django.template.defaultfilters import truncatewords
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, SmartResize
from simple_history.models import HistoricalRecords
from taggit.managers import TaggableManager
from .lefu.cl_territory import COMMUNE_CHOICES
from django.core.exceptions import ValidationError
from . import iclass
from django.core.validators import FileExtensionValidator
from django.contrib.humanize.templatetags.humanize import intcomma
from .pulso  import request_address_pulso, get_complete_address



def previous_and_next(some_iterable):
    prevs, items, nexts = tee(some_iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(prevs, items, nexts)


class BaseModel(models.Model):
    history = HistoricalRecords(inherit=True)
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)

    def action_log(self, just=[]):
        fields = self.__class__._meta.fields
        filtered_fields = list(
            filter(lambda f: f.get_attname() in just, fields) if just else fields
        )
        field_names = [f.get_attname() for f in filtered_fields]
        historic_field_names = (just or field_names) + [
            "history_user_id",
            "history_date",
        ]
        users = {None: "matrix"}
        users.update(
            {
                u.pk: u.username
                for u in get_user_model().objects.filter(
                    id__in=self.history.values_list(
                        "history_user_id", flat=True
                    ).distinct()
                )
            }
        )
        rx = self.history.values(*historic_field_names)
        changes = []

        for prev, record, nxt in previous_and_next(rx):
            if nxt:
                changed_fields = [
                    f
                    for f in filtered_fields
                    if record[f.get_attname()] != nxt[f.get_attname()]
                ]
                verbose_changed_fields = [f.verbose_name for f in changed_fields]
                if changed_fields:
                    changes.append(
                        {
                            "user": users[record["history_user_id"]],
                            "action": _("changed"),
                            "changes": verbose_changed_fields,
                            "current_values": {
                                field.attname: record[field.attname]
                                for field in changed_fields
                            },
                            "previous_values": {
                                field.attname: nxt[field.attname]
                                for field in changed_fields
                            },
                            "dt": record["history_date"],
                        }
                    )
            else:
                if not just:
                    changes.append(
                        {
                            "user": users[record["history_user_id"]],
                            "action": _("created"),
                            "dt": record["history_date"],
                        }
                    )
        return changes

    @property
    def agent(self):
        return self.history.earliest().history_user

    @property
    def last_edited_by(self):
        return self.history.latest().history_user

    class Meta:
        abstract = True


class Contact(BaseModel):
    TECHNICAL = 1
    COMMERCIAL = 2
    FINANCIAL = 3
    TYPE_CHOICES = (
        (TECHNICAL, _("technical")),
        (COMMERCIAL, _("commercial")),
        (FINANCIAL, _("financial")),
    )

    kind = models.PositiveSmallIntegerField(
        _("kind"), default=TECHNICAL, choices=TYPE_CHOICES
    )
    name = models.CharField(_("name"), max_length=255, blank=True)
    phone = models.CharField(_("phone"), max_length=75, blank=True)
    email = models.EmailField(blank=True)
    customer = models.ForeignKey("Customer", verbose_name=_("customer"))

    class Meta:
        verbose_name = _("contact")
        verbose_name_plural = _("contacts")


class CustomerQuerySet(models.QuerySet):
    def latest_rut_as_int(self):
        from django.db import connection

        cursor = connection.cursor()
        cursor.execute(
            "select replace(split_part(rut, '-', 1), '.', '')::int as latest_rut_int from customers_customer order by latest_rut_int DESC limit 1;"
        )
        row = cursor.fetchone()
        return row[0]


class Customer(BaseModel):
    RUT = 1
    CI = 2
    PASAPORTE = 3
    DNI= 4

    DOCUMENTS_CHOICES = (
        (RUT, _("rut")),
        (CI, _("C.I")),
        (PASAPORTE, _("Pasaporte")),
        (DNI, _("DNI")),
    )
    # Chile
    CHILE_CHOICES = (
        (RUT, _("rut")),
    )
    # Peru
    PERU_CHOICES = (
        (RUT, _("rut")),
        (DNI, _("DNI")),
    )
    # Colombia
    COLOMBIA_CHOICES = (
        (CI, _("C.I")),
        (DNI, _("DNI")),
    )

    HOME = 1
    COMPANY = 2
    TYPE_CHOICES = ((HOME, _("home")), (COMPANY, _("company")))

    FEMALE = 1
    MALE = 2
    OTHER = 3
    GENDER_CHOICES = ((FEMALE, _("female")), (MALE, _("male")), (OTHER, _("other"))) 

    STATUS_CLASSES = {"En línea": "success", "Cortado": "danger"}

    send_email = models.BooleanField(_("send document by email"), default=True)
    send_snail_mail = models.BooleanField(
        _("send document by snail mail"), default=False
    )
    type_of_document= models.PositiveIntegerField(_("type of document"), choices=DOCUMENTS_CHOICES, default=1)
    # Por los momentos se quedo como rut porque habria que revisar y cambiar todas las partes del codigo donde se utilice
    rut = models.CharField(_("documento identidad"), max_length=20)
    first_name = models.CharField(_("name"), max_length=255, null = True)
    last_name = models.CharField(_("last_name"), max_length = 255, null = True)
    gender = models.PositiveIntegerField(_("gender"), choices=GENDER_CHOICES, null=True)
    kind = models.PositiveSmallIntegerField(
        _("kind"), choices=TYPE_CHOICES, default=HOME
    )
    commercial_activity = models.ForeignKey("CommercialActivity", null=True)
    email = models.EmailField()
    street = models.CharField(_("street"), max_length=75, blank=True)
    house_number = models.CharField(_("house number"), max_length=75, blank=True)
    apartment_number = models.CharField(
        _("apartment number"), max_length=75, blank=True
    )
    tower = models.CharField(_("tower"), max_length=75, blank=True)
    location = models.CharField(_("commune"), max_length=5, choices=COMMUNE_CHOICES)
    phone = models.CharField(_("phone"), max_length=75)
    default_due_day = models.PositiveSmallIntegerField(_("default_due_day"), default=5)
    unified_billing = models.BooleanField(_("unified billing"), default=False)
    full_process = models.BooleanField(_("full process"), default=True)
    notes = models.TextField(_("notes"), blank=True)
    birth_date = models.DateField(_("birth date"), blank=True, null=True)
    service_last_modified = models.DateTimeField(default=timezone.now)
    flag = JSONField(_("Flag"), null=True, blank=False)
    first_category_activity = models.BooleanField(
        _("first_category_activity"), default=False
    )
    change_to_service = models.BooleanField(default=False)
    id_pulso = models.PositiveIntegerField(blank = True, null=True)
    street_location_id = models.PositiveIntegerField(blank = True, null=True)
    company = models.ForeignKey("Company", default=1, verbose_name=_("company"))

    # comuna: use a special module for that
    # debug: check if already saved
    objects = CustomerQuerySet.as_manager()

    @property
    def name(self):
        if self.first_name and self.last_name:
            complete_name = self.first_name + ' ' + self.last_name
        elif self.first_name:
            complete_name = self.first_name
        elif self.last_name:
            complete_name = self.last_name
        else:
            complete_name = ''
        return complete_name

    @property
    def commune(self):
        if self.id_pulso:
            res = get_complete_address(self.id_pulso)   
            if res['ok']:
                return res['data']["commune"]
        else:
            return self.get_location_display()

    @property
    def composite_address(self):
        partial_address = "{} {}".format(self.street, self.house_number)

        if self.apartment_number.strip():
            partial_address += " dpto {}".format(self.apartment_number)

        if self.tower.strip():
            partial_address += " torre {}".format(self.tower)

        return partial_address.strip()
    
    @property
    def age(self):
        now = timezone.now()
        birth = self.birth_date

        if birth:
            age = now.year - birth.year
            if now.month < birth.month:
                age = age - 1
            elif now.month == birth.month:
                if now.day < birth.day:
                    age = age - 1
            return age
        else:
            return

    @property
    def address(self):
        if self.id_pulso:
            res = get_complete_address(self.id_pulso)   
            if res['ok']:
                return res['data']["street_location"]
        else:
            return self.composite_address

    address.fget.short_description = u"Dirección"

    def save(self, *args, **kwargs):
        super(Customer, self).save(*args, **kwargs)

        if not self.payorrut_set.exists():
            self.payorrut_set.create(rut=self.rut, name=self.name)

    def get_address(self):

        res = get_complete_address(self.id_pulso)   
        
        if res['ok']:
            return res['data']

        return True

    def get_statement(self, limit=15, offset=0):
        records = self.financialrecord_set.all()
        previous_balance = 0
        current_balance = previous_balance

        field_names = [
            field.get_attname() for field in FinancialRecord._meta.fields
        ] + ["balance", "absolute_url"]
        EnhancedRecord = namedtuple("EnhancedRecord", field_names)
        enhanced_results = []

        for record in reversed(records.values()):
            if record["record_id"].startswith("P"):
                record["absolute_url"] = reverse(
                    "payment-detail", args=[record["record_id"][1:]]
                )
            else:
                record["absolute_url"] = reverse(
                    "invoice-detail", args=[record["record_id"][1:]]
                )

            current_balance -= record["charge"] or 0
            current_balance += record["credit"] or 0
            record["balance"] = current_balance
            enhanced_record = EnhancedRecord(**record)
            enhanced_results.insert(0, enhanced_record)

        # try:
        #     previous_balance = self.balance(as_of=results[-1].ts)
        # except IndexError:
        #     previous_balance = self.balance()

        return dict(
            initial_balance=previous_balance,
            final_balance=current_balance,
            records=enhanced_results,
        )

    # def balance(self, as_of=None):
    #     # if not as_of:
    #     #     as_of = timezone.now()
    #     credits = self.financialrecord_set.aggregate(models.Sum('credit'))['credit__sum']
    #     charges = self.financialrecord_set.aggregate(models.Sum('charge'))['charge__sum']
    #     return credits - charges

    def record_count(self):
        return self.invoice_set.count() + self.payment_set.count()

    @property
    def email_history(self):
        first, *hx = self.history.order_by("history_date")
        history = [first]
        for hh in hx:
            if history[-1].email != hh.email:
                history.append(hh)
        return reversed(history)

    @property
    def aggregated_network_status(self):
        cached = self.service_set.filter(networkequipment__primary=True).values_list(
            "seen_connected", flat=True
        )
        if cached:
            if all(cached):
                return "En línea"
            elif all((e is False for e in cached)):
                return "Cortado"
        return "Otro"

    @property
    def aggregated_network_status_class(self):
        return self.STATUS_CLASSES.get(self.aggregated_network_status, "default")

    @property
    def extra_credits(self):
        return Credit.objects.filter(service__customer=self)

    @property
    def extra_charges(self):
        return Charge.objects.filter(service__customer=self)
    
    def get_first_name(self):
        if self.first_name: 
            fn = self.first_name.split(" ")[0]
        elif self.last_name:
            fn = self.last_name.split(" ")[0]
        else:
            fn = ''
        return str(fn)

    get_first_name.short_description = _("the first name")
    
    @property
    def first_email(self):
        if "/" in self.email:
            return self.email.split("/")[0]
        else:
            return self.email.split(" ")[0]

    @property
    def truncated_name(self):
        return truncatewords(self.name, 5)

    @property
    def service_ids(self):
        return [str(i) for i in self.service_set.values_list("id", flat=True)]

    @property
    def service_numbers(self):
        return [str(i) for i in self.service_set.values_list("number", flat=True)]

    @property
    def operators_ids(self):
        operators = self.service_set.values_list("operator", flat=True)
        if len(operators) == 1:
            operator = Operator.objects.filter(pk=operators[0])
            return operator[0]
        elif len(operators) > 1:
            flag = False

            for i in range(0, len(operators)):
                if i != 0:
                    if op != operators[i]:
                        flag = True
                        break
                op = operators[i]

            if flag == True:
                return "Varios"

            operator = Operator.objects.filter(pk=operators[0])
            return operator[0]
        else:
            return ""

    def get_absolute_url(self):
        return reverse("customer-detail", kwargs={"pk": self.pk})
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = _("customer")
        verbose_name_plural = _("customers")
        ordering = ["-service_last_modified"]
        permissions = (
            ("view_customer", "Can view customer data"),
            ("list_customer", "Can list customer data"),
            ("search_customer", "Can search customer data"),
        )


class ServiceQuerySet(models.QuerySet):
    def active(self):
        return self.filter(status=Service.ACTIVE)

    def pending(self):
        return self.filter(status=Service.PENDING)

    def inactive(self):
        return self.filter(status=Service.INACTIVE)

    def not_installed(self):
        return self.filter(status=Service.NOT_INSTALLED)

    def free(self):
        return self.filter(status=Service.FREE)

    def install_rejected(self):
        return self.filter(status=Service.INSTALL_REJECTED)

    def delinquent(self):
        return self.filter(status=Service.DELINQUENT)

    def holder_change(self):
        return self.filter(status=Service.HOLDER_CHANGE)

    def discarded(self):
        return self.filter(status=Service.DISCARDED)

    def on_hold(self):
        return self.filter(status=Service.ON_HOLD)

    def lost(self):
        return self.filter(status=Service.LOST)


class Service(BaseModel):
    # ACTIVE = 1
    # PENDING = 2
    # INACTIVE = 3
    # NOT_INSTALLED = 4
    # FREE = 5
    # INSTALL_REJECTED = 6
    # OFF_PLAN_SALE = 7
    # DELINQUENT = 8
    # TO_RESCHEDULE = 9
    # HOLDER_CHANGE = 10
    # DISCARDED = 11
    # ON_HOLD = 12
    # LOST = 13

    # NUEVO STATUS
    ACTIVE = 1
    PENDING = 2
    INACTIVE = 3
    NOT_INSTALLED = 4 # <=  + (TO RESCHEDULE)
    FREE = 5
    INSTALL_REJECTED = 6
    DELINQUENT = 7
    HOLDER_CHANGE = 8 
    DISCARDED = 9 # <= + (OFF_PLAN_SALE)
    ON_HOLD = 10
    LOST = 11

    STATUS_CHOICES = (
        (ACTIVE, _("Activo")), #activo
        (PENDING, _("Por Retirar")), #por retirar
        (INACTIVE, _("Retirado")), #retirado
        (NOT_INSTALLED, _("No Instalado")), #no instalado
        (FREE, _("Canje")), #canje
        (INSTALL_REJECTED, _("Instalación Rechazada")), #instalación rechazada
        (DELINQUENT, _("Moroso")), #moroso
        (HOLDER_CHANGE, _("Cambio de Titular")), #cambio de titular
        (DISCARDED, _("Descartado")), #descartado
        (ON_HOLD, _("Baja Temporal")), #baja Temporal
        (LOST, _("Perdido, no se pudo retirar")) #perdido, no se pudo encontrar
    )

    STATUS_CHOICES_IF_ACTIVE = (
        (ACTIVE, _("Activo")),
        (PENDING, _("Por retirar")),
        (DELINQUENT, _("Moroso")),
        (HOLDER_CHANGE, _("Cambio de Titular")),
        (ON_HOLD, _("Baja Temporal")),
    )

    STATUS_CHOICES_IF_ACTIVE_SPECIAL = (
        (ACTIVE, _("Activo")),
        (PENDING, _("Por Retirar")),
        (DELINQUENT, _("Moroso")),
        (HOLDER_CHANGE, _("Cambio de Titular")),
        (ON_HOLD, _("Baja Temporal")),
        (DISCARDED, _("Descartado")),
    )

    STATUS_CHOICES_IF_PENDING = (
        (PENDING, _("Por Retirar")),
        (ACTIVE, _("Activo")),
        (INACTIVE, _("Retirado")),
        (LOST, _("Perdido, no se pudo retirar")),
    )
    
    STATUS_CHOICES_IF_INACTIVE = (
        (INACTIVE, _("Retirado")),
        (PENDING, _("Por Retirar")), 
    )

    STATUS_CHOICES_IF_NOT_INSTALLED = (
        (NOT_INSTALLED, _("No Instalado")),
        (ACTIVE, _("Activo")),
        (FREE, _("Canje")),
        (INSTALL_REJECTED, _("Instalación Rechazada")),
        (DISCARDED, _("Descartado")),
    )

    STATUS_CHOICES_IF_FREE = (
        (FREE, _("Canje")),
        (PENDING, _("Por Retirar")), 
        (HOLDER_CHANGE, _("Cambio de Titular")),
    )

    STATUS_CHOICES_IF_INSTALL_REJECTED = (
        (INSTALL_REJECTED, _("Instalación Rechazada")),
        (NOT_INSTALLED, _("No Instalado")),
    )

    STATUS_CHOICES_IF_DELINQUENT = (
        (DELINQUENT, _("Moroso")),
        (ACTIVE, _("Activo")),
        (INACTIVE, _("Retirado")),
        (DISCARDED, _("Descartado")),
    )

    STATUS_CHOICES_IF_HOLDER_CHANGE = (
        (HOLDER_CHANGE, _("Cambio de Titular")),
        (PENDING, _("Por Retirar")), 
    )

    STATUS_CHOICES_IF_DISCARDED = (
        (DISCARDED, _("Descartado")),
        (ACTIVE, _("Activo")),
        (PENDING, _("Por Retirar")), 
    )

    STATUS_CHOICES_IF_ON_HOLD = (
        (ON_HOLD, _("Baja Temporal")),
        (ACTIVE, _("Activo")),
        (PENDING, _("Por Retirar")), 
    )

    STATUS_CHOICES_IF_LOST = (
        (LOST, _("Perdido, no se pudo retirar")),
        (PENDING, _("Por Retirar")), 
    )

    STATUS_CLASSES = {
        ACTIVE: "success",
        PENDING: "warning",
        INACTIVE: "danger",
        HOLDER_CHANGE: "danger",
    }

    BOLETA = 1
    FACTURA = 2
    NOTA_DE_VENTA = 3

    DOCUMENT_CHOICES = (
        (BOLETA, _("boleta")),
        (FACTURA, _("factura")),
        (NOTA_DE_VENTA, _("nota de venta")),
    )

    NETWORK_CABLE = 1
    GPON = 2
    GEPON = 3
    ANTENNA = 4

    TECHNOLOGY_KIND_CHOICES = (
        (NETWORK_CABLE, _("UTP")),
        (GPON, _("GPON")),
        (GEPON, _("GEPON")),
        (ANTENNA, _("antenna")),
    )

    HOME = 1
    COMPANY = 2
    EXCHANGE = 3
    CUSTOMER_CHOICES = (
        (HOME, _("home")),
        (COMPANY, _("company")),
        (EXCHANGE, _("exchange")),
    )

    tags = TaggableManager(blank=True)
    number = models.PositiveIntegerField(_("number"), default=0)
    customer = models.ForeignKey("Customer", verbose_name=_("customer"))
    document_type = models.PositiveSmallIntegerField(
        _("document type"), default=BOLETA, choices=DOCUMENT_CHOICES
    )
    street = models.CharField(_("street"), max_length=75, blank=True)
    house_number = models.CharField(_("house number"), max_length=75, blank=True)
    apartment_number = models.CharField(
        _("apartment number"), max_length=75, blank=True
    )
    tower = models.CharField(_("tower"), max_length=75, blank=True)
    location = models.CharField(
        _("location"), max_length=5, choices=COMMUNE_CHOICES, blank=True
    )
    plan = models.ForeignKey("Plan", verbose_name=_("plan"))
    promos = models.ManyToManyField("Promo", blank=True)
    promotions = models.ManyToManyField("Promotions", blank=True)
    price_override = models.DecimalField(
        _("price override"), max_digits=12, decimal_places=2, null=True, blank=True
    )
    due_day = models.PositiveSmallIntegerField(_("due day"), default=5)
    activated_on = models.DateTimeField(_("activated on"), null=True, blank=True)
    installed_on = models.DateTimeField(_("installed on"), null=True, blank=True)
    uninstalled_on = models.DateTimeField(_("uninstalled on"), null=True, blank=True)
    activation_fee = models.DecimalField(
        _("activation fee"), max_digits=12, decimal_places=2, default=0
    )
    expired_on = models.DateTimeField(_("expired on"), null=True, blank=True)
    status = models.PositiveSmallIntegerField(
        _("status"), default=NOT_INSTALLED, choices=STATUS_CHOICES
    )
    seller = models.ForeignKey(
        "Seller", verbose_name=_("seller"), null=True, blank=True
    )
    technician = models.ForeignKey(
        "hr.Technician", verbose_name=_("technician"), null=True, blank=True
    )
    node = models.ForeignKey("infra.Node", verbose_name=_("connection node"), null=True)
    original_node = models.ForeignKey(
        "infra.Node",
        verbose_name=_("original node"),
        null=True,
        blank=True,
        related_name="+",
    )
    network_mismatch = models.BooleanField(_("network mismatch"), default=False)
    node_mismatch = models.ForeignKey(
        "infra.Node",
        verbose_name=_("node mismatch"),
        null=True,
        blank=True,
        related_name="+",
    )
    notes = models.TextField(_("notes"), blank=True)
    billing_notes = models.TextField(_("billing notes"), blank=True)
    network_notes = models.TextField(_("network notes"), blank=True)
    internal_notes = models.TextField(_("internal notes"), blank=True)
    ssid = models.CharField(_("SSID"), max_length=255, blank=True, null=True)
    ssid_5g = models.CharField(_("SSID 5G"), max_length=255, blank=True, null=True)
    password = models.CharField(_("password"), max_length=255, blank=True, null=True)
    technology_kind = models.PositiveSmallIntegerField(
        _("technology kind"), default=NETWORK_CABLE, choices=TECHNOLOGY_KIND_CHOICES
    )
    pics = GenericRelation("gallery.Pic")
    documents = GenericRelation("documents.Document")
    allow_auto_cut = models.BooleanField(_("alow auto cut"), default=True)
    seen_connected = models.NullBooleanField(_("seen connected"), default=None)
    mac_onu = models.CharField(max_length=17, blank=True)
    objects = ServiceQuerySet.as_manager()
    operator = models.ForeignKey("Operator", default=1)
    unified_billing = models.BooleanField(_("unified billing"), default=False)
    flag = JSONField(_("Flag"), null=True, blank=False)

    _original_status = None
    customer_type = models.PositiveSmallIntegerField(
        _("document type"), choices=CUSTOMER_CHOICES, null=True
    )

    id_pulso = models.PositiveIntegerField(blank = True, null=True)
    street_location_id = models.PositiveIntegerField(blank = True, null=True)
    
    def __init__(self, *args, **kwargs):
        super(Service, self).__init__(*args, **kwargs)
        self._original_status = self.status

    def set_cache_data(self):
        data = {
            'operator':self.operator,
            'number':self.number,
            'house_number':self.house_number,
            'apartment_number':self.apartment_number, 
            'tower':self.tower,
            'location':self.location,
            'price_override': self.price_override,
            'due_day': self.due_day,
            'activated_on': self.activated_on,
            'installed_on': self.installed_on,
            'uninstalled_on': self.uninstalled_on,
            'expired_on': self.expired_on,
            'status': self.status,
            'notes': self.notes,
            'billing_notes': self.billing_notes,
            'network_notes': self.network_notes,
            'internal_notes': self.internal_notes,
            'ssid':self.ssid,
            'ssid_5g':self.ssid_5g,
            'password':self.password,
            'technology_kind':self.technology_kind,
            'customer_type':self.customer_type,
            'rut':self.customer.rut,
            'composite_address':self.customer.composite_address,
            'name':self.customer.name,
            'text_service_1':'{} - {} - {}'.format(self.customer.rut, self.customer.name, self.customer.composite_address),
            'text_service_2':'{} - {}'.format(self.number, self.customer.name),
            'plan':{
              'id':self.plan.id,
              'name':self.plan.name
            }
        }
        cache.set('service_data_{}'.format(self.number), data, 3600*24)
        cache.set('service_raw_{}'.format(self.number), self, 3600*24)

    def get_cache_data(self):
        return cache.get('service_data_{}'.format(self.number))

    def get_address(self):

        res = get_complete_address(self.id_pulso)   
        
        if res['ok']:
            return res['data']

        return True

    def action_log_plans(self):
        first, *hx = self.history.order_by("history_date")
        history = [first]
        for hh in hx:
            if history[-1].plan != hh.plan:
                history.append(hh)
        return reversed(history)

    def last_ip_history_primary_equipment(self):
        if self.primary_equipment:
            first, *hx = self.primary_equipment.history.order_by("history_date")
            history = [first]
            for hh in hx:
                if history[-1].ip != hh.ip:
                    history.append(hh)
            last = history.pop()
            return last
        return None

    def get_statement(self, limit=15, offset=0):
        records = self.financialrecordservice_set.all()
        previous_balance = 0
        current_balance = previous_balance

        field_names = [
            field.get_attname() for field in FinancialRecordService._meta.fields
        ] + ["balance", "absolute_url","original"]
        EnhancedRecord = namedtuple("EnhancedRecord", field_names)
        enhanced_results = []

        for record in reversed(records.values()):
            #print(record)
            if record["record_id"].startswith("P"):
                record["absolute_url"] = reverse(
                    "payment-detail", args=[record["record_id"][1:]]
                )
            else:
                record["absolute_url"] = reverse(
                    "invoice-detail", args=[record["record_id"][1:]]
                )

            if record["original_charge"]:
                record["original"]=record["original_charge"]
            else:
                record["original"]=record["original_credit"]
            current_balance -= record["charge"] or 0
            current_balance += record["credit"] or 0
            record["balance"] = current_balance
            enhanced_record = EnhancedRecord(**record)
            enhanced_results.insert(0, enhanced_record)

        return dict(
            initial_balance=previous_balance,
            final_balance=current_balance,
            records=enhanced_results,
        )
    
    def get_statement_actual(self, limit=15, offset=0):
        records = self.financialrecordservice_set.all()
        previous_balance = 0
        current_balance = previous_balance

        for record in reversed(records.values()):
            current_balance -= record["charge"] or 0
            current_balance += record["credit"] or 0
            record["balance"] = current_balance
            #print(current_balance)

        return dict(
            initial_balance=previous_balance,
            final_balance=current_balance,
        )
        
    def get_absolute_url(self):
        return reverse("service-detail", kwargs={"pk": self.pk})

    @property
    def latest_network_changes(self):
        return self.networkstatuschange_set.order_by("-created_at")[:10]

    @property
    def leases(self):
        from operator import itemgetter
        from .mikrotik import get_leases
        try:
            leases = get_leases(self.node.mikrotik_ip,
                                self.node.mikrotik_username,
                                self.node.mikrotik_password)
            return sorted(leases, key=itemgetter('host_name'))
        except Exception as e:
            print(str(e))
            return []

    @property
    def network_status(self):

        ### Giuli Aquí hacer consulta a sentinel

        is_active = self.status == self.ACTIVE
        has_primary = self.primary_equipment

        if has_primary:
            if self.seen_connected is None:
                return ("default", "Otro")
            elif self.seen_connected:
                return ("success", "En línea")
            else:
                return ("danger", "Cortado")
        else:
            return ("default", "Otro")

        # temp fix ^^^^

        primary = self.primary_equipment

        if primary:
            if primary.ip == "0.0.0.0":
                return ("default", "Otro")
            else:
                from customers.mikrotik import get_client_status

                try:
                    is_there = get_client_status(
                        self.node.mikrotik_ip,
                        self.node.mikrotik_username,
                        self.node.mikrotik_password,
                        primary.ip,
                    )
                    if is_there:
                        self.__class__.objects.filter(pk=self.pk).update(
                            seen_connected=False
                        )
                        return ("danger", "Cortado")
                    else:
                        self.__class__.objects.filter(pk=self.pk).update(
                            seen_connected=True
                        )
                        return ("success", "En línea")
                except:
                    self.__class__.objects.filter(pk=self.pk).update(
                        seen_connected=None
                    )
                    return ("default", "Otro")
        else:
            self.__class__.objects.filter(pk=self.pk).update(seen_connected=None)
            return ("default", "Otro")

    def network_action_log(self):
        return self.action_log(just=["ssid", "password", "network_notes", "node_id"])

    def clean(self, *args, **kwargs):
        if self.expired_on is not None and self.activated_on is not None:
            if self.expired_on <= self.activated_on:
                raise ValidationError(
                    _("The expiration date can not be less than the activation date")
                )
        super(Service, self).clean(*args, **kwargs)

    @property
    def status_history(self):
        first, *hx = self.history.order_by("history_date")
        history = [first]
        for hh in hx:
            if history[-1].status != hh.status:
                history.append(hh)
        return reversed(history)

    @property
    def notes_history(self):
        first, *hx = self.history.order_by("history_date")
        history = [first]
        for hh in hx:
            if history[-1].notes != hh.notes:
                history.append(hh)
        return reversed(history)

    @property
    def billing_notes_history(self):
        first, *hx = self.history.order_by("history_date")
        history = [first]
        for hh in hx:
            if history[-1].billing_notes != hh.billing_notes:
                history.append(hh)
        return reversed(history)

    @property
    def dates_history(self):
        first, *hx = self.history.order_by("history_date")
        history = [first]
        for hh in hx:
            if any(
                [
                    getattr(history[-1], attr) != getattr(hh, attr)
                    for attr in (
                        "created_at",
                        "activated_on",
                        "installed_on",
                        "uninstalled_on",
                        "expired_on",
                    )
                ]
            ):
                history.append(hh)
        return reversed(history)

    @property
    def agent(self):
        return self.history.earliest().history_user

    @property
    def get_status_class(self):
        return self.STATUS_CLASSES.get(self.status, "default")

    @property
    def primary_equipment(self):
        try:
            return self.networkequipment_set.get(primary=True)
        except (
            self.networkequipment_set.model.DoesNotExist,
            self.networkequipment_set.model.MultipleObjectsReturned,
        ):
            return None

    @property
    def slug(self):
        return self.pk

    @property
    def composite_address(self):
        partial_address = "{} {}".format(self.best_street, self.best_house_number)

        if self.best_apartment_number.strip():
            partial_address += " dpto {}".format(self.best_apartment_number)

        if self.best_tower.strip():
            partial_address += " torre {}".format(self.best_tower)

        return partial_address.strip()

    @property
    def address(self):
        if self.id_pulso:
            res = get_complete_address(self.id_pulso)   
            if res['ok']:
                return res['data']["street_location"]
        else:
            return self.composite_address

    address.fget.short_description = u"Dirección"

    @property
    def best_old_address(self):
        return self.address #or self.customer.address

    @property
    def best_address(self):
        if self.id_pulso:
            res = get_complete_address(self.id_pulso)   
            if res['ok']:
                return res['data']["street_location"]
        else:
            return (
                self.composite_address.strip()
                or self.address.strip()
                #or self.customer.address
            )

    @property
    def best_street(self):
        return self.street #or self.customer.street

    @property
    def best_house_number(self):
        return self.house_number #or self.customer.house_number

    @property
    def best_apartment_number(self):
        return self.apartment_number #or self.customer.apartment_number

    @property
    def best_tower(self):
        return self.tower #or self.customer.tower

    @property
    def best_location(self):
        return self.get_location_display() #or self.customer.get_location_display()

    @property
    def commune(self):
        return self.best_location

    @property
    def best_raw_location(self):
        return self.location #or self.customer.location

    @property
    def best_activity(self):
        return self.customer.commercial_activity

    def plan_days(self, year, month):
        month_days = self.month_days(year, month)
        first_month_day = timezone.datetime(
            year, month, 1, tzinfo=timezone.get_default_timezone()
        )
        last_month_day = timezone.datetime(
            year, month, month_days, 23, 59, 59, tzinfo=timezone.get_default_timezone()
        )

        # TODO: validate that expired_on is always > activated_on
        if (
            self.expired_on
            and self.activated_on
            and self.expired_on < self.activated_on
        ):
            raise ValueError("Expiration date must be greater than activation date")

        activated_this_month = (
            self.activated_on and first_month_day <= self.activated_on <= last_month_day
        )
        expires_this_month = (
            self.expired_on and first_month_day <= self.expired_on <= last_month_day
        )
        expired_long_ago = self.expired_on and self.expired_on < first_month_day

        plan_days = month_days

        if activated_this_month:
            plan_days -= self.activated_on.day - 1
        if expires_this_month:
            plan_days -= month_days - self.expired_on.day + 1
        if expired_long_ago:
            plan_days = 0

        return plan_days

    def month_days(self, year, month):
        return monthrange(year, month)[1]

    def price(self, dt=None):
        if dt is None:
            dt = timezone.now()

        days = self.plan_days(dt.year, dt.month)
        price = self.price_override or self.plan.price
        month_days = self.month_days(dt.year, dt.month)

        proportional_price = days * price / month_days
        if self.plan.uf:
            return proportional_price
        else:
            return ceil(proportional_price)

    def activation_fee2(self, dt=None):
        if dt is None:
            dt = timezone.now()

        if (
            self.activated_on
            and self.activated_on.year == dt.year
            and self.activated_on.month == dt.month
        ):
            return self.activation_fee or 0
        else:
            return 0

    @property
    def get_activation_fee_display(self):
        if self.plan.uf:
            return "{} UF".format(self.activation_fee2())
        else:
            return "${}".format(ceil(self.activation_fee2()))

    @property
    def get_price_display(self):
        if self.plan.uf:
            return "{} UF".format(self.price())
        else:
            return "${}".format(ceil(self.price()))

    @property
    def current_due_date(self):
        today = timezone.now().date()
        return date(today.year, today.month, self.due_day)

    @property
    def next_due_date(self):
        today = timezone.now().date()
        current = self.current_due_date
        try:
            return current.replace(month=today.month + 1)
        except ValueError:
            if current.month == 12:
                current.replace(year=today.year + 1, month=1)

    @property
    def prev_due_date(self):
        today = timezone.now().date()
        current = self.current_due_date
        try:
            return current.replace(month=today.month - 1)
        except ValueError:
            if current.month == 1:
                current.replace(year=today.year - 1, month=12)

    @property
    def latest_invoice(self):
        try:
            return self.invoice_set.latest("due_date")
        except Invoice.DoesNotExist:
            return None

    @property
    def billing_pending(self):
        return not (
            self.latest_invoice
            and self.current_due_date <= self.latest_invoice.due_date
        )

    @property
    def is_active(self):
        return self.status == self.ACTIVE

    @property
    def has_outstanding_balance(self):
        return self.latest_invoice and not self.latest_invoice.paid_on

    def generate_invoice(self):
        if self.billing_pending:
            with transaction.atomic():
                now = timezone.now()
                today = now.date()
                latest_invoice = self.latest_invoice
                new_invoice = Invoice.objects.create(
                    Service=self, due_date=self.next_due_date
                )

                items = []

                # charge normal plan
                items.append(InvoiceItem(description=self.plan, price=self.price()))

                if latest_invoice and not latest_invoice.paid:
                    # charge previous debt
                    items.append(
                        InvoiceItem(
                            description="last unpaid balance",
                            price=latest_invoice.total,
                        )
                    )

                # apply extra credits and charges
                credits = self.credit_set.filter(cleared_at__isnull=True)
                charges = self.charge_set.filter(cleared_at__isnull=True)

                for c in credits:
                    c.cleared_at = now
                    c.save(update_fields=["cleared_at"])
                    items.append(
                        InvoiceItem(description=c.description, price=-c.amount)
                    )

                for c in charges:
                    c.cleared_at = now
                    c.save(update_fields=["cleared_at"])
                    items.append(InvoiceItem(description=c.description, price=c.amount))

                new_invoice.item_set.add(*items, bulk=False)
                return new_invoice

    @property
    def monthly_price(self):
        return self.price_override or self.plan.price

    @property
    def pdf_url(self):
        return reverse_lazy("generate-service-pdf", kwargs={"pk": self.pk})

    def get_absolute_url(self):
        return reverse("service-detail", args=[self.slug])

    def __str__(self):
        return " - ".join([str(self.number), str(self.plan)])

    def save(self, *args, **kwargs):
        cache.delete('service_data_{}'.format(self.number))
        cache.delete('service_raw_{}'.format(self.number))
        super(Service, self).save(*args, **kwargs)
        self.set_cache_data()
        self.customer.service_last_modified = timezone.now()
        self.customer.save(update_fields=["service_last_modified"])

    class Meta:
        verbose_name = "Servicio"
        verbose_name_plural = "Servicios"
        ordering = ["-number"]
        permissions = (
            ("export_service", "Can export service data"),
            ("view_service", "Can view service data"),
            ("list_service", "Can list service data"),
            ("view_dashboard", "Can view the dashboard"),
        )


class Call(BaseModel):
    TECHNICAL_COMPLAINT = 1
    COMMERCIAL_COMPLAINT = 2
    TECHNICAL_REQUEST = 3
    COMMERCIAL_REQUEST = 4
    PAYMENT_PROBLEMS = 5
    OTHER_TECHNICAL = 6
    OTHER_COMMERCIAL = 7
    WIFI_PROBLEMS = 8
    GAMING_PROBLEMS = 9
    SPEED_PROBLEMS = 10
    PAYMENT_NOTICE = 11
    WRONG_DROP = 12

    SUBJECT_CHOICES = (
        (TECHNICAL_COMPLAINT, _("technical complaint")),
        (COMMERCIAL_COMPLAINT, _("commercial complaint")),
        (TECHNICAL_REQUEST, _("technical request")),
        (COMMERCIAL_REQUEST, _("commercial request")),
        (PAYMENT_PROBLEMS, _("payment problems")),
        (OTHER_TECHNICAL, _("other technical")),
        (OTHER_COMMERCIAL, _("other commercial")),
        (WIFI_PROBLEMS, _("wifi problems")),
        (GAMING_PROBLEMS, _("gaming problems")),
        (SPEED_PROBLEMS, _("speed problems")),
        (PAYMENT_NOTICE, _("payment notice")),
        (WRONG_DROP, _("wrong drop")),
    )

    answered_by = models.ForeignKey("auth.User", verbose_name=_("answered by"))
    subject = models.PositiveSmallIntegerField(
        _("subject"), default=TECHNICAL_COMPLAINT, choices=SUBJECT_CHOICES
    )
    phone_number = models.CharField("teléfono", max_length=255, blank=True, null=True)
    notes = models.TextField(_("call description"), blank=True)
    recording = models.FileField(
        _("grabación"), upload_to="calls", null=True, blank=True
    )
    qa_comment = models.TextField("comentario QA", blank=True)
    service = models.ForeignKey("Service", blank=True, null=True)

    def subject_description(self):
        return self.SUBJECT_CHOICES[self.subject - 1][1]

    @property
    def truncated_notes(self):
        return truncatewords(self.notes, 5)

    def get_absolute_url(self):
        return reverse("call-detail", kwargs={"call_pk": self.pk})

    class Meta:
        ordering = ["-pk"]
        permissions = (("view_call", "Can view calls"), ("list_call", "Can list calls"))


class CallQA(BaseModel):
    SERVICE_HIRING_YES = '1'
    SERVICE_HIRING_NO = '2'

    SERVICE_HIRING_CHOICES = ((SERVICE_HIRING_YES, _('Yes')),
                      (SERVICE_HIRING_NO, _('No')))

    KIND_CALL = 1
    KIND_CHANNEL = 2

    KIND_CHOICES = ((KIND_CALL, 'Llamada'),
                    (KIND_CHANNEL, 'Canal'))

    call_date = models.DateField(_('Date'))
    call_time = models.TimeField(_('Time'))
    agent = models.ForeignKey('hr.Agent', verbose_name=_('Voip Now Extension'))
    customer_phone = models.CharField(_('Customer phone number'), max_length=255, blank=True, null=True)
    rut = models.CharField(_('Rut'), max_length=20)
    service = models.ManyToManyField('Service', blank=True)
    service_hiring = models.CharField(_('Service hiring'), max_length=1, choices=SERVICE_HIRING_CHOICES, blank=True, null=True)
    #Tipificación
    typing = models.PositiveSmallIntegerField(_('Typing'))
    #Categoria
    category = models.PositiveSmallIntegerField(_('Category'))
    #Subcategoria
    subcategory = models.PositiveSmallIntegerField(_('Subcategory'))
    address = models.CharField(_('Address'), max_length=255)
    email = models.EmailField('Correo', null=True, blank=True)
    kind = models.PositiveSmallIntegerField('Tipo', choices=KIND_CHOICES)
    channel = models.ForeignKey('QAChannel', blank=True, null=True)
    qa_comment = models.TextField('Observaciones QA', max_length=1255, blank=True)
    indicators = models.ManyToManyField('QAIndicator')
    indicators_apply = models.ManyToManyField('QAIndicator', related_name="indicators_apply", blank=True)
    created_by = models.ForeignKey('auth.User', verbose_name=_('Crated_by'), default=1)

    class Meta:
        ordering = ['-pk']
        permissions = (('view_call_qa', 'Can view calls QA'),
                       ('list_call_qa', 'Can list calls QA'))

class QAIndicator(BaseModel):

    description = models.CharField(_('Description'), max_length=255)
    parent = models.ForeignKey('QAIndicator', verbose_name=_('Parent'), blank=True, null=True)

    def __str__(self):
        return self.description

    class Meta:
        ordering = ['-pk']
        permissions = (('view_qaindicator', 'Can view calls QA indicator'),
                       ('list_qaindicator', 'Can list calls QA indicator'))


class QAMonitoring(BaseModel):

    date = models.DateField(_('Date'))
    agent = models.ForeignKey('hr.Agent', verbose_name=_('Voip Now Extension'))
    rut = models.CharField(_('Rut'), max_length=20)
    reason = models.TextField(_('Reason'), max_length=255)
    address = models.CharField(_('Address'), max_length=255)
    phone = models.CharField(_('Customer phone number'), max_length=75)
    comment = models.TextField(_('QA Comment'), max_length=255, blank=True)
    service = models.ForeignKey('Service', blank=True, null=True)
    active = models.BooleanField(_('Active'), default=True)
    email = models.EmailField('Correo')
    created_by = models.ForeignKey('auth.User', verbose_name="created_by", blank=True)
    subcategory = models.PositiveSmallIntegerField('Subcategoria', blank=True, null=True)

    def __str__(self):
        return self.reason

    class Meta:
        ordering = ['-pk']
        permissions = (('view_qamonitoring', 'Can view calls QA monitoring'),
                       ('list_qamonitoring', 'Can list calls QA monitoring'))

class QATracing(BaseModel):

    solved = models.BooleanField(_('Solved'), default=False)
    reason = models.TextField(_('Reason'), max_length=255)
    comment = models.TextField(_('QA Comment'), max_length=255, blank=True)
    monitoring = models.ForeignKey('QAMonitoring', verbose_name=_('Monitoring'))

    def __str__(self):
        return self.reason

    class Meta:
        ordering = ['-pk']
        permissions = (('view_qatracing', 'Can view calls QA tracing'),
                       ('list_qatracing', 'Can list calls QA tracing'))

class QAChannel(BaseModel):

    name = models.CharField('Nombre', max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-pk']
        permissions = (('view_qachannel', 'Can view QA channel'),
                       ('list_qachannel', 'Can list QA channel'))


class Plan(BaseModel):
    INTERNET = 1
    DUO = 2
    TELEFONIA = 3
    RED = 4
    TELEVISION = 5
    CATEGORY_CHOICES = (
        (INTERNET, "Internet"),
        (DUO, "Duo"),
        (TELEFONIA, "Telefonía"),
        (RED, "Red"),
        (TELEVISION, "Televisión"),
    )

    HOGAR = 1
    EMPRESA = 2
    TYPE_CHOICES = (
        (HOGAR, "Hogar"),
        (EMPRESA, "Empresa"),
    )
    
    name = models.CharField(_("name"), max_length=75)
    description_facturacion = models.CharField("Descripción facturación", max_length=75, null=True, blank=True)
    price = models.DecimalField(_("price"), max_digits=12, decimal_places=2)
    category = models.PositiveSmallIntegerField(
        _("category"), default=INTERNET, choices=CATEGORY_CHOICES
    )
    type_plan = models.PositiveSmallIntegerField(
        _("type"), default=HOGAR, choices=TYPE_CHOICES
    )
    uf = models.BooleanField(_("in UF"), default=False)
    active = models.BooleanField(_("active"), default=True)
    operator = models.ForeignKey("Operator", default=1)
    currency = models.ForeignKey("Currency", null=True, blank=True)

    def __str__(self):
        result = (
            intcomma(self.price).replace(".", "*").replace(",", ".").replace("*", ",")
        )
        total = result.split(",")
        if len(total)==2:
            if total[1]=="00":
                return " - ".join([self.name, str(total[0])])
            else:
                return " - ".join([self.name, str(result)])
        else:
            return " - ".join([self.name, str(result)])

    class Meta:
        verbose_name = "Plan"
        verbose_name_plural = "Planes"
        ordering = ["-pk"]
        permissions = (("list_plan", "Can list plans"),)


class Invoice(BaseModel):
    PAID = 1
    OVERDUE = 2
    PENDING = 3

    STATUS_CHOICES = ((PAID, "Pagada"), (OVERDUE, "Vencida"), (PENDING, "Pendiente"))

    BOLETA = 1
    FACTURA = 2
    NOTA_DE_CREDITO = 3
    AJUSTE_POR_CARGO = 4
    DEVOLUCION = 5
    NOTA_DE_COBRO = 6
    NOTA_DE_VENTA = 7
    NOTA_DE_DEBITO = 8
    AJUSTE_INCOBRABLE = 9
    INVOICE_CHOICES = (
        (BOLETA, "Boleta"),
        (FACTURA, "Factura"),
        (NOTA_DE_CREDITO, "Nota de crédito"),
        (AJUSTE_POR_CARGO, "Ajuste por cargo"),
        (DEVOLUCION, "Devolución"),
        (NOTA_DE_COBRO, "Nota de cobro"),
        (NOTA_DE_VENTA, "Nota de venta"),
        (NOTA_DE_DEBITO, "Nota de débito"),
        (AJUSTE_INCOBRABLE, "Ajuste incobrable"),
    )

    MANUAL = 1  # Este es el default, incluye todos los de carga manual y los cargados por excel sin problema.
    NOT_FOUND = 2
    MULTIPLE_SERVICES = 3
    TYPE_OPTIONS = (
        (MANUAL, "manual"),
        (NOT_FOUND, "not found"),
        (MULTIPLE_SERVICES, "multiples services"),
    )

    kind = models.PositiveSmallIntegerField(
        _("kind"), default=1, choices=INVOICE_CHOICES
    )
    folio = models.PositiveIntegerField(default=0)
    customer = models.ForeignKey("Customer", verbose_name=_("customer"), null=True, blank=True)
    due_date = models.DateTimeField(_("due date"))
    #total = models.PositiveIntegerField(_("total"), default=0)
    total = models.DecimalField(_("total"), max_digits=12, decimal_places=2, default=0)
    comment = models.CharField(_("comment"), max_length=255, default="")
    paid_on = models.DateTimeField(_("paid on"), null=True, blank=True)
    file = models.ImageField(_("file"), upload_to="invoices", null=True, blank=True)
    file_standard = ImageSpecField(
        source="file",
        processors=[SmartResize(800, 600)],
        format="JPEG",
        options={"quality": 80},
    )
    file_thumbnail = ImageSpecField(
        source="file",
        processors=[ResizeToFill(320, 240)],
        format="JPEG",
        options={"quality": 60},
    )

    operator = models.ForeignKey("Operator", default=1)
    service = models.ManyToManyField("Service", blank=True)
    credit = models.ManyToManyField("CreditApplied", blank=True)
    charge = models.ManyToManyField("ChargeApplied", blank=True)
    kind_load = models.PositiveSmallIntegerField(
        _("kind_load"), default=MANUAL, choices=TYPE_OPTIONS
    )
    iva = models.DecimalField(
        _("iva"), max_digits=5, decimal_places=2, blank=True, null=True
    )
    barcode = models.TextField(_("Barcode"), blank=True, null=True)
    parent_id = models.ForeignKey(
        "self",
        verbose_name=_("parent invoice"),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    json = JSONField(_("details"), null=True, blank=False)
    observation =models.CharField(_("observation"), max_length=255, blank=True, null=True)
    sended = models.BooleanField(_("sended"), default=False)
    pdf_date = models.DateTimeField(_("pdf date"), blank=True, null=True)
    currency = models.ForeignKey("Currency", null=True, blank=True)
    tasa = models.DecimalField(_("tasa"), max_digits=20, decimal_places=5, null=True, blank=True)

    # new comment
    # @property
    # def total(self):
    #     return sum(self.item_set.values_list('price', flat=True))

    def status(self):
        if self.paid_on:
            return self.PAID
        elif self.due_date < timezone.now():
            return self.OVERDUE
        else:
            return self.PENDING

    def get_status_display(self):
        return self.STATUS_CHOICES[self.status - 1][1]

    def get_absolute_url(self):
        return reverse("invoice-detail", kwargs={"pk": self.pk})

    @property
    def customer_operator(self):
        customer = Customer.objects.get(pk=self.customer.pk)
        return customer.operators_ids

    @property
    def price(self):
        if self.tasa:
            return self.tasa*self.total
        return ""

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("invoice")
        verbose_name_plural = _("invoices")
        unique_together = (("kind", "folio", "customer", "operator"),)
        permissions = (
            ("search_invoice", "Can search invoices"),
            ("view_invoice", "Can view invoices"),
        )


class InvoiceItem(BaseModel):
    invoice = models.ForeignKey(
        "Invoice", related_name="item_set", verbose_name=_("invoice")
    )
    description = models.CharField(_("description"), max_length=75)
    price = models.IntegerField(_("price"))

    class Meta:
        verbose_name = _("invoice item")
        verbose_name_plural = _("invoice items")
        permissions = (("list_invoice", "Can list invoices"),)


class ExtraQuerySet(models.QuerySet):
    def pending(self):
        return self.filter(cleared_at=None)

    def cleared(self):
        return self.exclude(cleared_at=None)


class MainReason(BaseModel):
    DESCUENTO = 1
    CARGO = 2
    PROMOCIONES = 3

    TYPE_OPTIONS = (
        (DESCUENTO, "descuento"),
        (CARGO, "cargo"),
        (PROMOCIONES, "promociones"),
    )

    reason = models.CharField(_("reason"), max_length=150)
    valor = models.PositiveIntegerField(_("amount"))
    kind = models.PositiveSmallIntegerField(_("kind"), choices=TYPE_OPTIONS)
    percent = models.BooleanField(_("is percentage"), default=False)
    occurrence_number = models.PositiveIntegerField(
        _("ocurrence"), default=0
    )  # Solo para promos
    calculate_by_days = models.BooleanField(_("calculate by days"), default=False)
    active = models.BooleanField(_("active"), default=False)
    uf = models.BooleanField(_("uf"), default=False)
    status_field = models.BooleanField(_("deleted"), default=False)
    promo =  models.ForeignKey("Promotions", verbose_name=_("promo"), null=True, blank=True)
    operator = models.ForeignKey("Operator", default=2, verbose_name=_("operator"))

    def __str__(self):
        return self.reason

    class Meta:
        verbose_name = _("main reason")
        verbose_name_plural = _("main reasons")
        permissions = (
            ("view_mainreason", "Can view main reason data"),
            ("list_mainreason", "Can list main reason data"),
        )


class ReasonApplied(BaseModel):
    reason = models.ForeignKey("MainReason", verbose_name=_("reason"), null=True)
    service = models.ForeignKey("Service", verbose_name=_("service"))
    times_applied = models.PositiveIntegerField(_("times_applied"), default=0)
    number_billing = models.PositiveIntegerField(_("number billing"), default=1)
    days_count = models.PositiveIntegerField(_("number of days"), null=True)
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    cleared_at = models.DateTimeField(_("cleared at"), null=True)
    status_field = models.BooleanField(_("deleted"), default=False)
    quantity = models.PositiveIntegerField(_("quantity"), null=True)
    amount_applied = models.PositiveIntegerField(_("amount"), null=True)
    permanent = models.BooleanField(_("permanent"), default=False)
    active = models.BooleanField(_("active"), default=True)
    date_to_set_active = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True


class Credit(ReasonApplied):
    def __str__(self):
        return str(self.id) + " " + str(self.reason) + " " + str(self.service)

    class Meta:
        verbose_name = _("extra credit")
        verbose_name_plural = _("extra credits")


class Charge(ReasonApplied):
    def __str__(self):
        return str(self.id) + " " + str(self.reason) + " " + str(self.service)

    class Meta:
        verbose_name = _("extra charge")
        verbose_name_plural = _("extra charges")


class CreditApplied(BaseModel):
    credit_applied = models.ForeignKey("Credit", verbose_name=_("credit"))
    amount = models.PositiveIntegerField(_("amount"))

    def __str__(self):
        return str(self.credit_applied.reason) + " " + str(self.amount)

    class Meta:
        verbose_name = _("applied credit")
        verbose_name_plural = _("applied credits")


class ChargeApplied(BaseModel):
    charge_applied = models.ForeignKey("Charge", verbose_name=_("charge"))
    amount = models.PositiveIntegerField(_("amount"))

    def __str__(self):
        return str(self.charge_applied.reason) + " " + str(self.amount)

    class Meta:
        verbose_name = _("applied charge")
        verbose_name_plural = _("applied charges")


class Payment(BaseModel):
    TRANSBANK = 1
    CASH = 2
    CHEQUE = 3
    MASS_TRANSFER = 4
    SERVIPAG = 5
    BCI = 6
    MANUAL_TRANSFER = 7
    WEBPAY = 8
    OTHER = 9
    NOT_REGISTERED = 10
    CANCELLATION = 11
    BCI_MASS_TRANSFER = 12
    PAYPAL = 13
    MULTICAJA = 14
    CAJA_VECINA = 15

    PAYMENT_OPTIONS = (
        (TRANSBANK, _("transbank")),
        (CASH, _("cash")),
        (CHEQUE, _("cheque")),
        (MASS_TRANSFER, _("mass transfer")),
        (SERVIPAG, _("servipag")),
        (BCI, _("BCI deposit")),
        (MANUAL_TRANSFER, _("manual transfer")),
        (WEBPAY, _("webpay")),
        (OTHER, _("other")),
        (NOT_REGISTERED, "NO registrado en banco"),
        (CANCELLATION, "Condonación"),
        (BCI_MASS_TRANSFER, "Pago Masivo BCI"),
        (PAYPAL, "paypal"),
        (MULTICAJA, "multicaja"),
        (CAJA_VECINA, "caja vecina"),
    )

    MANUAL = 1  # Este es el default, incluye todos los de carga manual y los cargados por excel sin problema.
    NOT_FOUND = 2
    MULTIPLE_SERVICES = 3
    TYPE_OPTIONS = (
        (MANUAL, "manual"),
        (NOT_FOUND, "not found"),
        (MULTIPLE_SERVICES, "multiples services"),
    )

    customer = models.ForeignKey("Customer", verbose_name=_("customer"), null=True)
    paid_on = models.DateTimeField(_("paid on"))
    deposited_on = models.DateTimeField(_("deposited on"), null=True, blank=True)
    #amount = models.PositiveIntegerField(_("payment amount"), default=0)
    amount = models.DecimalField(_("payment amount"), max_digits=12, decimal_places=2, default=0)
    manual = models.BooleanField(default=False)
    kind = models.PositiveSmallIntegerField(
        _("kind"), default=MASS_TRANSFER, choices=PAYMENT_OPTIONS
    )
    kind_load = models.PositiveSmallIntegerField(
        _("kind_load"), default=MANUAL, choices=TYPE_OPTIONS
    )
    transfer_id = models.CharField(null=True, blank=True, max_length=255)
    proof = models.ImageField(_("proof"), upload_to="photos", null=True, blank=True)
    proof_standard = ImageSpecField(
        source="proof",
        processors=[SmartResize(800, 600)],
        format="JPEG",
        options={"quality": 80},
    )
    proof_thumbnail = ImageSpecField(
        source="proof",
        processors=[ResizeToFill(320, 240)],
        format="JPEG",
        options={"quality": 60},
    )
    proof2 = models.ImageField(_("proof2"), upload_to="photos", null=True, blank=True)
    proof2_standard = ImageSpecField(
        source="proof2",
        processors=[SmartResize(800, 600)],
        format="JPEG",
        options={"quality": 80},
    )
    proof2_thumbnail = ImageSpecField(
        source="proof2",
        processors=[ResizeToFill(320, 240)],
        format="JPEG",
        options={"quality": 60},
    )
    proof3 = models.ImageField(_("proof3"), upload_to="photos", null=True, blank=True)
    proof3_standard = ImageSpecField(
        source="proof3",
        processors=[SmartResize(800, 600)],
        format="JPEG",
        options={"quality": 80},
    )
    proof3_thumbnail = ImageSpecField(
        source="proof3",
        processors=[ResizeToFill(320, 240)],
        format="JPEG",
        options={"quality": 60},
    )
    proof4 = models.FileField(
        _("proof4"),
        upload_to="documents",
        validators=[FileExtensionValidator(allowed_extensions=["pdf"])],
        null=True,
        blank=True,
    )
    cleared = models.BooleanField(_("cleared"), default=False)
    comment = models.CharField(_("comment"), max_length=255, default="")
    operator = models.ForeignKey("Operator", default=1, verbose_name=_("operator"))
    bank_account = models.ForeignKey(
        "BankAccount", null=True, verbose_name=_("bank account")
    )
    service = models.ManyToManyField("Service", blank=True)
    operation_number = models.CharField(
        _("operation number"), max_length=75, blank=True
    )
    invoice = models.ManyToManyField("Invoice", blank=True)
    number_document = models.BigIntegerField(null=True, blank=True)
    tasa = models.DecimalField(_("tasa"), max_digits=20, decimal_places=5, null=True, blank=True)

    @property
    def previous_customer(self):
        changes = self.action_log(just=["customer_id"])
        if changes:
            return Customer.objects.get(pk=changes[0]["previous_values"]["customer_id"])

    @property
    def price(self):
        if self.tasa:
            return self.tasa*self.amount
        return ""
        
    def get_absolute_url(self):
        return reverse("payment-detail", kwargs={"pk": self.pk})

    @property
    def customer_operator(self):
        customer = Customer.objects.get(pk=self.customer.pk)
        return customer.operators_ids

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("payment")
        verbose_name_plural = _("payments")
        permissions = (
            ("list_payment", "Can list payments"),
            ("view_payment", "Can view payments"),
            ("cleared_payment", "Can clear payments"),
        )


class PayorRut(BaseModel):
    rut = models.CharField(_("rut"), max_length=20)
    name = models.CharField(_("name"), max_length=255, blank=True)
    customer = models.ForeignKey("Customer")

    class Meta:
        verbose_name = _("payor rut")
        verbose_name_plural = _("payor ruts")
        permissions = (("search_payorrut", "Can search payor ruts"),)


class Seller(BaseModel):
    name = models.CharField(_("name"), max_length=255)
    active = models.BooleanField(_("active"), default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("seller")
        verbose_name_plural = _("sellers")


class DurationRecord(models.Model):
    duration = models.DurationField()

    class Meta:
        managed = False


class FinancialRecord(models.Model):
    record_id = models.CharField(max_length=255, primary_key=True)
    ts = models.DateTimeField()
    paid = models.BooleanField()
    original_charge = models.DecimalField(_("original charge"), max_digits=20, decimal_places=2, null=True, blank=True)
    original_credit = models.DecimalField(_("original credit"), max_digits=20, decimal_places=2, null=True, blank=True)
    charge = models.DecimalField(_("charge"), max_digits=20, decimal_places=2)
    credit =models.DecimalField(_("credit"), max_digits=20, decimal_places=2)
    tasa = models.DecimalField(_("tasa"), max_digits=20, decimal_places=5, null=True, blank=True)
    #charge = models.PositiveIntegerField()
    #credit = models.PositiveIntegerField()
    comment = models.CharField(max_length=255)
    customer = models.ForeignKey("Customer", models.DO_NOTHING)

    class Meta:
        managed = False


class Balance(models.Model):
    customer = models.OneToOneField("Customer", models.DO_NOTHING)
    balance = models.DecimalField(_("balance"), max_digits=20, decimal_places=2)

    class Meta:
        managed = False


class FinancialRecordService(models.Model):
    record_id = models.CharField(max_length=255, primary_key=True)
    ts = models.DateTimeField()
    paid = models.BooleanField()
    original_charge = models.DecimalField(_("original charge"), max_digits=20, decimal_places=2, null=True, blank=True)
    original_credit = models.DecimalField(_("original credit"), max_digits=20, decimal_places=2, null=True, blank=True)
    charge = models.DecimalField(_("charge"), max_digits=20, decimal_places=2)
    credit =models.DecimalField(_("credit"), max_digits=20, decimal_places=2)
    tasa = models.DecimalField(_("tasa"), max_digits=20, decimal_places=5, null=True, blank=True)
    comment = models.CharField(max_length=255)
    customer = models.ForeignKey("Customer", models.DO_NOTHING)
    service = models.ForeignKey("Service", models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False


class BalanceService(models.Model):
    customer = models.OneToOneField("Customer", models.DO_NOTHING)
    balance = models.DecimalField(_("balance"), max_digits=20, decimal_places=2)
    service = models.ForeignKey("Service", models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False


class Promo(models.Model):
    name = models.CharField(_("name"), max_length=255)
    description = models.TextField(_("description"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("promo")
        verbose_name_plural = _("promos")

class Promotions(BaseModel):
    name = models.CharField(_("name"), max_length=255)
    description = models.TextField(_("description"))
    plan = models.ManyToManyField('Plan', blank=True)
    number_periods = models.PositiveIntegerField(default=1)
    info_periods = JSONField(_("Info periods"), null=True, blank=False)
    initial_date = models.DateField(null=True, blank=True)
    status_field = models.BooleanField(_("deleted"), default=False)
    active = models.BooleanField(_("active"), default=False)
    end_date = models.DateField(null=True, blank=True)
    operator = models.ForeignKey("Operator", default=2, verbose_name=_("operator"))
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("promotion")
        verbose_name_plural = _("promotions")
        ordering = ["-pk"]
        permissions = (
            ("view_promotion", "Can view promotions data"),
            ("list_promotion", "Can list promotions data"),
        )


class NetworkStatusChange(models.Model):
    service = models.ForeignKey("Service", verbose_name=_("service"))
    new_state = models.CharField(_("new state"), max_length=10)
    agent = models.ForeignKey("auth.User", verbose_name=_("agent"))
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)

    class Meta:
        verbose_name = _("network status change")
        verbose_name_plural = _("network status changes")


class Email(models.Model):
    subject = models.CharField(max_length=255)
    body = models.TextField()
    sent = models.BooleanField(default=False)


class Prospect(BaseModel):
    HOME = 1
    COMPANY = 2

    PROSPECT_CHOICES = ((HOME, "Hogar"), (COMPANY, "Empresa"))

    rut = models.CharField(_("rut"), max_length=20, null=True, blank=True)
    name = models.CharField(_("name"), max_length=255)
    kind = models.PositiveSmallIntegerField(
        _("kind"), choices=PROSPECT_CHOICES, default=HOME
    )
    location = models.CharField(_("location"), max_length=5, choices=COMMUNE_CHOICES)
    # address = JSONField(_('address'), blank=True, null=True)
    address = models.CharField(_("address"), max_length=75, null=True, blank=True)
    street = models.CharField(_("street"), max_length=75, blank=True)
    house_number = models.CharField(_("house number"), max_length=75, blank=True)
    apartment_number = models.CharField(
        _("apartment number"), max_length=75, blank=True
    )
    tower = models.CharField(_("tower"), max_length=75, blank=True)
    phone = models.CharField(_("phone"), max_length=75, blank=True)
    email = models.EmailField()
    current_company = models.CharField(_("current company"), max_length=255, blank=True)
    notes = models.TextField(_("notes"), blank=True)

    @property
    def composite_address(self):
        partial_address = "{} {}".format(self.street, self.house_number)

        if self.apartment_number.strip():
            partial_address += " dpto {}".format(self.apartment_number)

        if self.tower.strip():
            partial_address += " torre {}".format(self.tower)

        return partial_address.strip()

    @property
    def truncated_name(self):
        return truncatewords(self.name, 5)

    def get_absolute_url(self):
        return reverse("prospect-detail", kwargs={"pk": self.pk})

    class Meta:
        verbose_name = _("prospect")
        verbose_name_plural = _("prospects")
        ordering = ["-pk"]
        permissions = (
            ("view_prospect", "Can view prospect data"),
            ("list_prospect", "Can list prospect data"),
            ("search_prospect", "Can search prospect data"),
        )


class PlanOrder(BaseModel):
    KIND_CHOICES = (
        ("Instalacion Hogar - UTP 1h", "Instalacion Hogar - UTP 1h"),
        ("Instalacion Hogar - UTP 2h", "Instalacion Hogar - UTP 2h"),
        ("Instalacion Hogar - Fibra 1h", "Instalacion Hogar - Fibra 1h"),
        ("Instalacion Hogar - Fibra 2h", "Instalacion Hogar - Fibra 2h"),
        ("Instalacion Hogar - Antena 2h", "Instalacion Hogar - Antena 2h"),
        (
            "Instalacion Hogar - Apoyo Instalacion 1h",
            "Instalacion Hogar - Apoyo Instalacion 1h",
        ),
        (
            "Instalacion Hogar - Apoyo Instalacion 2h",
            "Instalacion Hogar - Apoyo Instalacion 2h",
        ),
        (
            "Instalacion Hogar - Migracion a fibra 1h",
            "Instalacion Hogar - Migracion a fibra 1h",
        ),
        (
            "Instalacion Hogar - Migracion a fibra 2h",
            "Instalacion Hogar - Migracion a fibra 2h",
        ),
        (
            "Visita Tecnica - Cliente sin servicio",
            "Visita Tecnica - Cliente sin servicio (60 min)",
        ),
        (
            "Visita Tecnica - Cambio de equipo",
            "Visita Tecnica - Cambio de equipo (60 min)",
        ),
        ("Visita Tecnica - Cablear SmarTV", "Visita Tecnica - Cablear SmarTV (60 min)"),
        ("Visita Tecnica - Otros 1h", "Visita Tecnica - Otros 1h"),
        ("Visita Tecnica - Otros 2h", "Visita Tecnica - Otros 2h"),
        ("Retiro de equipo", "Retiro de equipo (30 min)"),
        ("Mantencion red Fibra - 1 Hora", "Mantencion red Fibra - 1 Hora"),
        ("Mantencion red Fibra - 2 Horas", "Mantencion red Fibra - 2 Horas"),
        ("Mantencion red UTP - 1 Hora", "Mantencion red UTP - 1 Hora"),
        ("Mantencion red UTP - 2 Horas", "Mantencion red UTP - 2 Horas"),
        ("Mantencion Electrica - 1 Hora", "Mantencion Electrica - 1 Hora"),
        ("Mantencion Electrica - 2 Horas", "Mantencion Electrica - 2 Horas"),
        ("Mantencion Alineacion antena 1h", "Mantencion Alineacion antena 1h"),
        ("Mantencion red Cambio antena 2h", "Mantencion red Cambio antena 2h"),
    )

    service = models.ForeignKey("Service")
    kind = models.CharField("tipo", max_length=255, choices=KIND_CHOICES)
    subject = models.CharField(
        "asunto", max_length=255, default="", blank=True, null=True
    )
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    team = models.CharField(max_length=255, blank=True, null=True)
    cancelled = models.BooleanField("cancelada", default=False)

    @property
    def street_address(self):
        return self.service.best_address

    @property
    def commune(self):
        return self.service.best_location

    @property
    def estado(self):
        return "SA"

    @property
    def city(self):
        if self.service.best_raw_location.startswith("13"):
            return "Santiago"
        else:
            return self.service.best_location

    @property
    def microarea(self):
        return self.city

    @property
    def address_code(self):
        return "END_{}".format(self.customer_code)

    @property
    def customer_code(self):
        return str(self.service.number)

    @property
    def cidade(self):
        return str(self.city)

    @property
    def customer_name(self):
        return self.service.customer.name

    @property
    def customer_phone(self):
        return self.service.customer.phone

    @property
    def code(self):
        if settings.DEBUG:
            import socket

            prefix = "D-" + socket.gethostname() + "-"
        else:
            prefix = "P-"
        return prefix + str(self.pk)

    def get_availabilities(self):
        return iclass.find_availability(self)

    def auto_schedule(self):
        r = iclass.auto_schedule(self)
        print(r)
        return r

    def prepare_order(self):
        r1 = iclass.prepare_order(self)
        # r3 = iclass.find_availability(self)
        # print(r3)

        # r2 = iclass.auto_schedule(self)
        # print(r2)
        # start = r2['resultadoAgendamento']['atendimentoPlanejado']['inicio']
        # end = r2['resultadoAgendamento']['atendimentoPlanejado']['fim']
        # team = r2['resultadoAgendamento']['equipe']
        # self.__class__.objects.filter(pk=self.pk).update(start=start,
        # end=end,
        #                                                 team=team)

    def create_iclass_order(self, start, end):
        return iclass.create_order(self, start, end)

    def reschedule_iclass_order(self, start, end):
        return iclass.reschedule_order(self, start, end)

    def delete_iclass_order(self, subject=None):
        iclass.delete_order(self, subject_override=subject)
        self.team = None
        self.save()

    class Meta:
        ordering = ["-pk"]


class Region(models.Model):
    entel_id = models.CharField(max_length=20, blank=True)
    nombre = models.CharField(max_length=255)

    def __str__(self):
        return self.nombre
        # return "Region {} - {} ({})".format(self.id, self.nombre, self.entel_id)


class Comuna(models.Model):
    entel_id = models.CharField(max_length=20, blank=True)
    nombre = models.CharField(max_length=255)
    region = models.ForeignKey("Region")

    def __str__(self):
        return self.nombre
        # return "Comuna {} - {} ({})".format(self.id, self.nombre, self.entel_id)


class Calle(models.Model):
    entel_id = models.CharField(max_length=20, blank=True)
    nombre = models.CharField(max_length=255)
    comuna = models.ForeignKey("Comuna")

    def __str__(self):
        return self.nombre
        # return "Calle {} - {} ({})".format(self.id, self.nombre, self.entel_id)


class Direccion(models.Model):
    entel_id = models.CharField(max_length=20, blank=True)
    numeroMunicipal = models.CharField(max_length=10)
    lat = models.CharField(max_length=255, blank=True)
    lon = models.CharField(max_length=255, blank=True)
    calle = models.ForeignKey("Calle")

    def __str__(self):
        return self.numeroMunicipal
        # return "Direccion {} - {} ({})".format(self.id, self.numeroMunicipal, self.entel_id)


class TagColor(models.Model):
    tag = models.CharField(max_length=255)
    html_color = models.CharField(max_length=7, default="9a9a9a")


class Operator(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=5)
    company = models.ForeignKey("Company", null=True)
    currency = models.ManyToManyField("Currency", blank=True)
    
    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("operator")
        verbose_name_plural = _("operators")

class ConfigOperator(models.Model):
    operator = models.ForeignKey("Operator")
    name = models.CharField(max_length=100)
    flag = JSONField(_("batch"), null=True, blank=False)

    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ["-pk"]
        verbose_name = _("operator configurations")
        verbose_name_plural = _("operators configurations")


class CommercialActivity(models.Model):
    name = models.CharField(max_length=255)
    code = models.IntegerField(null=True)
    type = models.ForeignKey("TypeCommercialActivity", null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("commercial activity")
        verbose_name_plural = _("commercial activities")


class CategoryCommercialActivity(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("category of commercial activity")
        verbose_name_plural = _("categories of commercial activity")


class TypeCommercialActivity(models.Model):
    name = models.CharField(max_length=255)
    category = models.ForeignKey("CategoryCommercialActivity")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("type of commercial activity")
        verbose_name_plural = _("types of commercial activity")


class Bank(models.Model):
    name = models.CharField(max_length=255)
    code = models.IntegerField(null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("bank")
        verbose_name_plural = _("banks")


class BankAccount(models.Model):
    tag = models.CharField(max_length=255)
    account_number = models.CharField(max_length=255, unique=True)
    bank = models.ForeignKey("Bank")
    operator = models.ForeignKey("Operator", default=1)
    currency = models.ForeignKey("Currency", null=True, blank=True)

    def __str__(self):
        return self.tag

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("bank account")
        verbose_name_plural = _("bank accounts")
        permissions = (
            ("list_bankaccount", "Can list bank account"),
        )


class OperatorInformation(models.Model):
    operator = models.ForeignKey("Operator")
    rut = models.CharField(_("rut"), max_length=20, unique=True)
    name = models.CharField(_("name"), max_length=100)
    address = models.CharField(_("address"), max_length=200)
    description = models.CharField(_("description"), max_length=200, blank=True)
    email = models.EmailField(blank=True)
    logo = models.ImageField(_("logo"), upload_to="operators", blank=True)
    phone = models.CharField(_("phone"), max_length=75, blank=True)
    whatsapp_phone = models.CharField(_("Whatsapp phone"), max_length=75, blank=True)
    email_comprobante = models.EmailField(_("email comprobante"), blank=True)
    account_number = models.CharField(_("account number"), max_length=75, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-operator_id"]
        verbose_name = _("operator information")
        verbose_name_plural = _("operators information")


class InvoiceBatch(models.Model):
    BOLETA = 1
    FACTURA = 2
    NOTA_DE_CREDITO = 3
    NOTA_DE_DEBITO = 8
    DEVOLUCION = 5
    NOTA_DE_COBRO = 6
    NOTA_DE_VENTA = 7
    INVOICE_CHOICES = (
        (BOLETA, "Boleta"),
        (FACTURA, "Factura"),
        (NOTA_DE_CREDITO, "Nota de crédito"),
        (NOTA_DE_DEBITO, "Nota de débito"),
        (DEVOLUCION, "Devolución"),
        (NOTA_DE_COBRO, "Nota de cobro"),
        (NOTA_DE_VENTA, "Nota de venta"),
    )
    batch = JSONField(_("batch"), null=True, blank=False)
    kind = models.PositiveSmallIntegerField(
        _("kind"), default=1, choices=INVOICE_CHOICES
    )
    created_on = models.DateTimeField(_("created on"), blank=True, null=True)
    expiration = models.DateTimeField(_("expiration"), blank=True, null=True)
    stock = models.PositiveIntegerField(_("stock"), default=0)
    operator = models.ForeignKey("Operator", default=2, verbose_name=_("operator"))
    
    def __str__(self):
        return "InvoiceBatch %d" % self.pk

    class Meta:
        ordering = ["-created_on"]
        verbose_name = _("invoice batch")
        verbose_name_plural = _("invoices batch")
        permissions = (
            ("list_invoice_batch", "Can list invoice batch"),
        )


class ProcessedBatch(models.Model):
    batch = models.ForeignKey("InvoiceBatch", null=True)
    processed = JSONField(_("processed"), null=True, blank=False)
    processed_on = models.DateTimeField(_("created on"), blank=True, null=True)
    status = models.BooleanField(_("status"), default=False)

    def __str__(self):
        return str(self.processed_on)

    class Meta:
        ordering = ["-processed_on"]
        verbose_name = _("processed batch")


class TaxationResponse(models.Model):
    response = JSONField(_("response"), null=False, blank=False)
    date = models.DateTimeField(_("date"), blank=False, null=False)

    def __str__(self):
        return str(self.date)

    class Meta:
        ordering = ["-date"]
        verbose_name = _("taxation response")


class BillingCycle(models.Model):
    processed = JSONField(_("processed"), null=True)
    start = models.DateTimeField(_("start"))
    end = models.DateTimeField(_("end"))
    operator = models.ForeignKey("Operator", default=2, verbose_name=_("operator"))

    def __str__(self):
        return str(self.start) + " - " +str(self.end)

    class Meta:
        ordering = ["-start"]
        verbose_name = _("billing cycle")
        permissions = (
            ("list_billing_cycle", "Can list billing cycle"),
        )

class Indicators(BaseModel):
    UF = 1
    IVA = 2
    DOLAR = 3
    DOLAR_INTERCAMBIO = 4
    EURO = 5
    UTM = 6
    TPM = 7
    BITCOIN = 8
    PESO_CHILENO = 9
    SOLES = 10
    INDICATOR_CHOICES = (
        (UF, "Unidad de fomento (UF)"),
        (IVA, "Impuesto sobre el Valor Agregado (IVA)"),
        (DOLAR, "Dólar observado"),
        (DOLAR_INTERCAMBIO, "Dólar acuerdo"),
        (EURO, "Euro"),
        (UTM, "Unidad Tributaria Mensual (UTM)"),
        (TPM, "Tasa Política Monetaria (TPM)"),
        (BITCOIN, "Bitcoin"),
        (PESO_CHILENO, "Peso chileno (CLP)"),
        (SOLES, "Soles peruanos (PEN)"),
    )
    kind = models.PositiveSmallIntegerField(
        _("kind"), default=UF, choices=INDICATOR_CHOICES
    )
    value = models.DecimalField(_("value"), max_digits=20, decimal_places=6)
    company = models.ForeignKey("Company", default=1, verbose_name=_("company"))

    def __str__(self):
        return str(self.kind)

    class Meta:
        ordering = ["kind", "-created_at"]
        verbose_name = _("Indicators")
        permissions = (
            ("list_indicators", "Can list indicators"),
        )

class Cartola(BaseModel):
    number = models.PositiveIntegerField()
    start = models.DateField()
    end = models.DateField()
    saldo_final=models.DecimalField(_("Saldo final"), max_digits=12, decimal_places=2)
    saldo_inicial =models.DecimalField(_("Saldo inicial"), max_digits=12, decimal_places=2) 
    total_cargo =models.DecimalField(_("Saldo final"), max_digits=12, decimal_places=2, null=True)
    total_abono =models.DecimalField(_('Total Abonos y Depositos'), max_digits=12, decimal_places=2, null=True)
    bank_account = models.ForeignKey(
        "BankAccount", verbose_name=_("bank account")
    )
    pdf = models.FileField(
        _("pdf"),
        upload_to="cartolas",
        validators=[FileExtensionValidator(allowed_extensions=["pdf"])],
        null=True,
        blank=True,
    )
    excel = models.FileField(
        _("excel"),
        upload_to="cartolas",
        null=True,
        blank=True,
    )

    def __str__(self):
        return "Cartola #" + str(self.number)

    class Meta:
        ordering = ["-created_at"]
        verbose_name = _("Cartola")
        permissions = (
            ("list_cartolas", "Can list cartolas"),
        )
    
class CartolaMovement(BaseModel):

    CARGO = 1
    ABONO = 2
    TYPE_CHOICES = (
        (CARGO, _("Cheques y otros cargos")),
        (ABONO, _("Depositos y abonos")),
    )
    cartola = models.ForeignKey("Cartola", null=True)
    date = models.DateField()
    sucursal = models.CharField(_('Sucursal'), max_length=255)
    number_document = models.BigIntegerField()
    description = models.CharField(_('Description'), max_length=255)
    total =models.DecimalField(_("Total"), max_digits=12, decimal_places=2) 
    kind = models.PositiveSmallIntegerField(
        _("kind"), default=CARGO, choices=TYPE_CHOICES
    )
    saldo_diario =models.DecimalField(_("Saldo diario"), max_digits=12, decimal_places=2) 
    payment = models.ForeignKey("Payment", blank=True, null=True)
    payment_provider = models.ForeignKey("providers.Payment", blank=True, null=True)

    def __str__(self):
        return "Movimiento Cartola " + str(self.id)

    class Meta:
        verbose_name = _("Movimiento Cartola")
        permissions = (
            ("list_movimiento_cartola", "Can list movimineto cartola"),
        )

class CalendarioCortes(BaseModel):
    #restricción de una sola al mes
    billing_date = models.DateTimeField(_("billing_date"), blank=True)
    class Meta:
        verbose_name = _("Calendario de Cortes")


class UpdateInfoTemp(BaseModel):
    # Datos principales
    customer = models.ForeignKey("Customer", verbose_name=_("customer"))
    # Datos de contacto
    email = models.EmailField()
    phone = models.CharField(_("phone"), max_length=75)
    id_pulso = models.PositiveIntegerField(blank = True, null=True)
    street_location_id = models.PositiveIntegerField(blank = True, null=True)
    # Datos de direccion actual en Customer y Servicio
    services = JSONField(_("Flag"), null=True, blank=False)

    def get_address(self):

        res = get_complete_address(self.id_pulso)   
        
        if res['ok']:
            return res['data']

        return True


class Company(BaseModel):
    name = models.CharField(max_length=100)
    country_id = models.PositiveSmallIntegerField("Country", blank=True, null=True)
    currency = models.ForeignKey("Currency", blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("company")
        verbose_name_plural = _("companies")

class UserCompany(BaseModel):
    user = models.ForeignKey("auth.User", verbose_name=_("user"))
    company = models.ManyToManyField("Company", blank=True)

    def __str__(self):
        return str(self.user) +" - " + str(self.company)

    class Meta:
        verbose_name = _("User Company")

class UserOperator(BaseModel):
    # Datos principales
    user = models.ForeignKey("auth.User", verbose_name=_("user"))
    operator =  models.PositiveSmallIntegerField(
        _("operator"), default=0
    )
    company =  models.PositiveSmallIntegerField(
        _("company"), default=0
    )
    
    def __str__(self):
        return str(self.user) +" - " + str(self.operator)

    class Meta:
        verbose_name = _("User Operator")

class Currency(BaseModel):
    name = models.CharField(max_length=100)
    symbol = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("currency")
        verbose_name_plural = _("currencies")

class GeneralExpense(BaseModel):

    type = models.ForeignKey("ExpenseType", verbose_name=_("expense_type"), null = True)
    description = models.TextField(_("description"), blank=True, null = True)
    issue_date = models.DateTimeField(_("issue_date"), null=True, blank=True)
    amount = models.DecimalField(_("amount"), null = True, max_digits = 12, decimal_places = 3)
    currency = models.ForeignKey("Currency", verbose_name=_("currency"))
    operator = models.ForeignKey("Operator", default=1)
    bank_account = models.ForeignKey("BankAccount", verbose_name=_("bank account"), null = True)

    def get_absolute_url(self):
        return reverse("general-expense-list")

    def __str__(self):
        return str(self.type.name) + ' ' + str(self.issue_date.month) + '-' + str(self.issue_date.year)

class ExpenseType(BaseModel):

    name = models.CharField(_("name"), max_length=255)

    def __str__(self):
        return self.name

