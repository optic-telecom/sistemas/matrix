import requests
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save,pre_save
from django.dispatch import receiver
from django.utils import timezone
from constance import config
from .Iris import event_status_changed_to_iris
from .models import Service, PlanOrder, NetworkStatusChange, Prospect, Payment, Invoice, Indicators
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
import urllib3 
import json

def notify_slack(webhook_url, payload):
    if config.allow_send_slack:
        return requests.post(webhook_url, json=payload)
    return False


@receiver(pre_save, sender=Service)
def activate_service(sender, instance, **kwargs):
    if instance.pk:
        old = Service.objects.get(pk=instance.pk)
        new = instance
        if old.status != Service.ACTIVE and new.status == Service.ACTIVE:
            pass  # FIXME
            #new.activated_on = timezone.now()
            #new.expired_on = None
    

@receiver(pre_save, sender=Service)
def copy_node_on_creation(sender, instance, **kwargs):
    if not instance.pk:
        instance.original_node = instance.node

@receiver(post_save, sender=Service)
def notify_if_status_changed(sender, **kwargs):
    if getattr(config, "block_slack_status_change") == 'no':
        try:
            i = kwargs['instance']
            # check if status changed
            if i._original_status == i.status:
                return False

            config_slack=ConfigOperator.objects.filter(operator=i.operator, name="Slack")
            if config_slack.count()==1:
                STATUS_WEBHOOKS = {Service.ACTIVE: config_slack[0].flag.get("ACTIVE_CUSTOMER_WEBHOOK", None),
                            Service.PENDING: config_slack[0].flag.get("PENDING_CUSTOMER_WEBHOOK", None),
                            Service.INACTIVE: config_slack[0].flag.get("INACTIVE_CUSTOMER_WEBHOOK", None),
                            Service.NOT_INSTALLED: config_slack[0].flag.get("NOT_INSTALLED_CUSTOMER_WEBHOOK", None),
                            Service.FREE: config_slack[0].flag.get("FREE_CUSTOMER_WEBHOOK", None),
                            Service.INSTALL_REJECTED: config_slack[0].flag.get("INSTALL_REJECTED_CUSTOMER_WEBHOOK", None),
                            Service.DELINQUENT: config_slack[0].flag.get("DELINQUENT_CUSTOMER_WEBHOOK", None),
                            Service.HOLDER_CHANGE: config_slack[0].flag.get("HOLDER_CHANGE_CUSTOMER_WEBHOOK", None),
                            Service.DISCARDED: config_slack[0].flag.get("DISCARDED_CUSTOMER_WEBHOOK", None),
                            Service.ON_HOLD: config_slack[0].flag.get("ON_HOLD_CUSTOMER_WEBHOOK", None),
                            Service.LOST: config_slack[0].flag.get("LOST_CUSTOMER_WEBHOOK", None),
                            }
                new_status_name = dict(Service.STATUS_CHOICES)[i.status]
                # Get webhook_url if any
                webhook_url = STATUS_WEBHOOKS.get(i.status, None)
                old_status_name = dict(Service.STATUS_CHOICES)[i._original_status] if i._original_status else None
                last_edited_by = ''
                if i.last_edited_by:
                    last_edited_by =  i.last_edited_by.username
                notify_slack(webhook_url,
                    {'text': "Servicio #{} ahora está en estado *{}* (anterior {}).".format(i.number, new_status_name, old_status_name),
                    'attachments': [{
                    'fallback': "Servicio #{}".format(i.number),
                    'color': "good",
                    'author_name': last_edited_by,
                    'author_link': "https://optic.matrix2.cl{}#activity_log".format(i.get_absolute_url()),
                    'title': "Servicio #{}".format(i.number),
                    'title_link': "https://optic.matrix2.cl{}".format(i.get_absolute_url()),
                    'text': "{}".format(i.plan.name),
                    'fields': [
                        {'title': "Cliente",
                            'value': i.customer.name,
                            'short': True},
                        {'title': "RUT",
                            'value': i.customer.rut,
                            'short': True},
                        {'title': "Dirección Servicio",
                            'value': i.best_address,
                            'short': True}
                    ],
                    'actions': [
                        {
                            'type': 'button',
                            'text': 'Ver',
                            'url': "https://optic.matrix2.cl{}".format(i.get_absolute_url()),
                            'style': 'primary'
                        },
                        {
                            'type': 'button',
                            'text': 'Editar',
                            'url': "https://optic.matrix2.cl{}edit/".format(i.get_absolute_url())
                        }
                    ]
                }]})
        except Exception as e:

            http = urllib3.PoolManager()
            i = kwargs['instance']
            fields = []
            fields.append({
                "title": "Servicio",
                "value": i.number,
                "short": True
            })
            fields.append({
                "title": "Función",
                "value": "notify_if_status_change",
                "short": True
            })
            fields.append({
                "title": "Cambio de Estado",
                "value": "Se cambió de " + str(i.STATUS_CHOICES[i._original_status-1][1])+ ' a ' + str(i.STATUS_CHOICES[i.status-1][1]) + ' sin notificar al canal #bandaancha-cliente-activo',
                "short": False
            })
            fields.append({
                "title": "Excepcion",
                "value": str(e),
                "short": False
            })
            data_slack = {
            "text": "Error signal.py",
            "attachments": [
                    {
                        "color": "#c82d10",
                        "text": "Datos",
                        "fields": fields
                    }       
                ] 
            }

            #url = 'https://hooks.slack.com/services/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'
            #notify_slack(url, {'text': 'prueba error notificación slack'})
            
            config_slack=ConfigOperator.objects.filter(operator=i.operator, name="Slack")
            if config_slack.count()==1:
                if "slack_cambio_estado_servicio" in config_slack[0].flag:
                    response_slack = http.request(
                        "POST",
                        config_slack[0].flag["slack_cambio_estado_servicio"],
                        #str('https://hooks.slack.com/services/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'),
                        body=json.dumps(data_slack),
                        headers={'Content-Type': 'application/json'}
                    )
            
            

@receiver(post_save, sender=PlanOrder)
def auto_schedule_order(sender, instance, created, **kwargs):
    if created:
        instance.prepare_order()


@receiver(post_save, sender=NetworkStatusChange)
def notify_if_network_status(sender, instance, created, **kwargs):
    if created:
        config_slack=ConfigOperator.objects.filter(operator=instance.service.operator, name="Slack")
        hook=None
        if config_slack.count()==1:
            if instance.new_state == 'activado':
                if "NETWORK_CONNECT_WEBHOOK" in config_slack[0].flag:
                    hook = config_slack[0].flag["NETWORK_CONNECT_WEBHOOK"]
            else:
                if "NETWORK_DROP_WEBHOOK" in config_slack[0].flag:
                    hook = config_slack[0].flag["NETWORK_DROP_WEBHOOK"]

        notify_slack(hook, {'text': "Servicio #{} {}.".format(instance.service.number, instance.new_state).capitalize(),
       'attachments': [{
           'fallback': "Servicio #{} {}.".format(instance.service.number, instance.new_state).capitalize(),
           'color': "good" if instance.new_state == 'activado' else "danger",
           'author_name': "{}".format(instance.agent.username),
           'author_link': "https://optic.matrix2.cl{}network/#network_changes".format(instance.service.get_absolute_url()),
           'title': "Servicio #{}".format(instance.service.number),
           'title_link': "https://optic.matrix2.cl{}network/".format(instance.service.get_absolute_url()),
           'text': "{}".format(instance.service.plan.name),
           'fields': [
               {'title': "Cliente",
                'value': instance.service.customer.name,
                'short': True},
               {'title': "RUT",
                'value': instance.service.customer.rut,
                'short': True},
               {'title': "Dirección Servicio",
                'value': instance.service.best_address,
                'short': True},
               {'title': "Estado",
                'value': instance.service.get_status_display(),
                'short': True},
               {'title': "Saldo Cliente",
                'value': "${}".format(instance.service.customer.balance.balance),
                'short': True},
           ]}]})


@receiver(post_save, sender=Prospect)
def notify_if_prospect_company(sender, instance, created, **kwargs):
    if created and instance.kind == Prospect.COMPANY:
        # No necesita el cambio de slack porque no se utiliza
        notify_slack(config.PROSPECT_WEBHOOK, {'text': "Ingresó nuevo prospecto empresa",
                                                   'attachments': [{
                                                       'fallback': "Prospecto #{}".format(instance.pk).capitalize(),
                                                       'author_name': "{}".format(instance.agent.username),
                                                       'title': "Prospecto #{}".format(instance.pk),
                                                       'title_link': "https://optic.matrix2.cl{}".format(instance.get_absolute_url()),
                                                       'text': "{}".format(instance.name)
                                                   }]})


@receiver(post_save, sender=Payment)
def notify_if_arica_payment(sender, instance, created, **kwargs):
    try:
        if created and instance.last_edited_by.username in ('karla.tambo', 'alvarotejeda', 'hso'):
            config_slack=ConfigOperator.objects.filter(operator=instance.operator, name="Slack")
            if config_slack.count()==1:
                if "ARICA_PAYMENTS_WEBHOOK" in config_slack[0].flag:
                    hook = config_slack[0].flag["ARICA_PAYMENTS_WEBHOOK"]
                    notify_slack(hook, {'text': "Ingresó nuevo pago en Arica",
                                                            'attachments': [{
                                                                'fallback': "{} - Estado de cuenta".format(instance.customer.name).capitalize(),
                                                                'author_name': "{}".format(instance.last_edited_by.username),
                                                                'title': "{} - Estado de cuenta".format(instance.customer.name),
                                                                'title_link': "https://optic.matrix2.cl{}billing/".format(instance.customer.get_absolute_url()),
                                                                'text': "{}".format(instance.customer.rut)
                                                            }]})    
    except:
        pass


@receiver(pre_save, sender=Service)
def notify_if_status_changed_to_iris(sender, **kwargs):
    if kwargs['instance']._original_status == kwargs['instance'].status:
        return False
    return event_status_changed_to_iris(kwargs['instance'])

@receiver(post_save, sender=Service)
def updateCache(sender,**kwargs):
    service = kwargs['instance']
    slug = service.pk
    createdKey = 'created_at' + str(slug)
    activatedKey = 'activated_on' + str(slug)
    installedKey = 'installed_on' + str(slug)
    uninstalledKey = 'uninstalled_on' + str(slug)
    expiredKey = "expired_on" + str(slug)

    cache.set(createdKey, service.created_at, timeout = None)
    cache.set(activatedKey, service.activated_on, timeout = None)
    cache.set(installedKey, service.installed_on, timeout = None)
    cache.set(uninstalledKey, service.uninstalled_on, timeout = None)
    cache.set(expiredKey, service.expired_on, timeout = None)
    
@receiver(pre_save, sender=Service)
def plan_change(sender, instance, **kwargs):
    try:
        old_instance = Service.objects.get(pk = instance.pk)
        if old_instance.plan.currency != instance.plan.currency: 
            # Se trae el indicador de la empresa del operador.
            indicadores=Indicators.objects.filter(company=old_instance.operator.company)
            # Si la moneda de la empresa coincide con la del servicio.
            # significa que la nueva moneda es el Dolar.
            if old_instance.plan.currency==old_instance.operator.company.currency:
                tasa=1/indicadores.filter(kind=3).last().value     
            else:
                # Si la moneda de la empresa no coincide con la del servicio.
                # significa que la nueva moneda es la de la compania.
                tasa=indicadores.filter(kind=3).last().value
            print(tasa)
            # Truncar en 5 decimales.
            tasa=round(tasa, 5)

            # Todos los pagos que son de la moneda anterior le tengo que colocar la tasa.
            Payment.objects.filter(
                service=old_instance.id,
                bank_account__currency=old_instance.plan.currency).update(tasa=tasa)

            Payment.objects.filter(
                service=old_instance.id,
                bank_account__currency=instance.plan.currency).update(tasa=1)
 
            # Todos los pagos que son de la moneda anterior le tengo que colocar la tasa.
            Invoice.objects.filter(
                service=old_instance.id,
                currency=old_instance.plan.currency).update(tasa=tasa)

            Invoice.objects.filter(
                service=old_instance.id,
                currency=instance.plan.currency).update(tasa=1)

            print("jajajja")
        else:
            # No hago nada con los pagos y boletas
            pass
    except Service.DoesNotExist:
        pass
