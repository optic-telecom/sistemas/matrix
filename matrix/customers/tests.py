from datetime import date
from math import ceil
from calendar import monthrange
from django.test import TestCase
from django.utils import timezone
from model_mommy import mommy
from subprocess import call
from .models import Service


class ServiceTestCase(TestCase):
    def test_service_full_month_price(self):
        dt = timezone.now()
        year = dt.year
        month = dt.month
        day = 1
        activation_dt = timezone.datetime(
            year, month, day, tzinfo=timezone.get_default_timezone()
        )

        plan = mommy.make("Plan")
        service = mommy.make("Service", plan=plan, activated_on=activation_dt)

        self.assertEqual(service.price(), plan.price)

    # FIXME: refactor testing for greater granularity
    #        e.g. plan_days test
    def test_service_proportional_signup(self):
        # february 2016 has 29 days
        year = 2016
        month = 2
        day = 12
        dt = timezone.datetime(year, month, day, tzinfo=timezone.get_default_timezone())
        _, month_days = monthrange(year, month)
        plan_days = month_days - day + 1

        service = mommy.make("Service", activated_on=dt)
        normal_price = service.plan.price
        proportional_price = plan_days * normal_price / month_days
        self.assertEqual(service.price(dt=dt), ceil(proportional_price))

    def test_service_proportional_leave(self):
        # february 2016 has 29 days
        year = 2016
        month = 2
        day = 12
        dt = timezone.datetime(year, month, day, tzinfo=timezone.get_default_timezone())
        _, month_days = monthrange(year, month)
        plan_days = day - 1

        service = mommy.make(
            "Service", activated_on=dt - timezone.timedelta(days=30), expired_on=dt
        )
        normal_price = service.plan.price
        proportional_price = plan_days * normal_price / month_days
        self.assertEqual(service.price(dt=dt), ceil(proportional_price))

    def test_service_proportional_signup_and_leave(self):
        # february 2016 has 29 days
        year = 2016
        month = 2
        activation_day = 12
        expiration_day = 27
        activation_dt = timezone.datetime(
            year, month, activation_day, tzinfo=timezone.get_default_timezone()
        )
        expiration_dt = timezone.datetime(
            year, month, expiration_day, tzinfo=timezone.get_default_timezone()
        )
        _, month_days = monthrange(year, month)
        plan_days = expiration_day - activation_day

        service = mommy.make(
            "Service", activated_on=activation_dt, expired_on=expiration_dt
        )
        normal_price = service.plan.price
        proportional_price = plan_days * normal_price / month_days
        self.assertEqual(service.price(dt=activation_dt), ceil(proportional_price))

    def test_service_proportional_nonsense_dates(self):
        # february 2016 has 29 days
        year = 2016
        month = 2
        activation_day = 12
        expiration_day = 27
        activation_dt = timezone.datetime(
            year, month, expiration_day, tzinfo=timezone.get_default_timezone()
        )
        expiration_dt = timezone.datetime(
            year, month, activation_day, tzinfo=timezone.get_default_timezone()
        )
        _, month_days = monthrange(year, month)
        plan_days = expiration_day - activation_day

        service = mommy.make(
            "Service", activated_on=activation_dt, expired_on=expiration_dt
        )
        normal_price = service.plan.price
        proportional_price = plan_days * normal_price / month_days
        with self.assertRaises(ValueError):
            service.price(dt=activation_dt)

    def test_prev_due_date(self):
        c = mommy.make("Service")
        today = timezone.now().date()
        due_date = date(today.year, today.month, c.due_day)
        try:
            prev_due_date = due_date.replace(month=today.month - 1)
        except ValueError:
            if due_date.month == 1:
                prev_due_date = due_date.replace(year=today.year - 1, month=12)
        self.assertEqual(c.prev_due_date, prev_due_date)

    def test_current_due_date(self):
        c = mommy.make("Service")
        today = timezone.now().date()
        due_date = date(today.year, today.month, c.due_day)
        self.assertEqual(c.current_due_date, due_date)

    def test_next_due_date(self):
        c = mommy.make("Service")
        today = timezone.now().date()
        due_date = date(today.year, today.month, c.due_day)
        try:
            next_due_date = due_date.replace(month=today.month + 1)
        except ValueError:
            if due_date.month == 12:
                next_due_date = due_date.replace(year=today.year + 1, month=1)
        self.assertEqual(c.next_due_date, next_due_date)

    def test_latest_invoice_does_not_exist(self):
        c = mommy.make("Service")
        self.assertIsNone(c.latest_invoice)

    def test_latest_invoice_single(self):
        c = mommy.make("Service")
        i = mommy.make("Invoice", service=c)
        self.assertEqual(c.latest_invoice.pk, i.pk)

    def test_latest_invoice_many(self):
        c = mommy.make("Service")
        mommy.make("Invoice", service=c, _quantity=3)
        latest_invoice = c.latest_invoice
        self.assertIsNotNone(latest_invoice.pk)
        self.assertFalse(c.invoice_set.filter(due_date__gt=latest_invoice.due_date))

    def test_billing_is_pending_first_time(self):
        c = mommy.make("Service")
        self.assertTrue(c.billing_pending)

    def test_billing_is_pending(self):
        c = mommy.make("Service")
        today = timezone.now().date()
        mommy.make("Invoice", service=c, due_date=today - timezone.timedelta(days=60))
        self.assertTrue(c.billing_pending)

    def test_billing_is_not_pending(self):
        c = mommy.make("Service")
        today = timezone.now().date()
        due_date = date(today.year, today.month, c.due_day)
        mommy.make("Invoice", service=c, due_date=due_date)
        self.assertFalse(c.billing_pending)

    def test_generate_not_pending_invoice(self):
        c = mommy.make("Service")
        mommy.make(
            "Invoice", service=c, due_date=c.current_due_date, paid_on=timezone.now()
        )

        self.assertEqual(c.invoice_set.count(), 1)
        c.generate_invoice()
        self.assertEqual(c.invoice_set.count(), 1)

    def test_generate_pending_invoice(self):
        c = mommy.make("Service")
        self.assertEqual(c.invoice_set.count(), 0)
        new_invoice = c.generate_invoice()
        self.assertEqual(c.invoice_set.count(), 1)
        self.assertEqual(new_invoice.total, c.price())

    def test_generate_pending_invoice_with_debt(self):
        c = mommy.make("Service")
        fake_debt = 4096
        prev_invoice = mommy.make("Invoice", service=c, due_date=c.prev_due_date)

        mommy.make("InvoiceItem", invoice=prev_invoice, price=fake_debt)

        self.assertEqual(c.invoice_set.count(), 1)
        new_invoice = c.generate_invoice()
        self.assertEqual(c.invoice_set.count(), 2)
        self.assertEqual(new_invoice.total, c.price() + prev_invoice.total)

    def test_generate_pending_invoice_with_extra_credit(self):
        c = mommy.make("Service")
        fake_credit = 8192
        mommy.make("Credit", service=c, amount=fake_credit)
        new_invoice = c.generate_invoice()
        self.assertEqual(new_invoice.total, c.price() - fake_credit)
        self.assertTrue(new_invoice.service.credit_set.get().cleared_at)

    def test_generate_pending_invoice_with_extra_charge(self):
        c = mommy.make("Service")
        fake_charge = 2048
        mommy.make("Charge", service=c, amount=fake_charge)
        new_invoice = c.generate_invoice()
        self.assertEqual(new_invoice.total, c.price() + fake_charge)
        self.assertTrue(new_invoice.service.charge_set.get().cleared_at)


class InvoiceTestCase(TestCase):
    def test_total(self):
        i = mommy.make("Invoice")
        mommy.make("InvoiceItem", invoice=i, _quantity=3)
        total = sum(i.item_set.values_list("price", flat=True))
        self.assertEqual(i.total, total)

    def test_is_paid(self):
        dt = timezone.now()
        i = mommy.make("Invoice", paid_on=dt)
        self.assertEqual(i.status, i.PAID)

    def test_is_overdue(self):
        date = timezone.now().date()
        day = timezone.timedelta(days=1)
        i = mommy.make("Invoice", due_date=date - day)
        self.assertEqual(i.status, i.OVERDUE)

    def test_is_pending(self):
        date = timezone.now().date()
        day = timezone.timedelta(days=1)
        i = mommy.make("Invoice", due_date=date + day)
        self.assertEqual(i.status, i.PENDING)


class ExpireServicesCommandsTestCase(TestCase):
    def test_expire_services(self):
        now = timezone.now()
        yesterday = now - timezone.timedelta(days=1)
        c1 = mommy.make(
            "Service", number=1, status=Service.ACTIVE, expired_on=yesterday
        )
        c2 = mommy.make("Service", number=2, status=Service.ACTIVE, expired_on=None)
        c3 = mommy.make(
            "Service", number=3, status=Service.INACTIVE, expired_on=yesterday
        )

        count, qs = Service.objects.expire()

        self.assertEqual(count, 1)
        self.assertSequenceEqual(qs.values_list("id", flat=True), [c1.id])


# FIXME: missing test for activation fee
# FIXME: missing test for service fee override

{
    "Documento": {
        "Encabezado": {
            "IdDoc": {
                "TipoDTE": 39,
                "Folio": 786355616,
                "FchEmis": "2020-07-24",
                "IndServicio": 3,
                "IndMntNeto": 2,
                "FchVenc": "2020-08-05",
            },
            "Emisor": {
                "RUTEmisor": "76171094-k",
                "RznSocEmisor": "OPTIC TELECOMUNICACIONES LTDA.",
                "GiroEmisor": "Servicio de telecomunicaciones e instalaci\u00f3n de redes.",
                "DirOrigen": "ANTONIA LOPEZ DE BELLO 114 OF. 403",
                "CmnaOrigen": "RECOLETA",
                "CiudadOrigen": "SANTIAGO",
            },
            "Receptor": {
                "RUTRecep": "19647604-0",
                "CdgIntRecep": 12498,
                "RznSocRecep": "Bruno Vittorio Barra Succo",
                "Contacto": "bbarrasucco@gmail.com",
                "DirRecep": "Natalie cox, 13101, Torre , Apto. 603, Casa Nro.599",
                "CmnaRecep": "Santiago",
                "CiudadRecep": "Santiago",
                "GiroRecep": "No espec\u00edfico",
            },
            "Totales": {
                "MntNeto": 20243,
                "IVA": 3846,
                "MntTotal": 24089,
                "SaldoAnterior": 0,
                "MontoNF": 0,
                "TotalPeriodo": 24089,
                "VlrPagar": 24089,
            },
        },
        "Detalle": {
            "NroLinDet": 1,
            "CdgItem": {"TpoCodigo": "INT1", "VlrCodigo": 114},
            "NmbItem": "2018 - Fibra - Internet 150Megas",
            "QtyItem": 1,
            "UnmdItem": "UN",
            "PrcItem": 16723,
            "MontoItem": 16723,
        },
        "DscRcgGlobal": [
            {
                "NroLinDR": 1,
                "TpoMov": "R",
                "GlosaDR": "prueba de cargo",
                "IndExeDR": 0,
                "ValorDR": 2508,
                "TpoValor": "$",
            },
            {
                "NroLinDR": 2,
                "TpoMov": "R",
                "GlosaDR": "cargo monto $ 1000",
                "IndExeDR": 0,
                "ValorDR": 1000,
                "TpoValor": "$",
            },
        ],
    }
}
