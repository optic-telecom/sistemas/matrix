import requests, io, base64
from .models import Service
from django.core.exceptions import ValidationError
from constance import config
from django.utils import timezone
from django.template.loader import get_template
from wkhtmltopdf.utils import render_pdf_from_template
from documents.models import Document
from io import BytesIO, StringIO
from django.core.files import File


class ServiceStateMachine(object):
    """
    Maquina de estado simple 
    para validar las reglas de cambios de estados
    """

    ACTIVE = Service.ACTIVE
    PENDING = Service.PENDING
    INACTIVE = Service.INACTIVE
    NOT_INSTALLED = Service.NOT_INSTALLED
    FREE = Service.FREE
    INSTALL_REJECTED = Service.INSTALL_REJECTED
    DELINQUENT = Service.DELINQUENT
    HOLDER_CHANGE = Service.HOLDER_CHANGE
    DISCARDED = Service.DISCARDED
    ON_HOLD = Service.ON_HOLD
    LOST = Service.LOST
    
    _por_activar = [PENDING, NOT_INSTALLED, DELINQUENT, ON_HOLD, DISCARDED]
    _por_canje = [NOT_INSTALLED,]
    _por_descartar = [ACTIVE, NOT_INSTALLED, DELINQUENT,]
    _por_rechazo_instalacion = [NOT_INSTALLED,]
    _por_moroso = [ACTIVE,]
    _por_retirar = [ACTIVE, INACTIVE, FREE, HOLDER_CHANGE, DISCARDED, ON_HOLD, LOST]
    _por_inactivo = [PENDING, DELINQUENT,]
    _por_cambio_de_titular = [ACTIVE, FREE]
    _por_no_instalado = [INSTALL_REJECTED]  
    _por_baja_temporal = [ACTIVE,]  
    _por_perdido = [PENDING,]
    _name_event = ''

    def __init__(self, service):
        self.service = service
        self.validate_from_model()

    def __str__(self):
        if self.service:
            return "StateMachine of %s" % str(self.service)
        return "StateMachine of Service"

    def get_status_display(self, status):
        return Service.STATUS_CHOICES[status-1][1]

    def error(self, old, new):
        raise ValidationError('No se puede cambiar el estado de "{}" a "{}"'.format(
            self.get_status_display(old),
            self.get_status_display(new))
        )

    def validate_from_model(self):
        if self.service._original_status == self.service.status:
            return True
        else:
            return self._validate_change(
                self.service._original_status,
                self.service.status)

    @classmethod
    def validate_from_external(cls, old, new):
        return self._validate_change(old, new)

    def _validate_change(self, old, new):
        
        if getattr(config, "allow_status_validation") == 'yes':
            if old in self._por_activar and new == self.ACTIVE:
                if old == self.DELINQUENT:
                    self._name_event = 'moroso_a_activo'
                elif old == self.PENDING and new == self.ACTIVE:
                    self._name_event = 'por_retirar_a_activo'
                elif old == self.NOT_INSTALLED and new == self.ACTIVE:
                    self._name_event = 'bienvenida_cliente'
                elif old == self.ON_HOLD and new == self.ACTIVE:
                    self._name_event = 'baja_temporal_a_activo'
                elif old == self.DISCARDED and new == self.ACTIVE:
                    self._name_event = 'descartad_a_activo'
                else:
                    self.error(old,new)
            
            if old in self._por_canje and new == self.FREE:
                if self.NOT_INSTALLED == old:
                    self._name_event = 'bienvenida_canje'
                else:
                    self.error(old, new)
            
            if old in self._por_descartar and new == self.DISCARDED:
                if self.ACTIVE == old:
                    self._name_event = 'activo_a_descartado'
                elif self.NOT_INSTALLED == old:
                    self._name_event = 'no_instalado_a_descartado'
                elif self.DELINQUENT == old:
                    self._name_event = 'moroso_a_descartado' 
                else:
                    self.error(old,new)

            if old in self._por_rechazo_instalacion and new == self.INSTALL_REJECTED:
                if self.NOT_INSTALLED == old:
                    self._name_event = 'no_instalado_a_instalacion_rechazada'
                else:
                    self.error(old,new)

            if old in self._por_moroso and new == self.DELINQUENT:
                if self.ACTIVE == old:
                    self._name_event = 'activo_a_moroso'
                else:
                    self.error(old, new)
            
            if old in self._por_retirar and new == self.PENDING:
                if old == self.ACTIVE:
                    self._name_event = 'activo_por_retirar'
                elif old == self.INACTIVE:
                    self._name_event = 'retirado_a_por_retirar'
                elif old == self.FREE:
                    self._name_event = 'canje_por_retirar'
                elif old == self.HOLDER_CHANGE:
                    self._name_event = 'cambio_de_titular_a_por_retirar'
                elif old == self.DISCARDED:
                    self._name_event = 'descartado_a_por_retirar'
                elif old == self.ON_HOLD:
                    self._name_event = 'baja_temporal_a_por_retirar'
                elif old == self.LOST:
                    self._name_event = 'perdido_a_por_retirar'
                else:
                    self.error(old, new)
            
            if old in self._por_inactivo and new == self.INACTIVE:
                if old == self.PENDING:
                    self._name_event = 'por_retira_a_inactivo'
                elif old == self.DELINQUENT:
                    self._name_event =  'moroso_a_inactivo'
                else:
                    self.error(old, new)

            if old in self._por_cambio_de_titular and new == self.HOLDER_CHANGE:
                if old == self.ACTIVE:
                    self._name_event = 'activo_a_cambio_de_titular'
                elif old == self.FREE:
                    self._name_event = 'canje_a_cambio_de_titular'
                else:
                    self.error(old, new)
            
            if old in self._por_no_instalado and new == self.NOT_INSTALLED:
                if old == self.INSTALL_REJECTED:
                    self._name_event = 'instalación_rechazada_a_no_instalado'
                else:
                    self.error(old,new)
            
            if old in self._por_baja_temporal and new == self.ON_HOLD:
                if self.ACTIVE == old:
                    self._name_event = 'activo_a_baja_temporal'
                else:
                    self.error(old, new)

            if old in self._por_perdido and new == self.LOST:
                if self.PENDING == old:
                    self._name_event = 'activo_a_por_retirar'
                else:
                    self.error(old, new)
        
            if self._name_event:
                return True
            else:
                self.service.status = self.service._original_status
                self.service.save()
                self.error(old, new)


def event_status_changed_to_iris(instance):
    valid_state_machine = ServiceStateMachine(instance)
    service = instance
    if valid_state_machine._name_event == 'bienvenida_cliente':
        context = {
                "date": timezone.now(),
                "object": service,
                "service": service,
                "customer": service.customer,
                "kind": service.customer.kind,
                "description": "Anexo de Contrato",
                "class_number": 2,
            }
        filename = "../media/contratos/service_annex.pdf"
        template = get_template("customers/service_contract.html")
        pdf = render_pdf_from_template(template, None, None, context=context)
        bb = io.BytesIO(pdf)
        ff = File(filename)
        ff.file = bb
        doc = Document.objects.create(
                content_object=context["service"],
                description=context["description"],
                agent=instance.agent,
            )
        doc.file.save(filename, ff)
        doc.save()
        pdf64 = base64.b64encode(pdf).decode("UTF-8")

        '''
        if getattr(config, "allow_generate_contract") == "yes":
            url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_url").format(NUMBER=int(service.number))
            token_sentinel = getattr(config, "sentinel_token")
            headers = {'Authorization':token_sentinel}
            node=None
            network_mismatch=""
            try:
                r = requests.get(url,headers=headers,verify=False, timeout=30).json()
                node=r["node"]
                network_mismatch=r["network_mismatch"]
            except Exception as e:
                print(str(e))

            if node is None:
                node={
                    'alias': '',
                    'street': '',
                    'house_number': '',
                    'commune':'',
                    'expected_power':'',
                    'towers': '',
                    'apartments':'',
                    'phone':'',
                }

                # To get the network info
            url = getattr(config, "sentinel_server")+ getattr(config, "sentinel_get_service_network_url").format(NUMBER=int(service.number))
            try:
                r = requests.get(url,headers=headers,verify=False, timeout=30)
                #print(r)
                network = r.json()
            except Exception as e:
                print(str(e))
                network={
                    "mac_eth1":"",
                    "kind_technology":""
                }
            info = []
            info.append(
            {
                "number": str(service.number),
                "street": str(service.street),
                "house_number": str(service.house_number),
                "apartment_number": str(service.apartment_number),
                "tower": str(service.tower),
                "location": str(service.location),
                "price_override": str(service.price_override),
                "due_day": str(service.due_day),
                "activated_on": str(service.activated_on) if service.activated_on else "",
                "installed_on": str(service.installed_on)  if service.installed_on else "",
                "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                "expired_on": str(service.expired_on)  if service.expired_on else "",
                "status": str(service.get_status_display()),
                "network_mismatch": "Sí" if network_mismatch else "No",
                "ssid": "", # str(service.ssid), #####
                "technology_kind":  str(network["kind_technology"]) if "kind_technology" in network else "", 
                "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                "seen_connected": str(service.seen_connected),
                "mac_onu": str(network["mac_eth1"]) if "mac_eth1" in network else "", #str(service.mac_onu),
                "composite_address": str(service.customer.composite_address),
                "commune": str(node["commune"]),
                "node__code": str(node["alias"]),
                "node__address": "",
                "node__street": "",
                "node__house_number": "",
                "node__commune": str(node["commune"]),
                "node__olt_config":"",
                "node__is_building": "",
                "node__is_optical_fiber": "",
                "node__gpon":  "",
                "node__gepon":  "",
                "node__pon_port":"",
                "node__box_location":"",
                "node__splitter_location":"",
                "node__expected_power": "",
                "node__apartments": "",
                "node__towers":  str(node["towers"]) ,
                "node__phone": str(node["phone"]),
                "plan__name": str(service.plan.description_facturacion),
                "plan__price": str(service.plan.price),
                "plan__category": str(service.plan.category),
                "plan__uf":  "Sí" if service.plan.uf else "No",
                "plan__active": "Sí" if service.plan.active else "No", 
                "customer__rut": str(service.customer.rut),
                "customer__name": str(service.customer.name),
                "customer__first_name": str(service.customer.first_name),
                "customer__last_name": str(service.customer.last_name), ###NUEVO###
                "customer__email": str(service.customer.email),
                "customer__street": str(service.customer.street),
                "customer__house_number": str(service.customer.house_number),
                "customer__apartment_number": str(
                    service.customer.apartment_number
                ),
                "customer__tower": str(service.customer.tower),
                "customer__location": str(service.customer.location),
                "customer__phone": str(service.customer.phone),
                "customer__default_due_day": str(
                    service.customer.default_due_day
                ),
                "customer__composite_address": str(
                    service.customer.composite_address
                ),
                "customer__commune": str(service.customer.commune),
                "files": [pdf64],
            })
            
            data = {"event": getattr(config, "evento_correo_contrato"), "info": info}
            url = getattr(config, "iris_server") + "api/v1/communications/trigger_event/active/"
            
            try:
                send = requests.post(
                    url=url,
                    json=data,
                    headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                    verify = False
                )
                if "Error" in send.json() or "error" in send.json():
                    print("ERROR")
            except Exception as e:
                print("e",e)
        '''
