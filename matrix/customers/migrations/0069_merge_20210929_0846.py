# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-09-29 12:46
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0068_historicalreceipt_bank_account'),
        ('customers', '0067_merge_20210927_1120'),
    ]

    operations = [
    ]
