# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2020-05-25 07:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0023_auto_20200505_0516'),
    ]

    operations = [
        migrations.AddField(
            model_name='charge',
            name='permanent',
            field=models.BooleanField(default=False, verbose_name='permanent'),
        ),
        migrations.AddField(
            model_name='credit',
            name='permanent',
            field=models.BooleanField(default=False, verbose_name='permanent'),
        ),
        migrations.AddField(
            model_name='historicalcharge',
            name='permanent',
            field=models.BooleanField(default=False, verbose_name='permanent'),
        ),
        migrations.AddField(
            model_name='historicalcredit',
            name='permanent',
            field=models.BooleanField(default=False, verbose_name='permanent'),
        ),
        migrations.AddField(
            model_name='historicalmainreason',
            name='uf',
            field=models.BooleanField(default=False, verbose_name='uf'),
        ),
        migrations.AddField(
            model_name='mainreason',
            name='uf',
            field=models.BooleanField(default=False, verbose_name='uf'),
        ),
        migrations.AddField(
            model_name='operatorinformation',
            name='description',
            field=models.CharField(blank=True, max_length=200, verbose_name='description'),
        ),
        migrations.AddField(
            model_name='operatorinformation',
            name='email',
            field=models.EmailField(blank=True, max_length=254),
        ),
        migrations.AddField(
            model_name='operatorinformation',
            name='logo',
            field=models.ImageField(blank=True, upload_to='operators', verbose_name='logo'),
        ),
        migrations.AddField(
            model_name='operatorinformation',
            name='phone',
            field=models.CharField(blank=True, max_length=75, verbose_name='phone'),
        ),
    ]
