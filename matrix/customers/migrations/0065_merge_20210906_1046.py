# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-09-06 14:46
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0063_merge_20210825_1112'),
        ('customers', '0064_auto_20210906_0954'),
    ]

    operations = [
    ]
