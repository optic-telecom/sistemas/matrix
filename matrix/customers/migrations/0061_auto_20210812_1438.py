# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-08-12 18:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0060_auto_20210623_0727'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartolamovement',
            name='number_document',
            field=models.BigIntegerField(),
        ),
        migrations.AlterField(
            model_name='historicalcartolamovement',
            name='number_document',
            field=models.BigIntegerField(),
        ),
        migrations.AlterField(
            model_name='historicalpayment',
            name='number_document',
            field=models.BigIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='payment',
            name='number_document',
            field=models.BigIntegerField(blank=True, null=True),
        ),
    ]
