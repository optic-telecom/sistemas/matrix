# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2020-04-24 03:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0019_auto_20200422_0650'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='processedbatch',
            name='stock',
        ),
        migrations.AddField(
            model_name='invoicebatch',
            name='stock',
            field=models.PositiveIntegerField(default=0, verbose_name='stock'),
        ),
        migrations.AddField(
            model_name='processedbatch',
            name='status',
            field=models.BooleanField(default=False, verbose_name='status'),
        ),
        migrations.AlterField(
            model_name='invoicebatch',
            name='created_on',
            field=models.DateTimeField(blank=True, null=True, verbose_name='created on'),
        ),
        migrations.AlterField(
            model_name='invoicebatch',
            name='expiration',
            field=models.DateTimeField(blank=True, null=True, verbose_name='expiration'),
        ),
        migrations.AlterField(
            model_name='processedbatch',
            name='processed_on',
            field=models.DateTimeField(blank=True, null=True, verbose_name='created on'),
        ),
        migrations.AlterField(
            model_name='taxationresponse',
            name='date',
            field=models.DateTimeField(verbose_name='date'),
        ),
    ]
