# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-06-23 11:27
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0059_auto_20210603_1025'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartola',
            name='excel',
            field=models.FileField(blank=True, null=True, upload_to='cartolas', verbose_name='excel'),
        ),
        migrations.AddField(
            model_name='cartola',
            name='pdf',
            field=models.FileField(blank=True, null=True, upload_to='cartolas', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['pdf'])], verbose_name='pdf'),
        ),
        migrations.AddField(
            model_name='historicalcartola',
            name='excel',
            field=models.TextField(blank=True, max_length=100, null=True, verbose_name='excel'),
        ),
        migrations.AddField(
            model_name='historicalcartola',
            name='pdf',
            field=models.TextField(blank=True, max_length=100, null=True, validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['pdf'])], verbose_name='pdf'),
        ),
        migrations.AddField(
            model_name='historicalpayment',
            name='number_document',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='payment',
            name='number_document',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
