# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-03-05 13:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0053_auto_20210216_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalservice',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Activo'), (2, 'Por Retirar'), (3, 'Retirado'), (4, 'No Instalado'), (5, 'Canje'), (6, 'Instalación Rechazada'), (7, 'Moroso'), (8, 'Cambio de Titular'), (9, 'Descartado'), (10, 'Baja Temporal'), (11, 'Perdido, no se pudo retirar')], default=4, verbose_name='status'),
        ),
        migrations.AlterField(
            model_name='service',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Activo'), (2, 'Por Retirar'), (3, 'Retirado'), (4, 'No Instalado'), (5, 'Canje'), (6, 'Instalación Rechazada'), (7, 'Moroso'), (8, 'Cambio de Titular'), (9, 'Descartado'), (10, 'Baja Temporal'), (11, 'Perdido, no se pudo retirar')], default=4, verbose_name='status'),
        ),
    ]
