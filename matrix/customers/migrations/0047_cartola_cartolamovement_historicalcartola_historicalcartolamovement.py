# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2020-12-29 18:28
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customers', '0046_auto_20200929_1325'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cartola',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('number', models.PositiveIntegerField()),
                ('start', models.DateField()),
                ('end', models.DateField()),
                ('saldo_final', models.PositiveIntegerField()),
                ('saldo_inicial', models.PositiveIntegerField()),
                ('bank_account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='customers.BankAccount', verbose_name='bank account')),
            ],
            options={
                'verbose_name': 'Cartola',
                'ordering': ['-created_at'],
                'permissions': (('list_cartolas', 'Can list cartolas'),),
            },
        ),
        migrations.CreateModel(
            name='CartolaMovement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('date', models.DateField()),
                ('sucursal', models.CharField(max_length=255, verbose_name='Sucursal')),
                ('number_document', models.PositiveIntegerField()),
                ('description', models.CharField(max_length=255, verbose_name='Description')),
                ('total', models.PositiveIntegerField()),
                ('kind', models.PositiveSmallIntegerField(choices=[(1, 'Cheques y otros cargos'), (2, 'Depositos y abonos')], default=1, verbose_name='kind')),
                ('saldo_diario', models.PositiveIntegerField()),
                ('payment', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='customers.Payment')),
            ],
            options={
                'verbose_name': 'Movimiento Cartola',
                'permissions': (('list_movimiento_cartola', 'Can list movimineto cartola'),),
            },
        ),
        migrations.CreateModel(
            name='HistoricalCartola',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, editable=False, verbose_name='created at')),
                ('number', models.PositiveIntegerField()),
                ('start', models.DateField()),
                ('end', models.DateField()),
                ('saldo_final', models.PositiveIntegerField()),
                ('saldo_inicial', models.PositiveIntegerField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('bank_account', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='customers.BankAccount')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical Cartola',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalCartolaMovement',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, editable=False, verbose_name='created at')),
                ('date', models.DateField()),
                ('sucursal', models.CharField(max_length=255, verbose_name='Sucursal')),
                ('number_document', models.PositiveIntegerField()),
                ('description', models.CharField(max_length=255, verbose_name='Description')),
                ('total', models.PositiveIntegerField()),
                ('kind', models.PositiveSmallIntegerField(choices=[(1, 'Cheques y otros cargos'), (2, 'Depositos y abonos')], default=1, verbose_name='kind')),
                ('saldo_diario', models.PositiveIntegerField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('payment', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='customers.Payment')),
            ],
            options={
                'verbose_name': 'historical Movimiento Cartola',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
    ]
