# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2020-08-10 14:23
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0040_auto_20200807_1434'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='qachannel',
            options={'ordering': ['-pk'], 'permissions': (('view_qachannel', 'Can view QA channel'), ('list_qachannel', 'Can list QA channel'))},
        ),
        migrations.AlterModelOptions(
            name='qaindicator',
            options={'ordering': ['-pk'], 'permissions': (('view_qaindicator', 'Can view calls QA indicator'), ('list_qaindicator', 'Can list calls QA indicator'))},
        ),
        migrations.AlterModelOptions(
            name='qamonitoring',
            options={'ordering': ['-pk'], 'permissions': (('view_qamonitoring', 'Can view calls QA monitoring'), ('list_qamonitoring', 'Can list calls QA monitoring'))},
        ),
        migrations.AlterModelOptions(
            name='qatracing',
            options={'ordering': ['-pk'], 'permissions': (('view_qatracing', 'Can view calls QA tracing'), ('list_qatracing', 'Can list calls QA tracing'))},
        ),
    ]
