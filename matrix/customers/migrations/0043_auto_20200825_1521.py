# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2020-08-25 19:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0042_auto_20200818_1210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='callqa',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='Correo'),
        ),
        migrations.AlterField(
            model_name='historicalcallqa',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='Correo'),
        ),
    ]
