# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-07-14 13:40
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class CompanyObjectHolder:
    @classmethod
    def get_object_id(cls):
        #return cls.company_object.id
        return 1

    @classmethod
    def create(cls, apps, schema_editor):

        # We get the model from the versioned app registry;
        # if we directly import it, it'll be the wrong version
        Company = apps.get_model('customers', 'Company')
        db_alias = schema_editor.connection.alias
        cls.company_object = Company.objects.create(name='Multifiber')
        print(f'created city with id: {cls.company_object.id}')

class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customers', '0060_auto_20210623_0727'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'company',
                'verbose_name_plural': 'companies',
                'ordering': ['-pk'],
            },
        ),
        migrations.CreateModel(
            name='HistoricalCompany',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, editable=False, verbose_name='created at')),
                ('name', models.CharField(max_length=100)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical company',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.RunPython(CompanyObjectHolder.create),
        migrations.CreateModel(
            name='HistoricalUserCompany',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, editable=False, verbose_name='created at')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical User Company',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalUserOperator',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, editable=False, verbose_name='created at')),
                ('operator', models.PositiveSmallIntegerField(default=0, verbose_name='operator')),
                ('company', models.PositiveSmallIntegerField(default=0, verbose_name='company')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical User Operator',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='UserCompany',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('company', models.ManyToManyField(blank=True, to='customers.Company')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'verbose_name': 'User Company',
            },
        ),
        migrations.CreateModel(
            name='UserOperator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('operator', models.PositiveSmallIntegerField(default=0, verbose_name='operator')),
                ('company', models.PositiveSmallIntegerField(default=0, verbose_name='company')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'verbose_name': 'User Operator',
            },
        ),
        migrations.AddField(
            model_name='billingcycle',
            name='operator',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='customers.Operator', verbose_name='operator'),
        ),
        migrations.AddField(
            model_name='historicalemployee',
            name='operator',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='customers.Operator'),
        ),
        migrations.AddField(
            model_name='historicalmainreason',
            name='operator',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='customers.Operator'),
        ),
        migrations.AddField(
            model_name='historicalpromotions',
            name='operator',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='customers.Operator'),
        ),
        migrations.AddField(
            model_name='invoicebatch',
            name='operator',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='customers.Operator', verbose_name='operator'),
        ),
        migrations.AddField(
            model_name='mainreason',
            name='operator',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='customers.Operator', verbose_name='operator'),
        ),
        migrations.AddField(
            model_name='promotions',
            name='operator',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='customers.Operator', verbose_name='operator'),
        ),
        migrations.AddField(
            model_name='customer',
            name='company',
            field=models.ForeignKey(default=CompanyObjectHolder.get_object_id, on_delete=django.db.models.deletion.CASCADE, to='customers.Company', verbose_name='company'),
        ),
        migrations.AddField(
            model_name='historicalcustomer',
            name='company',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='customers.Company'),
        ),
        migrations.AddField(
            model_name='historicalindicators',
            name='company',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='customers.Company'),
        ),
        migrations.AddField(
            model_name='indicators',
            name='company',
            field=models.ForeignKey(default=CompanyObjectHolder.get_object_id, on_delete=django.db.models.deletion.CASCADE, to='customers.Company', verbose_name='company'),
        ),
        migrations.AddField(
            model_name='operator',
            name='company',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='customers.Company'),
        ),
    ]
