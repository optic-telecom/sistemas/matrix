# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2020-06-09 13:25
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customers', '0027_auto_20200604_0636'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalIndicators',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, editable=False, verbose_name='created at')),
                ('kind', models.PositiveSmallIntegerField(choices=[(1, 'Unidad de fomento (UF)'), (2, 'Impuesto sobre el Valor Agregado (IVA)'), (3, 'Dólar observado'), (4, 'Dólar acuerdo'), (5, 'Euro'), (6, 'Unidad Tributaria Mensual (UTM)'), (7, 'Tasa Política Monetaria (TPM)'), (8, 'Bitcoin')], default=1, verbose_name='kind')),
                ('value', models.DecimalField(decimal_places=2, max_digits=20, verbose_name='value')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical Indicators',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='Indicators',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('kind', models.PositiveSmallIntegerField(choices=[(1, 'Unidad de fomento (UF)'), (2, 'Impuesto sobre el Valor Agregado (IVA)'), (3, 'Dólar observado'), (4, 'Dólar acuerdo'), (5, 'Euro'), (6, 'Unidad Tributaria Mensual (UTM)'), (7, 'Tasa Política Monetaria (TPM)'), (8, 'Bitcoin')], default=1, verbose_name='kind')),
                ('value', models.DecimalField(decimal_places=2, max_digits=20, verbose_name='value')),
            ],
            options={
                'verbose_name': 'Indicators',
                'ordering': ['kind', '-created_at'],
            },
        ),
    ]
