# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-02-03 13:46
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0051_auto_20210114_0851'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='flag',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True, verbose_name='Flag'),
        ),
        migrations.AddField(
            model_name='customer',
            name='gender',
            field=models.PositiveIntegerField(choices=[(1, 'female'), (2, 'male'), (3, 'other')], null=True, verbose_name='gender'),
        ),
        migrations.AddField(
            model_name='historicalcustomer',
            name='flag',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True, verbose_name='Flag'),
        ),
        migrations.AddField(
            model_name='historicalcustomer',
            name='gender',
            field=models.PositiveIntegerField(choices=[(1, 'female'), (2, 'male'), (3, 'other')], null=True, verbose_name='gender'),
        ),
        migrations.AddField(
            model_name='historicalservice',
            name='flag',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True, verbose_name='Flag'),
        ),
        migrations.AddField(
            model_name='service',
            name='flag',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True, verbose_name='Flag'),
        ),
    ]
