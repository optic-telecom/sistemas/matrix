import requests
from django.core.cache import cache
from constance import config


def request_address_pulso(id, timeout=30):
    # r = requests.get(f'http://127.0.0.1:9001/api/v1/complete-location/{id}/' )
    #TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    headers = {'content-type': 'application/json','Authorization': getattr(config, "pulso_token")}
    r = requests.get(getattr(config, "pulso_server") +'api/v1/complete-location/'+str(id)+'/', headers=headers, timeout=timeout, verify=False)
    status_code = r.status_code
    return status_code, r


def get_complete_address(id):
    text_cache = str("pulso_address")+str(id)
    _cache = cache.get(text_cache)
    if _cache:
        return _cache
    else:
        status_code, r = request_address_pulso(id)
        
        if status_code == 200:
            address = r.json()
            res = { 
                'ok': True,
                'data': {
                    'id': address['id'],
                    'number': address['number'],
                    'country': address['country'],
                    'country_id': address['country_id'],
                    'region': address['region'],
                    'region_id': address['region_id'],
                    'commune': address['commune'],
                    'commune_id': address['commune_id'],
                    'street': address['street'],
                    'street_id': address['street_id'],
                    'tower': address['tower'],
                    'floor': address['floor_num'],
                    'department': address['departament'],
                    'street_location': address['street_location']
                    
                }
                
            }
            cache.set(text_cache, res, 3600*1)
            return res

        else :
            res= {
                'ok': False, 
                'detail': f'error {status_code}', 
                'data': {
                    'id': '-',
                    'number': '-',
                    'country': '-',
                    'country_id': '-',
                    'region': '-',
                    'region_id': '-',
                    'commune': '-',
                    'commune_id': '-',
                    'street': '-',
                    'street_id': '-',
                    'tower': '-',
                    'floor': '-',
                    'department': '-',
                    'street_location': '-'
                }
            }
            cache.set(text_cache, res, 3600*1)
            return res

def get_street_location(id):
    text_cache = str("pulso_address_street")+str(id)
    _cache = cache.get(text_cache)
    if _cache:
        return _cache
    else:
        #TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
        headers = {'content-type': 'application/json','Authorization': getattr(config, "pulso_token")}
        r = requests.get(getattr(config, "pulso_server") +'api/v1/streetlocation/'+str(id)+'/', headers=headers, timeout=30, verify=False)
        status_code = r.status_code
        #print(r)
        if status_code == 200:
            address = r.json()
            #print(address)
            res = { 
                'ok': True,
                'data': r.json()
            }
            cache.set(text_cache, res, 3600*1)
            return res

        else :
            res= {
                'ok': False, 
                'detail': f'error {status_code}', 
                'data': {"id":'-',"street_no":'-',
                "street":'-',"edification_type":'-',
                "homepassed":'-',"gps_latitude":'-',"gtd_longitude":'-'}

            }
            cache.set(text_cache, res, 3600*1)
            return res