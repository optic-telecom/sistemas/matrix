
$(document).ready(function(){

var e=$("#employee-list").DataTable({
    order:[],
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
	url: "/hr/employees/ajax/",
	data: function (d) {
	    d.status = $('#id_status').val();
	}},
    language: {
    "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
	"buttons": {
	    "print": "Imprimir"
	}
    },
    
    pageLength:10,
    //colReorder:!0,
    buttons:["excel","pdf","print"],
    searching: true,
    aLengthMenu:[[10,20,50],[10,20,50]],
    //order:[[0,"desc"]],
    columns: [
	{data: "rut"},
	{data: "name"},
	{data: "address"},
	{data: "commune"},
	{data: "service_date"},
	{data: "health_care"},
	{data: "afp"},
	{data: "company"},
    ],
    columnDefs:[
	{
	    orderable: true,
	    targets:[0,1,2,3,4]
	},
	{
	    orderable: false,
	    targets:[5,6,7]
	},
	{
	    render: function(data, type, row) {
		return '<a href="/hr/employees/'+row.DT_RowId+'/">'+data+'</a>';
	    },
	    targets: 0,
	}

    ],
});

$('#id_status').change(function(){
    e.ajax.reload();
})


}
);

