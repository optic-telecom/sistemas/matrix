
$(document).ready(function(){

    var e=$("#call-qa-list").DataTable({
        order:[],
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: "/customers/register-support-call-qa/ajax/",
        data: function (d) {
            d.agent = $('#id_agent').val();
        }},
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        //order:[[0,"desc"]],
        columns: [
        {data: "DT_RowId"},
        {data: "agent"},
        {data: "customer_phone"},
        {data: "email"},
        {data: "rut"},
        {data: "service"},
        {data: "date"},
        {data: "created_by"},
        {data: "created_at"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2,3,4,7]
        },
        {
            orderable: false,
            targets:[5,6,8]
        },
        {
            render: function(data, type, row) {
                return '<a href="/customers/register-support-call-qa/'+row.DT_RowId+'/">'+data+'</a>';
            },
            targets: 0,
        },
        {
            render: function(data, type, row) {
                length = data.length
                if (length == 0) {
                    return 'Sin contrato';
                } else if (length == 1){
                    return '<a href="/services/'+data+'">'+data+'</a>';
                }else{
                    return 'Varios'
                }
            },
            targets: 5,
        },
        {
            render: function(data, type, row) {
                return '<div class="justify-content-center"><button class="btn btn-danger" data-name="action" data-id="'+row.DT_RowId+'">Eliminar</button></div>'
            },
            targets: 9,
        }
        ],
    });
    
    
    $('#id_agent').change(function(){
        e.ajax.reload();
    });

    $('#call-qa-list tbody').on('click', 'button[data-name="action"]', function(){
        var id = $(this).data('id');
        if (confirm("¿Esta seguro de eliminar el registro?")) {            
            $.ajax({
                url: "/customers/register-support-call-qa/"+id+"/delete/",
                type: 'GET',
                data: {},
                dataType: 'json',
                success: function response(json) {
                  if (json.valid) {
                    swal("ÉXITO", "Registro eliminado satisfactoriamente", "success");
                    e.ajax.reload();
                  } else {
                    swal("ALERTA", "Error al eliminar el registro", "warning");
                  }
                },
            }); 
        }
        
    });

    
    }
    );
    
    
