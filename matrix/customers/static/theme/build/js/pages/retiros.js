
$(document).ready(function(){
var e=$("#porretirar-list").DataTable({
        order:[],
        processing: true,
        serverSide: true,
        //searchDelay: 400,
        ajax: {
        url: "/customers/retiros-table-ajax/",
        data: function (d) {
            d.year = $('#id_year').val();
            d.mes = $('#id_mes').val();
            d.kind = $('#id_kind').val();
        }},
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: false,
        aLengthMenu:[[10,20,50],[10,20,50]],
        //order:[[0,"desc"]],
        columns: [
        {data: "service"},
        {data: "customer"},
        {data: "plan"},
        {data: "address"},
        {data: "comuna"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[]
        },
        {
            orderable: false,
            targets:[0,1,2,3,4]
        },
        {
            render: function(data, type, row) {
                return 'Servicio #'+row.service;

            },
            targets: 0,
        },
        {
            render: function(data, type, row) {
                return row.customer;
            },
            targets: 1,
        },
        {
            render: function(data, type, row) {
            return row.plan;
            },
            targets: 2,
        },
        {
            render: function(data, type, row) {
            return row.address;
            },
            targets: 3,
        },
        {
            render: function(data, type, row) {
            return row.comuna;
            },
            targets: 4,
        },
        ],
    });
    
    $('#id_year').change(function(){
        e.ajax.reload();
    })
    $('#id_mes').change(function(){
        e.ajax.reload();
    })
    $('#id_kind').change(function(){
        e.ajax.reload();
    })
    }
    );

    $('#export-button').click(function() {
        var kind = $('#id_kind').val();
        var year = $('#id_year').val();
        var mes = $('#id_mes').val();
        var url_open = "/reports/retiros_excel/?kind="+kind+"&mes="+mes+"&year="+year;
        window.open(url_open, "_blank");
    });