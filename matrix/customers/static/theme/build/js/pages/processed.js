$(document).ready(function () {
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');
  var e = $("#processed-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax/",
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
    },
    searching: false,
    ordering: false,
    bPaginate: true,
    /* paging: false,
    info: false, */
    columns: [
      { data: "id" },
      { data: "date" },
      { data: "processed" },
      { data: "type" },
      { data: "status" },
    ],
    columnDefs: [
      {
        orderable: false,
        targets: [0, 1, 2, 3]
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.id + '</div>';
        },
        targets: 0,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.date + '</div>';
        },
        targets: 1,
      },
      {
        render: function (data, type, row) {
          if (row.type == "No procesado") {
            detail = '<p style="text-align:center" title="No se procesaron (' + row.processed.notProcessed.length + '): ';
            for (let i = 0; i < row.processed.notProcessed.length; i++) {
              detail += row.processed.notProcessed[i] + " ";
            }
            detail += '">' + row.processed.message + '<br><i class="fa fa-info-circle"></i></p>';
            return detail
          } else {
            var waitingForResult = [];
            var resultError = [];
            var resultSuccess = [];
            var resultNulled = [];
            var processed = Object.values(row.processed);
            for (let i = 0; i < processed.length; i++) {
              if (row.status) {
                /* status true, then result  */
                if (processed[i]["error"]) {
                  /* Error */
                  if (processed[i]["unified"]) {
                    var service = processed[i]["unified"]["services"];
                    var folio = Object.values(processed[i]["folio"])[0];
                    var error = processed[i]["error"];
                    var unifiedError = true;
                    resultError.push([service, folio, error]);
                  } else {
                    var service = processed[i]["service"];
                    var folio = Object.values(processed[i]["folio"])[0];
                    var error = processed[i]["error"];
                    resultError.push([service, folio, error]);
                  }
                  /* Nulled */
                } else if (processed[i]["nulled"]) {
                  if (processed[i]["unified"]) {
                    var service = processed[i]["unified"]["services"];
                    var folio = Object.values(processed[i]["folio"])[0];
                    var unifiedNulled = true;
                    resultNulled.push([service, folio]);
                  } else {
                    var service = processed[i]["service"];
                    var folio = Object.values(processed[i]["folio"])[0];
                    resultNulled.push([service, folio]);
                  }
                } else {
                  /* Success */
                  if (processed[i]["unified"]) {
                    var service = processed[i]["unified"]["services"];
                    var folio = Object.values(processed[i]["folio"])[0];
                    var unifiedSuccess = true;
                    resultSuccess.push([service, folio]);
                  } else {
                    var service = processed[i]["service"];
                    var folio = Object.values(processed[i]["folio"])[0];
                    resultSuccess.push([service, folio]);
                  }
                }
              } else {
                /* waiting for result */
                if (processed[i]["unified"]) {
                  var service = processed[i]["unified"]["services"];
                  var folio = Object.values(processed[i]["folio"])[0];
                  var unifiedWait = true;
                  waitingForResult.push([service, folio]);
                } else {
                  var service = processed[i]["service"];
                  var folio = Object.values(processed[i]["folio"])[0];
                  waitingForResult.push([service, folio]);
                }
              }
            }
            /* Draw HTML */
            if (waitingForResult.length > 0) {
              if (unifiedWait) {
                waitingH = '<table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">#</th> <th scope="col">Servicio</th> <th scope="col">Folio</th></tr> </thead> <tbody>'
                for (let i = 0; i < waitingForResult.length; i++) {
                  b = '<tr class="table-active"> <th scope="row">' + (i + 1) + '</th> <td>';
                  for (let ind = 0; ind < waitingForResult[i][0].length; ind++) {
                    b += '<a title="Ver detalles del servicio" target="_blank" href="/services/' + waitingForResult[i][0][ind] + '">' + waitingForResult[i][0][ind]["number"] + '</a> ';
                  }
                  b += '</td> <td>' + waitingForResult[i][1] + '</td></tr>';
                  waitingH = waitingH + b;
                }
                waitingH = waitingH + '</tbody> </table>';
              } else {
                waitingH = '<table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">#</th> <th scope="col">Servicio</th> <th scope="col">Folio</th> </tr> </thead> <tbody>'
                for (let i = 0; i < waitingForResult.length; i++) {
                  b = '<tr class="table-active"> <th scope="row">' + (i + 1) + '</th> <td><a title="Ver detalles del servicio" target="_blank" href="/services/' + waitingForResult[i][0] + '">' + waitingForResult[i][0]["number"] + '</a></td> <td>' + waitingForResult[i][1] + '</td> </tr>';
                  waitingH = waitingH + b;
                }
                waitingH = waitingH + '</tbody> </table>';
              }
            } else {
              waitingH = '<p style="text-align:center"><stong><h3>No hay datos para mostrar.<h3><strong></p>'
            }
            if (resultSuccess.length > 0) {
              if (unifiedSuccess) {
                successH = '<table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">#</th> <th scope="col">Servicio</th> <th scope="col">Folio</th></tr> </thead> <tbody>'
                for (let i = 0; i < resultSuccess.length; i++) {
                  b = '<tr class="table-active"> <th scope="row">' + (i + 1) + '</th> <td>';
                  for (let ind = 0; ind < resultSuccess[i][0].length; ind++) {
                    b += '<a title="Ver detalles del servicio" target="_blank" href="/services/' + resultSuccess[i][0][ind]["id"]  + '">' + resultSuccess[i][0][ind]["number"] + '</a> ';
                  }
                  b += '</td> <td>' + resultSuccess[i][1] + '</td></tr>';
                  successH = successH + b;
                }
                successH = successH + '</tbody> </table>';
              } else {
                successH = '<table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">#</th> <th scope="col">Servicio</th> <th scope="col">Folio</th> <th scope="col">PDF</th> </tr> </thead> <tbody>'
                for (let i = 0; i < resultSuccess.length; i++) {
                  b = '<tr class="table-active"> <th scope="row">' + (i + 1) + '</th> <td><a title="Ver detalles del servicio" target="_blank" href="/services/' + resultSuccess[i][0]["id"]  + '">' + resultSuccess[i][0]["number"] + '</a></td> <td>' + resultSuccess[i][1] + '</td><td> <a title="Previsualizar documento" target="_blank" href="/pre-bill/' + row.id + '"><button class="btn btn-link"><h3><i class="fa fa-file-pdf"></i></h3></button></a> <a title="Descargar documento" target="_blank" href="/unified-billpdf/' + row.id + '"><button class="btn btn-link"><h3><i class="fa fa-download"></i></h3></button></a> </td> </tr>';
                  successH = successH + b;
                }
                successH = successH + '</tbody> </table>';
              }
            } else {
              successH = '<p style="text-align:center"><stong><h3>No hay datos para mostrar.<h3><strong></p>';
            }
            /* NULLED START */
            if (resultNulled.length > 0) {
              nulledH = "hello"
              if (unifiedNulled) {
                nulledH = '<table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">#</th> <th scope="col">Servicio</th> <th scope="col">Folio</th></tr> </thead> <tbody>'
                for (let i = 0; i < resultNulled.length; i++) {
                  b = '<tr class="table-active"> <th scope="row">' + (i + 1) + '</th> <td>';
                  for (let ind = 0; ind < resultNulled[i][0].length; ind++) {
                    b += '<a title="Ver detalles del servicio" target="_blank" href="/services/' + resultNulled[i][0][ind]["id"] + '">' + resultNulled[i][0][ind]["number"] + '</a> ';
                  }
                  b += '</td> <td>' + resultNulled[i][1] + '</td></tr>';
                  nulledH = nulledH + b;
                }
                nulledH = nulledH + '</tbody> </table>';
              } else {
                nulledH = '<table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">#</th> <th scope="col">Servicio</th> <th scope="col">Folio</th></tr> </thead> <tbody>'
                for (let i = 0; i < resultNulled.length; i++) {
                  b = '<tr class="table-active"> <th scope="row">' + (i + 1) + '</th> <td><a title="Ver detalles del servicio" target="_blank" href="/services/' + resultNulled[i][0]["id"] + '">' + resultNulled[i][0]["number"] + '</a></td> <td>' + resultNulled[i][1] + '</td></tr>';
                  nulledH = nulledH + b;
                }
                nulledH = nulledH + '</tbody> </table>';
              }
            } else {
              nulledH = '<p style="text-align:center"><stong><h3>No hay datos para mostrar.<h3><strong></p>';
            }
            /* NULLED END */
            if (resultError.length > 0) {
              if (unifiedError) {
                errorH = '<table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">#</th> <th scope="col">Servicio</th> <th scope="col">Folio</th> <th scope="col">Error</th> </tr> </thead> <tbody>'
                for (let i = 0; i < resultError.length; i++) {
                  b = '<tr class="table-active"> <th scope="row">' + (i + 1) + '</th> <td>';
                  for (let ind = 0; ind < resultError[i][0].length; ind++) {
                    b += '<a title="Ver detalles del servicio" target="_blank" href="/services/' + resultError[i][0][ind]["id"]  + '">' + resultError[i][0][ind]["number"] + '</a> ';
                  }
                  b += '</td> <td>' + resultError[i][1] + '</td><td>' + resultError[i][2] + '</td> </tr>';
                  errorH = errorH + b;
                }
                errorH = errorH + '</tbody> </table>';
              } else {
                errorH = '<table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">#</th> <th scope="col">Servicio</th> <th scope="col">Folio</th> <th scope="col">Error</th> </tr> </thead> <tbody>'
                for (let i = 0; i < resultError.length; i++) {
                  b = '<tr class="table-active"> <th scope="row">' + (i + 1) + '</th> <td><a title="Ver detalles del servicio" target="_blank" href="/services/' + resultError[i][0]["id"]  + '">' + resultError[i][0]["number"]  + '</a></td> <td>' + resultError[i][1] + '</td><td>' + resultError[i][2] + '</td> </tr>';
                  errorH = errorH + b;
                }
                errorH = errorH + '</tbody> </table>';
              }
            } else {
              errorH = '<p style="text-align:center"><stong><h3>No hay datos para mostrar.<h3><strong></p>';
            }
            return '<div style="text-align:center"><button onclick="showModal(' + row.id + ')" type="button" class="btn btn-primary"> <i class="fa fa-eye"></i> Ver</button> <div id="shmod' + row.id + '" class="modal"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <h5 class="modal-title">Documentos del lote ' + row.id + '</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> </div><div class="modal-body"> <ul class="nav nav-tabs"> <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#waiting' + row.id + '">En proceso (' + waitingForResult.length + ')</a> </li><li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#success' + row.id + '">Correctos (' + resultSuccess.length + ')</a> </li><li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#err' + row.id + '">Con error (' + resultError.length + ')</a> </li><li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#nulled' + row.id + '">Anulados (' + resultNulled.length + ')</a> </li></ul> <div id="myTabContent" class="tab-content"> <div class="tab-pane fade show active" id="waiting' + row.id + '"> ' + waitingH + ' </div><div class="tab-pane fade" id="success' + row.id + '"> ' + successH + ' </div><div class="tab-pane fade" id="err' + row.id + '"> ' + errorH + ' </div><div class="tab-pane fade" id="nulled' + row.id + '"> ' + nulledH + ' </div></div></div></div></div></div></div>';
          }
        },
        targets: 2,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.type + '</div>';
        },
        targets: 3,
      },
      {
        render: function (data, type, row) {
          if (row.status) {
            return '<div style="text-align:center">Procesada</div>';
          } else {
            return '<div style="text-align:center">En proceso</div>';
          }
        },
        targets: 4,
      },
    ],
  });
});

