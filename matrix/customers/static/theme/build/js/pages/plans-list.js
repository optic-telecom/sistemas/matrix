
$(document).ready(function(){
    var e=$("#plans-list").DataTable({
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: "/plans/ajax2/",
        data: function (d) {
            d.operator = $('#id_operator').val();
            d.category = $('#id_category').val();
            d.uf = $('#uf').val();
        }},
        language: {
        "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        order:[[0,"desc"]],
        columns: [
            {data: "id"},
            {data: "name"},
            {data: "description_facturacion"},
            {data: "price"},
            {data: "currency"},
            {data: "uf"},
            {data: "category"},
            {data: "active_services"},
            {data: "total_services"},
            {data: "operator"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2,3,4,5,6,7,8]
        },
        {
            render: function(data, type, row) {
                return '<a href="/plans/'+row.id+'/edit/">'+row.id+'</a>';
            },
            targets: 0,
        },
        {
            render: function(data, type, row) {
                return row.symbol+row.price
            },
            targets: 3,
        },
        {
            render: function(data, type, row) {
                return row.currency
            },
            targets: 4,
        },
        {
            render: function(data, type, row) {
                return row.uf ? 'Sí':'No'
            },
            targets: 5,
        },
        ],
    });
    
    $('#id_operator').change(function(){
        e.ajax.reload();
    })
    $('#id_category').change(function(){
        e.ajax.reload();
    })
    $('#uf').change(function(){
        e.ajax.reload();
    })
    }
    );