$(document).ready(function(){

	var e=$("#customer-list").DataTable({
	    order:[],
	    processing: true,
	    serverSide: true,
	    searchDelay: 400,
	    ajax: {
		url: "/customers/ajax/",
		data: function (d) {
		    d.network_status = $('#id_network_status').val();
		    d.balance_from = $('#balance_from').val();
		    d.balance_to = $('#balance_to').val();
		    d.service_status = $('#id_service_status').val();
			d.plan= $('#id_plan').val();
			d.operator = $('#id_operator').val();
			d.id_type_client = $('#id_type_client').val();
		}},
	    language: {
	    "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    },
		"buttons": {
		    "print": "Imprimir"
		}
	    },
	    
	    pageLength:10,
	    //colReorder:!0,
	    buttons:["excel","pdf","print"],
	    searching: true,
	    aLengthMenu:[[10,20,50,100],[10,20,50,100]],
	    //order:[[0,"desc"]],
	    columns: [
		{data: "DT_RowId"},
		{data: "rut"},
		{data: "name"},
		{data: "email"},
		{data: "address"},
		{data: "commune"},
		{data: "status"},
		{data: "services"},
		{data: "balance"},
		{data: "operator"},
	    ],
	    columnDefs:[
		{
		    orderable: true,
		    targets:[0,1,2,3,4,8]
		},
		{
		    orderable: false,
		    targets:[5,6,7,9]
		},
		{
		    render: function(data, type, row) {
			return '<span class="label label-'+row.status_class+'">'+data+'</span>';
		    },
		    targets: 6,
		},
		{
		    render: function(data, type, row) {
			var value = "";
			if (data.length == 0) {
			    value = 'Ninguno';
			} else if (data.length == 1) {
			$.each(data, function (index, service_id) {
			    var link = '<a href="/services/'+service_id+'/">'+row.service_number+'</a>';
			    if (index == 0) {
				value += link;
			    } else {
				value += ', '+link;
			    }
			});
			} else {
			    var atitle = "";
			$.each(data, function (index, service_id) {
			    var ntext = service_id;
			    if (index == 0) {
				atitle += ntext;
			    } else {
				atitle += ', '+ntext;
			    }
			});
			    value = '<a href="/customers/'+row.DT_RowId+'/services/" title="'+atitle+'">Varios</a>';
			}
			return value;
		    },
		    targets: 7,
		},
		{
		    render: function(data, type, row) {
			return '<a href="/customers/'+row.DT_RowId+'/billing/">'+"$" +data+'</a>';
		    },
		    targets: 8,
		},
		{
		    render: function(data, type, row) {
			return '<a href="/customers/'+row.DT_RowId+'/">'+data+'</a>';
		    },
		    targets: 1,
		}

	    ],
	});

	$('#id_network_status').change(function(){
	    e.ajax.reload();
	})

	$('#balance_from').blur(function(){
	    e.ajax.reload();
	})

	$('#balance_to').blur(function(){
	    e.ajax.reload();
	})

	$('#id_service_status').change(function(){
	    e.ajax.reload();
	})

	$('#id_plan').change(function(){
	    e.ajax.reload();
	})
	$('#id_operator').change(function(){
	    e.ajax.reload();
	})
	$('#id_type_client').change(function(){
	    e.ajax.reload();
	})

	$('#export-button').click(function() {
	    var network_status = $('#id_network_status').val();
	    var balance_from = $('#balance_from').val();
	    var balance_to = $('#balance_to').val();
	    var service_status = $('#id_service_status').val();
		var plan = $('#id_plan').val();
		var operator = $('#id_operator').val();
	    var search_query = $('#customer-list_filter > label > input').val();
		var id_type_client = $('#id_type_client').val();
	    var url_open = "/customers/export/?"+"network_status="+network_status+"&balance_from="+balance_from+"&balance_to="+balance_to+"&service_status="+service_status+"&plan="+plan+"&search_query="+search_query+"&operator="+operator+"&id_type_client="+id_type_client;
	    window.open(url_open, "_blank");
	});
			     

});

