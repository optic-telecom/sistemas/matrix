
$(document).ready(function(){

    var e=$("#provider-payment-list").DataTable({
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: "/providers/payments/ajax2/",
        data: function (d) {
            d.kind_load = $('#id_kind_load').val();
            d.start_date = $('#id_start_date').val();
            d.end_date = $('#id_end_date').val();
            d.paid_on = $('#id_paid_on').val();
            d.operator = $('#id_operator').val();
        }},
        language: {
        "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        order:[[0,"desc"]],
        columns: [
        {data: "created_at"},
        {data: "pk"},
        {data: "paid_on"},
        {data: "formatted_amount"},
        {data: "kind"},
        {data: "kind_load"},
        {data: "operator"},
        {data: "comment"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1]
            
        },
        {
            orderable: false,
            targets:[2,3,4,5,6,7],
            
        },
        {
            render: function(data, type, row) {
            return '<a href="/providers/payments/'+row.pk+'">'+row.pk+'</a>';
            },
            targets: 1,
        },
        {
            render: function(data, type, row) {
            return row.symbol+" " +row.formatted_amount;
            },
            targets: 3,
        },
        {
            render: function(data, type, row) {
                d = new Date(row.created_at);
                day = ('0'+d.getDate()).slice(-2);
                mins = ('0'+d.getMinutes()).slice(-2);
                month = ('0'+(d.getMonth()+1)).slice(-2);
                hours = ('0'+d.getHours()).slice(-2);
                ds = day  + "-" + month + "-" + d.getFullYear() + " " + hours + ":" + mins;
                return ds;
            },
            targets: 0,
            targets: 2,
        },
        ],
    });
    

    $('#id_kind_load').change(function(){
        e.ajax.reload();
    })
    $('#id_paid_on').change(function(){
        e.ajax.reload();
    })
    $('#id_operator').change(function(){
        e.ajax.reload();
    })
    $('#id_start_date').change(function(){
        e.ajax.reload();
    })
    $('#id_end_date').change(function(){
        e.ajax.reload();
    })
    }
    );