"use strict";

$(document).ready(function () {
  function getCookie(name) {
    var cookieValue = null;

    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');

      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]); // Does this cookie string begin with the name we want?

        if (cookie.substring(0, name.length + 1) === name + '=') {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }

    return cookieValue;
  }

  function delay(x) {
    setTimeout(function () {
      x.ajax.reload();
    }, 2000);
  }

  var csrftoken = getCookie('csrftoken');
  var e = $("#service-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax/",
      data: function data(d) {
        d.operator = $('#id_operator').val();
        d.document_type = $('#id_document').val();
      }
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "print": "Imprimir"
      }
    },
    pageLength: 10,
    //colReorder:!0,
    buttons: ["excel", "pdf", "print"],
    searching: true,
    aLengthMenu: [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],
    order: [[0, "desc"]],
    columns: [{
      data: "number"
    }, {
      data: "customer"
    }, {
      data: "plan"
    }, {
      data: "plan_price"
    }, {
      data: "operator"
    }],
    columnDefs: [{
      orderable: true,
      targets: [0, 2, 4]
    }, {
      orderable: false,
      targets: [1, 3]
    }, {
      render: function render(data, type, row) {
        return '<div style="text-align:center"><a href="/services/' + row.number + '">' + row.number + '</a></div>';
      },
      targets: 0
    }, {
      render: function render(data, type, row) {
        return '<div style="text-align:center"><a title="Añadir cargo" target="_blank" href="/services/' + row.number + '/charge/new"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button></a> <a title="Añadir descuento" target="_blank" href="/services/' + row.number + '/credit/new"><button type="button" class="btn btn-primary"><i class="fa fa-minus"></i></button></a></div>';
      },
      targets: 5
    }, {
      render: function render(data, type, row) {
        buttons = '<div style="text-align:center"><a title="Previsualizar documento" target="_blank" href="/pre-bill/' + row.pk + '"><button type="button" class="btn btn-link"><h3><i class="fa fa-file-pdf"></i></h3></button></a> <button title="Añadir observación al documento" data-toggle="modal" data-target="#modalObservation' + row.number + '" type="button" class="btn btn-link"><h3><i class="fa fa-edit"></i></h3></button></div>';
        modal = '<div class="modal fade" id="modalObservation' + row.number + '" tabindex="-1" role="dialog" aria-labelledby="modalObservation' + row.number + 'Label" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <h5 class="modal-title" id="modalObservation' + row.number + 'Label">Observaciones del documento ' + row.number + '</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div><div class="modal-body"> <div class="form-group"> <label for="observation">Ingrese las observaciones que serán mostradas al cliente en el documento</label> <textarea maxlength="255" class="form-control" id="observation' + row.pk + '" rows="3"></textarea> </div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button> <button type="button" onclick="saveObservation(' + row.pk + ')" class="btn btn-primary" data-dismiss="modal">Guardar</button> </div></div></div></div>';
        return buttons + modal;
      },
      targets: 6
    }, {
      render: function render(data, type, row) {
        return '<div style="text-align:center"><input name="service" type="checkbox" id="' + row.pk + '"></div>';
      },
      targets: 7
    }]
  });
  $('#id_operator').change(function () {
    e.ajax.reload();
  });
  $('#id_document').change(function () {
    e.ajax.reload();
  });
  $('#tax').click(function () {
    parentCheck[0].checked = 0;
    var serv = [];

    for (i = 0; i < services.length; i++) {
      if (services[i].checked) {
        observationText = observationList[services[i].id];

        if (observationText) {// pass
        } else {
          observationText = false;
        }

        serv.push([{
          id: services[i].id,
          observation: observationText
        }]);
      }
    }

    console.log(serv);

    if (serv.length > 0) {
      $.ajax({
        url: 'ajax2/',
        type: 'POST',
        headers: {
          'X-CSRFToken': csrftoken
        },
        data: {
          "data": serv
        },
        success: function response(json) {
          if (json.buttonResult) {
            swal("ÉXITO", "Se están procesando " + serv.length + " documentos.", "success");
          } else {
            swal("ALERTA", "No se pudieron procesar los documentos debido a un error de conexión.", "warning");
          }

          delay(e);
        }
      });
    } else {
      swal("ALERTA", "Debe seleccionar al menos un documento para ser procesado.", "warning");
    }
  });
  /* UNIFIED */

  var u = $("#united-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax-unified/",
      data: function data(d) {
        d.operator = $('#id_operator').val();
      }
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "print": "Imprimir"
      }
    },
    pageLength: 10,
    searching: true,
    aLengthMenu: [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],
    order: [[0, "desc"]],
    columns: [{
      data: "id"
    }, {
      data: "name"
    }, {
      data: "services"
    }],
    columnDefs: [{
      orderable: true,
      targets: [0, 1]
    }, {
      orderable: false,
      targets: [2]
    }, {
      render: function render(data, type, row) {
        return '<div style="text-align:center"><a title="Información del cliente" target="_blank" href="/customers/' + row.id + '"><button class="btn btn-link">' + row.id + '</button></a></div>';
      },
      targets: 0
    }, {
      render: function render(data, type, row) {
        return '<div style="text-align:left">' + row.name + '</div>';
      },
      targets: 1
    }, {
      render: function render(data, type, row) {
        a = '<div style="text-align:center"><button onclick="showModal(' + row.id + ')" type="button" class="btn btn-primary"> <i class="fa fa-eye"></i> Ver</button></div><div id="services' + row.id + '" class="modal"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <h5 class="modal-title">Servicios del cliente #' + row.id + '</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> </div> <div class="modal-body"><table class="table table-hover"> <thead style="text-align:center"> <tr> <th scope="col">Número</th> <th scope="col">Plan</th> <th scope="col">Precio</th> <th scope="col">Cargos y Descuentos</th> </tr> </thead> <tbody>';

        for (var _i = 0; _i < row.services.length; _i++) {
          a += '<tr> <td scope="row"><a title="Ver detalles del servicio" target="_blank" href="/services/' + row.services[_i]["number"] + '">' + row.services[_i]["number"] + '</a></td> <td>' + row.services[_i]["plan"] + '</td> <td>' + row.services[_i]["plan_price"] + '</td> <td> <div style="text-align:center"><a title="Añadir cargo" target="_blank" href="/services/' + row.services[_i]["number"] + '/charge/new"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button></a> <a title="Añadir descuento" target="_blank" href="/services/' + row.services[_i]["number"] + '/credit/new"><button type="button" class="btn btn-primary"><i class="fa fa-minus"></i></button></a> </div> </td> </tr>';
        }

        a += '</tbody> </table> </div> </div> </div></div>';
        return a;
      },
      targets: 2
    }, {
      render: function render(data, type, row) {
        buttons = '<div style="text-align:center"><a title="Previsualizar documento" target="_blank" href="/pre-bill-unified/' + row.id + '"><button class="btn btn-link"><h3><i class="fa fa-file-pdf"></i></h3></button></a> <button title="Añadir observación al documento" data-toggle="modal" data-target="#modalObservation' + row.id + '" type="button" class="btn btn-link"><h3><i class="fa fa-edit"></i></h3></button></div>';
        modal = '<div class="modal fade" id="modalObservation' + row.id + '" tabindex="-1" role="dialog" aria-labelledby="modalObservation' + row.id + 'Label" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <h5 class="modal-title" id="modalObservation' + row.id + 'Label">Observaciones del documento ' + row.id + '</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div><div class="modal-body"> <div class="form-group"> <label for="observation">Ingrese las observaciones que serán mostradas al cliente en el documento</label> <textarea maxlength="255" class="form-control" id="observation' + row.id + '" rows="3"></textarea> </div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button> <button type="button" onclick="saveObservation(' + row.id + ')" class="btn btn-primary" data-dismiss="modal">Guardar</button> </div></div></div></div>';
        return buttons + modal;
      },
      targets: 3
    }, {
      render: function render(data, type, row) {
        return '<div style="text-align:center"><input name="unified" id="' + row.id + '" type="checkbox" ></div>';
      },
      targets: 4
    }]
  });
  $('#taxUnified').click(function () {
    parentCheckUnified[0].checked = 0;
    var customer = [];

    for (i = 0; i < unifiedDocument.length; i++) {
      if (unifiedDocument[i].checked) {
        observationText = observationList[unifiedDocument[i].id];

        if (observationText) {// pass
        } else {
          observationText = false;
        }

        customer.push([{
          customer: unifiedDocument[i].id,
          observation: observationText
        }]);
      }
    }

    console.log(customer);

    if (customer.length > 0) {
      /* AJAX */
      $.ajax({
        url: 'ajax-unified-2/',
        type: 'POST',
        headers: {
          'X-CSRFToken': csrftoken
        },
        data: {
          "data": customer
        },
        dataType: 'json',
        success: function response(json) {
          if (json.buttonResult) {
            swal("ÉXITO", "Se están procesando " + customer.length + " documentos.", "success");
          } else {
            swal("ALERTA", "No se pudieron procesar los documentos debido a un error de conexión.", "warning");
          }

          delay(u);
        }
      });
    } else {
      swal("ALERTA", "Debe seleccionar al menos un documento para ser procesado.", "warning");
    }
  });
});