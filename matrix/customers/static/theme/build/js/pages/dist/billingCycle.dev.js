"use strict";

$(document).ready(function () {
  function getCookie(name) {
    var cookieValue = null;

    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');

      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]); // Does this cookie string begin with the name we want?

        if (cookie.substring(0, name.length + 1) === name + '=') {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }

    return cookieValue;
  }

  function delay(x) {
    setTimeout(function () {
      x.ajax.reload();
    }, 2000);
  }

  var csrftoken = getCookie('csrftoken');
  /* Date's var declaration */

  var now = new Date();
  var month = now.getMonth() + 1;
  var day = now.getDate();
  var today = new Date(now.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day);
  /* AJAX */

  var e = $("#cycle-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax/"
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "print": "Imprimir"
      }
    },
    pageLength: 10,
    searching: false,
    aLengthMenu: [[10, 20, 30, 40, 50], [10, 20, 30, 40, 50]],
    order: [[0, "desc"]],
    columns: [{
      data: "id"
    }, {
      data: "interval"
    }],
    columnDefs: [{
      orderable: true,
      targets: [0]
    }, {
      orderable: false,
      targets: [1, 2]
    }, {
      render: function render(data, type, row) {
        return '<div style="text-align:center">' + row.id + '</div>';
      },
      targets: 0
    }, {
      render: function render(data, type, row) {
        return '<div style="text-align:center" name="interval">' + row.interval + '</div>';
      },
      targets: 1
    }, {
      render: function render(data, type, row) {
        var dateStart = new Date(row.interval.split(" - ")[0]);
        var dateEnd = new Date(row.interval.split(" - ")[1]);

        if (today >= dateStart && today <= dateEnd) {
          return '<div style="text-align:center" id="' + row.id + '">El reporte estará disponible al vencerse el ciclo</div>';
        } else {
          return '<div style="text-align:center" id="' + row.id + '"><a title="Ver documentos emitidos en el ciclo" href="' + row.id + '"><button type="button" class="btn btn-primary"><i class="fa fa-eye"></i> Documentos emitidos</button></a> <a title="Descargar excel" href="' + row.id + '/export/"><button type="button" class="btn btn-outline-success"><i class="fa fa-file-excel"></i> Exportar</button></a> </div>';
        }
      },
      targets: 2
    }]
  });
});