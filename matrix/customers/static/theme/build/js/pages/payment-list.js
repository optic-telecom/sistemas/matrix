
$(document).ready(function(){

var e=$("#payment-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
    url: "/customers/payments/ajax/",
    data: function (d) {
        d.kind_kind = $('#id_kind_kind').val();
        d.kind = $('#id_kind').val();
        d.agent = $('#id_agent').val();
        d.operator = $('#id_operator').val();
        d.operator_cliente = $('#id_operator_cliente').val();
        d.bankaccount = $('#id_bankaccount').val();
        d.cleared = $('#id_cleared').val();
        d.service = $('#id_service').val();
        d.start_date = $('#id_start_date').val();
        d.end_date = $('#id_end_date').val();
    }},
    language: {
    "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "print": "Imprimir"
    }
    },
    
    pageLength:10,
    //colReorder:!0,
    buttons:["excel","pdf","print"],
    searching: true,
    aLengthMenu:[[10,20,50],[10,20,50]],
    order:[[0,"desc"]],
    columns: [
    {data: "created_at"},
    {data: "pk"},
    {data: "customer_name"},
    {data: "customer_operator"},
    {data: "paid_on"},
    {data: "formatted_amount"},
    {data: "manual"},
    {data: "kind"},
    {data: "agent"},
    {data: "operator"},
    {data: "cleared"},
    ],
    columnDefs:[
    {
        orderable: true,
        targets:[0,1,2,4,5,6]
        
    },
    {
        orderable: false,
        targets:[3,7,8,9,10],
        
    },
    {
        render: function(data, type, row) {
            d = new Date(row.created_at);
            day = ('0'+d.getDate()).slice(-2);
            mins = ('0'+d.getMinutes()).slice(-2);
            month = ('0'+(d.getMonth()+1)).slice(-2);
            hours = ('0'+d.getHours()).slice(-2);
            ds = day  + "-" + month + "-" + d.getFullYear() + " " + hours + ":" + mins;
            return ds;
        },
        targets: 0,
    },
    {
        render: function(data, type, row) {
        return '<a href="/customers/payments/'+row.pk+'">'+row.pk+'</a>';
        },
        targets: 1,
    },
    {
        render: function(data, type, row) {
        return '<a href="/customers/'+row.customer_pk+'/billing/">'+data+'</a>';
        },
        targets: 2,
    },
    {
        render: function(data, type, row) {
        return row.customer_operator;
        },
        targets: 3,
    },
    {
	    render: function(data, type, row) {
		return row.customer_operator;
	    },
	    targets: 3,
    },
    {
        render: function(data, type, row) {
            d = new Date(row.paid_on);
            day = ('0'+d.getDate()).slice(-2);
            mins = ('0'+d.getMinutes()).slice(-2);
            month = ('0'+(d.getMonth()+1)).slice(-2);
            hours = ('0'+d.getHours()).slice(-2);
            ds = day  + "-" + month + "-" + d.getFullYear() + " " + hours + ":" + mins;
            return ds;
        },
        targets: 4,
    },
    {
	    render: function(data, type, row) {
		return row.symbol+" " +row.formatted_amount;
	    },
	    targets: 5,
    },
    {
        render: function(data, type, row) {
            var count = 0;
            if (row.proof == true){
                count++;
            }
            if (row.proof2 == true){
                count++;
            }
            if (row.proof3 == true){
                count++;
            }
            if (count == 0){
                return row.kind;
            }else{
                return row.kind + ' <i class="ti-folder"></i><sup>.<span title="comprobante de pago">'+ count +'</span></sup>.'
            }
        
        },
        targets: 7,
    },
    {
	    render: function(data, type, row) {
		return row.manual;
	    },
	    targets: 6,
    },
    {
	    render: function(data, type, row) {
		return row.agent;
	    },
	    targets: 8,
    },
    {
	    render: function(data, type, row) {
		return row.operator;
	    },
	    targets: 9,
    },
    {
        render: function(data, type, row) {
            var canClear = document.getElementById("canClear").value;
            if (row.cleared == false) {
                if (canClear == "yes"){
                    return '<input type="checkbox" payment_id="'+row.pk+'" onclick="clearPayment(this)">';
                } else{
                    return '<input type="checkbox" disabled>';
                }
                               
            } else {
                if (canClear == "yes"){
                    return '<input type="checkbox" checked payment_id="'+row.pk+'" onclick="clearPayment(this)">';
                } else{
                    return '<input type="checkbox" checked disabled>';
                }
                
            }
	    },
	    targets: 10,
	},
    ],
});

$('#id_kind_kind').change(function(){
    e.ajax.reload();
})

$('#id_kind').change(function(){
    e.ajax.reload();
})

$('#id_agent').change(function(){
    e.ajax.reload();
})

$('#id_operator').change(function(){
    e.ajax.reload();
})
$('#id_operator_cliente').change(function(){
    e.ajax.reload();
})
$('#id_bankaccount').change(function(){
    e.ajax.reload();
})

$('#id_cleared').change(function(){
    e.ajax.reload();
})

$('#id_service').change(function(){
    e.ajax.reload();
})

$('#id_start_date').change(function(){
    e.ajax.reload();
})

$('#id_end_date').change(function(){
    e.ajax.reload();
})


}
);


$('#export-button').click(function() {
    var bankaccount = $('#id_bankaccount').val();
    var operator = $('#id_operator').val();
    var operator_cliente = $('#id_operator_cliente').val();
    var kind_kind = $('#id_kind_kind').val();
    var kind = $('#id_kind').val();
    var agent = $('#id_agent').val();
    var cleared = $('#id_cleared').val();
    var start_date = $('#id_start_date').val();
    var end_date = $('#id_end_date').val();    
    var service = $('#id_service').val();
    var search_query = $('#customer-list_filter > label > input').val();
    var url_open = "/customers/payments/excel/?start_date="+start_date+"&cleared="+cleared+
                   "&end_date="+end_date+"&search_query="+search_query+"&bankaccount="+bankaccount+
                   "&kind_kind="+kind_kind+"&kind="+kind+"&agent="+agent+"&operator="+operator+
                   "&service="+service+"&operator_cliente="+operator_cliente;
    window.open(url_open, "_blank");
});