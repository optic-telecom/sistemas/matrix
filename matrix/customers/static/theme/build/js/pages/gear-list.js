
$(document).ready(function(){

var e=$("#gear-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
        url: "/gear/ajax/",
        data: function (d) {
            d.vlan = $('#id_vlan').val();
            d.brand = $('#id_brand').val();
            d.kind = $('#id_kind').val();
            d.node= $('#id_node').val();
        }},
    language: {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
	"buttons": {
	    "print": "Imprimir"
	}
    },
    
    pageLength:10,
    //colReorder:!0,
    buttons:["excel","pdf","print"],
    searching: true,
    aLengthMenu:[[10,20,50],[10,20,50]],
    //order:[[0,"desc"]],
    columns: [
	{data: "pk"},
	{data: "ip"},
	{data: "description"},
	{data: "kind"},
	{data: "mac"},
	{data: "vlan"},
	{data: "node"},
	{data: "days"},
    ],
    columnDefs:[
	{
	    orderable: true,
	    targets:[0,1,4,7]
	},
	{
	    orderable: false,
	    targets:[2,3,5,6]
	},
	{
	    render: function(data, type, row) {
		return '<a href="/gear/'+row.DT_RowId+'/">'+data+'</a>';
	    },
	    targets: 1,
	},
	{
	    render: function(data, type, row) {
		if (row.days != null) {
		    return '<span class="label label-lg label-'+row.days_class+'">'+data+' d.</span>';
		} else {
		    return '<span class="label label-lg label-'+row.days_class+'">Nunca</span>';
		}
	    },
	    targets: 7,
	},
    ],
});

$('#id_vlan').change(function(){
    e.ajax.reload();
})

$('#id_brand').change(function(){
    e.ajax.reload();
})

$('#id_kind').change(function(){
    e.ajax.reload();
})

$('#id_node').change(function(){
    e.ajax.reload();
})

}
);
