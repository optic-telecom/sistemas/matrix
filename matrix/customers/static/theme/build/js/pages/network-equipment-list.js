
    let table = $("#network_equipment_table").DataTable({
        processing: true,
        serverSide: true,
        ajax:{
          url:dt_url,
          method: "GET",
          type: "json",
          dataSrc: function(response){
            // console.log($(this).page)
            // response.recordsTotal = response.recordsTotal
            // var info = $('#network_equipment_table').DataTable().page.info();
            // console.log(info.page + 1)
            return response.results
          },
          data:function(d, settings){
            d.ip = $("#ips").val();
            d.mac = $("#mac").val();
            d.service = $("#service").val();
  
            var api = new $.fn.dataTable.Api(settings);
  
            // Convert starting record into page number
            d.page = Math.min(
              Math.max(0, Math.round(d.start / api.page.len())),
              api.page.info().pages
            )+1;
  
          }
        },
        language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ equipos de cliente",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando equipos de cliente del _START_ al _END_ de un total de _TOTAL_ equipos de cliente",
          "sInfoEmpty":      "Mostrando equipos de cliente del 0 al 0 de un total de 0 equipos de cliente",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ equipos de cliente)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          },
          "buttons": {
              "print": "Imprimir"
          }
          },
        responsive: true, 
        columns:[
          {"data": "service.id" },
          //{"data": "service.number"},
          {"data": "service.address"},
          {"data": "service.status_service"},
          {"data": "ip"},
          {"data": "service.status_lease"},
          {"data": "brand"},
          {"data": "mac"},
          {"data": "model"},
        ],
        "columnDefs": [ 
        {"targets":[0,1,2,3,4,5,6,7], orderable:false},  
        {
          "targets": 0,
          orderable:false,
          "render": function ( data, type, row, meta ) {
            return '<a href="/services/'+row.service.id+'/network/">'+row.service.number+'</a>';
          }
        } 
      ],
  
      })
  
      let ips = $("#ips").select2({
        width: '100%',
        minimumInputLength: 6,
        placeholder: "Filtro de IP's",
        ajax:{
          url: "autocompletes/ips-networks-equipment/",
          data: function (params) {
            var query = {
              q: params.term,
              page: params.page || 1,
              type: 'public'
            }
  
            // Query parameters will be ?search=[term]&type=public
            return query;
          },
        }
      })
      
      let service = $("#service").select2({
        width: '100%',
        placeholder: "Filtro de Servicios",
        ajax:{
          url: "autocompletes/service-networks-equipment/",
          data: function (params) {
            var query = {
              q: params.term,
              page: params.page || 1,
              type: 'public'
            }
  
            // Query parameters will be ?search=[term]&type=public
            return query;
          },
        }
      })
  
      let mac = $("#mac").select2({
        width: '100%',
        placeholder: "Filtro de MAC's",
        minimumInputLength: 3,
        ajax:{
          url: "autocompletes/macs-networks-equipment/",
          data: function (params) {
            var query = {
              q: params.term,
              page: params.page || 1,
              type: 'public'
            }
  
            // Query parameters will be ?search=[term]&type=public
            return query;
          },
        }
      })
  
      $("#ips").on("change", function(){ 
        table.ajax.reload(null, false);
      })
      $("#service").on("change", function(){ 
        table.ajax.reload(null, false);
      })
      $("#mac").on("change", function(){ 
        table.ajax.reload(null, false);
      })
  