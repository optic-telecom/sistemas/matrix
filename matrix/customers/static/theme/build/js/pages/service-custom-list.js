
$(document).ready(function(){

    var e=$("#service-custom-list").DataTable({
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: window.location.pathname+'ajax',
        data: function (d) {
            const filters = window.location.search.slice(1).split('&')
            filters.forEach(filter=>{
                const splittedFilter = filter.split('=')
                d[splittedFilter[0]] = splittedFilter[1]
            })
        }},
        language: {
        "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        order:[[0,"desc"]],
        columns: [
            {data: "number"},
            {data: "customer"},
            {data: "address"},
            {data: "plan"},
            {data: "status"}
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2,3]
        },
        {
            render: function(data, type, row) {
                return '<a href="/services/'+row.number+'/">'+data+'</a>';
            },
            targets: 0,
        },
        ]
    });
   
});