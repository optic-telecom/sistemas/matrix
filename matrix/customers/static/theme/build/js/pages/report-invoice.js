$(document).ready(function () {

  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');

      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);

        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  function delay(x) {
    setTimeout(function () {
      x.ajax.reload();
    }, 2000);
  }

  var csrftoken = getCookie('csrftoken');

  /* AJAX */
  var e = $("#reported-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,

    ajax: {
      url: "ajax/",
    },

    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "print": "Imprimir"
      }
    },

    pageLength: 10,
    searching: false,
    aLengthMenu: [
      [10, 20, 30, 40, 50],
      [10, 20, 30, 40, 50]
    ],

    order: [
      [0, "desc"]
    ],
    columns: [{
      data: "invoice"
    }, {
      data: "kind"
    }, {
      data: "created"
    }, {
      data: "customer"
    }, {
      data: "services"
    }, {
      data: "total"
    }, ],

    columnDefs: [{
      orderable: true,
      targets: [0, 1, 2, 3, 4, 5]
    }, {
      render: function (data, type, row) {
        return '<div style="text-align:center">' + row.invoice + '</div>';
      },
      targets: 0,
    }, {
      render: function (data, type, row) {
        return '<div style="text-align:center" name="kind">' + row.kind + '</div>';
      },
      targets: 1,
    }, {
      render: function (data, type, row) {
        return '<div style="text-align:center" name="kind">' + row.created + '</div>';
      },
      targets: 2,
    }, {
      render: function (data, type, row) {
        return '<div style="text-align:center" name="kind">' + row.customer + '</div>';
      },
      targets: 3,
    }, {
      render: function (data, type, row) {
        html = '<div style="text-align:center" name="kind">';
        for (let i = 0; i < row.services.length; i++) {
          html += '<a title="Reporte de documentos emitidos" target="_blanck" href="/services/' + row.services_id[i] + '"><button type="button" class="btn btn-link">' + row.services[i] + '</button></a>';
        }
        html += '</div>';
        return html
      },
      targets: 4,
    }, {
      render: function (data, type, row) {
        return '<div style="text-align:center" name="kind">' + row.total + '</div>';
      },
      targets: 5,
    }, ],
  });
});