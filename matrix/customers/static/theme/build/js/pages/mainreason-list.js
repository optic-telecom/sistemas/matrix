$(document).ready(function(){

	var e=$("#reasons-list").DataTable({
	    order:[],
	    processing: true,
	    serverSide: true,
	    searchDelay: 400,
	    ajax: {
		url: "/finances/management/mainreason_ajax/",
		data: function (d) {
			d.kind = $('#id_kind').val();
			d.active = $('#active').val();
			d.percent = $('#percent').val();
			d.calculate_by_days = $('#calculate_by_days').val();
			d.operator = $('#id_operator').val();
		}},
	    language: {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    },
		"buttons": {
		    "print": "Imprimir"
		}
	    },
	    pageLength:10,
	    //colReorder:!0,
	    buttons:["excel","pdf","print"],
	    searching: true,
	    aLengthMenu:[[10,20,50,100],[10,20,50,100]],
	    //order:[[0,"desc"]],
	    columns: [
		{data: "reason"},
		{data: "valor"},
		{data: "kind"},
		{data: "active"},
		{data: "id_mr"},
	    ],
	    columnDefs:[
		{
		    orderable: true,
		    targets:[0,1]
		},
		{
		    orderable: false,
		    targets:[2,3,4]
		},
		{
		    render: function(data, type, row) {
			return row.reason;
		    },
		    targets: 0,
		},
		{
		    render: function(data, type, row) {
				if (row.percent){
					return row.valor+"%";
				}else if (row.uf) {
					return row.valor+" UF";
				}else{
					return "$"+row.valor;
				}
		    },
		    targets: 1,
		},
		{
		    render: function(data, type, row) {
			return row.kind;
		    },
		    targets: 2,
		},
		{
		    render: function(data, type, row) {
				if (row.active){
					return "Sí";
				}else{
					return "No";
				}
		    },
		    targets: 3,
		},
		{
		    render: function(data, type, row) {
				var editar='<a class="btn btn-primary" style="width:50%" href="/finances/management/reasons/'+row.id_mr+ '/edit/"> Editar </a>';
				var eliminar='<div class="dropdown">\
				<a class="btn btn-danger dropdown-toggle" style="width:50%" href="#" role="button" id="dropdownMenuLink{{extra.pk}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Eliminar <span class="dropdown-menu-arrow"></span></a>\
				<div class="dropdown-menu" aria-labelledby="dropdownMenuLink{{extra.pk}}" style="min-width: 6.5rem !important; ">\
						<a class="dropdown-item" href="../../management/mainreason/'+row.id_mr+ '/delete/">Sí, estoy seguro</a>\
					</div>\
				</div>';
				if ($('#id_user').val()=="true"){
					return editar + eliminar;
				}
				return editar;
		    },
		    targets: 4,
		},
	    ],
	});
    $('#id_operator').change(function(){
        e.ajax.reload();
    })
	$('#id_kind').change(function(){
		e.ajax.reload();
	})

	$('#calculate_by_days').change(function(){
		e.ajax.reload();
	})
	
	$('#active').change(function(){
		e.ajax.reload();
	})
	$('#percent').change(function(){
		e.ajax.reload();
	})
});

