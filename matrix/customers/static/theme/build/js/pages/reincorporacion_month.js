
$(document).ready(function(){
var e=$("#reincorporacion-list").DataTable({
        order:[],
        processing: true,
        serverSide: true,
        //searchDelay: 400,
        ajax: {
        url: window.location.pathname+'ajax/',
        data: function (d) {
            d.plan = $('#id_plan').val();
        }},
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: false,
        aLengthMenu:[[10,20,50],[10,20,50]],
        //order:[[0,"desc"]],
        columns: [
        {data: "service"},
        {data: "plan"},
        {data: "address"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[]
        },
        {
            orderable: false,
            targets:[0,1,2]
        },
        {
            render: function(data, type, row) {
                return 'Servicio <a href="/services/'+row.service_id+'">#'+row.service+'</a>';

            },
            targets: 0,
        },
        {
            render: function(data, type, row) {
            return row.plan;
            },
            targets: 1,
        },
        {
            render: function(data, type, row) {
            return row.address;
            },
            targets: 2,
        },
        ],
    });
    
    $('#id_plan').change(function(){
        e.ajax.reload();
    })
    }
    );
