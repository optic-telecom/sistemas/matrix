
$(document).ready(function(){
var e=$("#porretirar-list").DataTable({
        order:[],
        processing: true,
        serverSide: true,
        //searchDelay: 400,
        ajax: {
        url: "/customers/porretirar-table-ajax/",
        data: function (d) {
            d.year = $('#id_year').val();
            d.report = $('#id_report').val();
        }},
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:13,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: false,
        aLengthMenu:[[13],[13]],
        //order:[[0,"desc"]],
        columns: [
        {data: "mes"},
        {data: "hsu"},
        {data: "fibra"},
        {data: "empresa"},
        {data: "total"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[]
        },
        {
            orderable: false,
            targets:[0,1,2,3,4]
        },
        {
            render: function(data, type, row) {
            return '<a href="/reports/porretirar-mes/'+row.month_number+'/'+row.year+'/">'+row.mes+'</a>';
            },
            targets: 0,
        },
        {
            render: function(data, type, row) {
            return row.hsu;
            },
            targets: 1,
        },
        {
            render: function(data, type, row) {
            return row.fibra;
            },
            targets: 2,
        },
        {
            render: function(data, type, row) {
            return row.empresa;
            },
            targets: 3,
        },
        {
            render: function(data, type, row) {
            return row.total;
            },
            targets: 4,
        },
        ],
    });
    
    $('#id_year').change(function(){
        e.ajax.reload();
    })
    $('#id_report').change(function(){
        e.ajax.reload();
    })
    }
    );

