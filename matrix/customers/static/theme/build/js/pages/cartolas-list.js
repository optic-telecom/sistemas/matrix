
$(document).ready(function(){

    var e=$("#cartolas-list").DataTable({
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: window.location.pathname+'ajax',
        data: function (d) {
            d.start_date = $('#cartola_start_date').val();
            d.end_date = $('#cartola_end_date').val();
        }},
        language: {
        "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        order:[[0,"desc"]],
        columns: [
            {data: "number"},
            {data: "start"},
            {data: "end"},
            {data: "saldo_final"},
            {data: "saldo_inicial"},
            {data: "total_cargo"},
            {data: "total_abono"},
            {data: "pdf"},
            {data: "details"}
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2,3,4]
        },
        {
            render: function(data, type, row) {
                pdf="";
                if (row.pdf){
                    pdf='<a href="'+row.pdf+'" class="btn btn-primary" target="_blank" title="Previsualizar pdf"><i class="fa fa-file-pdf"></i> </a> ';
                }

                excel="";
                if (row.excel){
                    excel=' <a href="'+row.excel+'" class="btn btn-primary" target="_blank" title="Abrir excel"><i class="fa fa-file"></i> </a>';
                }
                
                return pdf+excel;
            },
            targets: 7,
        },
        {
            render: function(data, type, row) {
                return ' <a href="/customers/bank-accounts/cartola/'+row.pk+'" class="btn btn-success">Ver movimientos</a>';
            },
            targets: 8,
        },
        {
            render: function(data, type, row) {
            return row.symbol+" " +row.saldo_final;
            },
            targets: 3,
        },
        {
            render: function(data, type, row) {
            return row.symbol+" " +row.saldo_inicial;
            },
            targets: 4,
        },
        {
            render: function(data, type, row) {
            return row.symbol+" " +row.total_cargo;
            },
            targets: 5,
        },
        {
            render: function(data, type, row) {
            return row.symbol+" " +row.total_abono;
            },
            targets: 6,
        },
        ],
    });
    $('#cartola_start_date').change(function(){
        e.ajax.reload();
    })
    
    $('#cartola_end_date').change(function(){
        e.ajax.reload();
    })
    }
    );