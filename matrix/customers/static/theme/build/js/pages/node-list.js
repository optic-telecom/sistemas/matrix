
$(document).ready(function(){

var e=$("#node-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: "/nodes/ajax/",
    language: {
    "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
	"buttons": {
	    "print": "Imprimir"
	}
    },
    
    pageLength:10,
    //colReorder:!0,
    buttons:["excel","pdf","print"],
    searching: true,
    aLengthMenu:[[10,20,50],[10,20,50]],
    //order:[[0,"desc"]],
    columns: [
	{data: "pk"},
	{data: "code"},
	{data: "address"},
	{data: "commune"},
	{data: "parent"},
	{data: "services"},
	{data: "status"},
	{data: "seller"},
    ],
    columnDefs:[
	{
	    orderable: true,
	    targets:[0,1,2,3,7]
	},
	{
	    orderable: false,
	    targets:[4,5,6]
	},
	{
	    render: function(data, type, row) {
		return '<span class="label label-'+row.status_class+'">'+data+'</span>';
	    },
	    targets: 6,
	},
	{
	    render: function(data, type, row) {
		return '<a href="/nodes/'+row.DT_RowId+'/">'+data+'</a>';
	    },
	    targets: 1,
	},
	{
	    render: function(data, type, row) {
		return '<a href="/nodes/'+row.DT_RowId+'/">'+data+'</a>';
	    },
	    targets: 0,
	},
	{
	    render: function(data, type, row) {
		return '<a href="/nodes/'+row.DT_RowId+'/services/">'+data+'</a>';
	    },
	    targets: 5,
	}
    ],
});


}
);

