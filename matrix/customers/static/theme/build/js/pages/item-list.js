$(document).ready(function () {
    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
          }
        }
      }
      return cookieValue;
    }
  
    function delay(x) {
      setTimeout(function () {
        x.ajax.reload();
      }, 2000);
    }
    var csrftoken = getCookie('csrftoken');
    /* Date's var declaration */
    var now = new Date();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var today = new Date(now.getFullYear() + '-' +
      (month < 10 ? '0' : '') + month + '-' +
      (day < 10 ? '0' : '') + day);
    /* AJAX */
    var e = $("#item-list").DataTable({
      processing: true,
      serverSide: true,
      searchDelay: 400,
      ajax: {
        url: "ajax/",
      },
      language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst": "Primero",
          "sLast": "Último",
          "sNext": "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
          "print": "Imprimir"
        }
      },
  
      pageLength: 10,
      searching: false,
      aLengthMenu: [
        [10, 20, 30, 40, 50],
        [10, 20, 30, 40, 50]
      ],
      order: [
        [0, "desc"]
      ],
      columns: [{
            data: "name"
        },
        {
            data: "multiplicative"
        },
        {
            data: "sumative"
        },
        {
            data: "sign"
        },
        {
            data: "description"
        },
        {
            data: "company"
        },
        {
            data: "active"
        }
      ],
      columnDefs: [{
          orderable: false,
          targets: [1,2,4]
  
        },
        {
          render: function(data, type, row) {
            return '<a href="/hr/items/'+row.pk+'/">'+data+'</a>';
          },
          targets: 0,
        },
        {
          render: function (data, type, row) {
            return '<div style="text-align:center">' + row.name + '</div>';
          },
          targets: 0,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.multiplicative + '</div>';
            },
            targets: 1,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.sumative + '</div>';
            },
            targets: 2,
        },
        {
          render: function (data, type, row) {
            return '<div style="text-align:center">' + row.sign + '</div>';
          },
          targets: 3,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.description + '</div>';
            },
            targets: 4,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.company + '</div>';
            },
            targets: 5,
        },
        {
          render: function (data, type, row) {
              if (row.active == "False") {
                  return '<div style="text-align:center;"> <input type="checkbox" item_id="'+row.pk+'" onclick="activateItem(this)"></div>';               
            } else {
                  console.log(row.active);
                  return '<div style="text-align:center;"> <input type="checkbox" checked item_id="'+row.pk+'" onclick="activateItem(this)"></div>';
            }
          },
          targets: 6,
      },
        {
          render: function(data, type, row) {
            html = '<div class="text-center"><a href="/hr/items/'+row.pk+'/delete/" class="btn btn-danger" onclick="return confirm(\'¿Seguro?\');" style="text-align: center"><i class="fa fa-times" ></i></a></div>';
            return html;
          },
          targets: 7
        }
      ],
    });
  });