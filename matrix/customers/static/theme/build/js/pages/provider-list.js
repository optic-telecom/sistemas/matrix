
$(document).ready(function(){

    var e=$("#provider-list").DataTable({
        order:[],
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: "/providers/ajax/",
        },
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        //order:[[0,"desc"]],
        columns: [
        {data: "DT_RowId"},
        {data: "rut"},
        {data: "name"},
        {data: "email"},
        {data: "address"},
        {data: "commune"},
        {data: "phone"},
        {data: "balance"},
        {data: "operator"}
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2,3,4,5,7,8]
        },
        {
            orderable: false,
            targets:[7]
        },
        {
            render: function(data, type, row) {
            return '<a href="/providers/'+row.DT_RowId+'/">'+data+'</a>';
            },
            targets: 1,
        },
        {
            render: function(data, type, row) {
            return '<a href="/providers/'+row.DT_RowId+'/statement/">'+data+'</a>';
            },
            targets: 7,
        },
        ],
    });
    
    
    
    }
    );

