
$(document).ready(function(){
var e=$("#porretirar-list").DataTable({
        order:[],
        processing: true,
        serverSide: true,
        //searchDelay: 400,
        ajax: {
        url: "/customers/payments-plan-table-ajax/",
        data: function (d) {
            d.year = $('#id_year').val();
            d.mes = $('#id_mes').val();
            d.plan = $('#id_plan').val();
        }},
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: false,
        aLengthMenu:[[10,20,50],[10,20,50]],
        //order:[[0,"desc"]],
        columns: [
        {data: "created_at"},
        {data: "comment"},
        {data: "customer"},
        {data: "plan"},
        {data: "monto"},
        {data: "invoice"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0]
        },
        {
            orderable: false,
            targets:[1,2,3,4,5]
        },
        {
            render: function(data, type, row) {
                return row.created_at;

            },
            targets: 0,
        },
        {
            render: function(data, type, row) {
                return '<a href="/customers/payments/'+row.pk+'">'+row.comment+'</a>' ;
            },
            targets: 1,
        },
        {
            render: function(data, type, row) {
            return row.customer;
            },
            targets: 2,
        },
        {
            render: function(data, type, row) {
            return '$ '+row.monto;
            },
            targets: 3,
        },
        {
            render: function(data, type, row) {
            return row.plan;
            },
            targets: 4,
        },
        {
            render: function(data, type, row) {
                var html='';
				var obj = JSON.parse(row.invoice);
				for (const p in obj){
					var html=html+'<a style="width:50%" href="../../../../../../customers/invoices/'+obj[p]['pk']+'">'+obj[p]['pk']+'</a> <br>';
				}
				return html;
            },
            targets: 5,
        },
        ],
    });
    
    $('#id_year').change(function(){
        e.ajax.reload();
    })
    $('#id_mes').change(function(){
        e.ajax.reload();
    })
    $('#id_plan').change(function(){
        e.ajax.reload();
    })
    }
    );

    $('#export-button').click(function() {
        var plan = $('#id_plan').val();
        var year = $('#id_year').val();
        var mes = $('#id_mes').val();
        var url_open = "/reports/payments_plan_excel/?plan="+plan+"&mes="+mes+"&year="+year;
        window.open(url_open, "_blank");
    });