
$(document).ready(function () {
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
  function delay(x) {
    setTimeout(function () { x.ajax.reload(); }, 2000);
  }
  var csrftoken = getCookie('csrftoken');
  var e = $("#indicator").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax/",
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "new": "Nuevo"
      }
    },
    bPaginate: false,
    searching: false,
    paging: false,
    ordering: false,
    info: false,
    order: [[0, "desc"]],
    columns: [
      { data: "kind" },
      { data: "created" },
      { data: "value" },
    ],
    columnDefs: [
      {
        orderable: true,
        targets: [0, 1, 2]
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.kind + '</div>';
        },
        targets: 0,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.created + '</div>';
        },
        targets: 1,
      },
      {
        render: function (data, type, row) {
          if (row.kind_id ===2) {
            return '<div style="text-align:center">' + row.value * 100 + '%</div>';
          } else {
            return '<div style="text-align:center">$ ' + row.value + '</div>';
          }
        },
        targets: 2,
      },
    ],
  });
  $('#createIndicator').click(function () {
    var value = $('#value').val();
    var type = $('#type').val();
    if (value.length > 0 & type.length > 0) {
      if (value > 1 & type == 2) {
        value = value / 100;
        console.log(value);
      }
      $.ajax({
        url: 'ajax2/',
        type: 'POST',
        headers: { 'X-CSRFToken': csrftoken },
        data: {
          "value": value,
          "type": type,
        },
        dataType: 'json',
      });
      delay(e);
      swal("ÉXITO", "Se ha registrado el indicador.", "success");
      $('#indicatorModal').modal('toggle');
      $("#formIndicator")[0].reset();
    } else {
      swal("ERROR", "Debe todos los campos.", "warning");
    }
  });
});

