
$(document).ready(function(){
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
            }
          }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');
    var monitoring_id = 0;
    var tracing_id = false;
    var edit = false;

    var e=$("#qa-monitoring-list").DataTable({
        order:[],
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: "/customers/qa-monitoring/ajax/",
        data: function (d) {
            d.date_from = $('#date_from').val();
		    d.date_to = $('#date_to').val();
        }},
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        columns: [
        {data: "DT_RowId"},        
        {data: "date"},
        {data: "agent"},
        {data: "phone"},
        {data: "email"},
        {data: "rut"},
        {data: "service"},
        {data: "created_at"},
        {data: "created_by"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2,3,4,5,6,7,8]
        },
        {
            render: function(data, type, row) {
            return data;
            },
            targets: 0,
        },
        {
            render: function(data, type, row) {
            return '<div class="text-center"><button data-name="seguimientos" data-id="'+row.DT_RowId+'" class="btn btn-primary col-10">+</button></div>';
            },
            targets: 9,
        },
        {
            render: function(data, type, row) {
              return '<div class="text-center"><i class="fa fa-eye mx-1 text-primary" aria-hidden="true" style="cursor:pointer;" title="Ver" data-name="action" data-type="detail" data-id="'+row.DT_RowId+'"></i><a href="/customers/qa-monitoring/'+row.DT_RowId+'/edit/"><i class="fa fa-edit mx-1 text-indigo" aria-hidden="true" style="cursor:pointer;" title="Editar" data-name="action" data-type="edit" data-id="'+row.DT_RowId+'"></i></a><i class="fa fa-trash mx-1 text-danger" aria-hidden="true" style="cursor:pointer;" title="ELiminar" data-name="action" data-type="delete" data-id="'+row.DT_RowId+'"></i></div>';
            },
            targets: 10,
        }
    
        ],
    });

  $('#date_from').blur(function(){
    if ($('#date_to').val()){
      if ($('#date_to').val() < $(this).val()){
        $('#date_to').val($(this).val())
      }
    }
	  e.ajax.reload();
	});

	$('#date_to').blur(function(){
    if ($('#date_from').val()){
      if ($('#date_from').val() > $(this).val()){
        $(this).val($('#date_from').val())
      }
    }
	  e.ajax.reload();
  });

  var s=$("#tracings-list").DataTable({
      order:[],
      processing: true,
      serverSide: true,
      searchDelay: 400,
      ajax: {
      url: "/customers/qa-monitoring/tracing/ajax/",
      data: function (d) {
          d.monitoring_id = monitoring_id;
      }},
      language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Registros del _START_ al _END_ de _TOTAL_ registros",
        "sInfoEmpty":      "Registros del 0 al 0 de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:5,
        /*searching: true,*/
        bFilter: false,
        aLengthMenu:[[5,10],[5,10]],
        columns: [       
        {data: "solved"},
        {data: "reason"},
        {data: "comment"},
        ],
        columnDefs:[
        {
            orderable: false,
            targets:[0,1,2]
        },
        {
          render: function(data, type, row) {
            if (data){
              return 'Si';
            }else{
              return 'No';
            }
          },
          targets: 0,
        },
        {
            render: function(data, type, row) {
            return '<div class="text-center"><i class="fa fa-edit mx-1 text-primary" aria-hidden="true" style="cursor:pointer;" title="Editar" data-name="action" data-type="edit" data-id="'+row.DT_RowId+'"></i><i class="fa fa-trash mx-1 text-danger" aria-hidden="true" style="cursor:pointer;" title="ELiminar" data-name="action" data-type="delete" data-id="'+row.DT_RowId+'"></i></div>';
            },
            targets: 3,
        }
    
        ],
    });

    $("#qa-monitoring-list tbody").on("click", "button[data-name='seguimientos']", function (){
        monitoring_id = $(this).data("id");
        s.ajax.reload();
        editRecord(false);
        $("#tracings").modal("show");
    });

    $("#qa-monitoring-list tbody").on("click", "i[data-name='action']", function (){
      var action_type = $(this).data("type");
      var id = $(this).data("id");
      if (action_type == "delete"){
        if (confirm("¿Esta seguro de eliminar el registro?")) {
            $.ajax({
              url: "/customers/qa-monitoring/"+id+"/delete/",
              type: 'GET',
              headers: { 'X-CSRFToken': csrftoken },
              data: {},
              dataType: 'json',
              success: function response(json) {
                if (json.valid) {
                  swal("ÉXITO", "Registro eliminado satisfactoriamente", "success");
                  e.ajax.reload();
                } else {
                  swal("ALERTA", "Error al eliminar el registro", "warning");
                }
              },
          });
        }
      }else if(action_type == "detail"){
        $.ajax({
          url: "/customers/qa-monitoring/"+id,
          type: 'GET',
          headers: { 'X-CSRFToken': csrftoken },
          data: {},
          dataType: 'json',
          success: function response(json) {
            if (json.data != ""){
              $("td[data-name='detail-contact-date']").text(json.data.date);
              $("td[data-name='detail-agent']").text(json.data.agent);
              $("td[data-name='detail-phone']").text(json.data.phone);
              $("td[data-name='detail-email']").text(json.data.email);
              $("td[data-name='detail-rut']").text(json.data.rut);
              $("td[data-name='detail-service']").text(json.data.service);
              $("td[data-name='detail-subcategory']").text(json.data.subcategory);
              $("td[data-name='detail-reason']").text(json.data.reason);
              $("td[data-name='detail-comment']").text(json.data.comment);
              $("td[data-name='detail-created-at']").text(json.data.created_at);
              $("td[data-name='detail-created-by']").text(json.data.created_by);

              $("#monitoring-detail").modal("show");
            }else{
              swal("ALERTA", "Se encontro un error al intentar procesar su solicitud.\nPor favor intente más tarde", "warning");
            }
          },
        });
      }
    });

    $('#tracing_save').click(function () {
        var url = '/customers/qa-monitoring/tracing/new/';
        var data = { 
          "solved": false, 
          "reason": $("#id_reason").val(),
          "comment": $("#id_comment").val(),
          "monitoring": monitoring_id,
          "tracing_pk": tracing_id
        }
        if (edit == true){
          url = '/customers/qa-monitoring/tracing/edit/';          
        }
        if ($('#id_reason').val().length > 0) {
          if ($('input[name="solved"]:checked').val() == 'Y'){
            data.solved = true
          }
          /* AJAX */
          $.ajax({
            url: url,
            type: 'POST',
            headers: { 'X-CSRFToken': csrftoken },
            data: data,
            dataType: 'json',
            success: function response(json) {
              if (json.valid) {
                if (edit == true){
                  swal("ÉXITO", "Registro modificado satisfactoriamente", "success");
                  editRecord(false);
                }else{
                  swal("ÉXITO", "Registro creado satisfactoriamente", "success");
                }
                $("#id_reason").val('');
                $("#id_comment").val('');
                s.ajax.reload();
              } else {
                if (edit == true){
                  swal("ALERTA", "Error al modificar el registro", "warning");
                }else{
                  swal("ALERTA", "Error al crear nuevo registro", "warning");
                }
              }
            },
          });
        } else {
          swal("ALERTA", "Debe indicar un motivo.", "warning");
        }
      });

      $("#tracings-list tbody").on("click", "i[data-name='action']", function (){
        var action_type = $(this).data("type");
        var id = $(this).data("id");
        var url = "";
        if (action_type == "edit"){
          editRecord(true);
          tracing_id = id;
          var row = $(this).closest("tr");
          var solved = row.find("td:eq(0)").text();
          if (solved == "Si"){
            $("#id_solved").prop('checked',true);
          }else{
            $("#id_solved_n").prop('checked',true);
          }
          $("#id_reason").val(row.find("td:eq(1)").text());
          $("#id_comment").val(row.find("td:eq(2)").text());
        }else{
          if (edit){
            swal("ALERTA", "Debe culminar la edición del registro actual antes de eliminar cualquier registro", "warning");
          }else{
            if (confirm("¿Esta seguro de eliminar el registro?")) {
              url = "/customers/qa-monitoring/tracing/"+id+"/delete/";
            }
          }
        }
        if (url.length > 0){
          $.ajax({
            url: url,
            type: 'GET',
            headers: { 'X-CSRFToken': csrftoken },
            data: {},
            dataType: 'json',
            success: function response(json) {
              if (json.valid) {
                swal("ÉXITO", "Registro eliminado satisfactoriamente", "success");
                s.ajax.reload();
              } else {
                swal("ALERTA", "Error al eliminar el registro", "warning");
              }
            },
          });
        }
    });

    function editRecord(activate){
      if (activate){
        edit = true;
        $("#tracing_save").text("Guardar");
        $("#tracing_cancel").removeClass("d-none");
      }else{
        edit = false;
        $("#tracing_save").text("Agregar");
        $("#tracing_cancel").addClass("d-none");
        $("#id_solved_n").prop("checked",true);
        $("#id_reason").val("");
        $("#id_comment").val("");
      }
    }

    $("#tracing_cancel").click(function(){
      editRecord(false);
    });

    $("button[data-name='export']").click(function(){
      let report_type = $(this).data("type");
      let date_from = $("#date_from").val();
      let date_to = $("#date_to").val();
      let search = $("#qa-monitoring-list_filter > label > input").val();
      let url = "/customers/qa-monitoring/export/?date_from="+date_from+"&date_to="+date_to+"&search="+search+"&report_type="+report_type;
      window.open(url, "_blank");
    });

});
    
    