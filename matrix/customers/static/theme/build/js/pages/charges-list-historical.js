$(document).ready(function(){

	var e=$("#charge-list-historical").DataTable({
	    order:[],
	    processing: true,
	    serverSide: true,
	    searchDelay: 400,
	    ajax: {
		url: "/customers/charges_applied_ajax/",
		data: function (d) {
			d.service = $('#id_service').val();
			d.status = $('#status').val();
			
		}},
	    language: {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    },
		"buttons": {
		    "print": "Imprimir"
		}
	    },
	    pageLength:10,
	    //colReorder:!0,
	    buttons:["excel","pdf","print"],
	    searching: true,
	    aLengthMenu:[[10,20,50,100],[10,20,50,100]],
	    //order:[[0,"desc"]],
	    columns: [
		{data: "reason"},
		{data: "valor"},
		{data: "created_at"},
		{data: "number_billing"},
		{data: "times_applied"},
		{data: "cleared_at"},
		{data: "invoice_applied"},
	    ],
	    columnDefs:[
		{
		    orderable: true,
		    targets:[0,1]
		},
		{
		    orderable: false,
		    targets:[2,3,4,5,6]
		},
		{
		    render: function(data, type, row) {
			return row.reason;
		    },
		    targets: 0,
		},
		{
		    render: function(data, type, row) {
				if (row.percent){
					return row.valor+"%";
				}else if (row.uf) {
					return row.valor+" UF";
				}else{
					return "$"+row.valor;
				}
		    },
		    targets: 1,
		},
		{
		    render: function(data, type, row) {
			return row.created_at;
		    },
		    targets: 2,
		},
		{
		    render: function(data, type, row) {
				if (row.permanent){
					return "Infinito";
				}else{
					return row.number_billing;
				}
		    },
		    targets: 3,
		},
		{
		    render: function(data, type, row) {
				return row.times_applied;
		    },
		    targets: 4,
		},
		{
		    render: function(data, type, row) {
				return row.cleared_at;
		    },
		    targets: 5,
		},
		{
		    render: function(data, type, row) {
				var html='';
				var obj = JSON.parse(row.invoice_applied);
				for (const p in obj){
					var html=html+' <a style="width:50%" href="../../../../../../customers/invoices/'+obj[p]['pk']+'">'+obj[p]['pk']+'</a> <br>';
				}
				return html;
		    },
		    targets: 6,
		},
	    ],
	});
    
	$('#id_service').change(function(){
		e.ajax.reload();
	})

	$('#permanent').change(function(){
		e.ajax.reload();
	})
	
	$('#status').change(function(){
		e.ajax.reload();
	})
	$('#percent').change(function(){
		e.ajax.reload();
	})
});

