$(document).ready(function(){

	var e=$("#reasons-list").DataTable({
	    order:[],
	    processing: true,
	    serverSide: true,
	    searchDelay: 400,
	    ajax: {
		url: "/customers/reasons_applied_ajax/",
		data: function (d) {
			d.kind = $('#id_kind').val();
			d.active = $('#active').val();
			d.percent = $('#percent').val();
			d.permanent = $('#permanent').val();
			d.start_date = $('#id_start_date').val();
        	d.end_date = $('#id_end_date').val();
		}},
	    language: {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    },
		"buttons": {
		    "print": "Imprimir"
		}
	    },
	    pageLength:10,
	    //colReorder:!0,
	    buttons:["excel","pdf","print"],
	    searching: true,
	    aLengthMenu:[[10,20,50,100],[10,20,50,100]],
	    //order:[[0,"desc"]],
	    columns: [
		{data: "reason"},
		{data: "valor"},
		{data: "kind"},
		{data: "number_billing"},
		{data: "times_applied"},
		{data: "customer"},
		{data: "service"},
		{data: "invoice_applied"},
	    ],
	    columnDefs:[
		{
		    orderable: true,
		    targets:[0,1]
		},
		{
		    orderable: false,
		    targets:[2,3,4,5,6,7]
		},
		{
		    render: function(data, type, row) {
			return row.reason;
		    },
		    targets: 0,
		},
		{
		    render: function(data, type, row) {
				if (row.percent){
					return row.valor+"%";
				}else if (row.uf) {
					return row.valor+" UF";
				}else{
					return "$"+row.valor;
				}
		    },
		    targets: 1,
		},
		{
		    render: function(data, type, row) {
			return row.kind;
		    },
		    targets: 2,
		},
		{
		    render: function(data, type, row) {
				if (row.permanent){
					return "Infinito";
				}else{
					return row.number_billing;
				}
		    },
		    targets: 3,
		},
		{
		    render: function(data, type, row) {
				return row.times_applied;
		    },
		    targets: 4,
		},
		{
		    render: function(data, type, row) {
				return row.customer;
		    },
		    targets: 5,
		},
		{
		    render: function(data, type, row) {
				return ' <a style="width:50%" href="../../services/'+row.service_id+'/extras/">'+row.service +'</a>';
		    },
		    targets: 6,
		},
		{
		    render: function(data, type, row) {
				var html='';
				var obj = JSON.parse(row.invoice_applied);
				for (const p in obj){
					var html=html+' <a style="width:50%" href="../../customers/invoices/'+obj[p]['pk']+'">'+obj[p]['pk']+'</a> <br>';
				}
				return html;
		    },
		    targets: 7,
		},
	    ],
	});
    
	$('#id_kind').change(function(){
		e.ajax.reload();
	})

	$('#permanent').change(function(){
		e.ajax.reload();
	})
	
	$('#active').change(function(){
		e.ajax.reload();
	})
	$('#percent').change(function(){
		e.ajax.reload();
	})
	$('#id_start_date').change(function(){
		e.ajax.reload();
	})
	
	$('#id_end_date').change(function(){
		e.ajax.reload();
	})
});

