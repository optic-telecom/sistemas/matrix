
$(document).ready(function(){
    var e=$("#cartola-movement-list").DataTable({        
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: window.location.pathname+'ajax',
        data: function (d) {
            d.start_date = $('#cartola_movement_start_date').val();
            d.end_date = $('#cartola_movement_end_date').val();
            d.kind = $('#cartola_movement_kind').val();
            d.conciliado = $('#cartola_movement_conciliado').val();
        }},
        language: {
        "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        order:[[0,"desc"]],
        columns: [
            {data: "date"},
            {data: "sucursal"},
            {data: "number_document"},
            {data: "description"},
            {data: "total"},
            {data: "kind"},
            {data: "saldo_diario"},
            {data: "posible_pago"},
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData.conciliado){
                $('td', nRow).css('background-color', '#f0f0f0');
            } else if (aData.posible_pago) {
                $('td', nRow).css('background-color', '#49b6d6');
            } else {
                $('td', nRow).css('background-color', '#f59c1a');
            }
            
        },
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2,3,4,5,6,7]
        },
        {
            render: function(data, type, row) {
            return row.symbol+" " +row.total;
            },
            targets: 4,
        },
        {
            render: function(data, type, row) {
            return row.symbol+" " +row.saldo_diario;
            },
            targets: 6,
        },
        {
            render: function(data, type, row) {
                if (row.payment){
                    return '<a href="/customers/payments/'+row.pk_payment+'/">'+row.payment +'</a>';
                } else if (row.payment_provider){
                    return '<a href="/providers/payments/'+row.pk_payment_provider+'/">'+row.payment_provider +'</a>';
                } else {
                    if (row.kind_number==2){
                        return '<a href="/customers/bank-acccounts/cartola/movements/'+row.pk+'/" class="btn btn-white">Asociar pago manualmente</a>';
                    } else{
                        return '';
                    }
                    
                }
        
            },
            targets: 7,
        },

    ]
    });

    $('#cartola_movement_start_date').change(function(){
        e.ajax.reload();
    })
    
    $('#cartola_movement_end_date').change(function(){
        e.ajax.reload();
    })

    $('#cartola_movement_kind').change(function(){
        e.ajax.reload();
    })
    $('#cartola_movement_conciliado').change(function(){
        e.ajax.reload();
    })
    }
    );