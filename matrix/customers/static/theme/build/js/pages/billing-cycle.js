$(document).ready(function () {
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  function delay(x) {
    setTimeout(function () {
      x.ajax.reload();
    }, 2000);
  }
  var csrftoken = getCookie('csrftoken');
  /* Date's var declaration */
  var now = new Date();
  var month = now.getMonth() + 1;
  var day = now.getDate();
  var today = new Date(now.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day);
  /* AJAX */
  var e = $("#cycle-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax/",
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "print": "Imprimir"
      }
    },

    pageLength: 10,
    searching: false,
    aLengthMenu: [
      [10, 20, 30, 40, 50],
      [10, 20, 30, 40, 50]
    ],
    order: [
      [0, "desc"]
    ],
    columns: [{
        data: "id"
      },
      {
        data: "interval"
      }, {
        data: "operator"
      }
    ],
    columnDefs: [{
        orderable: true,
        targets: [0]

      },
      {
        orderable: false,
        targets: [1, 2,3]

      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.id + '</div>';
        },
        targets: 0,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center" name="interval">' + row.interval + '</div>';
        },
        targets: 1,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center" name="interval">' + row.operator + '</div>';
        },
        targets: 2,
      },
      {
        render: function (data, type, row) {
          var dateStart = new Date(row.interval.split(" - ")[0]);
          var dateEnd = new Date(row.interval.split(" - ")[1]);
          if (today >= dateStart && today <= dateEnd) {
            return '<div style="text-align:center" id="' + row.id + '"><a title="Servicios por facturar" href="forbilling/' + row.id + '"><button type="button" class="btn btn-primary"><i class="fa fa-exclamation-circle"></i> Por facturar</button></a> <a title="Documentos procesados" href="processed/' + row.id + '"><button type="button" class="btn btn-primary"><i class="fa fa-cogs"></i> Procesados</button></a> </div>';
          } else {
            return '<div style="text-align:center" id="' + row.id + '"><a title="Documentos procesados" href="processed/' + row.id + '"><button type="button" class="btn btn-primary"><i class="fa fa-cogs"></i> Procesados</button></a> </div>';
          }
        },
        targets: 3,
      },
    ],
  });
  $.ajax({
    url: 'ajax/',
    type: 'GET',
    dataType: 'json',
    success: function response(json) {
      if (json.data.length > 0) {
        var dateEnd = new Date(json.data[0]["interval"].split(" - ")[1]);
        if (((day >= 20 && day <= 30) || ((day == (1 || 2)) && month == 3)) && (today >= dateEnd)) {
          var nd = new Date();
          var nmonth = nd.getMonth() + 1;
          var nday = nd.getDate();
          var noutput = nd.getFullYear() + '-' +
            (('' + nmonth).length < 2 ? '0' : '') + nmonth + '-' +
            (('' + nday).length < 2 ? '0' : '') + nday;
          $.ajax({
            url: 'ajax2/',
            type: 'POST',
            headers: {
              'X-CSRFToken': csrftoken
            },
            data: {
              "data": noutput
            },
            dataType: 'json',
          });
          delay(e);
        } else {
          $('#activeCycle').html('');
        }
      } else {
        $('#activeCycle').html('<p><i class="fa fa-exclamation-circle"></i> El ciclo se creará automáticamente el día 20 del mes actual</p>');
        if ((day >= 20 && day <= 30) || ((day == (1 || 2)) && month == 3)) {
          var nd = new Date();
          var nmonth = nd.getMonth() + 1;
          var nday = nd.getDate();
          var noutput = nd.getFullYear() + '-' +
            (('' + nmonth).length < 2 ? '0' : '') + nmonth + '-' +
            (('' + nday).length < 2 ? '0' : '') + nday;
          $.ajax({
            url: 'ajax2/',
            type: 'POST',
            headers: {
              'X-CSRFToken': csrftoken
            },
            data: {
              "data": noutput
            },
            dataType: 'json',
          });
          delay(e);
        }
      }
    },
  });
});