
$(document).ready(function(){

var e=$("#service-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: "/services/ajax/?node="+$("#node-pk").val(),
    language: {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
	"buttons": {
	    "print": "Imprimir"
	}
    },
    
    pageLength:10,
    //colReorder:!0,
    buttons:["excel","pdf","print"],
    searching: false,
    aLengthMenu:[[10,20,50],[10,20,50]],
    //order:[[0,"desc"]],
    columns: [
	{data: "number"},
	{data: "address"},
	{data: "status"},
    ],
    columnDefs:[
	{
	    orderable: true,
	    targets:[0,2]
	},
	{
	    orderable: false,
	    targets:[1]
	},
	{
	    render: function(data, type, row) {
		return '<a href="/services/'+row.DT_RowId+'/">'+data+'</a>';
	    },
	    targets: 0,
	},
	{
	    render: function(data, type, row) {
		return '<a href="/services/'+row.DT_RowId+'/edit/">'+row.number+'</a>';
	    },
	    targets: 1,
	},
	{
	    render: function(data, type, row) {
		if (data === 'activo') {
		    var label = 'success';
		} else if (data === 'por retirar') {
		    var label = 'warning';
		} else if (data === 'retirado') {
		    var label = 'danger';
		} else {
		    var label = 'default';
		}

		return '<span class="label label-'+label+'">'+data+'</span>';
	    },
	    targets: 2,
	},
    ],
});

}
);

