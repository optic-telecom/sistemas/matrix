
$(document).ready(function () {
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
  function delay(x) {
    setTimeout(function () { x.ajax.reload(); }, 2000);
  }
  var csrftoken = getCookie('csrftoken');
  /* var e = $("#batch").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax/",
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "new": "Nuevo"
      }
    },
    pageLength: 10,
    searching: false,
    aLengthMenu: [[10, 20, 100], [10, 20, 100]],
    order: [[0, "desc"]],
    columns: [
      { data: "id" },
      { data: "kind" },
      { data: "created" },
      { data: "expiration" },
      { data: "stock" },
    ],
    columnDefs: [
      {
        orderable: true,
        targets: [0, 1, 4]
      },
      {
        orderable: false,
        targets: [2, 3]
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.id + '</div>';
        },
        targets: 0,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.kind + '</div>';
        },
        targets: 1,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.created + '</div>';
        },
        targets: 2,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.expiration + '</div>';
        },
        targets: 3,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center">' + row.stock + '</div>';
        },
        targets: 4,
      },
    ],
  });
  $('#createBatch').click(function () {
    var first = $('#first').val();
    var last = $('#last').val();
    var type = $('#type').val();
    var expiration = $("#expiration").val();
    if (expiration.length <= 0) {
      expiration = false
    }
    if (first.length > 0 & last.length > 0 & type.length > 0) {
      $.ajax({
        url: 'ajax2/',
        type: 'POST',
        headers: { 'X-CSRFToken': csrftoken },
        data: {
          "first": first,
          "last": last,
          "type": type,
          "expiration": expiration,
        },
        dataType: 'json',
      });
      delay(e);
      swal("ÉXITO", "Se ha registrado el lote.", "success");
      $('#formFolio').modal('toggle');
      $("#formBatch")[0].reset();
    } else {
      console.log("INGRESE LOS DATOS FALTANTES");
      swal("ERROR", "Debe llenar los campos faltantes.", "warning");
    }
  }); */
});

