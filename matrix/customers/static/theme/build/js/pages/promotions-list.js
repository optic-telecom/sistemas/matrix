
$(document).ready(function(){

    var e=$("#promotions-list").DataTable({
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: "/promotions/ajax/",
        data: function (d) {
            d.operator = $('#id_operator').val();
            d.active = $('#active').val();
            d.permanent= $('#permanent').val();
            d.start_date = $('#id_start_date').val();
            d.end_date = $('#id_end_date').val();
        }},
        language: {
        "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        //colReorder:!0,
        buttons:["excel","pdf","print"],
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        order:[[0,"desc"]],
        columns: [
            {data: "name"},
            {data: "description"},
            {data: "plan"},
            {data: "periods"},
            {data: "initial_date"},
            {data: "end_date"},
            {data: "edit"},
            //{data: "delete"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2,3]
        },
        {
            orderable: false,
            targets:[4,5,6]
        },
        {
            render: function(data, type, row) {
                return '<a href="/promotions/'+row.pk+'" class="btn btn-success">Editar</a>';
            },
            targets: 6,
        },

        ],
    });
    
    $('#id_operator').change(function(){
        e.ajax.reload();
    })
    $('#active').change(function(){
        e.ajax.reload();
    })
    $('#permanent').change(function(){
        e.ajax.reload();
    })

    $('#id_start_date').change(function(){
        e.ajax.reload();
    })

    $('#id_end_date').change(function(){
        e.ajax.reload();
    })
    }
    );