
$(document).ready(function(){
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
            }
          }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    var e=$("#qa-indicator-list").DataTable({
        order:[],
        processing: true,
        serverSide: true,
        searchDelay: 400,
        ajax: {
        url: "/qa-indicators/ajax/",
        data: function (d) {
        }},
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "print": "Imprimir"
        }
        },
        
        pageLength:10,
        searching: true,
        aLengthMenu:[[10,20,50],[10,20,50]],
        columns: [
        {data: "DT_RowId"},        
        {data: "description"},
        {data: "parent"},
        ],
        columnDefs:[
        {
            orderable: true,
            targets:[0,1,2]
        },
        {
            render: function(data, type, row) {
            return '<a href="/qa-indicators/'+row.DT_RowId+'/">'+data+'</a>';
            },
            targets: 0,
        },
        {
            render: function(data, type, row) {
                return '<div class="text-center"><button class="btn btn-danger" data-id="'+row.DT_RowId+'">Eliminar</button></div>'
            },
            targets: 3
        }
    
        ],
    });

    $("#qa-indicator-list tbody").on("click", "button", function(){
        var id = $(this).data("id");
        if (confirm("¿Esta seguro de eliminar el registro?")) {
            $.ajax({
              url: "/qa-indicators/"+id+"/delete/",
              type: 'GET',
              headers: { 'X-CSRFToken': csrftoken },
              data: {},
              dataType: 'json',
              success: function response(json) {
                if (json.error == "") {
                  swal("ÉXITO", "Registro eliminado satisfactoriamente", "success");
                  e.ajax.reload();
                } else {
                  swal("ALERTA", json.error);
                }
              },
          });
        }
    });
    
    
    }
    );
    
    
