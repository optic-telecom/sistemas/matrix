$(document).ready(function () {
  var e = $("#service-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax/",
      data: function (d) {
        d.operator = $('#id_operator').val();
        d.document_type = $('#id_document').val();
      }
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "print": "Imprimir"
      }
    },
    pageLength: 10,
    //colReorder:!0,
    buttons: ["excel", "pdf", "print"],
    searching: true,
    aLengthMenu: [
      [10, 20, 50, 100, 200],
      [10, 20, 50, 100, 200]
    ],
    order: [
      [0, "desc"]
    ],
    columns: [{
        data: "number"
      },
      {
        data: "customer"
      },
      {
        data: "plan"
      },
      {
        data: "operator"
      },
    ],
    columnDefs: [{
        orderable: true,
        targets: [0, 2, 3]

      },
      {
        orderable: false,
        targets: [1]
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center"><a href="/services/' + row.pk + '/billing/">' + row.number + '</a></div>';
        },
        targets: 0,
      },
    ],
  });
  $('#id_operator').change(function () {
    e.ajax.reload();
  })
  $('#id_document').change(function () {
    e.ajax.reload();
  })
});