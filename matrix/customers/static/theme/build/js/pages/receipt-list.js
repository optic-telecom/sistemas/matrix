$(document).ready(function () {
    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
          }
        }
      }
      return cookieValue;
    }
  
    function delay(x) {
      setTimeout(function () {
        x.ajax.reload();
      }, 2000);
    }
    var csrftoken = getCookie('csrftoken');
    /* Date's var declaration */
    var now = new Date();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var today = new Date(now.getFullYear() + '-' +
      (month < 10 ? '0' : '') + month + '-' +
      (day < 10 ? '0' : '') + day);
    /* AJAX */
    var e = $("#receipt-list").DataTable({
      processing: true,
      serverSide: true,
      searchDelay: 400,
      ajax: {
        url: "ajax/",
      },
      language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst": "Primero",
          "sLast": "Último",
          "sNext": "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
          "print": "Imprimir"
        }
      },
  
      pageLength: 10,
      searching: false,
      aLengthMenu: [
        [10, 20, 30, 40, 50],
        [10, 20, 30, 40, 50]
      ],
      order: [
        [0, "desc"]
      ],
      columns: [
        {
          data: "pk"
        },
        {
            data: "worker"
        },
        {
            data: "month"
        },
        {
            data: "year"
        },
        {
            data: "amount"
        },
        {
            data: "currency"
        },
        {
            data: "exchange_rate"
        },
        {
            data: "issue_date"
        },
      ],
      columnDefs: [{
          orderable: true,
          targets: [0,1,2,3,4,5,6,7]
  
        },
        {
          render: function(data, type, row) {
          return '<a href="/hr/receipts/'+row.pk+'">'+row.pk+'</a>';
          },
          targets: 0,
        },
        {
          render: function (data, type, row) {
            return '<div style="text-align:center">' + row.worker + '</div>';
          },
          targets: 1,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.month + '</div>';
            },
            targets: 2,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.year + '</div>';
            },
            targets: 3,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.amount + '</div>';
            },
            targets: 4,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.currency + '</div>';
            },
            targets: 5,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.exchange_rate + '</div>';
            },
            targets: 6,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center" name="interval">' + row.issue_date + '</div>';
            },
            targets: 7,
        },
        {
          render: function(data, type, row) {
            html = '<div class="text-center"><a href="/hr/receipts/'+row.pk+'/delete/" class="btn btn-danger" onclick="return confirm(\'¿Seguro?\');" style="text-align: center"><i class="fa fa-times" ></i></a></div>';
            return html;
          },
          targets: 8
        }
      ],
    });
  });
