$(document).ready(function () {
    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
          }
        }
      }
      return cookieValue;
    }
  
    function delay(x) {
      setTimeout(function () {
        x.ajax.reload();
      }, 2000);
    }
    var csrftoken = getCookie('csrftoken');
    /* Date's var declaration */
    var now = new Date();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var today = new Date(now.getFullYear() + '-' +
      (month < 10 ? '0' : '') + month + '-' +
      (day < 10 ? '0' : '') + day);
    /* AJAX */
    var e = $("#report-list").DataTable({
      processing: true,
      serverSide: true,
      searchDelay: 400,
      ajax: {
        url: "ajax/",
      },
      language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst": "Primero",
          "sLast": "Último",
          "sNext": "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
          "print": "Imprimir"
        }
      },
  
      pageLength: 10,
      searching: false,
      aLengthMenu: [
        [10, 20, 30, 40, 50],
        [10, 20, 30, 40, 50]
      ],
      order: [
        [0, "desc"]
      ],
      columns: [{
          data: "pk"
        },
        {
          data: "name"
        },
        {
          data: "created_at"
        },
      ],
      columnDefs: [{
          orderable: true,
          targets: [0,1,2,3]
  
        },
        {
          render: function (data, type, row) {
            return '<div style="text-align:center">' + row.pk + '</div>';
          },
          targets: 0,
        },
        {
            render: function (data, type, row) {
              return '<div style="text-align:center">' + row.name + '</div>';
            },
            targets: 1,
          },
        {
          render: function (data, type, row) {
            return '<div style="text-align:center" name="interval">' + row.created_at + '</div>';
          },
          targets: 2,
        },
        {
          render: function(data, type, row) {
            html = '<div class="text-center"><a href="/report_list/'+row.pk+'/delete/" class="btn btn-danger" style="text-align: center">Delete</a></div>';
            return html;
          },
          targets: 3,
        },
      ],
    });
  });