
$(document).ready(function(){

var e=$("#call-list").DataTable({
    order:[],
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
	url: "/customers/register-support-call/ajax/",
	data: function (d) {
	    d.agent = $('#id_agent').val();
	    d.subject = $('#id_subject').val();
	}},
    language: {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
	"buttons": {
	    "print": "Imprimir"
	}
    },
    
    pageLength:10,
    //colReorder:!0,
    buttons:["excel","pdf","print"],
    searching: true,
    aLengthMenu:[[10,20,50],[10,20,50]],
    //order:[[0,"desc"]],
    columns: [
	{data: "DT_RowId"},
	{data: "answered_by"},
	{data: "subject"},
	{data: "notes"},
	{data: "service"},
	{data: "date"},
    ],
    columnDefs:[
	{
	    orderable: true,
	    targets:[0,1,2,3]
	},
	{
	    orderable: false,
	    targets:[4,5]
	},
	{
	    render: function(data, type, row) {
		if (row.has_call) {
		    return '<a href="/customers/register-support-call/'+row.DT_RowId+'/">'+data+'</a> <i class="ti-headphone-alt"></i>';
		} else {
		    return '<a href="/customers/register-support-call/'+row.DT_RowId+'/">'+data+'</a>';
		}
	    },
	    targets: 0,
	},
	{
	    render: function(data, type, row) {
		if (data) {
		    return '<a href="/services/'+data+'/calls/">'+data+'</a>';
		} else {
		    return 'Sin contrato';
		}
	    },
	    targets: 4,
	}
    ],
});


$('#id_agent').change(function(){
    e.ajax.reload();
});

$('#id_subject').change(function(){
    e.ajax.reload();
});

}
);

