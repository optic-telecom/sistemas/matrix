$(document).ready(function () {

  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  function delay(x) {
    setTimeout(function () {
      x.ajax.reload();
    }, 2000);
  }

  // var csrftoken = getCookie('csrftoken');

  /* AJAX */
  var e = $("#duration-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
      url: "ajax/",
      data: function (d) {
        d.status = $('#status').val();
        d.interval = $('#interval').val();
      },
    },
    language: {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
        "print": "Imprimir"
      }
    },

    pageLength: 10,
    searching: false,
    aLengthMenu: [
      [10, 20, 30, 40, 50],
      [10, 20, 30, 40, 50]
    ],
    order: [
      [0, "desc"]
    ],
    columns: [{
        data: "customer"
      },
      {
        data: "number"
      },
      {
        data: "status"
      },
      {
        data: "active_intervals"
      },
      {
        data: "active_days"
      },
      {
        data: "active_total"
      },
    ],
    columnDefs: [{
        orderable: true,
        targets: [0, 1, 2]

      },
      {
        orderable: false,
        targets: [3, 4, 5]

      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center"><a title="Reporte de documentos emitidos" target="_blank" href="/customers/' + row.customer + '/"><button type="button" class="btn btn-link">' + row.customer + '</button></a> </div>';
        },
        targets: 0,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center"><a title="Reporte de documentos emitidos" target="_blank" href="/services/' + row.service_id + '/"><button type="button" class="btn btn-link">' + row.number + '</button></a> </div>';
        },
        targets: 1,
      },
      {
        render: function (data, type, row) {
          const servicios = {
            "1": "Activo",
            "2": "Por retirar",
            "3": "Retirado",
            "4": "No instalado",
            "5": "Canje",
            "6": "Instalación rechazada",
            "7": "Moroso",
            "8": "Cambio de Titular",
            "9": "Descartado",
            "10": "Baja Temporal",
            "11": "Perdido, no se pudo retirar",
          }

          showService = servicios[row.status.toString()]

          return '<div style="text-align:center">' + showService + '</div>';
        },
        targets: 2,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center;"><center><p style="max-width:150px;">' + row.active_intervals + '</p></center></div>';
        },
        targets: 3,
      },
      {
        render: function (data, type, row) {
          return '<div style="text-align:center;"><center><p>' + row.active_days + '</p></center></div>';
        },
        targets: 4,
      },
      {
        render: function (data, type, row) {
          return '<div><center><p>' + row.active_total + '</p></center></div>';
        },
        targets: 5,
      },
    ],
  });

  $("#status").change(function () {
    e.ajax.reload();
  });

  $("#interval").change(function () {
    e.ajax.reload();
  });

  $("#export").click(function () {
    console.log("click");
    if ($('#status').val()) {
      var status = $('#status').val();
    } else {
      var status = 0;
    }

    if ($('#interval').val()) {
      var interval = $('#interval').val();
    } else {
      var interval = 0;
    }

    window.open("export/" + status + "/" + interval + "/", "_blank");

    /* $.ajax({
      url: 'export/',
      data: function (d) {
        d.status = $('#status').val();
        d.interval = $('#interval').val();
      },
      success: function response(json) {
        window.open("data:application/vnd.ms-excel;base64," + json, "_blank");
      },
    }); */
  });

});