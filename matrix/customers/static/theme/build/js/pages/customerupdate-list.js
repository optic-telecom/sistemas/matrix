$(document).ready(function(){

	var e=$("#customer-list").DataTable({
		order:[],
		//scrollX: true,
		processing: true,
	    serverSide: true,
	    searchDelay: 400,
	    ajax: {
		url: "/customersupdate/ajax/",
		data: function (d) {
		    d.update = $('#id_update').val();
        	d.validated = $('#id_validated').val();
		}},
	    language: {
	    "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		},
		"buttons": {
		    "print": "Imprimir"
		}
	    },
	    
	    pageLength:10,
	    //colReorder:!0,
	    buttons:["excel","pdf","print"],
	    searching: true,
	    aLengthMenu:[[10,20,50,100],[10,20,50,100]],
	    //order:[[0,"desc"]],
	    columns: [
		{data: "rut"},
		{data: "name"},
		{data: "email"},
		{data: "is_validated"},

	    ],
	    columnDefs:[
		{
		    orderable: true,
		    targets:[]
		},
		{
		    orderable: false,
		    targets:[0,1,2,3]
		},
		{
		    render: function(data, type, row) {
			return '<a href="/customers/'+row.DT_RowId+'/">'+row.rut+'</a>';
		    },
		    targets: 0,
		},
		{
		    render: function(data, type, row) {
			return '<a class="btn btn-primary" href="/customers/update/'+row.DT_RowId+'/">Detalles</a>';
		    },
		    targets: 1,
		},
		{
		    render: function(data, type, row) {
			return row.update_page;
		    },
		    targets: 2,
		},
		{
		    render: function(data, type, row) {
			return row.is_validated;
		    },
		    targets: 3,
		},
		
		
	    ],
	});


	$('#id_update').change(function(){
		e.ajax.reload();
	})
	
	$('#id_validated').change(function(){
		e.ajax.reload();
	})	     

});

