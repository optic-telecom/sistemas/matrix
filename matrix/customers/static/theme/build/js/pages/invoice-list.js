
$(document).ready(function(){

var e=$("#invoice-list").DataTable({
    processing: true,
    serverSide: true,
    searchDelay: 400,
    ajax: {
	url: "/customers/invoices/ajax/",
	data: function (d) {
	    d.kind = $('#id_kind').val();
        d.start_date = $('#id_start_date').val();
        d.end_date = $('#id_end_date').val();
        d.service = $('#id_service').val();
        d.paid_on = $('#id_paid_on').val();
        d.operator = $('#id_operator').val();
	}},
    language: {
    "sProcessing":     '<div class="fa-4x"><i class="fas fa-sync fa-spin"></i></div>',
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
	"buttons": {
	    "print": "Imprimir"
	}
    },
    
    pageLength:10,
    //colReorder:!0,
    buttons:["excel","pdf","print"],
    searching: true,
    aLengthMenu:[[10,20,50],[10,20,50]],
    order:[[0,"desc"]],
    columns: [
	{data: "pk"},
	{data: "created_at"},
    {data: "comment"},
	{data: "kind"},
    {data: "total"},
    {data: "paid"},
	{data: "customer_name"},
    {data: "operator"},
    {data: "customer_operator"},
    {data: "paid"},
    ],
    columnDefs:[
	{
        orderable: true,
        targets:[0,1]
        
    },
    {
        orderable: false,
        targets:[2,3,4,5,6,7,8,9],
        
    },
    {
	    render: function(data, type, row) {
		return '<a href="/customers/invoices/'+row.pk+'">'+row.pk+'</a>';
	    },
	    targets: 0,
    },
    {
	    render: function(data, type, row) {
		return row.symbol+" " +row.total;
	    },
	    targets: 4,
    },
    {
        render: function(data, type, row) {
            if (row.paid=="Sí") {
                html = '<a href="/customers/invoices/'+row.pk+'/mark-as-unpaid/" class="btn btn-danger">Marcar como no pagada</a>';
            } else {
                html = '<a href="/customers/invoices/'+row.pk+'/mark-as-paid/" class="btn btn-success">Marcar como pagada</a>';
            }
            return html;
        },
        targets: 9,
    },
    ],
});


$('#id_kind').change(function(){
    e.ajax.reload();
})

$('#id_paid_on').change(function(){
    e.ajax.reload();
})
$('#id_operator').change(function(){
    e.ajax.reload();
})

$('#id_service').change(function(){
    e.ajax.reload();
})

$('#id_start_date').change(function(){
    e.ajax.reload();
})

$('#id_end_date').change(function(){
    e.ajax.reload();
})


}
);

$('#export-button').click(function() {
    var kind = $('#id_kind').val();
    var start_date = $('#id_start_date').val();
    var end_date = $('#id_end_date').val();
    var service = $('#id_service').val();
    var paid_on = $('#id_paid_on').val();
    var operator = $('#id_operator').val();
    var search_query = $('#customer-list_filter > label > input').val();
    var url_open = "/customers/invoices/excel/?start_date="+start_date+"&paid_on="+paid_on+
                   "&end_date="+end_date+"&search_query="+search_query+
                   "&kind="+kind+"&operator="+operator+"&service="+service;
    window.open(url_open, "_blank");
});