$('.col-sm-9').has('#id_notes').each(function(i) {
    a = '<span style="color: #999; opacity: 1; display:block; margin-top:20px;">Vista previa de las notas:</span>';
    b = '<div id="preview" style="display: inline-block; margin-top:7px; height: 200px; color: #999; opacity: 1; "></div><br>';
    $(this).append($(a + b));
    if (document.getElementById("id_notes")) {
        function Editor(input, preview) {
            this.update = function () {
                preview.innerHTML = markdown.toHTML(input.value);
            };
            input.editor = this;
            this.update();
        }
        var IdElement = function (id) { return document.getElementById(id); };
        new Editor(IdElement("id_notes"), IdElement("preview"));
    }
    $("#id_notes").attr("oninput", "this.editor.update()");
});