/* 
Script para el manejo de clases javascript de manera gÃ©nerica

*/

var SPA = function(title="",color="success"){
    /*
        Clase Javascript para el manejo del renderizado con modales
    */

    this.title=title; // Define el titulo ppal de la instancia 
    this.color=color;//Define el color de el como buttons o headers en caso de modales

    this.headers = {'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    this.modalId = '#modal_create';
    this.select2 = [];
    
    this.Input = function(title,idInput,type="text",value=null){

        return(`<div class="form-group row m-b-15">
                    <label class="col-md-3 col-sm-3 col-form-label" for="${idInput}">${title}</label>
                        <div class="col-md-9 col-sm-9">
                            <input 
                                name="${idInput}" 
                                id="${idInput}" 
                                type="${type}"
                                class="form-control form-control-sm"
                                placeholder="${title}" 
                                value="${value?value:''}"/>
                            <span id="${idInput}_error" class="text-danger"></span>
                        </div>
                </div>`);

    }

    this.Select = function(title,idSelect){
        return(`<div class="form-group row m-b-15">
                    <label class="col-md-3 col-sm-3 col-form-label" for="${idSelect}">${title}</label>
                    <div class="col-md-9 col-sm-9">
                        <select id="${idSelect}" 
                                class="form-control form-control-sm" 
                                width="100%">
                                <option value="">${title}</option>
                        </select>
                    <span id="${idSelect}_error" class="text-danger"></span>
                    </div>
                </div>`);
    }

    this.TextArea = function(title,idInput,value=null){
        return(`<div class="form-group row m-b-15">
                    <label class="col-md-3 col-sm-3 col-form-label" for="${idInput}">${title}</label>
                        <div class="col-md-9 col-sm-9">
                            <textarea 
                                class="form-control form-control-sm" 
                                id="${idInput}" 
                                name="${idInput}"
                                rows="3">
                                
                                ${value?value:''}
                            </textarea>
                            <span id="${idInput}_error" class="text-danger"></span>
                        </div>
                </div>`);
    }

    this.BtnAction=function(title="Cerrar",action='close',funcion=null,id=0){
        var color = null;
        switch(action){
            case 'function':
                action = `onclick="return ${funcion}(${id})"`;
                color = this.color;
            break;

            default:
                action = `data-dismiss="modal"`;
                color = 'white';
            break;
        }

        return (`<a href="javascript:;" class="btn btn-${color}" ${action} >${title}</a>`);
    }

    this.modalBody = function(content=null,botones=null){
    
        return(`<div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-${this.color}">
                            <h4 class="modal-title">${title}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã—</button>
                        </div>
                        <div class="modal-body">
                            <div id="loader_modal"></div>
                        
                            ${content?content:''}
                        </div>
                        <div class="modal-footer">

                            ${botones?botones:''}
        
                        </div>
                    </div>
                </div>`);
    
    }

    // Tablas para mostrar un detail
    this.RowTable = function(key,value){
        return(`<tr><th>${key}</th><td>${value?value:''}</td>`);
    }

    this.TableBody = function(title,content){

        return (`<div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-${this.color}">
                            <h4 class="modal-title">${this.title}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã—</button>
                        </div>
                        <div class="modal-body">
                            <div id="loader_modal"></div>
                        
                            <table class="table table-border table-hover">
                                <thead><tr><th colspan="2" class="text-center">${title}</th></tr></thead>
                                <tbody>
                                    ${content}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>`);
    } 
    // selector ajax para los autocompletes
    this.Select2Ajax=function(
                                path,//ruta del api
                                selectorId,// id del selector
                                placeholder="",
                                multiple=false,//indica si el selector es multiple o no
                                selected=false,//Bandera que indica si estoy consultando un obj pre existente o no
                                value=null // valor del objeto pre existente para este campo
                                )
    {
        
        var Select2 = $(`#${selectorId}`).empty().select2({
            width: '100%',
            minimumInputLength: 1,
            multiple:multiple,
            placeholder:placeholder,
            dropdownParent: $(this.modalId),
            ajax: {
                url: path,
                method: 'GET',
                headers:this.headers,
                data: function(params) {
                    var query = {
                        q: params.term,
                    };
    
                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },    
            },
        })
        
        if(selected){
            for(i=0;i<value.length;i++)
            {
                var option = new Option(value[i].text, value[i].id, true, true);
                Select2.append(option).trigger('change');
            }
        }
    
      }

    this.Select2AjaxWithoutModal=function(
                                path,//ruta del api
                                selectorId,// id del selector
                                placeholder="",
                                minimumInputLength=1,
                                multiple=false,//indica si el selector es multiple o no
                                selected=false,//Bandera que indica si estoy consultando un obj pre existente o no
                                value=null // valor del objeto pre existente para este campo
                                )
    {
    
        var Select2 = $(`#${selectorId}`).empty().select2({
            width: '100%',
            minimumInputLength: minimumInputLength,
            multiple:multiple,
            placeholder:placeholder,
            allowClear: true,
            closeOnSelect: true,
            ajax: {
            url: path,
            method: 'GET',
            headers:this.headers,

            data: function(params) {
                var query = {
                            q: params.term,
                            };
                            
                // Query parameters will be ?search=[term]&type=public
                    return query;
                },   
            },
            
        })
        
        if(selected){
            for(i=0;i<value.length;i++)
            {
                var option = new Option(value[i].text, value[i].id, true, true);
                Select2.append(option).trigger('change');
            }
        }
        
    }

}


Gritter = function(msj,title="InformaciÃ³n Importante"){
    // popup con msj de alerta
    $.gritter.add({
      title: title,
      text: msj,
      sticky: false,
      time: '5000',
      class_name:'gritter-success gritter-light' 
      // 'my-sticky-class'
    });
}


ValidatorField = function(data,idInput,titleField){
    // vÃ¡lida que los campos 
    if(data[idInput] == ''||data[idInput]==null)
    {
      var title = `El campo ${titleField} no puede estar vacio, 
                    este es un dato requerido para proseguir con esta acciÃ³n.`;
      Gritter(title);
      $(`#${idInput}_error`).empty().text(title).fadeOut(5000);
      return true;
    }
    else {
        return false;
    }
    
}

dt_language =(palabraClave='registros')=>{
    return {
      sLengthMenu:     `Mostrar _MENU_ ${palabraClave}`,
      sZeroRecords:    `No se encontraron resultados`,
      sEmptyTable:     `NingÃºn dato disponible en esta tabla`,
      sInfo:           `Mostrando ${palabraClave} del _START_ al _END_ de un total de _TOTAL_ ${palabraClave}`,
      sInfoEmpty:      `Mostrando ${palabraClave} del 0 al 0 de un total de 0 ${palabraClave}`,
      sInfoFiltered:   `(filtrado de un total de _MAX_ ${palabraClave})`,
      sInfoPostFix:    "",
      sSearch:         `Buscar:`,
      sUrl:            "",
      sInfoThousands:  ",",
      sLoadingRecords: `Cargando...`,
      sProcessing: `Cargando...`,
      oPaginate: {
        sFirst:    "Primero",
        sLast:     "Ãšltimo",
        sNext:     "Siguiente",
        sPrevious: "Anterior"
      },

      oAria: {
        sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
        sSortDescending: ": Activar para ordenar la columna de manera descendente"
      }
    };
} 
