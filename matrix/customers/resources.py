from import_export import resources
from .models import *

class ContactResource(resources.ModelResource):

    class Meta:
        model = Contact

class CustomerResource(resources.ModelResource):

    class Meta:
        model = Customer

class ServiceResource(resources.ModelResource):

    class Meta:
        model = Service

class CallResource(resources.ModelResource):

    class Meta:
        model = Call

class CallQAResource(resources.ModelResource):

    class Meta:
        model = CallQA

class QAIndicatorResource(resources.ModelResource):

    class Meta:
        model = QAIndicator

class QAMonitoringResource(resources.ModelResource):

    class Meta:
        model = QAMonitoring

class QATracingResource(resources.ModelResource):

    class Meta:
        model = QATracing

class QAChannelResource(resources.ModelResource):

    class Meta:
        model = QAChannel

class PlanResource(resources.ModelResource):

    class Meta:
        model = Plan

class InvoiceResource(resources.ModelResource):

    class Meta:
        model = Invoice

class InvoiceItemResource(resources.ModelResource):

    class Meta:
        model = InvoiceItem


class PaymentResource(resources.ModelResource):

    class Meta:
        model = Payment

class PayorRutResource(resources.ModelResource):

    class Meta:
        model = PayorRut

class SellerResource(resources.ModelResource):

    class Meta:
        model = Seller

class FinancialRecordResource(resources.ModelResource):

    class Meta:
        model = FinancialRecord


class FinancialRecordServiceResource(resources.ModelResource):

    class Meta:
        model = FinancialRecordService

class BalanceResource(resources.ModelResource):

    class Meta:
        model = Balance

class BalanceServiceResource(resources.ModelResource):

    class Meta:
        model = BalanceService

class PromoResource(resources.ModelResource):

    class Meta:
        model = Promotions

class NetworkStatusChangeResource(resources.ModelResource):

    class Meta:
        model = NetworkStatusChange

class EmailResource(resources.ModelResource):

    class Meta:
        model = Email

class ProspectResource(resources.ModelResource):

    class Meta:
        model = Prospect

class PlanOrderResource(resources.ModelResource):

    class Meta:
        model = PlanOrder

class RegionResource(resources.ModelResource):

    class Meta:
        model = Region

class ComunaResource(resources.ModelResource):

    class Meta:
        model = Comuna

class CalleResource(resources.ModelResource):

    class Meta:
        model = Calle

class DireccionResource(resources.ModelResource):

    class Meta:
        model = Direccion

class TagColorResource(resources.ModelResource):

    class Meta:
        model = TagColor

class OperatorResource(resources.ModelResource):

    class Meta:
        model = Operator

class CommercialActivityResource(resources.ModelResource):

    class Meta:
        model = CommercialActivity

class BankResource(resources.ModelResource):

    class Meta:
        model = Bank

class BankAccountResource(resources.ModelResource):

    class Meta:
        model = BankAccount

class MainReasonResource(resources.ModelResource):

    class Meta:
        model = MainReason

class CreditResource(resources.ModelResource):

    class Meta:
        model = Credit

class ChargeResource(resources.ModelResource):

    class Meta:
        model = Charge
