import sys, os, io, json, string, random, pdfkit, xlsxwriter, zipfile, calendar, datetime, math, django, urllib3, re, urllib3
import petl as etl
import unidecode, pytz
import mimetypes, dateutil
from magic import from_buffer
from django.core.files.base import ContentFile
from collections import defaultdict
from billing.processing import packaging, creditNote, invoiceNulling
from billing.serializers import *
from billing.views import invoiceDataAfter, invoiceData, unifiedInvoiceData
from billing.services import *
from billing.tasks import end_onboarding_email, four_days_whatsapp, nuevo_cliente_email, post_activacion_email
from django.template import loader
from django.template.loader import get_template
from io import BytesIO, StringIO
from pdf417gen import render_image, encode
from dateutil import parser as date_parser
from dateutil.relativedelta import relativedelta
from datetime import date, timedelta
from tempfile import mkstemp
from decimal import *
from itertools import chain
from django.forms import formset_factory
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
    user_passes_test,
)
from .pulso  import request_address_pulso, get_complete_address, get_street_location
from django.views.decorators.clickjacking import xframe_options_exempt
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import get_user_model
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.humanize.templatetags.humanize import intcomma
from django.template.defaultfilters import yesno
from django.core.files import File
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models import Value, Q, F, Max, Count, Sum, Min
from django.db.models.functions import Concat, ExtractMonth, ExtractYear
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import get_template
from django.utils import formats, timezone
from django.views.generic import FormView, CreateView, DetailView, ListView, UpdateView
from django.views.decorators.csrf import csrf_exempt
from wkhtmltopdf.views import PDFTemplateResponse
from gallery.forms import PicForm, PicUpdateForm
from gallery.models import Pic
from documents.forms import DocumentForm, DocumentUpdateForm
from documents.models import Document
from hr.models import *
from infra.models import NetworkEquipment
from infra.forms import NetworkEquipmentForm
from localflavor.cl.forms import CLRutField
from reports.models import ServiceSumSnapshot, ServiceStatusLog, ServiceChangeStatusLog
from taggit.models import Tag
from rest_framework import viewsets, views, permissions, filters, status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from pydub import AudioSegment
from hr.models import Technician, Employee, Agent
from .mikrotik import activate_client, drop_client
from .models import *
from infra.models import Node
from providers.models import Payment as ProviderPayment
from .forms import *
from .serializers import *
from django.core import serializers
from url_filter.integrations.drf import DjangoFilterBackend
from django import forms
from wkhtmltopdf.utils import render_pdf_from_template
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from constance import config
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
import numpy as np
import base64
from django.contrib.auth.models import Group, User
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from fuzzywuzzy import fuzz
from functools import reduce
from django.db.models.functions import Length
from wkhtmltopdf.utils import (_options_to_args, make_absolute_paths,
                               wkhtmltopdf, render_pdf_from_template,
                               render_to_temporary_file, RenderedFile)
from django.utils.timezone import get_current_timezone, make_aware
import requests
from rest_framework.renderers import JSONRenderer

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


def has_group(user, group_name):
    try:
        group =  Group.objects.get(name=group_name)
    except Group.DoesNotExist:
        return False
        
    return group in user.groups.all()

# Permiso para token
class IsSystemInternalPermission(permissions.BasePermission):
    message = "No tiene permiso"

    def has_permission(self, request, view):
        token = request.META.get("HTTP_AUTHORIZATION", None)
        if token:
            return (
                token
                == "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s"
            )
        return False


@csrf_exempt
def lookup_from_liveagent(request):
    if request.method == "POST":
        try:
            email = str(request.body).split("email")[2][3:].split('"')[0]
            customer = get_object_or_404(Customer, email=email)
            if customer.service_set.count() == 1:
                service = customer.service_set.first()
                if service.seen_connected:
                    network_status = "Activo"
                else:
                    network_status = "Cortado"
            else:
                network_status = "Desconocido"
            return render(
                request, "customers/lookup_from_liveagent.html", {"customer": customer}
            )
        except:
            return HttpResponse("Cliente no encontrado")
    else:
        try:
            email = str(request.GET["email"])
            customer = get_object_or_404(Customer, email=email)
            if customer.service_set.count() == 1:
                service = customer.service_set.first()
                if service.seen_connected:
                    network_status = "Activo"
                else:
                    network_status = "Cortado"
            else:
                network_status = "Desconocido"
            return render(
                request, "customers/lookup_from_liveagent.html", {"customer": customer}
            )
        except Customer.DoesNotExist:
            return HttpResponse("Cliente no encontrado")
    raise Http404


@login_required(login_url="/accounts/login/")
def home(request):
    username = None
    context={}
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        try:
            companies=UserCompany.objects.get(user=user).company.all()
        except:
            pass
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                name_operator="Todos"
            else:
                name_operator=Operator.objects.get(id=op.operator).name
            context={"companies":companies.exclude(id=op.company),
                    "current_company":op.company,
                    "current_company_name":Company.objects.get(id=op.company).name,
                    "current_operator":op.operator,
                    "current_operator_name":name_operator,
                    "operadores": Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
                }
        except:
            pass
    return render(request, "customers/home.html", context)

@login_required
def user_operador_new_ajax(request):
    operator = request.GET.get("operator", False)
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        op=UserOperator.objects.filter(user=user).update(operator=int(operator))
    context={}

    data = json.dumps({"data": context}, default=default)
    return HttpResponse(data, content_type="application/json")

@login_required
def user_company_new_ajax(request):
    company = request.GET.get("company", False)
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        op=UserOperator.objects.filter(user=user)
        for i in op: 
            i.company=company
            i.save()
            if i.operator==0:
                pass
            else:
                i.operator=0
                i.save()
    context={}

    data = json.dumps({"data": context}, default=default)
    return HttpResponse(data, content_type="application/json")

def export_network_drops(request):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    now = timezone.now()
    current_year = now.year
    current_month = now.month
    qs = NetworkStatusChange.objects.filter(
        new_state="cortado",
        created_at__year=current_year,
        created_at__month=current_month,
    )

    header = ("ID", "Agente", "Servicio", "Cuando")
    bold = workbook.add_format({"bold": True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0

    for drop in qs:
        formatted_date = formats.date_format(drop.created_at, "SHORT_DATE_FORMAT")

        worksheet.write(row, 0, drop.pk)
        worksheet.write(row, 1, drop.agent.username)
        worksheet.write(row, 2, drop.service.number)
        worksheet.write(row, 3, formatted_date)
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response[
        "Content-Disposition"
    ] = 'attachment; filename="registro-cortes-junio.xlsx"'
    return response


## exporta la base de datos de direcciones
def export_txt(request):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    customers = Customer.objects.all()

    header = ("RUT cliente con dirección en blanco", "Valor")
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0

    for cc in customers:
        if not cc.composite_address:
            worksheet.write(row, 0, cc.rut)
            worksheet.write(row, 1, cc.composite_address)
            row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response[
        "Content-Disposition"
    ] = 'attachment; filename="servicio-falta-direccion.xlsx"'
    return response


def price_tag(number):
    value = ('%.6f' % number).rstrip('0').rstrip('.')
    return intcomma(value).replace(".", "*").replace(",", ".").replace("*", ",")


# exporta los clientes con sus comunas antes y después
def export_customers(request):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    filtered_services = Customer.objects.annotate(num_services=Count("service")).filter(
        num_services__gt=1
    )

    header = (
        "Nombre",
        "RUT",
        "Dirección (old)",
        "Comuna",
        "Ubicacion",
        "Calle",
        "Número",
        "Departamento",
        "Torre",
        "URL",
    )
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0

    for call in filtered_services:
        worksheet.write(row, 0, call.name)
        worksheet.write(row, 1, call.rut)
        worksheet.write(row, 2, call.address)
        worksheet.write(row, 3, call.commune)
        worksheet.write(row, 4, call.get_location_display())
        worksheet.write(row, 5, call.street)
        worksheet.write(row, 6, call.house_number)
        worksheet.write(row, 7, call.apartment_number)
        worksheet.write(row, 8, call.tower)
        worksheet.write(
            row, 9, "https://optic.matrix2.cl{}".format(call.get_absolute_url())
        )
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response[
        "Content-Disposition"
    ] = 'attachment; filename="clientes-comuna-diferente.xlsx"'
    return response


def export_facturacion(request):
    today = timezone.now().date()
    services = [Service.objects.active().latest("pk")]
    output_file = io.StringIO()
    lx = []
    for service in services:
        lx.append("->Boleta<-")
        lx.append(
            "39;81921;{};1;0;2018-06-28;2018-05-1;2018-05-21;{};{};{};;{};{};Santiago;{};".format(
                today,
                service.customer.rut,
                service.number,
                service.customer.name,
                service.best_address,
                service.customer.commune,
                service.customer.first_email,
            )
        )
        lx.append("->BoletaTotales<-")
        lx.append("0;0;0;7490;0;0;0;0;")
        lx.append("->BoletaDetalle<-")
        lx.append("1;77;Servicio Plan XL;0;1;7490;0;7490;INT1;UN;;")
    output_file.write("\n".join(lx))
    output_file.seek(0)
    response = HttpResponse(output_file.read(), content_type="text/plain")
    response["Content-Disposition"] = 'attachment; filename="facturacion.txt"'
    return response


class PayorSearch(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = PayorRut
    queryset = PayorRut.objects.none()
    paginate_by = 50
    permission_required = "customers.search_payorrut"

    def get_queryset(self):
        search = self.request.GET.get("search", "")

        if search:
            if self.request.user.is_authenticated():
                username = self.request.user.username
                user=User.objects.get(username=username)
                companies=UserCompany.objects.get(user=user).company.all()
                try:
                    op=UserOperator.objects.get(user=user)
                    
                    if op.operator==0:
                        try:
                            rut_regex = "^" + "\.*".join(list(search))
                            object_list = PayorRut.objects.filter(rut__iregex=rut_regex)
                            for i in object_list:
                                if i.customer.service_set.filter(operator__company__id=op.company).count()==0:
                                    object_list=object_list.exclude(id=i.id)
                        except ValueError:
                            object_list = self.queryset
                    else:
                        try:
                            rut_regex = "^" + "\.*".join(list(search))
                            object_list = PayorRut.objects.filter(rut__iregex=rut_regex)
                            for i in object_list:
                                if i.customer.service_set.filter(operator=op.operator).count()==0:
                                    object_list=object_list.exclude(id=i.id)
                        except ValueError:
                            object_list = self.queryset
                except:
                    pass
            
        else:
            object_list = self.queryset
        return object_list


payor_search = PayorSearch.as_view()


class InvoiceSearch(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Invoice
    queryset = Invoice.objects.none()
    template_name = "customers/invoice_search.html"
    paginate_by = 50
    permission_required = "customers.search_invoice"

    def get_queryset(self):
        search = self.request.GET.get("search", "")
        if search:
            if self.request.user.is_authenticated():
                username = self.request.user.username
                user=User.objects.get(username=username)
                companies=UserCompany.objects.get(user=user).company.all()
                try:
                    op=UserOperator.objects.get(user=user)
                    
                    if op.operator==0:
                        try:
                            folio = int(search)
                            object_list = Invoice.objects.filter(folio=folio, operator__company__id=op.company)
                        except ValueError:
                            object_list = self.queryset
                    else:
                        try:
                            folio = int(search)
                            object_list = Invoice.objects.filter(folio=folio, operator=op.operator)
                        except ValueError:
                            object_list = self.queryset
                except:
                    pass
        else:
            object_list = self.queryset
        return object_list


invoice_search = InvoiceSearch.as_view()


########### DURATION REPORT ###########
@login_required
def durationExport(arguments, **kwargs):

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    # Header
    worksheet.write(0, 0, "RUT")
    worksheet.write(0, 1, "N.SERVICIO")
    worksheet.write(0, 2, "ESTATUS")
    worksheet.write(0, 3, "PERÍODOS ACTIVOS")
    worksheet.write(0, 4, "DÍAS ACTIVO")
    worksheet.write(0, 5, "TOTAL ACTIVO")

    # Filter
    interval = kwargs["interval"]
    status = int(kwargs["status"])

    duration = DurationRecord.objects.exclude(duration__lte=timezone.timedelta(0))
    durationKey = 'interval ' + interval
    if interval:
        intervalCache = cache.get(durationKey)
        if intervalCache:
            duration = intervalCache
        else: 
            if interval == "a":
                duration = duration.filter(duration__lte=timezone.timedelta(days=30))
            elif interval == "b":
                duration = duration.filter(
                    duration__gt=timezone.timedelta(days=30),
                    duration__lte=timezone.timedelta(days=60),
                )
            elif interval == "c":
                duration = duration.filter(
                    duration__gt=timezone.timedelta(days=60),
                    duration__lte=timezone.timedelta(days=90),
                )
            elif interval == "d":
                duration = duration.filter(
                    duration__gt=timezone.timedelta(days=90),
                    duration__lte=timezone.timedelta(days=180),
                )
            elif interval == "e":
                duration = duration.filter(
                    duration__gt=timezone.timedelta(days=180),
                    duration__lte=timezone.timedelta(days=360),
                )
            elif interval == "f":
                duration = duration.filter(
                    duration__gt=timezone.timedelta(days=360),
                    duration__lte=timezone.timedelta(days=540),
                )
            elif interval == "g":
                duration = duration.filter(
                    duration__gt=timezone.timedelta(days=540),
                    duration__lte=timezone.timedelta(days=720),
                )
            elif interval == "h":
                duration = duration.filter(duration__gt=timezone.timedelta(days=720))
            cache.set(durationKey, duration, timeout = 3600)

    durationList = []
    for d in duration:
        durationList.append(d.id)
    
    username = None
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator!=0:
                services = Service.objects.filter(id__in=durationList, status__isnull=False, operator__id=op.operator)
            else:
                services = Service.objects.filter(id__in=durationList, status__isnull=False,operator__company__id=op.company)
        except:
            pass
    else:
        services = Service.objects.filter(id__in=durationList, status__isnull=False)

    if status != 0:
        services = services.filter(status=status)

    r = 1
    for s in services.order_by("id"):
        # Filter
        worksheet.autofilter("A1:F" + str(r))

        traductor = {
            str(1): "Activo",
            str(2): "Por retirar",
            str(3): "Retirado",
            str(4): "No instalado",
            str(5): "Canje",
            str(6): "Instalación rechazada",
            str(7): "Moroso",
            str(8): "Cambio de Titular",
            str(9): "Descartado",
            str(10): "Baja Temporal",
            str(11): "Perdido, no se pudo retirar",
        }

        history = HistoricalService.objects.filter(id=s.id).order_by("history_date")
        daysActive = 0
        intervals = []
        active = 0
        anterior = 0

        for h in history:
            date = h.history_date

            if anterior == 0 and h.status != 1:
                inactive = date
                anterior = h.status
                continue

            if anterior == 0 and h.status == 1:
                active = date
                anterior = h.status
                continue

            if anterior == 1 and h.status == 1:
                continue

            if anterior != 1 and h.status == 1:
                active = date
                anterior = h.status
                continue

            if anterior == 1 and h.status != 1:
                anterior = h.status
                inactive = date
                daysActive += abs((active - inactive).days)
                intervals.append(
                    [active.strftime("%d/%m/%Y"), inactive.strftime("%d/%m/%Y")]
                )

        if anterior == 1:
            intervals.append([active.strftime("%d/%m/%Y"), "Actualmente"])

            daysActive += abs(
                (active.replace(tzinfo=None) - datetime.now().replace(tzinfo=None)).days
            )

        if active == 0:
            intervals = False

        if intervals:
            period = ""
            for i in intervals:
                if len(intervals) > 1:
                    period += i[0] + " - " + i[1] + ", "
                else:
                    period = i[0] + " - " + i[1]

        else:
            period = "Nunca activado."

        if daysActive > 0:
            rest, year = math.modf(daysActive / 365)
            days, month = math.modf(rest * 365 / (365 / 12))
            year = str(int(year))
            month = str(int(month))
            day = str(int(round(days * (365 / 12), 0)))
            totalTime = year + " años, " + month + " meses, " + day + " días."

        else:
            totalTime = "Nunca activado."

        rut = s.customer.rut if s.customer.rut else "Cliente sin RUT."
        number = s.number if s.number else "Sin número de servicio."
        status = traductor[str(s.status)]
        data = [rut, number, status, period, daysActive, totalTime]

        # Writing rows
        worksheet.write(r, 0, data[0])
        worksheet.write(r, 1, data[1])
        worksheet.write(r, 2, data[2])
        worksheet.write(r, 3, data[3])
        worksheet.write(r, 4, data[4])
        worksheet.write(r, 5, data[5])
        r += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = (
        'attachment; filename="reporte de permanencia de servicios '
        + datetime.now().strftime("%d-%m-%Y")
        + '.xlsx"'
    )
    return response


def prepareDurationReport(service):
    history = HistoricalService.objects.filter(id=service.id).order_by("history_date")

    daysActive = 0
    intervals = []
    active = 0
    anterior = 0
    for h in history:
        date = h.history_date
        if anterior == 0 and h.status != 1:
            inactive = date
            anterior = h.status
            continue

        if anterior == 0 and h.status == 1:
            active = date
            anterior = h.status
            continue

        if anterior == 1 and h.status == 1:
            continue

        if anterior != 1 and h.status == 1:
            active = date
            anterior = h.status
            continue

        if anterior == 1 and h.status != 1:
            anterior = h.status
            inactive = date
            daysActive += abs((active - inactive).days)
            intervals.append(
                [active.strftime("%d/%m/%Y"), inactive.strftime("%d/%m/%Y")]
            )

    if anterior == 1:
        intervals.append([active.strftime("%d/%m/%Y"), "Actualmente"])
        daysActive += abs(
            (active.replace(tzinfo=None) - datetime.now().replace(tzinfo=None)).days
        )

    if active == 0:
        intervals = False

    if intervals:
        period = ""
        for i in intervals:
            if len(intervals) > 1:
                period += i[0] + " - " + i[1] + ", "
            else:
                period = i[0] + " - " + i[1]
    else:
        period = "Nunca activado."

    if daysActive > 0:
        rest, year = math.modf(daysActive / 365)
        days, month = math.modf(rest * 365 / (365 / 12))
        year = str(int(year))
        month = str(int(month))
        day = str(int(round(days * (365 / 12), 0)))
        totalTime = year + " años, " + month + " meses, " + day + " días."
    else:
        totalTime = "Nunca activado."

    customerId = service.customer.id
    number = service.number if service.number else "Sin número de servicio."
    service_id = service.id if service.id else "Sin número de servicio."
    choices = dict(service.STATUS_CHOICES)
    status = service.status if service.status else "Sin estatus."
    dictionary = {
        "customer": customerId,
        "number": number,
        "service_id":service_id,
        "status": status,
        "active_intervals": period,
        "active_days": daysActive,
        "active_total": totalTime,
    }

    return dictionary


def durationReportAjax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "customer",
            "1": "number",
            "2": "status",
            "3": "active_intervals",
            "4": "active_days",
            "5": "active_total",
        }

        # Sort request
        sort = {"asc": "", "desc": "-"}
        sortCol = request.GET.get("order[0][column]", None)
        sortDirection = request.GET.get("order[0][dir]", None)

        # Search request
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        # Filter
        changeStatus = request.GET.get("status", None)
        interval = request.GET.get("interval", None)

        duration = DurationRecord.objects.exclude(duration__lte=timezone.timedelta(0))
        durationKey = 'interval ' + interval
        if interval:
            intervalCache = cache.get(durationKey)
            if intervalCache:
                duration = intervalCache
            else: 
                if interval == "a":
                    duration = duration.filter(duration__lte=timezone.timedelta(days=30))
                elif interval == "b":
                    duration = duration.filter(
                        duration__gt=timezone.timedelta(days=30),
                        duration__lte=timezone.timedelta(days=60),
                    )
                elif interval == "c":
                    duration = duration.filter(
                        duration__gt=timezone.timedelta(days=60),
                        duration__lte=timezone.timedelta(days=90),
                    )
                elif interval == "d":
                    duration = duration.filter(
                        duration__gt=timezone.timedelta(days=90),
                        duration__lte=timezone.timedelta(days=180),
                    )
                elif interval == "e":
                    duration = duration.filter(
                        duration__gt=timezone.timedelta(days=180),
                        duration__lte=timezone.timedelta(days=360),
                    )
                elif interval == "f":
                    duration = duration.filter(
                        duration__gt=timezone.timedelta(days=360),
                        duration__lte=timezone.timedelta(days=540),
                    )
                elif interval == "g":
                    duration = duration.filter(
                        duration__gt=timezone.timedelta(days=540),
                        duration__lte=timezone.timedelta(days=720),
                    )
                elif interval == "h":
                    duration = duration.filter(duration__gt=timezone.timedelta(days=720))
                cache.set(durationKey, duration, timeout = 3600)
                

        durationList = []
        for d in duration:
            durationList.append(d.id)

        # Filtro multiempresa 
        username = None
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    services = Service.objects.filter(id__in=durationList, status__isnull=False, operator__id=op.operator)
                else:
                    services = Service.objects.filter(id__in=durationList, status__isnull=False, operator__company__id=op.company)
            except:
                pass
        else:
            services = Service.objects.filter(id__in=durationList, status__isnull=False)

        if changeStatus:
            services = services.filter(status=changeStatus)

        if sortCol and sortDirection:
            allServices = services.order_by(sort[sortDirection] + columns[sortCol])
        else:
            allServices = services.order_by("status")

        totalRecord = allServices.count()

        sortedServices = allServices.distinct()
        filteredServices = sortedServices.count()

        if length == -1:
            slicedServices = sortedServices
        else:
            slicedServices = sortedServices[start : start + length]

        dataList = list(map(prepareDurationReport, slicedServices))

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": totalRecord,
                "recordsFiltered": filteredServices,
                "data": dataList,
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def duration_report(request):

    status = dict(Service.objects.all().first().STATUS_CHOICES)
    status_options = []
    for s in status:
        status_options.append({"key": s, "value": status[s]})

    # Duration view
    qs = DurationRecord.objects.exclude(duration__lte=timezone.timedelta(0))

    group_a = qs.filter(duration__lte=timezone.timedelta(days=30))
    group_b = qs.filter(
        duration__gt=timezone.timedelta(days=30),
        duration__lte=timezone.timedelta(days=60),
    )
    group_c = qs.filter(
        duration__gt=timezone.timedelta(days=60),
        duration__lte=timezone.timedelta(days=90),
    )
    group_d = qs.filter(
        duration__gt=timezone.timedelta(days=90),
        duration__lte=timezone.timedelta(days=180),
    )
    group_e = qs.filter(
        duration__gt=timezone.timedelta(days=180),
        duration__lte=timezone.timedelta(days=360),
    )
    group_f = qs.filter(
        duration__gt=timezone.timedelta(days=360),
        duration__lte=timezone.timedelta(days=540),
    )
    group_g = qs.filter(
        duration__gt=timezone.timedelta(days=540),
        duration__lte=timezone.timedelta(days=720),
    )
    group_h = qs.filter(duration__gt=timezone.timedelta(days=720))

    # Gráfica

    fig, ax = plt.subplots()
    ax.set_xlabel("Intervalos de meses")
    ax.set_ylabel("Cantidad de servicios")

    bar_x = [1, 2, 3, 4, 5, 6, 7, 8]
    bar_height = [
        group_a.count(),
        group_b.count(),
        group_c.count(),
        group_d.count(),
        group_e.count(),
        group_f.count(),
        group_g.count(),
        group_h.count(),
    ]
    bar_tick_label = [
        "<1",
        "1-2",
        "2-3",
        "3-6",
        "6-12",
        "12-18",
        "18-24",
        ">24",
    ]
    bar_label = bar_height

    bar_plot = plt.bar(bar_x, bar_height, tick_label=bar_tick_label)

    for idx, rect in enumerate(bar_plot):
        height = rect.get_height()
        ax.text(
            rect.get_x() + rect.get_width() / 2.0,
            1.0 * height,
            bar_label[idx],
            ha="center",
            va="bottom",
            rotation=0,
        )

    plt.title("Gráfico de permanencia de clientes")

    buffer = BytesIO()
    plt.savefig(buffer, format="png")
    buffer.seek(0)
    image_png = buffer.getvalue()
    buffer.close()

    graphic = base64.b64encode(image_png)
    graphic = graphic.decode("utf-8")

    plt.clf()

    return render(
        request,
        "customers/duration_report.html",
        {
            "interval_options": [
                ["a", "<1"],
                ["b", "1-2"],
                ["c", "2-3"],
                ["d", "3-6"],
                ["e", "6-12"],
                ["f", "12-18"],
                ["g", "18-24"],
                ["h", ">24"],
            ],
            "status_options": status_options,
            "group_a": group_a.count(),
            "group_b": group_b.count(),
            "group_c": group_c.count(),
            "group_d": group_d.count(),
            "group_e": group_e.count(),
            "group_f": group_f.count(),
            "group_g": group_g.count(),
            "group_h": group_h.count(),
            "graphic": graphic,
        },
    )


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def count_report(request):
    status_displays = dict(Service.STATUS_CHOICES)
    statuses = [
        {"code": status, "name": status_displays[status]}
        for status in sorted(
            Service.objects.values_list("status", flat=True)
            .distinct()
            .order_by("status")
        )
    ]
    return render(
        request,
        "customers/count_report.html",
        {
            "statuses": statuses,
            "first_of_month": ServiceSumSnapshot.objects.earliest(
                "created_at"
            ).created_at.isoformat(),
            "last_of_month": ServiceSumSnapshot.objects.latest(
                "created_at"
            ).created_at.isoformat(),
        },
    )


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def count_table_ajax(request):
    if request.is_ajax():
        initiald = date_parser.parse(request.GET.get("initiald"))
        finald = date_parser.parse(request.GET.get("finald"))
        status = request.GET.get("status", "")

        if status:
            try:
                status_code = int(status)
            except ValueError:
                status_code = 1

        if status_code:
            try:
                sn = ServiceSumSnapshot.objects.get(
                    created_at=initiald, status=status_code
                )
                initial_count = sn.count
            except ServiceSumSnapshot.DoesNotExist:
                initial_count = 0

            try:
                sn = ServiceSumSnapshot.objects.get(
                    created_at=finald, status=status_code
                )
                final_count = sn.count
            except ServiceSumSnapshot.DoesNotExist:
                final_count = 0

            snapshot_data = {"initial_count": initial_count, "final_count": final_count}
        else:
            snapshot_data = {}

        data = json.dumps({"data": snapshot_data})
        return HttpResponse(data, content_type="application/json")
    else:
        return Http404


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def plan_report(request):
    status_displays = dict(Service.STATUS_CHOICES)
    statuses = [
        {"code": status, "name": status_displays[status]}
        for status in sorted(
            Service.objects.values_list("status", flat=True)
            .distinct()
            .order_by("status")
        )
    ]
    sellers = Seller.objects.all()
    technicians = Technician.objects.filter(active=True)
    kind_displays = dict(Payment.PAYMENT_OPTIONS)
    kinds = [
        {"code": kind, "name": kind_displays[kind]}
        for kind in sorted(
            Payment.objects.values_list("kind", flat=True).distinct().order_by("kind")
        )
    ]

    return render(
        request,
        "customers/plan_report.html",
        {
            "statuses": statuses,
            "first_of_month": timezone.now().date().replace(day=1).isoformat(),
            "last_of_month": (
                timezone.now().date() + relativedelta(day=31)
            ).isoformat(),
            "sellers": sellers,
            "technicians": technicians,
            "nodes": Node.objects.all(),
            "kinds": kinds,
        },
    )


@login_required
@user_passes_test(lambda u: has_group(u, "Operador QC") or has_group(u, "Head QA"))
def call_report(request):
    subject_displays = dict(Call.SUBJECT_CHOICES)
    subjects = [
        {"code": subject, "name": subject_displays[subject]}
        for subject in sorted(
            Call.objects.values_list("subject", flat=True)
            .distinct()
            .order_by("subject")
        )
    ]
    agents = get_user_model().objects.filter(is_active=True)

    return render(
        request,
        "customers/call_report.html",
        {
            "subjects": subjects,
            "first_of_month": timezone.now().date().replace(day=1).isoformat(),
            "last_of_month": (
                timezone.now().date() + relativedelta(day=31)
            ).isoformat(),
            "agents": agents,
        },
    )


def order_typing(typing):
    if isinstance(typing, tuple):
        typing_unorder = typing[1].split(" ")[0]
    else:
        typing_unorder = typing["name"].split(" ")[0]
    return [int(typing_order) for typing_order in typing_unorder.split(".")]


@login_required
@user_passes_test(lambda u: has_group(u, "Operador QC") or has_group(u, "Head QA"))
def call_qa_report(request):
    agents = Agent.objects.all().order_by("user")
    TYPING_EMPTY = ((0, "---------"),)
    for record in get_typing("classification=1"):
        if record["operator"] == 2:
            TYPING_EMPTY = TYPING_EMPTY + ((record["ID"], record["name"]),)
    TYPING_EMPTY = sorted(TYPING_EMPTY, key=lambda tup: tup[1])
    typings = list(TYPING_EMPTY)
    categories = [
        (0, "---------"),
    ]
    subcategories = [
        (0, "---------"),
    ]

    return render(
        request,
        "customers/callqa_report.html",
        {
            "first_of_month": timezone.now().date().replace(day=1).isoformat(),
            "last_of_month": (
                timezone.now().date() + relativedelta(day=31)
            ).isoformat(),
            "agents": agents,
            "typings": typings,
            "categories": categories,
            "subcategories": subcategories,
        },
    )


def call_qa_report_data_format(data):
    initiald = date_parser.parse(data.get("initiald"))
    finald = date_parser.parse(data.get("finald"))
    qs = CallQA.objects.filter(created_at__gte=initiald, created_at__lte=finald)
    total_calls = len(qs)
    dd = {}

    if data.get("type") == "agent":
        report_type = "Agentes"
        agent = data.get("agent", "")
        if agent:
            try:
                agent_pk = int(agent)
                qs = qs.filter(agent=agent_pk)
            except ValueError:
                pass

        for record in qs:
            if str(record.agent) not in dd:
                dd[str(record.agent)] = {"qty": 0, "total": total_calls}
            dd[str(record.agent)]["qty"] += 1

    else:
        report_type = "Tipificaciones"
        typing_id = int(data.get("typing", 0))
        category_id = int(data.get("category", 0))
        subcategory_id = int(data.get("subcategory", 0))
        typing_total = 0
        category_total = 0
        subcategory_total = 0
        try:
            if typing_id > 0:
                typing = get_typing("ID={}".format(typing_id))
                if len(typing) > 0:
                    dd["TIPO"] = {"qty": -1, "total": 0}
                    typing_total = len(qs.filter(typing=typing_id))
                    dd[typing[0]["name"]] = {"qty": typing_total, "total": total_calls}

            if category_id > 0:
                category = get_typing("ID={}".format(category_id))
                if len(category) > 0:
                    dd["CATEGORIA"] = {"qty": -1, "total": 0}
                    category_total = len(qs.filter(category=category_id))
                    dd[category[0]["name"]] = {
                        "qty": category_total,
                        "total": typing_total,
                    }

            if subcategory_id > 0:
                subcategory = get_typing("ID={}".format(subcategory_id))
                if len(subcategory) > 0:
                    dd["SUBCATEGORIA"] = {"qty": -1, "total": 0}
                    subcategory_total = len(qs.filter(subcategory=subcategory_id))
                    dd[subcategory[0]["name"]] = {
                        "qty": subcategory_total,
                        "total": category_total,
                    }

        except ValueError:
            pass

    return dd, total_calls, report_type


@login_required
@user_passes_test(lambda u: has_group(u, "Head QA") or has_group(u, "Operador QC"))
def call_qa_table_ajax(request):
    if request.is_ajax():
        dd, total_calls, report_type = call_qa_report_data_format(request.GET)

        graph_image = ""

        if len(dd):

            labels = []
            values = []

            for key, value in dd.items():
                if value["qty"] >= 0:
                    labels.append(key)
                    percentage = (
                        round((value["qty"] * 100) / value["total"], 2)
                        if value["total"] != 0
                        else 0
                    )
                    values.append(percentage)

            x = np.arange(len(labels))
            width = 0.35

            fig, ax = plt.subplots()
            graph = ax.bar(x, values, width)

            ax.set_ylabel("Cantidad de evaluaciones en %")
            ax.set_title("Cantidad de Evaluaciones", pad=20)
            ax.set_xticks(x)
            ax.set_xticklabels(labels)
            ax.set_ylim([0, 100])

            for val in graph:
                height = val.get_height()
                ax.annotate(
                    "{}".format(height),
                    xy=(val.get_x() + val.get_width() / 2, height),
                    xytext=(0, 3),
                    textcoords="offset points",
                    ha="center",
                    va="bottom",
                )

            buf = io.BytesIO()
            canvas = FigureCanvasAgg(fig)
            graph_image = (
                "data:image/jpeg;base64," + base64.b64encode(buf.getvalue()).decode()
            )

        data = json.dumps({"snapshot_data": dd, "graph": graph_image})
        return HttpResponse(data, content_type="application/json")
    else:
        return Http404


def export_call_qa_excel(request):
    dd, total_calls, report_type = call_qa_report_data_format(request.GET)

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = (report_type, "Evaluaciones", "Porcentajes")
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)
    worksheet.set_column(0, 0, 30)
    worksheet.set_column(1, 1, 8)
    worksheet.set_column(2, 2, 12)
    chart = workbook.add_chart({"type": "column"})

    row = 1

    for key, value in dd.items():
        if value["qty"] >= 0:
            worksheet.write(row, 0, key)
            worksheet.write(row, 1, value["qty"])
            worksheet.write(
                row,
                2,
                round((value["qty"] * 100) / value["total"], 2)
                if value["total"] != 0
                else 0,
            )
            chart.add_series(
                {"name": ["Sheet1", row, 0], "values": ["Sheet1", row, 2, row, 2],}
            )
            row += 1
        else:
            worksheet.merge_range(row, 0, row, 2, key)
            row += 1

    chart.set_title({"name": "Cantidad de Evaluaciones"})
    chart.set_x_axis({"name": report_type})
    chart.set_y_axis({"name": "Cantidad de Evaluaciones en %"})
    worksheet.insert_chart(row + 1, 0, chart)

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response[
        "Content-Disposition"
    ] = 'attachment; filename="reporte_de_inspección_de_calidad.xlsx"'
    return response


@login_required
def export_call_qa_pdf(request):
    dd, total_calls, report_type = call_qa_report_data_format(request.GET)

    graph_image = ""

    if len(dd):

        labels = []
        values = []

        for key, value in dd.items():
            if value["qty"] >= 0:
                labels.append(key)
                percentage = (
                    round((value["qty"] * 100) / value["total"], 2)
                    if value["total"] != 0
                    else 0
                )
                values.append(percentage)

        x = np.arange(len(labels))
        width = 0.35

        fig, ax = plt.subplots()
        graph = ax.bar(x, values, width)

        ax.set_ylabel("Cantidad de evaluaciones en %")
        ax.set_title("Cantidad de Evaluaciones", pad=20)
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        ax.set_ylim([0, 100])

        for val in graph:
            height = val.get_height()
            ax.annotate(
                "{}".format(height),
                xy=(val.get_x() + val.get_width() / 2, height),
                xytext=(0, 3),
                textcoords="offset points",
                ha="center",
                va="bottom",
            )

        buf = io.BytesIO()
        canvas = FigureCanvasAgg(fig)
        graph_image = (
            "data:image/jpeg;base64," + base64.b64encode(buf.getvalue()).decode()
        )

    context = {
        "data": dict(dd),
        "graph": graph_image,
        "from": request.GET.get("initiald"),
        "to": request.GET.get("finald"),
        "type": report_type,
    }
    filename = "reporte_de_llamadas.pdf"

    response = PDFTemplateResponse(
        request=request,
        template="customers/callqa_report_pdf.html",
        context=context,
        filename=filename,
        show_content_in_browser=True,
    )
    return response


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def payment_report(request):
    status_displays = dict(Service.STATUS_CHOICES)
    #operators = Operator.objects.all()
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                operators = Operator.objects.filter(company__id=op.company)
            else:
                operators = Operator.objects.filter(id=op.operator)
        except:
            pass
    statuses = [
        {"code": status, "name": status_displays[status]}
        for status in sorted(
            Service.objects.values_list("status", flat=True)
            .distinct()
            .order_by("status")
        )
    ]
    sellers = Seller.objects.all()
    kind_displays = dict(Payment.PAYMENT_OPTIONS)
    kinds = [
        {"code": kind, "name": kind_displays[kind]}
        for kind in sorted(
            Payment.objects.values_list("kind", flat=True).distinct().order_by("kind")
        )
    ]

    return render(
        request,
        "customers/payment_report.html",
        {
            "statuses": statuses,
            "first_of_month": timezone.now().date().replace(day=1).isoformat(),
            "last_of_month": (
                timezone.now().date() + relativedelta(day=31)
            ).isoformat(),
            "sellers": sellers,
            "nodes": Node.objects.all(),
            "kinds": kinds,
            "operators": operators,
        },
    )

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def reincorporacion_report(request):
    year = timezone.now().date().year
    YEARS =  list(range(2020, year)) #list(range(year-1, year - 7, -1))

    return render(
        request,
        "customers/report_reincorporacion.html",
        {
            "years":YEARS,
            "actual_year": year,
        },
    )

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def reincorporacion_table_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        year = request.GET.get("year", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        #Filtro por year
        all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, status=1)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_logs=all_logs.filter(service__operator__company__id=op.company)
                else:
                    all_logs=all_logs.filter(service__operator__id=op.operator)
            except:
                pass

        array_months=[]
        months={'Enero':1, 'Febrero':2, 'Marzo':3, 'Abril':4, 'Mayo':5, 'Junio':6,
        'Julio':7, 'Agosto':8, 'Septiembre':9, 'Octubre':10, 'Noviembre':11, 'Diciembre':12}
        suma=0
        for mes in months.keys():
            #print(months[mes]) 
            porRetirar=all_logs.filter(created_at__month=months[mes], prev_status=2).count()
            moroso=all_logs.filter(created_at__month=months[mes], prev_status=7).count()
            retirado=all_logs.filter(created_at__month=months[mes], prev_status=3).count()
            baja=all_logs.filter(created_at__month=months[mes], prev_status=10).count()
            otro=all_logs.filter(created_at__month=months[mes], prev_status__in=[5,6,8,9,11]).count()
            array_months.append(
                {"mes": mes,
                "porRetirar": porRetirar,
                "moroso": moroso,
                "retirado": retirado,
                "baja":baja,
                "otro":otro,
                "total": porRetirar+moroso+retirado+baja+otro, 
                "year": year,
                "month_number":months[mes]}
                )
            suma=suma+porRetirar+moroso+retirado+baja+otro

        array_months.append(
                {"mes": "",
                "porRetirar": "",
                "moroso": "",
                "retirado": "",
                "baja":"",
                "otro":"Total anual",
                "total": suma , 
                "year": year,
                "month_number":1}
                )
        """
        array_months.append({"mes": "Febrero",
        "porRetirar": 0,
        "moroso": 1,
        "retirado": 2,
        "baja": 4,
        "otro":5,
        "total": 6})
        data={"mes": "Enero",
        "porRetirar": 0,
        "moroso": 1,
        "retirado": 2,
        "baja": 4,
        "otro":5,
        "total": 6}
        """
        data = json.dumps(
                {
                    #"draw": request.GET.get("draw", 1),
                    "recordsTotal": 13,
                    "recordsFiltered": 0,
                    "data": array_months,
                }
        )
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def reincorporacion_report_month(request, mes, year):
    Month = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }
    month = Month[int(mes)]
    #Filtro por year y mes
    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year,created_at__month=mes,
                                    prev_status__in=[2,3,5,6,7,8,9,10,11], status=1)
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        #Filtro por year y mes
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                all_logs=all_logs.filter(service__operator__company__id=op.company)
                plans=Plan.objects.filter(operator__company__id=op.company)
            else:
                all_logs=all_logs.filter(service__operator__id=op.operator)
                plans=Plan.objects.filter(operator__id=op.operator)
        except:
            plans=Plan.objects.all()
            pass

    return render(
        request,
        "customers/report_reincorporacion_month.html",
        {
            "mes":mes,
            "year": year,
            "all_logs":all_logs,
            "month":month,
            "plans":plans,
        },
    )
 
@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def reincorporacion_month_table_ajax(request, mes, year, to_excel=False):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        plan = request.GET.get("plan", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))


        #Filtro por year y mes
        all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year,created_at__month=mes,
                                    prev_status__in=[2,3,5,6,7,8,9,10,11], status=1)

        if plan:
            all_logs=all_logs.filter(plan__id=int(plan))

        all_count = all_logs.count()

        if search:  
            pass
        else:
            filtered_logs = all_logs

        filtered_logs = filtered_logs.distinct()

        filtered_count = filtered_logs.count()

        if length == -1:
            sliced_logs = filtered_logs
        else:
            sliced_logs = filtered_logs[start : start + length]

        if to_excel:
            return list(map(prepare_logs, filtered_logs))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_logs, sliced_logs)),
                }
            )
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def reincorporacion_report_month_excel(request):
    year = request.GET.get("year", None)
    mes = request.GET.get("mes", None)
    plan = request.GET.get("plan", None)

    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, created_at__month=mes,
                                        prev_status__in=[2,3,5,6,7,8,9,10,11], status=1)

    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        #Filtro por year y mes
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                all_logs=all_logs.filter(service__operator__company__id=op.company)
            else:
                all_logs=all_logs.filter(service__operator__id=op.operator)
        except:
            pass

    if plan:
        all_logs=all_logs.filter(plan__id=int(plan))

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha",
        "Service",
        "Plan",
        "Dirección",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in all_logs:
        try:
            formatted_creation_date = formats.date_format(
                item_data.created_at, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, "Servicio #"+str(item_data.service.number))
        worksheet.write(row, 2, str(item_data.plan))
        worksheet.write(row, 3, str(item_data.address))
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="reincorporacion_matrix.xlsx"'
    return response

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def ventas_report(request):
    year = timezone.now().date().year
    YEARS =  list(range(year-1, year)) #list(range(year-1, year - 7, -1))

    return render(
        request,
        "customers/report_ventas.html",
        {
            "years":YEARS,
            "actual_year": year,
        },
    )

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def ventas_table_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        year = request.GET.get("year", None)
        report = request.GET.get("report", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        #Filtro por year
        all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, prev_status=4, status=1)
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_logs=all_logs.filter(service__operator__company__id=op.company)
                else:
                    all_logs=all_logs.filter(service__operator__id=op.operator)
            except:
                pass

        array_months=[]
        months={'Enero':1, 'Febrero':2, 'Marzo':3, 'Abril':4, 'Mayo':5, 'Junio':6,
        'Julio':7, 'Agosto':8, 'Septiembre':9, 'Octubre':10, 'Noviembre':11, 'Diciembre':12}
        suma=0
        for mes in months.keys():
            #print(months[mes]) 
            """hsu=all_logs.filter(created_at__month=months[mes], kind=1).count()
            fibra=all_logs.filter(created_at__month=months[mes], kind=2).count()
            empresa=all_logs.filter(created_at__month=months[mes], empresa=True).count()"""
            if report=="1":
                hsu=all_logs.filter(created_at__month=months[mes], kind=1, empresa=False).count()
                fibra=all_logs.filter(created_at__month=months[mes], kind=2, empresa=False).count()
                empresa=all_logs.filter(created_at__month=months[mes], empresa=True).count()
                array_months.append(
                    {"mes": mes,
                    "hsu": hsu,
                    "fibra": fibra,
                    "empresa": empresa,
                    "total": hsu+fibra+empresa, 
                    "year": year,
                    "month_number":months[mes]}
                    )
                suma=suma+hsu+fibra+empresa
            else:
                hsu=all_logs.filter(created_at__month=months[mes], kind=1).count()
                fibra=all_logs.filter(created_at__month=months[mes], kind=2).count()
                empresa=all_logs.filter(created_at__month=months[mes], empresa=True).count()
                array_months.append(
                    {"mes": mes,
                    "hsu": hsu,
                    "fibra": fibra,
                    "empresa": empresa,
                    "total": hsu+fibra, 
                    "year": year,
                    "month_number":months[mes]}
                    )
                suma=suma+hsu+fibra
        array_months.append(
                {"mes": "",
                "hsu":  "",
                "fibra":  "",
                "empresa":"Total anual",
                "total": suma , 
                "year": year,
                "month_number":1}
                )

        data = json.dumps(
                {
                    #"draw": request.GET.get("draw", 1),
                    "recordsTotal": 13,
                    "recordsFiltered": 0,
                    "data": array_months,
                }
        )
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def ventas_report_month(request, mes, year):
    Month = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }
    month = Month[int(mes)]

    #Filtro por year y mes
    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year,created_at__month=mes,
                                    prev_status=4, status=1)

    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        #Filtro por year y mes
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                all_logs=all_logs.filter(service__operator__company__id=op.company)
                plans=Plan.objects.filter(operator__company__id=op.company)
            else:
                all_logs=all_logs.filter(service__operator__id=op.operator)
                plans=Plan.objects.filter(operator__id=op.operator)
        except:
            plans=Plan.objects.all()
            pass

    return render(
        request,
        "customers/report_ventas_month.html",
        {
            "mes":mes,
            "year": year,
            "all_logs":all_logs,
            "month":month,
            "plans":plans,
        },
    )
 
@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def ventas_month_table_ajax(request, mes, year, to_excel=False):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        plan = request.GET.get("plan", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))


        #Filtro por year y mes
        all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year,created_at__month=mes,
                                    prev_status=4, status=1)

        if plan:
            all_logs=all_logs.filter(plan__id=int(plan))

        all_count = all_logs.count()

        if search:  
            pass
        else:
            filtered_logs = all_logs

        filtered_logs = filtered_logs.distinct()

        filtered_count = filtered_logs.count()

        if length == -1:
            sliced_logs = filtered_logs
        else:
            sliced_logs = filtered_logs[start : start + length]

        if to_excel:
            return list(map(prepare_logs, filtered_logs))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_logs, sliced_logs)),
                }
            )
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

    
@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def ventas_report_month_excel(request):
    year = request.GET.get("year", None)
    mes = request.GET.get("mes", None)
    plan = request.GET.get("plan", None)

    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, created_at__month=mes,
                                        prev_status=4, status=1)

    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        #Filtro por year y mes
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                all_logs=all_logs.filter(service__operator__company__id=op.company)
            else:
                all_logs=all_logs.filter(service__operator__id=op.operator)
        except:
            pass

    if plan:
        all_logs=all_logs.filter(plan__id=int(plan))

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha",
        "Service",
        "Plan",
        "Dirección",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in all_logs:
        try:
            formatted_creation_date = formats.date_format(
                item_data.created_at, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, "Servicio #"+str(item_data.service.number))
        worksheet.write(row, 2, str(item_data.plan))
        worksheet.write(row, 3, str(item_data.address))
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="ventas_matrix.xlsx"'
    return response

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def morosos_report(request):
    year = timezone.now().date().year
    YEARS =  list(range(2020, year)) #list(range(year-1, year - 7, -1))

    return render(
        request,
        "customers/report_morosos.html",
        {
            "years":YEARS,
            "actual_year": year,
        },
    )

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo"))
def morosos_table_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        year = request.GET.get("year", None)
        report = request.GET.get("report", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        #Filtro por year
        all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, prev_status=1, status=7)
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_logs=all_logs.filter(service__operator__company__id=op.company)
                else:
                    all_logs=all_logs.filter(service__operator__id=op.operator)
            except:
                pass
        array_months=[]
        months={'Enero':1, 'Febrero':2, 'Marzo':3, 'Abril':4, 'Mayo':5, 'Junio':6,
        'Julio':7, 'Agosto':8, 'Septiembre':9, 'Octubre':10, 'Noviembre':11, 'Diciembre':12}
        suma=0
        for mes in months.keys():
            #print(months[mes])
            if report=="1":
                hsu=all_logs.filter(created_at__month=months[mes], kind=1, empresa=False).count()
                fibra=all_logs.filter(created_at__month=months[mes], kind=2, empresa=False).count()
                empresa=all_logs.filter(created_at__month=months[mes], empresa=True).count()
                array_months.append(
                    {"mes": mes,
                    "hsu": hsu,
                    "fibra": fibra,
                    "empresa": empresa,
                    "total": hsu+fibra+empresa, 
                    "year": year,
                    "month_number":months[mes]}
                    )
                suma=suma+hsu+fibra+empresa
            else:
                hsu=all_logs.filter(created_at__month=months[mes], kind=1).count()
                fibra=all_logs.filter(created_at__month=months[mes], kind=2).count()
                empresa=all_logs.filter(created_at__month=months[mes], empresa=True).count()
                array_months.append(
                    {"mes": mes,
                    "hsu": hsu,
                    "fibra": fibra,
                    "empresa": empresa,
                    "total": hsu+fibra, 
                    "year": year,
                    "month_number":months[mes]}
                    )
                suma=suma+hsu+fibra
        array_months.append(
                {"mes": "",
                "hsu":  "",
                "fibra":  "",
                "empresa":"Total anual",
                "total": suma , 
                "year": year,
                "month_number":1}
                )

        data = json.dumps(
                {
                    #"draw": request.GET.get("draw", 1),
                    "recordsTotal": 13,
                    "recordsFiltered": 0,
                    "data": array_months,
                }
        )
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def morosos_report_month(request, mes, year):
    Month = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }
    month = Month[int(mes)]

    #Filtro por year y mes
    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year,created_at__month=mes,
                                    prev_status=1, status=7)

    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        #Filtro por year y mes
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                all_logs=all_logs.filter(service__operator__company__id=op.company)
                plans=Plan.objects.filter(operator__company__id=op.company)
            else:
                all_logs=all_logs.filter(service__operator__id=op.operator)
                plans=Plan.objects.filter(operator__id=op.operator)
        except:
            plans=Plan.objects.all()
            pass

    return render(
        request,
        "customers/report_morosos_month.html",
        {
            "mes":mes,
            "year": year,
            "all_logs":all_logs,
            "month":month,
            "plans":plans,
        },
    )
 

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def morosos_month_table_ajax(request, mes, year, to_excel=False):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        plan = request.GET.get("plan", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        #Filtro por year y mes
        all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year,created_at__month=mes,
                                    prev_status=1, status=7)

        if plan:
            all_logs=all_logs.filter(plan__id=int(plan))

        all_count = all_logs.count()

        if search:  
            pass
        else:
            filtered_logs = all_logs

        filtered_logs = filtered_logs.distinct()

        filtered_count = filtered_logs.count()

        if length == -1:
            sliced_logs = filtered_logs
        else:
            sliced_logs = filtered_logs[start : start + length]

        if to_excel:
            return list(map(prepare_logs, filtered_logs))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_logs, sliced_logs)),
                }
            )
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_logs(log):
    dictionary = {
            "DT_RowId": log.pk,
            "created_at": log.created_at.strftime("%Y-%m-%d %H:%M"),
            "pk": log.pk,
            "service": log.service.number,
            "service_id": log.service.id,
            "customer": log.service.customer.name,
            "plan": log.plan.name,
            "address": log.address,
            #"comuna":log.service.best_location,
        }
    return dictionary

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def morosos_report_month_excel(request):
    year = request.GET.get("year", None)
    mes = request.GET.get("mes", None)
    plan = request.GET.get("plan", None)
    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, created_at__month=mes,
                                        prev_status=1, status=7)

    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        #Filtro por year y mes
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                all_logs=all_logs.filter(service__operator__company__id=op.company)
            else:
                all_logs=all_logs.filter(service__operator__id=op.operator)
        except:
            pass

    if plan:
        all_logs=all_logs.filter(plan__id=int(plan))
        
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha",
        "Service",
        "Plan",
        "Dirección",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in all_logs:
        try:
            formatted_creation_date = formats.date_format(
                item_data.created_at, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, "Servicio #"+str(item_data.service.number))
        worksheet.write(row, 2, str(item_data.plan))
        worksheet.write(row, 3, str(item_data.address))
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="morosos_matrix.xlsx"'
    return response


@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def porretirar_report(request):
    year = timezone.now().date().year
    YEARS =  list(range(2020, year)) #list(range(year-1, year - 7, -1))

    return render(
        request,
        "customers/report_porretirar.html",
        {
            "years":YEARS,
            "actual_year": year,
        },
    )

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def porretirar_table_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        year = request.GET.get("year", None)
        report = request.GET.get("report", None)
        kind = request.GET.get("kind", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        #Filtro por year
        if kind=="1":
            all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, prev_status=1, status=2)
        else:
            all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, prev_status=1, status=3)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_logs=all_logs.filter(service__operator__company__id=op.company)
                else:
                    all_logs=all_logs.filter(service__operator__id=op.operator)
            except:
                pass
        array_months=[]
        months={'Enero':1, 'Febrero':2, 'Marzo':3, 'Abril':4, 'Mayo':5, 'Junio':6,
        'Julio':7, 'Agosto':8, 'Septiembre':9, 'Octubre':10, 'Noviembre':11, 'Diciembre':12}
        suma=0
        for mes in months.keys():
            #print(months[mes])
            if report=="1":
                hsu=all_logs.filter(created_at__month=months[mes], kind=1, empresa=False).count()
                fibra=all_logs.filter(created_at__month=months[mes], kind=2, empresa=False).count()
                empresa=all_logs.filter(created_at__month=months[mes], empresa=True).count()
                array_months.append(
                    {"mes": mes,
                    "hsu": hsu,
                    "fibra": fibra,
                    "empresa": empresa,
                    "total": hsu+fibra+empresa, 
                    "year": year,
                    "month_number":months[mes]}
                    )
                suma=suma+hsu+fibra+empresa
            else:
                hsu=all_logs.filter(created_at__month=months[mes], kind=1).count()
                fibra=all_logs.filter(created_at__month=months[mes], kind=2).count()
                empresa=all_logs.filter(created_at__month=months[mes], empresa=True).count()
                array_months.append(
                    {"mes": mes,
                    "hsu": hsu,
                    "fibra": fibra,
                    "empresa": empresa,
                    "total": hsu+fibra, 
                    "year": year,
                    "month_number":months[mes]}
                    )
                suma=suma+hsu+fibra
        array_months.append(
                {"mes": "",
                "hsu":  "",
                "fibra":  "",
                "empresa":"Total anual",
                "total": suma , 
                "year": year,
                "month_number":1}
                )

        data = json.dumps(
                {
                    #"draw": request.GET.get("draw", 1),
                    "recordsTotal": 13,
                    "recordsFiltered": 0,
                    "data": array_months,
                }
        )
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def porretirar_report_month(request, mes, year):
    Month = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }
    month = Month[int(mes)]

    #Filtro por year y mes
    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year,created_at__month=mes,
                                    prev_status=1, status=2)

    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        #Filtro por year y mes
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                all_logs=all_logs.filter(service__operator__company__id=op.company)
                plans=Plan.objects.filter(operator__company__id=op.company)
            else:
                all_logs=all_logs.filter(service__operator__id=op.operator)
                plans=Plan.objects.filter(operator__id=op.operator)
        except:
            plans=Plan.objects.all()
            pass
    return render(
        request,
        "customers/report_porretirar_month.html",
        {
            "mes":mes,
            "year": year,
            "all_logs":all_logs,
            "month":month,
             "plans":plans,
        },
    )
 
@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def porretirar_month_table_ajax(request, mes, year, to_excel=False):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        plan = request.GET.get("plan", None)
        
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))


        #Filtro por year y mes
        all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year,created_at__month=mes,
                                    prev_status=1, status=2)

        if plan:
            all_logs=all_logs.filter(plan__id=int(plan))

        all_count = all_logs.count()

        if search:  
            pass
        else:
            filtered_logs = all_logs

        filtered_logs = filtered_logs.distinct()

        filtered_count = filtered_logs.count()

        if length == -1:
            sliced_logs = filtered_logs
        else:
            sliced_logs = filtered_logs[start : start + length]

        if to_excel:
            return list(map(prepare_logs, filtered_logs))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_logs, sliced_logs)),
                }
            )
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo") or has_group(u, "Head Comercial"))
def porretirar_report_month_excel(request):
    year = request.GET.get("year", None)
    mes = request.GET.get("mes", None)
    plan = request.GET.get("plan", None)

    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, created_at__month=mes,
                                        prev_status=1, status=2)

    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        #Filtro por year y mes
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                all_logs=all_logs.filter(service__operator__company__id=op.company)
            else:
                all_logs=all_logs.filter(service__operator__id=op.operator)
        except:
            pass

    if plan:
        all_logs=all_logs.filter(plan__id=int(plan))

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha",
        "Service",
        "Plan",
        "Dirección",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in all_logs:
        try:
            formatted_creation_date = formats.date_format(
                item_data.created_at, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, "Servicio #"+str(item_data.service.number) )
        worksheet.write(row, 2, str(item_data.plan))
        worksheet.write(row, 3, str(item_data.address))
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="porretirar_matrix.xlsx"'
    return response


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def retiros_report(request):
    year = timezone.now().date().year
    YEARS =  list(range(2020, year)) #list(range(year-1, year - 7, -1))

    return render(
        request,
        "customers/report_retiros.html",
        {
            "years":YEARS,
            "actual_year": year,
        },
    )

def prepare_retiros(retiro):
    dictionary = {
            "DT_RowId": retiro.pk,
            "created_at": retiro.created_at.strftime("%Y-%m-%d %H:%M"),
            "pk": retiro.pk,
            "service": retiro.service.number,
            "customer": retiro.service.customer.name,
            "plan": retiro.plan.name,
            "address": retiro.address,
            "comuna":retiro.service.best_location,
        }
    return dictionary

@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def retiros_table_ajax(request, to_excel=False):
    if request.is_ajax() or to_excel:
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        year = request.GET.get("year", None)
        mes = request.GET.get("mes", None)
        kind = request.GET.get("kind", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        #Filtro por year
        if kind=="1":
            all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, status=2)
        else:
            all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, status=3)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_logs=all_logs.filter(service__operator__company__id=op.company)
                else:
                    all_logs=all_logs.filter(service__operator__id=op.operator)
            except:
                pass
        if mes:
            all_logs=all_logs.filter(created_at__month=int(mes))

        all_count = all_logs.count()

        if search:  
            pass
        else:
            filtered_retiros = all_logs

        filtered_retiros = filtered_retiros.distinct()

        filtered_count = filtered_retiros.count()

        if length == -1:
            sliced_retiros = filtered_retiros
        else:
            sliced_retiros = filtered_retiros[start : start + length]

        if to_excel:
            return list(map(prepare_retiros, filtered_retiros))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_retiros, sliced_retiros)),
                }
            )

            return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def retiros_excel(request):
    """year = request.GET.get("year", None)
    mes = request.GET.get("mes", None)

    all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, created_at__month=mes,
                                        status=2)
    """
    data = retiros_table_ajax(request, True)
    #print(data)
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha",
        "Service",
        "Cliente",
        "Plan",
        "Dirección",
        "Comuna",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in data:
        try:
            formatted_creation_date = formats.date_format(
                item_data["created_at"], "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, "Servicio #"+str(item_data["service"]) )
        worksheet.write(row, 2, str(item_data["customer"]))
        worksheet.write(row, 3, str(item_data["plan"]))
        worksheet.write(row, 4, str(item_data["address"]))
        worksheet.write(row, 5, str(item_data["comuna"]))
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="retiros_matrix.xlsx"'
    return response



@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def newinstalled_report(request):
    year = timezone.now().date().year
    YEARS = list(range(2020, year)) #list(range(year-1, year - 7, -1))

    return render(
        request,
        "customers/report_newinstalled.html",
        {
            "years":YEARS,
            "actual_year": year,
        },
    )

def prepare_newinstalled(newinstalled):
    dictionary = {
            "DT_RowId": newinstalled.pk,
            "created_at": newinstalled.created_at.strftime("%Y-%m-%d %H:%M"),
            "pk": newinstalled.pk,
            "service": newinstalled.service.number,
            "customer": newinstalled.service.customer.name,
            "plan": newinstalled.plan.name,
            "address": newinstalled.address,
            "comuna":newinstalled.service.best_location,
        }
    return dictionary

@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def newinstalled_table_ajax(request, to_excel=False):
    if request.is_ajax() or to_excel:
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        year = request.GET.get("year", None)
        mes = request.GET.get("mes", None)
        #kind = request.GET.get("kind", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        #Filtro por year
        all_logs=ServiceChangeStatusLog.objects.filter(created_at__year=year, status=1)
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_logs=all_logs.filter(service__operator__company__id=op.company)
                else:
                    all_logs=all_logs.filter(service__operator__id=op.operator)
            except:
                pass
        if mes:
            all_logs=all_logs.filter(created_at__month=int(mes))

        all_count = all_logs.count()

        if search:  
            pass
        else:
            filtered_newinstalled = all_logs

        filtered_newinstalled = filtered_newinstalled.distinct()

        filtered_count = filtered_newinstalled.count()

        if length == -1:
            sliced_newinstalled = filtered_newinstalled
        else:
            sliced_newinstalled = filtered_newinstalled[start : start + length]

        if to_excel:
            return list(map(prepare_newinstalled, filtered_newinstalled))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_newinstalled, sliced_newinstalled)),
                }
            )

            return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def newinstalled_excel(request):
    data = newinstalled_table_ajax(request, True)
    #print(data)
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha",
        "Cliente",
        "Service",
        "Plan",
        "Dirección",
        "Comuna",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in data:
        try:
            formatted_creation_date = formats.date_format(
                item_data["created_at"], "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, str(item_data["customer"]))
        worksheet.write(row, 2, "Servicio #"+str(item_data["service"]) )
        worksheet.write(row, 3, str(item_data["plan"]))
        worksheet.write(row, 4, str(item_data["address"]))
        worksheet.write(row, 5, str(item_data["comuna"]))
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="newinstalled_matrix.xlsx"'
    return response
 

@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def payments_report(request):
    year = timezone.now().date().year
    YEARS = list(range(year-1, 2015, -1)) #list(range(year-1, year - 7, -1))
    plans=Plan.objects.all()
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                plans=plans.filter(operator__company__id=op.company)
            else:
                plans=plans.filter(operator__id=op.operator)
        except:
            pass
    return render(
        request,
        "customers/report_payments.html",
        {
            "years":YEARS,
            "actual_year": year,
            "plans":plans,
        },
    )

def prepare_payments_report(payment):
    plan=""
    boletas=""
    for i in  payment.service.all():
        plan=plan+str(i.plan)+" "

    for i in payment.invoice.all():
        boletas=boletas+str(i.comment)+" "

    ia = serializers.serialize("json", payment.invoice.all())
    dictionary = {
            "DT_RowId": payment.pk,
            "created_at": payment.created_at.strftime("%Y-%m-%d %H:%M"),
            "pk": payment.pk,
            "comment": payment.comment,
            #"service": payment.service.all(),
            "customer": payment.customer.name if payment.customer else "",
            "plan": plan,
            "monto": price_tag(payment.amount),
            "invoice":ia,
            "boletas":boletas,
        }
    return dictionary

@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def payments_report_table_ajax(request, to_excel=False):
    if request.is_ajax() or to_excel:
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        year = request.GET.get("year", None)
        mes = request.GET.get("mes", None)
        plan = request.GET.get("plan", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        #Filtro por year
        all_logs=Payment.objects.filter(created_at__year=year)
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_logs=all_logs.filter(operator__company__id=op.company)
                else:
                    all_logs=all_logs.filter(operator__id=op.operator)
            except:
                pass

        if mes:
            all_logs=all_logs.filter(created_at__month=int(mes))
        
        if plan:
            #print(Hey)
            service=Service.objects.filter(plan__id=int(plan))
            #print(service)
            all_logs=all_logs.filter(service__in=service)
            #print(all_logs)
        
        """for i in all_logs:
            print(i.customer)"""

        all_count = all_logs.count()

        if search:  
            pass
        else:
            filtered_retiros = all_logs

        filtered_retiros = filtered_retiros.distinct()

        filtered_count = filtered_retiros.count()

        if length == -1:
            sliced_retiros = filtered_retiros
        else:
            sliced_retiros = filtered_retiros[start : start + length]

        if to_excel:
            return list(map(prepare_payments_report, filtered_retiros))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_payments_report, sliced_retiros)),
                }
            )

            return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def payments_plan_excel(request):
    data = payments_report_table_ajax(request, True)
    #print(data)
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha",
        "Pago",
        "Cliente",
        "Monto",
        "Plan",
        "Boletas asociadas",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in data:
        try:
            formatted_creation_date = formats.date_format(
                item_data["created_at"], "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, str(item_data["comment"]))
        worksheet.write(row, 2, str(item_data["customer"]) )
        worksheet.write(row, 3, str(item_data["monto"]))
        worksheet.write(row, 4, str(item_data["plan"]))
        worksheet.write(row, 5, str(item_data["boletas"]))
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="pagos_report_matrix.xlsx"'
    return response

@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo"))
def node_report(request):
    status_displays = dict(Service.STATUS_CHOICES)
    statuses = [
        {"code": status, "name": status_displays[status]}
        for status in sorted(
            Service.objects.values_list("status", flat=True)
            .distinct()
            .order_by("status")
        )
    ]
    sorted_statuses = sorted(statuses, key=lambda k: k["code"])
    return render(
        request,
        "customers/node_report.html",
        {
            "statuses": sorted_statuses,
            "first_of_month": timezone.now().date().replace(day=1).isoformat(),
            "last_of_month": (
                timezone.now().date() + relativedelta(day=31)
            ).isoformat(),
        },
    )


@login_required
@user_passes_test(lambda u: has_group(u, "Head Desarrollo"))
def node_table_ajax(request):
    if request.is_ajax():
        initiald = date_parser.parse(request.GET.get("initiald"))
        finald = date_parser.parse(request.GET.get("finald"))
        status_displays = dict(Service.STATUS_CHOICES)
        statuses = [
            {"code": status, "name": status_displays[status]}
            for status in sorted(
                Service.objects.values_list("status", flat=True)
                .distinct()
                .order_by("status")
            )
        ]
        qs = ServiceStatusLog.objects.filter(
            created_at__gte=initiald, created_at__lte=finald
        )

        dd = {}

        for node in Node.objects.all():
            snx = (
                qs.filter(node=node)
                .values("status")
                .annotate(status_changes=Count("id"))
            )

            dd[node.code] = {"address": node.address}

            status_data = defaultdict(int)
            for st in statuses:
                status_main_key = "status_code_" + str(st["code"])
                status_data[status_main_key]

            for sn in snx:
                status_key = "status_code_" + str(sn["status"])
                status_data[status_key] += sn["status_changes"]

            dd[node.code]["status_data"] = status_data

        data = json.dumps({"snapshot_data": dd})
        print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        return Http404


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def network_report(request):
    return render(
        request,
        "customers/network_report.html",
        {
            "first_of_month": timezone.now().date().replace(day=1).isoformat(),
            "last_of_month": (
                timezone.now().date() + relativedelta(day=31)
            ).isoformat(),
        },
    )


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def network_table_ajax(request):
    if request.is_ajax():
        initiald = date_parser.parse(request.GET.get("initiald"))
        finald = date_parser.parse(request.GET.get("finald"))
        # days = [initiald + timezone.timedelta(days=n) for n in range((finald - initiald).days+1)]
        
        activated = NetworkStatusChange.objects.filter(new_state="activado").filter(
            created_at__gte=initiald, created_at__lte=finald
        )

        dropped = NetworkStatusChange.objects.filter(new_state="cortado").filter(
            created_at__gte=initiald, created_at__lte=finald
        )
        
        username = None
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    activated = NetworkStatusChange.objects.filter(new_state="activado", service__operator__id=op.operator).filter(
                        created_at__gte=initiald, created_at__lte=finald
                    )
                    dropped = NetworkStatusChange.objects.filter(new_state="cortado", service__operator__id=op.operator).filter(
                        created_at__gte=initiald, created_at__lte=finald
                    )
                else:
                    activated = NetworkStatusChange.objects.filter(new_state="activado", service__operator__company__id=op.company).filter(
                        created_at__gte=initiald, created_at__lte=finald
                    )
                    dropped = NetworkStatusChange.objects.filter(new_state="cortado", service__operator__company__id=op.company).filter(
                        created_at__gte=initiald, created_at__lte=finald
                    )
                
            except:
                pass
        
        activated_by_day = (
            activated.extra(select={"created_date": "date(customers_networkstatuschange.created_at)"})
            .values("created_date")
            .order_by()
            .annotate(Count("id"))
        )

        dropped_by_day = (
            dropped.extra(select={"created_date": "date(customers_networkstatuschange.created_at)"})
            .values("created_date")
            .order_by()
            .annotate(Count("id"))
        )

        dd = defaultdict(lambda: defaultdict(int))

        for change in activated_by_day:
            dd[change["created_date"].isoformat()]["activado"] = change["id__count"]

        for change in dropped_by_day:
            dd[change["created_date"].isoformat()]["cortado"] = change["id__count"]

        data = json.dumps(
            {
                "snapshot_data": dd,
                "activated_count": activated.count(),
                "dropped_count": dropped.count(),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        return Http404


@login_required
def table_ajax(request):
    if request.is_ajax():
        initiald = date_parser.parse(request.GET.get("initiald"))
        finald = date_parser.parse(request.GET.get("finald"))
        status = request.GET.get("status", "")
        node = request.GET.get("node", "")
        seller = request.GET.get("seller", "")
        technician = request.GET.get("technician", "")
        

        dateKey = str(initiald.date()) + ' ' + str(finald.date())
        date_cache = cache.get(dateKey)
        if date_cache:
            qs = date_cache
        else:
            qs = ServiceStatusLog.objects.filter(
                created_at__gte=initiald, created_at__lte=finald
            )
            cache.set(dateKey, qs, timeout = 3600)
        
        statusKey = dateKey + ' '  + str(status)
        if status:
            status_cache = cache.get(statusKey)
            if status_cache:
                qs = status_cache
            else:
                try:
                    status_code = int(status)
                    qs = qs.filter(status=status_code)
                    cache.set(statusKey, qs, timeout = 3600)
                except ValueError:
                    pass

        nodeKey = statusKey + ' ' + str(node)
        if node:
            node_cache = cache.get(nodeKey)
            if node_cache:
                qs = node_cache
            else:
                try:
                    node_pk = int(node)
                    qs = qs.filter(node=node_pk)
                    cache.set(nodeKey, qs, timeout = 3600)
                except ValueError:
                    pass
   
        sellerKey = nodeKey + ' ' + str(seller)
        if seller:
            seller_cache = cache.get(sellerKey)
            if seller_cache:
                qs = seller_cache
            else:
                try:
                    seller_pk = int(seller)
                    qs = qs.filter(seller=seller_pk)
                    cache.set(sellerKey, qs, timeout = 3600)
                except ValueError:
                    pass
        
        technicianKey = sellerKey + ' ' + str(technician)
        if technician:
            technician_cache = cache.get(technicianKey)
            if technician_cache:
                qs = technician_cache
            else: 
                try:
                    technician_pk = int(technician)
                    qs = qs.filter(technician=technician_pk)
                    cache.set(technicianKey, qs, timeout = 3600)
                except ValueError:
                    pass
        
        snx = qs.values("status", "service__number")
        dd = defaultdict(lambda: dict(count=0, services=[]))
        status_displays = dict(Service.STATUS_CHOICES)
      
        for sn in snx:
            dd[str(status_displays[sn["status"]])]["count"] += 1
            if sn["service__number"]:
                dd[str(status_displays[sn["status"]])]["services"].append(
                    sn["service__number"]
                )

        data = json.dumps({"snapshot_data": dd})
        return HttpResponse(data, content_type="application/json")
    else:
        return Http404


@login_required
@user_passes_test(lambda u: has_group(u, "Head QA") or has_group(u, "Operador QC"))
def call_table_ajax(request):
    if request.is_ajax():
        initiald = date_parser.parse(request.GET.get("initiald"))
        finald = date_parser.parse(request.GET.get("finald"))
        agent = request.GET.get("agent", "")
        qs = Call.objects.filter(created_at__gte=initiald, created_at__lte=finald)

        if agent:
            try:
                agent_pk = int(agent)
                qs = qs.filter(answered_by=agent_pk)
            except ValueError:
                pass

        snx = qs.values("subject").annotate(calls=Count("id"))
        dd = defaultdict(int)

        subject_displays = dict(Call.SUBJECT_CHOICES)

        for sn in snx:
            dd[str(subject_displays[sn["subject"]])] += sn["calls"]

        data = json.dumps({"snapshot_data": dd})
        return HttpResponse(data, content_type="application/json")
    else:
        return Http404


@login_required
@user_passes_test(lambda u: has_group(u, "Head Finanzas") or has_group(u, "Coordinador de facturación") or has_group(u, "Operador de finanzas"))
def payments_table_ajax(request):
    if request.is_ajax():
        initiald = date_parser.parse(request.GET.get("initiald"))
        finald = date_parser.parse(request.GET.get("finald"))
        operator = request.GET.get("operator")

        # Para que incluya el último día
        if finald:
            newfinald = finald.replace(hour=23, minute=59, second=59)

        username = None
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    qs = Payment.objects.exclude(customer__rut="76.171.094-K").filter(
                        created_at__gte=initiald, created_at__lte=newfinald, operator=op.operator
                    )
                else:
                    if int(operator) == 0:
                        qs = Payment.objects.exclude(customer__rut="76.171.094-K").filter(
                            created_at__gte=initiald, created_at__lte=newfinald, operator__company__id=op.company)
                        
                    else:
                        qs = Payment.objects.exclude(customer__rut="76.171.094-K").filter(
                            created_at__gte=initiald, created_at__lte=newfinald, operator=operator
                        )
            except:
                pass

        total_amount = qs.aggregate(total_amount=Sum("amount"))["total_amount"]
        payment_count = qs.count()

        snx = qs.values("kind").order_by().annotate(Count("id"), Sum("amount"))

        kind_displays = dict(Payment.PAYMENT_OPTIONS)

        dd = defaultdict(dict)

        for sn in snx:
            dd[str(kind_displays[sn["kind"]])] = {
                "count": sn["id__count"],
                "amount": sn["amount__sum"],
            }

        data = json.dumps(
            {
                "snapshot_data": dd,
                "total_amount": total_amount,
                "total_count": payment_count,
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        return Http404


@login_required
def generate_service_preview(request, pk):
    service = get_object_or_404(Service, pk=pk)
    context = {"plans": [service.plan], "additional_plans": [service.plan]}
    return render(request, "customers/service_pdf.html", context)


@login_required
def generate_service_pdf(request, pk):
    service = get_object_or_404(Service, pk=pk)
    filename = "contrato-%s.pdf" % str(service.pk)
    context = {"plans": [service.plan], "additional_plans": [service.plan]}

    response = PDFTemplateResponse(
        request=request,
        template="customers/service_pdf.html",
        context=context,
        filename=filename,
        show_content_in_browser=True,  # FIXME: set to False
        # cmd_options={'margin-top': 50}
    )
    return response


@login_required
@permission_required("customers.export_service")
def export_services(request):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    # filtering explicit here
    search = request.GET.get("search_query", "").replace("+", "")

    raw_operator = request.GET.get("operator", None)

    if raw_operator:
        try:
            operator = int(raw_operator)
        except ValueError:
            operator = None
    else:
        operator = None

    raw_network_status = request.GET.get("network_status", None)
    raw_balance_from = request.GET.get("balance_from", "null")
    raw_balance_to = request.GET.get("balance_to", "null")

    if raw_network_status:
        try:
            network_status = int(raw_network_status)
        except ValueError:
            network_status = None
    else:
        network_status = None

    if raw_balance_from:
        try:
            balance_from = int(raw_balance_from)
        except ValueError:
            balance_from = "null"
    else:
        balance_from = "null"

    if raw_balance_to:
        try:
            balance_to = int(raw_balance_to)
        except ValueError:
            balance_to = "null"
    else:
        balance_to = "null"

    raw_service_status = request.GET.get("service_status", None)

    if raw_service_status:
        try:
            service_status = int(raw_service_status)
        except ValueError:
            service_status = None
    else:
        service_status = None

    id_type_client = request.GET.get("id_type_client", None)
    if id_type_client:
        try:
            type_client = int(id_type_client)
        except ValueError:
            type_client = None
    else:
        type_client = None

    db_composite_address = Concat(
        "street",
        Value(" "),
        "house_number",
        Value(" depto "),
        "apartment_number",
        Value(" torre "),
        "tower",
    )
    all_services = Service.objects.select_related("customer", "plan").annotate(
        db_composite_address=db_composite_address
    )
    if operator:
        all_services = all_services.filter(operator=operator)

    if network_status:
        NETWORK_STATUSES = {
            1: {"seen_connected": True},
            2: {"seen_connected": False},
            3: {"seen_connected": None},
        }
        all_services = all_services.filter(**NETWORK_STATUSES.get(network_status, {}))
    if balance_from != "null" and balance_to != "null":
        all_services = all_services.filter(
            Q(customer__balance__balance__gte=balance_from),
            Q(customer__balance__balance__lte=balance_to),
        )
    else:
        if balance_from != "null":
            all_services = all_services.filter(
                customer__balance__balance__gte=balance_from
            )

        if balance_to != "null":
            all_services = all_services.filter(
                customer__balance__balance__lte=balance_to
            )

    if service_status:
        all_services = all_services.filter(status=service_status)

    if type_client and type_client != 0:
        all_services = all_services.filter(customer__kind=type_client)

    if search:
        rut_regex = "^" + "\.*".join(list(search))

        q_objects = Q()
        q_objects |= Q(customer__rut__iregex=rut_regex)
        q_objects |= Q(number__icontains=search)
        q_objects |= Q(customer__email__icontains=search)
        q_objects |= Q(db_composite_address__unaccent__icontains=search)

        splitted_name_queries = [
            Q(customer__first_name__unaccent__icontains=term) for term in search.split(" ")
        ]
        q_name_objects = Q()
        for q in splitted_name_queries:
            q_name_objects.add(q, Q.AND)
            
        q_objects |= q_name_objects

        splitted_last_name_queries = [
            Q(customer__last_name__unaccent__icontains=term) for term in search.split(" ")
        ]
        q_last_name_objects = Q()
        for q in splitted_last_name_queries:
            q_last_name_objects.add(q, Q.AND)
            
        q_objects |= q_last_name_objects
        filtered_services = all_services.filter(q_objects)
    else:
        filtered_services = all_services

    filtered_services = filtered_services.distinct()

    header = (
        "Número de servicio",
        "Nombre",
        "RUT",
        "Vencimiento",
        "Plan",
        "Valor a cobrar",
        "Costo de activación",
        "Dirección (old)",
        "Calle",
        "Número",
        "Departamento",
        "Torre",
        "Fecha de activación",
        "Fecha de instalación",
        "Fecha de término",
        "Email",
        "Tipo de documento",
        "Vendedor",
        "Balance del cliente",
        "Notas del servicio",
        "Comuna (old)",
        "Ubicacion (new)",
        "Notas de facturación",
        "Operador",
    )
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0

    for service in filtered_services:
        try:
            formatted_activation_date = formats.date_format(
                service.activated_on, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_activation_date = None

        try:
            formatted_installation_date = formats.date_format(
                service.installed_on, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_installation_date = None

        try:
            formatted_expiration_date = formats.date_format(
                service.expired_on, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_expiration_date = None

        worksheet.write(row, 0, service.number)
        worksheet.write(row, 1, service.customer.name)
        worksheet.write(row, 2, service.customer.rut)
        worksheet.write(row, 3, service.due_day)
        worksheet.write(row, 4, service.plan.name)
        worksheet.write(row, 5, service.get_price_display)
        worksheet.write(row, 6, service.get_activation_fee_display)
        worksheet.write(row, 7, service.best_old_address)
        worksheet.write(row, 8, service.best_street)
        worksheet.write(row, 9, service.best_house_number)
        worksheet.write(row, 10, service.best_apartment_number)
        worksheet.write(row, 11, service.best_tower)

        if service.activated_on and service.activated_on > timezone.now():
            worksheet.write(row, 12, formatted_activation_date, red)
        else:
            worksheet.write(row, 12, formatted_activation_date)

        worksheet.write(row, 13, formatted_installation_date)
        worksheet.write(row, 14, formatted_expiration_date)
        worksheet.write(row, 15, service.customer.email)
        worksheet.write(row, 16, service.get_document_type_display())
        worksheet.write(row, 17, getattr(service.seller, "name", ""))
        worksheet.write(row, 18, "$" + intcomma(service.customer.balance.balance))
        worksheet.write(row, 19, service.notes)
        worksheet.write(row, 20, service.customer.commune)
        worksheet.write(row, 21, service.best_location)
        worksheet.write(row, 22, service.billing_notes)
        worksheet.write(row, 23, service.operator.name)
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="servicios.xlsx"'
    return response


@login_required
@permission_required("customers.export_service")
def export_services_one_more_time(request):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    # filtering explicit here
    search = request.GET.get("search_query", "").replace("+", "")

    raw_network_status = request.GET.get("network_status", None)
    raw_balance_status = request.GET.get("balance_status", None)

    if raw_network_status:
        try:
            network_status = int(raw_network_status)
        except ValueError:
            network_status = None
    else:
        network_status = None

    if raw_balance_status:
        try:
            balance_status = int(raw_balance_status)
        except ValueError:
            balance_status = None
    else:
        balance_status = None

    raw_service_status = request.GET.get("service_status", None)

    if raw_service_status:
        try:
            service_status = int(raw_service_status)
        except ValueError:
            service_status = None
    else:
        service_status = None

    raw_customer_kind = request.GET.get("customer_kind", None)
    if raw_customer_kind:
        try:
            customer_kind = int(raw_customer_kind)
        except ValueError:
            customer_kind = None
    else:
        customer_kind = None
    all_services = Service.objects.select_related("customer", "plan")
    if network_status:
        NETWORK_STATUSES = {
            1: {"seen_connected": True},
            2: {"seen_connected": False},
            3: {"seen_connected": None},
        }
        all_services = all_services.filter(**NETWORK_STATUSES.get(network_status, {}))
    if balance_status:
        BALANCE_STATUSES = {
            1: {"customer__balance__balance__gt": 0},
            2: {"customer__balance__balance": 0},
            3: {"customer__balance__balance__lt": 0},
        }
        all_services = all_services.filter(**BALANCE_STATUSES.get(balance_status, {}))
    if service_status:
        all_services = all_services.filter(status=service_status)
    if customer_kind:
        all_services = all_services.filter(customer__kind=customer_kind)

    if search:
        rut_regex = "^" + "\.*".join(list(search))

        q_objects = Q()
        q_objects |= Q(customer__rut__iregex=rut_regex)
        q_objects |= Q(number__icontains=search)
        q_objects |= Q(customer__email__icontains=search)
        q_objects |= Q(address__unaccent__icontains=search)
        q_objects |= Q(customer__address__unaccent__icontains=search)
        q_objects |= Q(customer__commune__unaccent__icontains=search)

        splitted_name_queries = [
            Q(customer__name__unaccent__icontains=term) for term in search.split(" ")
        ]

        q_name_objects = Q()
        for q in splitted_name_queries:
            q_name_objects.add(q, Q.AND)

        q_objects |= q_name_objects

        filtered_services = all_services.filter(q_objects)
    else:
        filtered_services = all_services

    filtered_services = filtered_services.distinct()

    header = (
        "Número de servicio",
        "Nombre",
        "Email",
        "Teléfono",
        "Dirección",  # best address
        "Calle",  # best street
        "Número",  # best number
        "Dpto",  # best dtpo
        "Torre",  # best tower
        "Comuna",
    )  # best location
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)

    row = 1
    col = 0

    for service in filtered_services:
        try:
            formatted_activation_date = formats.date_format(
                service.activated_on, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_activation_date = None

        try:
            formatted_installation_date = formats.date_format(
                service.installed_on, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_installation_date = None

        try:
            formatted_expiration_date = formats.date_format(
                service.expired_on, "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_expiration_date = None

        worksheet.write(row, 0, service.number)
        worksheet.write(row, 1, service.customer.name)
        worksheet.write(row, 2, service.customer.email)
        worksheet.write(row, 3, service.customer.phone)
        worksheet.write(row, 4, service.best_address)
        worksheet.write(row, 5, service.best_street)
        worksheet.write(row, 6, service.best_house_number)
        worksheet.write(row, 7, service.best_apartment_number)
        worksheet.write(row, 8, service.best_tower)
        worksheet.write(row, 9, service.best_location)
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="servicios.xlsx"'
    return response


class CustomerDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Customer
    permission_required = "customers.view_customer"
    
    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        servicios = Service.objects.filter(customer=self.object)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.company==self.object.company.id:
                    can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail

        if servicios.count() == 1:
            ctx["service_preselect"] = servicios[0]

        ctx['warning_contrato'] = ''
        if self.object.flag:
            if "aceptacion_de_contrato" in self.object.flag.keys():
                ctx['warning_contrato'] = self.object.flag['aceptacion_de_contrato']

        try:
            h = {"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'}
            data = {
                "rut": self.object.rut
            }
            url = getattr(config, "iris_server_contact") +'api/v1/customers/customer/'
            send = requests.get(
                url=url,
                params=data,
                headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
                verify=False
            )

            phone_list = []
            email_list = []
            phones = 0
            emails = 0
            if send.json():
                phones = len(send.json()[0]['phones'])
                phone_list = send.json()[0]['phones']
                emails = len(send.json()[0]['emails'])
                email_list = send.json()[0]['emails']

            if phones == 0 and self.object.phone:
                phone_list.append(self.object.phone)
                phones += 1
            if emails == 0 and self.object.email:
                email_list.append(self.object.email)
                emails += 1

            maximo = max(phones, emails)
            conts = []
            for i in range(0,maximo):
                if phones >= i+1 and emails >= i+1:
                    conts.append((phone_list[i],email_list[i]))
                elif phones >= i+1 and emails < i+1:
                    conts.append((phone_list[i], ''))
                elif phones < i+1 and emails >= i+1:
                    conts.append(('',email_list[i]))
            ctx["contactos"] = conts

        except:
            conts = []
            if self.object.phone and self.object.email:
                conts.append((self.object.phone, self.object.email))
            elif self.object.phone:
                conts.append((self.object.phone, ''))
            elif self.object.email:
                conts.append(('', self.object.email))

            ctx["contactos"] = conts
            messages.add_message(
                self.request,
                messages.WARNING,
                "No se pudo establecer comunicación con Iris, se mostrará la información de contacto de Matrix",
            )
            
        return ctx


customer_detail = CustomerDetail.as_view()


class CustomerCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Customer
    form_class = CustomerCreateForm
    template_name = "customers/customer_create.html"
    permission_required = "customers.add_customer"

    def get_context_data(self, **kwargs):
        ctx = super(CustomerCreate, self).get_context_data(**kwargs)
        ctx["commercial_activities"] = CommercialActivity.objects.all()
        if self.request.method == "POST":
            ctx["inlines"] = ContactFormSet(self.request.POST, instance=self.object)
        else:
            ctx["inlines"] = ContactFormSet(instance=self.object)
            ctx["commercial_activities"] = CommercialActivity.objects.all()
        return ctx

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(CustomerCreate, self).get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        
        inlines = context["inlines"]

        self.object = form.save()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                self.object.company=Company.objects.get(id=op.company)
                self.object.save()
            except:
                pass
        
        if inlines.is_valid():
            inlines.instance = self.object
            inlines.save()
        
        ## Esto debe ser una tarea en segundo plano y guardar cuando no se le envie a Iris en la conf del servicio
        data={
            "operator": 2,
            "rut": str(self.object.rut),
            "web": None,
            "facebook": None,
            "instagram": None,
            "twitter": None,
            "creator": None,
            "updater": None
        }
        #print(data)
        try:
            url = getattr(config, "iris_server") + getattr(config, "iris_create_customer")
            #print(url)
            send = requests.post(
                url=url,
                json=data,
                headers={"Content-Type": "application/json","Authorization": getattr(config, "invoices_to_iris_task_token")},
                timeout=60,
            )
            print(send)
            print(send.json())
            if "Error" in send.json() or "error" in send.json():
                print("ERROR")
            if "ID" in send.json() or "id" in send.json():
                try:
                    #Agregando el email.
                    url = getattr(config, "iris_server") + getattr(config, "iris_add_email").format(ID=send.json()["ID"])
                    print(url)
                    data={
                        "email": str(self.object.email)
                    }
                    send = requests.post(
                        url=url,
                        json=data,
                        headers={"Content-Type": "application/json","Authorization": getattr(config, "invoices_to_iris_task_token")},
                        timeout=60,
                    )
                    print(send)
                    print(send.text)
                except Exception as e:
                    print("e",e)
                
                try:
                    #Agregando el tlf.
                    url = getattr(config, "iris_server") + getattr(config, "iris_add_phone").format(ID=send.json()["ID"])
                    #print(url)
                    data={
                        "phone": str(self.object.phone)
                    }
                    send = requests.post(
                        url=url,
                        json=data,
                        headers={"Content-Type": "application/json","Authorization": getattr(config, "invoices_to_iris_task_token")},
                        timeout=60,
                    )
                    print(send)
                    print(send.text)
                except Exception as e:
                    print("e",e)
        except Exception as e:
            print("e",e)
        
        
        messages.add_message(
            self.request, messages.SUCCESS, "Se ha creado un nuevo cliente."
        )
        return super(CustomerCreate, self).form_valid(form)

    def get_success_url(self, *args, **kwargs):
        if self.request.user.has_perm("customers.view_customer"):
            return super(CustomerCreate, self).get_success_url()
        elif self.request.user.has_perm("customers.search_customer"):
            return reverse("customer-list")  # FIXME: must be customer-search
        else:
            return reverse("customer-list")


customer_create = CustomerCreate.as_view()


class CustomerUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Customer
    form_class = CustomerUpdateForm
    success_message = "Se ha actualizado la información del cliente."
    template_name = "customers/customer_update.html"
    permission_required = "customers.change_customer"

    def get_context_data(self, **kwargs):
        ctx = super(CustomerUpdate, self).get_context_data(**kwargs)
        ctx["commercial_activities"] = CommercialActivity.objects.all()
        ctx["active_commercial_activity"] = self.object.commercial_activity
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    # Duda de cuando no tiene servicio todavia
                    can_show_detail=True
                else:   
                    if servicios.filter(operator__id=op.operator).count()!=0:
                        can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        if self.request.method == "POST":
            ctx["inlines"] = ContactFormSet(self.request.POST, instance=self.object)
        else:
            ctx["inlines"] = ContactFormSet(instance=self.object)
        return ctx

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(CustomerUpdate, self).get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        inlines = context["inlines"]

        self.object = form.save()
        if inlines.is_valid():
            inlines.instance = self.object
            inlines.save()

        return super(CustomerUpdate, self).form_valid(form)


customer_update = CustomerUpdate.as_view()


class ProspectDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Prospect
    permission_required = "customers.view_prospect"


prospect_detail = ProspectDetail.as_view()


class ProspectCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Prospect
    form_class = ProspectForm
    template_name = "customers/prospect_form.html"
    permission_required = "customers.add_prospect"

    # def get_form(self, *args, **kwargs):
    #    return self.form_class(initial={'address': {'hola': 1}})


prospect_create = ProspectCreate.as_view()


class ProspectUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Prospect
    form_class = ProspectForm
    success_message = "Se ha actualizado la información del prospecto."
    template_name = "customers/prospect_form.html"
    permission_required = "customers.change_prospect"


prospect_update = ProspectUpdate.as_view()


class ProspectList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Prospect
    queryset = Prospect.objects.all()[:40]  # FIXME
    permission_required = "customers.list_prospect"

    def get_context_data(self, *args, **kwargs):
        ctx = super(ProspectList, self).get_context_data(*args, **kwargs)
        ctx["can_export"] = self.request.user.has_perm("prospects.can_export_services")
        ctx["plans"] = Plan.objects.all()
        return ctx


prospect_list = ProspectList.as_view()


def prepare_prospect(prospect):
    return {
        "DT_RowId": prospect.pk,
        "rut": prospect.rut,
        "name": prospect.truncated_name,
        "location": prospect.get_location_display(),
        "email": prospect.email,
        "address": prospect.address,
        "phone": prospect.phone,
        "current_company": prospect.current_company,
    }


@login_required
@permission_required("customers.list_prospect")
def prospect_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "id",
            "1": "rut",
            "2": "name",
            "3": "location",
            "4": "email",
            "5": "address",
            "6": "phone",
            "7": "current_company",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))
        

        if order_column and order_dir:
            all_prospects = Prospect.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_prospects = Prospect.objects.all()
        all_count = all_prospects.count()

        if search:
            rut_regex = "^" + "\.*".join(list(search))
            filtered_prospects = all_prospects.filter(
                Q(rut__iregex=rut_regex)
                | Q(name__unaccent__icontains=search)
                | Q(email__icontains=search)
                | Q(current_company__unaccent__icontains=search)
            )
        else:
            filtered_prospects = all_prospects

        filtered_prospects = filtered_prospects.distinct()

        filtered_count = filtered_prospects.count()

        if length == -1:
            sliced_prospects = filtered_prospects
        else:
            sliced_prospects = filtered_prospects[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_prospect, sliced_prospects)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


class CustomerList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Customer
    queryset = Customer.objects.all()[:40]  # FIXME
    permission_required = "customers.list_customer"

    def get_context_data(self, *args, **kwargs):
        ctx = super(CustomerList, self).get_context_data(*args, **kwargs)
        ctx["can_export"] = self.request.user.has_perm("customers.export_service")
        ctx["plans"] = Plan.objects.all()
        ctx["operators"] = Operator.objects.all()
        return ctx


customer_list = CustomerList.as_view()


def prepare_customer(customer):
    return {
        "DT_RowId": customer.pk,
        "rut": customer.rut,
        "service_number": customer.service_numbers,
        "services": customer.service_ids,
        "name": customer.truncated_name,
        "email": customer.first_email,
        "address": customer.address,
        "commune": customer.commune,
        "status": customer.aggregated_network_status,
        "status_class": customer.aggregated_network_status_class,
        "balance": price_tag(customer.balance.balance),
        "operator": str(customer.operators_ids),
    }


@cache_page(CACHE_TTL)
@login_required
@permission_required("customers.list_customer")
def customer_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "id",
            "1": "rut",
            "2": "complete_name",
            "3": "email",
            "4": "db_composite_address",
            "5": "commune",
            "6": "kind",
            "8": "balance__balance",
            "9": "operator",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        raw_operator = request.GET.get("operator", None)
        if raw_operator:
            try:
                operator = int(raw_operator)
            except ValueError:
                operator = None
        else:
            operator = None

        raw_type_client = request.GET.get("id_type_client", None)
        if raw_type_client:
            try:
                type_client = int(raw_type_client)
            except ValueError:
                type_client = None
        else:
            type_client = None

        raw_network_status = request.GET.get("network_status", None)
        raw_balance_from = request.GET.get("balance_from", "null")
        raw_balance_to = request.GET.get("balance_to", "null")

        if raw_balance_from:
            try:
                balance_from = int(raw_balance_from)
            except ValueError:
                balance_from = "null"
        else:
            balance_from = "null"
        if raw_balance_to:
            try:
                balance_to = int(raw_balance_to)
            except ValueError:
                balance_to = "null"
        else:
            balance_to = "null"

        if raw_network_status:
            try:
                network_status = int(raw_network_status)
            except ValueError:
                network_status = None
        else:
            network_status = None
        raw_service_status = request.GET.get("service_status", None)

        if raw_service_status:
            try:
                service_status = int(raw_service_status)
            except ValueError:
                service_status = None
        else:
            service_status = None

        raw_plan = request.GET.get("plan", None)

        if raw_plan:
            try:
                plan = int(raw_plan)
            except ValueError:
                plan = None
        else:
            plan = None

        all_customers = Customer.objects.all()

        if order_column and order_dir:
            all_customers = Customer.objects.all().annotate(
                db_composite_address=Concat(
                    "street",
                    Value(" "),
                    "house_number",
                    Value(" dpto "),
                    "apartment_number",
                    Value(" torre "),
                    "tower",
                )
            ).annotate(
                complete_name = Concat(
                    F("first_name"),
                    Value(" "),
                    F("last_name"),
                )
            ).order_by(orderings[order_dir] + columns[order_column])
        else:
            all_customers = Customer.objects.all().annotate(
                db_composite_address=Concat(
                    "street",
                    Value(" "),
                    "house_number",
                    Value(" dpto "),
                    "apartment_number",
                    Value(" torre "),
                    "tower",
                )
            ).annotate(
                complete_name = Concat(
                    F("first_name"),
                    Value(" "),
                    F("last_name"),
                )
            )

        if operator:
            if operator==-1:
                all_customers = all_customers.filter(service__isnull=True)
            else:
                all_customers = all_customers.filter(service__operator__pk=operator)
        else:
            username = None
            context={}
            if request.user.is_authenticated():
                username = request.user.username
                user=User.objects.get(username=username)
                try:
                    op=UserOperator.objects.get(user=user)
                    if op.operator!=0:
                        all_customers = all_customers.filter(service__operator__id=op.operator)
                    else:
                        all_customers = all_customers.filter(service__operator__company__id=op.company)
                except:
                    pass

        
        if network_status:
            NETWORK_STATUSES = {
                1: {"service__seen_connected": True},
                2: {"service__seen_connected": False},
                3: {"service__seen_connected": None},
            }
            all_customers = all_customers.filter(
                **NETWORK_STATUSES.get(network_status, {})
            )

        if balance_from != "null" and balance_to != "null":
            all_customers = all_customers.select_related('balance').filter(
                Q(balance__balance__gte=balance_from),
                Q(balance__balance__lte=balance_to),
            )
        else:
            if balance_from != "null":
                all_customers = all_customers.select_related('balance').filter(balance__balance__gte=balance_from)

            if balance_to != "null":
                all_customers = all_customers.select_related('balance').filter(balance__balance__lte=balance_to)

        if service_status:
            all_customers = all_customers.filter(service__status=service_status)
        if plan:
            all_customers = all_customers.filter(service__plan__pk=plan)

        if type_client and type_client != 0:
            all_customers = all_customers.filter(kind=type_client)

        all_count = all_customers.count()

        if search:
            rut_regex = "^" + "\.*".join(list(search))
            
            q_objects = Q()
            q_objects |= Q(rut__iregex=rut_regex)
            q_objects |= Q(service__number__icontains=search)
            q_objects |= Q(email__icontains=search)
            q_objects |= Q(db_composite_address__unaccent__icontains=search)
            # q_objects |= Q(street__unaccent__icontains=search)
            # q_objects |= Q(commune__unaccent__icontains=search)
            q_objects |= Q(service__operator__name__icontains=search)
            splitted_name_queries = [
                Q(complete_name__unaccent__icontains=term) for term in search.split(" ")
            ]
            q_name_objects = Q()
            for q in splitted_name_queries:
                q_name_objects.add(q, Q.AND)
                
            q_objects |= q_name_objects
            
            filtered_customers = all_customers.filter(q_objects)
        else:
            filtered_customers = all_customers

        filtered_customers = filtered_customers.distinct()

        filtered_count = filtered_customers.count()

        if length == -1:
            sliced_customers = filtered_customers
        else:
            sliced_customers = filtered_customers[start : start + length]

        
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_customer, sliced_customers)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_service(service):
    return {
        "DT_RowId": service.id,
        "number": service.number,
        "address": service.best_address,
        "status": service.get_status_display(),
    }


@login_required
@permission_required("customers.list_service")
def service_ajax(request):
    if request.is_ajax():
        node = request.GET.get("node", None)

        # ordering
        columns = {"0": "number", "1": "address", "2": "status", "9": "operator"}
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        raw_service_status = request.GET.get("service_status", None)

        if raw_service_status:
            try:
                service_status = int(raw_service_status)
            except ValueError:
                service_status = None
        else:
            service_status = None

        if order_column and order_dir:
            all_services = Service.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_services = Service.objects.all()
        if service_status:
            all_services = all_services.filter(status=service_status)

        if node:
            filtered_services = all_services.filter(node__pk=node)
        else:
            filtered_services = all_services

        filtered_services = filtered_services.distinct()

        filtered_count = filtered_services.count()
        all_count = filtered_services.count()

        if length == -1:
            sliced_services = filtered_services
        else:
            sliced_services = filtered_services[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_service, sliced_services)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.view_service")
def service_extras(request, slug):
    service = get_object_or_404(Service, pk=slug)
    customer = get_object_or_404(Customer, service=service.pk)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    context = {
        "can_show_detail":can_show_detail,
        "customer": customer,
        "service": service,
        "object": service,
        "descuentos": service.credit_set.filter(
            status_field=False, cleared_at__isnull=True
        ).order_by("-created_at"),
        "cargos": service.charge_set.filter(
            status_field=False, cleared_at__isnull=True
        ).order_by("-created_at"),
    }
    return render(request, "customers/service_extras.html", context)


@login_required
@permission_required("customers.view_service")
def service_extras_history(request, slug):
    service = get_object_or_404(Service, pk=slug)
    customer = get_object_or_404(Customer, service=service.pk)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    # Get all credits that has been applied completely.
    descuentos = (
        service.credit_set.filter(status_field=False, cleared_at__isnull=False)
        .annotate(boletas=Count("service"))
        .order_by("-created_at")
    )
    for i in descuentos:
        aplicada = CreditApplied.objects.filter(credit_applied=i.id)
        boletas = Invoice.objects.none()
        for j in aplicada:
            boletas = boletas | j.invoice_set.all()
        i.boletas = boletas
        i.save()

    # Get all charges that has been applied completely.
    cargos = (
        service.charge_set.filter(status_field=False, cleared_at__isnull=False)
        .annotate(boletas=Count("service"))
        .order_by("-created_at")
    )
    for i in cargos:
        aplicada = ChargeApplied.objects.filter(charge_applied=i.id)
        boletas = Invoice.objects.none()
        for j in aplicada:
            boletas = boletas | j.invoice_set.all()
        i.boletas = boletas
        i.save()

    context = {
        "can_show_detail":can_show_detail,
        "customer": customer,
        "service": service,
        "object": service,
        "descuentos": descuentos,
        "cargos": cargos,
    }
    return render(request, "customers/service_extras_history.html", context)


@login_required
@permission_required("customers.change_service")
def create_service_extra_credit(request, slug):
    service = get_object_or_404(Service, pk=slug)

    reason = MainReason.objects.filter(active=True, status_field=False, kind=1).exclude(
        kind=3
    )
    # Filtrando por operador de servicio
    reason=reason.filter(operator=service.operator)
    need_days = False
    if reason.count() != 0:
        # To know if the first reason needs the attribute calculated_by_days
        need_days = reason[0].calculate_by_days

    if request.method == "POST":
        form = CreateCreditForm(request.POST)
        if form.is_valid():
            reason = form.cleaned_data["reason"]
            number_billing = form.cleaned_data["number_billing"]
            if not number_billing:
                number_billing = 1
            if reason.calculate_by_days:
                days_count = form.cleaned_data["days_count"]
                Credit.objects.create(
                    service=service,
                    reason=reason,
                    number_billing=number_billing,
                    days_count=days_count,
                    quantity=1,
                    permanent=form.cleaned_data["permanent"],
                )
            else:
                Credit.objects.create(
                    service=service,
                    reason=reason,
                    number_billing=number_billing,
                    quantity=1,
                    permanent=form.cleaned_data["permanent"],
                )
            messages.add_message(
                request, messages.SUCCESS, "Se ha añadido un nuevo descuento."
            )
            return redirect(reverse("service-extras", kwargs={"slug": service.slug}))
    else:
        form = CreateCreditForm()

    context = {
        "customer": service.customer,
        "service": service,
        "object": service,
        "reason": reason,
        "need_days": need_days,
        "cancel_url": reverse("service-extras", kwargs={"slug": service.slug}),
        "form": form,
    }
    return render(request, "customers/service_extra_form.html", context)


@login_required
@permission_required("customers.change_service")
def create_service_extra_charge(request, slug):
    service = get_object_or_404(Service, pk=slug)
    reason = MainReason.objects.filter(active=True, status_field=False, kind=2).exclude(
        kind=3
    )

    # Filtrando por operador de servicio
    reason=reason.filter(operator=service.operator)

    need_days = False
    if reason.count() != 0:
        # To know if the first reason needs the attribute calculated_by_days
        need_days = reason[0].calculate_by_days

    if request.method == "POST":
        form = CreateChargeForm(request.POST)
        if form.is_valid():
            reason = form.cleaned_data["reason"]
            number_billing = form.cleaned_data["number_billing"]
            if not number_billing:
                number_billing = 1
            quantity = form.cleaned_data["quantity"]
            if reason.calculate_by_days:
                days_count = form.cleaned_data["days_count"]
                Charge.objects.create(
                    service=service,
                    reason=reason,
                    number_billing=number_billing,
                    quantity=quantity,
                    days_count=days_count,
                    permanent=form.cleaned_data["permanent"],
                )
            else:
                Charge.objects.create(
                    service=service,
                    reason=reason,
                    number_billing=number_billing,
                    quantity=quantity,
                    permanent=form.cleaned_data["permanent"],
                )
            messages.add_message(
                request, messages.SUCCESS, "Se ha añadido un nuevo cargo."
            )
            return redirect(reverse("service-extras", kwargs={"slug": service.slug}))
    else:
        form = CreateChargeForm()

    context = {
        "customer": service.customer,
        "service": service,
        "object": service,
        "reason": reason,
        "cancel_url": reverse("service-extras", kwargs={"slug": service.slug}),
        "form": form,
    }
    return render(request, "customers/service_extra_form.html", context)


@login_required
@permission_required("customers.change_service")
def update_service_extra_credit(request, slug, credit_pk):
    service = get_object_or_404(Service, pk=slug)
    credit = get_object_or_404(Credit, pk=credit_pk)

    reason = (
        MainReason.objects.filter(active=True, status_field=False, kind=1)
        .exclude(kind=3)
        .exclude(reason=credit.reason)
    )
    # Filtrando por operador de servicio
    reason=reason.filter(operator=service.operator)
    if request.method == "POST":
        form = CreditUpdateForm(request.POST)
        if form.is_valid():
            number_billing = form.cleaned_data["number_billing"]
            # Muestra mensaje de error si quiere cambiar la cantidad de facturas a aplicar
            # a una menor o igual a la ya aplicada.
            if credit.times_applied >= number_billing:
                messages.add_message(
                    request,
                    messages.ERROR,
                    "No se puede editar para que la cantidad de ciclos de facturación sea menor o igual a la de las veces aplicadas desde este formulario.",
                )
                form = CreditUpdateForm()
            else:
                reason = form.cleaned_data["reason"]

                if reason.calculate_by_days:
                    days_count = form.cleaned_data["days_count"]
                    Credit.objects.update_or_create(
                        pk=credit_pk,
                        defaults={
                            "reason": reason,
                            "number_billing": number_billing,
                            "days_count": days_count,
                            "permanent": form.cleaned_data["permanent"],
                        },
                    )
                else:
                    Credit.objects.update_or_create(
                        pk=credit_pk,
                        defaults={
                            "reason": reason,
                            "number_billing": number_billing,
                            "permanent": form.cleaned_data["permanent"],
                        },
                    )
                messages.add_message(
                    request, messages.SUCCESS, "Se ha editado el descuento."
                )
                return redirect(
                    reverse("service-extras", kwargs={"slug": service.slug})
                )
    else:
        form = CreditUpdateForm()

    context = {
        "customer": service.customer,
        "service": service,
        "object": service,
        "obj": credit,
        "reason": reason,
        "cancel_url": reverse("service-extras", kwargs={"slug": service.slug}),
        "form": form,
    }
    return render(request, "customers/service_extra_update.html", context)


@login_required
@permission_required("customers.change_service")
def update_service_extra_charge(request, slug, charge_pk):
    service = get_object_or_404(Service, pk=slug)
    charge = get_object_or_404(Charge, pk=charge_pk)
    reason = (
        MainReason.objects.filter(active=True, status_field=False, kind=2)
        .exclude(kind=3)
        .exclude(reason=charge.reason)
    )
    # Filtrando por operador de servicio
    reason=reason.filter(operator=service.operator)
    if request.method == "POST":
        form = ChargeUpdateForm(request.POST)
        if form.is_valid():
            number_billing = form.cleaned_data["number_billing"]
            # Muestra mensaje de error si quiere cambiar la cantidad de facturas a aplicar
            # a una menor o igual a la ya aplicada.
            if charge.times_applied >= number_billing:
                messages.add_message(
                    request,
                    messages.ERROR,
                    "No se puede editar para que la cantidad de ciclos de facturación sea menor o igual a la de las veces aplicadas desde este formulario.",
                )
                form = ChargeUpdateForm()
            else:
                reason = form.cleaned_data["reason"]
                quantity = form.cleaned_data["quantity"]

                if reason.calculate_by_days:
                    days_count = form.cleaned_data["days_count"]
                    Charge.objects.update_or_create(
                        pk=charge_pk,
                        defaults={
                            "reason": reason,
                            "number_billing": number_billing,
                            "quantity": quantity,
                            "days_count": days_count,
                            "permanent": form.cleaned_data["permanent"],
                        },
                    )
                else:
                    Charge.objects.update_or_create(
                        pk=charge_pk,
                        defaults={
                            "reason": reason,
                            "number_billing": number_billing,
                            "quantity": quantity,
                            "permanent": form.cleaned_data["permanent"],
                        },
                    )
                messages.add_message(
                    request, messages.SUCCESS, "Se ha editado el cargo."
                )
                return redirect(
                    reverse("service-extras", kwargs={"slug": service.slug})
                )
    else:
        form = ChargeUpdateForm()

    context = {
        "customer": service.customer,
        "service": service,
        "object": service,
        "obj": charge,
        "reason": reason,
        "cancel_url": reverse("service-extras", kwargs={"slug": service.slug}),
        "form": form,
    }
    return render(request, "customers/service_extra_update.html", context)


@login_required
@permission_required("customers.change_service")
def delete_service_credit(request, credit_pk):
    credit = get_object_or_404(Credit, pk=credit_pk)
    credit.status_field = True
    credit.save()
    return redirect(reverse("service-extras", kwargs={"slug": credit.service.slug}))


@login_required
@permission_required("customers.change_service")
def delete_service_charge(request, charge_pk):
    charge = get_object_or_404(Charge, pk=charge_pk)
    charge.status_field = True
    charge.save()
    return redirect(reverse("service-extras", kwargs={"slug": charge.service.slug}))


@login_required
@permission_required("customers.list_mainreason")
def creditChargeList(request):
    context = {}
    return render(request, "customers/reason_applied_list.html", context)


def prepare_reason(reason_applied):
    if reason_applied.reason.kind == 1 or reason_applied.reason.kind == 3 :
        applied = CreditApplied.objects.filter(credit_applied=reason_applied.id)
    else:
        applied = ChargeApplied.objects.filter(charge_applied=reason_applied.id)

    invoice_applied = Invoice.objects.none()
    for j in applied:
        invoice_applied = invoice_applied | j.invoice_set.all()
    invoice_applied = invoice_applied

    if reason_applied.cleared_at:
        date_cleared = reason_applied.cleared_at.strftime("%d-%m-%Y")
    else:
        date_cleared = "Activo"
    ia = serializers.serialize("json", invoice_applied)
    return {
        "reason": reason_applied.reason.reason,
        "valor": price_tag(reason_applied.reason.valor),
        "permanent": reason_applied.permanent,
        "kind": reason_applied.reason.get_kind_display().capitalize(),
        "number_billing": reason_applied.number_billing,
        "times_applied": reason_applied.times_applied,
        "customer": str(reason_applied.service.customer),
        "service_id": reason_applied.service.pk,
        "service": reason_applied.service.number,
        "invoice_applied": ia,
        "percent": reason_applied.reason.percent,
        "uf": reason_applied.reason.uf,
        "created_at": reason_applied.created_at.strftime("%d-%m-%Y"),
        "cleared_at": date_cleared,
    }


@login_required
def reasons_applied_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "reason",
            "1": "valor",
            "2": "kind",
            "3": "number_billing",
            "4": "times_applied",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        kind = request.GET.get("kind", None)
        active = request.GET.get("active", None)
        percent = request.GET.get("percent", None)
        permanent = request.GET.get("permanent", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)

        if order_column and order_dir:
            credits = Credit.objects.filter(status_field=False).order_by(
                orderings[order_dir] + columns[order_column]
            )
            charges = Charge.objects.filter(status_field=False).order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            credits = Credit.objects.filter(status_field=False)
            charges = Charge.objects.filter(status_field=False)

        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    credits=credits.filter(service__operator__company__id=op.company)
                    charges=charges.filter(service__operator__company__id=op.company)
                else:
                    credits=credits.filter(service__operator__id=op.operator)
                    charges=charges.filter(service__operator__id=op.operator)
            except:
                pass

        # Filtra las que no se han aplicado nunca.
        for i in credits:
            applied = CreditApplied.objects.filter(credit_applied=i.id)
            if applied.count() == 0:
                credits = credits.exclude(id=i.id)

        # Get all charges that has been applied completely.
        for i in charges:
            applied = ChargeApplied.objects.filter(charge_applied=i.id)
            if applied.count() == 0:
                charges = charges.exclude(id=i.id)

        if kind == "1":
            charges = Charge.objects.none()
        elif kind == "2":
            credits = Credit.objects.none()
        
        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                credit_applied= (
                CreditApplied.objects.filter(created_at__gte=datetime(year, month, day))
                .order_by("credit_applied_id")
                .values_list("credit_applied_id")
                .distinct("credit_applied_id")
                )
            except pytz.exceptions.AmbiguousTimeError:
                credit_applied= (
                CreditApplied.objects.filter(created_at__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True))
                .order_by("credit_applied_id")
                .values_list("credit_applied_id")
                .distinct("credit_applied_id")
            )
            #print(credit_applied)
            credits = credits.filter(id__in=credit_applied
            )
            try:
                charge_applied= (
                ChargeApplied.objects.filter(created_at__gte=datetime(year, month, day))
                .order_by("charge_applied_id")
                .values_list("charge_applied_id")
                .distinct("charge_applied_id")
                )
            except pytz.exceptions.AmbiguousTimeError:
                charge_applied= (
                ChargeApplied.objects.filter(created_at__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True))
                .order_by("charge_applied_id")
                .values_list("charge_applied_id")
                .distinct("charge_applied_id")
                )
                
            charges = charges.filter(
                id__in=charge_applied
            )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            try:
                credit_applied= (
                CreditApplied.objects.filter(created_at__lte=datetime(year, month, day, 23, 59, 59))
                .order_by("credit_applied_id")
                .values_list("credit_applied_id")
                .distinct("credit_applied_id")
                )
            except pytz.exceptions.AmbiguousTimeError:
                credit_applied= (
                CreditApplied.objects.filter(created_at__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True))
                .order_by("credit_applied_id")
                .values_list("credit_applied_id")
                .distinct("credit_applied_id")
                )
            #print(credit_applied)
            credits = credits.filter(id__in=credit_applied
            )
            try:
                charge_applied= (
                ChargeApplied.objects.filter(created_at__lte=datetime(year, month, day, 23, 59, 59))
                .order_by("charge_applied_id")
                .values_list("charge_applied_id")
                .distinct("charge_applied_id")
            )
            except pytz.exceptions.AmbiguousTimeError:
                charge_applied= (
                ChargeApplied.objects.filter(created_at__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True))
                .order_by("charge_applied_id")
                .values_list("charge_applied_id")
                .distinct("charge_applied_id")
                )
            charges = charges.filter(
                id__in=charge_applied
            )

        if active == "True":
            credits = credits.filter(cleared_at__isnull=True)
            charges = charges.filter(cleared_at__isnull=True)
        elif active == "False":
            credits = credits.filter(cleared_at__isnull=False)
            charges = charges.filter(cleared_at__isnull=False)

        if permanent == "True":
            credits = credits.filter(permanent=True)
            charges = charges.filter(permanent=True)
        elif permanent == "False":
            credits = credits.filter(permanent=False)
            charges = charges.filter(permanent=False)

        if percent == "True":
            credits = credits.filter(reason__percent=True, reason__uf=False)
            charges = charges.filter(reason__percent=True, reason__uf=False)
        elif percent == "False":
            credits = credits.filter(reason__percent=False, reason__uf=False)
            charges = charges.filter(reason__percent=False, reason__uf=False)
        elif percent == "UF":
            credits = credits.filter(reason__uf=True)
            charges = charges.filter(reason__uf=True)

        all_things = list(credits) + list(charges)
        all_reasons = sorted(all_things, key=lambda x: x.created_at, reverse=True)
        # all_reasons = list(chain(credits, charges))
        all_count = len(all_reasons)

        if search:
            filtered_credits_reasons = credits.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(reason__reason__unaccent__icontains=search)
                | Q(service__customer__first_name__unaccent__icontains=search)
                | Q(service__customer__last_name__unaccent__icontains=search)
                | Q(service__number=int(search) if search.isdigit() else None)
                | Q(reason__valor=int(search) if search.isdigit() else None)
            )
            filtered_charges_reasons = charges.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(reason__reason__unaccent__icontains=search)
                | Q(service__customer__first_name__unaccent__icontains=search)
                | Q(service__customer__last_name__unaccent__icontains=search)
                | Q(service__number=int(search) if search.isdigit() else None)
                | Q(reason__valor=int(search) if search.isdigit() else None)
            )
            # Quito los repetidos.
            filtered_credits_reasons = filtered_credits_reasons.distinct()
            filtered_charges_reasons = filtered_charges_reasons.distinct()
            all_things = list(filtered_charges_reasons) + list(filtered_credits_reasons)
            filtered_reasons = sorted(
                all_things, key=lambda x: x.created_at, reverse=True
            )
            # filtered_reasons = list(chain(filtered_credits_reasons, filtered_charges_reasons))
        else:
            filtered_reasons = all_reasons

        filtered_count = len(filtered_reasons)

        if length == -1:
            sliced_reasons = filtered_reasons
        else:
            sliced_reasons = filtered_reasons[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_reason, sliced_reasons)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
def credits_applied_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "reason",
            "1": "valor",
            "2": "created_at",
            "3": "number_billing",
            "4": "times_applied",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        service = request.GET.get("service", None)
        active = request.GET.get("active", None)

        if order_column and order_dir:
            all_credits = Credit.objects.filter(
                status_field=False, service__id=service
            ).order_by(orderings[order_dir] + columns[order_column])
        else:
            all_credits = Credit.objects.filter(status_field=False, service__id=service)

        all_credits = all_credits.order_by("-cleared_at")
        # Filtra las que no se han aplicado nunca.
        for i in all_credits:
            applied = CreditApplied.objects.filter(credit_applied=i.id)
            if applied.count() == 0:
                all_credits = all_credits.exclude(id=i.id)

        if active == "True":
            all_credits = all_credits.filter(cleared_at__isnull=True)
        elif active == "False":
            all_credits = all_credits.filter(cleared_at__isnull=False)

        all_count = all_credits.count()

        if search:
            filtered_credits = all_credits.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(reason__reason__unaccent__icontains=search)
                | Q(reason__valor=int(search) if search.isdigit() else None)
            )

        else:
            filtered_credits = all_credits

        filtered_count = filtered_credits.count()

        if length == -1:
            sliced_reasons = filtered_credits
        else:
            sliced_reasons = filtered_credits[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_reason, sliced_reasons)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
def charges_applied_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "reason",
            "1": "valor",
            "2": "created_at",
            "3": "number_billing",
            "4": "times_applied",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        service = request.GET.get("service", None)
        status = request.GET.get("status", None)

        if order_column and order_dir:
            all_charges = Charge.objects.filter(
                status_field=False, service__id=service
            ).order_by(orderings[order_dir] + columns[order_column])
        else:
            all_charges = Charge.objects.filter(status_field=False, service__id=service)

        all_charges = all_charges.order_by("-cleared_at")
        # Filtra las que no se han aplicado nunca.
        for i in all_charges:
            applied = ChargeApplied.objects.filter(charge_applied=i.id)
            if applied.count() == 0:
                all_charges = all_charges.exclude(id=i.id)

        if status == "True":
            all_charges = all_charges.filter(cleared_at__isnull=True)
        elif status == "False":
            all_charges = all_charges.filter(cleared_at__isnull=False)

        all_count = all_charges.count()

        if search:
            filtered_charges = all_charges.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(reason__reason__unaccent__icontains=search)
                | Q(reason__valor=int(search) if search.isdigit() else None)
            )

        else:
            filtered_charges = all_charges

        filtered_count = filtered_charges.count()

        if length == -1:
            sliced_reasons = filtered_charges
        else:
            sliced_reasons = filtered_charges[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_reason, sliced_reasons)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.view_customer")
def service_list(request, customer_pk):
    customer = get_object_or_404(Customer, pk=customer_pk)
    operators = Operator.objects.all().values()
    operators_json = json.dumps({"data": list(operators)})
    services=customer.service_set.all()
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.company==customer.company.id:
                can_show_detail=True
            if op.operator==0:
                services=services.filter(operator__company__id=op.company)
            else:
                services=services.filter(operator__id=op.operator)
        except:
            pass
    context = {
        "can_show_detail":can_show_detail,
        "customer": customer,
        "services": services,
        "operators": operators,
        "operators_json": operators_json,
    }
    return render(request, "customers/services.html", context)

@login_required
@permission_required("customers.view_service")
def service_detail(request, slug):
    service = get_object_or_404(Service, pk=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    if request.method == "POST":
        form = TagCreateForm(request.POST)
        if form.is_valid():
            service.tags.add(form.cleaned_data["tag_name"])
            messages.add_message(
                request, messages.SUCCESS, "Se ha añadido la etiqueta."
            )

        return redirect(reverse("service-detail", kwargs={"slug": slug}))
    else:
        created_date = ''
        activated_date = ''
        installed_date = ''
        uninstalled_date = ''
        expired_date = ''

        createdKey = 'created_at' + str(slug)
        activatedKey = 'activated_on' + str(slug)
        installedKey = 'installed_on' + str(slug)
        uninstalledKey = 'uninstalled_on' + str(slug)
        expiredKey = "expired_on" + str(slug)

        value_cache = cache.get(createdKey)
        if value_cache:
            created_date = value_cache
        else:
            created_date = service.created_at
            cache.set(createdKey, created_date, timeout = None)
        
        value_cache = cache.get(activatedKey)
        if value_cache:
            activated_date = value_cache
        else:
            activated_date = service.activated_on
            cache.set(activatedKey, activated_date, timeout = None)

        value_cache = cache.get(installedKey)
        if value_cache:
            installed_date = value_cache
        else:
            installed_date = service.installed_on
            cache.set(installedKey, installed_date, timeout = None)
        
        value_cache = cache.get(uninstalledKey)
        if value_cache:
            uninstalled_date = value_cache
        else:
            uninstalled_date = service.uninstalled_on
            cache.set(uninstalledKey, uninstalled_date, timeout = None)

        value_cache = cache.get(expiredKey)
        if value_cache:
            expired_date = value_cache
        else:
            expired_date = service.expired_on
            cache.set(expiredKey, expired_date, timeout = None)


        logs = service.action_log()
        change_by_user = []
        
        for hh in logs:
            if 'changes' in hh.keys():
                if 'status' in hh['changes']:
                    change_by_user.append(hh['user'])
        
        change_by_user.append(logs[len(logs)-1]['user'])
        hstatus = zip(service.status_history,change_by_user)
        aux = list(hstatus)
        hstatus = zip(service.status_history,change_by_user)

        onboard_time = ''
        if aux[0][0].status == 1:
            activated_on = service.activated_on
            three_ago = timezone.now() - timedelta(days = 90)
            if activated_on:
                if three_ago <= activated_on:
                    onboard_time = activated_on 
        
        warning_activated_on = 0
        if service.status == 1:
            three_ago = timezone.now() - timedelta(days = 90)
            if three_ago <= service.created_at:
                if not service.activated_on:
                    warning_activated_on = 1
        
        warning_installation = 0
        if service.status == 4:
            orders = PlanOrder.objects.all().filter(service = service.pk)
            if len(orders) <= 0:
                warning_installation = 1

        try:
            h = {"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'}
            url = getattr(config, "iris_server") + "api/v1/retentions/customer_retention/?service=" + str(service.number)
            
            send = requests.get(
                url=url,
                headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
                verify=False
            )

            is_retention = "No esta en retención"
            if len(send.json()) != 0: 
                is_retention = "No esta en retención, estuvo en retención"
                for info in send.json():
                    if info['status'] == 0:
                        is_retention = "Esta en retención"
        except:
            is_retention = "No hubo conexión con Iris"

        escalation_message = ''
        try: 
            url = getattr(config, "iris_server") + "api/v1/escalation_ti/escalation/?services=[" + str(service.number) + "]&status=1"
            
            send = requests.get(
                url=url,
                headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
                verify=False
            )
            
            escalation = []
            for info in send.json():
                data = {}
                data['ID'] = info['ID']
                data['agent'] = info['agent']
                fecha = info['created'][:16]
                fecha_dt = datetime.strptime(fecha, '%Y-%m-%dT%H:%M')
                data['created'] = fecha_dt.strftime("%d-%m-%Y %H:%M")
                escalation.append(data)
            if len(escalation) == 0:
                escalation_message = 'No tiene escalamientos activos'
        except:
            escalation_message = 'No hubo conexión con Iris'


        context = {
            "can_show_detail":can_show_detail,
            "object": service,
            "service": service,
            "customer": service.customer,
            "created_at": created_date,
            "activated_on": activated_date,
            "installed_on": installed_date,
            "uninstalled_on": uninstalled_date,
            "expired_on": expired_date,
            "onboard_time": onboard_time,
            "network_status": service.network_status,
            "autocomplete_datalist": [
                "{} ({})".format(tag["name"], tag["tag_count"])
                for tag in Tag.objects.annotate(
                    tag_count=Count("taggit_taggeditem_items")
                )
                .order_by("-tag_count")
                .values("name", "tag_count")
            ],
            "tag_create_form": TagCreateForm(),
            "hstatus": hstatus,
            "warning_activated_on": warning_activated_on,
            "warning_installation": warning_installation,
            "is_retention": is_retention,
            "escalation": escalation,
            "escalation_message": escalation_message,
        }
        return render(request, "customers/service_detail.html", context)


@login_required
@permission_required("customers.view_service")
def service_network_detail(request, slug):
    service = get_object_or_404(Service, pk=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    context={"can_show_detail":can_show_detail}
    context["service"] = service
    context["customer"] = service.customer
    return render(request, "customers/service_network_detail.html", context)


@login_required
@permission_required("customers.change_service")
def service_network_activate(request, slug):
    service = get_object_or_404(Service, pk=slug)
    activate_client(service)

    try: 
        config_operator = ConfigOperator.objects.get(operator = service.operator, name=service.operator.name)
        config = config_operator.flag
        start_day = ''
        end_day = ''

        if config:
            if "start_day" in config.keys():
                start_day = config['start_day']
            if  "end_day" in config.keys():
                end_day = config['end_day']

        time_now = timezone.now()
        month = time_now.month
        year = time_now.year

        start_date = datetime(year, month, int(start_day))
        start_date = pytz.UTC.localize(start_date)
        end_date = datetime(year, month, int(end_day))
        end_date = pytz.UTC.localize(end_date)

        if start_date <= time_now and time_now <= end_date and service.seen_connected is not None:
            
            if service.seen_connected == False:
                last_edited_by = ''
                if service.last_edited_by:
                    last_edited_by =  service.last_edited_by.username
                #webhook_url = "https://hooks.slack.com/services/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"
                #webhook_url = "https://hooks.slack.com/services/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"
                webhook_url = config["slack_channel"]
                payload = {
                    'text': "Servicio #{} reconectado".format(service.number),
                    'attachments': [{
                    'fallback': "Servicio #{}".format(service.number),
                    'color': "good",
                    'author_name': last_edited_by,
                    'author_link': "https://optic.matrix2.cl{}#activity_log".format(service.get_absolute_url()),
                    'title': "Servicio #{}".format(service.number),
                    'title_link': "https://optic.matrix2.cl{}".format(service.get_absolute_url()),
                    'text': "{}".format(service.plan.name),
                    'fields': [
                        {'title': "Cliente",
                            'value': service.customer.name,
                            'short': True},
                        {'title': "RUT",
                            'value': service.customer.rut,
                            'short': True},
                        {'title': "Dirección Servicio",
                            'value': service.best_address,
                            'short': True}
                    ],
                    'actions': [
                        {
                            'type': 'button',
                            'text': 'Ver',
                            'url': "https://optic.matrix2.cl{}".format(service.get_absolute_url()),
                            'style': 'primary'
                        },
                        {
                            'type': 'button',
                            'text': 'Editar',
                            'url': "https://optic.matrix2.cl{}edit/".format(service.get_absolute_url())
                        }
                    ]
                }]}
                requests.post(webhook_url, json=payload)
    except Exception as e:
        print(e)
        pass
    
    service.seen_connected = True
    service.save()
    NetworkStatusChange.objects.create(
        service=service, new_state="activado", agent=request.user
    )
    messages.add_message(request, messages.SUCCESS, "Se ha activado el servicio.")
    return redirect(
        request.META.get(
            "HTTP_REFERER", reverse("service-network-detail", args=[service.number])
        )
    )


@login_required
@permission_required("customers.change_service")
def service_network_drop(request, slug):
    service = get_object_or_404(Service, pk=slug)
    drop_client(service)
    service.seen_connected = False
    service.save()
    NetworkStatusChange.objects.create(
        service=service, new_state="cortado", agent=request.user
    )
    messages.add_message(request, messages.SUCCESS, "Se ha cortado el servicio.")
    return redirect(
        request.META.get(
            "HTTP_REFERER", reverse("service-network-detail", args=[service.number])
        )
    )


@login_required
@permission_required("customers.change_service")
@transaction.atomic
def service_network_choose(request, slug, pk):
    service = get_object_or_404(Service, pk=slug)
    service.networkequipment_set.update(primary=False)
    service.networkequipment_set.filter(pk=pk).update(primary=True)
    messages.add_message(request, messages.SUCCESS, "Se ha elegido el equipo primario.")
    return redirect(reverse("service-network-detail", args=[service.number]))


@login_required
@permission_required("customers.change_service")
def service_network_item_add(request, slug):
    service = get_object_or_404(Service, pk=slug)
    
    if request.method == "POST":
        
        form = NetworkEquipmentForm(request.POST)
        try:
            technician = Technician.objects.get(pk=int(request.POST["technician"]))
            # se agrega al tecnico al servicio y se guarda
            service.technician = technician
            service.save()
        except Technician.DoesNotExist:
            technician = None
        except Technician.MultipleObjectsReturned:
            technician = None
        except ValueError:
            # ocurre solo cuando estamos guardando despues
            # de haber guardado el primer equipo que el
            # tecnico del request llega ""
            technician = service.technician

        if form.is_valid():
            # activamos el servicio al momento de agregar un equipo
            # si este no tiene status no instalado
            if service.status == 4:
                service.status = 1
                service.save()
            net = form.save(commit=False)
            net.service = service
            net.save()
            messages.add_message(request, messages.SUCCESS, "Se ha añadido el equipo.")
            return redirect(
                reverse("service-network-detail", kwargs={"slug": service.number})
            )
    else:
        servicio = {"service_id": service.pk}
        form = NetworkEquipmentForm(**servicio)

    context = {
        "object": service,
        "service": service,
        "customer": service.customer,
        "form": form,
        "cancel_url": reverse(
            "service-network-detail", kwargs={"slug": service.number}
        ),
    }
    return render(request, "customers/service_network_item_add.html", context)


@login_required
@permission_required("customers.change_service")
def service_network_item_update(request, slug, pk):
    service = get_object_or_404(Service, pk=slug)
    net = get_object_or_404(NetworkEquipment, pk=pk)

    if request.method == "POST":
        form = NetworkEquipmentForm(request.POST, instance=net)
        if form.is_valid():
            nnet = form.save(commit=False)
            nnet.service = service
            nnet.save()
            messages.add_message(request, messages.SUCCESS, "Se ha editado el equipo.")
            return redirect(
                reverse("service-network-detail", kwargs={"slug": service.number})
            )
    else:
        servicio = {"service_id": service.pk}
        form = NetworkEquipmentForm(instance=net, **servicio)

    context = {
        "object": net,
        "service": service,
        "customer": service.customer,
        "form": form,
        "cancel_url": reverse(
            "service-network-detail", kwargs={"slug": service.number}
        ),
    }
    return render(request, "customers/service_network_item_update.html", context)


@login_required
@permission_required("customers.change_service")
def service_network_item_delete(request, slug, pk):
    net = get_object_or_404(NetworkEquipment, pk=pk)
    net.delete()

    messages.add_message(request, messages.SUCCESS, "Se ha eliminado el equipo.")

    return redirect(reverse("service-network-detail", kwargs={"slug": slug}))


@login_required
@permission_required("customers.view_service")
def service_gallery(request, slug):
    service = get_object_or_404(Service, pk=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    context = {"object": service, "service": service, "customer": service.customer,
        "can_show_detail":can_show_detail}
    return render(request, "customers/service_gallery.html", context)


@login_required
@permission_required("gallery.add_pic")
def service_gallery_add(request, slug):
    service = get_object_or_404(Service, pk=slug)
    if request.method == "POST":
        form = PicForm(request.POST, request.FILES)
        if form.is_valid():
            pic = form.save(commit=False)
            pic.content_object = service
            pic.save()
            messages.add_message(request, messages.SUCCESS, "Se ha subido la imagen.")
            return redirect(reverse("service-gallery", kwargs={"slug": service.number}))
    else:
        form = PicForm()

    context = {
        "object": service,
        "service": service,
        "customer": service.customer,
        "form": form,
        "cancel_url": reverse("service-gallery", kwargs={"slug": service.number}),
    }
    return render(request, "customers/service_gallery_add.html", context)


@login_required
@permission_required("gallery.change_pic")
def service_gallery_update(request, slug, pic_pk):
    service = get_object_or_404(Service, pk=slug)
    pic = get_object_or_404(Pic, pk=pic_pk)

    if request.method == "POST":
        form = PicUpdateForm(request.POST, request.FILES)
        if form.is_valid():
            pic.caption = form.cleaned_data["caption"]
            pic.save()
            messages.add_message(request, messages.SUCCESS, "Se ha editado la imagen.")
            return redirect(reverse("service-gallery", kwargs={"slug": service.number}))
    else:
        form = PicUpdateForm(instance=pic)

    context = {
        "object": pic,
        "service": service,
        "customer": service.customer,
        "form": form,
        "cancel_url": reverse("service-gallery", kwargs={"slug": service.number}),
    }
    return render(request, "customers/service_gallery_update.html", context)


@login_required
def service_gallery_delete(request, slug, pic_pk):
    pic = get_object_or_404(Pic, pk=pic_pk)
    pic.delete()

    messages.add_message(request, messages.SUCCESS, "Se ha eliminado la imagen.")

    return redirect(reverse("service-gallery", kwargs={"slug": slug}))


@login_required
@permission_required("customers.view_service")
def service_documents(request, slug):
    service = get_object_or_404(Service, pk=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    context = {"object": service, "service": service, "customer": service.customer,
        "can_show_detail":can_show_detail}
    return render(request, "customers/service_documents.html", context)


@login_required
@permission_required("documents.add_document")
def service_documents_add(request, slug):
    service = get_object_or_404(Service, pk=slug)
    if request.method == "POST":
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            doc = form.save(commit=False)
            doc.content_object = service
            doc.agent = request.user
            doc.save()
            messages.add_message(
                request, messages.SUCCESS, "Se ha subido el documento."
            )
            return redirect(
                reverse("service-documents", kwargs={"slug": service.number})
            )
    else:
        form = DocumentForm()

    context = {
        "object": service,
        "service": service,
        "customer": service.customer,
        "form": form,
        "cancel_url": reverse("service-documents", kwargs={"slug": service.number}),
    }
    return render(request, "customers/service_documents_add.html", context)


@login_required
@permission_required("documents.change_document")
def service_documents_update(request, slug, document_pk):
    service = get_object_or_404(Service, pk=slug)
    document = get_object_or_404(Document, pk=document_pk)

    if request.method == "POST":
        form = DocumentUpdateForm(request.POST, request.FILES)
        if form.is_valid():
            document.description = form.cleaned_data["description"]
            document.save()
            messages.add_message(
                request, messages.SUCCESS, "Se ha editado el documento."
            )
            return redirect(
                reverse("service-documents", kwargs={"slug": service.number})
            )
    else:
        form = DocumentUpdateForm(instance=document)

    context = {
        "object": document,
        "service": service,
        "customer": service.customer,
        "form": form,
        "cancel_url": reverse("service-documents", kwargs={"slug": service.number}),
    }
    return render(request, "customers/service_documents_update.html", context)


@login_required
@permission_required("customers.change_service")
def service_documents_delete(request, slug, document_pk):
    document = get_object_or_404(Document, pk=document_pk)
    document.delete()

    messages.add_message(request, messages.SUCCESS, "Se ha eliminado el documento.")

    return redirect(reverse("service-documents", kwargs={"slug": slug}))


def prepare_mainreason(mainreason):
    return {
        "reason": mainreason.reason,
        "valor": price_tag(mainreason.valor),
        "percent": mainreason.percent,
        "kind": mainreason.get_kind_display().capitalize(),
        "active": mainreason.active,
        "id_mr": mainreason.id,
        "need_days": mainreason.calculate_by_days,
        "uf": mainreason.uf,
    }


@login_required
def mainreason_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "reason",
            "1": "valor",
            "2": "percent",
            "3": "kind",
            "4": "active",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        operator = request.GET.get("operator", None)
        kind = request.GET.get("kind", None)
        active = request.GET.get("active", None)
        percent = request.GET.get("percent", None)
        calculate_by_days = request.GET.get("calculate_by_days", None)

        if order_column and order_dir:
            all_reasons = MainReason.objects.filter(status_field=False).order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_reasons = MainReason.objects.filter(status_field=False)

        if operator and operator != "null":
            all_reasons = all_reasons.filter(operator=operator)

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_reasons = all_reasons.filter(operator=op.operator)
                else:
                    all_reasons = all_reasons.filter(operator__company__id=op.company)
            except:
                pass

        all_reasons = all_reasons.exclude(kind=3)

        if kind == "1":
            all_reasons = all_reasons.filter(kind=1)
        elif kind == "2":
            all_reasons = all_reasons.filter(kind=2)

        if active == "True":
            all_reasons = all_reasons.filter(active=True)
        elif active == "False":
            all_reasons = all_reasons.filter(active=False)

        if calculate_by_days == "True":
            all_reasons = all_reasons.filter(calculate_by_days=True)
        elif calculate_by_days == "False":
            all_reasons = all_reasons.filter(calculate_by_days=False)

        if percent == "True":
            all_reasons = all_reasons.filter(percent=True, uf=False)
        elif percent == "False":
            all_reasons = all_reasons.filter(percent=False, uf=False)
        elif percent == "UF":
            all_reasons = all_reasons.filter(uf=True)

        all_count = all_reasons.count()

        if search:
            filtered_reasons = all_reasons.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(reason__unaccent__icontains=search)
                | Q(valor=int(search) if search.isdigit() else None)
            )
        else:
            filtered_reasons = all_reasons

        filtered_reasons = filtered_reasons.distinct()

        filtered_count = filtered_reasons.count()

        if length == -1:
            sliced_reasons = filtered_reasons
        else:
            sliced_reasons = filtered_reasons[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_mainreason, sliced_reasons)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


class MainReasonList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = MainReason
    queryset = MainReason.objects.all()[:40]  # FIXME
    permission_required = "customers.list_mainreason"

    def get_context_data(self, *args, **kwargs):
        ctx = super(MainReasonList, self).get_context_data(*args, **kwargs)
        ctx["operators"] = Operator.objects.all()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    ctx["operators"]= Operator.objects.filter(company__id=op.company)
                else:
                    ctx["operators"] = Operator.objects.filter(id=op.operator)
            except:
                pass
        return ctx


mainreason_list = MainReasonList.as_view()


class MainReasonCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = MainReason
    form_class = MainReasonCreateForm
    template_name = "customers/mainreason_form.html"
    permission_required = "customers.list_mainreason"

    def get_context_data(self, **kwargs):
        ctx = super(MainReasonCreate, self).get_context_data(**kwargs)
        ctx["cancel_url"] = reverse("mainreason-list")
        operators=Operator.objects.all()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    ctx["operators"]= Operator.objects.filter(company__id=op.company)
                else:
                    ctx["operators"] = Operator.objects.filter(id=op.operator)
            except:
                pass
        return ctx

    def form_valid(self, form, **kwargs):
        error = False
        if form["percent"].value() and form["uf"].value():
            messages.error(
                self.request, "No puede estar porcentaje y uf al mismo tiempo",
            )
            return HttpResponseRedirect(reverse_lazy("mainreason-create"))
        if form["percent"].value():
            # Verifica que no coloquen un valor mayor a 100, si es en porcentaje.
            if int(form["valor"].value()) > 100:
                error = True
        if error == False:
            self.object = form.save()
            messages.add_message(
                self.request,
                messages.SUCCESS,
                "Se ha creado un nuevo motivo de cargo o descuento.",
            )
            return super(MainReasonCreate, self).form_valid(form)
        else:
            messages.error(
                self.request,
                "El valor debe ser menor o igual a 100 si esta en porcentaje",
            )
            return HttpResponseRedirect(reverse_lazy("mainreason-create"))

    def get_success_url(self, *args, **kwargs):
        return reverse("mainreason-list")


mainreason_create = MainReasonCreate.as_view()


class MainReasonUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = MainReason
    form_class = MainReasonUpdateForm
    slug_field = "id"
    success_message = "Se ha actualizado la información del motivo."
    template_name = "customers/mainreason_update.html"
    permission_required = "customers.list_mainreason"

    def dispatch(self, *args, **kwargs):
        self.reason = get_object_or_404(MainReason, pk=self.kwargs["pk"])
        return super(MainReasonUpdate, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse("mainreason-list")

    def get_context_data(self, *args, **kwargs):
        context = super(MainReasonUpdate, self).get_context_data(*args, **kwargs)
        context["cancel_url"] = reverse("mainreason-list")
        return context

    def form_valid(self, form, **kwargs):
        error = False
        if form["percent"].value() and form["uf"].value():
            messages.error(
                self.request, "No puede estar porcentaje y uf al mismo tiempo",
            )
            return HttpResponseRedirect(
                reverse_lazy("mainreason-update", kwargs={"pk": self.reason.id})
            )
        if form["percent"].value():
            # Verifica que no coloquen un valor mayor a 100, si es en porcentaje.
            if int(form["valor"].value()) > 100:
                error = True
        if error == False:
            self.object = form.save()
            return super(MainReasonUpdate, self).form_valid(form)
        else:
            messages.error(
                self.request,
                "El valor debe ser menor o igual a 100 si esta en porcentaje",
            )
            return HttpResponseRedirect(
                reverse_lazy("mainreason-update", kwargs={"pk": self.reason.id})
            )


mainreason_update = MainReasonUpdate.as_view()


@login_required
def delete_reason(request, pk):

    reason = get_object_or_404(MainReason, pk=pk)
    reason.status_field = True
    reason.save()
    return redirect(reverse("mainreason-list"))


@login_required
@permission_required("customers.view_customer")
def customer_payment_methods(request, customer_pk):
    customer = get_object_or_404(Customer, pk=customer_pk)
    servicios = Service.objects.filter(customer=customer)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.company==customer.company.id:
                can_show_detail=True
            if op.operator==0:
                if servicios.filter(operator__company__id=op.company).count()!=0:
                    can_show_detail=True
            else:
                if servicios.filter(operator__id=op.operator).count()!=0:
                    can_show_detail=True
        except:
            pass
    service_preselect = None
    if servicios.count() == 1:
        service_preselect = servicios[0]

    context = {"customer": customer, "service_preselect": service_preselect, "can_show_detail":can_show_detail}
    return render(request, "customers/customer_payment_methods.html", context)


@login_required
@permission_required("customers.change_customer")
def customer_payment_methods_add(request, customer_pk):
    customer = get_object_or_404(Customer, pk=customer_pk)
    if request.method == "POST":
        form = PayorRutForm(request.POST)
        if form.is_valid():
            payor_rut = form.save(commit=False)
            payor_rut.customer = customer
            payor_rut.save()
            messages.add_message(
                request, messages.SUCCESS, "Se ha creado nuevo RUT pagador."
            )
            return redirect(
                reverse("customer-payment-methods", kwargs={"customer_pk": customer.pk})
            )
    else:
        form = PayorRutForm()

    context = {
        "object": customer,
        "customer": customer,
        "form": form,
        "cancel_url": reverse(
            "customer-payment-methods", kwargs={"customer_pk": customer.pk}
        ),
    }
    return render(request, "customers/customer_payment_methods_add.html", context)


@login_required
@permission_required("customers.change_customer")
def customer_payment_methods_delete(request, customer_pk, pk):
    pr = get_object_or_404(PayorRut, pk=pk)
    pr.delete()

    messages.add_message(request, messages.SUCCESS, "Se ha eliminado el RUT pagador.")

    return redirect(
        reverse("customer-payment-methods", kwargs={"customer_pk": customer_pk})
    )


class ServiceCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Service
    form_class = ServiceCreateForm
    template_name = "customers/service_create.html"
    permission_required = "customers.add_service"

    def get_success_url(self):
        return reverse("service-detail", kwargs={"slug": self.object.id})

    def get_context_data(self, *args, **kwargs):
        context = super(ServiceCreate, self).get_context_data(*args, **kwargs)
        context["customer"] = get_object_or_404(Customer, pk=self.kwargs["customer_pk"])
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    context["operators"]= Operator.objects.filter(company__id=op.company)
                else:
                    context["operators"] = Operator.objects.filter(id=op.operator)
            except:
                pass
        #self.operators_value=context["operators"]
        return context

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(ServiceCreate, self).get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def get_form(self, form_class=ServiceCreateForm):
        form = super(ServiceCreate, self).get_form(form_class)
        form.fields["seller"].queryset = Seller.objects.filter(active=True)
        """try:
            form.fields["number"].initial = (
                Service.objects.filter(number__lt=99000).aggregate(Max("number"))[
                    "number__max"
                ]
                + 1
            )
        except TypeError as e:
            form.fields["number"].initial = 0"""
        return form

    def form_valid(self, form):
        service = form.save(commit=False)
        service.customer_id = self.kwargs["customer_pk"]
        messages.add_message(
            self.request, messages.SUCCESS, "Se ha creado un nuevo servicio."
        )
        return super(ServiceCreate, self).form_valid(form)

    # review this!! should not allow to create for someone non-existent person
    def get_queryset(self, *args, **kwargs):
        qs = super(ServiceCreate, self).get_queryset(*args, **kwargs)
        customer_pk = self.kwargs["customer_pk"]
        return qs.filter(customer__pk=customer_pk)


service_create = ServiceCreate.as_view()



def next_month(x):
    try:
        nextmonthdate = x.replace(month=x.month+1)
        print(nextmonthdate)
        return nextmonthdate
    except ValueError:
        if x.month == 12:
            nextmonthdate = x.replace(year=x.year+1, month=1)
            return nextmonthdate
        else:
            # next month is too short to have "same date"
            # pick your own heuristic, or re-raise the exception:
            raise

def next_month_date(x):
    try:
        nextmonthdate = x.replace(month=x.month+1, day=5)
        print(nextmonthdate)
        return nextmonthdate
    except ValueError:
        if x.month == 12:
            nextmonthdate = x.replace(year=x.year+1, month=1, day=5)
            return nextmonthdate
        else:
            # next month is too short to have "same date"
            # pick your own heuristic, or re-raise the exception:
            raise

class ServiceUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Service
    form_class = ServiceUpdateForm
    slug_field = "id"
    success_message = "Se ha actualizado la información del servicio."
    template_name = "customers/service_update.html"
    permission_required = "customers.change_service"

    def dispatch(self, *args, **kwargs):
        self.service = get_object_or_404(Service, pk=self.kwargs["slug"])
        self.plan = self.service.plan
        self.status = self.service.status
        self.promotions = self.service.promotions.all()
        return super(ServiceUpdate, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse("service-detail", kwargs={"slug": self.object.pk})

    def get_context_data(self, *args, **kwargs):
        context = super(ServiceUpdate, self).get_context_data(*args, **kwargs)
        context["customer"] = self.object.customer
        if self.object.id_pulso:
            direccion=self.object.get_address()
            context["region_actual"]=json.dumps(direccion["region"])
            context["comuna_actual"]=direccion["commune"]
            context["calle_actual"]=direccion["street"]
            context["nro_actual"]=direccion["department"]
            context["dpto_actual"]=direccion["department"]
        else:
            context["region_actual"]=json.dumps("Region")
            context["comuna_actual"]="Comuna"
            context["calle_actual"]="Calle"
            context["nro_actual"]="Nro de Calle"
            context["dpto_actual"]="Nro de Casa o Dpto."
        return context

    def get_form(self, form_class=ServiceUpdateForm):
        form = super(ServiceUpdate, self).get_form(form_class)
        form.fields["seller"].queryset = Seller.objects.filter(active=True)
        form.fields["plan"].queryset = Plan.objects.filter(operator=self.service.operator)
        form.fields["operator"].queryset = Operator.objects.filter(id=self.service.operator.id)

        # Fltrando por activo
        form.fields["promotions"].queryset = Promotions.objects.filter(plan=self.plan, active=True,operator=self.service.operator) | Promotions.objects.filter(plan__isnull=True, active=True,operator=self.service.operator)
        return form

    def get_form_kwargs(self):
        kwargs = super(ServiceUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user,'id': self.object.id})
        return kwargs

    def form_valid(self, form):
        service = form.save(commit=False)
        promociones=Credit.objects.filter(status_field=False, cleared_at__isnull=True, reason__kind=3,service=self.service)
        for promos in promociones:
            if not (promos.reason.promo in form.cleaned_data["promotions"]):
                messages.add_message(
                    self.request, messages.ERROR,
                    "No se pudo quitar promoción porque tiene descuentos asociados en la prefacturación. Elimine los descuentos si desea quitar la promo."
                )
                form.cleaned_data["promotions"]= form.cleaned_data["promotions"] | Promotions.objects.filter(id=promos.reason.promo.id)

        #print(self.promotions)
        #print(form.cleaned_data["promotions"])

        # Obteniendo la diferencia para ver si agregaron nueva promo.
        qs_diff = form.cleaned_data["promotions"].difference(self.promotions) 
        #print(qs_diff)

        if getattr(config, "allow_promotion_anytime")=="no":
            # Si esta en los dias 20-31 del mes no permite asociar nuevas promos.
            today=date.today().day
            can_add_promo=True
            if ( today==20 or today==21 or today==22 or today==23 or today==24 or
                today==25 or today==26 or today==27 or today==28 or today==29 or 
                today==30 or today==31): 
                can_add_promo=False
            if can_add_promo==False:
                form.cleaned_data["promotions"]=self.promotions
        else:
            today=date.today().day
            can_add_promo=True
                
        if qs_diff.count()!=0 and can_add_promo:
            for i in qs_diff:
                today_is=date.today()
                for index in range(i.number_periods):
                    #print(j)
                    motivo=MainReason.objects.get(id=i.info_periods[str(index+1)])
                    #print(motivo.reason)
                    
                    # Falta probar, porque falta la logica para las fechas.
                    if index==0:
                        if motivo.promo.info_periods["permanent"]:
                            Credit.objects.create(
                                    reason = motivo,
                                    service = self.service,
                                    quantity = 1,
                                    active = True,
                                    permanent= True
                            )
                        else:
                            Credit.objects.create(
                                    reason = motivo,
                                    service = self.service,
                                    quantity = 1,
                                    active = True
                            )
                    else:
                        today_is=next_month(today_is).replace(day=19)
                        Credit.objects.create(
                                reason = motivo,
                                service = self.service,
                                quantity = 1,
                                active = False,
                                date_to_set_active = today_is
                        )
        
        # Cuando cambio de estado a por retirar
        if self.status!=2 and service.status==2:
            #print("Paso")
            data ={
                "operator": service.operator.id,
                "rut": str(service.customer.rut),
                "service": int(service.number),
                "serial": None,
                "iclass_id": None,
                "status": 0,
                "status_agendamiento": None,
                "status_storage": None,
                "status_ti": None,
                "slack_thread": None,
                "previous_state": self.status,
                "creator": None,
                "updater": None,
                "agent_ti": None,
                "agent_sac": None,
                "agent_storage": None
            }

            url = getattr(config, "iris_server") + getattr(config, "iris_notify_remove_equipament")
            # print (url)
            # print(data)

            try:
                send = requests.post(
                    url=url,
                    json=data,
                    headers={"Authorization": getattr(config, "invoices_to_iris_task_token")},
                    timeout=60,
                )
                print(send)
                print(send.text)
                if "Error" in send.json() or "error" in send.json():
                    print("ERROR")
            except Exception as e:
                print("e",e)

        # Cuando cambio de plan
        if self.plan!=service.plan:
            info = []
            info.append(
                    {
                        "number": str(service.number),
                        "street": str(service.street),
                        "house_number": str(service.house_number),
                        "apartment_number": str(service.apartment_number),
                        "tower": str(service.tower),
                        "location": str(service.location),
                        "price_override": str(service.price_override),
                        "due_day": str(service.due_day),
                        "activated_on": str(service.activated_on) if service.activated_on else "",
                        "installed_on": str(service.installed_on)  if service.installed_on else "",
                        "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                        "expired_on": str(service.expired_on)  if service.expired_on else "",
                        "status": str(service.get_status_display()),
                        "network_mismatch": "Sí" if service.network_mismatch else "No",
                        "ssid": str(service.ssid),
                        "technology_kind": str(service.get_technology_kind_display()),
                        "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                        "seen_connected": str(service.seen_connected),
                        "mac_onu": str(service.mac_onu),
                        "composite_address": str(service.customer.composite_address),
                        "commune": str(service.node.commune),
                        "node__code": str(service.node.code),
                        "node__address": str(service.node.address),
                        "node__street": str(service.node.street),
                        "node__house_number": str(service.node.house_number),
                        "node__commune": str(service.node.commune),
                        "node__olt_config": str(service.node.olt_config)  if service.node.olt_config else "",
                        "node__is_building": "Sí" if service.node.is_building else "No", 
                        "node__is_optical_fiber": "Sí" if service.node.is_optical_fiber else "No",
                        "node__gpon":  "Sí" if service.node.gpon else "No",
                        "node__gepon":  "Sí" if service.node.gepon else "No",
                        "node__pon_port": str(service.node.pon_port),
                        "node__box_location": str(service.node.box_location),
                        "node__splitter_location": str(service.node.splitter_location),
                        "node__expected_power": str(service.node.expected_power),
                        "node__towers": str(service.node.towers),
                        "node__apartments": str(service.node.apartments),
                        "node__phone": str(service.node.phone),
                        "plan__name": str(service.plan.description_facturacion),
                        "plan__price": str(service.plan.price),
                        "plan__category": str(service.plan.category),
                        "plan__uf":  "Sí" if service.plan.uf else "No",
                        "plan__active": "Sí" if service.plan.active else "No", 
                        "customer__rut": str(service.customer.rut),
                        "customer__name": str(service.customer.name),
                        "customer__first_name": str(service.customer.first_name),
                        "customer__last_name": str(service.customer.last_name),
                        "customer__email": str(service.customer.email),
                        "customer__street": str(service.customer.street),
                        "customer__house_number": str(service.customer.house_number),
                        "customer__apartment_number": str(
                            service.customer.apartment_number
                        ),
                        "customer__tower": str(service.customer.tower),
                        "customer__location": str(service.customer.location),
                        "customer__phone": str(service.customer.phone),
                        "customer__default_due_day": str(
                            service.customer.default_due_day
                        ),
                        "customer__composite_address": str(
                            service.customer.composite_address
                        ),
                        "customer__commune": str(service.customer.commune),
                        "files": [],
                        "invoice__kind":"",
                        "invoice__total_pagar":""
                    }
                )
            data = {"event": getattr(config, "evento_change_plan"), "info": info}
            url = getattr(config, "iris_server") + getattr(config, "invoices_to_iris_task_url")
            # print (url)
            # print(data)

            try:
                send = requests.post(
                    url=url,
                    json=data,
                    headers={"Authorization": getattr(config, "invoices_to_iris_task_token")},
                    timeout=60,
                )
                print(send)
                print(send.text)
                if "Error" in send.json() or "error" in send.json():
                    print("ERROR")
            except Exception as e:
                print("e",e)
        
        return super(ServiceUpdate, self).form_valid(form)

service_update = ServiceUpdate.as_view()


class ServiceDatesUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Service
    form_class = ServiceDatesForm
    template_name = "customers/service_dates_form.html"
    slug_field = "number"
    success_message = "Se han actualizado las fechas del servicio."
    context_object_name = "service"
    permission_required = "customers.change_service"

    def get_context_data(self, *args, **kwargs):
        context = super(ServiceDatesUpdate, self).get_context_data(*args, **kwargs)
        context["customer"] = self.object.customer
        context["cancel_url"] = self.get_success_url()
        return context

    def get_success_url(self):
        return reverse("service-detail", kwargs={"slug": self.object.number})


service_dates_update = ServiceDatesUpdate.as_view()


class ServiceNetworkUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Service
    form_class = ServiceNetworkForm
    template_name = "customers/service_network_form.html"
    slug_field = "number"
    success_message = "Se ha actualizado la información del servicio."
    context_object_name = "service"
    permission_required = "customers.change_service"

    def get_context_data(self, *args, **kwargs):
        context = super(ServiceNetworkUpdate, self).get_context_data(*args, **kwargs)
        context["customer"] = self.object.customer
        return context

    def get_success_url(self):
        return reverse("service-network-detail", kwargs={"slug": self.object.number})


service_network_update = ServiceNetworkUpdate.as_view()


@login_required
def service_network_reset_ssid(request, slug):
    cc = get_object_or_404(Service, pk=slug)
    cc.ssid = "{} - optic.cl".format(cc.number)
    cc.save()
    return redirect(reverse("service-network-update", kwargs={"slug": slug}))


@login_required
def service_network_generate_password(request, slug):
    cc = get_object_or_404(Service, pk=slug)
    initials = "".join([word[0].lower() for word in cc.customer.name.split()])
    if len(initials) < 4:
        initials += "".join(random.sample(string.ascii_lowercase, 4 - len(initials)))
    cc.password = "{}{}".format(initials, cc.number)
    cc.save()
    return redirect(reverse("service-network-update", kwargs={"slug": slug}))


@login_required
def service_support(request, slug):
    context = {}
    return render(request, "customers/service_support.html", context)


@login_required
@permission_required("customers.view_service")
def service_calls(request, slug):
    service = get_object_or_404(Service, pk=slug)
    context = {
        "object": service,
        "service": service,
        "customer": service.customer,
        "calls": service.call_set.all(),
    }
    return render(request, "customers/service_calls.html", context)


@login_required
@permission_required("customers.view_customer")
def invoice_list(request, customer_pk):
    customer = get_object_or_404(Customer, pk=customer_pk)
    servicios = Service.objects.filter(customer=customer)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.company==customer.company.id:
                can_show_detail=True
        except:
            pass
    service_preselect = None
    if servicios.count() == 1:
        service_preselect = servicios[0]
        currency=servicios[0].plan.currency
        show_saldo=True
    if servicios.count()> 1:
        currency=servicios[0].plan.currency
        servicios_currency=servicios.filter(plan__currency=currency)
        # todos los servicios tienen la misma moneda
        if servicios_currency.count()==servicios.count():
            show_saldo=True
        else:
            # Tiene un servicio con diferente moneda
            show_saldo=False 
    context = {
        "show_saldo":show_saldo,
        "currency":currency,
        "can_show_detail":can_show_detail,
        "customer": customer,
        "service_preselect": service_preselect,
        "invoices": Invoice.objects.filter(customer=customer),
        "payments": Payment.objects.filter(customer=customer),
    }
    return render(request, "customers/invoice_list.html", context)


@login_required
@permission_required("customers.view_customer")
def service_billing(request, slug):
    service = get_object_or_404(Service, pk=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    context = {
        "can_show_detail":can_show_detail,
        "object": service,
        "service": service,
        "customer": service.customer,
        "invoices": Invoice.objects.filter(service=service),
        "payments": Payment.objects.filter(service=service),
    }
    return render(request, "customers/service_billing.html", context)

def prepare_typify(get_typify, service):
    typify = []
    agents = {}
    categories = {}
    statuses = {
        "0": "ABIERTO",
        "1": "CERRADO",
        "2": "ESPERANDO RESPUESTA",
        "3": "DAR SEGUIMIENTO",
        "4": "LLAMAR LUEGO",
        "5": "CANCELADO",
    }
    for x in get_typify:
        aux = dict()
        aux['ID'] = x['ID']

        if x['created']:
            fecha = x['created'][:16]
            fecha_dt = datetime.strptime(fecha, '%Y-%m-%dT%H:%M')
            aux['created'] = fecha_dt.strftime("%d-%m-%Y %H:%M")
        else:
            aux['created'] = ''

        if x['operator'] == 1:
            aux['operator'] = 'Optic'
        elif x['operator'] == 2:
            aux['operator'] = 'Banda Ancha'
        else:
            aux['operator'] = x['operator']

        aux['rut'] = x['rut']
        aux['status'] = statuses[str(x['status'])]

        if str(x['category']) in categories.keys():
            aux['category'] = categories[str(x['category'])]
        else:
            h = {"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'}
            url = getattr(config, "iris_server_typify") + 'api/v1/tickets/category/' + str(x['category']) + '/'
            send = requests.get(
                url=url,
                headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
                verify=False
            )
            aux['category'] = send.json()['name']
            categories[str(x['category'])] = send.json()['name']

        if str(x['agent']) in agents.keys():
            aux['agent'] = agents[str(x['agent'])]
        else:
            url = getattr(config,"iris_server_typify") + 'api/v1/user/data/' + str(x['agent']) + '/'
            send = requests.get(
                url=url,
                headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
                verify=False
            )
            aux['agent'] = send.json()['username']
            agents[str(x['agent'])] = send.json()['username']
        
        typify.append(aux)
    return typify



@login_required
@permission_required("customers.view_customer")
def service_typify(request, slug):
    service = get_object_or_404(Service, pk=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    try:
        h = {"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'}
        
        url = getattr(config, "iris_server_typify") + 'api/v1/tickets/typify/' + '?rut=["' + str(service.customer.rut) + '"]'
        print("url typify", url)
        send = requests.get(
            url=url,
            headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
            verify=False
        )
        get_typify = []
        for typ in send.json():
            if service.number in typ['services']:
                get_typify.append(typ)

        typify = prepare_typify(get_typify, service)

        context = {
            "object": service,
            "service": service,
            "customer": service.customer,
            "invoices": Invoice.objects.filter(service=service),
            "payments": Payment.objects.filter(service=service),
            "typify": typify,
            "can_show_detail":can_show_detail
        }
    except:
        context = {
            "object": service,
            "service": service,
            "customer": service.customer,
            "invoices": Invoice.objects.filter(service=service),
            "payments": Payment.objects.filter(service=service),
            "typify": [],
            "can_show_detail":can_show_detail
        }
        messages.add_message(
                request,
                messages.WARNING,
                "No se puso establecer comunicación con Iris",
            )
    return render(request, "customers/service_typify.html", context)


@login_required
@permission_required("customers.view_invoice")
def invoice_detail(request, pk):
    invoice = get_object_or_404(Invoice, pk=pk)

    # Solo a las boletas o facturas se les permite tener notas de credito y debito asociadas.
    if invoice.kind in [1, 2]:
        option_credit = True
        option_debit = False
    else:
        option_credit = False
        if invoice.kind in [3]:
            option_debit = True
        else:
            option_debit = False

    if invoice.kind in [4, 9]:
        option_delete = True
    else:
        option_delete = False

    Month = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }
    pdf_date=""
    if invoice.pdf_date:
        pdf_date= str(Month[invoice.pdf_date.month])+" / "+str(invoice.pdf_date.year)
    credit_notes = Invoice.objects.filter(parent_id=invoice, kind=3)
    debit_notes = Invoice.objects.filter(parent_id=invoice, kind=8)
    cobro_notes = Invoice.objects.filter(parent_id=invoice, kind=6)
    payment = Payment.objects.filter(invoice=invoice)
    context = {
        "invoice": invoice,
        "pdf_date":pdf_date,
        "customer": invoice.customer,
        "service": invoice.service.all(),
        "option_credit": option_credit,
        "option_debit": option_debit,
        "option_delete": option_delete,
        "credit_notes": credit_notes,
        "debit_notes": debit_notes,
        "cobro_notes": cobro_notes,
        "credit": invoice.credit.all(),
        "charge": invoice.charge.all(),
        "payment": payment,
    }
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            
            if op.operator==0:
                if invoice.operator.company.id==op.company:
                    context["can_show_detail"]=True
                name_operator="Todos"
                todos=True
            else:
                if invoice.operator.id==op.operator:
                    context["can_show_detail"]=True
                name_operator=Operator.objects.get(id=op.operator).name
                todos=False
            context["companies"]=companies.exclude(id=op.company)
            context["current_company"]=op.company
            context["current_company_name"]=Company.objects.get(id=op.company).name
            context["current_operator"]= op.operator
            context["current_operator_name"]= name_operator
            context["operadores"]= Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            context["if_todos"]=todos
        except:
            pass
    return render(request, "customers/invoice_detail.html", context)


@login_required
@permission_required("customers.change_service")
def billing(request):
    if request.method == "POST":
        for service in Service.objects.filter(status=Service.ACTIVE):
            service.generate_invoice()
        messages.add_message(
            request,
            messages.SUCCESS,
            "Se ha completado con éxito el proceso de facturación.",
        )
        return redirect("dashboard")
    else:
        context = {
            "customer_count": Customer.objects.count(),
            "active_customer_count": Customer.objects.filter(
                service__status=Service.ACTIVE
            ).count(),
        }
        return render(request, "customers/billing.html", context)


class PaymentList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Payment
    paginate_by = 50
    permission_required = "customers.list_payment"

    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentList, self).get_context_data(*args, **kwargs)
        ctx["agents"] = get_user_model().objects.filter(is_active=True)
        ctx["operators"] = Operator.objects.all()
        ctx["payment_options"] = Payment.PAYMENT_OPTIONS

        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass

        return ctx


payment_list = PaymentList.as_view()


class PaymentListWithoutCustomer(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Payment
    paginate_by = 50
    permission_required = "customers.list_payment"
    template_name = "customers/payment_list_without_customer.html"

    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentListWithoutCustomer, self).get_context_data(*args, **kwargs)
        ctx["agents"] = get_user_model().objects.filter(is_active=True)
        ctx["operators"] = Operator.objects.all()
        ctx["payment_options"] = Payment.PAYMENT_OPTIONS
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx


payment_list_without_customer = PaymentListWithoutCustomer.as_view()


class PaymentOtherAccountReport(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Payment
    paginate_by = 50
    permission_required = "customers.list_payment"
    template_name = "customers/payment_other_account_report.html"

    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentOtherAccountReport, self).get_context_data(*args, **kwargs)
        ctx["agents"] = get_user_model().objects.filter(is_active=True)
        ctx["operators"] = Operator.objects.all()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    ctx["operators"] = Operator.objects.filter(company__id=op.company)
                else:
                    ctx["operators"] = Operator.objects.filter(id=op.operator)
            except:
                pass
        ctx["payment_options"] = Payment.PAYMENT_OPTIONS
        return ctx


payments_other_account = PaymentOtherAccountReport.as_view()


class PaymentDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Payment
    permission_required = "customers.view_payment"

    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentDetail, self).get_context_data(*args, **kwargs)
        ctx["service"] = Payment.objects.get(id=self.kwargs["pk"]).service.all()
        ctx["invoice"] = Payment.objects.get(id=self.kwargs["pk"]).invoice.all()
        pago=Payment.objects.get(id=self.kwargs["pk"])
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if pago.operator.company.id==op.company:
                        ctx["can_show_detail"]=True
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    if pago.operator.id==op.operator:
                        ctx["can_show_detail"]=True
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx


payment_detail = PaymentDetail.as_view()


@login_required
@permission_required("customers.view_dashboard")
def dashboard(request):
    #operators = Operator.objects.all().values()
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                operators = Operator.objects.filter(company__id=op.company).values()
            else:
                operators = Operator.objects.filter(id=op.operator).values()
        except:
            pass
    print(operators)
    context = {"operators": list(operators)}

    return render(request, "customers/dashboard.html", context)


@login_required
@permission_required("customers.view_dashboard")
def dashboard_ajax(request):

    operator = request.GET["operator"]
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                if operator != "":
                    services = Service.objects.filter(operator=operator)
                else:
                    services = Service.objects.filter(operator__company__id=op.company)
    
            else:
                if operator != "":
                    services = Service.objects.filter(operator=operator)
                else:
                    services = Service.objects.filter(operator__id=op.operator)
        except:
            pass

    total_count = services.count()
    active_count = services.filter(status=Service.ACTIVE).count()
    pending_count = services.filter(status=Service.PENDING).count()
    inactive_count = services.filter(status=Service.INACTIVE).count()
    not_installed_count = services.filter(status=Service.NOT_INSTALLED).count()
    free_count = services.filter(status=Service.FREE).count()
    install_rejected_count = services.filter(status=Service.INSTALL_REJECTED).count()
    delinquent_count = services.filter(status=Service.DELINQUENT).count()
    holder_change_count = services.filter(status=Service.HOLDER_CHANGE).count()
    discarded_count = services.filter(status=Service.DISCARDED).count()
    on_hold_count = services.filter(status=Service.ON_HOLD).count()
    lost_count = services.filter(status=Service.LOST).count()
    operators = Operator.objects.all().values()

    context = {
        "total_count": total_count,
        "active_count": active_count,
        "active_percentage": round(active_count * 100 / total_count, 2)
        if total_count
        else 0,
        "pending_count": pending_count,
        "pending_percentage": round(pending_count * 100 / total_count, 2)
        if total_count
        else 0,
        "inactive_count": inactive_count,
        "inactive_percentage": round(inactive_count * 100 / total_count, 2)
        if total_count
        else 0,
        "not_installed_count": not_installed_count,
        "not_installed_percentage": round(not_installed_count * 100 / total_count, 2)
        if total_count
        else 0,
        "free_count": free_count,
        "free_percentage": round(free_count * 100 / total_count, 2)
        if total_count
        else 0,
        "install_rejected_count": install_rejected_count,
        "install_rejected_percentage": round(
            install_rejected_count * 100 / total_count, 2
        )
        if total_count
        else 0,
        "delinquent_count": delinquent_count,
        "delinquent_percentage": round(delinquent_count * 100 / total_count, 2)
        if total_count
        else 0,
        "holder_change_count": holder_change_count,
        "holder_change_percentage": round(holder_change_count * 100 / total_count, 2)
        if total_count
        else 0,
        "discarded_count": discarded_count,
        "discarded_percentage": round(discarded_count * 100 / total_count, 2)
        if total_count
        else 0,
        "on_hold_count": on_hold_count,
        "on_hold_percentage": round(on_hold_count * 100 / total_count, 2)
        if total_count
        else 0,
        "lost_count": lost_count,
        "lost_percentage": round(lost_count * 100 / total_count, 2)
        if total_count
        else 0,
        "operators": list(operators),
    }

    data = json.dumps({"data": context})
    return HttpResponse(data, content_type="application/json")

@login_required
@permission_required("customers.view_dashboard")
def dashboard_ajax2(request):

    operator = request.GET["operator"]
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                operators = Operator.objects.filter(company__id=op.company).values()
                if operator != "":
                    services = Service.objects.filter(operator=operator)
                else:
                    services = Service.objects.filter(operator__company__id=op.company)
    
            else:
                operators = Operator.objects.filter(id=op.operator).values()
                if operator != "":
                    services = Service.objects.filter(operator=operator)
                else:
                    services = Service.objects.filter(operator__id=op.operator)
        except:
            pass

    # Busqueda de las promociones.
    promotions=Promotions.objects.all() #FALTA FILTRO OPERADOR O COMPANY

    # Cuenta de todas las promociones activas.
    total_service=services.filter(promotions__isnull=False).count() #.distinct().count()
    #print(total_service)
    total_count = promotions.count()
    #operators = Operator.objects.all().values()

    data_before=[]
    # Organizando la data de promociones.
    for promotion in promotions:
        filtrado_service= services.filter(promotions=promotion).count()
    
        dictionary = {
                "label": promotion.name,
                "value": filtrado_service,
        }
        data_before.append(dictionary)

    filtered_promotions = promotions.distinct()

    filtered_count = promotions.count()

    sliced_promotions = filtered_promotions
    data = json.dumps(
        {
            "total_service": total_service,
            "operators": list(operators),
            "recordsTotal": total_count,
            "data": data_before,
            }
        )
    #print(data)
    return HttpResponse(data, content_type="application/json")


@login_required
@permission_required("customers.change_service")
def operators_ajax(request):
    context = json.loads(request.GET["services"])
    operator_id = json.loads(request.GET["operator"])

    con_op = {}

    for i in context:
        service = Service.objects.filter(number=i).first()
        service.operator_id = operator_id
        service.save()
        service = Service.objects.filter(number=i).first()
        service.operator_id = operator_id
        operator = Operator.objects.filter(pk=operator_id).first()
        con_op[service.number] = operator.name

    data = json.dumps({"data": con_op}, sort_keys=True)
    return HttpResponse(data, content_type="application/json")


@login_required
@permission_required("customers.change_service")
def operator_ajax(request):
    service_id = json.loads(request.GET["service"])
    operator_id = json.loads(request.GET["operator"])

    service = Service.objects.filter(number=service_id).first()
    service.operator_id = operator_id
    service.save()

    service = Service.objects.filter(number=service_id).first()
    operator = Operator.objects.filter(pk=service.operator_id).first()

    data = json.dumps({"data": operator.name})
    return HttpResponse(data, content_type="application/json")


@login_required
@permission_required("customers.cleared_payment")
def clear_payment_ajax(request):
    payment_id = json.loads(request.GET["payment"])
    cleared = json.loads(request.GET["cleared"])

    payment = Payment.objects.filter(pk=payment_id).first()
    payment.cleared = cleared
    payment.save()

    payment = Payment.objects.filter(pk=payment_id).first()

    data = json.dumps({"data": payment.cleared})
    return HttpResponse(data, content_type="application/json")


@login_required
@permission_required("customers.change_service")
def collect(request):
    if request.method == "POST":
        form = SearchNumberForm(request.POST)
        if form.is_valid():
            number = form.cleaned_data["number"]
            return redirect(
                reverse("collect-by-number", kwargs={"service_number": number})
            )
    else:
        form = SearchNumberForm()
    context = {"form": form, "excel_form": MassPaymentForm()}
    return render(request, "customers/collect.html", context)


@login_required
@permission_required("customers.change_service")
def collect_by_number(request, service_number):
    service = get_object_or_404(Service, number=service_number)
    invoices = service.invoice_set.filter(paid_on=None)  # FIXME: create a special qs
    if request.method == "POST":
        form = ManualPaymentForm(request.POST)
        if form.is_valid():
            payment = form.save(commit=False)
            payment.manual = True
            payment.save()
            messages.add_message(
                request, messages.SUCCESS, "Pago manual agregado con éxito."
            )
            return redirect(reverse("collect"))
    else:
        form = ManualPaymentForm()
    context = {"service": service, "invoices": invoices, "form": form}
    return render(request, "customers/collect_by_number.html", context)


@login_required
@permission_required("customers.change_service")
def collect_by_excel(request):
    return render(request, "customers/collect_by_excel.html", {})


class PlanList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Plan
    paginate_by = 8
    permission_required = "customers.list_plan"

    def get_context_data(self, *args, **kwargs):
        ctx = super(PlanList, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx

plan_list = PlanList.as_view()


class PlanCreate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView
):
    model = Plan
    form_class = PlanForm
    success_url = "/plans/"
    success_message = "Se ha creado un nuevo plan."
    permission_required = "customers.add_plan"

    def get_form_kwargs(self):
        kwargs = super(PlanCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(PlanCreate, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx

plan_create = PlanCreate.as_view()


class PlanUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Plan
    form_class = PlanForm
    success_url = "/plans/"
    success_message = "El plan fue actualizado con éxito."
    permission_required = "customers.change_plan"

    def get_form_kwargs(self):
        kwargs = super(PlanUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(PlanUpdate, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx


plan_update = PlanUpdate.as_view()


@login_required
@permission_required("customers.view_call")
def call_detail(request, call_pk):
    call = get_object_or_404(Call, pk=call_pk)

    if call.recording:
        import magic

        mime = magic.from_buffer(call.recording.file.read(), mime=True)
    else:
        mime = None

    context = {
        "object": call,
        "service": call.service,
        "customer": call.service.customer,
        "recording_mime": mime,
    }
    return render(request, "customers/call_detail.html", context)


class CallList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Call
    paginate_by = 8
    permission_required = "customers.list_call"

    def get_context_data(self, *args, **kwargs):
        ctx = super(CallList, self).get_context_data(*args, **kwargs)
        ctx["filter_form"] = CallFilterForm()
        return ctx


call_list = CallList.as_view()


def prepare_call(call):
    return {
        "DT_RowId": call.pk,
        "answered_by": call.answered_by.username,
        "subject": call.get_subject_display(),
        "notes": call.truncated_notes,
        "service": getattr(call.service, "number", None),
        "date": call.created_at.strftime("%d-%m-%Y"),
        "has_call": bool(call.recording),
    }


@login_required
@permission_required("customers.list_call")
def call_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {"0": "id", "1": "answered_by", "2": "subject", "3": "notes"}
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        raw_agent = request.GET.get("agent", None)
        raw_subject = request.GET.get("subject", None)

        if raw_agent:
            try:
                agent_id = int(raw_agent)
            except ValueError:
                agent_id = None
        else:
            agent_id = None

        if raw_subject:
            try:
                subject_code = int(raw_subject)
            except ValueError:
                subject_code = None
        else:
            subject_code = None

        if order_column and order_dir:
            all_calls = Call.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_calls = Call.objects.all()
        if agent_id:
            all_calls = all_calls.filter(answered_by__id=agent_id)
        if subject_code:
            all_calls = all_calls.filter(subject=subject_code)
        all_count = all_calls.count()

        if search:
            rut_regex = "^" + "\.*".join(list(search))
            filtered_calls = all_calls.filter(
                Q(answered_by__username__unaccent__icontains=search)
                | Q(subject__icontains=search)
                | Q(service__number__icontains=search)
                | Q(notes__unaccent__icontains=search)
            )
        else:
            filtered_calls = all_calls

        filtered_calls = filtered_calls.distinct()

        filtered_count = filtered_calls.count()

        if length == -1:
            sliced_calls = filtered_calls
        else:
            sliced_calls = filtered_calls[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_call, sliced_calls)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


class CallUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Call
    form_class = CallForm
    template_name = "customers/call_create.html"
    success_message = "El registro de llamada fue actualizado con éxito."
    permission_required = "customers.change_call"

    def get_success_url(self):
        return reverse("call-detail", kwargs={"call_pk": self.object.pk})


call_update = CallUpdate.as_view()


@login_required
@permission_required("customers.add_call")
def call_empty_create(request):
    context = {}

    if request.method == "POST":
        form = CallForm(request.POST, request.FILES)
        if form.is_valid():
            call = form.save(commit=False)
            call.answered_by = request.user
            call.save()

            messages.add_message(
                request, messages.SUCCESS, "Se ha registrado la llamada."
            )
            return redirect(reverse("call-list"))
        else:
            context["form"] = form
    else:
        context["form"] = CallForm()
    return render(request, "customers/call_empty_create.html", context)


@login_required
@permission_required("customers.add_call")
def call_create(request):
    context = {}

    raw_number = request.GET.get("number", 0)
    if raw_number:
        slug = int(raw_number)
        context["slug"] = slug

        try:
            context["service"] = Service.objects.get(pk=slug)
            if request.method == "POST":

                if request.FILES:
                    old_filename = request.FILES["recording"].name
                    segment = AudioSegment.from_file(request.FILES["recording"])
                    request.FILES["recording"].file = segment.export(format="webm")
                    request.FILES["recording"].name = "{}.{}".format(
                        old_filename.split(".")[0], "webm"
                    )

                form = CallForm(request.POST, request.FILES)
                if form.is_valid():
                    call = form.save(commit=False)
                    call.service = context["service"]
                    call.answered_by = request.user
                    call.save()

                    messages.add_message(
                        request, messages.SUCCESS, "Se ha registrado la llamada."
                    )
                    return redirect(reverse("service-calls", kwargs={"slug": slug}))
                else:
                    context["form"] = form
            else:
                context["form"] = CallForm()
        except Service.DoesNotExist:
            pass

    return render(request, "customers/call_create.html", context)


## Call QA
class CallQAList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = CallQA
    permission_required = "customers.list_call_qa"

    def get_context_data(self, *args, **kwargs):
        ctx = super(CallQAList, self).get_context_data(*args, **kwargs)
        ctx["filter_form"] = CallQAFilterForm()
        return ctx


call_qa_list = CallQAList.as_view()


def prepare_call_qa(callQA):
    return {
        "DT_RowId": callQA.pk,
        "agent": str(callQA.agent),
        "customer_phone": callQA.customer_phone,
        "email": callQA.email,
        "rut": callQA.rut,
        "service": [service.pk for service in callQA.service.all()],
        "date": callQA.call_date.strftime("%d-%m-%Y")
        + " "
        + callQA.call_time.strftime("%H:%M"),
        "created_at": callQA.created_at.strftime("%d-%m-%Y %H:%M"),
        "created_by": str(callQA.created_by),
    }


@login_required
@permission_required("customers.list_call_qa")
def call_qa_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "id",
            "1": "agent",
            "2": "customer_phone",
            "3": "email",
            "4": "rut",
            "7": "created_by",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        raw_agent = request.GET.get("agent", None)

        if raw_agent:
            try:
                agent_id = int(raw_agent)
            except ValueError:
                agent_id = None
        else:
            agent_id = None

        if order_column and order_dir:
            all_calls = CallQA.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_calls = CallQA.objects.all()
        if agent_id:
            all_calls = all_calls.filter(agent__id=agent_id)
        all_count = all_calls.count()

        if search:
            filtered_calls = all_calls.filter(
                Q(agent__user_id__first_name__unaccent__icontains=search)
                | Q(agent__user_id__last_name__unaccent__icontains=search)
                | Q(agent__voip_extension__icontains=search)
                | Q(customer_phone__icontains=search)
                | Q(rut__icontains=search)
                | Q(service__id__icontains=search)
                | Q(call_date__icontains=search)
                | Q(created_by__username__icontains=search)
            )
        else:
            filtered_calls = all_calls

        filtered_calls = filtered_calls.distinct()

        filtered_count = filtered_calls.count()

        if length == -1:
            sliced_calls = filtered_calls
        else:
            sliced_calls = filtered_calls[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_call_qa, sliced_calls)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.add_callqa")
def call_qa_create(request):
    context = {}
    # context["form"] = CallQAForm()
    context["indicators"] = get_indicator_list()
    try:
        if request.method == "POST":
            form = CallQAForm(request.POST)
            context["form"] = form
            if form.is_valid():
                callQA = form.save(commit=False)
                callQA.created_by = request.user
                callQA.save()
                form.save_m2m()
                return redirect(reverse("call-qa-list"))
            else:
                context["form"] = form
        else:
            context["form"] = CallQAForm()
    except Service.DoesNotExist:
        pass

    return render(request, "customers/callqa_create.html", context)


@login_required
@permission_required("customers.view_call_qa")
def call_qa_detail(request, pk):
    context = {}
    callQA = get_object_or_404(CallQA, pk=pk)
    context["object"] = callQA
    context["indicators"] = get_indicator_list()
    context["services"] = prepare_services(callQA.service.all())
    context["active_indicators"] = [
        indicator.pk for indicator in callQA.indicators.all()
    ]
    context["apply_indicators"] = [
        indicator.pk for indicator in callQA.indicators_apply.all()
    ]
    typing = get_typing("ID={}".format(callQA.typing))
    context["typing"] = typing[0]["name"] if typing else ""
    category = get_typing("ID={}".format(callQA.category))
    context["category"] = category[0]["name"] if category else ""
    subcategory = get_typing("ID={}".format(callQA.subcategory))
    context["subcategory"] = subcategory[0]["name"] if subcategory else ""
    return render(request, "customers/callqa_detail.html", context)


@login_required
@permission_required("customers.delete_callqa")
def call_qa_delete(request, pk):
    try:
        callQA = get_object_or_404(CallQA, pk=pk)
        callQA.delete()
        data = json.dumps({"valid": True})
    except:
        data = json.dumps({"valid": False})
    return HttpResponse(data, content_type="application/json")


@login_required
def data_by_phone_ajax(request):
    rut = ""
    services = []
    address = ""
    email = ""
    if request.method == "POST":
        phone = request.POST.get("phone", False)
        if phone:
            customer = Customer.objects.filter(Q(phone=phone))[:1]
            if customer:
                rut = customer[0].rut
                services = prepare_services(
                    customer[0].service_set.all().order_by("pk")
                )
                email = customer[0].email
                address = customer[0].address

    data = json.dumps(
        {"rut": rut, "email": email, "services": services, "address": address}
    )

    return HttpResponse(data, content_type="application/json")


@login_required
def data_by_email_ajax(request):
    rut = ""
    services = []
    address = ""
    if request.method == "POST":
        email = request.POST.get("email", False)
        if email:
            customer = Customer.objects.filter(Q(email=email))[:1]
            if customer:
                rut = customer[0].rut
                services = prepare_services(
                    customer[0].service_set.all().order_by("pk")
                )
                address = customer[0].address

    data = json.dumps({"rut": rut, "services": services, "address": address})

    return HttpResponse(data, content_type="application/json")


@login_required
def data_by_rut_ajax(request):
    services = []
    address = ""
    if request.method == "POST":
        rut = request.POST.get("rut", False)
        if rut:
            customer = Customer.objects.filter(Q(rut=rut))[:1]
            if customer:
                services = prepare_services(
                    customer[0].service_set.all().order_by("pk")
                )
                address = address = customer[0].address

    data = json.dumps({"services": services, "address": address})

    return HttpResponse(data, content_type="application/json")


@login_required
def get_typing_childs(request):
    records = []
    if request.is_ajax():
        parent = (
            request.POST.get("parent") if request.POST else request.GET.get("parent")
        )
        if parent:
            records = get_typing("parent={}".format(parent))
            records = sorted(records, key=order_typing)

    data = json.dumps({"records": records})

    return HttpResponse(data, content_type="application/json")


def prepare_services(service_list):
    services = []
    for service in service_list:
        services.append([service.pk, str(service.number) + " - " + service.plan.name])
    return services


class CallQAUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = CallQA
    form_class = CallQAForm
    template_name = "customers/callqa_create.html"
    permission_required = "customers.change_callqa"

    def get_success_url(self):
        return reverse("call-qa-detail", kwargs={"pk": self.object.pk})

    def get_context_data(self, *args, **kwargs):
        ctx = super(CallQAUpdate, self).get_context_data(*args, **kwargs)
        ctx["indicators"] = get_indicator_list()
        ctx["active_indicators"] = [
            indicator.pk for indicator in ctx["object"].indicators.all()
        ]
        ctx["apply_indicators"] = [
            indicator.pk for indicator in ctx["object"].indicators_apply.all()
        ]
        ctx["active_category"] = self.object.category
        CATEGORIES = []
        for category in get_typing("parent={}".format(self.object.typing)):
            CATEGORIES.append((category["ID"], category["name"]))
        CATEGORIES = sorted(CATEGORIES, key=order_typing)
        ctx["categories"] = CATEGORIES
        ctx["active_subcategory"] = self.object.subcategory
        SUBCATEGORIES = []
        for subcategory in get_typing("parent={}".format(self.object.category)):
            SUBCATEGORIES.append((subcategory["ID"], subcategory["name"]))
        SUBCATEGORIES = sorted(SUBCATEGORIES, key=order_typing)
        ctx["subcategories"] = SUBCATEGORIES
        return ctx


call_qa_update = CallQAUpdate.as_view()

## QAIndicators
class QAIndicatorList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = QAIndicator
    paginate_by = 10
    permission_required = "customers.list_qaindicator"

    def get_context_data(self, *args, **kwargs):
        ctx = super(QAIndicatorList, self).get_context_data(*args, **kwargs)
        return ctx


qa_indicator_list = QAIndicatorList.as_view()


def prepare_qa_indicator(indicator):
    return {
        "DT_RowId": indicator.pk,
        "description": indicator.description,
        "parent": indicator.parent.description if indicator.parent else "",
    }


@login_required
@permission_required("customers.list_qaindicator")
def qa_indicator_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {"0": "id", "1": "description", "2": "parent"}
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_indicators = QAIndicator.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_indicators = QAIndicator.objects.all()
        all_count = all_indicators.count()

        filtered_indicators = all_indicators

        if search:
            filtered_indicators = filtered_indicators.filter(
                Q(description__unaccent__icontains=search)
                | Q(parent__description__unaccent__icontains=search)
            )

        filtered_indicators = filtered_indicators.distinct()

        filtered_count = filtered_indicators.count()

        if length == -1:
            sliced_indicators = filtered_indicators
        else:
            sliced_indicators = filtered_indicators[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_qa_indicator, sliced_indicators)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


class QAIndicatorDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = QAIndicator
    permission_required = "customers.view_qaindicator"


qa_indicator_detail = QAIndicatorDetail.as_view()


class QAIndicatorCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = QAIndicator
    form_class = QAIndicatorForm
    success_url = "/qa-indicators/"
    permission_required = "customers.add_qaindicator"


qa_indicator_create = QAIndicatorCreate.as_view()


class QAIndicatorUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = QAIndicator
    form_class = QAIndicatorForm
    template_name = "customers/qaindicator_form.html"
    permission_required = "customers.change_qaindicator"

    def get_success_url(self):
        return reverse("qa-indicator-detail", kwargs={"pk": self.object.pk})


qa_indicator_update = QAIndicatorUpdate.as_view()


@login_required
@permission_required("customers.delete_qaindicator")
def qa_indicator_delete(request, pk):
    try:
        if QAIndicator.objects.filter(Q(parent=pk)).count():
            data = {
                "error": "El registro no puede ser eliminado si posee elementos asociados"
            }
        else:
            indicator = get_object_or_404(QAIndicator, pk=pk)
            indicator.delete()
            data = {"error": ""}
    except:
        data = {"error": "Ocurrio un error al intentar eliminar el registro"}
    return HttpResponse(json.dumps(data), content_type="application/json")


def get_indicator_list():
    indicator_list = list()
    indicators = QAIndicator.objects.order_by("id").filter(parent=None)
    for indicator in indicators:
        childs = QAIndicator.objects.order_by("id").filter(parent=indicator.id)
        if childs.count() > 0:
            indicator_list.append(indicator)
            for child in childs:
                indicator_list.append(child)
    return indicator_list


## Monitoreo Exploratorio
@login_required
@permission_required("customers.add_qamonitoring")
def qa_monitoring_create(request):
    context = {}
    # context['form'] = QAMonitoringForm()
    try:
        if request.method == "POST":
            form = QAMonitoringForm(request.POST)
            context["form"] = form
            if form.is_valid():
                monitoring = form.save(commit=False)
                monitoring.created_by = request.user
                monitoring.save()
                return redirect(reverse("qa-monitoring-list"))
            else:
                context["form"] = form
        else:
            context["form"] = QAMonitoringForm()
    except Service.DoesNotExist:
        pass
    return render(request, "customers/qamonitoring_form.html", context)


class QAMonitoringList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = QAMonitoring
    paginate_by = 10
    permission_required = "customers.list_qamonitoring"

    def get_context_data(self, *args, **kwargs):
        ctx = super(QAMonitoringList, self).get_context_data(*args, **kwargs)
        return ctx


qa_monitoring_list = QAMonitoringList.as_view()


def prepare_qa_monitoring(monitoring):
    return {
        "DT_RowId": monitoring.pk,
        "date": monitoring.date.strftime("%d-%m-%Y"),
        "agent": str(monitoring.agent),
        "phone": monitoring.phone,
        "rut": monitoring.rut,
        "service": str(monitoring.service) if monitoring.service else "Sin servicio",
        "reason": monitoring.reason,
        "comment": monitoring.comment,
        "email": monitoring.email,
        "created_at": monitoring.created_at.strftime("%d-%m-%Y %H:%M"),
        "created_by": str(monitoring.created_by),
    }


@login_required
@permission_required("customers.list_qamonitoring")
def qa_monitoring_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "id",
            "1": "date",
            "2": "agent",
            "3": "phone",
            "4": "email",
            "5": "rut",
            "6": "service",
            "7": "created_at",
            "8": "created_by",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_monitorings = QAMonitoring.objects.filter(Q(active=True)).order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_monitorings = QAMonitoring.objects.filter(Q(active=True))

        date_from = request.GET.get("date_from", None)
        date_to = request.GET.get("date_to", None)

        if not date_from:
            date_from = None

        if not date_to:
            date_to = None

        if date_from is not None and date_to is not None:
            all_monitorings = all_monitorings.filter(
                Q(date__gte=date_from), Q(date__lte=date_to)
            )
        else:
            if date_from is not None:
                all_monitorings = all_monitorings.filter(Q(date__gte=date_from))
            if date_to is not None:
                all_monitorings = all_monitorings.filter(Q(date__lte=date_to))

        if search:
            all_monitorings = all_monitorings.filter(
                Q(date__icontains=search)
                | Q(agent__user__first_name__unaccent__icontains=search)
                | Q(agent__user__last_name__unaccent__icontains=search)
                | Q(agent__voip_extension__icontains=search)
                | Q(phone__icontains=search)
                | Q(rut__icontains=search)
                | Q(service__number__icontains=search)
                | Q(created_by__username__icontains=search)
                | Q(email__icontains=search)
            )

        all_count = all_monitorings.count()

        filtered_monitorings = all_monitorings

        filtered_monitorings = filtered_monitorings.distinct()

        filtered_count = filtered_monitorings.count()

        if length == -1:
            sliced_monitorings = filtered_monitorings
        else:
            sliced_monitorings = filtered_monitorings[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_qa_monitoring, sliced_monitorings)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.view_qamonitoring")
def qa_monitoring_detail(request, pk):
    try:
        monitoring = get_object_or_404(QAMonitoring, pk=pk)
        monitoring_data = prepare_qa_monitoring(monitoring)
        subcategory = get_typing("ID={}".format(monitoring.subcategory))
        monitoring_data["subcategory"] = subcategory[0]["name"] if subcategory else ""
        data = json.dumps({"data": monitoring_data})
    except:
        data = json.dumps({"data": ""})
    return HttpResponse(data, content_type="application/json")


class QAMonitoringUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = QAMonitoring
    form_class = QAMonitoringForm
    template_name = "customers/qamonitoring_form.html"
    permission_required = "customers.change_qamonitoring"

    def get_success_url(self):
        return reverse("qa-monitoring-list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["selected_service"] = (
            context["object"].service.id if context["object"].service else ""
        )
        return context


qa_monitoring_update = QAMonitoringUpdate.as_view()


@login_required
@permission_required("customers.delete_qamonitoring")
def qa_monitoring_delete(request, pk):
    try:
        monitoring = get_object_or_404(QAMonitoring, pk=pk)
        monitoring.active = False
        monitoring.save()
        data = json.dumps({"valid": True})
    except:
        data = json.dumps({"valid": False})
    return HttpResponse(data, content_type="application/json")


@login_required
@permission_required("customers.view_qamonitoring")
def qa_monitoring_export(request):
    report_type = request.GET.get("report_type", None)

    date_from = request.GET.get("date_from", None)
    if not date_from:
        date_from = None

    date_to = request.GET.get("date_to", None)
    if not date_to:
        date_to = None

    search = request.GET.get("search", None)
    if not search:
        search = None

    monitorings = QAMonitoring.objects.filter(Q(active=True))

    if date_from is not None and date_to is not None:
        monitorings = monitorings.filter(Q(date__gte=date_from), Q(date_lte=date_to))
    else:
        if date_from is not None:
            monitorings = monitorings.filter(Q(date__gte=date_from))

        if date_to is not None:
            monitorings = monitorings.filter(Q(date__lte=date_to))

    if search is not None:
        monitorings = monitorings.filter(
            Q(date__icontains=search)
            | Q(agent__user__first_name__unaccent__icontains=search)
            | Q(agent__user__last_name__unaccent__icontains=search)
            | Q(agent__voip_extension__icontains=search)
            | Q(phone__icontains=search)
            | Q(rut__icontains=search)
            | Q(service__number__icontains=search)
            | Q(created_by__username__icontains=search)
            | Q(email__icontains=search)
        )

    monitorings = monitorings.distinct()

    if report_type == "excel":
        response = qa_monitoring_export_excel(monitorings)
    else:
        context = {
            "data": monitorings,
        }
        filename = "listado_monitoreo_exploratorio.pdf"

        response = PDFTemplateResponse(
            request=request,
            template="customers/qamonitoring_export_pdf.html",
            context=context,
            filename=filename,
            show_content_in_browser=True,
            cmd_options={
                "encoding": "utf8",
                "quiet": True,
                "orientation": "landscape",
            },
        )

    return response


def qa_monitoring_export_excel(data):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    header = (
        "Fecha de Contacto",
        "Agente",
        "N° de Teléfono de Cliente",
        "Correo de Cliente",
        "Rut",
        "N° de Servicio",
        "Sucategoria",
        "Motivo",
        "Observación",
        "Fecha de Creación",
        "Creado por",
    )
    bold = workbook.add_format({"bold": True})
    worksheet.set_row(0, None, bold)
    worksheet.write_row(0, 0, header)
    worksheet.set_column(0, 1, 15)
    # worksheet.set_column(1, 1, 15)
    worksheet.set_column(2, 3, 25)
    # worksheet.set_column(3, 3, 25)
    worksheet.set_column(4, 4, 15)
    worksheet.set_column(5, 8, 30)
    # worksheet.set_column(6, 6, 30)
    # worksheet.set_column(7, 7, 30)
    # worksheet.set_column(8, 8, 30)
    worksheet.set_column(9, 10, 15)
    # worksheet.set_column(10, 10, 15)

    row = 1

    for record in data:
        worksheet.write(row, 0, record.date.strftime("%d/%m/%Y"))
        worksheet.write(row, 1, str(record.agent))
        worksheet.write(row, 2, record.phone)
        worksheet.write(row, 3, record.email)
        worksheet.write(row, 4, record.rut)
        worksheet.write(
            row, 5, str(record.service) if record.service else "Sin servicio"
        )
        subcategory = get_typing("ID={}".format(record.subcategory))
        worksheet.write(row, 6, subcategory[0]["name"] if subcategory else "")
        worksheet.write(row, 7, record.reason)
        worksheet.write(row, 8, record.comment)
        worksheet.write(row, 9, record.created_at.strftime("%d/%m/%Y"))
        worksheet.write(row, 10, str(record.created_by))
        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response[
        "Content-Disposition"
    ] = 'attachment; filename="listado_monitoreo_exploratorio.xlsx"'
    return response


def qa_monitoring_export_pdf(data):

    context = {
        "data": dict(data),
    }
    filename = "listado_monitoreo_exploratorio.pdf"

    response = PDFTemplateResponse(
        request=request,
        template="customers/callqa_report_pdf.html",
        context=context,
        filename=filename,
        show_content_in_browser=True,
    )
    return response


def prepare_qa_tracing(tracing):
    return {
        "DT_RowId": tracing.pk,
        "solved": tracing.solved,
        "reason": tracing.reason,
        "comment": tracing.comment,
    }


@login_required
def qa_monitoring_tracing_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {"0": "solved", "1": "reason", "2": "comment"}
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        """
        search = request.GET.get('search[value]', '').replace('+', '')
        """
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_tracings = QATracing.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_tracings = QATracing.objects.all()

        monitoring_id = request.GET.get("monitoring_id", None)

        all_tracings = all_tracings.filter(Q(monitoring__id=monitoring_id))

        all_count = all_tracings.count()

        filtered_tracings = all_tracings

        filtered_tracings = filtered_tracings.distinct()

        filtered_count = filtered_tracings.count()

        if length == -1:
            sliced_tracings = filtered_tracings
        else:
            sliced_tracings = filtered_tracings[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_qa_tracing, sliced_tracings)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.add_qatracing")
def qa_monitoring_tracing_create(request):
    context = {}
    context["valid"] = False
    try:
        if request.method == "POST":
            form = QATracingForm(request.POST)
            if form.is_valid():
                tracing = form.save()
                context["valid"] = True
    except:
        pass
    data = json.dumps(context)
    return HttpResponse(data, content_type="application/json")


@login_required
@permission_required("customers.change_qatracing")
def qa_monitoring_tracing_edit(request):
    context = {}
    context["valid"] = False
    try:
        if request.method == "POST":
            tracing = get_object_or_404(QATracing, pk=request.POST.get("tracing_pk"))
            form = QATracingForm(request.POST, instance=tracing)
            if form.is_valid():
                tracing = form.save()
                context["valid"] = True
    except Service.DoesNotExist:
        pass
    data = json.dumps(context)
    return HttpResponse(data, content_type="application/json")


@login_required
@permission_required("customers.delete_qatracing")
def qa_monitoring_tracing_delete(request, pk):
    tracing = get_object_or_404(QATracing, pk=pk)
    tracing.delete()

    data = json.dumps({"valid": True})
    return HttpResponse(data, content_type="application/json")


## Canales QC
class QAChannelList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = QAChannel
    paginate_by = 10
    permission_required = "customers.list_qachannel"


qa_channel_list = QAChannelList.as_view()


def prepare_qa_channel(channel):
    return {"DT_RowId": channel.pk, "name": channel.name}


@login_required
@permission_required("customers.list_qachannel")
def qa_channel_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {"0": "id", "1": "name"}
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            channels = QAChannel.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            channels = QAChannel.objects.all()

        all_count = channels.count()

        if search:
            channels = channels.filter(Q(name__unaccent__icontains=search))

        channels = channels.distinct()

        filtered_count = channels.count()

        if length != -1:
            channels = channels[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_qa_channel, channels)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


class QAChannelCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = QAChannel
    form_class = QAChannelForm
    success_url = "/qa-channels/"
    permission_required = "customers.add_qachannel"


qa_channel_create = QAChannelCreate.as_view()


class QAChannelUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = QAChannel
    form_class = QAChannelForm
    template_name = "customers/qachannel_form.html"
    permission_required = "customers.change_qachannel"

    def get_success_url(self):
        return reverse("qa-channel-detail", kwargs={"pk": self.object.pk})


qa_channel_update = QAChannelUpdate.as_view()


@login_required
@permission_required("customers.delete_qachannel")
def qa_channel_delete(request, pk):
    try:
        channel = get_object_or_404(QAChannel, pk=pk)
        channel.delete()
        data = {"error": ""}
    except Exception as e:
        data = {"error": "Ocurrio un error al intentar eliminar el registro"}
    return HttpResponse(json.dumps(data), content_type="application/json")


## manual invoicing
RUT_FIELD = CLRutField()

DATE_PARSER = etl.dateparser("%d-%m-%Y")

INVOICE_KINDS = {
    "BOLETA ELECTRONICA": Invoice.BOLETA,
    "FACTURA ELECTRONICA": Invoice.FACTURA,
    "NOTA DE CREDITO ELECTRONICA": Invoice.NOTA_DE_CREDITO,
    "NOTA DE DEBITO ELECTRONICA": Invoice.NOTA_DE_DEBITO,
}

CONSTRAINTS = [
    dict(
        name="kind_enum",
        field="DOCUMENTO",
        assertion=lambda v: v in INVOICE_KINDS.keys(),
    ),
    dict(name="folio_int", field="FOLIO", test=int),
    dict(name="due_date", field="FECHA", test=DATE_PARSER),
    dict(name="rut_valid", field="RUT", test=RUT_FIELD.clean),
    dict(name="amount_int", field="TOTAL", test=lambda x: x and isinstance(x, float) or isinstance(x, int)), #test=int),
]


def validate_data(table, constraints):
    t2 = etl.addrownumbers(table)
    problems = etl.validate(t2, constraints=constraints)
    rows_with_errors = set(problems.columns()["row"])
    invalid = etl.selectin(t2, "row", rows_with_errors)
    valid = etl.selectnotin(t2, "row", rows_with_errors)
    return valid, invalid


def process_manual_billing(excel_buffer, operator):
    raw_table = etl.fromxlsx(excel_buffer)
    valid, invalid = validate_data(raw_table, CONSTRAINTS)
    not_found = [raw_table.header()]
    multiple_services = [raw_table.header()]

    for t in valid.data():
        try:
            if t[4] != "0-0":
                cleaned_rut = RUT_FIELD.clean(t[4].lstrip("0"))
            else:
                cleaned_rut = t[4]

            customer = Customer.objects.get(payorrut__rut=cleaned_rut)

            comment = "{} #{} - Ingreso Manual".format(t[1], t[2])
            servicio = Service.objects.filter(customer=customer.id)
            if servicio.count() == 1:
                ii, _ = Invoice.objects.update_or_create(
                    customer=customer,
                    kind=INVOICE_KINDS[t[1]],
                    folio=t[2],
                    defaults={
                        "due_date": DATE_PARSER(t[3]),
                        "comment": comment,
                        "total": float(t[6]),
                    },
                    operator=operator,
                    kind_load=1,
                )

                ii.service.add(servicio[0])
                if ii.currency==None:
                    ii.currency=servicio[0].plan.currency
                    ii.save()   
            else:
                if  operator.company:
                    currency=operator.company.currency
                else:
                    currency=None
                ii, _ = Invoice.objects.update_or_create(
                    customer=customer,
                    kind=INVOICE_KINDS[t[1]],
                    folio=t[2],
                    defaults={
                        "due_date": DATE_PARSER(t[3]),
                        "comment": comment,
                        "total": float(t[6]),
                        "currency":currency
                    },
                    operator=operator,
                    kind_load=1,
                )

        except Customer.MultipleObjectsReturned:
            comment = "{} #{} - Ingreso Manual {}".format(t[1], t[2], cleaned_rut)

            invoice_created = Invoice.objects.filter(
                folio=t[2], kind=INVOICE_KINDS[t[1]], customer__isnull=True
            )
            if not invoice_created.exists():
                if  operator.company:
                    currency=operator.company.currency
                else:
                    currency=None
                ii = Invoice.objects.create(
                    kind=INVOICE_KINDS[t[1]],
                    folio=t[2],
                    due_date=DATE_PARSER(t[3]),
                    comment=comment,
                    total=float(t[6]),
                    operator=operator,
                    kind_load=3,
                    currency=currency
                )
            else:
                multiple_services.append(t[1:])

        except Customer.DoesNotExist:
            comment = "{} #{} - Ingreso Manual {}".format(t[1], t[2], cleaned_rut)

            invoice_created = Invoice.objects.filter(
                folio=t[2], kind=INVOICE_KINDS[t[1]], customer__isnull=True
            )
            if not invoice_created.exists():
                if  operator.company:
                    currency=operator.company.currency
                else:
                    currency=None
                ii = Invoice.objects.create(
                    kind=INVOICE_KINDS[t[1]],
                    folio=t[2],
                    due_date=DATE_PARSER(t[3]),
                    comment=comment,
                    total=float(t[6]),
                    operator=operator,
                    kind_load=2,
                    currency=currency
                )
            else:
                not_found.append(t[1:])

    excel_buffer.seek(0)

    valid_buffer = io.BytesIO()
    valid.cutout("row").toxlsx(valid_buffer)
    valid_buffer.seek(0)

    invalid_buffer = io.BytesIO()
    invalid.cutout("row").toxlsx(invalid_buffer)
    invalid_buffer.seek(0)

    not_found_buffer = io.BytesIO()
    etl.toxlsx(not_found, not_found_buffer)
    not_found_buffer.seek(0)

    multiple_services_buffer = io.BytesIO()
    etl.toxlsx(multiple_services, multiple_services_buffer)
    multiple_services_buffer.seek(0)

    return dict(
        processed_count=raw_table.nrows(),
        valid_count=valid.nrows(),
        invalid_count=invalid.nrows(),
        not_found_count=len(not_found) - 1,
        multiple_services_count=len(multiple_services) - 1,
        files=dict(
            processed=excel_buffer,
            valid=valid_buffer,
            invalid=invalid_buffer,
            not_found=not_found_buffer,
            multiple_services=multiple_services_buffer,
        ),
    )


@login_required
@permission_required("customers.change_service")
def manual_billing(request):
    context = {}
    username = None
    context={}
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                name_operator="Todos"
                todos=True
            else:
                name_operator=Operator.objects.get(id=op.operator).name
                todos=False
            context={"companies":companies.exclude(id=op.company),
                    "current_company":op.company,
                    "current_company_name":Company.objects.get(id=op.company).name,
                    "current_operator":op.operator,
                    "current_operator_name":name_operator,
                    "operadores": Operator.objects.filter(company__id=op.company).exclude(id=op.operator),
                    "if_todos":todos,
                }
        except:
            pass
    if request.method == "POST":
        form = BillingExcelForm(request.POST, request.FILES)
        context["form"] = form
        if form.is_valid():
            excel_file = form.cleaned_data["excel_file"]
            operator = form.cleaned_data["operator"]
            processing_results = process_manual_billing(excel_file, operator)
            files = processing_results.pop("files")
            processing_results["created_count"] = (
                processing_results["valid_count"]
                - processing_results["not_found_count"]
            )
            context.update(processing_results)

            # File shenaningans
            old_file_paths = request.session.get("billing_files", {})
            for file_path in old_file_paths.values():
                try:
                    os.remove(file_path)
                except FileNotFoundError:
                    pass

            new_file_paths = {}

            for k, v in files.items():
                fd, temp_path = mkstemp()
                with open(temp_path, "wb") as f:
                    f.write(v.read())
                new_file_paths[k] = temp_path
                os.close(fd)

            request.session["billing_files"] = new_file_paths
    else:
        context["form"] = BillingExcelForm()

        asks_for_file = request.GET.get("file", None)
        if asks_for_file:
            file_paths = request.session.get("billing_files", {})
            path = file_paths.get(asks_for_file, None)
            if path:
                with open(path, "rb") as f:
                    response = HttpResponse(
                        f,
                        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    )
                    response[
                        "Content-Disposition"
                    ] = "attachment; filename='{}.xlsx'".format(asks_for_file)
                    return response

    return render(request, "customers/manual_billing.html", context)


## manual payment load
DATETIME_PARSER = etl.datetimeparser("%d/%m/%Y", strict=True)

PAYMENT_CONSTRAINTS = [
    dict(name="paid_on_valid", field="Fecha", test=DATETIME_PARSER),
    dict(name="rut_valid", field="Rut Origen/Destino", test=RUT_FIELD.clean),
    dict(name="amount_int", field="Monto $", test=lambda x: x and isinstance(x, float) or isinstance(x, int)), #test=int),
    dict(name="document_int", field="Numero Documento", test=lambda x: x and int(x) or None)
]


def validate_payment_data(table, constraints):
    t2 = etl.addrownumbers(table)
    problems = etl.validate(t2, constraints=constraints)
    #print(problems)
    rows_with_errors = set(problems.columns()["row"])
    invalid = etl.selectin(t2, "row", rows_with_errors)
    valid = etl.selectnotin(t2, "row", rows_with_errors)
    return valid, invalid


def process_manual_payments(table, operator, bank_account):
    raw_table = table
    # special RUT massage
    stripped_table = etl.convert(raw_table, "Rut Origen/Destino", str.strip)
    #int_table = stripped_table.convert("Monto $", lambda x: int(x.replace(".", "")))
    valid, invalid = validate_payment_data(stripped_table, PAYMENT_CONSTRAINTS)
    not_found = [stripped_table.header()]
    multiple_services = [stripped_table.header()]

    for t in valid.data():
        #print(t)
        try:
            if t[3] != "0-0":
                cleaned_rut = RUT_FIELD.clean(t[3].lstrip("0"))
            else:
                cleaned_rut = t[3]
            try:
                number_doc=int(t[9])
            except:
                number_doc=None
            customer = Customer.objects.get(payorrut__rut=cleaned_rut)
            comment = "{} {} {} {}".format(t[4], t[5], t[8], cleaned_rut)
            servicio = Service.objects.filter(customer=customer.id)
            if servicio.count() == 1:
                pp, _ = Payment.objects.update_or_create(
                    transfer_id=t[2],
                    defaults={
                        "customer": customer,
                        "paid_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "deposited_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "amount": float(t[6]),
                        "comment": comment,
                        "operator": operator,
                        "bank_account": bank_account,
                        "kind_load": 1,
                        "number_document": number_doc, #int(t[9]),
                    },
                )
                pp.service.add(servicio[0])
                if pp.invoice.all().count() == 0:
                    object_list = [3, 4, 5, 6]
                    bol = Invoice.objects.filter(
                        customer=customer, paid_on__isnull=True, service=servicio[0]
                    ).filter(~Q(kind__in=object_list))
                    # Si tiene solo una boleta sin pagar.
                    if bol.count() == 1:
                        pp.invoice.add(bol[0])
                        total = bol[0].total

                        # Buscar notas de cobro o crédito asociadas.
                        credit_notes = Invoice.objects.filter(parent_id=bol[0], kind=3)
                        cobro_notes = Invoice.objects.filter(parent_id=bol[0], kind=6)

                        # Calculo del total de cuanto cuesta.
                        for j in credit_notes:
                            total = total - j.total
                        for j in cobro_notes:
                            total = total + j.total

                        # Busco si la boleta tiene pagos asociados.
                        payment_invoice = Payment.objects.filter(
                            invoice=bol[0]
                        ).exclude(id=pp.id)
                        subtotal = 0
                        for p in payment_invoice:
                            subtotal = subtotal + p.amount

                        # Si el pago es mayor o igual al monto de la boleta, se marcan como pagadas.
                        if float(t[6]) + subtotal >= total:
                            inv = Invoice.objects.get(id=bol[0].id)
                            inv.paid_on = date.today()
                            inv.save()
                            # Revisando si existe una prorroga de esa boleta.
                            try:
                                if not(inv.customer.flag is None):
                                    if "prorrogas" in inv.customer.flag:
                                        if str(inv.id) in inv.customer.flag['prorrogas']:
                                            fecha_actual=datetime.now()
                                            fecha=inv.customer.flag['prorrogas'][str(inv.id)]['date']
                                            if dateutil.parser.isoparse(fecha)>=fecha_actual:
                                                inv.customer.flag['prorrogas'][str(inv.id)]['paid_on_time']=True
                                                inv.customer.flag['prorrogas'][str(inv.id)]['paid_on']=datetime.now().isoformat()
                                            else:
                                                inv.customer.flag['prorrogas'][str(inv.id)]['paid_on_time']=False
                                                inv.customer.flag['prorrogas'][str(inv.id)]['paid_on']=datetime.now().isoformat()
                                            inv.customer.save()
                            except:
                                pass
            else:
                pp, _ = Payment.objects.update_or_create(
                    transfer_id=t[2],
                    defaults={
                        "customer": customer,
                        "paid_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "deposited_on": DATETIME_PARSER(t[1]).replace(hour=21),
                        "amount": float(t[6]),
                        "comment": comment,
                        "operator": operator,
                        "bank_account": bank_account,
                        "kind_load": 1,
                        "number_document": number_doc,
                    },
                )
        except Customer.MultipleObjectsReturned:
            comment = "{} {} {} {}".format(t[4], t[5], t[8], cleaned_rut)
            try:
                number_doc=int(t[9])
            except:
                number_doc=None
            pp, _ = Payment.objects.update_or_create(
                transfer_id=t[2],
                defaults={
                    "paid_on": DATETIME_PARSER(t[1]).replace(hour=21),
                    "deposited_on": DATETIME_PARSER(t[1]).replace(hour=21),
                    "amount": float(t[6]),
                    "comment": comment,
                    "operator": operator,
                    "bank_account": bank_account,
                    "kind_load": 3,
                    "number_document": number_doc,
                },
            )
            multiple_services.append(t[1:])
        except Payment.MultipleObjectsReturned:
            multiple_services.append(t[1:])
        except Customer.DoesNotExist:
            try:
                number_doc=int(t[9])
            except:
                number_doc=None
            comment = "{} {} {} {}".format(t[4], t[5], t[8], cleaned_rut)
            pp, _ = Payment.objects.update_or_create(
                transfer_id=t[2],
                defaults={
                    "paid_on": DATETIME_PARSER(t[1]).replace(hour=21),
                    "deposited_on": DATETIME_PARSER(t[1]).replace(hour=21),
                    "amount": float(t[6]),
                    "comment": comment,
                    "operator": operator,
                    "bank_account": bank_account,
                    "kind_load": 2,
                    "number_document": number_doc,
                },
            )
            not_found.append(t[1:])

    excel_buffer = io.BytesIO()
    stripped_table.toxlsx(excel_buffer)
    excel_buffer.seek(0)

    valid_buffer = io.BytesIO()
    valid.cutout("row").toxlsx(valid_buffer)
    valid_buffer.seek(0)

    invalid_buffer = io.BytesIO()
    invalid.cutout("row").toxlsx(invalid_buffer)
    invalid_buffer.seek(0)

    not_found_buffer = io.BytesIO()
    etl.toxlsx(not_found, not_found_buffer)
    not_found_buffer.seek(0)

    multiple_services_buffer = io.BytesIO()
    etl.toxlsx(multiple_services, multiple_services_buffer)
    multiple_services_buffer.seek(0)

    return dict(
        processed_count=stripped_table.nrows(),
        valid_count=valid.nrows(),
        invalid_count=invalid.nrows(),
        not_found_count=len(not_found) - 1,
        multiple_services_count=len(multiple_services) - 1,
        files=dict(
            processed=excel_buffer,
            valid=valid_buffer,
            invalid=invalid_buffer,
            not_found=not_found_buffer,
            multiple_services=multiple_services_buffer,
        ),
    )


@login_required
@permission_required("customers.change_service")
def manual_payments(request):
    context = {}
    username = None
    context={}
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                name_operator="Todos"
                todos=True
                banco=BankAccount.objects.filter(operator__id=op.operator)
            else:
                name_operator=Operator.objects.get(id=op.operator).name
                todos=False
                banco=BankAccount.objects.filter(operator__id=op.operator)
            context={"companies":companies.exclude(id=op.company),
                    "current_company":op.company,
                    "current_company_name":Company.objects.get(id=op.company).name,
                    "current_operator":op.operator,
                    "current_operator_name":name_operator,
                    "operadores": Operator.objects.filter(company__id=op.company).exclude(id=op.operator),
                    "if_todos":todos,
                }
        except:
            pass
    if request.method == "POST":
        form = PaymentsExcelForm(request.POST, request.FILES)
        context["form"] = form
        if form.is_valid():
            table = form.cleaned_data["excel_file"]
            data = form.cleaned_data
            operator = data["operator"]
            bank_account = data["bank_account"]
            processing_results = process_manual_payments(table, operator, bank_account)
            files = processing_results.pop("files")
            processing_results["created_count"] = (
                processing_results["valid_count"]
                - processing_results["not_found_count"]
            )
            context.update(processing_results)

            # File shenaningans
            old_file_paths = request.session.get("payment_files", {})
            for file_path in old_file_paths.values():
                try:
                    os.remove(file_path)
                except FileNotFoundError:
                    pass

            new_file_paths = {}

            for k, v in files.items():
                fd, temp_path = mkstemp()
                with open(temp_path, "wb") as f:
                    f.write(v.read())
                new_file_paths[k] = temp_path
                os.close(fd)

            request.session["payment_files"] = new_file_paths
    else:
        context["form"] = PaymentsExcelForm()

        asks_for_file = request.GET.get("file", None)
        if asks_for_file:
            file_paths = request.session.get("payment_files", {})
            path = file_paths.get(asks_for_file, None)
            if path:
                with open(path, "rb") as f:
                    response = HttpResponse(
                        f,
                        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    )
                    response[
                        "Content-Disposition"
                    ] = "attachment; filename={}.xlsx".format(asks_for_file)
                    return response

    return render(request, "customers/manual_payments.html", context)


@login_required
def manual_payment_new_ajax(request):

    operator = request.GET.get("operator", False)
    bank = request.GET.get("bank", False)

    if operator and bank:
        bank_accounts = BankAccount.objects.filter(operator=operator, bank=bank)

    elif operator != "":
        bank_accounts = BankAccount.objects.filter(operator=operator)
    else:
        bank_accounts = BankAccount.objects.all()

    bank_accounts = list(bank_accounts.values("id", "tag", "account_number"))
    context = {"bank_accounts": bank_accounts}
    data = json.dumps({"data": context})
    return HttpResponse(data, content_type="application/json")


"""
Funcion que permite pasar un Decimal o datetime a un json .
"""


def default(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    if isinstance(obj, datetime):
        return obj.isoformat()
    raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)


@login_required
def manual_new_ajax_client(request):

    customer = request.GET.get("customer", False)
    service = request.GET.get("service", False)

    if customer and service:
        service = Service.objects.filter(customer=customer).annotate(
            plan_name=F("plan__name"), plan_price=F("plan__price")
        )
    elif customer != "":
        service = Service.objects.filter(customer=customer).annotate(
            plan_name=F("plan__name"), plan_price=F("plan__price")
        )
    else:
        service = Service.objects.none().annotate(
            plan_name=F("plan__name"), plan_price=F("plan__price")
        )

    # Anota en el queryset los valores del nombre y precio del plan.
    for i in service:
        i.plan_name = str(i.plan)
        i.plan_price = str(i.plan.price)

    service = list(service.values("id", "number", "plan_name", "plan_price"))
    context = {"service": service}
    data = json.dumps({"data": context}, default=default)
    return HttpResponse(data, content_type="application/json")


@login_required
def manual_new_ajax_service(request):

    customer = request.GET.get("customer", False)
    service = request.GET.getlist("service[]")

    object_list = [3, 4, 5, 6]
    if len(service) != 0:
        for i in range(len(service)):
            invoice = (
                Invoice.objects.filter(service=service[i], paid_on__isnull=True)
                .filter(~Q(kind__in=object_list))
                .order_by("due_date")
            )
    else:
        invoice = Invoice.objects.none()

    invoice = list(invoice.values("id", "comment", "due_date", "total"))
    context = {"invoice": invoice}
    data = json.dumps({"data": context}, default=default)
    return HttpResponse(data, content_type="application/json")

'''
@login_required
def ajax_user(request):

    search = request.GET.get("search", False)

    if search != " ":
        rut_regex = "^" + "\.*".join(list(search))
        customer = Customer.objects.filter(
            first_name__contains=search
        ) | Customer.objects.filter( last_name__contains=search ) | Customer.objects.filter(rut__iregex=rut_regex)
    else:
        customer = Customer.objects.none()

    customer = list(customer.values("id", "first_name", "last_name"))
    context = {"customer": customer}
    data = json.dumps({"data": context}, default=default)
    print(data)
    return HttpResponse(data, content_type="application/json")
'''
class UserSearch(views.APIView):
    
    renderer_classes = [JSONRenderer]
    
    def get(self,request):
        print("pase")
        serializer_context = {
            "request": request,
        }
        search = self.request.GET.get("search", False)
        if search != " ":
            rut_regex = "^" + "\.*".join(list(search))
            object_list = Customer.objects.filter(
                first_name__contains=search
            ) | Customer.objects.filter( last_name__contains=search ) | Customer.objects.filter(rut__iregex=rut_regex)
        else:
            object_list = self.queryset

        serializer = CustomerSerializer(
                object_list,
                many=True,
                context=serializer_context,
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

ajax_user = UserSearch.as_view()


class PaymentCreate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView
):
    model = Payment
    form_class = PaymentCreateForm
    success_message = "Se ha creado un nuevo pago."
    permission_required = "customers.add_payment"

    def dispatch(self, *args, **kwargs):
        self.customer = get_object_or_404(Customer, pk=self.kwargs["customer_pk"])
        try:
            self.service = get_object_or_404(Service, pk=self.kwargs["slug"])
        except:
            pass
        return super(PaymentCreate, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentCreate, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        ctx["customer"] = self.customer
        if ctx["if_todos"]:
            ctx["service"] = Service.objects.filter(customer=self.customer.id)
        else:
            ctx["service"] = Service.objects.filter(customer=self.customer.id, operator__id=op.operator)
        ctx["uf"] = Service.objects.filter(
            customer=self.customer.id, unified_billing=True
        )
        ctx["cancel_url"] = reverse("invoice-list", args=[self.customer.pk])
        bol = Invoice.objects.none()
        object_list = [3, 4, 5, 6]
        try:
            ctx["re"] = self.service.get_statement
            if self.service in ctx["uf"]:
                ctx["actual_service"] = ctx["uf"]
            else:
                ctx["actual_service"] = Service.objects.filter(
                    pk=self.kwargs["slug"]
                )

            # Las boletas del servicio actual
            for i in ctx["actual_service"]:
                bol = bol | Invoice.objects.filter(
                    customer=self.customer, paid_on__isnull=True, service=i
                ).filter(~Q(kind__in=object_list))
            else:
                if ctx["if_todos"]:
                    bol = bol | Invoice.objects.filter(
                        customer=self.customer, paid_on__isnull=True
                    ).filter(~Q(kind__in=object_list))
                else:
                    bol = bol | Invoice.objects.filter(
                        customer=self.customer, paid_on__isnull=True, operator__id=op.operator
                    ).filter(~Q(kind__in=object_list))
                

            ctx["actual"] = self.service
            ctx["url"] = self.service.get_absolute_url
            ctx["not_show"] = "t"
            ctx["boletas"] = bol.distinct().order_by("due_date")
        except:
            ctx["re"] = self.customer.get_statement
            ctx["url"] = self.customer.get_absolute_url
            ctx["not_show"] = "f"

            # Las boletas de todos los servicios del cliente
            for i in ctx["service"]:
                bol = bol | Invoice.objects.filter(
                    customer=self.customer, paid_on__isnull=True, service=i
                ).filter(~Q(kind__in=object_list))
            else:
                if ctx["if_todos"]:
                    bol = bol | Invoice.objects.filter(
                        customer=self.customer, paid_on__isnull=True
                    ).filter(~Q(kind__in=object_list))
                else:
                    bol = bol | Invoice.objects.filter(
                        customer=self.customer, paid_on__isnull=True, operator__id=op.operator
                    ).filter(~Q(kind__in=object_list))
            ctx["boletas"] = bol.distinct().order_by("due_date")
        return ctx

    def form_valid(self, form):
        uf = False
        for i in form["service"].value():
            if Service.objects.get(id=i).unified_billing:
                uf = True
        # Si el servicio es unificado, asocia los demás servicios al pago.
        if uf:
            uf = Service.objects.filter(customer=self.customer.id, unified_billing=True)
            array = []
            for j in uf:
                array.append(j.id)
            form.cleaned_data["service"] = array

        total = 0
        # Se obtiene la suma de los montos de las boletas.
        for i in form.cleaned_data["invoice"]:
            subtotal = i.total
            # Buscar notas de cobro o crédito asociadas.
            credit_notes = Invoice.objects.filter(parent_id=i, kind=3)
            cobro_notes = Invoice.objects.filter(parent_id=i, kind=6)

            # Calculo del total de cuanto cuesta la boleta.
            for j in credit_notes:
                subtotal = subtotal - j.total
            for j in cobro_notes:
                subtotal = subtotal + j.total

            # Sumo los subtotales de cada boleta
            total = total + subtotal

        # Si el pago es mayor o igual al monto de las boletas, se marcan como pagadas.
        if form.cleaned_data["amount"] >= total:
            for i in form.cleaned_data["invoice"]:
                if not (i.paid_on):
                    i.paid_on = date.today()
                    i.save()
                    try:
                        # Revisando si existe una prorroga de esa boleta.
                        if not(i.customer.flag is None):
                            if "prorrogas" in i.customer.flag:
                                if str(i.id) in i.customer.flag['prorrogas']:
                                    fecha_actual=datetime.now()
                                    fecha=i.customer.flag['prorrogas'][str(i.id)]['date']
                                    if dateutil.parser.isoparse(fecha)>=fecha_actual:
                                        i.customer.flag['prorrogas'][str(i.id)]['paid_on_time']=True
                                        i.customer.flag['prorrogas'][str(i.id)]['paid_on']=datetime.now().isoformat()
                                    else:
                                        i.customer.flag['prorrogas'][str(i.id)]['paid_on_time']=False
                                        i.customer.flag['prorrogas'][str(i.id)]['paid_on']=datetime.now().isoformat()
                                    i.customer.save()
                    except:
                        pass
                    credit_notes = Invoice.objects.filter(
                        parent_id=i, kind=3, paid_on__isnull=True
                    )
                    cobro_notes = Invoice.objects.filter(
                        parent_id=i, kind=6, paid_on__isnull=True
                    )
                    for j in credit_notes:
                        j.paid_on = date.today()
                        j.save()
                    for j in cobro_notes:
                        j.paid_on = date.today()
                        j.save()
        else:
            resto = form.cleaned_data["amount"]
            invoice_select = form.cleaned_data["invoice"].order_by("due_date")
            for i in invoice_select:
                total = i.total
                # Buscar notas de cobro o crédito asociadas.
                credit_notes = Invoice.objects.filter(parent_id=i, kind=3)
                cobro_notes = Invoice.objects.filter(parent_id=i, kind=6)

                # Calculo del total de cuanto cuesta.
                for j in credit_notes:
                    total = total - j.total
                for j in cobro_notes:
                    total = total + j.total

                # Busco si la boleta tiene pagos asociados.
                payment_invoice = Payment.objects.filter(invoice=i)
                subtotal = 0
                for p in payment_invoice:
                    subtotal = subtotal + p.amount

                if resto + subtotal >= total:
                    # Si la boleta no esta pagada, la marca como pagada.
                    if not (i.paid_on):
                        i.paid_on = date.today()
                        i.save()
                        # Revisando si existe una prorroga de esa boleta.
                        try: 
                            if not(i.customer.flag is None):
                                if "prorrogas" in i.customer.flag:
                                    if str(i.id) in i.customer.flag['prorrogas']:
                                        fecha_actual=datetime.now()
                                        fecha=i.customer.flag['prorrogas'][str(i.id)]['date']
                                        if dateutil.parser.isoparse(fecha)>=fecha_actual:
                                            i.customer.flag['prorrogas'][str(i.id)]['paid_on_time']=True
                                            i.customer.flag['prorrogas'][str(i.id)]['paid_on']=datetime.now().isoformat()
                                        else:
                                            i.customer.flag['prorrogas'][str(i.id)]['paid_on_time']=False
                                            i.customer.flag['prorrogas'][str(i.id)]['paid_on']=datetime.now().isoformat()
                                        i.customer.save()
                        except:
                            pass
                        credit_notes = Invoice.objects.filter(
                            parent_id=i, kind=3, paid_on__isnull=True
                        )
                        cobro_notes = Invoice.objects.filter(
                            parent_id=i, kind=6, paid_on__isnull=True
                        )
                        for j in credit_notes:
                            j.paid_on = date.today()
                            j.save()
                        for j in cobro_notes:
                            j.paid_on = date.today()
                            j.save()
                    resto = resto - total

        payment = form.save(commit=False)
        payment.customer = self.customer
        payment.manual = True
        return super(PaymentCreate, self).form_valid(form)


payment_create = PaymentCreate.as_view()

PAYMENT_OPTIONS = (
    (1, ("transbank")),
    (2, ("cash")),
    (3, ("cheque")),
    (5, ("servipag")),
    (6, ("BCI deposit")),
    (7, ("manual transfer")),
    (8, ("webpay")),
    (9, ("other")),
    (10, "NO registrado en banco"),
    (11, "Condonación"),
    (13, "paypal"),
    (14, "multicaja"),
    (15, "caja vecina"),
)


class PaymentUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Payment
    form_class = PaymentUpdateForm
    success_message = "Se ha actualizado el pago."
    permission_required = "customers.change_payment"
    template_name = "customers/payment_update.html"

    def dispatch(self, *args, **kwargs):
        self.payment = get_object_or_404(Payment, pk=self.kwargs["pk"])
        try:
            self.customer = self.payment.customer
            id_customer = self.object.customer.id
        except:
            if self.payment.kind_load != 1:
                com = self.payment.comment.split(" ")
                self.cleaned_rut = com[len(com) - 1]

        return super(PaymentUpdate, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(PaymentUpdate, self).get_context_data(*args, **kwargs)
        ctx["kind"] = PAYMENT_OPTIONS
        ctx["boletas_act"] = self.payment.invoice.all()
        try:
            actual = self.payment.service.all()
            servicios = Service.objects.filter(customer=self.customer.id)
            bol = Invoice.objects.none()
            object_list = [3, 4, 5, 6]
            for i in actual:
                servicios = servicios.exclude(id=i.id)
                bol = bol | Invoice.objects.filter(
                    customer=self.customer, paid_on__isnull=True, service=i
                ).filter(~Q(kind__in=object_list))
            else:
                bol = bol | Invoice.objects.filter(
                    customer=self.customer, paid_on__isnull=True
                ).filter(~Q(kind__in=object_list))

            for i in ctx["boletas_act"]:
                bol = bol.exclude(id=i.id)

            ctx["service"] = servicios
            ctx["customer"] = self.customer
            ctx["service_act"] = actual
            ctx["boletas"] = bol.distinct().order_by("due_date")
        except:
            cus = Customer.objects.filter(payorrut__rut=self.cleaned_rut)
            if cus.count() != 0:
                ctx["customers"] = cus

        ctx["cancel_url"] = reverse("payment-detail", args=[self.object.pk])
        pago=Payment.objects.get(id=self.kwargs["pk"])
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if pago.operator.company.id==op.company:
                        ctx["can_show_detail"]=True
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    if pago.operator.id==op.operator:
                        ctx["can_show_detail"]=True
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx

    def form_valid(self, form):
        payment = form.save(commit=False)
        if (
            form["customer"].value()
            and self.payment.kind_load == 2
            and (not self.payment.customer)
        ):
            cus = Customer.objects.get(id=form["customer"].value())
            PayorRut.objects.create(rut=self.cleaned_rut, name=cus.name, customer=cus)

        total = 0
        # Se obtiene la suma de los montos de las boletas.
        for i in form.cleaned_data["invoice"]:
            subtotal = i.total
            # Buscar notas de cobro o crédito asociadas.
            credit_notes = Invoice.objects.filter(parent_id=i, kind=3)
            cobro_notes = Invoice.objects.filter(parent_id=i, kind=6)

            # Calculo del total de cuanto cuesta la boleta.
            for j in credit_notes:
                subtotal = subtotal - j.total
            for j in cobro_notes:
                subtotal = subtotal + j.total

            # Sumo los subtotales de cada boleta
            total = total + subtotal

        # Si el pago es mayor o igual al monto de las boletas, se marcan como pagadas.
        if form.cleaned_data["amount"] >= total:
            for i in form.cleaned_data["invoice"]:
                if not (i.paid_on):
                    i.paid_on = date.today()
                    i.save()
                    # Revisando si existe una prorroga de esa boleta.
                    try:
                        if not(i.customer.flag is None):
                            if "prorrogas" in i.customer.flag:
                                if str(i.id) in i.customer.flag['prorrogas']:
                                    fecha_actual=datetime.now()
                                    fecha=i.customer.flag['prorrogas'][str(i.id)]['date']
                                    if dateutil.parser.isoparse(fecha)>=fecha_actual:
                                        i.customer.flag['prorrogas'][str(i.id)]['paid_on_time']=True
                                        i.customer.flag['prorrogas'][str(i.id)]['paid_on']=datetime.now().isoformat()
                                    else:
                                        i.customer.flag['prorrogas'][str(i.id)]['paid_on_time']=False
                                        i.customer.flag['prorrogas'][str(i.id)]['paid_on']=datetime.now().isoformat()
                                    i.customer.save()
                    except:
                        pass
                    credit_notes = Invoice.objects.filter(
                        parent_id=i, kind=3, paid_on__isnull=True
                    )
                    cobro_notes = Invoice.objects.filter(
                        parent_id=i, kind=6, paid_on__isnull=True
                    )
                    for j in credit_notes:
                        j.paid_on = date.today()
                        j.save()
                    for j in cobro_notes:
                        j.paid_on = date.today()
                        j.save()
        else:
            resto = form.cleaned_data["amount"]
            invoice_select = form.cleaned_data["invoice"].order_by("due_date")
            for i in invoice_select:
                total = i.total
                # Buscar notas de cobro o crédito asociadas.
                credit_notes = Invoice.objects.filter(parent_id=i, kind=3)
                cobro_notes = Invoice.objects.filter(parent_id=i, kind=6)

                # Calculo del total de cuanto cuesta.
                for j in credit_notes:
                    total = total - j.total
                for j in cobro_notes:
                    total = total + j.total

                # Busco si la boleta tiene pagos asociados.
                payment_invoice = Payment.objects.filter(invoice=i).exclude(
                    id=self.payment.id
                )
                subtotal = 0
                for p in payment_invoice:
                    subtotal = subtotal + p.amount

                if resto + subtotal >= total:
                    # Si no esta como pagado, se marca como pagado.
                    if not (i.paid_on):
                        i.paid_on = date.today()
                        i.save()
                        credit_notes = Invoice.objects.filter(
                            parent_id=i, kind=3, paid_on__isnull=True
                        )
                        cobro_notes = Invoice.objects.filter(
                            parent_id=i, kind=6, paid_on__isnull=True
                        )
                        for j in credit_notes:
                            j.paid_on = date.today()
                            j.save()
                        for j in cobro_notes:
                            j.paid_on = date.today()
                            j.save()
                    resto = resto - total + subtotal
        return super(PaymentUpdate, self).form_valid(form)


payment_update = PaymentUpdate.as_view()


class InvoiceCreate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView
):
    model = Invoice
    form_class = InvoiceCreateForm
    success_message = "Se ha creado una nueva factura."
    permission_required = "customers.add_invoice"

    def dispatch(self, *args, **kwargs):
        self.customer = get_object_or_404(Customer, pk=self.kwargs["customer_pk"])
        try:
            self.service = get_object_or_404(Service, pk=self.kwargs["slug"])
        except:
            pass
        return super(InvoiceCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(InvoiceCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(InvoiceCreate, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            user=User.objects.get(username=self.request.user.username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    ctx["if_todos"]=True
                else:
                    ctx["if_todos"]=False
            except:
                pass
        if ctx["if_todos"]:
            ctx["service"] = Service.objects.filter(customer=self.customer.id)
        else:
            ctx["service"] = Service.objects.filter(customer=self.customer.id, operator__id=op.operator)
        ctx["customer"] = self.customer
        #ctx["service"] = Service.objects.filter(customer=self.customer.id)
        ctx["uf"] = Service.objects.filter(
            customer=self.customer.id, unified_billing=True
        )
        ctx["cancel_url"] = reverse("invoice-list", args=[self.customer.pk])
        ctx["show_op1"] = True
        if getattr(config, "allow_generate_factura") == "no":
            ctx["show_fact"] = False
        else:
            ctx["show_fact"] = True
        try:
            ctx["re"] = self.service.get_statement
            if self.service in ctx["uf"]:
                ctx["actual_service"] = ctx["uf"]
            else:
                ctx["actual_service"] = Service.objects.filter(
                    pk=self.kwargs["slug"]
                )
            ctx["actual"] = self.service
            ctx["url"] = self.service.get_absolute_url
            ctx["not_show"] = "t"
        except:
            ctx["re"] = self.customer.get_statement
            ctx["url"] = self.customer.get_absolute_url
            ctx["not_show"] = "f"
        return ctx

    def form_valid(self, form):
        uf = False
        for i in form["service"].value():
            if Service.objects.get(id=i).unified_billing:
                uf = True
        # Si el servicio es unificado, asocia los demás servicios a la boleta.
        if uf:
            uf = Service.objects.filter(customer=self.customer.id, unified_billing=True)
            array = []
            for j in uf:
                array.append(j.id)
            form.cleaned_data["service"] = array
        kind = form.cleaned_data["kind"]

        if (
            form.cleaned_data["kind"] != "5"
            and form.cleaned_data["kind"] != "4"
            and form.cleaned_data["kind"] != "9"
        ):
            # Verifica si hay folios disponibles.
            avaliable_folio, folio_value, id_lote, newBatch = check_allocate_invoice(
                kind, form.cleaned_data["operator"]
            )
            # Si no hay folios disponibles muestra mensaje de error
            if not (avaliable_folio):
                messages.error(
                    self.request,
                    "Se acabaron los folios disponibles, registre un nuevo lote para este tipo de Boleta",
                )
                # return redirect(reverse("invoice-create", args=[{"customer_pk": self.customer.id}]))
                return HttpResponseRedirect(
                    reverse_lazy(
                        "invoice-create", kwargs={"customer_pk": self.customer.id}
                    )
                )
        elif kind == "4":
            notas = Invoice.objects.filter(kind=4).order_by("-folio")
            if notas.count() != 0:
                folio_value = int(notas[0].folio + 1)
            else:
                folio_value = 0
        elif kind == "9":
            notas = Invoice.objects.filter(kind=9).order_by("-folio")
            if notas.count() != 0:
                folio_value = int(notas[0].folio + 1)
            else:
                folio_value = 0
        else:
            notas = Invoice.objects.filter(kind=5).order_by("-folio")
            if notas.count() != 0:
                folio_value = int(notas[0].folio + 1)
            else:
                folio_value = 0
        invoice = form.save(commit=False)
        invoice.kind = kind
        invoice.folio = folio_value
        invoice.customer = self.customer
        invoice.iva = float(
            (
                Indicators.objects.filter(kind=2, company=self.customer.company)
                .distinct("kind")
                .values("kind", "created_at", "value")
                .first()["value"]
            )
        )
        invoice_created = Invoice.objects.filter(
            folio=folio_value, customer=self.customer, kind=kind
        )
        if True:  # not invoice_created.exists():
            if kind == "5" or kind == "4" or kind == "9":
                return super(InvoiceCreate, self).form_valid(form)
            else:
                # If there is not a service selected
                if len(form.cleaned_data["service"]) == 0:
                    messages.error(
                        self.request, "Debe seleccionar al menos un servicio",
                    )
                    return HttpResponseRedirect(
                        reverse_lazy(
                            "invoice-create", kwargs={"customer_pk": self.customer.id}
                        )
                    )
                # Starts the process to send request to facturacion.cl
                (result, folio, id_batch, first_key, batchBefore,) = allocate_invoice(
                    form.cleaned_data["kind"],
                    form.cleaned_data["service"],
                    self.customer.id,
                    form.cleaned_data["operator"]
                )
                res = {}
                # The info needs to be classified by kind
                if form.cleaned_data["kind"] == "1":
                    res["boleta"] = {"completed": result, "error": False}
                    documents = packaging(res["boleta"])
                if form.cleaned_data["kind"] == "2":
                    res["factura"] = {"completed": result, "error": False}
                    documents = packaging(res["factura"])
                if form.cleaned_data["kind"] == "2":
                    res["factura"] = {"completed": result, "error": False}
                    documents = packaging(res["factura"])
                # To create the document's json
                documentsReady = {
                    "user": getattr(config, "user"),
                    "rut": getattr(config, "rut"),
                    "password": getattr(config, "password"),
                    "puerto": getattr(config, "port"),
                    "documentList": json.dumps(documents),
                    "endPoint": json.dumps(False),
                    "order": json.dumps(False),
                    "celery": False,
                }
                # To identify if its multi service.
                if len(form.cleaned_data["service"]) > 1:
                    documentsReady["unified"] = True
                else:
                    documentsReady["unified"] = False
                try:
                    go = get_invoices(documentsReady)
                except:
                    # Update batch
                    InvoiceBatch.objects.filter(id=id_batch).update(
                        batch=batchBefore, stock=len(batchBefore)
                    )
                    messages.error(
                        self.request, "Ocurrió un error de conexión con Facturacion.cl",
                    )
                    return HttpResponseRedirect(
                        reverse_lazy(
                            "invoice-create", kwargs={"customer_pk": self.customer.id}
                        )
                    )
                response = json.loads(go["documentList"])
                if response[0]["resultado"] == "False":
                    # Update batch
                    InvoiceBatch.objects.filter(id=id_batch).update(
                        batch=batchBefore, stock=len(batchBefore)
                    )
                    messages.error(
                        self.request,
                        "Ocurrió un error mientras se procesaba en Facturacion.cl, "
                        + str(response[0]["error"]),
                    )
                    return HttpResponseRedirect(
                        reverse_lazy(
                            "invoice-create", kwargs={"customer_pk": self.customer.id}
                        )
                    )
                else:
                    # The info is good, and it means that we need to save the barcode
                    # and the credits and charges
                    infoReady = json.loads(documentsReady["documentList"])
                    invoice.total = int(
                        infoReady[0]["Documento"]["Encabezado"]["Totales"][
                            "TotalPeriodo"
                        ]
                    )
                    # invoice.barcode = response[0]["barCode"]
                    services = form.cleaned_data["service"]

                    json_inv = {}
                    iva = float(
                        (
                            Indicators.objects.filter(kind=2, company=self.customer.company)
                            .distinct("kind")
                            .values("kind", "created_at", "value")
                            .first()["value"]
                        )
                    )
                    for s in services:
                        if uf:
                            obj = Service.objects.get(id=s)
                            if obj.price_override and obj.price_override != 0:
                                price_to_apply = obj.price_override
                                if s.plan.uf:
                                    uf = (
                                        Indicators.objects.filter(kind=1, company=s.operator.company)
                                        .distinct("kind")
                                        .values("kind", "created_at", "value")
                                        .first()["value"]
                                    )
                                    price_to_apply = round(
                                        s.price_override * uf * Decimal(1 + iva), 0
                                    )
                            else:

                                price_to_apply = obj.plan.price
                        else:
                            if s.price_override and s.price_override != 0:
                                price_to_apply = s.price_override
                                if s.plan.uf:
                                    uf = (
                                        Indicators.objects.filter(kind=1, company=s.operator.company)
                                        .distinct("kind")
                                        .values("kind", "created_at", "value")
                                        .first()["value"]
                                    )
                                    price_to_apply = round(
                                        s.price_override * uf * Decimal(1 + iva), 0
                                    )
                            else:
                                price_to_apply = s.plan.price
                            json_inv[str(s.id)] = {
                                "QtyItem": 1,
                                "PrcItem": str(
                                    round(float(price_to_apply) / (1 + iva), 2)
                                ),
                                "MontoItem": str(
                                    round(float(price_to_apply) / (1 + iva), 0)
                                ),
                            }
                    invoice.json = json_inv

                    # initialize empty queryset
                    credits = Credit.objects.none()
                    charges = Charge.objects.none()
                    # We load the credits and charges of the services.
                    for service in services:
                        if uf:
                            # If it is unified, the array contains the id of the services
                            credits = credits | Service.objects.get(
                                id=service
                            ).credit_set.filter(
                                status_field=False, cleared_at__isnull=True
                            ).annotate(
                                unit_price=Count("id"),
                                monto=Count("id"),
                                year=ExtractYear("created_at"),
                                month=ExtractMonth("created_at"),
                            )
                            charges = charges | Service.objects.get(
                                id=service
                            ).charge_set.filter(
                                status_field=False, cleared_at__isnull=True
                            ).annotate(
                                unit_price=Count("id"),
                                monto=Count("id"),
                                year=ExtractYear("created_at"),
                                month=ExtractMonth("created_at"),
                            )
                        else:
                            # If not, it only has the Services objects.
                            credits = credits | service.credit_set.filter(
                                status_field=False, cleared_at__isnull=True
                            ).annotate(
                                unit_price=Count("id"),
                                monto=Count("id"),
                                year=ExtractYear("created_at"),
                                month=ExtractMonth("created_at"),
                            )
                            charges = charges | service.charge_set.filter(
                                status_field=False, cleared_at__isnull=True
                            ).annotate(
                                unit_price=Count("id"),
                                monto=Count("id"),
                                year=ExtractYear("created_at"),
                                month=ExtractMonth("created_at"),
                            )

                    iva = float(
                        (
                            Indicators.objects.filter(kind=2, company=s.operator.company)
                            .distinct("kind")
                            .values("kind", "created_at", "value")
                            .first()["value"]
                        )
                    )
                    if credits:
                        arraycredits = []
                        for i in credits:
                            subtotal = calculo(i)
                            descuento = CreditApplied.objects.create(
                                credit_applied=i, amount=subtotal
                            )
                            arraycredits.append(descuento.id)
                            if i.permanent:
                                # Si no tiene monto aplicado, se guarda 0
                                if i.amount_applied == None:
                                    i.amount_applied = 0
                                # Se aumentan ambos valores, para que sea infinito la cant de ciclos.
                                i.times_applied = i.times_applied + 1
                                i.number_billing = i.number_billing + 1
                                i.save()
                            else:
                                # Si no se aplicado se guarda el subtotal
                                if i.amount_applied == None:
                                    i.amount_applied = subtotal
                                else:
                                    # Si ya se aplico antes, se suman los valores.
                                    i.amount_applied = i.amount_applied + subtotal
                                i.times_applied = i.times_applied + 1
                                if i.times_applied == i.number_billing:
                                    i.cleared_at = date.today()
                                i.save()
                        form.cleaned_data["credit"] = arraycredits
                    if charges:
                        arraycharges = []
                        for i in charges:
                            subtotal = calculo(i)
                            cargo = ChargeApplied.objects.create(
                                charge_applied=i, amount=subtotal
                            )
                            arraycharges.append(cargo.id)
                            if i.permanent:
                                # Si no tiene monto aplicado, se guarda 0
                                if i.amount_applied == None:
                                    i.amount_applied = 0
                                # Se aumentan ambos valores, para que sea infinito la cant de ciclos.
                                i.times_applied = i.times_applied + 1
                                i.number_billing = i.number_billing + 1
                                i.save()
                            else:
                                # Si no se aplicado se guarda el subtotal
                                if i.amount_applied == None:
                                    i.amount_applied = subtotal
                                else:
                                    # Si ya se aplico antes, se suman los valores.
                                    i.amount_applied = i.amount_applied + subtotal
                                i.times_applied = i.times_applied + 1
                                if i.times_applied == i.number_billing:
                                    i.cleared_at = date.today()
                                i.save()
                        form.cleaned_data["charge"] = arraycharges

                    return super(InvoiceCreate, self).form_valid(form)
        else:
            messages.error(
                self.request,
                "Ya existe una factura con ese folio y el mismo tipo de este cliente",
            )
            return HttpResponseRedirect(
                reverse_lazy("invoice-create", kwargs={"customer_pk": self.customer.id})
            )


invoice_create = InvoiceCreate.as_view()


class InvoiceCreateNotaVenta(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView
):
    model = Invoice
    form_class = InvoiceNotaVentaCreateForm
    success_message = "Se ha creado una nueva factura."
    permission_required = "customers.add_invoice"
    template_name = "customers/invoice_form_note.html"

    def dispatch(self, *args, **kwargs):
        self.customer = get_object_or_404(Customer, pk=self.kwargs["customer_pk"])
        self.service = get_object_or_404(Service, pk=self.kwargs["slug"])
        return super(InvoiceCreateNotaVenta, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(InvoiceCreateNotaVenta, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(InvoiceCreateNotaVenta, self).get_context_data(*args, **kwargs)
        ctx["customer"] = self.customer
        ctx["service"] = Service.objects.filter(customer=self.customer.id)
        ctx["uf"] = Service.objects.filter(
            customer=self.customer.id, unified_billing=True
        )
        ctx["cancel_url"] = reverse("invoice-list", args=[self.customer.pk])

        ctx["re"] = self.service.get_statement
        if self.service in ctx["uf"]:
            ctx["actual_service"] = ctx["uf"]
        else:
            ctx["actual_service"] = Service.objects.filter(pk=self.kwargs["slug"])
        ctx["actual"] = self.service
        ctx["url"] = self.service.get_absolute_url
        ctx["not_show"] = "t"
        return ctx

    def form_valid(self, form):
        uf = False
        for i in form["service"].value():
            if Service.objects.get(id=i).unified_billing:
                uf = True
        if uf:
            uf = Service.objects.filter(customer=self.customer.id, unified_billing=True)
            array = []
            for j in uf:
                array.append(j.id)
            form.cleaned_data["service"] = array

        services = form.cleaned_data["service"]

        # Inicializa queryset vacio
        credits = Credit.objects.none()
        charges = Charge.objects.none()
        # Carga los credits y charges asociados a los servicios.
        for service in services:
            if uf:
                credits = credits | Service.objects.get(id=service).credit_set.filter(
                    status_field=False, cleared_at__isnull=True, active=True
                ).annotate(
                    unit_price=Count("id"),
                    monto=Count("id"),
                    year=ExtractYear("created_at"),
                    month=ExtractMonth("created_at"),
                )
                charges = charges | Service.objects.get(id=service).charge_set.filter(
                    status_field=False, cleared_at__isnull=True
                ).annotate(
                    unit_price=Count("id"),
                    monto=Count("id"),
                    year=ExtractYear("created_at"),
                    month=ExtractMonth("created_at"),
                )
            else:
                credits = credits | service.credit_set.filter(
                    status_field=False, cleared_at__isnull=True, active=True
                ).annotate(
                    unit_price=Count("id"),
                    monto=Count("id"),
                    year=ExtractYear("created_at"),
                    month=ExtractMonth("created_at"),
                )
                charges = charges | service.charge_set.filter(
                    status_field=False, cleared_at__isnull=True
                ).annotate(
                    unit_price=Count("id"),
                    monto=Count("id"),
                    year=ExtractYear("created_at"),
                    month=ExtractMonth("created_at"),
                )

        iva = float(
            (
                Indicators.objects.filter(kind=2, company=self.customer.company)
                .distinct("kind")
                .values("kind", "created_at", "value")
                .first()["value"]
            )
        )
        if len(services) > 1:
            data = unifiedInvoiceData(self.customer.id)
        else:
            data = invoiceData(self.service.id)
        if credits:
            arraycredits = []
            for i in credits:
                subtotal = calculo(i)
                descuento = CreditApplied.objects.create(
                    credit_applied=i, amount=subtotal
                )

                arraycredits.append(descuento.id)
                if i.permanent:
                    # Si no tiene monto aplicado, se guarda 0
                    if i.amount_applied == None:
                        i.amount_applied = 0
                    # Se aumentan ambos valores, para que sea infinito la cant de ciclos.
                    i.times_applied = i.times_applied + 1
                    i.number_billing = i.number_billing + 1
                    i.save()
                else:
                    # Si no se aplicado se guarda el subtotal
                    if i.amount_applied == None:
                        i.amount_applied = subtotal
                    else:
                        # Si ya se aplico antes, se suman los valores.
                        i.amount_applied = i.amount_applied + subtotal
                    i.times_applied = i.times_applied + 1
                    if i.times_applied == i.number_billing:
                        i.cleared_at = date.today()
                    i.save()
            form.cleaned_data["credit"] = arraycredits

        if charges:
            arraycharges = []
            for i in charges:
                subtotal = calculo(i)
                cargo = ChargeApplied.objects.create(charge_applied=i, amount=subtotal)
                arraycharges.append(cargo.id)
                if i.permanent:
                    # Si no tiene monto aplicado, se guarda 0
                    if i.amount_applied == None:
                        i.amount_applied = 0
                    # Se aumentan ambos valores, para que sea infinito la cant de ciclos.
                    i.times_applied = i.times_applied + 1
                    i.number_billing = i.number_billing + 1
                    i.save()
                else:
                    # Si no se aplicado se guarda el subtotal
                    if i.amount_applied == None:
                        i.amount_applied = subtotal
                    else:
                        # Si ya se aplico antes, se suman los valores.
                        i.amount_applied = i.amount_applied + subtotal
                    i.times_applied = i.times_applied + 1
                    if i.times_applied == i.number_billing:
                        i.cleared_at = date.today()
                    i.save()
            form.cleaned_data["charge"] = arraycharges

        # Calcula el número de folio
        notas = Invoice.objects.filter(kind=7).order_by("-id")
        if notas.count() != 0:
            folio_value = int(notas[0].folio + 1)
        else:
            folio_value = 0

        invoice = form.save(commit=False)
        invoice.folio = folio_value
        invoice.customer = self.customer
        invoice.iva = float(
            (
                Indicators.objects.filter(kind=2, company=self.customer.company)
                .distinct("kind")
                .values("kind", "created_at", "value")
                .first()["value"]
            )
        )
        invoice_created = Invoice.objects.filter(
            folio=folio_value, customer=self.customer, kind=7
        )
        if not invoice_created.exists():
            invoice.total = int(data["total"])
            return super(InvoiceCreateNotaVenta, self).form_valid(form)
        else:
            messages.error(
                self.request,
                "Ya existe una factura con ese folio y el mismo tipo de este cliente",
            )
            return HttpResponseRedirect(
                reverse_lazy(
                    "invoice-create-service-nota",
                    kwargs={
                        "customer_pk": self.customer.id,
                        "slug": self.service.number,
                    },
                )
            )


invoice_create_nota_venta = InvoiceCreateNotaVenta.as_view()


class InvoiceUpdate(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    model = Invoice
    form_class = InvoiceUpdateForm
    success_message = "Se ha actualizado la factura."
    permission_required = "customers.change_invoice"
    template_name = "customers/invoice_update.html"

    def dispatch(self, *args, **kwargs):
        self.invoice = get_object_or_404(Invoice, pk=self.kwargs["pk"])
        try:
            self.customer = self.invoice.customer
            id_customer = self.invoice.customer.id
        except:
            if self.invoice.kind_load != 1:
                com = self.invoice.comment.split(" ")
                self.cleaned_rut = com[len(com) - 1]
        return super(InvoiceUpdate, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(InvoiceUpdate, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if self.invoice.operator.company.id==op.company:
                        ctx["can_show_detail"]=True
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    if self.invoice.operator.id==op.operator:
                        ctx["can_show_detail"]=True
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        ctx["actual_payment"] = Payment.objects.filter(invoice=self.invoice)
        try:
            actual = self.invoice.service.all()
            servicios = Service.objects.filter(customer=self.customer.id)
            for i in actual:
                servicios = servicios.exclude(id=i.id)
            ctx["service"] = servicios
            ctx["customer"] = self.customer
            if ctx["if_todos"]:
                pagos = Payment.objects.filter(customer=self.customer)
            else:
                pagos = Payment.objects.filter(customer=self.customer, operator=op.operator)
            for i in ctx["actual_payment"]:
                pagos = pagos.exclude(id=i.id)
            ctx["pagos"] = pagos.order_by("-created_at")
            ctx["service_act"] = actual
        except:
            cus = Customer.objects.filter(payorrut__rut=self.cleaned_rut)
            if cus.count() != 0:
                ctx["customers"] = cus

        ctx["cancel_url"] = reverse("invoice-detail", args=[self.object.pk])

        user=self.request.user
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
        currency=currency.distinct()
        ctx['monedas']=currency
        return ctx

    def form_invalid(self, form):
        print(form.errors)
        return self.render_to_response(self.get_context_data())

    def form_valid(self, form):
        invoice = form.save(commit=False)
        invoice_created = Invoice.objects.filter(
            folio=self.invoice.folio,
            customer=form.cleaned_data["customer"],
            kind=self.invoice.kind,
        ).exclude(id=self.invoice.id)
        if not invoice_created.exists():
            if (
                form["customer"].value()
                and self.invoice.kind_load == 2
                and (not self.invoice.customer)
            ):
                cus = Customer.objects.get(id=form["customer"].value())
                PayorRut.objects.create(
                    rut=self.cleaned_rut, name=cus.name, customer=cus
                )

            sum_total = 0
            for i in form.cleaned_data["payment"]:
                i.invoice.add(self.invoice)
                i.save()
                sum_total = sum_total + i.amount

            if (not (self.invoice.paid_on)) and sum_total >= self.invoice.total:
                invoice.paid_on = date.today()

            return super(InvoiceUpdate, self).form_valid(form)
        else:
            messages.error(
                self.request,
                "Ya existe una factura con ese folio, tipo y el mismo cliente",
            )
            return HttpResponseRedirect(
                reverse_lazy("invoice-update", kwargs={"pk": self.kwargs["pk"]})
            )


invoice_update = InvoiceUpdate.as_view()

# Function to allocate folios on invoices
def check_allocate_invoice(kind, operator):
    batch = InvoiceBatch.objects.filter(
        operator=operator, kind=kind, stock__gt=0, expiration__gte=datetime.now(),
    ) | InvoiceBatch.objects.filter(kind=kind,operator=operator, stock__gt=0, expiration__isnull=True,)
    if batch:
        # The one that have less stock
        selectedBatch = batch.order_by("stock")
        first_batch = selectedBatch[0]
        first_key = list(first_batch.batch.keys())[0]
        lBatch = []
        newBatch = first_batch.batch
        folio = newBatch[first_key]
        lBatch.append(first_key)
        for l in lBatch:
            newBatch.pop(l)
        return True, folio, first_batch.id, newBatch
    else:
        return False, 0, 0, []


# Function to allocate folios on invoices
def allocate_invoice(kind, after, customer):
    batch = InvoiceBatch.objects.filter(
        kind=kind, operator=operator,stock__gt=0, expiration__gte=datetime.now(),
    ) | InvoiceBatch.objects.filter(kind=kind,operator=operator, stock__gt=0, expiration__isnull=True,)
    result = {}
    if batch:
        toProcess = {}
        # The one that have less stock
        selectedBatch = batch.order_by("stock")
        first_batch = selectedBatch[0]
        first_key = list(first_batch.batch.keys())[0]
        lBatch = []
        newBatch = first_batch.batch
        batchBefore = selectedBatch[0].batch
        folio = newBatch[first_key]
        if after:
            array = []
            for i in after:
                try:
                    array.append(int(i))
                except:
                    array.append(i.id)
            content = {"customer": customer, "services": array}
            toProcess[folio] = {
                "idFolio": first_batch.id,
                "unified": content,
                "folio": {first_key: folio},
            }
            result.update(toProcess)
        lBatch.append(first_key)
        for l in lBatch:
            newBatch.pop(l)

        # Update the batch
        InvoiceBatch.objects.filter(pk=first_batch.id).update(
            batch=newBatch, stock=len(newBatch)
        )
        return result, folio, first_batch.id, first_key, batchBefore
    else:
        return result, 0, 0, 0, 0, 0


class InvoiceCreateNotaCredito(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, FormView
):
    success_message = "Se ha creado una nueva factura."
    permission_required = "customers.add_invoice"
    template_name = "customers/invoice_form_credit_note.html"

    def post(self, request, *args, **kwargs):

        form = InvoiceNotaCreditoCreateForm(self.request.POST)
        ServiceQuantityFormSet = formset_factory(
            NotaCreditoForm, extra=self.invoice.service.count()
        )
        formset = ServiceQuantityFormSet(self.request.POST)

        if form.is_valid() and formset.is_valid():
            form.cleaned_data["service"] = self.invoice.service.all()
            avaliable_folio, folio_value, id_lote, newBatch = check_allocate_invoice(3, self.operator)
            # Si no hay folios disponibles muestra mensaje de error
            if not (avaliable_folio):
                messages.error(
                    self.request,
                    "Se acabaron los folios disponibles, registre un nuevo lote para este tipo de Boleta",
                )
                return HttpResponseRedirect(
                    reverse_lazy(
                        "invoice-create-service-nota-credito",
                        kwargs={
                            "customer_pk": self.customer.id,
                            "slug": self.invoice.id,
                        },
                    )
                )
            # Codigos de los documentos
            if self.invoice.kind == 1:
                code_doc = 39
            elif self.invoice.kind == 2:
                code_doc = 33
            else:
                code_doc = 56
            data = {
                "folio": int(folio_value),  # Folio de la nota de crédito
                "operator": OperatorInformation.objects.filter(
                    operator_id=self.invoice.operator.id
                ).first(),  # operator = OperatorInformation.objects.filter(operator_id=1).first()
                "customer": self.customer,  # customer = Customer.objects.filter(id=1234).first()
                "service": self.invoice.service.all(),  # service = Service.objects.filter(id__in=[1234, 4321])
            }
            try:
                data["iva"] = float(self.invoice.iva)  # Iva del documento
            except:
                data["iva"] = float(0.19)
            nulling = False
            if form.cleaned_data["code"] == "1":
                nulling = True
                data["ref"] = {
                    "TpoDocRef": code_doc,  # factura: 33, boleta: 39, n. débito: 56
                    "FolioRef": int(self.invoice.folio),  # Folio del documento afectado
                    "FchRef": str(datetime.now()).split(" ")[
                        0
                    ],  # Fecha en la que fue creado el documento afectado
                    "CodRef": 1,  # Anulacion: 1, texto: 2, monto: 3
                }
                total = self.invoice.total
            elif form.cleaned_data["code"] == "2":
                data["ref"] = {
                    "TpoDocRef": code_doc,  # factura: 33, boleta: 39, n. débito: 56
                    "FolioRef": int(self.invoice.folio),  # Folio del documento afectado
                    "FchRef": str(datetime.now()).split(" ")[
                        0
                    ],  # Fecha en la que fue creado el documento afectado
                    "CodRef": 2,  # Anulacion: 1, texto: 2, monto: 3
                }
                data["textCorrection"] = [
                    {
                        "previousDescription": form.cleaned_data["previous_text"],
                        "newDescription": form.cleaned_data["new_text"],
                    },
                ]
                total = 0
            else:
                data["ref"] = {
                    "TpoDocRef": code_doc,  # factura: 33, boleta: 39, n. débito: 56
                    "FolioRef": int(self.invoice.folio),  # Folio del documento afectado
                    "FchRef": str(datetime.now()).split(" ")[
                        0
                    ],  # Fecha en la que fue creado el documento afectado
                    "CodRef": 3,  # Anulacion: 1, texto: 2, monto: 3
                }
                json_inv = {}
                data["amountCorrection"] = []
                for i in formset.forms:
                    if "planCode" in i.cleaned_data:
                        code_val = i.cleaned_data["planCode"]
                        try:
                            plan_name = Plan.objects.get(id=code_val).name
                        except:
                            messages.error(
                                self.request,
                                "Ocurrió un error, el código "
                                + str(code_val)
                                + " no es válido",
                            )
                            return HttpResponseRedirect(
                                reverse_lazy(
                                    "invoice-create-service-nota-credito",
                                    kwargs={
                                        "customer_pk": self.customer.id,
                                        "slug": self.invoice.id,
                                    },
                                )
                            )
                        if "quantity" in i.cleaned_data:
                            quantity = i.cleaned_data["quantity"]
                        else:
                            quantity = 1
                        if "price" in i.cleaned_data:
                            if i.cleaned_data["price"]!=None:
                                price = i.cleaned_data["price"]
                            else:
                                messages.error(
                                    self.request,
                                    "No indico el precio sin iva",
                                )
                                return HttpResponseRedirect(
                                    reverse_lazy(
                                        "invoice-create-service-nota-credito",
                                        kwargs={
                                            "customer_pk": self.customer.id,
                                            "slug": self.invoice.id,
                                        },
                                    )
                                )
                        else:
                            price = 0

                        try:
                            # Si tiene json, puede contrarestar los valores.
                            if self.invoice.json:
                                for s in self.invoice.service.all():
                                    if s.plan.id == code_val:
                                        json_inv[str(s.id)] = {
                                            "QtyItem": quantity,
                                            "PrcItem": str(
                                                float(
                                                    self.invoice.json[str(s.id)][
                                                        "PrcItem"
                                                    ]
                                                )
                                                - round(float(price), 2)
                                            ),
                                            "MontoItem": str(
                                                float(
                                                    self.invoice.json[str(s.id)][
                                                        "MontoItem"
                                                    ]
                                                )
                                                - round(float(price), 0)
                                            ),
                                        }
                        except:
                            pass

                        data["amountCorrection"].append(
                            {
                                "VlrCodigo": code_val,  # Id del item (plan)
                                "NmbItem": plan_name,
                                "PrcItem": float(price),  # Precio corregido sin iva,
                                "QtyItem": quantity,  # Por defecto 1 en caso de ser plan o servicio
                            }
                        )
                total = form.cleaned_data["total"]

                if self.invoice.json:
                    for s in self.invoice.service.all():
                        if str(s.id) in json_inv:
                            pass
                        else:
                            json_inv[str(s.id)] = {
                                "QtyItem": 0,
                                "PrcItem": str(round(float(0), 2)),
                                "MontoItem": str(round(float(0), 0)),
                            }

            invoice_created = Invoice.objects.filter(
                folio=folio_value,
                customer=self.customer,
                kind=form.cleaned_data["kind"],
            )
            if not invoice_created.exists():
                try:
                    go = creditNote(data)
                    if nulling:
                        folioToUpdate = (
                            Invoice.objects.filter(pk=kwargs["slug"]).first().folio
                        )
                        invoiceNulling(folioToUpdate)
                except Exception as e:
                    print(str(e))
                    messages.error(
                        self.request, "Ocurrió un error de conexión con Facturacion.cl",
                    )
                    return HttpResponseRedirect(
                        reverse_lazy(
                            "invoice-create-service-nota-credito",
                            kwargs={
                                "customer_pk": self.customer.id,
                                "slug": self.invoice.id,
                            },
                        )
                    )
                response = json.loads(go["documentList"])
                if response[0]["resultado"] == "False":
                    messages.error(
                        self.request,
                        "Ocurrió un error mientras se procesaba en Facturacion.cl, "
                        + str(response[0]["error"]),
                    )

                    return HttpResponseRedirect(
                        reverse_lazy(
                            "invoice-create-service-nota-credito",
                            kwargs={
                                "customer_pk": self.customer.id,
                                "slug": self.invoice.id,
                            },
                        )
                    )

                # Update the batch
                InvoiceBatch.objects.filter(pk=id_lote).update(
                    batch=newBatch, stock=len(newBatch)
                )

                iva = (
                    Indicators.objects.filter(kind=2, company=self.customer.company)
                    .distinct("kind")
                    .values("kind", "created_at", "value")
                    .first()["value"]
                )

                inv = Invoice.objects.create(
                    created_at=datetime.now(),
                    due_date=datetime.now(),
                    folio=int(folio_value),
                    kind=3,
                    comment=form.cleaned_data["comment"],
                    customer=self.customer,
                    total=total,
                    operator=self.invoice.operator,
                    iva=iva,
                    parent_id=self.invoice,
                    currency=form.cleaned_data["currency"],
                )
                inv.service = self.invoice.service.all()
                inv.save()
                return HttpResponseRedirect(
                    reverse_lazy("invoice-detail", kwargs={"pk": inv.id})
                )

            else:
                messages.error(
                    self.request,
                    "Ya existe una factura con ese folio y el mismo tipo de este cliente",
                )
                return HttpResponseRedirect(
                    reverse_lazy(
                        "invoice-create-service-nota-credito",
                        kwargs={
                            "customer_pk": self.customer.id,
                            "slug": self.invoice.id,
                        },
                    )
                )
        else:
            return self.form_invalid(form, formset, **kwargs)

    def form_invalid(self, form, formset, **kwargs):
        form.prefix = "form"
        formset.prefix = "formset"
        return self.render_to_response(self.get_context_data())

    def dispatch(self, *args, **kwargs):
        self.customer = get_object_or_404(Customer, pk=self.kwargs["customer_pk"])
        self.invoice = get_object_or_404(Invoice, pk=self.kwargs["slug"])
        self.operator=self.invoice.operator
        return super(InvoiceCreateNotaCredito, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = (
            {}
        )  # super(InvoiceCreateNotaCredito, self).get_context_data(*args, **kwargs)
        ctx["customer"] = self.customer
        ctx["invoice"] = self.invoice
        ctx["cancel_url"] = reverse("invoice-list", args=[self.customer.pk])
        ctx["url"] = self.invoice.get_absolute_url
        show_op1 = False
        # Solo se puede anular facturas o notas de debito
        if self.invoice.kind in [1, 2, 4]:
            show_op1 = True
        ctx["show_op1"] = show_op1
        # Forms
        ServiceQuantityFormSet = formset_factory(
            NotaCreditoForm, extra=self.invoice.service.count()
        )
        ctx["form"] = InvoiceNotaCreditoCreateForm()
        ctx["formset"] = ServiceQuantityFormSet

        # Verifica si hay folios disponibles.
        avaliable_folio, folio, id_lote, newBatch = check_allocate_invoice(3, self.operator)
        if not (avaliable_folio):
            ctx["alert"] = "No hay folios disponibles."
        
        user=self.request.user
        if user:
            username = user.username
            user1=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user1).company.all()
            try:
                op=UserOperator.objects.get(user=user1)    
                if op.operator==0:
                    operators = Operator.objects.filter(company__id=op.company)
                else:
                    operators = Operator.objects.filter(id=op.operator)
            except:
                pass
        
        currency=Currency.objects.none()
        for o in operators:
            if o.currency.all() is not None:
                currency= currency | o.currency.all()
        currency=currency.distinct()
        ctx['monedas']=currency
        return ctx


invoice_create_nota_credito = InvoiceCreateNotaCredito.as_view()


class InvoiceCreateNotaCobro(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView
):
    model = Invoice
    form_class = InvoiceNotaCobroCreateForm
    success_message = "Se ha creado una nueva factura."
    permission_required = "customers.add_invoice"
    template_name = "customers/invoice_form_cobro_note.html"

    def dispatch(self, *args, **kwargs):
        self.customer = get_object_or_404(Customer, pk=self.kwargs["customer_pk"])
        self.invoice = get_object_or_404(Invoice, pk=self.kwargs["slug"])
        return super(InvoiceCreateNotaCobro, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(InvoiceCreateNotaCobro, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(InvoiceCreateNotaCobro, self).get_context_data(*args, **kwargs)
        ctx["customer"] = self.customer
        ctx["invoice"] = self.invoice
        ctx["cancel_url"] = reverse("invoice-list", args=[self.customer.pk])
        ctx["url"] = self.invoice.get_absolute_url
        """
        #Verifica si hay folios disponibles.
        avaliable_folio, folio, id_lote, newBatch=check_allocate_invoice(6)
        if not(avaliable_folio):
            ctx['alert']="No hay folios disponibles."
        """
        return ctx

    def form_valid(self, form):
        form.cleaned_data["service"] = self.invoice.service.all()
        notas = Invoice.objects.filter(kind=6).order_by("-folio")
        if notas.count() != 0:
            folio_value = int(notas[0].folio + 1)
        else:
            folio_value = 0
        """
        avaliable_folio, folio_value, id_lote, newBatch=check_allocate_invoice(6)
        
        #Si no hay folios disponibles muestra mensaje de error
        if not(avaliable_folio):
            messages.error(self.request,'Se acabaron los folios disponibles, registre un nuevo lote para este tipo de Boleta')
            return HttpResponseRedirect(reverse_lazy('invoice-create-service-nota-cobro',kwargs={'customer_pk': self.customer.id, 'slug': self.invoice.id}))
        """
        invoice = form.save(commit=False)
        invoice.folio = folio_value
        invoice.operator = self.invoice.operator
        invoice.due_date = datetime.now()
        invoice.customer = self.customer
        invoice.parent_id = self.invoice
        invoice.iva = float(
            (
                Indicators.objects.filter(kind=2, company=self.customer.company)
                .distinct("kind")
                .values("kind", "created_at", "value")
                .first()["value"]
            )
        )
        invoice_created = Invoice.objects.filter(
            folio=folio_value, customer=self.customer, kind=6
        )

        if not invoice_created.exists():
            """# Update the batch
            InvoiceBatch.objects.filter(pk=id_lote).update(
                batch=newBatch, stock=len(newBatch)
            )"""
            return super(InvoiceCreateNotaCobro, self).form_valid(form)
        else:
            messages.error(
                self.request,
                "Ya existe una factura con ese folio y el mismo tipo de este cliente",
            )
            return HttpResponseRedirect(
                reverse_lazy(
                    "invoice-create-service-nota-cobro",
                    kwargs={"customer_pk": self.customer.id, "slug": self.invoice.id},
                )
            )


invoice_create_nota_cobro = InvoiceCreateNotaCobro.as_view()


class InvoiceCreateNotaDebito(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView
):
    model = Invoice
    form_class = InvoiceNotaDebitoCreateForm
    success_message = "Se ha creado una nueva factura."
    permission_required = "customers.add_invoice"
    template_name = "customers/invoice_form_debito_note.html"

    def dispatch(self, *args, **kwargs):
        self.customer = get_object_or_404(Customer, pk=self.kwargs["customer_pk"])
        self.invoice = get_object_or_404(Invoice, pk=self.kwargs["slug"])
        return super(InvoiceCreateNotaDebito, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(InvoiceCreateNotaDebito, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(InvoiceCreateNotaDebito, self).get_context_data(*args, **kwargs)
        ctx["customer"] = self.customer
        ctx["invoice"] = self.invoice
        ctx["cancel_url"] = reverse("invoice-list", args=[self.customer.pk])
        ctx["url"] = self.invoice.get_absolute_url
        """
        #Verifica si hay folios disponibles.
        avaliable_folio, folio, id_lote, newBatch=check_allocate_invoice(4)
        if not(avaliable_folio):
            ctx['alert']="No hay folios disponibles."
        """
        return ctx

    def form_valid(self, form):
        form.cleaned_data["service"] = self.invoice.service.all()
        notas = Invoice.objects.filter(kind=8).order_by("-folio")
        if notas.count() != 0:
            folio_value = int(notas[0].folio + 1)
        else:
            folio_value = 0
        """
        avaliable_folio, folio_value, id_lote, newBatch=check_allocate_invoice(4)
        
        #Si no hay folios disponibles muestra mensaje de error
        if not(avaliable_folio):
            messages.error(self.request,'Se acabaron los folios disponibles, registre un nuevo lote para este tipo de Boleta')
            return HttpResponseRedirect(reverse_lazy('invoice-create-service-nota-debito',kwargs={'customer_pk': self.customer.id, 'slug': self.invoice.id}))
        """
        invoice = form.save(commit=False)
        invoice.folio = folio_value
        invoice.operator = self.invoice.operator
        invoice.customer = self.customer
        invoice.parent_id = self.invoice
        invoice.iva = float(
            (
                Indicators.objects.filter(kind=2, company=self.customer.company)
                .distinct("kind")
                .values("kind", "created_at", "value")
                .first()["value"]
            )
        )
        invoice_created = Invoice.objects.filter(
            folio=folio_value, customer=self.customer, kind=8
        )

        if not invoice_created.exists():
            """# Update the batch
            InvoiceBatch.objects.filter(pk=id_lote).update(
                batch=newBatch, stock=len(newBatch)
            )"""
            return super(InvoiceCreateNotaDebito, self).form_valid(form)
        else:
            messages.error(
                self.request,
                "Ya existe una factura con ese folio y el mismo tipo de este cliente",
            )
            return HttpResponseRedirect(
                reverse_lazy(
                    "invoice-create-service-nota-debito",
                    kwargs={"customer_pk": self.customer.id, "slug": self.invoice.id},
                )
            )


invoice_create_nota_debito = InvoiceCreateNotaDebito.as_view()


class GlobalInvoiceList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Invoice
    paginate_by = 8
    template_name = "customers/global_invoice_list.html"
    permission_required = "customers.list_invoice"


global_invoice_list = GlobalInvoiceList.as_view()


class NewGlobalInvoiceList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Invoice
    paginate_by = 8
    template_name = "customers/new_global_invoice_list.html"
    permission_required = "customers.list_invoice"

    def get_context_data(self, *args, **kwargs):
        ctx = super(NewGlobalInvoiceList, self).get_context_data(*args, **kwargs)
        ctx["invoice_options"] = Invoice.INVOICE_CHOICES
        ctx["operators"] = Operator.objects.all()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx


global_invoice_list_new = NewGlobalInvoiceList.as_view()


class InvoiceListWithoutCustomer(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Invoice
    paginate_by = 8
    template_name = "customers/invoice_list_without_customer.html"
    permission_required = "customers.list_invoice"

    def get_context_data(self, *args, **kwargs):
        ctx = super(InvoiceListWithoutCustomer, self).get_context_data(*args, **kwargs)
        ctx["invoice_options"] = Invoice.INVOICE_CHOICES
        ctx["operators"] = Operator.objects.all()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx


invoice_list_without_customer = InvoiceListWithoutCustomer.as_view()


@login_required
@permission_required("customers.delete_invoice")
def delete_invoice(request, pk):
    inv = get_object_or_404(Invoice, pk=pk)
    customer_pk = inv.customer.id

    if inv.kind in [4, 9]:
        inv.delete()

    return HttpResponseRedirect(
        reverse_lazy("invoice-list", kwargs={"customer_pk": customer_pk})
    )


@login_required
@permission_required("customers.change_service")
def mark_as_paid(request, pk):
    Invoice.objects.filter(pk=pk).update(paid_on=date.today())
    # Revisando si existe una prorroga de esa boleta.
    inv=Invoice.objects.get(pk=pk)
    try:
        if not(inv.customer.flag is None):
            if "prorrogas" in inv.customer.flag:
                if str(inv.id) in inv.customer.flag['prorrogas']:
                    fecha_actual=datetime.now()
                    fecha=inv.customer.flag['prorrogas'][str(inv.id)]['date']
                    if dateutil.parser.isoparse(fecha)>=fecha_actual:
                        inv.customer.flag['prorrogas'][str(inv.id)]['paid_on_time']=True
                        inv.customer.flag['prorrogas'][str(inv.id)]['paid_on']=datetime.now().isoformat()
                    else:
                        inv.customer.flag['prorrogas'][str(inv.id)]['paid_on_time']=False
                        inv.customer.flag['prorrogas'][str(inv.id)]['paid_on']=datetime.now().isoformat()
                    inv.customer.save()
    except:
        pass
    messages.add_message(request, messages.SUCCESS, "Se ha marcado como pagada.")
    return redirect(request.META.get("HTTP_REFERER", reverse("global-invoice-list")))

@login_required
@permission_required("customers.change_service")
def mark_all_invoice_as_paid(request, slug):
    servicio=Service.objects.get(pk=slug)
    boletas_sin_pagar=servicio.invoice_set.filter(paid_on__isnull=True)
    for inv in boletas_sin_pagar:
        try:
            if not(inv.customer.flag is None):
                if "prorrogas" in inv.customer.flag:
                    if str(inv.id) in inv.customer.flag['prorrogas']:
                        fecha_actual=datetime.now()
                        fecha=inv.customer.flag['prorrogas'][str(inv.id)]['date']
                        if dateutil.parser.isoparse(fecha)>=fecha_actual:
                            inv.customer.flag['prorrogas'][str(inv.id)]['paid_on_time']=True
                            inv.customer.flag['prorrogas'][str(inv.id)]['paid_on']=datetime.now().isoformat()
                        else:
                            inv.customer.flag['prorrogas'][str(inv.id)]['paid_on_time']=False
                            inv.customer.flag['prorrogas'][str(inv.id)]['paid_on']=datetime.now().isoformat()
                        inv.customer.save()
        except:
            pass
    boletas=boletas_sin_pagar.update(paid_on=date.today())

    messages.add_message(request, messages.SUCCESS, "Ya todas sus boletas estan pagadas.")
    return redirect(request.META.get("HTTP_REFERER", reverse("global-invoice-list")))


@login_required
@permission_required("customers.change_service")
def mark_as_unpaid(request, pk):
    Invoice.objects.filter(pk=pk).update(paid_on=None)
    messages.add_message(request, messages.SUCCESS, "Se ha marcado como no pagada.")
    return redirect(request.META.get("HTTP_REFERER", reverse("global-invoice-list")))


class BankAccountList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = BankAccount
    paginate_by = 8
    template_name = "customers/bank_account_list.html"
    permission_required = "customers.list_bankaccount"

    def get_context_data(self, *args, **kwargs):
        ctx = super(BankAccountList, self).get_context_data(*args, **kwargs)
        #ctx["operators"] = Operator.objects.all()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx

bank_account_list = BankAccountList.as_view()

@login_required
@permission_required("customers.list_bankaccount")
def bank_accounts_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "tag",
            "1": "account_number",
            "2": "bank",
            "3": "operator",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        operator = request.GET.get("operator", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_bank_accounts = BankAccount.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_bank_accounts = BankAccount.objects.all()

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_bank_accounts = all_bank_accounts.filter(operator=op.operator)
                else:
                    all_bank_accounts = all_bank_accounts.filter(operator__company__id=op.company)
            except:
                pass
        
        if operator and operator!="":
            all_bank_accounts = all_bank_accounts.filter(operator=operator)

        all_count = all_bank_accounts.count()

        if search:

            filtered_bank_accounts = all_bank_accounts.filter(
                Q(bank__name__unaccent__icontains=search)
                | Q(tag__unaccent__icontains=search)
                | Q(account_number__icontains=search)
            )
        else:
            filtered_bank_accounts = all_bank_accounts

        filtered_bank_accounts = filtered_bank_accounts.distinct()

        filtered_count = filtered_bank_accounts.count()

        if length == -1:
            sliced_bank_accounts = filtered_bank_accounts
        else:
            sliced_bank_accounts = filtered_bank_accounts[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_bank_account, sliced_bank_accounts)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_bank_account(bank_account):

    try:
        dictionary = {
            "account_number":bank_account.account_number,
            "operator": bank_account.operator.name,
            "currency": bank_account.currency.name if bank_account.currency else "",
            "bank": bank_account.bank.name,
            "tag": bank_account.tag
        }
    except:
        dictionary = {
            "account_number":bank_account.paid_on,
            "operator": bank_account.operator.name,
            "bank": bank_account.customer_operator,
            "currency": bank_account.currency.name if bank_account.currency else "",
            "tag": bank_account.tag
        }
    return dictionary

class CartolasList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Cartola
    paginate_by = 8
    template_name = "customers/cartolas_list.html"
    permission_required = "customers.list_cartolas"

    def dispatch(self, *args, **kwargs):
        self.account = get_object_or_404(BankAccount, account_number=self.kwargs["bank_account"])
        return super(CartolasList, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(CartolasList, self).get_context_data(*args, **kwargs)
        ctx["account"] = self.account
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx    

cartolas_list = CartolasList.as_view()

@login_required
@permission_required("customers.list_cartolas")
def cartolas_ajax(request, **kwargs):

    if request.is_ajax():
        # ordering
        columns = {
            "0": "number",
            "1": "start",
            "2": "end",
            "3": "saldo_final",
            "4": "saldo_inicial",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        # filters
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)

        if order_column and order_dir:
            all_cartolas = Cartola.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_cartolas = Cartola.objects.all()

        all_cartolas = Cartola.objects.filter(bank_account__account_number=kwargs["bank_account"])

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
            except:
                pass

        if op.operator!=0:
            all_cartolas = all_cartolas.filter(bank_account__operator=op.operator)
        else:
            all_cartolas = all_cartolas.filter(bank_account__operator__company__id=op.company)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                all_cartolas = all_cartolas.filter(
                start__gte=datetime(year, month, day))
            except pytz.exceptions.AmbiguousTimeError:
                all_cartolas = all_cartolas.filter(
                start__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True) 
                )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            try:
                all_cartolas = all_cartolas.filter(
                    end__lt=datetime(year, month, day, 23, 59, 59))
            except pytz.exceptions.AmbiguousTimeError:
                all_cartolas = all_cartolas.filter(
                    end__lt=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True) 
                )

        all_count = all_cartolas.count()
        
        if search:

            filtered_cartolas = all_cartolas.filter(
                Q(number__icontains=search)
                | Q(saldo_inicial__icontains=search)
                | Q(saldo_final__icontains=search)
            )
        else:
            filtered_cartolas = all_cartolas

        filtered_cartolas = filtered_cartolas.distinct()

        filtered_count = filtered_cartolas.count()

        if length == -1:
            sliced_cartolas = filtered_cartolas
        else:
            sliced_cartolas = filtered_cartolas[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_cartola, sliced_cartolas)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_cartola(cartola):

    try:
        dictionary = {
            "number":cartola.number,
            "start": cartola.start.strftime("%d-%m-%Y"),
            "end": cartola.end.strftime("%d-%m-%Y"),
            "saldo_final": price_tag(cartola.saldo_final),
            "saldo_inicial": price_tag(cartola.saldo_inicial),
            "total_cargo": price_tag(cartola.total_cargo),
            "total_abono": price_tag(cartola.total_abono),
            "pdf": cartola.pdf.url if cartola.pdf else None,
            "excel": cartola.excel.url if cartola.excel else None,
            "symbol": cartola.bank_account.currency.symbol if cartola.bank_account.currency.symbol else "$",
            "pk": cartola.pk
        }
    except:
        dictionary = {
            "number":cartola.number,
            "start": cartola.start.strftime("%d-%m-%Y"),
            "end": cartola.end.strftime("%d-%m-%Y"),
            "saldo_final": price_tag(cartola.saldo_final),
            "saldo_inicial": price_tag(cartola.saldo_inicial),
            "total_cargo": price_tag(cartola.total_cargo),
            "total_abono": price_tag(cartola.total_abono),
            "pdf": cartola.pdf.url if cartola.pdf else None,
            "excel": cartola.excel.url if cartola.excel else None,
            "symbol": cartola.bank_account.currency.symbol if cartola.bank_account.currency.symbol else "$",
            "pk": cartola.pk
        }
    return dictionary

class CartolaMovementsList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = CartolaMovement
    paginate_by = 8
    template_name = "customers/cartola_movements_list.html"
    permission_required = "customers.list_movimiento_cartola"

    def dispatch(self, *args, **kwargs):
        self.cartola = get_object_or_404(Cartola, pk=self.kwargs["pk"])
        self.account = get_object_or_404(BankAccount, account_number=self.cartola.bank_account.account_number)
        return super(CartolaMovementsList, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(CartolaMovementsList, self).get_context_data(*args, **kwargs)
        ctx["kind_options"] = CartolaMovement.TYPE_CHOICES
        ctx["cartola"] = self.cartola
        ctx["account"] = self.account
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx

cartola_movements_list = CartolaMovementsList.as_view()


@login_required
@permission_required("customers.list_movimiento_cartola")
def cartola_movements_ajax(request, **kwargs):

    if request.is_ajax():
        # ordering
        columns = {
            "0": "date",
            "1": "sucursal",
            "2": "number_document",
            "3": "description",
            "4": "total",
            "5": "kind",
            "6": "saldo_diario",
            "7": "payment"
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        # filters
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)
        kind = request.GET.get("kind", None)
        conciliado = request.GET.get("conciliado", None)

        if order_column and order_dir:
            all_cartola_movements = CartolaMovement.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_cartola_movements = CartolaMovement.objects.all()

        all_cartola_movements = CartolaMovement.objects.filter(cartola__pk=kwargs["pk"]) 

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
            except:
                pass

        if op.operator!=0:
            all_cartola_movements = all_cartola_movements.filter(cartola__bank_account__operator=op.operator)
        else:
            all_cartola_movements = all_cartola_movements.filter(cartola__bank_account__operator__company__id=op.company)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                all_cartola_movements = all_cartola_movements.filter(
                    date__gte=datetime(year, month, day))
            except pytz.exceptions.AmbiguousTimeError:
                all_cartola_movements = all_cartola_movements.filter(
                    date__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True) 
                )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            try:
                all_cartola_movements = all_cartola_movements.filter(
                    date__lte=datetime(year, month, day, 23, 59, 59))
            except pytz.exceptions.AmbiguousTimeError:
                all_cartola_movements = all_cartola_movements.filter(
                    date__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True) 
                )

        if kind:
            all_cartola_movements = all_cartola_movements.filter(kind=kind)

        if conciliado=="True":
            all_cartola_movements = all_cartola_movements.filter(Q(payment__isnull=False)|Q(payment_provider__isnull=False))
        else:
            all_cartola_movements = all_cartola_movements.filter(Q(payment__isnull=True) & Q(payment_provider__isnull=True))

        all_count = all_cartola_movements.count()
        
        if search:

            filtered_cartola_movements = all_cartola_movements.filter(
                Q(number__document__icontains=search)
                | Q(total__icontains=search)
                | Q(sucursal__unaccent__icontains=search)
                | Q(description__unaccent__icontains=search)
            )
        else:
            filtered_cartola_movements = all_cartola_movements

        filtered_cartola_movements = filtered_cartola_movements.distinct()

        filtered_count = filtered_cartola_movements.count()

        if length == -1:
            sliced_cartola_movements = filtered_cartola_movements
        else:
            sliced_cartola_movements = filtered_cartola_movements[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_cartola_movement, sliced_cartola_movements)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_cartola_movement(cartola_movement):
    if cartola_movement.payment or cartola_movement.payment_provider:
        conciliado=True
        posible_pago=False
    else:
        conciliado=False
        # Los posibles pagos no pueden ya haber sido utilizados.
        pagos=(
            CartolaMovement.objects.filter(payment__isnull=False)
            .order_by("payment_id")
            .values_list("payment_id")
            .distinct("payment_id")
            )

        if cartola_movement.kind==2:
            posible_pago=Payment.objects.filter(
                number_document=cartola_movement.number_document, 
                amount = cartola_movement.total,
                bank_account=cartola_movement.cartola.bank_account,
                paid_on__contains=cartola_movement.date).exclude(
                    id__in=pagos
                )
            if posible_pago.count()==0:
                posible_pago=Payment.objects.filter(
                    number_document__isnull=True, 
                    amount = cartola_movement.total,
                    bank_account=cartola_movement.cartola.bank_account,
                    paid_on__contains=cartola_movement.date).exclude(
                    id__in=pagos
                )
            if posible_pago.count()==1:
                posible_pago=True
            else:
                posible_pago=False
        else:
            posible_pago=False
        #print(posible_pago)
    
    try:
        dictionary = {
            "date": cartola_movement.date.strftime("%d-%m-%Y"),
            "sucursal": cartola_movement.sucursal,
            "number_document": cartola_movement.number_document,
            "description": cartola_movement.description,
            "total": price_tag(cartola_movement.total),
            "kind": cartola_movement.get_kind_display(),
            "kind_number": cartola_movement.kind,
            "saldo_diario": price_tag(cartola_movement.saldo_diario),
            "payment": cartola_movement.payment.comment if cartola_movement.payment else '',
            "payment_provider": cartola_movement.payment_provider.comment if cartola_movement.payment_provider else '',
            "pk_payment": cartola_movement.payment.id if cartola_movement.payment else '',
            "pk_payment_provider": cartola_movement.payment_provider.id if cartola_movement.payment_provider else '',
            "symbol": cartola_movement.cartola.bank_account.currency.symbol if cartola_movement.cartola.bank_account.currency.symbol else "$",
            "pk": cartola_movement.pk,
            "conciliado":conciliado,
            "posible_pago":posible_pago,
        }
    except:
         dictionary = {
            "date": cartola_movement.date.strftime("%d-%m-%Y"),
            "sucursal": cartola_movement.sucursal,
            "number_document": cartola_movement.number_document,
            "description": cartola_movement.description,
            "total": price_tag(cartola_movement.total),
            "kind": cartola_movement.get_kind_display(),
            "saldo_diario": price_tag(cartola_movement.saldo_diario),
            "payment": '',
            "payment_provider": '',
            "pk_payment": '',
            "pk_payment_provider": '',
            "pk": cartola_movement.pk,
            "conciliado":conciliado,
            "posible_pago":posible_pago,
        }
    return dictionary

class CartolaMovementsUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = CartolaMovement
    form_class = CartolaMovementUpdateForm
    success_message = "Se ha actualizado la información del movimiento"
    template_name = "customers/cartola_movements_update.html"
    permission_required = "customers.change_movimiento_cartola"

    def dispatch(self, *args, **kwargs):
        self.movement = get_object_or_404(CartolaMovement, pk=self.kwargs["pk"])
        self.cartola = get_object_or_404(Cartola, pk=self.movement.cartola.pk)
        self.account = get_object_or_404(BankAccount, account_number=self.cartola.bank_account.account_number)
        return super(CartolaMovementsUpdate, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(CartolaMovementsUpdate, self).get_context_data(*args, **kwargs)
        ctx["cartola"] = self.cartola
        ctx["account"] = self.account
        ctx["movement"] = self.movement
        if self.movement.kind==2:
            # Cuando es de tipo abono son los pagos de customer.
            pagos=(
                CartolaMovement.objects.filter(payment__isnull=False)
                .order_by("payment_id")
                .values_list("payment_id")
                .distinct("payment_id")
                )
            # Fechas para calcular antes y desps.
            fecha=datetime(self.movement.date.year, self.movement.date.month, self.movement.date.day)
            today = datetime.now()    
            before = fecha - timedelta(days=1)
            after = fecha + timedelta(days=2)
            payment=Payment.objects.filter(
                    number_document=self.movement.number_document, 
                    amount = self.movement.total,
                    bank_account=self.movement.cartola.bank_account,
                    paid_on__gte=before,
                    paid_on__lte=after,
                    ).exclude(
                    id__in=pagos
            )
            if payment.count()==0:
                payment=Payment.objects.filter(
                        number_document__isnull=True, 
                        amount = self.movement.total,
                        bank_account=self.movement.cartola.bank_account,
                        paid_on__gte=before,
                        paid_on__lte=after).exclude(
                        id__in=pagos
                    )
            ctx["payments"] = payment
            ctx["active_payment"] = self.movement.payment
            ctx["not_payment_provider"] = True
        else:
            # Cuando es cargo es pago de proveedores.
            ctx["payments_provider"] = ProviderPayment.objects.filter(paid_on__contains=self.movement.date)
            ctx["active_payment_provider"] = self.movement.payment_provider
            ctx["not_payment"] = True
        return ctx

    def get_success_url(self):
        return reverse(
            "cartolas-movements-list",
            kwargs={"pk": self.cartola.pk},
        )
    
    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        
        self.object = form.save()
        if self.object.payment:
            pago=self.object.payment
            if not (pago.cleared):
                pago.cleared=True
                pago.save()
        messages.add_message(
            self.request, messages.SUCCESS, "Se ha asociado el pago."
        )
        return super(CartolaMovementsUpdate, self).form_valid(form)

cartola_movements_update = CartolaMovementsUpdate.as_view()

@login_required
def conciliar_movimientos(request, pk):
    # Me traigos los movimientos que me interesa, los no conciliados.
    movimientos=CartolaMovement.objects.filter(cartola__id=pk, payment__isnull=True, kind=2)

    username = None
    context={}
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        try:
            op=UserOperator.objects.get(user=user)
        except:
            pass

    if op.operator!=0:
        movimientos = movimientos.filter(cartola__bank_account__operator=op.operator)
    else:
        movimientos = movimientos.filter(cartola__bank_account__operator__company__id=op.company)

    for i in movimientos:

        # Los posibles pagos no pueden ya haber sido utilizados.
        pagos=(
                CartolaMovement.objects.filter(payment__isnull=False)
                .order_by("payment_id")
                .values_list("payment_id")
                .distinct("payment_id")
        )
        posible_pago=Payment.objects.filter(
            number_document=i.number_document, 
            amount = i.total,
            bank_account=i.cartola.bank_account,
            paid_on__contains=i.date).exclude(
                id__in=pagos
        )
        if posible_pago.count()==0:
            posible_pago=Payment.objects.filter(
                number_document__isnull=True, 
                amount = i.total,
                bank_account=i.cartola.bank_account,
                paid_on__contains=i.date).exclude(
                id__in=pagos
            )
        if posible_pago.count()==1:
            pago=posible_pago.first()
            i.payment=pago
            i.save()

            if not(pago.cleared):
                pago.cleared=True
                pago.save()
        else:
            print("No paso")

    messages.add_message(request, messages.SUCCESS, "Se han conciliado las que tienen pagos que coninciden.")
    return redirect(request.META.get("HTTP_REFERER", reverse("cartolas-movements-list", kwargs={"pk": pk})))    

@login_required
@permission_required("customers.change_service")
def promotions_create(request):
    context = {}
    if request.method == "POST":
        form = PromoCreateForm(request.POST, extra=request.POST.get('number_periods'))
        context["form"] = form
        if form.is_valid():
            #print("AJJ")
            data=form.cleaned_data
            if data["plan"].count()!=0:
                #print("Tiene plan")

                # Asumiendo que la fecha es para el inicio de promocion.
                if data["initial_date"]<= date.today():
                    active=True
                else:
                    active=False

                if data['end_date'] is None:
                    new_promo=Promotions.objects.create(
                        name = data["name"],
                        description = data["description"],
                        #plan = data["plan"],
                        number_periods = data["number_periods"],
                        initial_date = data["initial_date"],
                        active=active,
                        operator=data["operator"]
                    )
                else:
                    if data["permanent"]:
                        new_promo=Promotions.objects.create(
                            name = data["name"],
                            description = data["description"],
                            number_periods = data["number_periods"],
                            initial_date = data["initial_date"],
                            active=active,
                            operator=data["operator"]
                        )
                    else:
                        new_promo=Promotions.objects.create(
                            name = data["name"],
                            description = data["description"],
                            number_periods = data["number_periods"],
                            initial_date = data["initial_date"],
                            end_date = data["end_date"],
                            active=active,
                            operator=data["operator"]
                        )

                json_to_promo={"permanent": data["permanent"]}
                services=Service.objects.none()
                for p in data["plan"]:
                    services=services | Service.objects.filter(plan=p)
                    new_promo.plan.add(p)
                    new_promo.save()

                today_is=data['initial_date']
                for index in range(data['number_periods']):
                    #print(i)
                    if data['tipo_descuento{index}'.format(index=index+1)]=="percent":
                        is_percentage=True
                    else:
                        is_percentage=False

                    motivo=MainReason.objects.create(
                        reason = "Promo "+data["name"],
                        valor = data['valor_descuento{index}'.format(index=index+1)],
                        kind = 3,
                        percent = is_percentage,
                        active = True,
                        promo = new_promo,
                        operator=data["operator"]
                    )
                    json_to_promo[index+1]=motivo.id
                    
                    # Falta probar, porque falta la logica para las fechas.
                    if index==0:
                        if data["permanent"]:
                            for s in services:
                                s.promotions.add(new_promo)
                                s.save()
                                Credit.objects.create(
                                        reason = motivo,
                                        service = s,
                                        quantity = 1,
                                        active = active,
                                        permanent= True,
                                        date_to_set_active = data['initial_date']
                                )
                        else:
                            for s in services:
                                s.promotions.add(new_promo)
                                s.save()
                                Credit.objects.create(
                                        reason = motivo,
                                        service = s,
                                        quantity = 1,
                                        active = active,
                                        date_to_set_active = data['initial_date']
                                )
                    else:
                        today_is=next_month(today_is).replace(day=19)
                        for s in services:
                            Credit.objects.create(
                                    reason = motivo,
                                    service = s,
                                    quantity = 1,
                                    active = False,
                                    date_to_set_active = today_is
                            )

                new_promo.info_periods=json_to_promo
                new_promo.save()
            
            else:
                if data["initial_date"]<= date.today():
                    active=True
                else:
                    active=False
                #print("No tiene plan")
                
                if data['end_date'] is None:
                    new_promo=Promotions.objects.create(
                        name = data["name"],
                        description = data["description"],
                        number_periods = data["number_periods"],
                        initial_date = data["initial_date"],
                        active=active,
                        operator=data["operator"]
                    )
                else:
                    if data["permanent"]:
                        new_promo=Promotions.objects.create(
                            name = data["name"],
                            description = data["description"],
                            number_periods = data["number_periods"],
                            initial_date = data["initial_date"],
                            active=active,
                            operator=data["operator"]
                        )
                    else:
                        new_promo=Promotions.objects.create(
                            name = data["name"],
                            description = data["description"],
                            number_periods = data["number_periods"],
                            initial_date = data["initial_date"],
                            end_date = data["end_date"],
                            active=active,
                        operator=data["operator"]
                        )
                
                json_to_promo={"permanent": data["permanent"]}

                for index in range(data['number_periods']):
                    if data['tipo_descuento{index}'.format(index=index+1)]=="percent":
                        is_percentage=True
                    else:
                        is_percentage=False

                    motivo=MainReason.objects.create(
                        reason = "Promo "+data["name"],
                        valor = data['valor_descuento{index}'.format(index=index+1)],
                        kind = 3,
                        percent = is_percentage,
                        active = True,
                        promo = new_promo,
                        operator=data["operator"]
                    )
                    json_to_promo[index+1]=motivo.id

                new_promo.info_periods=json_to_promo
                new_promo.save()

            return redirect(reverse("promotions-list"))

        else:
            print("Form no valido")
            print(form.errors)
    else:
        form = PromoCreateForm()
    if getattr(config, "allow_promotion_anytime")=="no":
        today_is=date.today()
        if today_is < date(today_is.year, today_is.month, 19):
            min_date='0'
        if today_is ==date(today_is.year, today_is.month, 19):
            min_date='0'
        else:
            min_date=str((next_month_date(today_is)-today_is).days)
            #print(min_date)
    else:
        min_date='0'
    operators=Operator.objects.all()
    plans=Plan.objects.all()
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)    
            if op.operator==0:
                operators= Operator.objects.filter(company__id=op.company)
                plans=Plan.objects.filter(operator__company__id=op.company)
            else:
                operators = Operator.objects.filter(id=op.operator)
                plans=Plan.objects.filter(operator__id=op.operator)
        except:
            pass
    context = {
        "operators": operators,
        "plans": plans,
        "my_dict": min_date,
        "cancel_url": reverse("promotions-list"),
        "form": form,
    }
    return render(request, "customers/promotions_create.html", context)

class PromotionsList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Promotions
    paginate_by = 8
    template_name = "customers/promotions_list.html"
    permission_required = "customers.list_promotion"

    def get_context_data(self, *args, **kwargs):
        ctx = super(PromotionsList, self).get_context_data(*args, **kwargs)
        ctx["operators"] = Operator.objects.all()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    ctx["operators"]= Operator.objects.filter(company__id=op.company)
                else:
                    ctx["operators"] = Operator.objects.filter(id=op.operator)
            except:
                pass
        return ctx

promotions_list = PromotionsList.as_view()

@login_required
@permission_required("customers.change_service")
def delete_promotions(request, promotion_pk):
    # Get de promo.
    promo = get_object_or_404(Promotion, pk=promotion_pk)
    # Verificar si solo 
    #promo.status_field = True
    #promo.active=False
    #promo.save()
    """
    for i in range(promo.number_period):
        print(promo.info_periods[i+1])
        motivo=MainReason.objects.get(id=promo.info_periods[i+1])
        motivo.active=False
        # No se si se tiene que eliminar o que.
        descuentos=Credit.objects.filter(reasson=motivo, active=True,cleared_at__isnull=True).update(active=False, cleared_at=datetime.now()) #,status_field=True)
    """
        
    return redirect(reverse("service-extras", kwargs={"slug": credit.service.slug}))

#endpoint para obtener planes por operador, para crear promociones. No sé si se cree así
@login_required
@permission_required("customers.list_plan")
def plans_ajax(request, **kwargs):

    if request.is_ajax():
        
        operator = request.GET["operator"]

        all_plans = Plan.objects.all()

        if operator and operator != "null":
            all_plans = all_plans.filter(operator=operator)

        data = json.dumps(
            {
                "data": list(map(prepare_plan_ajax, all_plans)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_plan_ajax(plan):

    try:
        dictionary = {
            "name": plan.name,
            "pk": plan.pk
        }
    except:
        dictionary = {
            "name": plan.name,
            "pk": plan.pk
        }
    return dictionary


@login_required
@permission_required("customers.list_promotion")
def promotions_ajax(request, **kwargs):

    if request.is_ajax():
        # ordering
        columns = {
            "0": "name",
            "1": "description",
            "2": "plan",
            "3": "periods",
            "4": "initial_date",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        operator = request.GET.get("operator", None)
        active = request.GET.get("active", None)
        permanent = request.GET.get("permanent", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)
        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_promotions = Promotions.objects.filter(status_field=False).order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_promotions = Promotions.objects.filter(status_field=False)

        if operator and operator != "null":
            all_promotions = all_promotions.filter(operator=operator)

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_promotions = all_promotions.filter(operator=op.operator)
                else:
                    all_promotions = all_promotions.filter(operator__company__id=op.company)
            except:
                pass

        if active == "True":
            all_promotions = all_promotions.filter(active=True)
        elif active == "False":
            all_promotions = all_promotions.filter(active=False)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                all_promotions = all_promotions.filter(
                initial_date__gte=datetime(year, month, day)
                )
            except pytz.exceptions.AmbiguousTimeError:
                all_promotions = all_promotions.filter(
                initial_date__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True)
                )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            try:
                all_promotions = all_promotions.filter(
                end_date__lte=datetime(year, month, day, 23, 59, 59)
                )
            except pytz.exceptions.AmbiguousTimeError:
                all_promotions = all_promotions.filter(
                end_date__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True)
                )

        if permanent == "True":
            all_promotions=all_promotions.filter(info_periods__contains={'permanent': True})
        elif permanent == "False":
            all_promotions=all_promotions.filter(info_periods__contains={'permanent': False})

        all_count = all_promotions.count()
        
        if search:
            #cambiar esto cuando se sepan los campos del modelo
            filtered_promotions = all_promotions.filter(
                Q(description__unaccent__icontains=search)
                | Q(name__unaccent__icontains=search)
            )
        else:
            filtered_promotions = all_promotions

        filtered_promotions = filtered_promotions.distinct()

        filtered_count = filtered_promotions.count()

        if length == -1:
            sliced_promotions = filtered_promotions
        else:
            sliced_promotions = filtered_promotions[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_promotion, sliced_promotions)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_promotion(promotion):

    try:
        plans=""
        for i in promotion.plan.all():
            plans=plans+" "+str(i.name)
        dictionary = {
            "name": promotion.name,
            "description": promotion.description if promotion.description else "",
            "plan": plans,
            "periods": promotion.number_periods,
            "pk": promotion.pk, 
            "initial_date": promotion.initial_date.strftime("%d-%m-%Y"),
            "end_date": promotion.end_date.strftime("%d-%m-%Y") if promotion.end_date else ""
        }
    except:
        plans=""
        for i in promotion.plan.all():
            plans=plans+" "+str(i.name)
        dictionary = {
            "name": promotion.name,
            "description": promotion.description if promotion.description else "",
            "plan": plans,
            "periods": promotion.number_periods,
            "pk": promotion.pk, 
            "initial_date": "",
            "end_date": promotion.end_date.strftime("%d-%m-%Y") if promotion.end_date else ""

        }
    return dictionary

class PromotionsUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Promotions
    form_class = PromotionUpdateForm
    success_message = "Se ha actualizado la información de la promoción"
    template_name = "customers/promotions_update.html"
    permission_required = "customers.change_movimiento_cartola"

    def dispatch(self, *args, **kwargs):
        self.promotion = get_object_or_404(Promotions, pk=self.kwargs["pk"])
        if self.promotion.plan.all().count()!=0:
            self.has_plan=True
        else:
            self.has_plan=False
        return super(PromotionsUpdate, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(PromotionsUpdate, self).get_context_data(*args, **kwargs)
        #ctx["operators"] = Operator.objects.all()
        #ctx["plans"] = Plan.objects.all()
        ctx["promotion"] = self.promotion
        descuentos=Credit.objects.filter(reason__promo=self.promotion, status_field=False, cleared_at__isnull=True, reason__kind=3)
        if descuentos.count()>0:
            ctx["need_to_hide"] = True
        else:
            ctx["need_to_hide"] = False
        if getattr(config, "allow_promotion_anytime")=="no":
            today_is=date.today()
            if today_is < date(today_is.year, today_is.month, 19):
                min_date='0'
            elif today_is ==date(today_is.year, today_is.month, 19):
                min_date='0'
            else:
                min_date=str((next_month_date(today_is)-today_is).days)
        else:
            min_date='0'

        ctx["my_dict"] =  min_date
        ctx["init"] = self.promotion.initial_date.strftime("%d/%m/%Y %H:%M:%S")
        if self.promotion.end_date:
            ctx["end"] = self.promotion.end_date.strftime("%d/%m/%Y %H:%M:%S")
        print(self.promotion.active)
        return ctx

    def get_success_url(self):
        return reverse(
            "promotions-list"
        )
    
    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        #print(context["object"])
        try:
            # Si cambio de fecha y tengo plan
            #if self.promotion.initial_date!=form["initial_date"].value() and self.has_plan:
                #print("Cambio fecha")
            today=date.today()
            if not(form["end_date"].value()==""):
                if form.cleaned_data["initial_date"]<= today and form.cleaned_data["end_date"]>=today:
                    # Si es por plan tengo que crear los descuentos.
                    if self.promotion.initial_date!=form["initial_date"].value() and form.cleaned_data["initial_date"]==today and self.has_plan:
                        #print("Creo descuentos")
                        # Traigo los servicios de los planes.
                        services=Service.objects.none()
                        for p in self.promotion.plan.all():
                            services=services | Service.objects.filter(plan=p)
                        today_is=date.today()
                        for index in range(self.promotion.number_periods):
                            #print(j)
                            motivo=MainReason.objects.get(id=self.promotion.info_periods[str(index+1)])
                            if index==0:
                                if motivo.promo.info_periods["permanent"]:
                                    for s in services:
                                        s.promotions.add(self.promotion)
                                        s.save()
                                        Credit.objects.create(
                                                reason = motivo,
                                                service = s,
                                                quantity = 1,
                                                active = True,
                                                permanent= True
                                        )
                                else:
                                    for s in services:
                                        s.promotions.add(self.promotion)
                                        s.save()
                                        Credit.objects.create(
                                                reason = motivo,
                                                service = s,
                                                quantity = 1,
                                                active = True
                                        )
                            else:
                                today_is=next_month(today_is).replace(day=19)
                                for s in services:
                                    s.promotions.add(self.promotion)
                                    s.save()
                                    Credit.objects.create(
                                            reason = motivo,
                                            service = s,
                                            quantity = 1,
                                            active = False,
                                            date_to_set_active = today_is
                                    )

                    if self.promotion.active==False:
                        context["object"].active=True
                        context["object"].save()
                else:
                    if self.promotion.active==True:
                        context["object"].active=False
                        context["object"].save()
                    # Creo los nuevos descuentos.
                    if self.promotion.initial_date!=form["initial_date"].value() and form.cleaned_data["initial_date"]>today and self.has_plan:
                        #print("Creo descuentos")
                        # Traigo los servicios de los planes.
                        services=Service.objects.none()
                        for p in self.promotion.plan.all():
                            services=services | Service.objects.filter(plan=p)
                        today_is=form["initial_date"].value()
                        for index in range(self.promotion.number_periods):
                            #print(j)
                            motivo=MainReason.objects.get(id=self.promotion.info_periods[str(index+1)])
                            if index==0:
                                if motivo.promo.info_periods["permanent"]:
                                    for s in services:
                                        s.promotions.add(self.promotion)
                                        s.save()
                                        Credit.objects.create(
                                                reason = motivo,
                                                service = s,
                                                quantity = 1,
                                                active = False,
                                                permanent= True
                                        )
                                else:
                                    for s in services:
                                        s.promotions.add(self.promotion)
                                        s.save()
                                        Credit.objects.create(
                                                reason = motivo,
                                                service = s,
                                                quantity = 1,
                                                active = False
                                        )
                            else:
                                today_is=next_month(today_is).replace(day=19)
                                for s in services:
                                    s.promotions.add(self.promotion)
                                    s.save()
                                    Credit.objects.create(
                                            reason = motivo,
                                            service = s,
                                            quantity = 1,
                                            active = False,
                                            date_to_set_active = today_is
                                    )
            else:
                if form.cleaned_data["initial_date"]<= date.today():
                    # Si es por plan tengo que crear los descuentos.
                    if self.promotion.initial_date!=form["initial_date"].value() and form.cleaned_data["initial_date"]==today and self.has_plan:
                        #print("Creo descuentos")
                        # Traigo los servicios de los planes.
                        services=Service.objects.none()
                        for p in self.promotion.plan.all():
                            services=services | Service.objects.filter(plan=p)
                        today_is=date.today()
                        for index in range(self.promotion.number_periods):
                            #print(j)
                            motivo=MainReason.objects.get(id=self.promotion.info_periods[str(index+1)])
                            if index==0:
                                if motivo.promo.info_periods["permanent"]:
                                    for s in services:
                                        s.promotions.add(self.promotion)
                                        s.save()
                                        Credit.objects.create(
                                                reason = motivo,
                                                service = s,
                                                quantity = 1,
                                                active = True,
                                                permanent= True
                                        )
                                else:
                                    for s in services:
                                        s.promotions.add(self.promotion)
                                        s.save()
                                        Credit.objects.create(
                                                reason = motivo,
                                                service = s,
                                                quantity = 1,
                                                active = True
                                        )
                            else:
                                today_is=next_month(today_is).replace(day=19)
                                for s in services:
                                    s.promotions.add(self.promotion)
                                    s.save()
                                    Credit.objects.create(
                                            reason = motivo,
                                            service = s,
                                            quantity = 1,
                                            active = False,
                                            date_to_set_active = today_is
                                    )
                    if self.promotion.active!=True:
                        context["object"].active=True
                        context["object"].save()
                else:
                    if self.promotion.active==True:
                        context["object"].active=False
                        context["object"].save()
                    if self.promotion.initial_date!=form["initial_date"].value() and form.cleaned_data["initial_date"]>today and self.has_plan:
                        #print("Creo descuentos")
                        # Traigo los servicios de los planes.
                        services=Service.objects.none()
                        for p in self.promotion.plan.all():
                            services=services | Service.objects.filter(plan=p)
                        today_is=form["initial_date"].value()
                        for index in range(self.promotion.number_periods):
                            #print(j)
                            motivo=MainReason.objects.get(id=self.promotion.info_periods[str(index+1)])
                            if index==0:
                                if motivo.promo.info_periods["permanent"]:
                                    for s in services:
                                        s.promotions.add(self.promotion)
                                        s.save()
                                        Credit.objects.create(
                                                reason = motivo,
                                                service = s,
                                                quantity = 1,
                                                active = False,
                                                permanent= True
                                        )
                                else:
                                    for s in services:
                                        s.promotions.add(self.promotion)
                                        s.save()
                                        Credit.objects.create(
                                                reason = motivo,
                                                service = s,
                                                quantity = 1,
                                                active = False
                                        )
                            else:
                                today_is=next_month(today_is).replace(day=19)
                                for s in services:
                                    s.promotions.add(self.promotion)
                                    s.save()
                                    Credit.objects.create(
                                            reason = motivo,
                                            service = s,
                                            quantity = 1,
                                            active = False,
                                            date_to_set_active = today_is
                                    )
        except Exception as e:
            print(str(e))
            pass
        return super(PromotionsUpdate, self).form_valid(form)

promotions_update = PromotionsUpdate.as_view()

class ServicePromotionsList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = CartolaMovement
    paginate_by = 8
    template_name = "customers/service_promotions_list.html"
    permission_required = "customers.list_promotion"

    def dispatch(self, *args, **kwargs):
        self.service = get_object_or_404(Service, pk=self.kwargs["slug"])
        self.customer = self.service.customer
        return super(ServicePromotionsList, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(ServicePromotionsList, self).get_context_data(*args, **kwargs)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if self.service.operator.company.id==op.company:
                        can_show_detail=True
                else:
                    if self.service.operator.id==op.operator:
                        can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        ctx["service"] = self.service
        ctx["customer"] = self.customer
        return ctx

service_promotions_list = ServicePromotionsList.as_view()


@login_required
@permission_required("customers.list_promotion")
def service_promotions_ajax(request, **kwargs):

    if request.is_ajax():
        # ordering
        columns = {
            "0": "reason__promo__name",
            "1": "reason__promo__description",
            "2": "reason__promo__plan",
            "3": "reason__promo__initial_date",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        operator = request.GET.get("operator", None)
        active = request.GET.get("active", None)
        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_promotions = Credit.objects.filter(status_field=False, cleared_at__isnull=True, reason__kind=3,service__pk=kwargs["slug"]).order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_promotions = Credit.objects.filter(status_field=False, cleared_at__isnull=True, reason__kind=3,service__pk=kwargs["slug"])

        if operator and operator != "null":
            all_promotions = all_promotions.filter(reason__promo__plan__operator=operator)

        if active == "True":
            all_promotions = all_promotions.filter(reason__promo__active=True)
        elif active == "False":
            all_promotions = all_promotions.filter(reason__promo__active=False)

        all_promotions=all_promotions.order_by('reason__promo__id').distinct('reason__promo__id')

        all_count = all_promotions.count()
        
        if search:
            #cambiar esto cuando se sepan los campos del modelo
            filtered_promotions = all_promotions.filter(
                Q(reason__promo__description__unaccent__icontains=search)
                | Q(reason__promo__name__unaccent__icontains=search)
            )
        else:
            filtered_promotions = all_promotions

        filtered_promotions=filtered_promotions.order_by('reason__promo__id').distinct('reason__promo__id')
        #filtered_promotions = filtered_promotions.distinct()

        filtered_count = filtered_promotions.count()

        if length == -1:
            sliced_promotions = filtered_promotions
        else:
            sliced_promotions = filtered_promotions[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_service_promotion, sliced_promotions)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_service_promotion(promos):
    promotion=promos.reason.promo
    try:
        plans=""
        for i in promotion.plan.all():
            plans=plans+" "+str(i.name)
        dictionary = {
            "date": promotion.created_at.strftime("%d-%m-%Y"),
            "name": promotion.name,
            "description": promotion.description if promotion.description else "",
            "plan": plans,
            "periods": promotion.number_periods,
            "pk": promotion.pk, 
            "initial_date": promotion.initial_date.strftime("%d-%m-%Y")
        }
    except:
        plans=""
        for i in promotion.plan.all():
            plans=plans+" "+str(i.name)
        dictionary = {
            "date": promotion.created_at.strftime("%d-%m-%Y"),
            "name": promotion.name,
            "description": promotion.description if promotion.description else "",
            "plan": plans,
            "periods": promotion.number_periods,
            "pk": promotion.pk, 
            "initial_date": ""
        }
    
    return dictionary

@login_required
@permission_required("customers.list_promotion")
def service_promotions_hist_ajax(request, **kwargs):

    if request.is_ajax():
        # ordering
        columns = {
            "0": "reason__promo__name",
            "1": "reason__promo__description",
            "2": "reason__promo__plan",
            "3": "reason__promo__initial_date",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        operator = request.GET.get("operator", None)
        active = request.GET.get("active", None)
        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_promotions = Credit.objects.filter(status_field=False, cleared_at__isnull=False, reason__kind=3, service__pk=kwargs["slug"]).order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_promotions = Credit.objects.filter(status_field=False, cleared_at__isnull=False, reason__kind=3,service__pk=kwargs["slug"])

        if operator and operator != "null":
            all_promotions = all_promotions.filter(reason__promo__plan__operator=operator)

        all_promotions=all_promotions.order_by('reason__promo__id').distinct('reason__promo__id')

        all_count = all_promotions.count()
        
        if search:
            #cambiar esto cuando se sepan los campos del modelo
            filtered_promotions = all_promotions.filter(
                Q(reason__promo__description__unaccent__icontains=search)
                | Q(reason__promo__name__unaccent__icontains=search)
            )
        else:
            filtered_promotions = all_promotions

        filtered_promotions=filtered_promotions.order_by('reason__promo__id').distinct('reason__promo__id')
        #filtered_promotions = filtered_promotions.distinct()

        filtered_count = filtered_promotions.count()

        if length == -1:
            sliced_promotions = filtered_promotions
        else:
            sliced_promotions = filtered_promotions[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_service_promotion_hist, sliced_promotions)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_service_promotion_hist(promos):
    promotion=promos.reason.promo
    try:
        plans=""
        for i in promotion.plan.all():
            plans=plans+" "+str(i.name)
        dictionary = {
            "date_hist": promotion.created_at.strftime("%d-%m-%Y"),
            "name_hist": promotion.name,
            "description_hist": promotion.description if promotion.description else "",
            "plan_hist": plans,
            "periods_hist": promotion.number_periods,
            "pk_hist": promotion.pk, 
            "initial_date_hist": '{}-{}-{}'.format(promotion.initial_date.day,promotion.initial_date.month, promotion.initial_date.year )
        }
    except:
        plans=""
        for i in promotion.plan.all():
            plans=plans+" "+str(i.name)
        dictionary = {
            "date_hist": promotion.created_at.strftime("%d-%m-%Y"),
            "name_hist": promotion.name,
            "description_hist": promotion.description if promotion.description else "",
            "plan_hist": plans,
            "periods_hist": promotion.number_periods,
            "pk_hist": promotion.pk, 
            "initial_date_hist": ""
        }
    return dictionary

class ServiceCustom(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Service
    paginate_by = 8
    template_name = "customers/services_custom_list.html"
    permission_required = "customers.list_service"

    def dispatch(self, *args, **kwargs):
        return super(ServiceCustom, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(ServiceCustom, self).get_context_data(*args, **kwargs)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if self.service.operator.company.id==op.company:
                        can_show_detail=True
                else:
                    if self.service.operator.id==op.operator:
                        can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        return ctx

services_custom_list = ServiceCustom.as_view()

@login_required
@permission_required("customers.list_service")
def services_custom_ajax(request, **kwargs):

    if request.is_ajax():
        # ordering
        columns = {
            "0": "number",
            "1": "customer",
            "2": "address",
            "3": "plan",
            "4": "status"
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_services = Service.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_services = Service.objects.all()


        all_count = all_services.count()
        
        if search:
            filtered_services = all_services.filter(
                Q(best_address__icontains=search)
                | Q(number__icontains=search)
            )
        else:
            filtered_services = all_services

        filtered_services = filtered_services.distinct()

        filtered_count = filtered_services.count()

        if length == -1:
            sliced_services = filtered_services
        else:
            sliced_services = filtered_services[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_custom_service, sliced_services)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_custom_service(service):

    try:
        dictionary = {
            "number": service.number,
            "customer": service.customer.name,
            "plan": service.plan.name,
            "status": service.get_status_display(),
            "address": service.best_address,
            "pk": service.pk
        }
    except:
         dictionary = {
            "number": service.number,
            "customer": service.customer.name,
            "plan": service.plan.name,
            "status": service.get_status_display(),
            "address": service.best_address,
            "pk": service.pk
        }
    return dictionary


class PlansList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Plan
    paginate_by = 8
    template_name = "customers/plans_list.html"
    permission_required = "customers.list_plan"

    def get_context_data(self, *args, **kwargs):
        ctx = super(PlansList, self).get_context_data(*args, **kwargs)
        ctx["operators"] = Operator.objects.all()
        ctx["categories"] = Plan.CATEGORY_CHOICES
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass
        return ctx

plans_list = PlansList.as_view()

@login_required
@permission_required("customers.list_plan")
def plans_list_ajax(request, **kwargs):

    if request.is_ajax():
        # ordering
        columns = {
            "0": "id",
            "1": "name",
            "2": "description_facturacion",
            "3": "price",
            "4": "uf",
            "5": "category",
            "6": "active_services",
            "7": "total_services",
            "8": "operator"
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        operator = request.GET.get("operator", None)
        category = request.GET.get("category", None)
        uf = request.GET.get("uf", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_plans = Plan.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_plans = Plan.objects.all()

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
            except:
                pass

        if op.operator!=0:
            all_plans = all_plans.filter(operator=op.operator)
        else:
            all_plans = all_plans.filter(operator__company__id=op.company)

        if operator and operator != "null":
            all_plans = all_plans.filter(operator=operator)

        if category and category != "null":
            all_plans = all_plans.filter(category=category)

        if uf == "True":
            all_plans = all_plans.filter(uf=True)
        elif uf == "False":
            all_plans = all_plans.filter(uf=True)

        all_count = all_plans.count()
        
        if search:
            #cambiar esto cuando se sepan los campos del modelo
            filtered_plans = all_plans.filter(
                Q(pk__icontains=search)
                | Q(name__unaccent__icontains=search)
                | Q(description_facturacion__unaccent__icontains=search)
            )
        else:
            filtered_plans = all_plans

        filtered_plans = filtered_plans.distinct()

        filtered_count = filtered_plans.count()

        if length == -1:
            sliced_plans = filtered_plans
        else:
            sliced_plans = filtered_plans[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_plan, sliced_plans)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_plan(plan):
    if plan.currency:
        symbol=plan.currency.symbol
    else:
        symbol="$"

    try:
        dictionary = {
            "id": plan.pk, 
            "name": plan.name,
            "description_facturacion": plan.description_facturacion,
            "price": str(plan.price),
            "uf": plan.uf,
            "category": plan.get_category_display(),
            "total_services": Service.objects.all().filter(plan__pk=plan.pk).count(),
            "active_services": Service.objects.all().filter(plan__pk=plan.pk, status=1).count(),
            "operator": plan.operator.name,
            "symbol":symbol,
            "currency":plan.currency.name if plan.currency else ""
        }
    except:
        dictionary = {
            "id": plan.pk, 
            "name": plan.name,
            "description_facturacion": plan.description_facturacion,
            "price": str(plan.price),
            "uf": plan.uf,
            "category": plan.get_category_display(),
            "total_services": Service.objects.all().filter(plan__pk=plan.pk).count(),
            "active_services": Service.objects.all().filter(plan__pk=plan.pk, status=1).count(),
            "operator": plan.operator.namesymbol,
            "symbol":symbol,
            "currency":plan.currency.name if plan.currency else ""
        }
    return dictionary

@login_required
@permission_required("customers.view_service")
def service_configuration(request, slug):
    service = get_object_or_404(Service, pk=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    checked_3month = 'No'
    checked_post_activacion_email = 'No'
    if service.flag:
        if "bloqueado_onboarding_email" in service.flag.keys():
            checked_3month = service.flag["bloqueado_onboarding_email"]
        if "bloqueado_post_activacion_email" in service.flag.keys():
            checked_post_activacion_email = service.flag["bloqueado_post_activacion_email"]

    context = {"object": service, 
               "can_show_detail":can_show_detail,
               "service": service, 
               "customer": service.customer, 
               "checked_3month": checked_3month,
               "checked_post_activacion_email": checked_post_activacion_email,}
    return render(request,  "customers/service_configuration.html", context)


@login_required
@permission_required("customers.view_service")
def service_configuration_ajax(request, slug):
    config = Service.objects.get(pk=slug)
    checked_3month = request.GET.get("month3", None)
    checked_post_activacion_email = request.GET.get("postActivacion", None)
    if config.flag:
        if checked_3month == 'true':
            config.flag["bloqueado_onboarding_email"] = "Si"
            config.save()
        else:
            config.flag["bloqueado_onboarding_email"] = "No"
            config.save()
    else:
        if checked_3month == 'true':
            json_aux = {"bloqueado_onboarding_email": "Si"}
        else:
            json_aux = {"bloqueado_onboarding_email": "No"}
        config.flag = json_aux
        config.save()
    
    if config.flag:
        if checked_post_activacion_email == 'true':
            config.flag["bloqueado_post_activacion_email"] = "Si"
            config.save()
        else:
            config.flag["bloqueado_post_activacion_email"] = "No"
            config.save()
    else:
        if checked_post_activacion_email == 'true':
            json_aux = {"bloqueado_post_activacion_email": "Si"}
        else:
            json_aux = {"bloqueado_post_activacion_email": "No"}
        config.flag = json_aux
        config.save()

    data = json.dumps({"data": config.flag})
    return HttpResponse(data, content_type="application/json")

@login_required
@permission_required("customers.view_customer")
def customer_configuration(request, pk):
    customer = get_object_or_404(Customer, pk=pk)
    servicios = Service.objects.filter(customer=customer)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.company==customer.company.id:
                can_show_detail=True
        except:
            pass
    servicios_list = []
    for s in servicios:
        servicios_list.append((s.pk, s.number))
    service_preselect = None
    if servicios.count() == 1:
        service_preselect = servicios[0]
    checked_nuevo_cliente_email = 'No'
    if customer.flag:
        if "bloqueado_nuevo_cliente_email" in customer.flag.keys():
            checked_nuevo_cliente_email = customer.flag["bloqueado_nuevo_cliente_email"]
    context = {"can_show_detail":can_show_detail,
                "customer": customer, 
               "service_preselect": service_preselect,
               "checked_nuevo_cliente_email": checked_nuevo_cliente_email,
               "servicios": servicios_list, }
    return render(request, "customers/customer_configuration.html", context)

@login_required
@permission_required("customers.view_customer")
def customer_widget(request, pk):
    customer = get_object_or_404(Customer, pk=pk)
    servicios = Service.objects.filter(customer=customer)
    service_preselect = None
    if servicios.count() == 1:
        service_preselect = servicios[0]

    context = {"customer": customer, "service_preselect": service_preselect}
    return render(request, "customers/customer_widget.html", context)

@login_required
@permission_required("customers.view_service")
def customer_configuration_ajax(request, pk):
    config = Customer.objects.get(pk=pk)
    checked_nuevo_cliente_email = request.GET.get("nuevoCliente", None)
    cambio_aceptacion = request.GET.get("cambioAceptacion", None)

    if config.flag:
        if checked_nuevo_cliente_email == 'true':
            config.flag["bloqueado_nuevo_cliente_email"] = "Si"
            config.save()
        else:
            config.flag["bloqueado_nuevo_cliente_email"] = "No"
            config.save()
    else:
        if checked_nuevo_cliente_email == 'true':
            json_aux = {"bloqueado_nuevo_cliente_email": "Si"}
        else:
            json_aux = {"bloqueado_nuevo_cliente_email": "No"}
        config.flag = json_aux
        config.save()

    if cambio_aceptacion:
        if config.flag:
            if cambio_aceptacion == '1':
                config.flag["aceptacion_de_contrato"] = "Aceptado"
            elif cambio_aceptacion == '2':
                config.flag["aceptacion_de_contrato"] = "No Aceptado"
            elif cambio_aceptacion == '3':
                config.flag["aceptacion_de_contrato"] = "No Definido"
        else:
            if cambio_aceptacion == '1':
                config.flag = {"aceptacion_de_contrato": "Aceptado"}
            elif cambio_aceptacion == '2':
                config.flag = {"aceptacion_de_contrato": "No Aceptado"}
            elif cambio_aceptacion == '3':
                config.flag = {"aceptacion_de_contrato": "No Definido"}
        config.save()
    data = json.dumps({"data": config.flag})
    return HttpResponse(data, content_type="application/json")

class SavePDFView(FormView):
    class_number = ""
    description = ""
    template_name_pdf = "customers/service_generic_html_to_pdf.html"
    template_name = "customers/service_savepdf_form.html"

    def dispatch(self, *args, **kwargs):
        self.service = get_object_or_404(Service, pk=self.kwargs["slug"])
        return super(SavePDFView, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(SavePDFView, self).get_context_data(*args, **kwargs)
        context.update(
            {
                "date": timezone.now(),
                "object": self.service,
                "service": self.service,
                "customer": self.service.customer,
                "kind": self.service.customer.kind,
                "description": self.description,
                "class_number": self.class_number,
            }
        )
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        context.update(form.cleaned_data)
        template = get_template(self.template_name_pdf)

        pdf = render_pdf_from_template(template, None, None, context=context)

        bb = io.BytesIO(pdf)
        ff = File(self.filename)
        ff.file = bb
        doc = Document.objects.create(
            content_object=self.service,
            description=self.description,
            agent=self.request.user,
        )
        doc.file.save(self.filename, ff)
        doc.save()

        messages.add_message(self.request, messages.SUCCESS, self.success_message)
        return redirect(reverse("service-documents", args=[self.service.slug]))


class TechnicalVisitFormPDF(SavePDFView):
    class_number = 1
    filename = "service_technical_visit.pdf"
    description = "Visita Técnica"
    success_message = "Se ha generado una visita técnica."
    form_class = TechnicalVisitForm


technical_visit_form = TechnicalVisitFormPDF.as_view()


class ServiceAnnexFormPDF(SavePDFView):
    class_number = 2
    filename = "service_annex.pdf"
    description = "Anexo de Contrato"
    success_message = "Se ha generado un anexo de contrato."
    form_class = ServiceAnnexForm


service_annex_form = ServiceAnnexFormPDF.as_view()


class ServiceTermFormPDF(SavePDFView):
    class_number = 3
    filename = "service_term.pdf"
    description = "Término de Contrato"
    success_message = "Se ha generado un término de contrato."
    form_class = ServiceTermForm


service_term_form = ServiceTermFormPDF.as_view()


class EquipmentRemovalFormPDF(SavePDFView):
    class_number = 4
    filename = "retiro_de_equipo.pdf"
    description = "Retiro de Equipo"
    success_message = "Se ha generado un retiro de equipo."
    form_class = EquipmentRemovalForm


equipment_removal = EquipmentRemovalFormPDF.as_view()


class PlanServiceFormPDF(SavePDFView):
    class_number = 5
    filename = "plan_service.pdf"
    description = "Formulario de Solicitud "
    success_message = (
        "Se ha generado generado un nuevo formulario de solicitud de servicio"
    )
    form_class = PlanServiceForm


plan_service = PlanServiceFormPDF.as_view()


@login_required
def generate_verifier(request):
    return render(request, "customers/generate_verifier.html")


@login_required
@csrf_exempt
def verifier_digit_ajax(request):
    if request.is_ajax() and request.method == "POST":
        try:
            yeison = json.loads(request.body.decode("utf-8"))
            rut = int(yeison["rut"])
            ff = CLRutField()
            verifier_digit = ff._algorithm(str(rut))
        except:
            verifier_digit = None

        data = json.dumps({"verifier_digit": verifier_digit})
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


def prepare_payment(payment):
    if payment.bank_account:
        if payment.bank_account.currency:
            symbol=payment.bank_account.currency.symbol
        else:
            symbol="$"
    else:
        symbol="$"
    try:
        dictionary = {
            "DT_RowId": payment.pk,
            "created_at": payment.created_at.strftime("%Y-%m-%d %H:%M"),
            "pk": payment.pk,
            "customer_name": payment.customer.name,
            "customer_pk": payment.customer.pk,
            "paid_on": payment.paid_on.strftime("%Y-%m-%d %H:%M"),
            "formatted_amount": price_tag(payment.amount),
            "manual": "Sí" if payment.manual else "No",
            "kind": payment.get_kind_display().capitalize(),
            "agent": payment.agent.username if payment.agent else "",
            "operator": payment.operator.name,
            "cleared": payment.cleared,
            "proof": True if payment.proof else False,
            "proof2": True if payment.proof2 else False,
            "proof3": True if payment.proof3 else False,
            "proof4": True if payment.proof4 else False,
            "customer_operator": str(payment.customer_operator),
            "kind_load": payment.get_kind_load_display().capitalize(),
            "bank_account_operator": payment.bank_account.operator.name
            if payment.bank_account
            else "",
            "symbol": symbol,
        }
    except:
        dictionary = {
            "DT_RowId": payment.pk,
            "created_at": payment.created_at.strftime("%Y-%m-%d %H:%M"),
            "pk": payment.pk,
            "customer_name": "",
            "customer_pk": "",
            "paid_on": payment.paid_on.strftime("%Y-%m-%d %H:%M"),
            "formatted_amount": price_tag(payment.amount),
            "manual": yesno(payment.manual, "Sí,No"),
            "kind": payment.get_kind_display().capitalize(),
            "agent": payment.agent.username if payment.agent else "",
            "operator": payment.operator.name,
            "cleared": payment.cleared,
            "proof": True if payment.proof else False,
            "proof2": True if payment.proof2 else False,
            "proof3": True if payment.proof3 else False,
            "proof4": True if payment.proof4 else False,
            "customer_operator": "",
            "kind_load": payment.get_kind_load_display().capitalize(),
            "bank_account_operator": payment.bank_account.operator.name
            if payment.bank_account
            else "",
            "symbol":symbol,
        }
    return dictionary


@login_required
@permission_required("customers.list_payment")
def payment_ajax(request, to_excel=False):
    if request.is_ajax() or to_excel:
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "complete_name",
            "3": "operator",
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        kind_kind = request.GET.get("kind_kind", None)
        kind = request.GET.get("kind", None)
        agent = request.GET.get("agent", None)
        operator = request.GET.get("operator", None)
        operator_cliente = request.GET.get("operator_cliente", None)
        bankaccount = request.GET.get("bankaccount", None)
        cleared = request.GET.get("cleared", None)
        service = request.GET.get("service", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_payments = Payment.objects.annotate(
                complete_name = Concat(
                    F('customer__first_name'),
                    Value(' '),
                    F('customer__last_name'),
                )
            ).order_by(orderings[order_dir] + columns[order_column])
        else:
            all_payments = Payment.objects.all().annotate(
                complete_name = Concat(
                    F('customer__first_name'),
                    Value(' '),
                    F('customer__last_name'),
                )
            )

        if operator_cliente and operator_cliente != "null":
            clientes = (
                Service.objects.filter(operator=operator_cliente)
                .order_by("customer_id")
                .values_list("customer_id")
                .distinct("customer_id")
            )
            all_payments = all_payments.filter(customer_id__in=clientes)

        if kind_kind:
            all_payments = all_payments.filter(kind=kind_kind)

        if kind == "1":
            all_payments = all_payments.filter(manual=True)
        elif kind == "2":
            all_payments = all_payments.filter(manual=False)

        if agent and agent != "null":
            payment_ids = Payment.history.filter(
                history_user_id=agent, history_type="+"
            ).values_list("id", flat=True)
            all_payments = all_payments.filter(id__in=payment_ids)

        if operator and operator != "null":
            all_payments = all_payments.filter(operator=operator)

        if bankaccount and bankaccount != "null":
            all_payments = all_payments.filter(bank_account=bankaccount)

        if cleared:
            all_payments = all_payments.filter(cleared=cleared)

        if service == "True":
            all_payments = all_payments.exclude(service=None)
        if service == "False":
            all_payments = all_payments.filter(service=None)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                all_payments = all_payments.filter(
                    created_at__gte=datetime(year, month, day))
            except pytz.exceptions.AmbiguousTimeError:
                all_payments = all_payments.filter(
                    created_at__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True) 
                )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            try:
                all_payments = all_payments.filter(
                created_at__lte=datetime(year, month, day, 23, 59, 59))
            except pytz.exceptions.AmbiguousTimeError:
                all_payments = all_payments.filter(
                    created_at__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True) 
                )

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_payments = all_payments.filter(operator=op.operator)
                else:
                    all_payments = all_payments.filter(operator__company__id=op.company)
            except:
                pass

        

        all_count = all_payments.count()

        if search:
            filtered_payments = all_payments.filter(
                Q(pk=int(search) if search.isdigit() else None)
                #| Q(customer__first_name__unaccent__icontains=search)
                #| Q(customer__last_name__unaccent__icontains=search)
                | Q(complete_name__unaccent__icontains=search)
                | Q(amount=int(search) if search.isdigit() else None)
            )
        else:
            filtered_payments = all_payments

        filtered_payments = filtered_payments.distinct()

        filtered_count = filtered_payments.count()

        if length == -1:
            sliced_payments = filtered_payments
        else:
            sliced_payments = filtered_payments[start : start + length]

        if to_excel:
            return list(map(prepare_payment, filtered_payments))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_payment, sliced_payments)),
                }
            )

            return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.list_payment")
def payments_excel(request):
    data = payment_ajax(request, True)
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha de creación",
        "Id pago",
        "Cliente",
        "Operador Cliente",
        "Fecha de pago",
        "Monto",
        "Manual",
        "Tipo",
        "Agente",
        "O. Pago",
        "Conciliado",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in data:
        try:
            formatted_creation_date = formats.date_format(
                item_data["created_at"], "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        try:
            formatted_paid_on_date = formats.date_format(
                item_data["paid_on"], "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_paid_on_date = item_data["paid_on"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, item_data["pk"])
        worksheet.write(row, 2, item_data["customer_name"])
        worksheet.write(row, 3, item_data["customer_operator"])
        worksheet.write(row, 4, formatted_paid_on_date)
        worksheet.write(row, 5, item_data["formatted_amount"])
        worksheet.write(row, 6, item_data["manual"])
        worksheet.write(row, 7, item_data["kind"])
        worksheet.write(row, 8, item_data["agent"])
        worksheet.write(row, 9, item_data["operator"])
        worksheet.write(row, 10, "Si" if item_data["cleared"] else "No")
        total_amount = total_amount + int(
            item_data["formatted_amount"].replace(",", "")
        )
        row += 1

    # Guarda el total con la suma de todos los montos
    worksheet.write(row, 4, "TOTAL")
    worksheet.write(row, 5, total_amount)
    row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="pagos_matrix.xlsx"'
    return response


@login_required
@permission_required("customers.list_payment")
def payments_other_account_report_ajax(request, to_excel=False):
    if request.is_ajax() or to_excel:
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "customer__name",
            "3": "customer__name",  # Deberia ser operador pero no es un campo
            "4": "paid_on",
            "5": "amount",
            "6": "manual",
            "7": "kind",
            "8": "agent",
            "9": "cleared",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        kind_kind = request.GET.get("kind_kind", None)
        kind = request.GET.get("kind", None)
        agent = request.GET.get("agent", None)
        operator = request.GET.get("operator", None)
        operator_cliente = request.GET.get("operator_cliente", None)
        bankaccount = request.GET.get("bankaccount", None)
        cleared = request.GET.get("cleared", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_payments = Payment.objects.order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_payments = Payment.objects.all()

        all_payments = all_payments.filter(customer__isnull=False)

        if operator_cliente:
            clientes = (
                Service.objects.filter(operator=operator_cliente)
                .order_by("customer_id")
                .values_list("customer_id")
                .distinct("customer_id")
            )
            all_payments = all_payments.filter(customer_id__in=clientes)

        if kind_kind:
            all_payments = all_payments.filter(kind=kind_kind)

        if kind == "1":
            all_payments = all_payments.filter(manual=True)
        elif kind == "2":
            all_payments = all_payments.filter(manual=False)

        if agent:
            payment_ids = Payment.history.filter(
                history_user_id=agent, history_type="+"
            ).values_list("id", flat=True)
            all_payments = all_payments.filter(id__in=payment_ids)

        username = None
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_payments = all_payments.filter(operator=op.operator)
                else:
                    all_payments = all_payments.filter(operator__company__id=op.company)
            except:
                pass

        if operator and operator != "null":
            all_payments = all_payments.filter(operator=operator)

        if bankaccount and bankaccount != "null":
            all_payments = all_payments.filter(bank_account=bankaccount)

        if cleared:
            all_payments = all_payments.filter(cleared=cleared)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                all_payments = all_payments.filter(paid_on__gte=datetime(year, month, day))
            except pytz.exceptions.AmbiguousTimeError:
                all_payments = all_payments.filter(paid_on__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True))

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            month = int(endDateList[1])
            day = int(endDateList[2])
            try:
                all_payments = all_payments.filter(
                paid_on__lte=datetime(year, month, day, 23, 59, 59)
                )
            except pytz.exceptions.AmbiguousTimeError:
                all_payments = all_payments.filter(
                paid_on__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True)
                )

        all_count = all_payments.count()

        if search:
            filtered_payments = all_payments.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(customer__first_name__unaccent__icontains=search)
                | Q(customer__last_name__unaccent__icontains=search)
                | Q(amount=int(search) if search.isdigit() else None)
            )
        else:
            filtered_payments = all_payments

        ### Se filtra por todos los casos.

        if operator_cliente and operator and operator_cliente == operator:
            # Quito los de operador y cuenta iguales y los usuarios que tengan varios operadores
            filtered_payments = filtered_payments.exclude(
                operator=F("bank_account__operator")
            )
            operators = Operator.objects.all().exclude(id=operator_cliente)
            for i in operators:
                clientes = (
                    Service.objects.filter(operator=i)
                    .order_by("customer_id")
                    .values_list("customer_id")
                    .distinct("customer_id")
                )
                filtered_payments = filtered_payments.exclude(customer_id__in=clientes)
        if operator_cliente and operator and operator_cliente != operator:
            # Quito los que su cliente tiene ambos operadores
            clientes = (
                Service.objects.filter(operator=operator)
                .order_by("customer_id")
                .values_list("customer_id")
                .distinct("customer_id")
            )
            clientes1 = (
                Service.objects.filter(
                    operator=operator_cliente, customer_id__in=clientes
                )
                .order_by("customer_id")
                .values_list("customer_id")
                .distinct("customer_id")
            )
            filtered_payments = filtered_payments.exclude(customer_id__in=clientes1)
        if operator_cliente and not operator:
            # Quito los que su cliente no tiene y los que estan bien
            operators = Operator.objects.all().exclude(id=operator_cliente)
            for i in operators:
                clientes = (
                    Service.objects.filter(operator=i)
                    .order_by("customer_id")
                    .values_list("customer_id")
                    .distinct("customer_id")
                )
                filtered_payments = filtered_payments.exclude(customer_id__in=clientes)
            # Se quitan los casos en que el todos los campos de operador coincidan o coincida el op cliente y op pago, sin cuenta.
            filtered_payments = filtered_payments.exclude(
                operator=operator_cliente, bank_account__operator=operator_cliente
            )
            filtered_payments = filtered_payments.exclude(
                operator=operator_cliente, bank_account__isnull=True
            )
        if not operator_cliente and operator:
            # Quito los que los clientes de esos pagos tengan servicios en ambos y los que estan bien
            op = (
                Operator.objects.all()
                .exclude(id=operator)
                .order_by("id")
                .values_list("id")
                .distinct("id")
            )
            clientes = (
                Service.objects.filter(operator=operator)
                .order_by("customer_id")
                .values_list("customer_id")
                .distinct("customer_id")
            )
            clientes2 = (
                Service.objects.filter(operator_id__in=op, customer_id__in=clientes)
                .order_by("customer_id")
                .values_list("customer_id")
                .distinct("customer_id")
            )
            # Se quitan los casos en que el todos los campos de operador coincidan o coincida el op cliente y op pago, sin cuenta.
            filtered_payments = filtered_payments.exclude(
                customer_id__in=clientes, bank_account__operator=operator
            )
            filtered_payments = filtered_payments.exclude(
                customer_id__in=clientes, bank_account__isnull=True
            )
            filtered_payments = filtered_payments.exclude(customer_id__in=clientes2)
        if not operator_cliente and not operator:
            op = Operator.objects.all()
            for i in op:
                # Se quitan los casos en que el todos los campos de operador coincidan o coincida el op cliente y op pago, sin cuenta.
                clientes = (
                    Service.objects.filter(operator=i)
                    .order_by("customer_id")
                    .values_list("customer_id")
                    .distinct("customer_id")
                )
                filtered_payments = filtered_payments.exclude(
                    customer_id__in=clientes, operator=i, bank_account__operator=i
                )
                filtered_payments = filtered_payments.exclude(
                    customer_id__in=clientes, operator=i, bank_account__isnull=True
                )
                op1 = (
                    Operator.objects.all()
                    .exclude(id=i.id)
                    .order_by("id")
                    .values_list("id")
                    .distinct("id")
                )
                # Quito los que los clientes de esos pagos tengan servicios en ambos.
                for j in op1:
                    clientes2 = (
                        Service.objects.filter(operator=j, customer_id__in=clientes)
                        .order_by("customer_id")
                        .values_list("customer_id")
                        .distinct("customer_id")
                    )
                    filtered_payments = filtered_payments.exclude(
                        customer_id__in=clientes2, operator=i
                    )
                    filtered_payments = filtered_payments.exclude(
                        customer_id__in=clientes2, operator=j
                    )

        # Quito los que no tienen servicios.
        clientes = (
            Service.objects.all()
            .order_by("customer_id")
            .values_list("customer_id")
            .distinct("customer_id")
        )
        sin_servicio = Customer.objects.exclude(id__in=clientes)
        filtered_payments = filtered_payments.exclude(customer_id__in=sin_servicio)

        # Filtro para no tener repetidos.
        filtered_payments = filtered_payments.distinct()

        final_qs = filtered_payments
        filtered_count = final_qs.count()

        if length == -1:
            sliced_payments = final_qs
        else:
            sliced_payments = final_qs[start : start + length]

        if to_excel:
            return list(map(prepare_payment, final_qs))
        else:
            internat_data = list(map(prepare_payment, sliced_payments))
            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": internat_data,
                }
            )
            return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.list_payment")
def payment_ajax2(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "created_at",
            "1": "pk",
            "2": "paid_on",
            "3": "amount",
            "4": "manual",
            "5": "kind",
            "6": "kind_load",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        kind_kind = request.GET.get("kind_kind", None)
        kind = request.GET.get("kind", None)
        kind_load = request.GET.get("kind_load", None)
        agent = request.GET.get("agent", None)
        operator = request.GET.get("operator", None)
        operator_cliente = request.GET.get("operator_cliente", None)
        bankaccount = request.GET.get("bankaccount", None)
        cleared = request.GET.get("cleared", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_payments = Payment.objects.filter(customer__isnull=True).order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_payments = Payment.objects.filter(customer__isnull=True)

        if kind_kind:
            all_payments = all_payments.filter(kind=kind_kind)

        if kind_load == "2":
            all_payments = all_payments.filter(kind_load=2)
        elif kind_load == "3":
            all_payments = all_payments.filter(kind_load=3)

        if kind == "1":
            all_payments = all_payments.filter(manual=True)
        elif kind == "2":
            all_payments = all_payments.filter(manual=False)

        if agent:
            payment_ids = Payment.history.filter(
                history_user_id=agent, history_type="+"
            ).values_list("id", flat=True)
            all_payments = all_payments.filter(id__in=payment_ids)

        if operator:
            all_payments = all_payments.filter(operator=operator)

        if bankaccount:
            all_payments = all_payments.filter(bank_account=bankaccount)

        if cleared:
            all_payments = all_payments.filter(cleared=cleared)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                all_payments = all_payments.filter(
                    created_at__gte=datetime(year, month, day))
            except pytz.exceptions.AmbiguousTimeError:
                all_payments = all_payments.filter(
                    created_at__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True) 
                )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            try:
                all_payments = all_payments.filter(
                created_at__lte=datetime(year, month, day, 23, 59, 59))
            except pytz.exceptions.AmbiguousTimeError:
                all_payments = all_payments.filter(
                    created_at__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True) 
                )

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_payments = all_payments.filter(operator=op.operator)
                else:
                    all_payments = all_payments.filter(operator__company__id=op.company)
            except:
                pass

        all_count = all_payments.count()

        if search:
            filtered_payments = all_payments.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(customer__first_name__unaccent__icontains=search)
                | Q(customer__last_name__unaccent__icontains=search)
                | Q(amount=int(search) if search.isdigit() else None)
            )
        else:
            filtered_payments = all_payments

        filtered_payments = filtered_payments.distinct()

        filtered_count = filtered_payments.count()

        if length == -1:
            sliced_payments = filtered_payments
        else:
            sliced_payments = filtered_payments[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_payment, sliced_payments)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.list_payment")
def payments_other_account_report_excel(request):
    data = payments_other_account_report_ajax(request, True)
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "Fecha de creación",
        "Id pago",
        "Cliente",
        "Fecha de pago",
        "Monto",
        "Manual",
        "Tipo",
        "Agente",
        "Conciliado",
        "Operador de pago",
        "Operador de cuenta",
        "Operador de cliente",
    )
    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    for item_data in data:
        try:
            formatted_creation_date = formats.date_format(
                item_data["created_at"], "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        try:
            formatted_paid_on_date = formats.date_format(
                item_data["paid_on"], "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_paid_on_date = item_data["paid_on"]

        worksheet.write(row, 0, formatted_creation_date)
        worksheet.write(row, 1, item_data["pk"])
        worksheet.write(row, 2, item_data["customer_name"])
        worksheet.write(row, 3, formatted_paid_on_date)
        worksheet.write(row, 4, item_data["formatted_amount"])
        worksheet.write(row, 5, item_data["manual"])
        worksheet.write(row, 6, item_data["kind"])
        worksheet.write(row, 7, item_data["agent"])
        worksheet.write(row, 8, "Si" if item_data["cleared"] else "No")
        worksheet.write(row, 9, item_data["operator"])
        worksheet.write(row, 10, item_data["bank_account_operator"])
        worksheet.write(row, 11, item_data["customer_operator"])

        row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="pagos_erroneos.xlsx"'
    return response


def prepare_invoice(invoice):
    services=""
    comuna=""
    commune=[]
    for serv in invoice.service.all():
        services=services+str(serv.number)+" "
        if not (serv.node.commune in commune):
            commune.append(serv.node.commune)
            comuna=comuna+str(serv.node.commune)+" "
        
    if invoice.currency:
        symbol=invoice.currency.symbol
    else:
        symbol="$"

    try:
        dictionary = {
            "DT_RowId": invoice.pk,
            "pk": invoice.pk,
            "created_at": invoice.created_at.strftime("%d-%m-%Y %H:%M")
            if invoice.created_at
            else None,
            "kind": invoice.get_kind_display().capitalize(),
            "customer_name": invoice.customer.name,
            "customer_pk": invoice.customer.pk,
            "total": price_tag(invoice.total),
            "comment": invoice.comment,
            "paid": yesno(invoice.paid_on, "Sí,No"),
            "operator": invoice.operator.name,
            "customer_operator": str(invoice.customer_operator),
            "kind_load": invoice.get_kind_load_display().capitalize(),
            "services":services,
            "comuna":comuna,
            "direccion":str((invoice.customer.composite_address)[0:70]),
            "symbol":symbol,
            "currency":invoice.currency.name if invoice.currency else ""
        }
    except:
        dictionary = {
            "DT_RowId": invoice.pk,
            "pk": invoice.pk,
            "created_at": invoice.created_at.strftime("%d-%m-%Y %H:%M")
            if invoice.created_at
            else None,
            "kind": invoice.get_kind_display().capitalize(),
            "customer_name": "",
            "customer_pk": "",
            "total": "$ {}".format(intcomma(invoice.total)),
            "comment": invoice.comment,
            "paid": yesno(invoice.paid_on, "Sí,No"),
            "operator": invoice.operator.name,
            "customer_operator": "",
            "kind_load": invoice.get_kind_load_display().capitalize(),
            "services":services,
            "comuna":comuna,
            "direccion":"",
            "symbol":symbol,
            "currency":invoice.currency.name if invoice.currency else ""
        }
    return dictionary


@login_required
@permission_required("customers.list_payment")
def invoice_ajax(request, to_excel=False):
    if request.is_ajax() or to_excel:
        # ordering
        columns = {
            "0": "pk",
            "1": "created_at",
            "2": "due_date",
            "3": "paid_on",
            "4": "kind",
            "5": "customer__name",
            "6": "comment",
            "7": "total",
            "8": "paid_on",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        kind = request.GET.get("kind", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)
        service = request.GET.get("service", None)
        paid = request.GET.get("paid_on", None)
        operator = request.GET.get("operator", None)

        # search    
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_invoices = Invoice.objects.annotate(
                complete_name = Concat(
                    F('customer__first_name'),
                    Value(' '),
                    F('customer__last_name')
                )
            ).order_by(orderings[order_dir] + columns[order_column])
        else:
            all_invoices = Invoice.objects.all().annotate(
                complete_name = Concat(
                    F('customer__first_name'),
                    Value(' '),
                    F('customer__last_name')
                )
            )

        if kind:
            all_invoices = all_invoices.filter(kind=kind)

        if service == "True":
            all_invoices = all_invoices.exclude(service=None)
        if service == "False":
            all_invoices = all_invoices.filter(service=None)
        
        if paid == "True":
            all_invoices = all_invoices.filter(paid_on__isnull=False)
        if paid == "False":
            all_invoices = all_invoices.filter(paid_on__isnull=True)

        if operator and operator != "null":
            all_invoices = all_invoices.filter(operator=operator)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                all_invoices = all_invoices.filter(
                created_at__gte=datetime(year, month, day))
            except pytz.exceptions.AmbiguousTimeError:
                all_invoices = all_invoices.filter(
                    created_at__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True) 
                )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])
            try:
                all_invoices = all_invoices.filter(
                created_at__lte=datetime(year, month, day, 23, 59, 59))
            except pytz.exceptions.AmbiguousTimeError:
                all_invoices = all_invoices.filter(
                    created_at__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True) 
                )
        
        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
            except:
                pass

            if op.operator!=0:
                all_invoices = all_invoices.filter(operator=op.operator)
            else:
                all_invoices = all_invoices.filter(operator__company__id=op.company)

        all_count = all_invoices.count()

        if search:
            filtered_invoices = all_invoices.filter(
                Q(pk=int(search) if search.isdigit() else None)
                #| Q(customer__first_name__unaccent__icontains=search)
                #| Q(customer__last_name__unaccent__icontains=search)
                | Q(complete_name__unaccent__icontains=search)
                | Q(comment__unaccent__icontains=search)
                | Q(total=int(search) if search.isdigit() else None)
            )
        else:
            filtered_invoices = all_invoices

        filtered_invoices = filtered_invoices.distinct()

        filtered_count = filtered_invoices.count()

        if length == -1:
            sliced_invoices = filtered_invoices
        else:
            sliced_invoices = filtered_invoices[start : start + length]
        if to_excel:
            return list(map(prepare_invoice, filtered_invoices))
        else:

            data = json.dumps(
                {
                    "draw": request.GET.get("draw", 1),
                    "recordsTotal": all_count,
                    "recordsFiltered": filtered_count,
                    "data": list(map(prepare_invoice, sliced_invoices)),
                }
            )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@permission_required("customers.list_invoice")
def invoice_excel(request):
    data = invoice_ajax(request, True)
    #print(data)
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({"bold": True})
    red = workbook.add_format({"font_color": "red"})
    worksheet.set_row(0, None, bold)
    header = (
        "ID",
        "Fecha",
        "Descripción",
        "Tipo",
        "Monto ($)",
        "Pagada",
        "Cliente",
        "O. Factura",
        "O. Cliente",
        "Direccion",
        "Comuna",
        "Numero Servicio"
    )

    worksheet.write_row(0, 0, header)
    row = 1
    col = 0

    total_amount = 0
    for item_data in data:
        try:
            formatted_creation_date = formats.date_format(
                item_data["created_at"], "SHORT_DATE_FORMAT"
            )
        except AttributeError:
            formatted_creation_date = item_data["created_at"]

        worksheet.write(row, 0, item_data["pk"] )
        worksheet.write(row, 1, formatted_creation_date)
        worksheet.write(row, 2, item_data["comment"])
        worksheet.write(row, 3, item_data["kind"])
        worksheet.write(row, 4, item_data["total"])
        worksheet.write(row, 5, "Si" if item_data["paid"] else "No")
        worksheet.write(row, 6, item_data["customer_name"])
        worksheet.write(row, 7, item_data["operator"])
        worksheet.write(row, 8, item_data["customer_operator"])
        worksheet.write(row, 9, item_data["direccion"])
        worksheet.write(row, 10, item_data["comuna"])
        worksheet.write(row, 11, item_data["services"])
        #total_amount = total_amount + int(
        #    item_data["formatted_amount"].replace(",", "")
        #)
        row += 1

    # Guarda el total con la suma de todos los montos
    #worksheet.write(row, 4, "TOTAL")
    #worksheet.write(row, 5, total_amount)
    row += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = 'attachment; filename="boletas_matrix.xlsx"'
    return response


@login_required
@permission_required("customers.list_payment")
def invoice_ajax2(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "pk",
            "1": "created_at",
            "2": "due_date",
            "3": "paid_on",
            "4": "kind",
            "5": "comment",
            "6": "total",
            "7": "paid_on",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        kind = request.GET.get("kind", None)
        kind_load = request.GET.get("kind_load", None)
        start_date = request.GET.get("start_date", None)
        end_date = request.GET.get("end_date", None)
        paid = request.GET.get("paid_on", None)
        operator = request.GET.get("operator", None)

        #print(paid)
        #print(operator)
        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_invoices = Invoice.objects.filter(customer__isnull=True).order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_invoices = Invoice.objects.filter(customer__isnull=True)

        if kind:
            all_invoices = all_invoices.filter(kind=kind, customer__isnull=True)

        if kind_load == "2":
            all_invoices = all_invoices.filter(kind_load=2)
        elif kind_load == "3":
            all_invoices = all_invoices.filter(kind_load=3)

        if paid == "True":
            all_invoices = all_invoices.filter(paid_on__isnull=False)
        if paid == "False":
            all_invoices = all_invoices.filter(paid_on__isnull=True)

        if operator and operator != "null":
            all_invoices = all_invoices.filter(operator=operator)

        if start_date:
            startDateList = start_date.split("-")
            year = int(startDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(int(startDateList[1]))
            day = int(startDateList[2])
            try:
                all_invoices = all_invoices.filter(
                created_at__gte=datetime(year, month, day))
            except pytz.exceptions.AmbiguousTimeError:
                all_invoices = all_invoices.filter(
                    created_at__gte=make_aware(datetime(year, month, day), get_current_timezone(), is_dst=True) 
                )

        if end_date:
            endDateList = end_date.split("-")
            year = int(endDateList[0])
            if year > datetime.now().year:
                year = int(str(year)[:4])
            month = int(endDateList[1])
            day = int(endDateList[2])

            try:
                all_invoices = all_invoices.filter(
                created_at__lte=datetime(year, month, day, 23, 59, 59))
            except pytz.exceptions.AmbiguousTimeError:
                all_invoices = all_invoices.filter(
                    created_at__lte=make_aware(datetime(year, month, day, 23, 59, 59), get_current_timezone(), is_dst=True) 
                )

        username = None
        context={}
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
            except:
                pass

            if op.operator!=0:
                all_invoices = all_invoices.filter(operator=op.operator)
            else:
                all_invoices = all_invoices.filter(operator__company__id=op.company)

        all_count = all_invoices.count()

        if search:
            filtered_invoices = all_invoices.filter(
                Q(pk=int(search) if search.isdigit() else None)
                | Q(customer__first_name__unaccent__icontains=search)
                | Q(customer__last_name__unaccent__icontains=search)
                | Q(comment__unaccent__icontains=search)
                | Q(total=int(search) if search.isdigit() else None)
            )
        else:
            filtered_invoices = all_invoices

        filtered_invoices = filtered_invoices.distinct()

        filtered_count = filtered_invoices.count()

        if length == -1:
            sliced_invoices = filtered_invoices
        else:
            sliced_invoices = filtered_invoices[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_invoice, sliced_invoices)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


@login_required
@permission_required("customers.list_payment")
def bankaccount_ajax(request):
    operator = request.GET.get("operator", None)
    if operator:
        bank_accounts = BankAccount.objects.filter(operator=operator).values()
    else:
        bank_accounts = BankAccount.objects.filter().values()

    data = json.dumps({"bankaccounts": list(bank_accounts)})

    return HttpResponse(data, content_type="application/json")


class FindExcelDifferences(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    permission_required = "customers.view_payment"
    template_name = "customers/find_excel_differences.html"
    form_class = DiffForm

    def form_valid(self, form):
        beginning = form.cleaned_data["beginning"]
        end = form.cleaned_data["end"]
        transfers = form.cleaned_data["transfers"]

        raw_table = etl.fromxlsx(transfers)
        # special RUT massage
        stripped_table = raw_table.convert("RUT Pagador", str.strip)
        valid, invalid = validate_payment_data(stripped_table, PAYMENT_CONSTRAINTS)
        local_only = [stripped_table.header()]
        not_found = [stripped_table.header()]
        multiple_services = [stripped_table.header()]

        found_payment_ids = []

        for t in valid.data():
            try:
                if t[3] != "0-0":
                    cleaned_rut = RUT_FIELD.clean(t[3].lstrip("0"))
                else:
                    cleaned_rut = t[3]

                customer = Customer.objects.get(payorrut__rut=cleaned_rut)
                payment = customer.payment_set.get(
                    kind=Payment.MASS_TRANSFER,
                    paid_on=t[1],
                    deposited_on=t[1],
                    amount=abs(t[4]),
                )
                found_payment_ids.append(payment.pk)
            except Customer.MultipleObjectsReturned:
                multiple_services.append(t[1:])
            except Payment.MultipleObjectsReturned:
                multiple_services.append(t[1:])
            except Customer.DoesNotExist:
                not_found.append(t[1:])
            except Payment.DoesNotExist:
                not_found.append(t[1:])
        px = Payment.objects.filter(
            kind=Payment.MASS_TRANSFER, paid_on__gte=beginning, paid_on__lte=end
        ).exclude(pk__in=found_payment_ids)

        for payment in px:
            local_only.append(
                (
                    payment.paid_on,
                    payment.customer.name,
                    " ".join(
                        payment.customer.payorrut_set.values_list("rut", flat=True)
                    ),
                    payment.amount,
                    None,
                    None,
                )
            )

        local_only_buffer = io.BytesIO()
        etl.toxlsx(local_only, local_only_buffer)
        local_only_buffer.seek(0)

        not_found_buffer = io.BytesIO()
        etl.toxlsx(not_found, not_found_buffer)
        not_found_buffer.seek(0)

        multiple_services_buffer = io.BytesIO()
        etl.toxlsx(multiple_services, multiple_services_buffer)
        multiple_services_buffer.seek(0)

        zip_buffer = io.BytesIO()
        zf = zipfile.ZipFile(zip_buffer, "w")
        zf.writestr("solo_en_matrix.xlsx", local_only_buffer.getvalue())
        zf.writestr("solo_en_excel.xlsx", not_found_buffer.getvalue())
        zf.writestr("coincidencia_multiple.xlsx", multiple_services_buffer.getvalue())
        zf.close()

        resp = HttpResponse(
            zip_buffer.getvalue(), content_type="application/x-zip-compressed"
        )
        resp["Content-Disposition"] = "attachment; filename=diferencias.zip"
        return resp


find_excel_differences = FindExcelDifferences.as_view()


# secure
class EmailList(LoginRequiredMixin, ListView):
    model = Email
    paginate_by = 8
    # permission_required = 'customers.list_call'


email_list = EmailList.as_view()


# FIXME secure
class EmailCreate(LoginRequiredMixin, CreateView):
    model = Email
    form_class = EmailForm
    success_url = "/emails/"
    # permission_required = 'customers.add_plan'


email_create = EmailCreate.as_view()


# @permission_required('customers.view_service')
@login_required
def service_plan_orders(request, slug):
    service = get_object_or_404(Service, pk=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                if service.operator.company.id==op.company:
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass
    orders = service.planorder_set.exclude(team__isnull=True).exclude(cancelled=True)
    cancelled_orders = service.planorder_set.exclude(cancelled=False)
    context = {
        "can_show_detail":can_show_detail,
        "object": service,
        "service": service,
        "customer": service.customer,
        "orders": orders,
        "cancelled_orders": cancelled_orders,
    }
    return render(request, "customers/service_plan_orders.html", context)


class PlanOrderDetail(LoginRequiredMixin, DetailView):
    model = PlanOrder

    def dispatch(self, *args, **kwargs):
        self.service = get_object_or_404(Service, pk=self.kwargs["slug"])
        return super(PlanOrderDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super(PlanOrderDetail, self).get_context_data(*args, **kwargs)
        ctx["customer"] = self.service.customer
        ctx["service"] = self.service
        ctx["cancel_url"] = reverse("service-plan-orders", args=[self.service.slug])
        ctx["schedule_url"] = reverse(
            "plan-order-schedule", args=[self.service.slug, self.object.pk]
        )
        return ctx


plan_order_detail = PlanOrderDetail.as_view()


class PlanOrderCreate(LoginRequiredMixin, CreateView):
    model = PlanOrder
    form_class = PlanOrderCreateForm
    # permission_required = 'customers.add_order'
    def dispatch(self, *args, **kwargs):
        self.service = get_object_or_404(Service, pk=self.kwargs["slug"])
        return super(PlanOrderCreate, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=PlanOrderCreateForm):
        form = super(PlanOrderCreate, self).get_form(form_class)
        enabled_kinds = self.service.node.plan_order_kinds.values_list(
            "name", flat=True
        )
        if enabled_kinds:
            form.fields["kind"].choices = [
                (value, label)
                for value, label in form.fields["kind"].choices
                if value in enabled_kinds
            ]
        return form

    def get_context_data(self, *args, **kwargs):
        ctx = super(PlanOrderCreate, self).get_context_data(*args, **kwargs)
        ctx["customer"] = self.service.customer
        ctx["service"] = self.service
        ctx["cancel_url"] = reverse("service-plan-orders", args=[self.service.slug])
        return ctx

    def get_success_url(self):
        # better redirect to detail view FIXME not! now to schedule
        return reverse(
            "plan-order-schedule",
            kwargs={"slug": self.service.number, "pk": self.object.pk},
        )

    def form_valid(self, form):
        order = form.save(commit=False)
        order.service = self.service
        # order.subject = order.subject or ''
        # order.subject += "\nTel: {}".format(order.service.customer.phone)
        order.save()
        # auto schedule

        # data = order.auto_schedule()
        # order.team = data['resultadoAgendamento']['equipe']
        # new_tuple = anon_tuple(data['resultadoAgendamento']['atendimentoPlanejado']['inicio'], data['resultadoAgendamento']['atendimentoPlanejado']['fim'])
        # order.start = new_tuple[0]
        # order.end = new_tuple[1]
        # order.save()
        return super(PlanOrderCreate, self).form_valid(form)


plan_order_create = PlanOrderCreate.as_view()


def anon_tuple(start, end):
    # fixme
    return (start, end)

    AM = (start.replace(hour=10, minute=0), end.replace(hour=14, minute=0))
    PM = (start.replace(hour=14, minute=1), end.replace(hour=18, minute=0))
    AFTER = (start.replace(hour=18, minute=1), end.replace(hour=22, minute=0))
    if AM[0] <= start < PM[0]:
        return AM
    elif PM[0] <= start < AFTER[0]:
        return PM
    else:
        return AFTER


def plan_order_schedule(request, **kwargs):
    order = get_object_or_404(PlanOrder, pk=kwargs["pk"])
    avx = order.get_availabilities()
    if request.method == "POST":
        # form = PlanOrderScheduleForm(request.POST)

        import dateutil.parser as dt

        start = timezone.localtime(dt.parse(request.POST["start"]))
        end = timezone.localtime(
            dt.parse(request.POST["end"]) + timezone.timedelta(minutes=1)
        )
        
        feriadoOk = 0
        if "feriado" in request.POST.keys():
            if request.POST["feriado"] == 'on':
                feriadoOk = 1
        
        feriados = getattr(config, 'feriados')
        if feriados:
            feriados = feriados.replace(' ', '')
            feriados = feriados.split(',')
            for f in feriados:
                date = f.split('/')
                if start.day == int(date[0]) and start.month == int(date[1]) and start.year == int(date[2]) and feriadoOk == 0:
                    messages.add_message(
                    request,
                    messages.WARNING,
                    "No se pudo agendar orden de servicio, Día Feriado",
                    )
                    return redirect(reverse("service-plan-orders", args=[order.service.slug]))
                    
        # HACK HACK HACK
        # order.delete_iclass_order()
        # order.start = None
        # order.end = None
        # order.team = None
        # order.save()
        # order.pk = None
        # order.save()
        # HACK HACK HACK

        req_start, req_end = anon_tuple(start, end)

        if order.start:
            rr = order.reschedule_iclass_order(req_start, req_end)
        else:
            rr = order.create_iclass_order(req_start, req_end)

        agenda = rr["resultadoAgendamento"]

        previous_orders = PlanOrder.objects.filter(service__number = order.service.number)
        is_active = False
        current_time = timezone.now()
        for po in previous_orders:
            if po.end != None:
                if current_time < po.end and po.cancelled == False:
                    is_active = True
                    break
        
        if is_active == True:
            messages.add_message(
                request,
                messages.WARNING,
                "No se puede agendar, ya hay una orden de servicio activa.",
            )
            return redirect(reverse("service-plan-orders", args=[order.service.slug]))


        try:
            plan = agenda["atendimentoPlanejado"]
            new_tuple = anon_tuple(plan["inicio"], plan["fim"])
            order.start = new_tuple[0]
            order.end = new_tuple[1]
            order.team = agenda["equipe"]
            order.save()
        except Exception as e:
            print(e)
            messages.add_message(
                request,
                messages.WARNING,
                "Problema con iclass, no se puede agendar en este horario.",
            )
            return redirect(reverse("service-plan-orders", args=[order.service.slug]))

        if getattr(config, "allow_service_order_slack") == "yes":
            try:
                TECHNOLOGY_KIND_CHOICES = {"0": "UTP","1": "GPON","2": "GEPON","3": "ANTENNA",}
                WEEK_DAY = {"0": "Lunes", "1": "Martes", "2": "Miercoles", "3": "Jueves", "4": "Viernes", "5": "Sabado", "6": "Domingo",}

                string = ''
                if order.service:
                    string = string + '*# del Cliente*: ' + str(order.service.number) + '\n'
                string = string + '*Nombre del Cliente*: ' + str(order.customer_name) + '\n'
                if order.service:
                    if order.service.customer.rut:
                        string = string + '*RUT del Cliente*: ' + str(order.service.customer.rut) + '\n'
                string = string + "*Dirección*: " + str(order.street_address) + '\n'
                string = string + "*Ciudad*: " + str(order.city) + '\n'
                if order.service:
                    if order.service.customer:
                        string = string + '*Teléfono*: ' + str(order.service.customer.phone) + '\n'
                        string = string + '*Email*: ' + str(order.service.customer.email) + '\n'
                string = string + "*Tipo de OS*: " + str(order.kind) + '\n'
                if order.service:
                    string = string + "*Tecnología*: " + str(TECHNOLOGY_KIND_CHOICES[str(order.service.technology_kind-1)]) + '\n'
                if order.service:
                    if order.service.plan:
                        string = string + "*Plan*: " + str(order.service.plan.name) + '\n'
                fecha = order.start.date().strftime("%d/%m/%Y")
                start_time = order.start.time().strftime("%H:%M")
                end_time = order.end.time().strftime("%H:%M")
                string = string + "*Fecha*: " + str(WEEK_DAY[str(order.start.weekday())]) + ' ' + str(fecha) + '\n'
                string = string + "*Hora*: " + str(start_time) + ' - ' + str(end_time) + '\n'
                string = string + '*Asunto*: ' + str(order.subject) + '\n'
                if order.agent:
                    string = string + '*Agente*: ' + str(order.agent) + ' ' + str(order.agent.email) + '\n'

                bloque = [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":matrix: *Nueva Orden de Servicio*"
                        },
                    },
                    {
                        "type": "divider",
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": string
                        },
                        "accessory": {
                            "type": "image",
                            "image_url": "https://www.pngix.com/pngfile/middle/427-4272403_calendario-10-jul-2017-tarih-png-transparent-png.png",
                            "alt_text": "Calendar"
                        },
                    },
                    {
                        "type": "divider",
                    },
                ]
                
                data_slack = {
                "text": "Orden de Servicio",
                "blocks": bloque,
                }
                http = urllib3.PoolManager()
                config_slack=ConfigOperator.objects.filter(operator=order.service.operator, name="Slack")
                if config_slack.count()==1:
                    if "slack_orden_servicio" in config_slack[0].flag:
                        clientes_slack = config_slack[0].flag["slack_orden_servicio"]
                        #devel_prueba_slack = 'https://hooks.slack.com/services/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'
                        #clientes_slack = 'https://hooks.slack.com/services/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'
                        response_slack = http.request(
                                "POST",
                                clientes_slack,
                                body=json.dumps(data_slack),
                                headers={'Content-Type': 'application/json'}
                        )
            except:
                messages.add_message(
                    request,
                    messages.WARNING,
                    "No se pudo enviar información de la Orden de Servicio a Slack",
                )
        
        if (start != order.start) or (end != order.end):
            # signal this to the user
            messages.add_message(
                request,
                messages.WARNING,
                "Verificar período de la orden de servicio: se agendó en horario diferente debido a disponibilidad.",
            )
        messages.add_message(
            request, messages.SUCCESS, "Se ha agendado una nueva orden de servicio."
        )

        return redirect(reverse("service-plan-orders", args=[order.service.slug]))
    else:
        form = PlanOrderScheduleForm((), initial={"kind": order.kind})
    return render(
        request,
        "customers/plan_order_schedule.html",
        dict(
            customer=order.service.customer,
            service=order.service,
            object=order,
            form=form,
            avx=avx,
            cancel_url=reverse("service-plan-orders", args=[order.service.slug]),
        ),
    )


def plan_order_delete(request, **kwargs):
    order = get_object_or_404(PlanOrder, pk=kwargs["pk"])

    if request.method == "POST":
        form = PlanOrderDeleteForm(request.POST)
        if form.is_valid():
            order.delete_iclass_order(subject=form.cleaned_data["subject"])
            order.cancelled = True
            order.save()
            messages.add_message(
                request, messages.SUCCESS, "Se ha cerrado la orden de servicio."
            )
        return redirect(reverse("service-plan-orders", args=[order.service.slug]))
    else:
        form = PlanOrderDeleteForm()
    return render(
        request,
        "customers/planorder_delete.html",
        dict(
            customer=order.service.customer,
            service=order.service,
            cancel_url=reverse("service-plan-orders", args=[order.service.slug]),
            form=form,
        ),
    )


@login_required
def quick_drop(request):
    q = request.GET.get("number", None)
    if q:
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                
                if op.operator==0:
                    try:
                        service = Service.objects.get(number=q, operator__company__id=op.company)
                    except Service.DoesNotExist:
                        service = None
                else:
                    try:
                        service = Service.objects.get(number=q, operator=op.operator)
                    except Service.DoesNotExist:
                        service = None
            except:
                pass
    else:
        service = None
    return render(request, "customers/quick_drop.html", {"service": service})


def required_login_view(request):
    if request.method == "POST":
        next = request.POST.get("next", "/")
        return HttpResponseRedirect(next)
        # return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/accounts/login'))
    return render(request, "registration/required_login.html")


## manual cartola load

DATETIME_PARSER = etl.datetimeparser("%d/%m/%Y")

CARTOLA_CONSTRAINTS = [
    dict(name="paid_on_valid", field="Fecha", test=DATETIME_PARSER),
    #dict(name="rut_valid", field="Rut Origen/Destino", test=RUT_FIELD.clean),
    dict(name="doc_int", field="Num. Documento", test=int),
    dict(name="cargo_int", field="Cargo", test=lambda x: x and isinstance(x, float) or isinstance(x, int)), #test=int),
    dict(name="abono_int", field="Abono", test=lambda x: x and isinstance(x, float) or isinstance(x, int)), #test=int),
    dict(name="saldo_int", field="Saldo diario", test=lambda x: x and isinstance(x, float) or isinstance(x, int)), #test=int),
]


def validate_cartola_data(table, constraints):
    t2 = etl.addrownumbers(table)
    #Excel_date = 44405
    #dt = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + Excel_date - 2).strftime("%d/%m/%Y")
    #print(etl.convert(t2,'Fecha',  lambda v: datetime.fromordinal(datetime(1900, 1, 1).toordinal() + int(v) - 2).strftime("%d/%m/%Y")))
    t2=etl.convert(t2,'Fecha',  lambda v: datetime.fromordinal(datetime(1900, 1, 1).toordinal() + int(v) - 2).strftime("%d/%m/%Y"))
    problems = etl.validate(t2, constraints=constraints)
    #print(problems)
    rows_with_errors = set(problems.columns()["row"])
    invalid = etl.selectin(t2, "row", rows_with_errors)
    table= etl.addfield(invalid, 'Atributo', str(set(problems.columns()["field"])))
    invalid= etl.addfield(table, 'Error', str(set(problems.columns()["error"])))
    valid = etl.selectnotin(t2, "row", rows_with_errors)
    return valid, invalid


def process_manual_cartola(request, table, excel, pdf, bank_account, data_json):
    raw_table = table
    stripped_table = etl.convert(raw_table, "Num. Documento", str.strip)
    #int_table = stripped_table.convert("Cargo", lambda x: int(x))
    #int_table1 = int_table.convert("Abono", lambda x: int(x))
    #int_table = stripped_table.convert("Monto", lambda x: x and isinstance(x, float) or isinstance(x, int)) #test=int),)
    #int_table = int_table.convert("Saldo diario", lambda x: x and isinstance(x, float) or isinstance(x, int)) #test=int),)
    valid, invalid = validate_cartola_data(stripped_table, CARTOLA_CONSTRAINTS)

    last_cartola=Cartola.objects.filter(bank_account=data_json["bank_account"]).order_by("-end")

    # Variable que detecta error si el saldo inicial no coincide con el saldo final de la anterior cartola.
    error=False
    if last_cartola.count()!=0:
        last_cartola=last_cartola.first()
        if last_cartola.saldo_final!=data_json["saldo_inicial"]:
            error=True

    if invalid.nrows() > 0:
        print("Invalid")
        # Mensaje de error
        messages.add_message(
            request, messages.ERROR, "La data es invalida, verifique para luego intentar volver a cargarla."
        )
        fails=True
    elif error:
        # Mensaje de error 
        messages.add_message(
            request, messages.ERROR, "El saldo inicial no coincide con el saldo final de la anterior cartola."
        )
        fails=True
    elif data_json["end"] < data_json["start"]:
        # Mensaje de error 
        messages.add_message(
            request, messages.ERROR, "La fecha del periodo final es menor a la inicial."
        )
        fails=True
    else:
        fails=False
        # Valores iniciales.
        total_cargo=0
        total_abono=0
        # Se obtiene el total por tipo de movimiento.
        for t in valid.data():
            if t[5]!=0:
                total_cargo= total_cargo+float(t[5])
            else:
                total_abono= total_abono+float(t[6])

        # Si no coincide con los ingresados en el form, no se crea nada.
        if float(total_cargo)!=float(data_json["total_cargo"]) or float(total_abono)!=float(data_json["total_abono"]):
            # Mensaje de error 
            messages.add_message(
                request, messages.ERROR, "Los montos totales no coinciden con el excel."
            )
        else:
            cartola=Cartola.objects.create(
                number = data_json["number"],
                start = data_json["start"],
                end = data_json["end"],
                saldo_final=data_json["saldo_final"],
                saldo_inicial=data_json["saldo_inicial"],
                total_cargo=data_json["total_cargo"],
                total_abono=data_json["total_abono"],
                bank_account=data_json["bank_account"],
                pdf= pdf,
                excel= excel,
            )
            for t in valid.data():
                #print(t[4])
                # Es un abono
                if t[6]!=0:
                    cc=CartolaMovement.objects.create(
                        cartola=cartola,
                        date=DATETIME_PARSER(t[1]),
                        sucursal = t[2],
                        description = t[3],
                        number_document = t[4],
                        total = t[6],
                        kind = 2,
                        saldo_diario=float(t[7]),
                    )
                else:
                    # Es un cargo
                    cc=CartolaMovement.objects.create(
                        cartola=cartola,
                        date=DATETIME_PARSER(t[1]),
                        sucursal = t[2],
                        description = t[3],
                        number_document = t[4],
                        total = t[5],
                        kind = 1,
                        saldo_diario=float(t[7])
                    )

    excel_buffer = io.BytesIO()
    stripped_table.toxlsx(excel_buffer)
    excel_buffer.seek(0)

    valid_buffer = io.BytesIO()
    valid.cutout("row").toxlsx(valid_buffer)
    valid_buffer.seek(0)

    invalid_buffer = io.BytesIO()
    invalid.cutout("row").toxlsx(invalid_buffer)
    invalid_buffer.seek(0)


    return dict(
        processed_count=stripped_table.nrows(),
        valid_count=valid.nrows(),
        invalid_count=invalid.nrows(),
        error=fails,
        files=dict(
            processed=excel_buffer,
            valid=valid_buffer,
            invalid=invalid_buffer,
        ),
    )



@login_required
@permission_required("customers.change_service")
def manual_cartolas(request):
    username = None
    context={}
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            if op.operator==0:
                name_operator="Todos"
                banco=BankAccount.objects.filter(operator__company__id=op.company)
            else:
                name_operator=Operator.objects.get(id=op.operator).name
                banco=BankAccount.objects.filter(operator__id=op.operator)
            context={"companies":companies.exclude(id=op.company),
                    "current_company":op.company,
                    "current_company_name":Company.objects.get(id=op.company).name,
                    "current_operator":op.operator,
                    "current_operator_name":name_operator,
                    "operadores": Operator.objects.filter(company__id=op.company).exclude(id=op.operator),
                    "cuantas_bancarias": banco
                }
        except:
            pass
    if request.method == "POST":
        form = CartolaLoadForm(request.POST, request.FILES)
        context["form"] = form
        if form.is_valid():

            table, excel = form.cleaned_data["excel_file"]
            pdf = form.cleaned_data["pdf_file"]
            data = form.cleaned_data
            bank_account = data["bank_account"]
            #number = data["number"]
            #start = data["start"]
            #end = data["end"]
            #saldo_final=data["saldo_final"]
            #saldo_inicial=data["saldo_inicial"]
            data_json = data
            processing_results = process_manual_cartola(request, table, excel, pdf, bank_account, data_json)
            files = processing_results.pop("files")
            if processing_results["error"]:
                processing_results["created_count"] = (
                    0
                )
            else:
                processing_results["created_count"] = (
                    processing_results["valid_count"]
                )
            context.update(processing_results)
            
            # Clientes con balance igual a 0
            balances = Balance.objects.filter(balance=0)
            for c in balances:
                boletas=Invoice.objects.filter(customer=c.customer, paid_on__isnull=True).update(paid_on=date.today())

            # Clientes con balance mayor a 0
            balances = Balance.objects.filter(balance__gt=0)
            # Marca como pagadas las boletas.
            for c in balances:
                boletas=Invoice.objects.filter(customer=c.customer, paid_on__isnull=True).update(paid_on=date.today())
            
            # File shenaningans
            old_file_paths = request.session.get("cartolas_files", {})
            for file_path in old_file_paths.values():
                try:
                    os.remove(file_path)
                except FileNotFoundError:
                    pass

            new_file_paths = {}

            for k, v in files.items():
                fd, temp_path = mkstemp()
                with open(temp_path, "wb") as f:
                    f.write(v.read())
                new_file_paths[k] = temp_path
                os.close(fd)

            request.session["cartolas_files"] = new_file_paths
    else:
        context["form"] = CartolaLoadForm()

        asks_for_file = request.GET.get("file", None)
        if asks_for_file:
            file_paths = request.session.get("cartolas_files", {})
            path = file_paths.get(asks_for_file, None)
            if path:
                with open(path, "rb") as f:
                    response = HttpResponse(
                        f,
                        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    )
                    response[
                        "Content-Disposition"
                    ] = "attachment; filename={}.xlsx".format(asks_for_file)
                    return response

    return render(request, "customers/manual_cartola.html", context)


#### API #####

class ConfigOperatorViewSet(viewsets.ViewSet):

    def list(self, request):
        queryset = ConfigOperator.objects.all()
        serializer = ConfigOperatorSerializer1(queryset, many=True,context={'request': request})
        print(serializer)
        return Response(serializer.data)

    def get(self, request):
        queryset = ConfigOperator.objects.all()
        serializer = ConfigOperatorSerializer1(queryset, many=True,context={'request': request})
        return Response(serializer.data)

    def post(self, request):
        data=request.data
        # print(data)
        try:
            serializer = ConfigOperatorSerializer(data,context={'request': request})
            #print(serializer.data)
            validated_data=serializer.data
        except Exception as e:
            print(str(e))
            return Response({"error": str(e)},status=status.HTTP_400_BAD_REQUEST)# JsonResponse([str(e)], safe=False)
        
        # Obtener el operador
        try:  
            operator=Operator.objects.get(id=validated_data["operator"])
        except Exception as e:
            print(str(e))
            return Response({"error": str(e)},status=status.HTTP_400_BAD_REQUEST)
        try:  
            config_op=ConfigOperator.objects.get(operator=operator, name=validated_data["name"])
        except Exception as e:
            config_op=ConfigOperator.objects.create(operator=operator, name=validated_data["name"], flag=validated_data["flag"])
            print(str(e))
            return Response(data, status=status.HTTP_201_CREATED)

        if config_op.flag is not None:
            for i in validated_data["flag"]:
                if i in config_op.flag:
                    config_op.flag[i]=validated_data["flag"][i]
                    config_op.save()
                else:
                    config_op.flag[i]=validated_data["flag"][i]
                    config_op.save()
            return Response(data, status=status.HTTP_201_CREATED)

class ServiceIpsViewSet(viewsets.ViewSet):

    def list(self, request):
        queryset = User.objects.all()
        serializer = ServiceIpsSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request):
        data=request.data
        # print(data)
        try:
            serializer = ServiceIpsSerializer(data,context={'request': request})
            #print(serializer.data)
            validated_data=serializer.data
        except Exception as e:
            print(str(e))
            return Response({"error": str(e)},status=status.HTTP_400_BAD_REQUEST)# JsonResponse([str(e)], safe=False)
        
        # Obtener el servicio
        try:  
            service=Service.objects.get(pk=validated_data["service"])
        except Exception as e:
            print(str(e))
            return Response({"error": str(e)},status=status.HTTP_400_BAD_REQUEST)
        
        # Busca el equipo primario.
        addr=validated_data["ip"]
        equipo=NetworkEquipment.objects.filter(primary=True, service=service)

        # Si no tiene equipo.
        if equipo.count()==0:
            return Response(
                {"error": "El servicio "+str(validated_data["service"])+ " no tiene equipo primario"},
                status=status.HTTP_400_BAD_REQUEST)
        elif equipo.count()==1:
            # Actualizo el equipo
            equipo.update(ip=addr)
            return Response(data, status=status.HTTP_201_CREATED)
        else:
            # Tiene mas equipos primarios.
            return Response( 
                {"error": "El servicio "+str(validated_data["service"])+ " tiene varios equipos primarios"},
                status=status.HTTP_200_OK)

        return Response(data, status=status.HTTP_200_OK)

class PlanViewSet(viewsets.ModelViewSet):
    queryset = Plan.objects.all()
    filter_fields = "__all__"
    serializer_class = PlanSerializer
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]

class BankViewSet(viewsets.ModelViewSet):
    queryset = Bank.objects.all()
    filter_fields = "__all__"
    serializer_class = BankSerializer
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
        
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]

class BankAccountViewSet(viewsets.ModelViewSet):
    queryset = BankAccount.objects.all()
    filter_fields = "__all__"
    serializer_class = BankAccountSerializer
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]

class PlanOrderViewSet(viewsets.ModelViewSet):
    queryset = PlanOrder.objects.all()
    filter_fields = "__all__"
    serializer_class = PlanOrderSerializer
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        
        try:
            serializer.is_valid(raise_exception=True)
            obj=serializer.save()
            #print(obj)
        except Exception as e:
            print(str(e))
            return Response(status=status.HTTP_400_BAD_REQUEST)

        content={
        #'serializer': data_serializer,
        'planOrder': PlanOrderSerializer(
                    PlanOrder.objects.get(id=obj.id),
                    context={'request': request}).data,
        'service':  ServiceSerializer(
                    Service.objects.get(id=obj.service.id),
                    context={'request': request}).data,
        }

        # Se coloca que el status es de creado.
        return  Response(content, status=status.HTTP_201_CREATED) 

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            obj=serializer.save()
            #print(obj)
        except Exception as e:
            print(str(e))
            return Response(status=status.HTTP_400_BAD_REQUEST)

        content={
        #'serializer': data_serializer,
        'planOrder': PlanOrderSerializer(
                    PlanOrder.objects.get(id=obj.id),
                    context={'request': request}).data,
        'service':  ServiceSerializer(
                    Service.objects.get(id=obj.service.id),
                    context={'request': request}).data,
        }

        # Se coloca que el status es de creado.
        return  Response(content, status=status.HTTP_201_CREATED) 

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]

class SellerViewSet(viewsets.ModelViewSet):
    queryset = Seller.objects.all()
    serializer_class = SellerSerializer
    filter_fields = "__all__"
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    filter_fields = "__all__"
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class PromoViewSet(viewsets.ModelViewSet):
    queryset = Promotions.objects.all()
    serializer_class = PromoSerializer
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"
    filter_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class ServiceViewSet(viewsets.ModelViewSet):
    permission_classes = (IsSystemInternalPermission,)
    serializer_class = ServiceSerializer
    filter_fields = "__all__"
    ordering_fields = "__all__"

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        #print("AQUIIII")
        queryset = Service.objects.all()
        number = self.request.query_params.get("number", None)
        if number is not None:
            queryset = queryset.filter(number=number)
        return queryset

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            serializer.save()
        except:
            return False

        return Response(
            status=status.HTTP_200_OK
        )

class CommercialActivityViewSet(viewsets.ModelViewSet):
    queryset = CommercialActivity.objects.all()
    serializer_class = CommercialActivitySerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filter_fields = "__all__"
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    filter_fields = "__all__"
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class CallViewSet(viewsets.ModelViewSet):
    queryset = Call.objects.all()
    serializer_class = CallSerializer
    filter_fields = "__all__"
    permission_classes = (IsSystemInternalPermission,)
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class ComunaViewSet(viewsets.ModelViewSet):
    queryset = Comuna.objects.all()
    serializer_class = ComunaSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class CalleViewSet(viewsets.ModelViewSet):
    queryset = Calle.objects.all()
    serializer_class = CalleSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class PaymentViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class BalanceViewSet(viewsets.ModelViewSet):
    queryset = Balance.objects.all()
    serializer_class = BalanceSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class InvoiceItemViewSet(viewsets.ModelViewSet):
    queryset = InvoiceItem.objects.all()
    serializer_class = InvoiceItemSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class DireccionViewSet(viewsets.ModelViewSet):
    queryset = Direccion.objects.all()
    serializer_class = DireccionSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class PayorRutViewSet(viewsets.ModelViewSet):
    queryset = PayorRut.objects.all()
    serializer_class = PayorRutSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class OperatorViewSet(viewsets.ModelViewSet):
    queryset = Operator.objects.all()
    serializer_class = OperatorSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = "__all__"
    ordering_fields = "__all__"

    @method_decorator(cache_page(60 * 2))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class FactibilityView(views.APIView):

    permission_classes = (IsSystemInternalPermission,)

    def post(self, request, *args, **kwargs):
        region = request.data.get("region", None)
        comuna = request.data.get("comuna", None)
        calle = request.data.get("calle", None)
        direccion = request.data.get("direccion", None)
        email = request.data.get("direccion", None)
        serializer_context = {
            "request": request,
        }
        if region:
            try:
                region_q = Region.objects.get(pk=region)
            except Region.DoesNotExist:
                content = {"message": "No data found"}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)
            if comuna:
                try:
                    comuna_q = Comuna.objects.get(region=region, pk=comuna)
                except Comuna.DoesNotExist:
                    content = {"message": "No data found"}
                    return Response(content, status=status.HTTP_400_BAD_REQUEST)
                if calle:
                    try:
                        calle_q = Calle.objects.get(comuna=comuna, pk=calle)
                    except Calle.DoesNotExist:
                        content = {"message": "No data found"}
                        return Response(content, status=status.HTTP_400_BAD_REQUEST)
                    if direccion:
                        try:
                            address = Direccion.objects.get(calle=calle, pk=direccion)
                            avail = address.availability_set.count() > 0
                            plans = list(
                                chain(
                                    *[
                                        map(lambda ss: str(ss), avx.node.plans.all())
                                        for avx in address.availability_set.all()
                                    ]
                                )
                            )
                        except Direccion.DoesNotExist:
                            address = None
                            avail = False
                            plans = []
                        if avail:
                            data = {"factibility": True, "plans": plans}
                        else:
                            data = {"factibility": False}
                        return Response(data, status=status.HTTP_200_OK)
                    else:
                        serializer = DireccionSerializer(
                            Direccion.objects.filter(calle=calle),
                            many=True,
                            context=serializer_context,
                        )
                        return Response(serializer.data, status=status.HTTP_200_OK)
                else:
                    serializer = CalleSerializer(
                        Calle.objects.filter(comuna=comuna),
                        many=True,
                        context=serializer_context,
                    )
                    return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                serializer = ComunaSerializer(
                    Comuna.objects.filter(region=region),
                    many=True,
                    context=serializer_context,
                )
                return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            serializer = RegionSerializer(Region.objects.all(), many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)


class ONUProvisioningView(viewsets.ModelViewSet):
    queryset = NetworkEquipment.objects.all()
    serializer_class = ONUProvisioningSerializer
    permission_classes = (IsSystemInternalPermission,)


#############################################

################# BILLING CYCLE REPORT ##################


def prepareCycles(cycle):
    dictionary = {
        "id": cycle.id,
        "interval": str(cycle.start).split(" ")[0]
        + " - "
        + str(cycle.end).split(" ")[0],
        "operator":cycle.operator.name,
    }
    return dictionary


@login_required
def cyclesAjax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "id",
            "1": "interval",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_cycles = BillingCycle.objects.all().order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_cycles = BillingCycle.objects.all()
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)    
                if op.operator==0:
                    all_cycles=all_cycles.filter(operator__company__id=op.company)
                else:
                    all_cycles=all_cycles.filter(operator__id=op.operator)
            except:
                pass
        all_count = all_cycles.count()
        if search:
            filtered_cycles = all_cycles.all()
        else:
            filtered_cycles = all_cycles
        filtered_cycles = filtered_cycles.distinct()
        filtered_count = filtered_cycles.count()
        if length == -1:
            slicedCycles = filtered_cycles
        else:
            slicedCycles = filtered_cycles[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepareCycles, slicedCycles)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404


class Cycle(LoginRequiredMixin, ListView):
    model = BillingCycle
    paginate_by = 10
    template_name = "customers/billingCycle.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Cycle, self).get_context_data(*args, **kwargs)
        return context


billingCycle = Cycle.as_view()


#############################################

################# INVOICES REPORT ##################


def prepareReport(invoice):
    allServices = []
    allServicesid = []
    for service in invoice.service.all():
        allServices.append(service.number)
        allServicesid.append(service.id)

    dictionary = {
        "invoice": invoice.id,
        "kind": dict(invoice.INVOICE_CHOICES)[invoice.kind],
        "created": invoice.created_at.strftime("%Y/%m/%d, %H:%M:%S"),
        "customer": invoice.customer.name,
        "services": allServices,
        "services_id": allServicesid,
        "total": price_tag(invoice.total),
    }

    allServices = None

    return dictionary


@login_required
def reportInvoiceAjax(request, **kwargs):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "invoice",
            "1": "kind",
            "2": "created",
            "3": "customer",
            "4": "services",
            "5": "total",
        }

        # Sort request
        sort = {"asc": "", "desc": "-"}
        sortCol = request.GET.get("order[0][column]", None)
        sortDirection = request.GET.get("order[0][dir]", None)

        # Search request
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        cycle = BillingCycle.objects.filter(id=kwargs["pk"]).first()
        cycleStart = cycle.start.strftime("%Y-%m-%d")
        cycleEnd = cycle.end.strftime("%Y-%m-%d")

        invoice = Invoice.objects.filter(
            (Q(created_at__gte=cycleStart) & Q(created_at__lte=cycleEnd))
            & Q(kind__gte=1, kind__lte=2)
        )
        if sortCol and sortDirection:
            allInvoice = invoice.order_by(sort[sortDirection] + columns[sortCol])
        else:
            allInvoice = invoice.order_by("kind", "-created_at")

        if search:
            pass

        totalRecord = allInvoice.count()

        sortedInvoice = allInvoice.distinct()
        filteredInvoice = sortedInvoice.count()

        if length == -1:
            slicedInvoice = sortedInvoice
        else:
            slicedInvoice = sortedInvoice[start : start + length]

        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": totalRecord,
                "recordsFiltered": filteredInvoice,
                "data": list(map(prepareReport, slicedInvoice)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

class ReportCycle(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Invoice
    paginate_by = 10
    permission_required = "customers.list_invoice"
    template_name = "customers/reportInvoice.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ReportCycle, self).get_context_data(*args, **kwargs)
        return context


reportInvoices = ReportCycle.as_view()

def reportInvoiceExport(arguments, **kwargs):
    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    # XLS HEADER
    worksheet.write(0, 0, "DOCUMENTO")
    worksheet.write(0, 1, "FECHA-Y/M/D")
    worksheet.write(0, 2, "HORA")
    worksheet.write(0, 3, "CLIENTE")
    worksheet.write(0, 4, "FOLIO")
    worksheet.write(0, 5, "SERVICIOS")
    worksheet.write(0, 6, "TOTAL")

    cycle = BillingCycle.objects.filter(id=kwargs["pk"]).first()
    start = cycle.start
    end = cycle.end

    # REPORT
    invoices = Invoice.objects.filter(
        Q(created_at__gte=start, created_at__lte=end) & Q(kind__gte=1, kind__lte=2)
    ).order_by("kind", "-created_at")

    r = 1
    for invoice in invoices:
        worksheet.autofilter("A1:G" + str(r))

        kind = dict(invoice.INVOICE_CHOICES)[invoice.kind]
        date = invoice.created_at.strftime("%Y/%m/%d")
        time = invoice.created_at.strftime("%H:%M:%S")
        customer = invoice.customer.name
        folio = invoice.folio
        total = invoice.total

        allServices = []
        for service in invoice.service.all():
            allServices.append(service.number)

        serviceString = str(allServices).replace("[", "").replace("]", "")

        # Writing data row by row
        worksheet.write(r, 0, kind)
        worksheet.write(r, 1, date)
        worksheet.write(r, 2, time)
        worksheet.write(r, 3, customer)
        worksheet.write(r, 4, folio)
        worksheet.write(r, 5, serviceString)
        worksheet.write(r, 6, total)

        r += 1

    workbook.close()
    output_file.seek(0)
    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )

    response["Content-Disposition"] = (
        'attachment; filename="reporte del ciclo con ID '
        + kwargs["pk"]
        + ', al '
        + datetime.now().strftime("%d-%m-%Y")
        + '.xlsx"'
    )
    return response

#############################################


def how_much(request):
    customer = get_object_or_404(Customer, rut=request.GET.get("rut"))
    data = json.dumps({"amount": customer.balance.balance})
    return HttpResponse(data, content_type="application/json")


@api_view(['GET', 'POST'])
@permission_classes([IsSystemInternalPermission,])
def service_filters_view(request):
    text_cache = str(json.dumps(request.body.decode('UTF-8')))
    _cache = cache.get(text_cache)
    if _cache:
        return JsonResponse(_cache, safe=False)
    data=request.data
    #print(data)

    # Subdivido la data de los filters para saber cual va con cual.
    lista_filtros = data["filters"] #[data.getlist("filters")[i:i+3] for i in range(0, len(data.getlist("filters")), 3)]
    # Inicializo con todos los servicios para luego filtrar.
    qs=Service.objects.all()

    balances_service=[]
    filters_dict: Dict[str, Any] = {}
    excludes_dict: Dict[str, Any] = {}
    #print('lista', lista_filtros)
    for _fitler in lista_filtros:
        filter_name: str = _fitler[0]
        fitler_comparison: str = _fitler[1]
        value: Any = _fitler[2]
        #Este es el unico caso que el atributo no se consigue facilmente.
        if filter_name=="customer__balance":
            if fitler_comparison=="equal":
                bal=Balance.objects.filter(balance=value)
            if fitler_comparison=="gt":
                bal=Balance.objects.filter(balance__gt=value)
            if fitler_comparison=="gte":
                bal=Balance.objects.filter(balance__gte=value)
            if fitler_comparison=="lt":
                bal=Balance.objects.filter(balance__lt=value)
            if fitler_comparison=="lte":
                bal=Balance.objects.filter(balance__lte=value)
            if fitler_comparison=="different":
                bal=Balance.objects.exclude(balance=value)

            # Obteniendo los id para luego filtrarlos.
            customersList = []
            for d in bal:
                customersList.append(d.customer.id)
            if data["criterion"]=="C":
                qs = qs.filter(customer__id__in=customersList)
            else:
                balances_service.append(Service.objects.filter(customer__id__in=customersList))
        elif filter_name=="balance":
            if fitler_comparison=="equal":
                bal=BalanceService.objects.filter(balance=value)
            if fitler_comparison=="gt":
                bal=BalanceService.objects.filter(balance__gt=value)
            if fitler_comparison=="gte":
                bal=BalanceService.objects.filter(balance__gte=value)
            if fitler_comparison=="lt":
                bal=BalanceService.objects.filter(balance__lt=value)
            if fitler_comparison=="lte":
                bal=BalanceService.objects.filter(balance__lte=value)
            if fitler_comparison=="different":
                bal=BalanceService.objects.exclude(balance=value)

            # Obteniendo los id para luego filtrarlos.
            serviceList = []
            for d in bal:
                serviceList.append(d.service.id)
            if data["criterion"]=="C":
                qs = qs.filter(id__in=serviceList)
            else:
                balances_service.append(Service.objects.filter(id__in=serviceList))
        else:
            if fitler_comparison == "equal":
                filters_dict[filter_name] = value
            elif fitler_comparison == "different":
                excludes_dict[filter_name] = value
            else:
                filters_dict[f"{filter_name}__{fitler_comparison}"] = value

    if data["criterion"]=="C":
        if filters_dict:
            qs: QuerySet = qs.filter(**filters_dict)
        if excludes_dict:
            qs: QuerySet = qs.exclude(**excludes_dict)
    else:
        qs=Service.objects.none()
        if filters_dict:
            for i in filters_dict:
                aux_dict: Dict[str, Any] = {}
                aux_dict[i] = filters_dict[i]
                #qs=qs | Service.objects.filter(**aux_dict)
                qs=qs.union(Service.objects.filter(**aux_dict))
                qs=qs.distinct()
        if excludes_dict:
            for i in excludes_dict:
                aux_dict: Dict[str, Any] = {}
                aux_dict[i] = excludes_dict[i]
                #qs=qs | Service.objects.exclude(**aux_dict)
                qs=qs.union(Service.objects.exclude(**aux_dict))
                qs=qs.distinct()
        for f in balances_service:
            qs=qs.union(f)
            qs=qs.distinct()

    #print(qs)
    #print(qs.count())
    
    # Si tiene o no boletas sin pagar.
    unpaid_ballot = data["unpaid_ballot"]
    if unpaid_ballot:
        ub=Service.objects.filter(invoice__paid_on__isnull=True).distinct()
    #else:
    #numbers=Service.objects.filter(invoice__paid_on__isnull=True).distinct().values('number')
    #ub=Service.objects.all().exclude(number__in=numbers)
    #print(ub.count())

    # Includes.
    includes=Service.objects.none()
    has_includes=False
    for i in data["includes"]: #data.getlist("includes"):
        has_includes=True
        includes = includes | Service.objects.filter(number=i)
        
    # Si es conjuncion se intersectan
    if data["criterion"]=="C":
        if unpaid_ballot:
            result_qs=qs.intersection(ub)
        else:
            result_qs=qs
        if has_includes:
            result_qs=result_qs.intersection(includes)
        # Se eliminan los repetidos.
        result_qs=result_qs.distinct()
    else:
        if unpaid_ballot:
            # Si es disyuncion se unen.
            result_qs=qs.union(ub)
        else:
            result_qs=qs
        if has_includes:
            result_qs=result_qs | includes
        # Se eliminan los repetidos.
        result_qs=result_qs.distinct()

    #print(result_qs.count())

    # Excluir.
    for i in data["excludes"]: # data.getlist("excludes"):
        result_qs = result_qs.exclude(number=i)
    
    #print(qs.count())

    list_to_send=[]
    for service in result_qs:
        list_to_send.append(
            {
            "service__technology_kind":str(service.get_technology_kind_display()),
            "service__number":str(service.number),
            "service__due_day":str(service.due_day),
            "service__status": str(service.get_status_display()),
            "service__composite_address":str(service.composite_address),
            "service__street":str(service.street),
            "service__house_number":str(service.house_number),
            "service__apartment_number":str(service.apartment_number),
            "service__tower":str(service.tower),
            "service__commune": "" if service.node is None else str(service.node.commune),
            "service__location":str(service.location),
            "service__activity":str(service.customer.commercial_activity),
            "node__code": "" if service.node is None else str(service.node.code),
            "node__address": "" if service.node is None else str(service.node.address),
            "node__house_number": "" if service.node is None else str(service.node.house_number),
            "node__commune": "" if service.node is None else str(service.node.commune),
            "node__gpon":  "" if service.node is None else ("Sí" if service.node.gpon else "No"),
            "node__gepon": "" if service.node is None else ("Sí" if service.node.gepon else "No"),
            "node__pon_port": "" if service.node is None else str(service.node.pon_port),
            "node__box_location": "" if service.node is None else str(service.node.box_location),
            "node__splitter_location": "" if service.node is None else str(service.node.splitter_location),
            "node__expected_power": "" if service.node is None else str(service.node.expected_power),
            "node__towers": "" if service.node is None else str(service.node.towers),
            "node__apartments": "" if service.node is None else str(service.node.apartments),
            "node__phone": "" if service.node is None else str(service.node.phone),
            "plan__name": str(service.plan.name),
            "plan__price": str(service.plan.price),
            "plan__category": str(service.plan.get_category_display()),
            "plan__uf":  "Sí" if service.plan.uf else "No",
            "plan__active": "Sí" if service.plan.active else "No",    
            "customer__rut":str(service.customer.rut),
            "customer__name": str(service.customer.name),
            "customer__first_name": str(service.customer.first_name),
            "customer__last_name": str(service.customer.last_name), ###NUEVO###
            "customer__email": str(service.customer.email),
            "customer__phone": str(service.customer.phone),
            "customer__default_due_day":str(service.customer.default_due_day),
            "customer__street":str(service.customer.street),
            "customer__house_number":str(service.customer.house_number),
            "customer__apartment_number":str(service.customer.apartment_number),
            "customer__tower":str(service.customer.tower),
            "customer__kind":  str(service.customer.get_kind_display()),
            "customer__id": str(service.customer.id),
        }
        )
    cache.set(text_cache, list_to_send, 3600*1)
    return JsonResponse(list_to_send, safe=False)

@api_view(['GET', 'POST'])
@permission_classes([IsSystemInternalPermission,])
def service_filters_dict(request):
    text_cache = str(json.dumps(request.body.decode('UTF-8')))+str("dict")
    #print(text_cache)
    _cache = cache.get(text_cache)
    if _cache:
        return JsonResponse(_cache, safe=False)
    
    data=request.data
    #print(data)

    # Subdivido la data de los filters para saber cual va con cual.
    lista_filtros = data["filters"] #[data.getlist("filters")[i:i+3] for i in range(0, len(data.getlist("filters")), 3)]
    # Inicializo con todos los servicios para luego filtrar.
    qs=Service.objects.all()

    balances_service=[]
    filters_dict: Dict[str, Any] = {}
    excludes_dict: Dict[str, Any] = {}
    #print('lista', lista_filtros)
    for _fitler in lista_filtros:
        filter_name: str = _fitler[0]
        fitler_comparison: str = _fitler[1]
        value: Any = _fitler[2]
        #Este es el unico caso que el atributo no se consigue facilmente.
        if filter_name=="customer__balance":
            if fitler_comparison=="equal":
                bal=Balance.objects.filter(balance=value)
            if fitler_comparison=="gt":
                bal=Balance.objects.filter(balance__gt=value)
            if fitler_comparison=="gte":
                bal=Balance.objects.filter(balance__gte=value)
            if fitler_comparison=="lt":
                bal=Balance.objects.filter(balance__lt=value)
            if fitler_comparison=="lte":
                bal=Balance.objects.filter(balance__lte=value)
            if fitler_comparison=="different":
                bal=Balance.objects.exclude(balance=value)

            # Obteniendo los id para luego filtrarlos.
            customersList = []
            for d in bal:
                customersList.append(d.customer.id)
            if data["criterion"]=="C":
                qs = qs.filter(customer__id__in=customersList)
            else:
                balances_service.append(Service.objects.filter(customer__id__in=customersList))
        elif filter_name=="balance":
            if fitler_comparison=="equal":
                bal=BalanceService.objects.filter(balance=value)
            if fitler_comparison=="gt":
                bal=BalanceService.objects.filter(balance__gt=value)
            if fitler_comparison=="gte":
                bal=BalanceService.objects.filter(balance__gte=value)
            if fitler_comparison=="lt":
                bal=BalanceService.objects.filter(balance__lt=value)
            if fitler_comparison=="lte":
                bal=BalanceService.objects.filter(balance__lte=value)
            if fitler_comparison=="different":
                bal=BalanceService.objects.exclude(balance=value)

            # Obteniendo los id para luego filtrarlos.
            serviceList = []
            for d in bal:
                serviceList.append(d.service.id)
            if data["criterion"]=="C":
                qs = qs.filter(id__in=serviceList)
            else:
                balances_service.append(Service.objects.filter(id__in=serviceList))
        else:
            if fitler_comparison == "equal":
                filters_dict[filter_name] = value
            elif fitler_comparison == "different":
                excludes_dict[filter_name] = value
            else:
                filters_dict[f"{filter_name}__{fitler_comparison}"] = value

    if data["criterion"]=="C":
        if filters_dict:
            qs: QuerySet = qs.filter(**filters_dict)
        if excludes_dict:
            qs: QuerySet = qs.exclude(**excludes_dict)
    else:
        qs=Service.objects.none()
        if filters_dict:
            for i in filters_dict:
                aux_dict: Dict[str, Any] = {}
                aux_dict[i] = filters_dict[i]
                qs=qs.union(Service.objects.filter(**aux_dict))
                qs=qs.distinct()
        if excludes_dict:
            for i in excludes_dict:
                aux_dict: Dict[str, Any] = {}
                aux_dict[i] = excludes_dict[i]
                #qs=qs | Service.objects.exclude(**aux_dict)
                qs=qs.union(Service.objects.exclude(**aux_dict))
                qs=qs.distinct()
        for f in balances_service:
            qs=qs.union(f)
            qs=qs.distinct()

    #print(qs)
    #print(qs.count())
    
    # Si tiene o no boletas sin pagar.
    unpaid_ballot = data["unpaid_ballot"]
    if unpaid_ballot:
        ub=Service.objects.filter(invoice__paid_on__isnull=True).distinct()
    #else:
    #numbers=Service.objects.filter(invoice__paid_on__isnull=True).distinct().values('number')
    #ub=Service.objects.all().exclude(number__in=numbers)
    #print(ub.count())

    # Includes.
    includes=Service.objects.none()
    has_includes=False
    for i in data["includes"]: #data.getlist("includes"):
        has_includes=True
        includes = includes | Service.objects.filter(number=i)
        
    # Si es conjuncion se intersectan
    if data["criterion"]=="C":
        if unpaid_ballot:
            result_qs=qs.intersection(ub)
        else:
            result_qs=qs
        if has_includes:
            result_qs=result_qs.intersection(includes)
        # Se eliminan los repetidos.
        result_qs=result_qs.distinct()
    else:
        if unpaid_ballot:
            # Si es disyuncion se unen.
            result_qs=qs.union(ub)
        else:
            result_qs=qs
        if has_includes:
            result_qs=result_qs | includes
        # Se eliminan los repetidos.
        result_qs=result_qs.distinct()

    #print(result_qs.count())

    # Excluir.
    for i in data["excludes"]: # data.getlist("excludes"):
        result_qs = result_qs.exclude(number=i)
    
    #print(qs.count())

    list_to_send={}
    for service in result_qs:
        list_to_send[service.number]=str(service.customer.rut)
    cache.set(text_cache, list_to_send, 3600*1)
    return JsonResponse(list_to_send, safe=False)

@api_view(['GET', 'POST'])
@permission_classes([IsSystemInternalPermission,])
def service_filters_count(request):
    text_cache = str(json.dumps(request.body.decode('UTF-8')))+str("count")
    _cache = cache.get(text_cache)
    if _cache:
        return JsonResponse(_cache, safe=False)
    
    data=request.data
    #print(data)

    # Subdivido la data de los filters para saber cual va con cual.
    lista_filtros = data["filters"] #[data.getlist("filters")[i:i+3] for i in range(0, len(data.getlist("filters")), 3)]

    # Inicializo con todos los servicios para luego filtrar.
    qs=Service.objects.all()

    balances_service=[]
    filters_dict: Dict[str, Any] = {}
    excludes_dict: Dict[str, Any] = {}

    for _fitler in lista_filtros:
        filter_name: str = _fitler[0]
        fitler_comparison: str = _fitler[1]
        value: Any = _fitler[2]
        #Este es el unico caso que el atributo no se consigue facilmente.
        if filter_name=="customer__balance":
            if fitler_comparison=="equal":
                bal=Balance.objects.filter(balance=value)
            if fitler_comparison=="gt":
                bal=Balance.objects.filter(balance__gt=value)
            if fitler_comparison=="gte":
                bal=Balance.objects.filter(balance__gte=value)
            if fitler_comparison=="lt":
                bal=Balance.objects.filter(balance__lt=value)
            if fitler_comparison=="lte":
                bal=Balance.objects.filter(balance__lte=value)
            if fitler_comparison=="different":
                bal=Balance.objects.exclude(balance=value)

            # Obteniendo los id para luego filtrarlos.
            customersList = []
            for d in bal:
                customersList.append(d.customer.id)
            if data["criterion"]=="C":
                qs = qs.filter(customer__id__in=customersList)
            else:
                balances_service.append(Service.objects.filter(customer__id__in=customersList))
        elif filter_name=="balance":
            if fitler_comparison=="equal":
                bal=BalanceService.objects.filter(balance=value)
            if fitler_comparison=="gt":
                bal=BalanceService.objects.filter(balance__gt=value)
            if fitler_comparison=="gte":
                bal=BalanceService.objects.filter(balance__gte=value)
            if fitler_comparison=="lt":
                bal=BalanceService.objects.filter(balance__lt=value)
            if fitler_comparison=="lte":
                bal=BalanceService.objects.filter(balance__lte=value)
            if fitler_comparison=="different":
                bal=BalanceService.objects.exclude(balance=value)

            # Obteniendo los id para luego filtrarlos.
            serviceList = []
            for d in bal:
                serviceList.append(d.service.id)
            if data["criterion"]=="C":
                qs = qs.filter(id__in=serviceList)
            else:
                balances_service.append(Service.objects.filter(id__in=serviceList))
        else:
            if fitler_comparison == "equal":
                filters_dict[filter_name] = value
            elif fitler_comparison == "different":
                excludes_dict[filter_name] = value
            else:
                filters_dict[f"{filter_name}__{fitler_comparison}"] = value

    if data["criterion"]=="C":
        if filters_dict:
            qs: QuerySet = qs.filter(**filters_dict)
        if excludes_dict:
            qs: QuerySet = qs.exclude(**excludes_dict)
    else:
        qs=Service.objects.none()
        if filters_dict:
            for i in filters_dict:
                aux_dict: Dict[str, Any] = {}
                aux_dict[i] = filters_dict[i]
                #qs=qs | Service.objects.filter(**aux_dict)
                qs=qs.union(Service.objects.filter(**aux_dict))
                qs=qs.distinct()
        if excludes_dict:
            for i in excludes_dict:
                aux_dict: Dict[str, Any] = {}
                aux_dict[i] = excludes_dict[i]
                #qs=qs | Service.objects.exclude(**aux_dict)
                qs=qs.union(Service.objects.exclude(**aux_dict))
                qs=qs.distinct()
        for f in balances_service:
            qs=qs.union(f)
            qs=qs.distinct()

    #print(qs)
    #print(qs.count())
    
    # Si tiene o no boletas sin pagar.
    unpaid_ballot = data["unpaid_ballot"]
    if unpaid_ballot:
        ub=Service.objects.filter(invoice__paid_on__isnull=True).distinct()
    #else:
    #numbers=Service.objects.filter(invoice__paid_on__isnull=True).distinct().values('number')
    #ub=Service.objects.all().exclude(number__in=numbers)
    #print(ub.count())

    # Includes.
    includes=Service.objects.none()
    has_includes=False
    for i in data["includes"]: #data.getlist("includes"):
        has_includes=True
        includes = includes | Service.objects.filter(number=i)
        
    # Si es conjuncion se intersectan
    if data["criterion"]=="C":
        if unpaid_ballot:
            result_qs=qs.intersection(ub)
        else:
            result_qs=qs
        if has_includes:
            result_qs=result_qs.intersection(includes)
        # Se eliminan los repetidos.
        result_qs=result_qs.distinct()
    else:
        if unpaid_ballot:
            # Si es disyuncion se unen.
            result_qs=qs.union(ub)
        else:
            result_qs=qs
        if has_includes:
            result_qs=result_qs | includes
        # Se eliminan los repetidos.
        result_qs=result_qs.distinct()

    #print(result_qs.count())

    # Excluir.
    for i in data["excludes"]: # data.getlist("excludes"):
        result_qs = result_qs.exclude(number=i)

    #print(qs.count())

    context={"count": result_qs.count()}
    cache.set(text_cache, context, 3600*1)
    return JsonResponse(context, safe=False)

@api_view(['POST'])
@permission_classes([IsSystemInternalPermission,])
def create_customerservice_view(request):
    data=request.data
    try:
        serializer = CustomerServiceSerializer(data,context={'request': request})
        data_serializer=serializer.data
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)
    
    try:
        # Verificando que pasaron numeros correctos.
        Operator.objects.get(id=data_serializer['operator'])
        Plan.objects.get(id=data_serializer['plan'])
        if data_serializer['commercial_activity'] is None:
            commercial=None
        else: 
            commercial=CommercialActivity.objects.get(id=data_serializer['commercial_activity'])
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)
    
    customer=Customer.objects.filter(rut=data_serializer['rut'])
    if customer.count()==0:
        company=Operator.objects.get(id=data_serializer['operator']).company
        try:
            # Creando el nuevo cliente
            new_customer = Customer.objects.create(
                send_email = data_serializer['send_email'],
                send_snail_mail = data_serializer['send_snail_mail'],
                rut = data_serializer['rut'],
                first_name = data_serializer['first_name'],
                last_name = data_serializer['last_name'],
                kind = data_serializer['kind'],
                commercial_activity = commercial,
                email = data_serializer['email'],
                street = data_serializer['street'],
                house_number = data_serializer['house_number'],
                apartment_number = data_serializer['apartment_number'],
                tower = data_serializer['tower'],
                location = data_serializer['location'],
                phone = data_serializer['phone'],
                default_due_day = data_serializer['default_due_day'],
                unified_billing = data_serializer['unified_billing'],
                full_process = data_serializer['unified_billing'],
                notes = data_serializer['notes'],
                birth_date = data_serializer['birth_date'],
                first_category_activity = data_serializer['first_category_activity'],
                company= company,
            )
        except Exception as e:
            print(str(e))
            return JsonResponse([str(e)], safe=False)
    else:
        #print("Ya estaba creado")
        new_customer=Customer.objects.get(rut=data_serializer['rut'])

    operator=Operator.objects.get(id=data_serializer['operator'])
    try:
        # Creando el nuevo servicio.
        new_service = Service.objects.create(
            number=Service.objects.filter(number__lt=99000, operator=operator).aggregate(
                Max("number")
            )["number__max"]
            + 1,
            customer=new_customer,
            document_type=data_serializer['document_type'],
            street=new_customer.street,
            house_number=new_customer.house_number,
            apartment_number=new_customer.apartment_number,
            tower=new_customer.tower,
            location=new_customer.location,
            plan= Plan.objects.get(id=data_serializer['plan']),
            due_day=5,
            activation_fee=0,
            status=4,
            technology_kind=data_serializer['technology_kind'],
            operator= Operator.objects.get(id=data_serializer['operator']),
            customer_type=1,
        )
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)
    
    # Para retornar la info del nuevo servicio y el nuevo cliente
    content={
        #'serializer': data_serializer,
        'service': ServiceSerializer(
                    Service.objects.get(id=new_service.id),
                    context={'request': request}).data,
        'customer':  CustomerSerializer(
                    Customer.objects.get(id=new_customer.id),
                    context={'request': request}).data
        }
    # Se coloca que el status es de creado.
    return  Response(content, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes([IsSystemInternalPermission,])
def create_payments_pomaire_view(request):
    from constance import config
    data=request.data
    # print(data)
    try:
        serializer = PaymentPomaireSerializer(data,context={'request': request})
        # print(serializer.data)
        validated_data=serializer.data
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)
    
    invoices = validated_data["invoices"]
    invoices_list = invoices.split(",")
        
    cliente=Customer.objects.filter(payorrut__rut=validated_data["customer_rut"])
    #clientes=Customer.objects.filter(payorrut__rut=validated_data["customer_rut"]).values_list("id")
    #print(clientes)

    # Fix me
    try:
        # Caso que solo tiene un cliente asociado
        if cliente.count()==1:
            new_payment = Payment.objects.create(
                    customer = cliente[0],
                    paid_on = datetime.now(),
                    deposited_on = validated_data['deposited_on'],
                    amount = validated_data["amount"],
                    kind = validated_data["kind"],
                    transfer_id = validated_data["transfer_id"],
                    comment = validated_data["comment"],
                    operation_number = validated_data["operation_number"],
                    operator = Operator.objects.get(id=2)
            )
            content={
            } 
            services=Service.objects.none()
            # Asociando las boletas.
            for i in invoices_list:
                #print(i)
                inv=Invoice.objects.get(folio=i, customer= cliente[0])
                # Marca como pagada la boleta.
                inv.paid_on=datetime.now()
                inv.save()
                try:
                    # Revisando si existe una prorroga de esa boleta.
                    if not(inv.customer.flag is None):
                        if "prorrogas" in inv.customer.flag:
                            if str(inv.id) in inv.customer.flag['prorrogas']:
                                fecha_actual=datetime.now()
                                fecha=inv.customer.flag['prorrogas'][str(i)]['date']
                                if dateutil.parser.isoparse(fecha)>=fecha_actual:
                                    inv.customer.flag['prorrogas'][str(i)]['paid_on_time']=True
                                    inv.customer.flag['prorrogas'][str(i)]['paid_on']=datetime.now().isoformat()
                                else:
                                    inv.customer.flag['prorrogas'][str(i)]['paid_on_time']=False
                                    inv.customer.flag['prorrogas'][str(i)]['paid_on']=datetime.now().isoformat()
                                inv.customer.save()
                except:
                    pass

                services=services | inv.service.all()
                new_payment.invoice.add(inv)
                new_payment.save()

            # Asociando los servicios.
            def_services=services.distinct()
            for i in def_services:
                #print(i)
                new_payment.service.add(i)
                new_payment.save()
                    
            # Logica para sentinel
            for i in def_services:
                # Si el status no es activo, verifico el balance.
                if not(i.status==1):
                    bal=BalanceService.objects.get(service=i)
                    # Si ya pago la deuda se debe activar el servicio.
                    if bal.balance>=0:
                        # Se cambia el estado del servicio.
                        i.status=1
                        try:
                            i.save()
                        except Exception as e:
                            error = {'message': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
                            print(error)
                        config_slack=ConfigOperator.objects.filter(operator=i.operator, name="Slack")
                        if config_slack.count()==1:
                            if "slack_reconectar_servicio" in config_slack[0].flag:
                                # Envio de info al canal de Slack
                                data = {
                                            "text":"Servicio que necesita activar/contectar Red en Matrix",
                                            "attachments": [
                                                {
                                                    "color": "#36A64F",
                                                    "fields": [
                                                        {
                                                            "title": "Monto de pago",
                                                            "value": validated_data["amount"],
                                                            "short": True
                                                        },
                                                        {
                                                            "title": "Nombre del cliente",
                                                            "value": str(cliente[0].name),
                                                            "short": True
                                                        },
                                                        {
                                                            "title": "Url del balance del servicio",
                                                            "value": getattr(config, "matrix_server")+"/services/" +str(i.number)+"/billing/",
                                                            "short": True
                                                        },
                                                        {
                                                            "title": "Url de la pestaña de red del servicio",
                                                            "value": getattr(config, "matrix_server")+"/services/" +str(i.number)+"/network/" ,
                                                            "short": True
                                                        }
                                                    ]
                                                },
                                                {
                                                    "fallback": "Activar servicio",
                                                    "title": "Activar servicio",
                                                    "callback_id": "activate_service_1234_xyz",
                                                    "color": "#3AA3E3",
                                                    "attachment_type": "default",
                                                    "actions": [
                                                        {
                                                        "type": "button",
                                                        "text": {
                                                            "type": "Activar",
                                                            "text": "Activar"
                                                            },
                                                        "url": getattr(config, "matrix_server")+"/services/" +str(i.id)+"/activate_pome/"
                                                        }
                                                    ],
                                                }
                                            ],
                                        }
                                url = config_slack[0].flag["slack_reconectar_servicio"]
                                #url = 'https://hooks.slack.com/services/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'
                                r = requests.post(url, json = data)

        else:
            # Caso son varios clientes asociados
            content={} 
            pass
        return Response(content, status=status.HTTP_201_CREATED) 
    except Exception as e:
        error = {'message': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
        print(error)
        return JsonResponse([str(error)], safe=False)


def activate_service_pomaire_view(request, slug):
    service = get_object_or_404(Service, pk=slug)

    activate_client(service)
    service.seen_connected = True
    service.save()
    NetworkStatusChange.objects.create(
        service=service, new_state="activado", agent=request.user
    )

    ctx={"mensaje":"Se ha activado el servicio."}
    return render(request,"customers/mensaje.html", context=ctx)

@xframe_options_exempt
def iframe_planorder_matrix(request, slug):
    service = get_object_or_404(Service, pk=slug)
    form = PlanOrderCreateForm()
    create_order=False
    if request.method == "POST":
        if request.POST.get("subject"):
            form=PlanOrderCreateForm(request.POST)
            if form.is_valid():
                order = form.save(commit=False)
                order.service = service
                order.save()
                create_order=True
        else:
            # form = PlanOrderScheduleForm(request.POST)
            import dateutil.parser as dt
            ctx={"mensaje":"Problema con iclass, no se puede agendar en este horario."}
            return render(request,"customers/mensaje.html", context=ctx)
            start = timezone.localtime(dt.parse(request.POST["start"]))
            end = timezone.localtime(
                dt.parse(request.POST["end"]) + timezone.timedelta(minutes=1)
            )

            req_start, req_end = anon_tuple(start, end)

            if order.start:
                rr = order.reschedule_iclass_order(req_start, req_end)
            else:
                rr = order.create_iclass_order(req_start, req_end)

            agenda = rr["resultadoAgendamento"]
            try:
                plan = agenda["atendimentoPlanejado"]
                new_tuple = anon_tuple(plan["inicio"], plan["fim"])
                order.start = new_tuple[0]
                order.end = new_tuple[1]
                order.team = agenda["equipe"]
                order.save()
            except Exception as e:
                print(e)
                ctx={"mensaje":"Problema con iclass, no se puede agendar en este horario."}
                return render(request,"customers/mensaje.html", context=ctx)
            '''
            previous_orders = PlanOrder.objects.filter(number = service.number)
            is_active = False
            current_time = timezone.now()
            for po in previous_orders:
                print("po service, po start, po end", po.service.number, po.start, po.end)
                if current_time < po.end:
                    is_active = True
                    break
            
            if is_active == True:
                ctx = {"mensaje": "No se puede agendar, ya hay una orden de servicio activa"}
                return render(request, "customers/mensaje.html", context = ctx)
            '''
            if (start != order.start) or (end != order.end):
                # signal this to the user
                ctx={"mensaje":"Verificar período de la orden de servicio: se agendó en horario diferente debido a disponibilidad."}
                return render(request,"customers/mensaje.html", context=ctx)
            
            
            ctx={"mensaje":"Se ha agendado una nueva orden de servicio."}
            return render(request,"customers/mensaje.html", context=ctx)
        
        if create_order:
            order = get_object_or_404(PlanOrder, pk=order.pk)
            #order = get_object_or_404(PlanOrder, pk="pk")
            avx = order.get_availabilities()

            form = PlanOrderScheduleForm((), initial={"kind": order.kind})
            return render(
                request,
                "customers/iframeplanorderschedule.html",
                dict(
                    customer=order.service.customer,
                    service=order.service,
                    object=order,
                    form=form,
                    avx=avx,
                    cancel_url=reverse("service-plan-orders", args=[order.service.slug]),
                ),
            )
            #return iframe_plan_order_schedule(request, order.pk)
            return reverse(
                "iframe-planorder-schedule",
                kwargs={"slug": service.number, "pk": order.pk},
            )
    #if request.method == "POST" and request.POST.get("Ver disponibilidad"):
    return render(request, "customers/iframeplanorder.html")


@api_view(['POST'])
@permission_classes([IsSystemInternalPermission,])
def retire_equipment_service_view(request):
    data=request.data
    # print(data)
    try:
        serializer = EquipmentRemovalSerializer(data,context={'request': request})
        # print(serializer.data)
        data_serializer=serializer.data
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)

    service_id=data_serializer['service_id']
    service=Service.objects.get(id=service_id)

    prev_status=service.status

    ctx={
        "date": timezone.now(),
        "object": service,
        "service": service,
        "customer": service.customer,
        "kind": service.customer.kind,
        "description": "Retiro de Equipo",
        "class_number": 4,
        "person": data_serializer['person'],
        "status": data_serializer['status'],
        "pick_up_date": data_serializer['pick_up_date'],
        "pick_up_time": data_serializer['pick_up_time']
        }
    filename = "retiro_de_equipo.pdf"
    template_name_pdf = "customers/service_generic_html_to_pdf.html"
    #template_name = "customers/service_savepdf_form.html"

    template = get_template(template_name_pdf)

    pdf = render_pdf_from_template(template, None, None, context=ctx)

    bb = io.BytesIO(pdf)
    ff = File(filename)
    ff.file = bb
    doc = Document.objects.create(
            content_object=ctx["service"],
            description=ctx["description"],
            agent=User.objects.get(username="multifiber"),
        )
    doc.file.save(filename, ff)
    doc.save()

    pdf64 = base64.b64encode(pdf).decode("UTF-8")

    # Info que se envia en la respuesta.
    content = { "service": ServiceSerializer(
                    Service.objects.get(id=service.id),
                    context={'request': request}).data,
                "files": [pdf64], 
                "previous_state": prev_status}
    return Response(content, status=status.HTTP_201_CREATED) 

@api_view(['POST'])
@permission_classes([IsSystemInternalPermission,])
def indicators_pomaire_view(request):
    data=request.data
    # print(data)
    try:
        serializer = IndicatorsSerializer(data,context={'request': request})
        # print(serializer.data)
        data_serializer=serializer.data
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)

    # Intenta traer el ultimo valor del dolar.
    try:
        dolar = float(Indicators.objects.filter(kind=3).distinct("kind").values("kind", "created_at", "value").first()["value"])
    except:
        dolar=None

    # Intenta traer el ultimo valor de uf.
    try:
        uf = float(Indicators.objects.filter(kind=1).distinct("kind").values("kind", "created_at", "value").first()["value"])
    except:
        uf=None

    # Si el valor que es envia no es nulo y es distinto al ya registrado, 
    # lo guarda en Matrix.
    if data_serializer["dolar"]!=None:
        if dolar!=float(data_serializer["dolar"]):
            new_dolar=Indicators.objects.create(kind=3, value=data_serializer["dolar"])

    # Si el valor que es envia no es nulo y es distinto al ya registrado, 
    # lo guarda en Matrix.
    if data_serializer["uf"]!=None:
        if uf!=float(data_serializer["uf"]):
            new_uf=Indicators.objects.create(kind=1, value=data_serializer["uf"])

    # Info que se envia en la respuesta.
    content = {}
    return Response(content, status=status.HTTP_201_CREATED) 

#############################################
@api_view(['GET'])
@permission_classes([IsSystemInternalPermission,])
def customer_filter(request):

    result = []
    criterion = request.GET['criterion']
    search = request.GET['search']

    if criterion == 'service':
        response_service = []

        if search.isdigit():
            value_cache = cache.get('service_data_{}'.format(search))
            if value_cache:
                result.append(
                    {
                        'id':value_cache.get('rut'),
                        'text':value_cache.get('text_service_1')
                    } 
                )
            else:
                response_service = Service.objects.filter(number=int(search))
                if response_service:
                    response_service[0].set_cache_data()
                    text_service_1 = '{} - {} - {}'.format(
                        response_service[0].customer.rut, response_service[0].customer.name, 
                        response_service[0].customer.composite_address
                    )
                    result.append(
                        {
                            'id':response_service[0].customer.rut,
                            'text': text_service_1
                        } 
                    )
    else:
        # Hacer esta consula por SQL Jose Tomas y con el metodo only
        response = ''

        customers = Customer.objects.all().only('rut', 'first_name', 'last_name', 'street', 'house_number', 'apartment_number', 'tower')
        
        if criterion == 'rut':
            rut_regex = "^" + "\.*".join(list(search))
            response = customers.filter(rut__iregex=rut_regex)
        elif criterion == 'name':
            q_objects = Q()
            splitted_name_queries = [
                Q(first_name__unaccent__icontains=term) for term in search.split(" ")
            ]
            q_name_objects = Q()
            for q in splitted_name_queries:
                q_name_objects.add(q, Q.AND)
                
            q_objects |= q_name_objects

            splitted_last_name_queries = [
                Q(last_name__unaccent__icontains=term) for term in search.split(" ")
            ]
            q_last_name_objects = Q()
            for q in splitted_last_name_queries:
                q_last_name_objects.add(q, Q.AND)
            
            q_objects |= q_last_name_objects
            response = customers.filter(q_objects)
        elif criterion == 'composite_address':
            customers = customers.all().annotate(
                db_composite_address=Concat(
                    "street",
                    Value(" "),
                    "house_number",
                    Value(" dpto "),
                    "apartment_number",
                    Value(" torre "),
                    "tower",
                ))
            response = customers.filter(db_composite_address__unaccent__icontains=search)
        
        if response: 
            text = '{} - {} - {}'.format(
                            response[0].rut, response[0].name, 
                            response[0].composite_address )
            result.append(
                            {
                                'id':response[0].rut,
                                'text': text
                            } 
                        )
        
    return JsonResponse(result, safe=False)

def verify_if_can_ativate(service):
    bal=BalanceService.objects.get(service=service)
    # Si ya pago la deuda se debe activar el servicio.
    if bal.balance>=0: 
        return True
    else:
        # Listado de tipos de boleta.
        object_list = [1, 2, 6, 7, 8]
        unpaid_invoice=Invoice.objects.filter(
            paid_on__isnull=True, service=service, kind__in=object_list 
        )
        #print(unpaid_invoice.count())
        # Deberia tener lo del rango de aceptacion.
        if  unpaid_invoice.count()<=getattr(config, "number_of_unpaid_invoice") and unpaid_invoice.count()!=0:
            can_activate=True
            if not(service.customer.flag is None):
                for inv in unpaid_invoice:
                    # Si alguna de las boletas no tiene prorroga, no se activa.
                    if "prorrogas" in inv.customer.flag:
                        if not(str(inv.id) in inv.customer.flag['prorrogas']):
                            can_activate=False
                return can_activate
            else:
                # Si no tiene prorroga no puede activar
                return False
        return False

@api_view(['POST'])
@permission_classes([IsSystemInternalPermission,])
def activate_drop_service_iris_view(request, slug):
    service = get_object_or_404(Service, pk=slug)

    data=request.data
    # print(data)
    try:
        action=data["action"]
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)

    if action=="activar":
        content={"mensaje":"No se pudo activar el servicio."}
        # Si el status no es activo, verifico el balance.
        if not(service.status==1):
            # Si ya pago la deuda se debe activar el servicio.
            if verify_if_can_ativate(service):
                activate_client(service)
                service.seen_connected = True
                service.save()
                NetworkStatusChange.objects.create(
                    service=service, new_state="activado", agent=User.objects.get(username="multifiber")
                )
                content={"mensaje":"Se ha activado el servicio."}
                # Se cambia el estado del servicio.
                service.status=1
                try:
                    service.save()
                except Exception as e:
                    error = {'message': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
                    print(error)
        else:
            # Si ya pago la deuda se debe activar el servicio.
            if verify_if_can_ativate(service):
                activate_client(service)
                service.seen_connected = True
                service.save()
                NetworkStatusChange.objects.create(
                    service=service, new_state="activado", agent=User.objects.get(username="multifiber")
                )
                content={"mensaje":"Se ha activado el servicio."}
        return Response(content, status=status.HTTP_201_CREATED) 
    elif action=="cortar":
        content={"mensaje":"No se pudo cortar el servicio."}
        if service.status==1:
            # Se cambia el estado del servicio.
            service.status=2
            drop_client(service)
            service.seen_connected = False
            service.save()
            NetworkStatusChange.objects.create(
                service=service, new_state="cortado", agent=User.objects.get(username="multifiber")
            )
            content={"mensaje": "Se ha cortado el servicio."}
            try:
                service.save()
                # Enviar notificacion de por retirar.
                data ={
                    "operator": service.operator.id,
                    "rut": str(service.customer.rut),
                    "service": int(service.number),
                    "serial": None,
                    "iclass_id": None,
                    "status": 0,
                    "status_agendamiento": None,
                    "status_storage": None,
                    "status_ti": None,
                    "slack_thread": None,
                    "previous_state": self.status,
                    "creator": None,
                    "updater": None,
                    "agent_ti": None,
                    "agent_sac": None,
                    "agent_storage": None
                }

                url = getattr(config, "iris_server") + getattr(config, "iris_notify_remove_equipament")
                # print (url)
                # print(data)

                try:
                    send = requests.post(
                        url=url,
                        json=data,
                        headers={"Authorization": getattr(config, "invoices_to_iris_task_token")},
                        timeout=60,
                    )
                    print(send)
                    print(send.text)
                    if "Error" in send.json() or "error" in send.json():
                        print("ERROR")
                except Exception as e:
                    print("e",e)
                
            except Exception as e:
                error = {'message': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
                print(error)
        else:
            drop_client(service)
            service.seen_connected = False
            service.save()
            NetworkStatusChange.objects.create(
                service=service, new_state="cortado", agent=User.objects.get(username="multifiber")
            )
            content={"mensaje": "Se ha cortado el servicio."}
        return Response(content, status=status.HTTP_201_CREATED) 
    else:
        content={"error": "No paso una opcion valida de action."}
        return Response(content, status=status.HTTP_400_BAD_REQUEST) 

@api_view(['GET'])
@permission_classes([IsSystemInternalPermission,])
def get_prorroga(request, pk):
    try:
        inv=Invoice.objects.get(id=pk)
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)

    # Si no tiene nada es que es la primera.
    if inv.customer.flag is None:
        content={}
    else:
        content=inv.customer.flag
    return Response(content, status=status.HTTP_200_OK) 

@api_view(['GET'])
@permission_classes([IsSystemInternalPermission,])
def get_prorroga_service(request, pk):
    try:
        inv=Service.objects.get(id=pk)
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)

    # Si no tiene nada es que es la primera.
    if inv.customer.flag is None:
        content={}
    else:
        content=inv.customer.flag
    return Response(content, status=status.HTTP_200_OK) 

@api_view(['POST'])
@permission_classes([IsSystemInternalPermission,])
def create_prorroga_iris_view(request):
    data=request.data
    # print(data)
    try:
        serializer = ProrrogaIrisSerializer(data,context={'request': request})
        #print(serializer.data)
        validated_data=serializer.data
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)
    
    invoices: List[str] = validated_data.get('invoices', [])

    ctx={"boletas": ""}
    content={}
    clientes={}
    for i in invoices:
        try:
            inv=Invoice.objects.get(id=i)
            # Si no se ha guardado el valor de prorroga en el json.
            if not str(inv.customer.id) in clientes:
                # Si no tiene nada es que es la primera.
                if inv.customer.flag is None:
                    clientes[str(inv.customer.id)]=0
                else:
                    # Para ver si ya ha tenido prorroga.
                    if "counts_prorroga" in inv.customer.flag:
                        clientes[str(inv.customer.id)]=inv.customer.flag['counts_prorroga']
                    else:
                        # Si no tiene nada es que es la primera.
                        clientes[str(inv.customer.id)]=0
        except Exception as e:
            print(e)
            # Los invoice que no encuentra.
            ctx["boletas"]=ctx["boletas"]+"Not Found invoice "+str(i)+". "
            content["boletas"]=ctx["boletas"]

    if len(clientes)==0:
        content={"error": "Las boletas no tienen ningun cliente asociado"} 
        return Response(content, status=status.HTTP_400_BAD_REQUEST) 

    for i in invoices:
        #print(i)
        try:
            inv=Invoice.objects.get(id=i)
            # Si no tiene nada en flag.
            if inv.customer.flag is None:
                #print("Es null")
                inv.customer.flag={
                    'prorrogas':
                        {str(i): {
                            'date': validated_data['date'],
                            'created_at': datetime.now().isoformat(), 
                            'paid_on_time': None, 
                            'paid_on': None,
                            'extension_number':0}
                            },
                    'counts_prorroga': 1,
                    'distribucion': {'0': [str(i)]}
                    }
                inv.customer.save()
            else:
                # Si ya ha tenido prorrogas.
                if "prorrogas" in inv.customer.flag:
                    #print("PASO AQUI")
                    # Traigo el valor de prorroga segun el cliente.
                    count_of_prorroga=clientes[str(inv.customer.id)]
                    # Guardo la info de la nueva prorroga.
                    inv.customer.flag['prorrogas'][str(i)]={
                        'date': validated_data['date'],
                        'created_at': datetime.now().isoformat(), 
                        'paid_on_time': None, 
                        'paid_on': None, 
                        'extension_number':count_of_prorroga}
                    inv.customer.save()
                    # Para ver si la prorroga es de varios invoice al mismo tiempo.
                    if str(count_of_prorroga) in inv.customer.flag['distribucion']:
                        inv.customer.flag['distribucion'][str(count_of_prorroga)].append(str(i))
                        #inv.customer.save()
                    else:
                        inv.customer.flag['distribucion'][str(count_of_prorroga)]=[str(i)]
                        #inv.customer.save()
                    inv.customer.flag['counts_prorroga']=count_of_prorroga+1
                    inv.customer.save()
                else:
                    # Si no ha tenido prorroga pero si hay info en el campo flag.
                    inv.customer.flag={
                        'prorrogas':
                            {str(i): {
                                'date': validated_data['date'],
                                'created_at': datetime.now().isoformat(), 
                                'paid_on_time': None, 
                                'paid_on': None, 
                                'extension_number':0}
                                },
                        'counts_prorroga': 1,
                        'distribucion': {'0': [str(i)]}
                    }
                    inv.customer.save()
        except:
            pass
    return Response(content, status=status.HTTP_201_CREATED) 

@api_view(['POST'])
@permission_classes([IsSystemInternalPermission,])
def create_payments_iris_view(request):
    data=request.data
    # print(data)
    try:
        serializer = PaymentIrisSerializer(data,context={'request': request})
        #print(serializer.data)
        validated_data=serializer.data
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)
    
    try:
        content={} 
        if validated_data["customer_rut"]=="":
            content={"error": "Se necesita un rut"} 
            return Response(content, status=status.HTTP_400_BAD_REQUEST) 

        operator=Operator.objects.get(id=validated_data["operator"])
        new_payment = Payment.objects.create(
            paid_on = validated_data['paid_on'],
            deposited_on = validated_data['deposited_on'],
            amount = validated_data["amount"],
            kind = validated_data["kind"],
            comment = validated_data["comment"],
            operator = operator,
            bank_account= BankAccount.objects.get(id=validated_data["bank_account"])
            )

        if "transfer_id" in validated_data and validated_data["transfer_id"]!="":
            new_payment.operation_number=validated_data["transfer_id"]
        if "operation_number" in validated_data and validated_data["operation_number"]!="":
            new_payment.operation_number=validated_data["operation_number"]
        if "customer_rut" in validated_data and validated_data["customer_rut"]!="":
            cliente=Customer.objects.filter(payorrut__rut=validated_data["customer_rut"])
            if cliente.count()==1:
                new_payment.customer=cliente[0]
            elif cliente.count()==0:
                new_payment.kind_load=2
                new_payment.comment= validated_data["comment"] + " " + validated_data["customer_rut"]
            else:
                new_payment.comment= validated_data["comment"] + " " + validated_data["customer_rut"]
                new_payment.kind_load=3

        new_payment.save()
        ctx={}
        if "service" in validated_data and validated_data["service"]!=[]:
            services: List[str] = validated_data.get('service', [])
            for i in services:
                ctx["service"]=""
                try:
                    new_payment.service.add(Service.objects.get(number=i, operator=operator))
                    new_payment.save()
                except:
                    ctx["service"]=ctx["service"]+"Not Found service "+str(i)+". "
                    content["service"]=ctx["service"]
                    pass

        if "invoices" in validated_data and validated_data["invoices"]!=[]:
            invoices: List[str] = validated_data.get('invoices', [])
            # Inicializando variables
            ctx["boletas"]=""
            total_pago=int(validated_data["amount"])
            acumulado=0
            invoice_list=Invoice.objects.none()
            for i in invoices:
                try:
                    invoice_list=invoice_list | Invoice.objects.filter(id=i)
                    Invoice.objects.get(id=i)
                except:
                    ctx["boletas"]=ctx["boletas"]+"Not Found invoice "+str(i)+". "
                    content["boletas"]=ctx["boletas"]
                    pass
                
            # Se ordena el listado de boletas.
            invoice_list=invoice_list.order_by("due_date")

            #print(invoice_list)
            for inv in invoice_list:
                #print(inv)
                try:
                    subtotal=int(inv.total)
                    # Buscar notas de crédito asociadas.
                    credit_notes = Invoice.objects.filter(parent_id=inv, kind=3)
                    # Calculo del total de cuanto cuesta la boleta.
                    for j in credit_notes:
                        subtotal = subtotal - j.total

                    acumulado=acumulado+subtotal
                    #print(acumulado)

                    # Marca como pagada la boleta.
                    if not(inv.paid_on) and ((acumulado - int(getattr(config, "money_without_paid")))<=total_pago):
                        #print("PASO y la marco pagada")
                        inv.paid_on=date.today()
                        inv.save()
                        # Revisando si existe una prorroga de esa boleta.
                        try:
                            if not(inv.customer.flag is None):
                                if "prorrogas" in inv.customer.flag:
                                    if str(inv.id) in inv.customer.flag['prorrogas']:
                                        fecha_actual=datetime.now()
                                        fecha=inv.customer.flag['prorrogas'][str(inv.id)]['date']
                                        if dateutil.parser.isoparse(fecha)>=fecha_actual:
                                            inv.customer.flag['prorrogas'][str(inv.id)]['paid_on_time']=True
                                            inv.customer.flag['prorrogas'][str(inv.id)]['paid_on']=datetime.now().isoformat()
                                        else:
                                            inv.customer.flag['prorrogas'][str(inv.id)]['paid_on_time']=False
                                            inv.customer.flag['prorrogas'][str(inv.id)]['paid_on']=datetime.now().isoformat()
                                        inv.customer.save()
                        except:
                            pass
                        credit_notes = credit_notes.filter(paid_on__isnull=True)
                        for j in credit_notes:
                            j.paid_on = date.today()
                            j.save()
                    new_payment.invoice.add(inv)
                    new_payment.save()
                except:
                    #ctx["boletas"]=ctx["boletas"]+"Not Found invoice "+str(i)+". "
                    #content["boletas"]=ctx["boletas"]
                    pass
        
        if "comprobante" in validated_data and validated_data["comprobante"]!=[]:
            comprobantes: List[str] = validated_data.get('comprobante', [])
            #print(comprobantes)

            index=0
            ctx["comprobante"]=""
            for imagen in comprobantes:
                try:
                    file_64=base64.b64decode(imagen)
                    mime_type = from_buffer(file_64, mime=True)
                    file_ext = mimetypes.guess_extension(mime_type)

                    file_object: ContentFile = ContentFile(file_64, name='comprobante.' + file_ext )
                    
                    #print(file_object)
                    if index==0:
                        new_payment.proof=file_object
                        new_payment.save()
                    if index==1:
                        new_payment.proof2=file_object
                        new_payment.save()
                    if index==2:
                        new_payment.proof3=file_object
                        new_payment.save()
                    if index==3:
                        new_payment.proof4=file_object
                        new_payment.save()
                except:
                    ctx["comprobante"]=ctx["comprobante"]+"Not Load proof "+str(index)+". "
                    content["comprobante"]=ctx["comprobante"]
                    pass
                index=index+1
                
        return Response(content, status=status.HTTP_201_CREATED) 
    except Exception as e:
        error = {'message': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
        print(error)
        return JsonResponse([str(error)], safe=False)

@api_view(['POST'])
@permission_classes([IsSystemInternalPermission,])
def load_proof_payment(request, pk):
    payment = get_object_or_404(Payment, id=pk)

    data=request.data
    # print(data)
    try:
        #comprobantes=data["comprobante"]
        comprobantes: List[str] = data.get('comprobante', [])
        if not(isinstance(comprobantes, list)): 
            #print(str(e))
            e="No es un listado"
            return JsonResponse([str(e)], safe=False)
    except Exception as e:
        print(str(e))
        return JsonResponse([str(e)], safe=False)

    content={}
    ctx={}
    if payment.proof and payment.proof2 and payment.proof3 and payment.proof4:
        e="Ya esta lleno de comprobantes"
        return JsonResponse([str(e)], safe=False)
    if payment.proof and payment.proof2 and payment.proof3:
        index=3
    elif payment.proof and payment.proof2:
        index=2
    elif payment.proof:
        index=1
    else:
        index=0
    #print(index)
    ctx["comprobante"]=""
    for imagen in comprobantes:
        try:
            file_64=base64.b64decode(imagen)
            mime_type = from_buffer(file_64, mime=True)
            file_ext = mimetypes.guess_extension(mime_type)

            file_object: ContentFile = ContentFile(file_64, name='comprobante.' + file_ext )
                
            #print(file_object)
            if index==0:
                payment.proof=file_object
                payment.save()
            if index==1:
                payment.proof2=file_object
                payment.save()
            if index==2:
                payment.proof3=file_object
                payment.save()
            if index==3:
                payment.proof4=file_object
                payment.save()
        except:
            ctx["comprobante"]=ctx["comprobante"]+"Not Load proof "+str(index)+". "
            content["comprobante"]=ctx["comprobante"]
            pass
        index=index+1
            
    return Response(content, status=status.HTTP_201_CREATED) 

# Fix me en template
def update_service_configuration(request):
    json_string = request.POST.get("nuevoJson", None)
    service_id = request.POST.get("number", None)
    nuevoJson = json.loads(json_string)
    service = Service.objects.get(id = service_id)
    service.flag = nuevoJson
    service.save()
    messages.add_message(
            request, messages.SUCCESS, "Se han guardado los cambios exitosamente."
    )

    return redirect(reverse("service-configuration", args=[service.slug]))

def update_customer_configuration(request):
    json_string = request.POST.get("nuevoJson", None)
    customer_number = request.POST.get("number", None)
    nuevoJson = json.loads(json_string)
    customer = Customer.objects.get(pk = customer_number)
    customer.flag = nuevoJson
    customer.save()
    return redirect(reverse("customer-configuration", args=[customer.pk]))


class UpdateCustomersInfoList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Customer
    queryset = Customer.objects.all()[:40]  # FIXME
    permission_required = "customers.list_customer"

    def get_context_data(self, *args, **kwargs):
        ctx = super(UpdateCustomersInfoList, self).get_context_data(*args, **kwargs)
        ctx["can_export"] = self.request.user.has_perm("customers.export_service")
        #ctx["plans"] = Plan.objects.all()
        ctx["operators"] = Operator.objects.all()
        return ctx


#customersinfo = UpdateCustomersInfoList.as_view()

@login_required
@permission_required("customers.list_customer")
def customersinfo(request):
    #customer = get_object_or_404(Customer, pk=customer_pk)
    operators = Operator.objects.all().values()
    #operators_json = json.dumps({"data": list(operators)})
    context = {
        #"customer": customer,
        #"services": customer.service_set.all(),
        #"operators": operators,
        #"operators_json": operators_json,
    }
    return render(request, "customers/customerupdate_list.html", context)

def prepare_updatecustomer(customer):
    
    validate=False
    if customer.flag:
        if "validado" in customer.flag:
            validate=customer.flag['validado']
            
    return {
        "DT_RowId": customer.pk,
        "rut": customer.rut,
        "services": customer.service_ids,
        "name": customer.truncated_name,
        "email": customer.first_email,
        "phone": customer.phone,
        "address": customer.composite_address,
        "update_page": "Si" if (UpdateInfoTemp.objects.filter(customer=customer).count()!=0) else "No",
        "is_validated": "Si" if validate else "No",
    }


@permission_required("customers.list_customer")
def customerinfo_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {
            "0": "rut",
            "1": "complete_name",
            "2": "email",
            "3": "db_composite_address",
            "4": "commune",
            "5": "kind",
            "7": "balance__balance",
            "8": "operator",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)

        # search
        search = request.GET.get("search[value]", "").replace("+", "")
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        update = request.GET.get("update", None)
        validado = request.GET.get("validated", None)

        all_customers = Customer.objects.all()

        username = None
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_customers = all_customers.filter(service__operator__id=op.operator)
                else:
                    all_customers = all_customers.filter(service__operator__company__id=op.company)
            except Exception as e:
                pass

        if update=="True":
            clientes = (
                UpdateInfoTemp.objects.all()
                .order_by("customer__id")
                .values_list("customer__id")
                .distinct("customer__id")
            )
            all_customers = all_customers.filter(id__in=clientes)
        elif update=="False":
            clientes = (
                UpdateInfoTemp.objects.all()
                .order_by("customer__id")
                .values_list("customer__id")
                .distinct("customer__id")
            )
            all_customers = all_customers.exclude(id__in=clientes)

        if validado=="True":
            all_customers = all_customers.filter(flag__contains={'validado': True})
        elif validado=="False":
            all_customers = all_customers.exclude(flag__contains={'validado': True})

        all_count = all_customers.count()

        if search:
            rut_regex = "^" + "\.*".join(list(search))

            q_objects = Q()
            q_objects |= Q(rut__iregex=rut_regex)
            q_objects |= Q(service__number__icontains=search)
            
            filtered_customers = all_customers.filter(q_objects)
        else:
            filtered_customers = all_customers

        filtered_customers = filtered_customers.distinct()

        filtered_count = filtered_customers.count()

        if length == -1:
            sliced_customers = filtered_customers
        else:
            sliced_customers = filtered_customers[start : start + length]

        
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": filtered_count,
                "data": list(map(prepare_updatecustomer, sliced_customers)),
            }
        )

        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

@login_required
@permission_required("customers.list_customer")
def customers_detail_info(request, pk):
    customer = get_object_or_404(Customer, pk=pk)
    operators = Operator.objects.all().values()
    new_info=UpdateInfoTemp.objects.filter(customer = customer)
    ser=[]
    if new_info.count()!=0:
        new_info=new_info.first()
        for i in new_info.services["info"]: #[{"number": "13328", "id_pulso" : "4656596"}, { "number": "12556","id_pulso" : 4656594,}]:
            r=get_complete_address(i["id_pulso"])
            if r["ok"]==True:
                j=i
                j["id"]=Service.objects.get(customer=customer,number=i["number"]).id
                j["address"]=r["data"]["street_location"]
                ser.append(j)

    validate=False
    if customer.flag:
        if "validado" in customer.flag:
            validate=customer.flag['validado']
    ready_to_validate=True
    if not(customer.id_pulso) or not(customer.street_location_id) :
        ready_to_validate=False
    services=Service.objects.filter(customer=customer)
    for i in services:
        if not(i.id_pulso) or not(i.street_location_id):
            ready_to_validate=False
    #print(ser)
    #operators_json = json.dumps({"data": list(operators)})
    context = {
        "customer": customer,
        "services": customer.service_set.all(),
        "new_info": new_info,
        "ser":ser,
        "ready_to_validate":ready_to_validate,
        "validate":validate,
        #"operators_json": operators_json,
    }
    return render(request, "customers/customerupdate_detail.html", context)

def mark_as_validado(request, pk):
    # Verificar que tanto el cliente como todos sus servicios tenga id_pulso
    customer = get_object_or_404(Customer, pk=pk)
    error=False
    if not(customer.id_pulso) or not(customer.street_location_id):
        messages.add_message(
                    request,
                    messages.ERROR,
                    "El cliente no tiene guardada la direccion de pulso.",
                )
        error=True
    services=Service.objects.filter(customer=customer)
    for i in services:
        if not(i.id_pulso) or not(i.street_location_id):
            messages.add_message(
                    request,
                    messages.ERROR,
                    "El servicio"+str(i.number)+" no tiene guardada la direccion de pulso.",
                )
            error=True
    if error==False:
        messages.add_message(request, messages.SUCCESS, "Se ha marcado como validada.")
        # Falta marcar como validado
        if customer.flag:
            customer.flag["validado"]=True
            customer.save()
        else:
            customer.flag={"validado": True}
            customer.save()
    return redirect(request.META.get("HTTP_REFERER", reverse("customerdetail-info", args=[customer.id])))


def accept_changes(request, pk):
    # Verificar que tanto el cliente como todos sus servicios tenga id_pulso
    customer = get_object_or_404(Customer, pk=pk)
    new_info=UpdateInfoTemp.objects.filter(customer = customer)
    new_info=new_info.first()
    customer.phone=new_info.phone
    customer.email=new_info.email
    customer.id_pulso=new_info.id_pulso
    customer.street_location_id=new_info.street_location_id
    customer.save()
    services=Service.objects.filter(customer=customer)

    for i in new_info.services["info"]:
        services.filter(number=i["number"]).update(id_pulso=i["id_pulso"], street_location_id=i["street_location_id"])

    # Falta marcar como validado
    if customer.flag:
        customer.flag["validado"]=True
        customer.save()
    else:
        customer.flag={"validado": True}
        customer.save()

    messages.add_message(request, messages.SUCCESS, "Se ha actualizado la informacion.")
    return redirect(request.META.get("HTTP_REFERER", reverse("global-invoice-list")))


@api_view(['GET', 'POST'])
@permission_classes([IsSystemInternalPermission,])
def customers_updateinfo_api(request):
    data=request.data
    #print(data)

    # Verificacion que tenga la estructura deseada.
    if not("customer")in data:
        content = {"error": "No es valida el formato enviado"}
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)

    if not("id_pulso") or not("street_location_id") or not("phone") or not("email") or not("services") in data:
        content = {"error": "No es valida el formato enviado"}
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)

    # Obtiene el cliente y sus servicios.
    customer=Customer.objects.filter(rut=data["customer"])
    if customer.count()==0:
        content = {"error": "No se encontro dicho cliente"}
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)
    else:
        customer=Customer.objects.get(rut=data["customer"])
    if UpdateInfoTemp.objects.filter(customer=customer).count()!=0:
        content = {"error": "Ya este cliente actualizo sus datos"}
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)
        
    services=Service.objects.filter(customer=customer)

    # Si necesita verificacion de que de verdad existe en pulso
    # Validando que el complete location exista
    r=get_complete_address(data["id_pulso"])
    if r["ok"]:
        facturacion=data["id_pulso"]
    else:
        content = {"error": "Direccion no valida, no existe en Pulso"}
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)

    # Validando que el steet location existe
    r=get_street_location(data["street_location_id"])
    if r["ok"]:
        street=data["street_location_id"]
    else:
        content = {"error": "Direccion no valida, no existe en Pulso"}
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)

    for i in data["services"]:
        # Verificando que tenga la estructura deseada.
        if not("number") or not("id_pulso")  or not("street_location_id") in i:
            content = {"error": "No es valida el formato enviado"}
            return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)
        services=services.exclude(number=i["number"])

        # Validando que el complete location exista
        r=get_complete_address(i["id_pulso"])
        if r["ok"]==False:
            content = {"error": "Complete location no valida, no existe en Pulso"}
            return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)
        # Validando que el steet location existe
        r=get_street_location(i["street_location_id"])
        if r["ok"]==False:
            content = {"error": "Street location no valida, no existe en Pulso"}
            return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)
    
    if services.count()==0:
        UpdateInfoTemp.objects.create(customer = customer,
            email = data["email"],
            phone = data["phone"],
            id_pulso = facturacion,
            street_location_id= street,
            services = {"info":  data["services"]}
        )
    else: 
        content = {"error": "Faltan servicios"}
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST) 
    # {number:id_pulso, number:id_pulso}

    list_to_send=[]
    return JsonResponse(list_to_send, safe=False)



@login_required
def region_ajax(request):
    val=request.GET.get('q', None)
    url=getattr(config, "pulso_server")+getattr(config, "search_region")+"?q="+str(val)
    headers={'Authorization':getattr(config, "pulso_token")}
    r= requests.get(url,
            headers=headers,
            verify=False)
    data = json.dumps(
            r.json()
           
        )
    return HttpResponse(data, content_type="application/json")

@login_required
def commune_ajax(request, region):
    val=request.GET.get('q', None)
    url=getattr(config, "pulso_server")+getattr(config, "search_commune")+str(region)+"?q="+str(val)
    headers={'Authorization':getattr(config, "pulso_token")}
    r = requests.get(url,
            headers=headers,
            verify=False)
    data = json.dumps(
            r.json()
        )
    return HttpResponse(data, content_type="application/json")

@login_required
def street_ajax(request, commune):
    val=request.GET.get('q', None)
    url=getattr(config, "pulso_server")+getattr(config, "search_street")+str(commune)+"?q="+str(val)
    headers={'Authorization':getattr(config, "pulso_token")}
    r = requests.get(url,
            headers=headers,
            verify=False)
    data = json.dumps(
            r.json()
        )
    return HttpResponse(data, content_type="application/json")


@login_required
def street_location_ajax(request, street):
    val=request.GET.get('q', None)
    url=getattr(config, "pulso_server")+getattr(config, "search_street_location")+str(street)+"?q="+str(val)
    headers={'Authorization':getattr(config, "pulso_token")}
    r = requests.get(url,
            headers=headers,
            verify=False)
    data = json.dumps(
            r.json()
        )
    return HttpResponse(data, content_type="application/json")


@login_required
def complete_location_ajax(request, street_location):
    val=request.GET.get('q', None)
    url=getattr(config, "pulso_server")+getattr(config, "search_complete_location")+str(street_location)+"/?q="+str(val)
    headers={'Authorization': getattr(config, "pulso_token")}
    r = requests.get(url,
            headers=headers,
            verify=False)
    data = json.dumps(
            r.json()
        )
    return HttpResponse(data, content_type="application/json")

def customers_address_save(request, pk, street, complete):
    customer = get_object_or_404(Customer, pk=pk)
    customer.id_pulso=complete
    customer.street_location_id=street
    customer.save()
    return redirect(request.META.get("HTTP_REFERER", reverse("customerdetail-info", args=[pk])))

def services_address_save(request, pk, street, complete):
    service = get_object_or_404(Service, pk=pk) #FIX ME
    service.id_pulso=complete
    service.street_location_id=street
    service.save()
    return redirect(request.META.get("HTTP_REFERER", reverse("customerdetail-info", args=[service.customer.pk])))

@api_view(["POST"])
def contract_acceptance(request):
    id = ''
    id = request.POST.get("id")
    aceptacion = request.POST.get("aceptacion")
    ip = request.POST.get("ip")
    aceptacion = int(aceptacion)
    try:
        customer = Customer.objects.get(id = int(id))
    except:
        error = {"error":"No existe cliente"}
        return JsonResponse(error, safe = False)

    if aceptacion == 1:
        if customer.flag:
            customer.flag['aceptacion_de_contrato'] = "Aceptado"
        else:
            customer.flag = {"aceptacion_de_contrato": "Aceptado"}
    elif aceptacion == 0:
        if customer.flag:
            customer.flag['aceptacion_de_contrato'] = "No Aceptado"
        else:
            customer.flag = {"aceptacion_de_contrato": "No Aceptado"}
    else:
        if customer.flag:
            customer.flag['aceptacion_de_contrato'] = "No Definido"
        else:
            customer.flag = {"aceptacion_de_contrato": "No Definido"}
    customer.save()
    fecha = timezone.now().strftime("%d/%m/%Y")
    if getattr(config, "allow_send_contract_acceptance") == "yes" and customer.flag["aceptacion_de_contrato"] == 'No Aceptado':
        string = ''
        string = string + '*Nombre*: ' + str(customer.name) + '\n'
        string = string + '*RUT*: ' + str(customer.rut) + '\n'
        string = string + '*Fecha*: ' + str(fecha) + '\n'
        string = string + '*Aceptación*: ' + str(customer.flag['aceptacion_de_contrato']) + '\n'
        if ip:
            string = string + '*Ip*: ' + str(ip) + '\n'

        bloque = [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": ":matrix: *Aceptación de Contrato*"
                },
            },
            {
                "type": "divider",
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": string
                },
            },
            {
                "type": "divider",
            },
        ]

        data_slack = {
            "text": "Aceptación de Contrato",
            "blocks": bloque,
        }
    
        http = urllib3.PoolManager()
        slack_channels=[]
        if customer.company:
            ops=Operator.objects.filter(company=customer.company)
            config_slack=ConfigOperator.objects.filter(operator__in=ops, name="Slack")
            if config_slack.count()==1:
                if "slack_aceptacion_contrato_canales" in config_slack[0].flag:
                    slack_channels = config_slack[0].flag["slack_aceptacion_contrato_canales"].split(',')
            #slack_channels = getattr(config,"aceptacion_contrato_canales").split(',')
            
            for channel in slack_channels:
                response_slack = http.request(
                    "POST",
                    channel,
                    body=json.dumps(data_slack),
                    headers={'Content-Type': 'application/json'}
                )
    return JsonResponse({"succes": True}, safe=False) 
    
def manual_cierre_ventas(request):
    service_pk = request.POST.get("service_pk")
    service = Service.objects.get(pk = int(service_pk))

    cen = 0
    message = 'Ejecución finalizada'
    if service.customer.flag:
        if "nuevo_cliente_email" not in service.customer.flag.keys():
            service.customer.flag['nuevo_cliente_email'] = "Por enviar"
            service.customer.save()
            cen = 1
        else:
            if service.customer.flag["nuevo_cliente_email"] != "Enviado":
                service.customer.flag["nuevo_cliente_email"] = "Por enviar"
                service.customer.save()
                cen = 1
            else:
                cen = 1
    else:
        jsonaux = {"nuevo_cliente_email": "Por enviar"}
        service.customer.flag = jsonaux
        service.customer.save()
        cen = 1

    if service.customer.flag:
        if "bloqueado_nuevo_cliente_email" in service.customer.flag.keys():
            if service.customer.flag["bloqueado_nuevo_cliente_email"] == 'Si':
                cen = 0
                message = "La configuración (bloqueado_nuevo_cliente_email) esta activa, no permite enviar el correo"

    orders = PlanOrder.objects.all().filter(service = service.pk)
    index = 0
    installation_date = ''
    while (index < len(orders)):
        if orders[index].cancelled == False and orders[index].start != None:
            installation_date = orders[index].start
            break
        index += 1
        
    if installation_date:
        MES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
            'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        formatted_installation_date = str(installation_date.day) + ' de ' + str(MES[installation_date.month-1]) + ' del ' + str(installation_date.year)
    else:
        formatted_installation_date = ''
        cen = 0
        message = "No se envió correo, no se tiene fecha de instalación"

    nombre = ''
    if service.customer.kind == 1:
        nombre = service.customer.first_name.split(" ")[0]
    else:
        nombre = service.customer.name

    costo_habilitacion = ''
    if service.technology_kind == 1:
        costo_habilitacion = '<li>Debido a que tu dirección arroja un Servicio UTP se adicionara un costo de Habilitación, habitualmente el costo es de $50.000 pesos; sin embargo, por una promoción referente al COVID-19 obtendrá una rebaja y solo deberá cancelar <strong>$20.000 pesos</strong>.</li>'

    if getattr(config, "allow_send_nuevo_cliente_email") == "yes":
        if cen == 1:
            info = []
            info.append(
            {
                "number": str(service.number),
                "street": str(service.street),
                "house_number": str(service.house_number),
                "apartment_number": str(service.apartment_number),
                "tower": str(service.tower),
                "location": str(service.location),
                "price_override": str(service.price_override),
                "due_day": str(service.due_day),
                "activated_on": str(service.activated_on) if service.activated_on else "",
                "installed_on": str(service.installed_on)  if service.installed_on else "",
                "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                "expired_on": str(service.expired_on)  if service.expired_on else "",
                "status": str(service.get_status_display()),
                "network_mismatch": "Sí" if service.network_mismatch else "No",
                "ssid": str(service.ssid),
                "technology_kind": str(service.get_technology_kind_display()),
                "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                "seen_connected": str(service.seen_connected),
                "mac_onu": str(service.mac_onu),
                "composite_address": str(service.customer.composite_address),
                "commune": str(service.node.commune),
                "plan__name": str(service.plan.description_facturacion),
                "plan__price": str(int(service.plan.price)),
                "plan__category": str(service.plan.category),
                "plan__uf":  "Sí" if service.plan.uf else "No",
                "plan__active": "Sí" if service.plan.active else "No", 
                "customer__rut": str(service.customer.rut),
                "customer__name": str(service.customer.name),
                "customer__first_name": str(nombre),
                "customer__last_name": str(service.customer.last_name), ###NUEVO###
                "customer__email": str(service.customer.email),
                "customer__street": str(service.customer.street),
                "customer__house_number": str(service.customer.house_number),
                "customer__apartment_number": str(
                    service.customer.apartment_number
                ),
                "customer__tower": str(service.customer.tower),
                "customer__location": str(service.customer.location),
                "customer__phone": str(service.customer.phone),
                "customer__default_due_day": str(
                    service.customer.default_due_day
                ),
                "customer__composite_address": str(
                    service.customer.composite_address
                ),
                "customer__commune": str(service.customer.commune),
                "installation_date": formatted_installation_date, 
                "service__number": str(service.number),
                "customer__id": str(service.customer.id),
                "customer_html|safe": str(costo_habilitacion), 
            })

            data = {"event": getattr(config, "evento_cierre_de_ventas"), "info": info}
            url = getattr(config, "iris_server") + "api/v1/communications/trigger_event/active/"
            
            try:
                send = requests.post(
                    url=url,
                    json=data,
                    headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                    verify = False
                )
                if "Error" in send.json() or "error" in send.json():
                    message = "Error proveniente de Iris"
                else:
                    service.customer.flag["nuevo_cliente_email"] = "Enviado"
                    service.customer.save()
                    message = "Se ha enviado correctamente el correo Cierre de Ventas"
            except Exception as e:
                message = str(e)
                print("e",e)
            
    data = json.dumps({
        "message": message,
    })
    return HttpResponse(data, content_type="application/json")

def constance_list(request):
    
    configs = [
        "month_invoice",
        "year_invoice",
        "feriados",
        "allow_send_nuevo_cliente_email",
        "allow_send_post_activacion_email",
        "allow_send_onboarding_email",
        "allow_send_contract_acceptance",
        "allow_feriados"
    ]
    constance_values = []
    for i in range(0,len(configs)):
        constance_values.append([str(i+1), configs[i], getattr(config, configs[i])])

    return render(
        request,
        "customers/constance_list.html",
        {
            "constance_values": constance_values
        }
    )

def constance_update(request):
    config_change = request.POST.get("config_change", None)
    new_value = request.POST.get("newVal", None)
    setattr(config, config_change, new_value)
    data = json.dumps({"success": True})
    return HttpResponse(data, content_type="application/json")


def manual_post_instalacion(request):
    service_number = request.POST.get("service_number")
    service = Service.objects.get(id = int(service_number))

    cen = 0
    message = "Ejecución Finalizada"
    if service.flag:
        if "post_activacion_email" not in service.flag.keys():
            service.flag['post_activacion_email'] = "Por enviar"
            service.save()
            cen = 1
        else:
            if service.flag["post_activacion_email"] != "Enviado":
                service.flag["post_activacion_email"] = "Por enviar"
                service.save()
                cen = 1
            else:
                cen = 1
    else:
        jsonaux = {"post_activacion_email": "Por enviar"}
        service.flag = jsonaux
        service.save()
        cen = 1

    if service.flag:
        if "bloqueado_post_activacion_email" in service.flag.keys():
            if service.flag["bloqueado_post_activacion_email"] == 'Si':
                cen = 0
                message = "La configuración (bloqueado_post_instalacion_email) esta activa, no permite enviar el correo"

    nombre = ''
    if service.customer.kind == 1:
        nombre = service.customer.first_name.split(" ")[0]
    else:
        nombre = service.customer.name

    if getattr(config, "allow_send_post_activacion_email") == "yes":
        if cen == 1:
            info = []
            info.append(
            {
                "number": str(service.number),
                "street": str(service.street),
                "house_number": str(service.house_number),
                "apartment_number": str(service.apartment_number),
                "tower": str(service.tower),
                "location": str(service.location),
                "price_override": str(service.price_override),
                "due_day": str(service.due_day),
                "activated_on": str(service.activated_on) if service.activated_on else "",
                "installed_on": str(service.installed_on)  if service.installed_on else "",
                "uninstalled_on": str(service.uninstalled_on)  if service.uninstalled_on else "",
                "expired_on": str(service.expired_on)  if service.expired_on else "",
                "status": str(service.get_status_display()),
                "network_mismatch": "Sí" if service.network_mismatch else "No",
                "ssid": str(service.ssid),
                "technology_kind": str(service.get_technology_kind_display()),
                "allow_auto_cut": "Sí" if service.allow_auto_cut else "No",
                "seen_connected": str(service.seen_connected),
                "mac_onu": str(service.mac_onu),
                "composite_address": str(service.customer.composite_address),
                "commune": str(service.node.commune),
                "plan__name": str(service.plan.description_facturacion),
                "plan__price": str(service.plan.price),
                "plan__category": str(service.plan.category),
                "plan__uf":  "Sí" if service.plan.uf else "No",
                "plan__active": "Sí" if service.plan.active else "No", 
                "customer__rut": str(service.customer.rut),
                "customer__name": str(service.customer.name),
                "customer__first_name": str(nombre),
                "customer__last_name": str(service.customer.last_name), ###NUEVO###
                "customer__email": str(service.customer.email),
                "customer__street": str(service.customer.street),
                "customer__house_number": str(service.customer.house_number),
                "customer__apartment_number": str(
                    service.customer.apartment_number
                ),
                "customer__tower": str(service.customer.tower),
                "customer__location": str(service.customer.location),
                "customer__phone": str(service.customer.phone),
                "customer__default_due_day": str(
                    service.customer.default_due_day
                ),
                "customer__composite_address": str(
                    service.customer.composite_address
                ),
                "customer__commune": str(service.customer.commune),
                "service__number": str(service.number),
                "customer__id": str(service.customer.id),
            })

            data = {"event": getattr(config, "evento_post_activacion"), "info": info}
            url = getattr(config, "iris_server") + "api/v1/communications/trigger_event/active/"


            try:
                send = requests.post(
                    url=url,
                    json=data,
                    headers={"Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0"},
                    verify = False
                )
                if "Error" in send.json() or "error" in send.json():
                    message = "Error proveniente de Iris"
                else:
                    service.flag["post_activacion_email"] = "Enviado"
                    service.save()
                    message = "Se ha enviado correctamente el correo Post Instalación"
            except Exception as e:
                print("e",e)
    else:
        message = "Bloqueada configuración en el constance"
        
    data = json.dumps({
        "message": message,
    })
    return HttpResponse(data, content_type="application/json")

def pruebage(request):
    ge = GeneralExpense.objects.all()
    te = ExpenseType.objects.all()
    
    for g in te:
        g.delete()

    ExpenseType.objects.create(
        name = "General Fixes"
    )
    
    result = []
    return JsonResponse(result, safe =False)

class GeneralExpenses(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = GeneralExpense
    paginate_by = 10
    template_name = "customers/general_expense_list.html"
    permission_required = "customers.general_expense_list"

    def get_context_data(self, *args, **kwargs):
        ctx = super(GeneralExpenses, self).get_context_data(*args, **kwargs)
        expense_type = ExpenseType.objects.all()
        type_list = [('0', 'Nuevo')]
        for et in expense_type:
            type_list.append((str(et.pk), str(et)))
        ctx["type_list"] = type_list

        ctx["can_show_detail"]=True
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                    ctx["can_show_detail"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                    ctx["can_show_detail"]=False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass

        return ctx

general_expense_list = GeneralExpenses.as_view()

def general_expense_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "pk",
            "1": "type",
            "2": "description",
            "3": "issue_date",
            "4": "amount",
            "5": "currency",
            "6": "operator",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_general_expense = GeneralExpense.objects.all().order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_general_expense = GeneralExpense.objects.all()

        all_general_expense = all_general_expense.distinct()
        all_count = all_general_expense.count()
        
        if length == -1:
            sliced_general_expense = all_general_expense
        else:
            sliced_general_expense = all_general_expense[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": all_count,
                "data": list(map(prepare_general_expense, sliced_general_expense)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_general_expense(general_expense):
    
    dictionary = {
        "pk": general_expense.pk,
        "type": str(general_expense.type.name),
        "description": str(general_expense.description),
        "issue_date": general_expense.issue_date.strftime("%Y/%m/%d") if general_expense.issue_date else "No definida",
        "amount": str(general_expense.amount),
        "currency": str(general_expense.currency.name),
        "operator": str(general_expense.operator.name),
    }
    return dictionary

class GeneralExpenseDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = GeneralExpense
    permission_required = "customers.general_expense_detail"
    template_name = "customers/general_expense_detail.html"
    
    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["can_show_detail"] = True
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False

                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass

            try:
                op=UserOperator.objects.get(user=user)
                if op.operator == 0:
                    ctx["can_show_detail"] = True
                else:
                    if op.operator == self.object.operator.pk:
                        ctx["can_show_detail"] = True
                    else:
                        ctx["can_show_detail"] = False
            except:
                pass


        return ctx

general_expense_detail = GeneralExpenseDetail.as_view()

class GeneralExpenseCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = GeneralExpense
    form_class = GeneralExpenseCreateForm
    template_name = "customers/general_expense_create.html"
    permission_required = "customers.add_general_expense"

    def get_form_kwargs(self):
        kwargs = super(GeneralExpenseCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super(GeneralExpenseCreate, self).get_context_data(**kwargs)
        ctx["can_show_detail"]=True
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                    ctx["can_show_detail"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                    ctx["can_show_detail"]= False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass

        return ctx

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)

        self.object = form.save()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                self.object.company=Company.objects.get(id=op.company)
                self.object.save()
            except:
                pass
        
        return super(GeneralExpenseCreate, self).form_valid(form)

general_expense_create = GeneralExpenseCreate.as_view()

class GeneralExpenseUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = GeneralExpense
    form_class = GeneralExpenseUpdateForm
    success_message = "Se ha actualizado la información del Egreso."
    template_name = "customers/general_expense_update.html"
    permission_required = "customers.change_general_expense"

    def get_form_kwargs(self):
        kwargs = super(GeneralExpenseUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super(GeneralExpenseUpdate, self).get_context_data(**kwargs)
        ctx["can_show_detail"]=True
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    name_operator="Todos"
                    ctx["if_todos"]=True
                    ctx["can_show_detail"]=True
                else:
                    name_operator=Operator.objects.get(id=op.operator).name
                    ctx["if_todos"]=False
                    ctx["can_show_detail"]= False
                ctx["companies"]=companies.exclude(id=op.company)
                ctx["current_company"]=op.company
                ctx["current_company_name"]=Company.objects.get(id=op.company).name
                ctx["current_operator"]=op.operator
                ctx["current_operator_name"]=name_operator
                ctx["operadores"]=Operator.objects.filter(company__id=op.company).exclude(id=op.operator)
            except:
                pass

        return ctx

general_expense_update = GeneralExpenseUpdate.as_view()

def general_expense_delete(request, pk):
    general_expense = get_object_or_404(GeneralExpense, pk=pk)
    
    if general_expense:
        general_expense.delete()
    
    return HttpResponseRedirect(reverse_lazy("general-expense-list"))

def expense_type_update(request):
    type_pk = request.POST.get("type_pk", None)
    new_value = request.POST.get("newVal", None)
    cen = 0
    if type_pk == '0':
        ExpenseType.objects.create(
            name = new_value
        )
    else:
        selected_type = ExpenseType.objects.get(pk=int(type_pk))
        if selected_type:
            selected_type.name = new_value
            selected_type.save()
        else:
            cen = 1
    if cen == 0:
        data = json.dumps({"success": True})
    else:
        data = json.dumps({"success": False, "error": "No se encontro tipo de Egreso selecccionado"})
    return HttpResponse(data, content_type="application/json")

@api_view(["POST"])
def service_retire(request):
    id = ''
    id = request.POST.get("id")
    aceptacion = request.POST.get("aceptacion")
    ip = request.POST.get("ip")
    aceptacion = int(aceptacion)
    try:
        service = Service.objects.get(number = int(id))
    except:
        error = {"error":"No existe servicio"}
        return JsonResponse(error, safe = False)

    if aceptacion == 1:
        if service.flag:
            service.flag['aceptacion_de_retiro'] = "Aceptado"
        else:
            service.flag = {"aceptacion_de_retiro": "Aceptado"}
    elif aceptacion == 0:
        if service.flag:
            service.flag['aceptacion_de_retiro'] = "No Aceptado"
        else:
            service.flag = {"aceptacion_de_retiro": "No Aceptado"}
    else:
        if service.flag:
            service.flag['aceptacion_de_retiro'] = "No Definido"
        else:
           service.flag = {"aceptacion_de_retiro": "No Definido"}
    service.save()

    return JsonResponse({"succes": True}, safe=False) 

@login_required
@permission_required("customers.view_customer")
def service_retention(request, slug):
    service = get_object_or_404(Service, number=slug)
    can_show_detail=False
    if request.user.is_authenticated():
        username = request.user.username
        user=User.objects.get(username=username)
        companies=UserCompany.objects.get(user=user).company.all()
        try:
            op=UserOperator.objects.get(user=user)
            print(op)
            print(op.operator)
            if op.operator==0:
                print(service.operator)
                print(service.operator.company.id)
                print(op.company)
                if service.operator.company.id==op.company:
                    print("entre")
                    can_show_detail=True
            else:
                if service.operator.id==op.operator:
                    can_show_detail=True
        except:
            pass

    try:
        '''
        h = {"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'}
        
        url = getattr(config, "iris_server_typify") + 'api/v1/tickets/typify/' + '?rut=["' + str(service.customer.rut) + '"]'
        print("url typify", url)
        send = requests.get(
            url=url,
            headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
            verify=False
        )
        get_typify = []
        for typ in send.json():
            if service.number in typ['services']:
                get_typify.append(typ)

        typify = prepare_typify(get_typify, service)
    	'''
        h = {"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'}
        url = "https://demo-iris.devel7.cl/api/v1/retentions/customer_retention/?service=" + str(service.number)
        print(url)
        send = requests.get(
            url=url,
            headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
            verify=False
        )
        print(send)
        print(send.raw)
        print(send.text)
        print(send.json())
        print(dir(send))
        

        url = "https://backend.iris7.cl/api/v1/escalation_ti/escalation/?services=[" + str(service.number) + "]"
        print(url)
        send = requests.get(
            url=url,
            headers={"Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU4OTQwOTE5OCwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.YCqmEQmQ7vSyzHf4UD1wy2vbhcvTHlw46JEsc0Ct-Z0'},
            verify=False
        )
        print(send)
        print(send.json())

        context = {
            "object": service,
            "service": service,
            "customer": service.customer,
            "invoices": Invoice.objects.filter(service=service),
            "payments": Payment.objects.filter(service=service),
        }
        context["can_show_detail"] = can_show_detail
    except Exception as e:
        print(e)
        context = {
            "object": service,
            "service": service,
            "customer": service.customer,
            "invoices": Invoice.objects.filter(service=service),
            "payments": Payment.objects.filter(service=service),
        }
        context["can_show_detail"] = can_show_detail
        messages.add_message(
                request,
                messages.WARNING,
                "No se puso establecer comunicación con Iris",
            )
    return render(request, "customers/service_retention.html", context)