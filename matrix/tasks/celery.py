from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings
from config.settings import get_env_variable
from celery.schedules import crontab

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.development")

# celery = Celery("config", broker="redis://localhost/", include=["billing.tasks"])

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")

celery_app = Celery("tasks")

celery_app.config_from_object("django.conf:settings", namespace="CELERY")

celery_app.conf.beat_schedule = {
    "documents_to_iris": {"task": "invoice_to_iris", "schedule": 10,},
    "end_onboarding_email": {"task": "end_onboarding_email", "schedule": crontab(minute = 0, hour = 11)},
    "four_days_whatsapp": {"task": "four_days_whatsapp", "schedule": crontab(minute = 0, hour = 7)},
    "nuevo_cliente_email": {"task": "nuevo_cliente_email", "schedule": crontab(minute = 0, hour = '*/1')},
    "post_activacion_email": {"task": "post_activacion_email", "schedule": crontab(minute = 0, hour = '*/1')},
}

celery_app.autodiscover_tasks(settings.INSTALLED_APPS)
