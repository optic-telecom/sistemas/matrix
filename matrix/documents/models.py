import os
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class Document(models.Model):
    file = models.FileField(_('file'), upload_to='documents')
    description = models.CharField(_('description'), max_length=255)
    tag = models.CharField(_('tag'), default="doc", max_length=3)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created_at = models.DateTimeField(default=timezone.now)
    agent = models.ForeignKey('auth.User', verbose_name=_('agent'), null=True, blank=True)
    
    class Meta:
        ordering = ['-created_at']

    def extension(self):
        name, extension = os.path.splitext(self.file.name)
        return extension

    def __str__(self):
        return self.description
