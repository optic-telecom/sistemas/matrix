from .models import (Technician)
from rest_framework import serializers
from drf_queryfields import QueryFieldsMixin

class TechnicianSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Technician
        fields = ('id','name')

