from import_export import resources
from .models import (Technician, Skill, Availability, Downtime,
    Employee, Vacation, Agent)

class TechnicianResource(resources.ModelResource):

    class Meta:
        model = Technician

class SkillResource(resources.ModelResource):

    class Meta:
        model = Skill
    
class AvailabilityResource(resources.ModelResource):

    class Meta:
        model = Availability

class DowntimeResource(resources.ModelResource):

    class Meta:
        model = Downtime

class EmployeeResource(resources.ModelResource):

    class Meta:
        model = Employee

class VacationResource(resources.ModelResource):

    class Meta:
        model = Vacation

class AgentResource(resources.ModelResource):

    class Meta:
        model = Agent