from dateutil.rrule import rrule, MONTHLY
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import JSONField

from customers.models import BaseModel, Operator, Company, Currency

class MultiDbModel(BaseModel):
    class Meta:
        abstract = True

    def save(self, *args, **kwarg):
        for dbname in settings.DATABASES:
            super(MultiDbModel, self).save(using=dbname)


class Technician(BaseModel):
    name = models.CharField(_('name'), max_length=255)
    active = models.BooleanField(_('active'), default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('technician')
        verbose_name_plural = _('technicians')


class Skill(BaseModel):
    name = models.CharField(_('name'), max_length=255)
    description = models.TextField(_('description'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('skill')
        verbose_name_plural = _('skills')


class Availability(BaseModel):
    DAYS_OF_WEEK = ((0, 'domingo'),
                    (1, 'lunes'),
                    (2, 'martes'),
                    (3, 'miércoles'),
                    (4, 'jueves'),
                    (5, 'viernes'),
                    (6, 'sábado'))

    technician = models.ForeignKey(Technician, verbose_name=_('technician'))
    day = models.CharField(_('day'), max_length=1, choices=DAYS_OF_WEEK)
    start = models.TimeField(_('start'))
    end = models.TimeField(_('end'))

    class Meta:
        verbose_name = _('availability')
        verbose_name_plural = _('availabilities')
        unique_together = (('technician', 'day', 'start'),
                           ('technician', 'day', 'end'))


class Downtime(BaseModel):
    technician = models.ForeignKey(Technician, verbose_name=_('technician'))
    start = models.DateTimeField(_('start'))
    end = models.DateTimeField(_('end'))
    description = models.CharField(_('description'), max_length=255)

    class Meta:
        verbose_name = _('downtime')
        verbose_name_plural = _('downtimes')
        unique_together = (('technician', 'start'),
                           ('technician', 'end'))


### NEW WORLD
class Employee(BaseModel):
    PUBLIC = 1
    PRIVATE = 2
    HEALTH_CARE_CHOICES = ((PUBLIC, _('FONASA')),
                           (PRIVATE, _('Isapre')))

    OPTIC = 1
    BANDA_ANCHA = 2
    KPTIVE = 3
    COMPANY_CHOICES = ((OPTIC, 'Optic'),
                       (BANDA_ANCHA, 'Banda Ancha'),
                       (KPTIVE, 'Kptivemedia'))

    CALL_CENTER = 1
    OPERACIONES = 2
    SOPORTE_TECNICO = 3
    ADMINISTRACION_FINANZAS = 4
    VENTAS = 5
    ARICA = 6
    VINA_DEL_MAR = 7
    PROGRAMADORES = 8
    BODEGA = 9
    DEPARTMENT_CHOICES = ((CALL_CENTER, 'Call center'),
                          (OPERACIONES, 'Operaciones'),
                          (SOPORTE_TECNICO, 'Soporte Técnico'),
                          (ADMINISTRACION_FINANZAS, 'Administración - Finanzas'),
                          (VENTAS, 'Ventas'),
                          (ARICA, 'Arica'),
                          (VINA_DEL_MAR, 'Viña del Mar'),
                          (PROGRAMADORES, 'Programadores'),
                          (BODEGA, 'Bodega'))

    MALE = 1
    FEMALE = 2
    GENDER_CHOICES = ((MALE, _('male')),
                      (FEMALE, _('female')))
    
    VALID = 1
    SETTLED = 2

    STATUS_CHOICES = ((VALID, _('valid')),
                      (SETTLED, _('settled')))

    status = models.PositiveSmallIntegerField(_('status'), choices=STATUS_CHOICES, default=VALID)
    rut = models.CharField(_('rut'), max_length=13, unique=True)
    name = models.CharField(_('name'), max_length=255)
    address = models.CharField(_('address'), max_length=75)
    commune = models.CharField(_('commune'), max_length=30)
    health_care = models.PositiveSmallIntegerField(_('health care'), choices=HEALTH_CARE_CHOICES, default=PUBLIC)
    afp = models.CharField(_('AFP'), max_length=255)
    company = models.PositiveSmallIntegerField(_('company'), choices=COMPANY_CHOICES, default=OPTIC)
    department = models.PositiveSmallIntegerField(_('department'), choices=DEPARTMENT_CHOICES, default=CALL_CENTER)
    gender = models.PositiveSmallIntegerField(_('gender'), choices=GENDER_CHOICES, default=MALE)
    service_date = models.DateField(_('service date'))
    base_salary = models.PositiveIntegerField(_('base salary'))
    transportation_bonus = models.PositiveIntegerField(_('transportation bonus'), blank=True, default=0)
    food_bonus = models.PositiveIntegerField(_('food bonus'), blank=True, default=0)
    productivity_bonus = models.PositiveIntegerField(_('productivity bonus'), blank=True, default=0)
    other_bonuses = models.PositiveIntegerField(_('other bonuses'), blank=True, default=0)
    service_notes = models.TextField(_('service notes'), blank=True)
    documents = GenericRelation('documents.Document')
    operator = models.ForeignKey("customers.Operator", default=2, verbose_name=_("operator"))

    @property
    def vacation_days_earned(self):
        if self.service_date.day > 28:
            start_date = self.service_date - timezone.timedelta(days=3)
        else:
            start_date = self.service_date
        end_date = timezone.now().date()

        dates = [date for date in rrule(MONTHLY, dtstart=start_date, until=end_date)]
        return (len(dates)-1) * 1.25

    @property
    def vacation_days_used(self):
        return sum(map(lambda v: v.days, self.vacation_set.all()))

    @property
    def vacation_days_balance(self):
        return self.vacation_days_earned - self.vacation_days_used

    def get_absolute_url(self):
        return reverse('employee-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('employee')
        verbose_name_plural = _('employees')


class Vacation(BaseModel):
    employee = models.ForeignKey(Employee, verbose_name=_('employee'))
    start_date = models.DateField(_('start date'))
    end_date = models.DateField(_('end date'))
    notes = models.TextField(_('notes'), blank=True)

    @property
    def days(self):
        return (self.end_date - self.start_date).days + 1

    class Meta:
        verbose_name = _('vacation')
        verbose_name_plural = _('vacations')


class Agent(MultiDbModel):
    user = models.ForeignKey('auth.User', verbose_name=_('User'))
    voip_extension = models.CharField(_('Extension'), max_length=10)

    def __str__(self):
        return ' - '.join([' '.join([self.user.first_name, self.user.last_name]), self.voip_extension])

    class Meta:
        ordering = ['-pk']
        permissions = (('view_agent', 'Can view agents'),
                       ('list_agent', 'Can list agents'))

class Worker(BaseModel):

    FEMALE = 1
    MALE = 2
    OTHER = 3
    GENDER_CHOICES = ((FEMALE, _("F")), 
                      (MALE, _("M")), 
                      (OTHER, _("O")))

    CEDULA = 1
    CARNET = 2
    PASAPORTE = 3
    LICENCIA_CONDUCIR = 4
    DOCUMENT_CHOICES = ((CEDULA, _("Cédula")),
                        (CARNET, _("Carnet")),
                        (PASAPORTE, _("Pasaporte")),
                        (LICENCIA_CONDUCIR, _("Licencia de conducir")))

    CEO = 1
    LEADER = 2
    SENIOR = 3
    JUNIOR = 4
    INTERN = 5
    POSITION_CHOICES = ((CEO, _("CEO")),
                        (LEADER, _("Leader")),
                        (SENIOR, _("Senior")),
                        (JUNIOR, _("Junior")),
                        (INTERN, _("Intern"))) 

    PROGRAMACION = 1
    MARKETING = 2
    QA = 3
    FINANZAS = 4
    RECURSOS_HUMANOS = 5
    DEPARTMENT_CHOICES = ((PROGRAMACION, _("Programación")),
                          (MARKETING, _("Marketing")),
                          (QA, _("QA")),
                          (FINANZAS, _("Finanzas")),
                          (RECURSOS_HUMANOS, _("Recursos Humanos")))

    first_name = models.CharField(_("first_name"), max_length=255, null = True)
    last_name = models.CharField(_("last_name"), max_length = 255, null = True)
    gender = models.PositiveIntegerField(_("gender"), choices=GENDER_CHOICES, null=True)
    address = models.CharField(_('address'), max_length=255, null = True)
    phone = models.CharField(_("phone"), max_length=75, default = '')
    email = models.EmailField(_("email"), default = '')
    base_salary = models.PositiveIntegerField(_('base_salary'), null = True, blank = True)
    entry_date = models.DateTimeField(_("entry_date"), null=True, blank=True)
    document_type = models.PositiveIntegerField(_("document_type"), choices=DOCUMENT_CHOICES, null=True)
    document_number = models.CharField(_('document_number'), max_length=255, null = True)
    job_position = models.PositiveIntegerField(_("job_type"), choices=POSITION_CHOICES, null=True)
    job_department = models.PositiveIntegerField(_("job_department"), choices=DEPARTMENT_CHOICES, null=True)
    active = models.BooleanField(_("active"), default=True)
    company = models.ForeignKey("customers.Company", default=1, verbose_name=_("company"))
    currency = models.ForeignKey("customers.Currency", verbose_name=_("currency"), default = 1)
    flag = JSONField(_("Flag"), default = "{}", blank=False)

    @property
    def name(self):
        if self.first_name and self.last_name:
            complete_name = self.first_name + ' ' + self.last_name
        elif self.first_name:
            complete_name = self.first_name
        elif self.last_name:
            complete_name = self.last_name
        else:
            complete_name = ''
        return complete_name

    def get_absolute_url(self):
        return reverse("worker-detail", kwargs={"pk": self.pk})

    def __str__(self):
      return str(self.first_name) + ' ' + str(self.last_name)

class Item(BaseModel):

    POSITIVE = 1
    NEGATIVE = 2
    SIGN_CHOICES = ((POSITIVE, _('Positivo')),
                    (NEGATIVE, _('Negativo')))

    name = models.CharField(_("name"), max_length=255, null = True)
    sign = models.PositiveIntegerField(_("sign"), choices=SIGN_CHOICES, null=True)
    description = models.TextField(_("description"), blank=True, null = True)
    company = models.ForeignKey("customers.Company", default=1, verbose_name=_("company"))
    active = models.BooleanField(_("active"), default=True)
    flag = JSONField(_("Flag"), default = "{}", blank=False)

    def get_absolute_url(self):
        return reverse("item-list")

    def __str__(self):
      return str(self.name)

class Receipt(BaseModel):

    ENERO = 1
    FEBRERO = 2
    MARZO = 3
    ABRIL = 4
    MAYO = 5
    JUNIO = 6
    JULIO = 7
    AGOSTO = 8
    SEPTIEMBRE = 9
    OCTUBRE = 10
    NOVIEMBRE = 11
    DICIEMBRE = 12
    MONTH_CHOICES = ((ENERO, ('Enero')), (FEBRERO, ('Febrero')), (MARZO, ('Marzo')),
                     (ABRIL, ('Abril')), (MAYO, ('Mayo')), (JUNIO, ('Junio')), 
                     (JULIO, ('Julio')), (AGOSTO, ('Agosto')), (SEPTIEMBRE, ('Septiembre')), 
                     (OCTUBRE, ('Octubre')), (NOVIEMBRE, ('Noviembre')), (DICIEMBRE, ('Diciembre')),)

    month = models.PositiveIntegerField(_("month"), choices=MONTH_CHOICES, null=True)
    year = models.PositiveIntegerField(_("year"), null = True)
    issue_date = models.DateTimeField(_("issue_date"), null=True, blank=True)
    amount = models.DecimalField(_("amount"), null = True, max_digits = 12, decimal_places = 3)
    exchange_rate = models.DecimalField(_("exchange_rate"), null = True, max_digits = 12, decimal_places = 3)
    worker = models.ForeignKey("hr.Worker", verbose_name=_("worker"))
    currency = models.ForeignKey("customers.Currency", verbose_name=_("currency"))
    bank_account = models.ForeignKey("customers.BankAccount", verbose_name=_("bank_account"), null=True)

    def __str__(self):
      return str(self.worker) + ' ' + str(self.month) + '-' + str(self.year)

class AppliedItem(BaseModel):

    real_amount = models.DecimalField(_("real_amount"), null = True, max_digits = 12, decimal_places = 3)
    receipt = models.ForeignKey("hr.Receipt", verbose_name=_("receipt"))
    item = models.ForeignKey("hr.Item", verbose_name=_("item"))

    def __str__(self):
        return str(self.receipt.worker.name) + ' ' + str(self.item.name) + ' ' + str(self.real_amount)