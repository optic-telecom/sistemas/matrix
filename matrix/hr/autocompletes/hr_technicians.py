from dal import autocomplete
from hr.models import Technician


class TechnicianAutocomplete(autocomplete.Select2QuerySetView):
    """ Maneja un listado de ip """
    def get_queryset(self):
        qs = Technician.objects.all()
        if self.q:
            qs = Technician.objects.filter(
                name__istartswith=self.q
            )

        return qs
