# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-09-06 13:54
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0063_auto_20210906_0954'),
        ('hr', '0003_employee_operators'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppliedItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('real_amount', models.DecimalField(decimal_places=3, max_digits=12, null=True, verbose_name='real_amount')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('name', models.CharField(max_length=255, null=True, verbose_name='name')),
                ('sign', models.PositiveIntegerField(choices=[(1, 'Positivo'), (2, 'Negativo')], null=True, verbose_name='sign')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
                ('flag', django.contrib.postgres.fields.jsonb.JSONField(default='{}', verbose_name='Flag')),
                ('company', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='customers.Company', verbose_name='company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Receipt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('month', models.PositiveIntegerField(choices=[(1, 'Enero'), (2, 'Febrero'), (3, 'Marzo'), (4, 'Abril'), (5, 'Mayo'), (6, 'Junio'), (7, 'Julio'), (8, 'Agosto'), (9, 'Septiembre'), (10, 'Octubre'), (11, 'Noviembre'), (12, 'Diciembre')], null=True, verbose_name='month')),
                ('year', models.PositiveIntegerField(null=True, verbose_name='year')),
                ('issue_date', models.DateTimeField(blank=True, null=True, verbose_name='issue_date')),
                ('amount', models.DecimalField(decimal_places=3, max_digits=12, null=True, verbose_name='amount')),
                ('exchange_rate', models.DecimalField(decimal_places=3, max_digits=12, null=True, verbose_name='exchange_rate')),
                ('currency', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='customers.Currency', verbose_name='currency')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Worker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('first_name', models.CharField(max_length=255, null=True, verbose_name='first_name')),
                ('last_name', models.CharField(max_length=255, null=True, verbose_name='last_name')),
                ('gender', models.PositiveIntegerField(choices=[(1, 'F'), (2, 'M'), (3, 'O')], null=True, verbose_name='gender')),
                ('address', models.CharField(max_length=255, null=True, verbose_name='address')),
                ('phone', models.CharField(default='', max_length=75, verbose_name='phone')),
                ('email', models.EmailField(default='', max_length=254, verbose_name='email')),
                ('base_salary', models.PositiveIntegerField(blank=True, null=True, verbose_name='base_salary')),
                ('entry_date', models.DateTimeField(blank=True, null=True, verbose_name='entry_date')),
                ('document_type', models.PositiveIntegerField(choices=[(1, 'Cédula'), (2, 'Carnet'), (3, 'Pasaporte'), (4, 'Licencia de conducir')], null=True, verbose_name='document_type')),
                ('document_number', models.CharField(max_length=255, null=True, verbose_name='document_number')),
                ('job_position', models.PositiveIntegerField(choices=[(1, 'CEO'), (2, 'Leader'), (3, 'Senior'), (4, 'Junior'), (5, 'Intern')], null=True, verbose_name='job_type')),
                ('job_department', models.PositiveIntegerField(choices=[(1, 'Programación'), (2, 'Marketing'), (3, 'QA'), (4, 'Finanzas'), (5, 'Recursos Humanos')], null=True, verbose_name='job_department')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
                ('flag', django.contrib.postgres.fields.jsonb.JSONField(default='{}', verbose_name='Flag')),
                ('company', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='customers.Company', verbose_name='company')),
                ('currency', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='customers.Currency', verbose_name='currency')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='receipt',
            name='worker',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hr.Worker', verbose_name='worker'),
        ),
        migrations.AddField(
            model_name='applieditem',
            name='item',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hr.Item', verbose_name='item'),
        ),
        migrations.AddField(
            model_name='applieditem',
            name='receipt',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hr.Receipt', verbose_name='receipt'),
        ),
    ]
