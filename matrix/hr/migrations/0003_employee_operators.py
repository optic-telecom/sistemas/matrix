# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2021-07-14 13:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0061_auto_20210714_0940'),
        ('hr', '0002_agent'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='operator',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='customers.Operator', verbose_name='operator'),
        ),
    ]
