import json
from django import forms
from localflavor.cl.forms import CLRutField
from .models import Employee, Vacation, Agent, Worker, Item
from easy_select2 import apply_select2, Select2

class HTML5DateInput(forms.DateInput):
    input_type = 'date'


class EmployeeForm(forms.ModelForm):
    rut = CLRutField()

    class Meta:
        model = Employee
        fields = ['status',
                  'rut',
                  'name',
                  'address',
                  'commune',
                  'health_care',
                  'afp',
                  'company',
                  'department',
                  'gender',
                  'service_date',
                  'base_salary',
                  'transportation_bonus',
                  'food_bonus',
                  'productivity_bonus',
                  'other_bonuses',
                  'service_notes', 
                  'operator']
        widgets = {'service_date': HTML5DateInput(format='%Y-%m-%d')}


class VacationForm(forms.ModelForm):
    class Meta:
        model = Vacation
        fields = ['start_date',
                  'end_date',
                  'notes']
        widgets = {'start_date': forms.SelectDateWidget(),
                   'end_date': forms.SelectDateWidget()}

class AgentForm(forms.ModelForm):
    class Meta:
        model = Agent
        fields = ['user',
                  'voip_extension']

class WorkersUpdateForm(forms.ModelForm):

    class Meta:
        model = Worker
        fields = [
            "first_name",
            "last_name",
            "gender",
            "address",
            "phone",
            "email",
            "base_salary",
            "currency",
            "entry_date",
            "document_type",
            "document_number",
            "job_position",
            "job_department",
            "active",
            "company",
        ]

class WorkersCreateForm(forms.ModelForm):

    class Meta:
        model = Worker
        fields = [
            "first_name",
            "last_name",
            "gender",
            "address",
            "phone",
            "email",
            "base_salary",
            "currency",
            "entry_date",
            "document_type",
            "document_number",
            "job_position",
            "job_department",
            "active",
            "company",
        ]

class ItemsUpdateForm(forms.ModelForm):

    multiplicative = forms.DecimalField(label="Factor Multiplicativo", min_value=0, required=True)
    sumative = forms.DecimalField(label="Factor Sumativo", min_value=0, required=True)

    gc = list(Worker.GENDER_CHOICES)
    gc.insert(0, (0, "Todos"))
    gc = tuple(gc)
    gender = forms.MultipleChoiceField(choices=gc, required=True)

    pc = list(Worker.POSITION_CHOICES)
    pc.insert(0, (0,"Todos"))
    pc = tuple(pc)
    position = forms.MultipleChoiceField(choices=pc, required=True)

    dc = list(Worker.DEPARTMENT_CHOICES)
    dc.insert(0, (0,"Todos"))
    dc = tuple(dc)
    department = forms.MultipleChoiceField(choices=dc, required = True)

    min_duration = forms.IntegerField(label="minima_duracion", min_value=0, required=True)

    def __init__(self, *args, **kwargs):
        super(ItemsUpdateForm, self).__init__(*args, **kwargs)
        self.fields["multiplicative"].initial = json.loads(self.instance.flag)['multiplicative']
        self.fields["sumative"].initial = json.loads(self.instance.flag)['sumative']
        if "gender" in json.loads(self.instance.flag):
            self.fields["gender"].initial = json.loads(self.instance.flag)['gender']
        if "position" in json.loads(self.instance.flag):
            self.fields["position"].initial = json.loads(self.instance.flag)['position']
        if "department" in json.loads(self.instance.flag):
            self.fields["department"].initial = json.loads(self.instance.flag)['department']
        if "min_duration" in json.loads(self.instance.flag):
            self.fields["min_duration"].initial = json.loads(self.instance.flag)['min_duration']

    class Meta:
        model = Item
        fields = [
            "name",
            "sign",
            "description",
            "company",
            "multiplicative",
            "sumative",
            "gender",
            "position",
            "department",
            "min_duration",
            "active",
        ]

class ItemsCreateForm(forms.ModelForm):

    multiplicative = forms.DecimalField(label="Factor Multiplicativo", min_value=0, required=True)
    sumative = forms.DecimalField(label="Factor Sumativo", min_value=0, required=True)

    gc = list(Worker.GENDER_CHOICES)
    gc.insert(0, (0, "Todos"))
    gc = tuple(gc)
    gender = forms.MultipleChoiceField(choices=gc, required=True)

    pc = list(Worker.POSITION_CHOICES)
    pc.insert(0, (0,"Todos"))
    pc = tuple(pc)
    position = forms.MultipleChoiceField(choices=pc, required=True)

    dc = list(Worker.DEPARTMENT_CHOICES)
    dc.insert(0, (0,"Todos"))
    dc = tuple(dc)
    department = forms.MultipleChoiceField(choices=dc, required = True)

    min_duration = forms.IntegerField(label="minima_duracion", min_value=0, required=True)

    def __init__(self, *args, **kwargs):
        super(ItemsCreateForm, self).__init__(*args, **kwargs)
        self.fields["multiplicative"].initial = 0
        self.fields["sumative"].initial = 0
        self.fields["gender"].initial = [0]
        self.fields["position"].initial = [0]
        self.fields["department"].initial = [0]
        self.fields["min_duration"].initial = 0

    class Meta:
        model = Item
        fields = [
            "name",
            "sign",
            "description",
            "company",
            "multiplicative",
            "sumative",
            "gender",
            "position",
            "department",
            "min_duration",
            "active",
        ]