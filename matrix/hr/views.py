import json, datetime, pdfkit, decimal
from django.contrib import messages
from django.core.urlresolvers import reverse
#from django.urls import reverse
from django.http import HttpResponse, Http404
from django.http.response import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.timezone import activate
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.core.cache import cache
from documents.forms import DocumentForm, DocumentUpdateForm
from documents.models import Document
from .models import Employee, Vacation, Worker, Receipt, AppliedItem
from .forms import *
from .serializers import *
from rest_framework import viewsets
from .models import *
from customers.views import IsSystemInternalPermission
from customers.models import *
from infra.serializers import *
from url_filter.integrations.drf import DjangoFilterBackend
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, User
from datetime import date, timedelta
from django.template.loader import get_template
from constance import config
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import *



class EmployeeList(LoginRequiredMixin, ListView):  # TODO: add PermissionRequiredMixin
    model = Employee

    def get_context_data(self, *args, **kwargs):
        context = super(EmployeeList, self).get_context_data(*args, **kwargs)
        return context
employee_list = EmployeeList.as_view()


class EmployeeDetail(LoginRequiredMixin, DetailView):
    model = Employee

    def get_context_data(self, *args, **kwargs):
        ctx = super(EmployeeDetail, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if self.object.operator.company.id==op.company:
                        ctx["can_show_detail"]=True
                else:
                    if self.object.operator.id==op.operator:
                        ctx["can_show_detail"]=True
            except:
                pass
        return ctx

employee_detail = EmployeeDetail.as_view()


class EmployeeVacations(LoginRequiredMixin, DetailView):
    model = Employee
    template_name = "hr/employee_vacations.html"

    def get_context_data(self, *args, **kwargs):
        ctx = super(EmployeeVacations, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if self.object.operator.company.id==op.company:
                        ctx["can_show_detail"]=True
                else:
                    if self.object.operator.id==op.operator:
                        ctx["can_show_detail"]=True
            except:
                pass
        return ctx

employee_vacations = EmployeeVacations.as_view()


class EmployeeCreate(LoginRequiredMixin, CreateView):
    model = Employee
    form_class = EmployeeForm

employee_create = EmployeeCreate.as_view()


class EmployeeUpdate(LoginRequiredMixin, UpdateView):
    model = Employee
    form_class = EmployeeForm

    def get_context_data(self, *args, **kwargs):
        ctx = super(EmployeeUpdate, self).get_context_data(*args, **kwargs)
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if self.object.operator.company.id==op.company:
                        ctx["can_show_detail"]=True
                else:
                    if self.object.operator.id==op.operator:
                        ctx["can_show_detail"]=True
            except:
                pass
        return ctx

employee_update = EmployeeUpdate.as_view()


class EmployeeDocuments(LoginRequiredMixin, DetailView):
    model = Employee
    template_name = 'hr/employee_documents.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(EmployeeDocuments, self).get_context_data(*args, **kwargs)
        ctx['documents'] = self.object.documents.filter(tag='doc')
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if self.object.operator.company.id==op.company:
                        ctx["can_show_detail"]=True
                else:
                    if self.object.operator.id==op.operator:
                        ctx["can_show_detail"]=True
            except:
                pass
        return ctx

employee_documents = EmployeeDocuments.as_view()


@login_required
def employee_documents_add(request, pk):
    employee = get_object_or_404(Employee,
                                 pk=pk)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            doc = form.save(commit=False)
            doc.content_object = employee
            doc.agent = request.user
            doc.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha subido el documento.')
            return redirect(reverse('employee-documents',
                                    kwargs={'pk': employee.pk}))
    else:
        form = DocumentForm()

    context = {'object': employee,
               'form': form,
               'cancel_url': reverse('employee-documents',
                                     kwargs={'pk': employee.pk})}
    return render(request, 'hr/employee_documents_add.html', context)


@login_required
def employee_documents_update(request, pk, document_pk):
    employee = get_object_or_404(Employee,
                                 pk=pk)
    document = get_object_or_404(Document,
                                 pk=document_pk)

    if request.method == 'POST':
        form = DocumentUpdateForm(request.POST, request.FILES)
        if form.is_valid():
            document.description = form.cleaned_data['description']
            document.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha editado el documento.')
            return redirect(reverse('employee-documents',
                                    kwargs={'pk': employee.pk}))
    else:
        form = DocumentUpdateForm(instance=document)

    context = {'object': document,
               'employee': employee,
               'form': form,
               'cancel_url': reverse('employee-documents',
                                     kwargs={'pk': employee.pk})}
    return render(request, 'hr/employee_documents_update.html', context)


@login_required
def employee_documents_delete(request, pk, document_pk):
    document = get_object_or_404(Document,
                                 pk=document_pk)
    document.delete()

    messages.add_message(request,
                         messages.SUCCESS,
                         'Se ha eliminado el documento.')

    return redirect(reverse('employee-documents',
                            kwargs={'pk': pk}))


class EmployeeServices(LoginRequiredMixin, DetailView):
    model = Employee
    template_name = 'hr/employee_services.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(EmployeeServices, self).get_context_data(*args, **kwargs)
        ctx['services'] = self.object.documents.filter(tag='con')
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator==0:
                    if self.object.operator.company.id==op.company:
                        ctx["can_show_detail"]=True
                else:
                    if self.object.operator.id==op.operator:
                        ctx["can_show_detail"]=True
            except:
                pass
        return ctx


employee_services = EmployeeServices.as_view()


@login_required
def employee_services_add(request, pk):
    employee = get_object_or_404(Employee,
                                 pk=pk)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            doc = form.save(commit=False)
            doc.content_object = employee
            doc.agent = request.user
            doc.tag = 'con'
            doc.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha subido el documento.')
            return redirect(reverse('employee-services',
                                    kwargs={'pk': employee.pk}))
    else:
        form = DocumentForm()

    context = {'object': employee,
               'form': form,
               'cancel_url': reverse('employee-services',
                                     kwargs={'pk': employee.pk})}
    return render(request, 'hr/employee_services_add.html', context)


@login_required
def employee_services_update(request, pk, document_pk):
    employee = get_object_or_404(Employee,
                                 pk=pk)
    document = get_object_or_404(Document,
                                 pk=document_pk)

    if request.method == 'POST':
        form = DocumentUpdateForm(request.POST, request.FILES)
        if form.is_valid():
            document.description = form.cleaned_data['description']
            document.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Se ha editado el documento.')
            return redirect(reverse('employee-services',
                                    kwargs={'pk': employee.pk}))
    else:
        form = DocumentUpdateForm(instance=document)

    context = {'object': document,
               'employee': employee,
               'form': form,
               'cancel_url': reverse('employee-services',
                                     kwargs={'pk': employee.pk})}
    return render(request, 'hr/employee_services_update.html', context)


@login_required
def employee_services_delete(request, pk, document_pk):
    document = get_object_or_404(Document,
                                 pk=document_pk)
    document.delete()

    messages.add_message(request,
                         messages.SUCCESS,
                         'Se ha eliminado el documento.')

    return redirect(reverse('employee-services',
                            kwargs={'pk': pk}))


@login_required
def employee_ajax(request):
    if request.is_ajax():
        # ordering
        columns = {'0': 'rut',
                   '1': 'name',
                   '2': 'address',
                   '3': 'commune',
                   '4': 'service_date',
                   '5': 'health_care',
                   '7': 'afp',
                   '8': 'company'}
        orderings = {'asc': '',
                     'desc': '-'}
        order_column = request.GET.get('order[0][column]', None)
        order_dir = request.GET.get('order[0][dir]', None)

        # search
        search = request.GET.get('search[value]', '').replace('+', '')
        start = int(request.GET.get('start', 0))
        length = int(request.GET.get('length', 10))

        if order_column and order_dir:
            all_employees = Employee.objects.order_by(orderings[order_dir]+columns[order_column])
        else:
            all_employees = Employee.objects

        username = None
        if request.user.is_authenticated():
            username = request.user.username
            user=User.objects.get(username=username)
            try:
                op=UserOperator.objects.get(user=user)
                if op.operator!=0:
                    all_employees = all_employees.filter(operator=op.operator)
                else:
                    all_employees = all_employees.filter(operator__company__id=op.company)
            except:
                pass

        raw_status = request.GET.get('status', None)
        if raw_status:
            try:
                status = int(raw_status)
            except ValueError:
                status = None
        else:
            status = None

        if status:
            all_employees = all_employees.filter(status=status)


        all_count = all_employees.count()
        if search:
            rut_regex = '^' + '\.*'.join(list(search))
            filtered_employees = all_employees.filter(Q(rut__iregex=rut_regex) |
                                                      Q(name__icontains=search) |
                                                      Q(address__unaccent__icontains=search) |
                                                      Q(commune__unaccent__icontains=search))
        else:
            filtered_employees = all_employees

        filtered_employees = filtered_employees.distinct()
            
        filtered_count = filtered_employees.count()

        if length == -1:
            sliced_employees = filtered_employees
        else:
            sliced_employees = filtered_employees[start:start+length]

        data = json.dumps({'draw': request.GET.get('draw', 1),
                           'recordsTotal': all_count,
                           'recordsFiltered': filtered_count,
                           'data': list(map(prepare_employee, sliced_employees))})

        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404


def prepare_employee(employee):
    return {'DT_RowId': employee.pk,
            'rut': employee.rut,
            'name': employee.name,
            'address': employee.address,
            'commune': employee.commune,
            'service_date': str(employee.service_date),
            'health_care': employee.get_health_care_display(),
            'afp': employee.afp,
            'company': employee.get_company_display()}


class VacationCreate(LoginRequiredMixin, CreateView):
    model = Vacation
    form_class = VacationForm

    def get_success_url(self):
        return reverse('employee-vacations', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form, *args, **kwargs):
        vacation = form.save(commit=False)
        employee = get_object_or_404(Employee, pk=self.kwargs['pk'])
        vacation.employee = employee
        vacation.save()

        messages.add_message(self.request,
                             messages.SUCCESS,
                             'Se han creado vacaciones.')
        return super(VacationCreate, self).form_valid(form)

vacation_create = VacationCreate.as_view()

## Agent
class AgentList(LoginRequiredMixin, ListView):
    model = Agent

    def get_context_data(self, *args, **kwargs):
        ctx = super(AgentList, self).get_context_data(*args, **kwargs)
        return ctx

agent_list = AgentList.as_view()

def prepare_agent(agent):
    return {'DT_RowId': agent.pk,
            'name': ' '.join([agent.user.first_name, agent.user.last_name]),
            'extension': agent.voip_extension}


@login_required
def agent_ajax(request):
    if request.is_ajax():
        # ordering
        
        columns = {'0': 'id',
                   '1': 'name',
                   '2': 'extension'}
        orderings = {'asc': '',
                     'desc': '-'}
        order_column = request.GET.get('order[0][column]', None)
        order_dir = request.GET.get('order[0][dir]', None)
        
        # search
        
        search = request.GET.get('search[value]', '').replace('+', '')
        start = int(request.GET.get('start', 0))
        length = int(request.GET.get('length', 10))

        if order_column and order_dir:
            all_agents = Agent.objects.order_by(orderings[order_dir]+columns[order_column])
        else:
            all_agents = Agent.objects.all()
        
        all_count = all_agents.count()

        if search:
            filtered_agents = all_agents.filter(Q(user__first_name__unaccent__icontains=search)|
            Q(voip_extension__icontains=search))
        else:
            filtered_agents = all_agents

        filtered_agents = filtered_agents.distinct()
            
        filtered_count = filtered_agents.count()

        if length == -1:
            sliced_agents = filtered_agents
        else:
            sliced_agents = filtered_agents[start:start+length]

        data = json.dumps({'draw': request.GET.get('draw', 1),
                           'recordsTotal': all_count,
                           'recordsFiltered': filtered_count,
                           'data': list(map(prepare_agent, sliced_agents))})

        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404


@login_required
def agent_create(request):
    context = {}
    context['form'] = AgentForm()

    try:
        if request.method == "POST":
            form = AgentForm(request.POST)
            if form.is_valid():
                agent = form.save()
                return redirect(reverse('agent-list'))
            else:
                context['form'] = form
        else:
                context['form'] = AgentForm()
    except:
        pass

    return render(request, 'hr/agent_form.html', context)

class AgentDetail(LoginRequiredMixin, DetailView):
    model = Agent

agent_detail = AgentDetail.as_view()

class AgentUpdate(LoginRequiredMixin, UpdateView):
    model = Agent
    form_class = AgentForm

    def get_success_url(self):
        return reverse('agent-detail', kwargs={'pk': self.object.pk})

agent_update = AgentUpdate.as_view()


###### API ###############

class TechnicianViewSet(viewsets.ModelViewSet):
    queryset = Technician.objects.filter(active=True)
    serializer_class = TechnicianSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

###### API INFRA ###############

class BuildingContactViewSet(viewsets.ModelViewSet):
    queryset = BuildingContact.objects.all()
    serializer_class = BuildingContactSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

class GearViewSet(viewsets.ModelViewSet):
    queryset = Gear.objects.all()
    serializer_class = GearSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

class AvailabilityViewSet(viewsets.ModelViewSet):
    queryset = Availability.objects.all()
    serializer_class = AvailabilitySerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

class NetworkEquipmentViewSet(viewsets.ModelViewSet):
    queryset = NetworkEquipment.objects.all()
    serializer_class = NetworkEquipmentSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

    
###############################    
###############################    


# Verifica la última conexión de un usuario
def last_seen(user):
    return cache.get('seen_%s' % user.username)
 
# Chequea si un usuario esta online 
def online(user):
    if last_seen(user):
        now = datetime.datetime.now()
        if now > last_seen(user) + datetime.timedelta(seconds=settings.USER_ONLINE_TIMEOUT):
            return False
        else:
            return True
    else:
        return False


def usuarios_activos(request):
    User = get_user_model()
    list_user = []
    qs = User.objects.filter(is_active=True)
    for us in qs:
        list_user.append({'usuario':us.username, 'last_seen':last_seen(us), 'online':online(us)})
    return render(request, 'hr/usuarios_activos.html', {'list_user':list_user})


def set_config(request):
    import config.urls
    from importlib import reload

    name = request.GET.get('name')
    value = request.GET.get('value')
    if hasattr(settings, name):
        if value in ['SI', 'si', 'True', 'true']:
            conf = setattr(settings, name, True)
        elif value in ['NO', 'No', 'False', 'false']:
            conf = setattr(settings, name, False)
        else:
            conf = setattr(settings, name, value)
        #print (getattr(settings, name))
        #reload(config.urls)
        # print(settings.INSTALLED_APPS)
        # print(settings.MIDDLEWARE_CLASSES)
        # print (config.urls.urlpatterns)
    return redirect('/')

class Workers(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Worker
    paginate_by = 10
    template_name = "hr/worker_list.html"
    permission_required = "hr.worker_list"

    def get_context_data(self, *args, **kwargs):
        context = super(Workers, self).get_context_data(*args, **kwargs)
        return context

worker_list = Workers.as_view()

def workers_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "first_name",
            "1": "last_name",
            "2": "gender",
            "3": "address",
            "4": "entry_date",
            "5": "job_position",
            "6": "job_department",
            "7": "active",
            "8": "company",
            "9": "pk",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_workers = Worker.objects.all().order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_workers = Worker.objects.all()

        all_workers = all_workers.distinct()
        all_count = all_workers.count()
        
        if length == -1:
            sliced_workers = all_workers
        else:
            sliced_workers = all_workers[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": all_count,
                "data": list(map(prepare_workers, sliced_workers)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_workers(worker):
    dictionary = {
        "name": str(worker.name),
        "gender":  str(worker.GENDER_CHOICES[worker.gender-1][1]),
        "address": str(worker.address),
        "entry_date": worker.entry_date.strftime("%Y/%m/%d") if worker.entry_date else "No tiene",
        "job_position": str(worker.POSITION_CHOICES[worker.job_position-1][1]),
        "job_department": str(worker.DEPARTMENT_CHOICES[worker.job_department-1][1]),
        "active": "Sí" if worker.active else "No",
        "company": str(worker.company),
        "pk": worker.pk
    }
    return dictionary


class WorkersDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Worker
    permission_required = "hr.worker_detail"
    
    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['gender_name'] = self.object.GENDER_CHOICES[self.object.gender-1][1]
        ctx['document_type_name'] = self.object.DOCUMENT_CHOICES[self.object.document_type-1][1]
        ctx['job_position_name'] = self.object.POSITION_CHOICES[self.object.job_position-1][1]
        ctx['job_department_name'] = self.object.DEPARTMENT_CHOICES[self.object.job_department-1][1]
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.company==self.object.company.id:
                    can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail  
        receipts = Receipt.objects.filter(worker = self.object)
        print(receipts)
        ctx["receipts"] = receipts
        return ctx


worker_detail = WorkersDetail.as_view()

class WorkersUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Worker
    form_class = WorkersUpdateForm
    success_message = "Se ha actualizado la información del trabajador."
    template_name = "hr/worker_update.html"
    permission_required = "hr.change_worker"

    def get_context_data(self, **kwargs):
        ctx = super(WorkersUpdate, self).get_context_data(**kwargs)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.company==self.object.company.id:
                    can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        return ctx

worker_update = WorkersUpdate.as_view()

class WorkersCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Worker
    form_class = WorkersCreateForm
    template_name = "hr/worker_create.html"
    permission_required = "hr.add_worker"

    def get_context_data(self, **kwargs):
        ctx = super(WorkersCreate, self).get_context_data(**kwargs)
        return ctx

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)

        self.object = form.save()
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                self.object.company=Company.objects.get(id=op.company)
                self.object.save()
            except:
                pass
        
        return super(WorkersCreate, self).form_valid(form)

worker_create = WorkersCreate.as_view()

class Items(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Item
    paginate_by = 10
    template_name = "hr/item_list.html"
    permission_required = "hr.item_list"

    def get_context_data(self, *args, **kwargs):
        context = super(Items, self).get_context_data(*args, **kwargs)
        return context

item_list = Items.as_view()

def items_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "name",
            "1": "multiplicative",
            "2": "sumative",
            "3": "sign",
            "4": "description",
            "5": "company",
            "6": "pk",
            "7": "active"
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_items = Item.objects.all().order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_items = Item.objects.all()

        all_items = all_items.distinct()
        all_count = all_items.count()
        
        if length == -1:
            sliced_items = all_items
        else:
            sliced_items = all_items[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": all_count,
                "data": list(map(prepare_items, sliced_items)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_items(item):
    multiplicative = json.loads(item.flag)['multiplicative']
    sumative = json.loads(item.flag)['sumative']
    dictionary = {
        "name": str(item.name),
        "multiplicative": str(multiplicative),
        "sumative": str(sumative),
        "sign": "Aumento" if item.sign == 1 else "Descuento",
        "description": str(item.description),
        "company": str(item.company),
        "active": str(item.active),
        "pk": item.pk,
    }
    return dictionary

class ItemsDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Item
    permission_required = "hr.item_detail"
    
    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.company==self.object.company.id:
                    can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail  
        w = Worker.objects.all()
        #ctx['sign'] = self.object.SIGN_CHOICES[self.object.sign-1][1]
        ctx['multiplicative'] = json.loads(self.object.flag)['multiplicative']
        ctx['sumative'] = json.loads(self.object.flag)['sumative']
        position = json.loads(self.object.flag)['position']
        positions = []
        for p in position:
            if int(p) == 0:
                positions.append('Todos')
            else:
                positions.append(w[0].POSITION_CHOICES[int(p)-1][1])
        ctx['positions'] = positions
        department = json.loads(self.object.flag)['department']
        departments = []
        for d in department:
            if int(d) == 0:
                departments.append('Todos')
            else:
                departments.append(w[0].DEPARTMENT_CHOICES[int(d)-1][1])
        ctx['departments'] = departments
        ctx['min_duration'] = json.loads(self.object.flag)['min_duration']
        return ctx


item_detail = ItemsDetail.as_view()

class ItemsUpdate(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Item
    form_class = ItemsUpdateForm
    success_message = "Se ha actualizado la información del Item."
    template_name = "hr/item_update.html"
    permission_required = "hr.change_item"

    def get_context_data(self, **kwargs):
        ctx = super(ItemsUpdate, self).get_context_data(**kwargs)
        can_show_detail=False
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.company==self.object.company.id:
                    can_show_detail=True
            except:
                pass
        ctx["can_show_detail"]=can_show_detail
        return ctx

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        print(form.data)
        name = form.data['name']
        if 'active'in form.data:
            active = True
        else:
            active = False
        sign = form.data['sign']
        description = form.data['description']
        company = form.data['company']
        gender = form.data.getlist('gender')
        position = form.data.getlist('position')
        department = form.data.getlist('department')
        min_duration = form.data['min_duration']
        flagaux = {
            'multiplicative': form.data['multiplicative'],
            'sumative': form.data['sumative'],
            'gender': gender,
            'position': position,
            'department': department,
            'min_duration': min_duration,
        }

        Item.objects.filter(pk = self.object.pk).update(
            name = name,
            active = active,
            sign = sign,
            description = description,
            company = Company.objects.get(pk = company)
        )
        self.object.flag = json.dumps(flagaux)
        self.object.save()

        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                self.object.company=Company.objects.get(id=op.company)
                self.object.save()
            except:
                pass
        
        return redirect(reverse("item-detail", kwargs={"pk": self.object.pk}))

item_update = ItemsUpdate.as_view()

class ItemsCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Item
    form_class = ItemsCreateForm
    template_name = "hr/item_create.html"
    permission_required = "hr.add_item"

    def get_context_data(self, **kwargs):
        ctx = super(ItemsCreate, self).get_context_data(**kwargs)
        return ctx

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        name = form.data['name']
        if 'active'in form.data:
            active = True
        else:
            active = False
        sign = form.data['sign']
        description = form.data['description']
        company = form.data['company']
        gender = form.data.getlist('gender')
        position = form.data.getlist('position')
        department = form.data.getlist('department')
        min_duration = form.data['min_duration']

        flagaux = {
            'multiplicative': form.data['multiplicative'],
            'sumative': form.data['sumative'],
            'gender': gender,
            'position': position,
            'department': department,
            'min_duration': min_duration,
        }

        Item.objects.create(
            name = name,
            flag = json.dumps(flagaux),
            active = active,
            sign = sign,
            description = description,
            company = Company.objects.get(pk = company)
        )
        
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                self.object.company=Company.objects.get(id=op.company)
                self.object.save()
            except:
                pass
        
        return redirect(reverse("item-list"))

item_create = ItemsCreate.as_view()

def item_delete(request, pk):
    item = get_object_or_404(Item, pk=pk)
    print(item)
    
    if item:
        item.delete()
    
    return HttpResponseRedirect(reverse_lazy("item-list"))

def activate_item(request):
    item_id = json.loads(request.GET["item_id"])
    check = json.loads(request.GET["checked"])
    
    item = Item.objects.get(pk = int(item_id))
    item.active = check
    item.save()
    print(item.active, check)
    data = json.dumps({"data": check})
    return HttpResponse(data, content_type="application/json")

class Receipts(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Receipt
    paginate_by = 10
    template_name = "hr/receipt_list.html"
    permission_required = "hr.receipt_list"

    def get_context_data(self, *args, **kwargs):
        context = super(Receipts, self).get_context_data(*args, **kwargs)
        return context

receipt_list = Receipts.as_view()

def receipts_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "pk",
            "1": "worker",
            "2": "month",
            "3": "year",
            "4": "amount",
            "5": "currency",
            "6": "exchange_rate",
            "7": "issue_date",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_receipts = Receipt.objects.all().order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_receipts = Receipt.objects.all()

        all_receipts = all_receipts.distinct()
        all_count = all_receipts.count()
        
        if length == -1:
            sliced_receipts = all_receipts
        else:
            sliced_receipts = all_receipts[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": all_count,
                "data": list(map(prepare_receipts, sliced_receipts)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_receipts(receipt):
    
    dictionary = {
        "pk": receipt.pk,
        "worker": str(receipt.worker.name),
        "month": str(receipt.get_month_display()),
        "year": str(receipt.year),
        "amount": str(receipt.amount),
        "currency": str(receipt.currency.name),
        "exchange_rate": str(receipt.exchange_rate),
        "issue_date": receipt.issue_date.strftime("%Y/%m/%d") if receipt.issue_date else "No tiene",
    }
    return dictionary

class ReceiptsDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Receipt
    permission_required = "hr.receipt_detail"
    
    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        can_show_detail=True
        if self.request.user.is_authenticated():
            username = self.request.user.username
            user=User.objects.get(username=username)
            companies=UserCompany.objects.get(user=user).company.all()
            try:
                op=UserOperator.objects.get(user=user)
                if op.company==self.object.company.id:
                    can_show_detail=True
            except:
                pass
        ctx["can_show_detail"] = can_show_detail
        
        appliedItems = AppliedItem.objects.filter(receipt = self.object)
        ctx['applied_items'] = appliedItems
        return ctx


receipt_detail = ReceiptsDetail.as_view()

def receipt_delete(request, pk):
    receipt = get_object_or_404(Receipt, pk=pk)
    print(receipt)
    
    if receipt:
        receipt.delete()
    
    return HttpResponseRedirect(reverse_lazy("receipt-list"))


def item_process(request):
    worker = Worker.objects.filter(active = True).order_by('pk')

    item = Item.objects.filter(active = True).order_by('pk')
    bonification = []
    for w in worker:
        info = [w.name, w.base_salary]
        bonos = []
        for i in item:
            if i.sign == 1:
                flag = json.loads(i.flag)
                amount = 0
                if '0' in flag['gender'] or str(w.gender) in flag['gender']:
                    if '0' in flag['position'] or str(w.job_position) in flag['position']:
                        if '0' in flag['department'] or str(w.job_department) in flag['department']:
                            if w.entry_date < timezone.now() - timezone.timedelta(days=30*int(flag['min_duration'])):
                                multiplicative = float(flag['multiplicative'])
                                sumative = float(flag['sumative'])
                                amount = w.base_salary*multiplicative + sumative
                bonos.append(amount)
        info.append(bonos)
        bonification.append(info)

    bono_item = Item.objects.filter(sign=1).filter(active = True).order_by('pk')

    discountation = []
    for w in worker:
        info = [w.name, w.base_salary]
        discounts = []
        for i in item:
            if i.sign == 2:
                flag = json.loads(i.flag)
                amount = 0
                if '0' in flag['gender'] or str(w.gender) in flag['gender']:
                    if '0' in flag['position'] or str(w.job_position) in flag['position']:
                        if '0' in flag['department'] or str(w.job_department) in flag['department']:
                            if w.entry_date < timezone.now() - timezone.timedelta(days=30*int(flag['min_duration'])):
                                multiplicative = float(flag['multiplicative'])
                                sumative = float(flag['sumative'])
                                amount = w.base_salary*multiplicative + sumative
                discounts.append(amount)
        info.append(discounts)
        discountation.append(info)

    discount_item = Item.objects.filter(sign=2).filter(active = True).order_by('pk')

    workers_total = []
    for w in worker:
        workers = []
        workers.append(w.name)
        workers.append(w.base_salary)
        workers.append(0)
        workers_total.append(workers)

    
    bas = BankAccount.objects.all()
    bank_accounts = []
    for ba in bas:
        bank_accounts.append([ba.pk, str(ba.tag + ' - ' + ba.account_number)])

    data = {"worker": worker, "bono_item":bono_item, "bonification": bonification,
            "discount_item": discount_item, "discountation": discountation, 
            "workers_total": workers_total, "bank_accounts": bank_accounts}

    return render(
        request,
        "hr/item_process.html",
        data
    )

def workers_wizard_ajax(request):
    if request.is_ajax():
        # Ordering
        columns = {
            "0": "first_name",
            "1": "last_name",
            "2": "gender",
            "3": "address",
            "4": "entry_date",
            "5": "job_position",
            "6": "job_department",
            "7": "active",
            "8": "company",
            "9": "pk",
        }
        orderings = {"asc": "", "desc": "-"}
        order_column = request.GET.get("order[0][column]", None)
        order_dir = request.GET.get("order[0][dir]", None)
        start = int(request.GET.get("start", 0))
        length = int(request.GET.get("length", 10))

        if order_column and order_dir:
            all_workers = Worker.objects.all().order_by(
                orderings[order_dir] + columns[order_column]
            )
        else:
            all_workers = Worker.objects.all()

        all_workers = all_workers.distinct()
        all_count = all_workers.count()
        
        if length == -1:
            sliced_workers = all_workers
        else:
            sliced_workers = all_workers[start : start + length]
        data = json.dumps(
            {
                "draw": request.GET.get("draw", 1),
                "recordsTotal": all_count,
                "recordsFiltered": all_count,
                "data": list(map(prepare_workers, sliced_workers)),
            }
        )
        return HttpResponse(data, content_type="application/json")
    else:
        raise Http404

def prepare_workers_wizard(worker):
    dictionary = {
        "name": str(worker.name),
        "gender":  str(worker.GENDER_CHOICES[worker.gender-1][1]),
        "address": str(worker.address),
        "entry_date": worker.entry_date.strftime("%Y/%m/%d") if worker.entry_date else "No tiene",
        "job_position": str(worker.POSITION_CHOICES[worker.job_position-1][1]),
        "job_department": str(worker.DEPARTMENT_CHOICES[worker.job_department-1][1]),
        "active": "Sí" if worker.active else "No",
        "company": str(worker.company),
        "pk": worker.pk
    }
    return dictionary

def receive_info(request):
    
    worker_data = request.POST.get('data')
    worker_data =json.loads(worker_data)
    selected_month = request.POST.get('month')
    selected_year = request.POST.get('year')
    selected_bank_account = request.POST.get('bank_account')
    month_traductor = {
        "Enero":1, "Febrero":2, "Marzo":3, "Abril":4, 
        "Mayo":5, "Junio":6, "Julio":7, "Agosto":8,
        "Septiembre": 9, "Octubre": 10, "Noviembre":11, "Diciembre":12
    }
    
    for data in worker_data:
        current_receipt = Receipt.objects.create(
            month = month_traductor[selected_month],
            year = selected_year,
            issue_date = timezone.now(),
            amount = data[2],
            exchange_rate = 1,
            worker = Worker.objects.get(pk = int(data[0])),
            currency = Currency.objects.get(pk = 1),
            bank_account = BankAccount.objects.get(pk = int(selected_bank_account))
        )

        for bono in data[3]:
            b = Item.objects.get(name=bono[0])
            if float(bono[1]) != 0:
                AppliedItem.objects.create(
                    real_amount = float(bono[1]),
                    receipt = current_receipt,
                    item = b
                )

        for disc in data[4]:
            d = Item.objects.get(name=disc[0])
            if float(disc[1]) != 0:
                AppliedItem.objects.create(
                    real_amount = float(disc[1]),
                    receipt = current_receipt,
                    item = d
                )

    return redirect(reverse("item-process"))

def download_receipt_PDF(request, pk):
    r = Receipt.objects.get(pk = pk)
    applied_items = AppliedItem.objects.filter(receipt = r)
    total_assigns = 0
    assign = []
    for ai in applied_items:
        if ai.item.sign == 1:
            total_assigns += float(ai.real_amount)
            assign.append((ai.item.name, float(ai.real_amount)))
    total_discounts = float(0)
    discount = []
    for ai in applied_items:
        if ai.item.sign == 2:
            total_discounts += float(ai.real_amount)
            discount.append((ai.item.name, float(ai.real_amount)))
    final_salary = float(r.amount) - total_assigns + total_discounts
    total_assigns += final_salary
    assign.insert(0,("Sueldo", final_salary))
    worker_currency = r.worker.currency.name
    worker_currency_symbol = r.worker.currency.symbol
    receipt_currency = r.currency.name
    receipt_currency_symbol = r.currency.symbol
    two_lines = 0
    if worker_currency == receipt_currency:
        two_lines = 1
    exchange_rate = r.exchange_rate
    now = timezone.now()
    date = str(now.date().day) + '/' + str(now.date().month) + '/' + str(now.date().year)
    receipt_total = total_assigns - total_discounts
    worker_total = float(receipt_total*float(exchange_rate))
    domain = getattr(config, "matrix_server")
    data = {
        "month": r.get_month_display(),
        "year": r.year,
        "worker_name": r.worker.name,
        "document_number": r.worker.document_number,
        "phone": r.worker.phone,
        "email": r.worker.email,
        "address": r.worker.address,
        "assigns": assign,
        "total_assigns": total_assigns,
        "discounts": discount,
        "total_discounts": total_discounts,
        "worker_currency": worker_currency,
        "worker_currency_symbol": worker_currency_symbol,
        "receipt_currency": receipt_currency,
        "receipt_currency_symbol": receipt_currency_symbol,
        "exchange_rate": exchange_rate,
        "date": date,
        "receipt_total": receipt_total,
        "worker_total": worker_total,
        "two_lines": two_lines,
        "domain": str(domain),
    }

    template = get_template('hr/receipt_template.html')
    html = template.render(data)

    options = {"page-size": "letter", "encoding": "UTF-8", "enable-local-file-access": None}
    pdf = pdfkit.from_string(html, False, options)

    filename_pdf = "Recibo " + str(r.get_month_display()) + ' ' +  str(r.year) + ' ' + str(r.worker.name)

    # Download settings
    response = HttpResponse(pdf, content_type="application/pdf")
    response["Content-Disposition"] = (
        'attachment; filename="' + str(filename_pdf) + '.pdf"'
    )
    
    return response
    #return render(request, 'hr/receipt_template.html', data)