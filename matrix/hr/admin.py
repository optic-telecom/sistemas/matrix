from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from .models import (Technician, Skill, Availability,
    Downtime, Employee, Vacation, Agent)
from import_export.admin import ImportExportModelAdmin
from .resources import (TechnicianResource, SkillResource,
    AvailabilityResource, DowntimeResource, EmployeeResource, 
    VacationResource, AgentResource )


@admin.register(Technician)
class TechnicianAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    resource_class = TechnicianResource


@admin.register(Skill)
class SkillAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    resource_class = SkillResource


@admin.register(Availability)
class AvailabilityEquipmentAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    raw_id_fields = ('technician',)
    resource_class = AvailabilityResource


@admin.register(Downtime)
class DowntimeEquipmentAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    raw_id_fields = ('technician',)
    resource_class = DowntimeResource


@admin.register(Employee)
class EmployeeAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ['rut', 'name', 'department', 'service_date', 'vacation_days_earned', 'vacation_days_used', 'vacation_days_balance']
    list_filter = ['department']
    resource_class = EmployeeResource

@admin.register(Vacation)
class VacationAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    resource_class = VacationResource

@admin.register(Agent)
class VacationAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    resource_class = AgentResource