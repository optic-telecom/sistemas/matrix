from django.conf.urls import url
from .views import (activate_item, employee_list, employee_ajax, employee_detail,
        employee_create, employee_update, employee_vacations,
        employee_documents, employee_documents_add, employee_documents_update,
        employee_documents_delete, employee_services, employee_services_add,
        employee_services_update, employee_services_delete, item_process, vacation_create,
        agent_list, agent_ajax, agent_detail, agent_create, agent_update, worker_list,
        workers_ajax, worker_detail, worker_update, worker_create, item_list,
        items_ajax, item_detail, item_create, item_update, receipt_list, receipts_ajax,
        receipt_detail, item_process, workers_wizard_ajax, receive_info, download_receipt_PDF,
        item_delete, receipt_delete, workers_wizard_ajax )
from .views import (TechnicianViewSet,BuildingContactViewSet,GearViewSet,
        AvailabilityViewSet,NetworkEquipmentViewSet, usuarios_activos, set_config)

from hr.autocompletes import TechnicianAutocomplete

# API section
from customers.urls import router
router.register(r"technicians", TechnicianViewSet)
router.register(r"building-contacts", BuildingContactViewSet)
router.register(r"gears", GearViewSet)
router.register(r"availabilities", AvailabilityViewSet)
router.register(r"network-equipments", NetworkEquipmentViewSet)

urlpatterns = [
    url(r'^employees/$', employee_list, name='employee-list'),
    url(r'^employees/actives$', usuarios_activos, name='usuarios-activos'),
    url(r'^employees/(?P<pk>\d+)/$', employee_detail, name='employee-detail'),
    url(r'^employees/add/$', employee_create, name='employee-add'),
    url(r'^employees/(?P<pk>\d+)/edit/$', employee_update, name='employee-update'),
    url(r'^employees/(?P<pk>\d+)/documents/$', employee_documents, name='employee-documents'),
    url(r'^employees/(?P<pk>\d+)/documents/add/$', employee_documents_add, name='employee-documents-add'),
    url(r'^employees/(?P<pk>\d+)/documents/(?P<document_pk>\d+)/edit/$', employee_documents_update, name='employee-documents-update'),
    url(r'^employees/(?P<pk>\d+)/documents/(?P<document_pk>\d+)/delete/$', employee_documents_delete, name='employee-documents-delete'),    url(r'^employees/(?P<pk>\d+)/services/$', employee_services, name='employee-services'),
    url(r'^employees/(?P<pk>\d+)/services/add/$', employee_services_add, name='employee-services-add'),
    url(r'^employees/(?P<pk>\d+)/services/(?P<document_pk>\d+)/edit/$', employee_services_update, name='employee-services-update'),
    url(r'^employees/(?P<pk>\d+)/services/(?P<document_pk>\d+)/delete/$', employee_services_delete, name='employee-services-delete'),
    url(r'^employees/(?P<pk>\d+)/payslips/$', employee_detail, name='employee-payslips'),
    url(r'^employees/(?P<pk>\d+)/vacations/$', employee_vacations, name='employee-vacations'),
    url(r'^employees/(?P<pk>\d+)/vacations/add/$', vacation_create, name='employee-vacations-add'),
    url(r'^employees/ajax/$', employee_ajax, name='employee-ajax'),
    url(r"^agents/$", agent_list, name="agent-list"),
    url(r"^agents/ajax/$", agent_ajax, name="agent-ajax"),
    url(r"^agents/(?P<pk>\d+)/$", agent_detail, name="agent-detail"),
    url(r"^agents/new/$", agent_create, name="agent-create"),
    url(r"^agents/(?P<pk>\d+)/edit/$", agent_update, name="agent-update"),
    url(r"^set_config$", set_config, name="set-config"),
    url(r"^tecnicos-ac/", TechnicianAutocomplete.as_view(), name="tecnicos-ac"),
    url(r"^workers/$", worker_list, name='worker-list'),
    url(r"^workers/ajax/$", workers_ajax, name='worker-ajax'),
    url(r"^workers/(?P<pk>\d+)/$", worker_detail, name='worker-detail'),
    url(r"^workers/(?P<pk>\d+)/edit/$", worker_update, name='worker-update'),
    url(r"^workers/new/$", worker_create, name ='worker-create'),
    url(r"^items/$", item_list, name='item-list'),
    url(r"^items/ajax/$",items_ajax, name='item-ajax'),
    url(r"^items/(?P<pk>\d+)/$", item_detail, name='item-detail'),
    url(r"^items/(?P<pk>\d+)/edit/$", item_update, name='item-update'),
    url(r"^items/(?P<pk>\d+)/delete/$", item_delete, name='item-delete'),
    url(r"^items/activate/$", activate_item, name='activate-item'),
    url(r"^items/new/$", item_create, name = 'item-create'),
    url(r"^receipts/$", receipt_list, name = 'receipt-list'),
    url(r"^receipts/ajax/$", receipts_ajax, name='receipt-ajax'),
    url(r"^receipts/(?P<pk>\d+)/$", receipt_detail, name='receipt-detail'),
    url(r"^receipts/(?P<pk>\d+)/delete/$", receipt_delete, name='receipt-delete'),
    url(r"^item_process/$", item_process, name='item-process'),
    url(r"^item_process/worker_ajax/$", workers_wizard_ajax, name='worker-wizard-ajax'),
    url(r"^item_process/receive_info/$", receive_info, name='receive-info'),
    url(r"^receipts/(?P<pk>\d+)/download_pdf/$", download_receipt_PDF, name ="download-receipt-pdf"),
]
