from import_export import resources
from .models import (ServiceSumSnapshot, ServiceStatusLog,
    ServiceChangeStatusLog)

class ServiceSumSnapshotResource(resources.ModelResource):

    class Meta:
        model = ServiceSumSnapshot

class ServiceStatusLogResource(resources.ModelResource):

    class Meta:
        model = ServiceStatusLog

class ServiceChangeStatusLogResource(resources.ModelResource):

    class Meta:
        model = ServiceChangeStatusLog