from rest_framework import serializers
from .models import (ServiceSumSnapshot, ServiceStatusLog)
from drf_queryfields import QueryFieldsMixin

class ServiceSumSnapshotSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = ServiceSumSnapshot
        fields = ('created_at','status','count')

class ServiceStatusLogSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):

    seller = serializers.HyperlinkedRelatedField(view_name='api:seller-detail', read_only=True)
    class Meta:
        model = ServiceStatusLog
        fields = ('created_at','status','seller','node','technician','service')