# coding: utf-8
import requests 
from django.core.management import BaseCommand
from django.utils import timezone
from reports.models import ServiceStatusLog


class Command(BaseCommand):

    def send_data_to_sentinel(self):
        from django.db.models import Q
        queryset = ServiceStatusLog.objects.exclude(Q(service=None)|Q(node=None)).order_by('-id')
        print("==============================================================")
        print(f"========== COPIANDO {queryset.count()}  REGISTROS HACIA SENTINEL ==========")
        print("==============================================================")

        url_base = 'http://localhost:8000/'

        for qs in queryset:
            print(qs.service)
            obj = {
                'created_at':qs.created_at,
                'status':qs.status,
                'seller':qs.seller.pk if qs.seller else None,
                'node':qs.node.pk if qs.node else None,
                'technician':qs.technician.pk if qs.technician else None,
                'service':qs.service.number if qs.service else None
            }
            url = f'{url_base}api/v1/ssl/' 
            token_sentinel = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s"
            headers = {'Authorization':token_sentinel}
            try:
                r = requests.post(url,headers=headers,data=obj,verify=False)
            except Exception as e:
                print(str(e))
                # r = requests.post(url,headers=headers,data=obj,verify=False)
            
            if r.status_code == 201:
                print(f'Guardado el SSL con el id {qs.pk} en Sentinel')
            
            print(r.text)
            

    def handle(self, *args, **options):
        self.send_data_to_sentinel()

        
