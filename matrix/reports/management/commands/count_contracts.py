# coding: utf-8
from django.core.management import BaseCommand
from django.db.models import Count
from django.utils import timezone
from customers.models import Service
from reports.models import ServiceSumSnapshot


class Command(BaseCommand):
    def handle(self, *args, **options):
        today = timezone.now().date()
        summary = Service.objects.values('status').annotate(Count('id')).order_by()

        for status_info in summary:
            ServiceSumSnapshot.objects.update_or_create(created_at=today,
                                                         status=status_info['status'],
                                                         defaults={'count': status_info['id__count']})
