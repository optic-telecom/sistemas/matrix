import sys, os, io, json, string, random, pdfkit, xlsxwriter, zipfile, calendar, datetime, math, django, urllib3, pytz
import petl as etl
import unidecode
from customers.models import *
from django.shortcuts import render, redirect
from .models import (ServiceStatusLog, ServiceSumSnapshot)
from rest_framework import viewsets
from customers.views import IsSystemInternalPermission
from .serializers import (ServiceSumSnapshotSerializer,
    ServiceStatusLogSerializer)
from url_filter.integrations.drf import DjangoFilterBackend
from dateutil import parser as date_parser
from dateutil.relativedelta import relativedelta
from datetime import date, timedelta
from django.db.models import Value, Q, F, Max, Count, Sum, Min
from django.db.models.functions import Concat, ExtractMonth, ExtractYear
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from hr.models import *
from infra.models import Node
import xlsxwriter
from tasks.celery import celery_app
from constance import config
import base64
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.core.urlresolvers import reverse

def price_tag(number):
    value = ('%.6f' % number).rstrip('0').rstrip('.')
    return intcomma(value).replace(".", "*").replace(",", ".").replace("*", ",")

def counts_chart(request):
    context = {}
    return render('reports/counts_chart.html', context)

###### API ###############

class ServiceSumSnapshotViewSet(viewsets.ModelViewSet):
    queryset = ServiceSumSnapshot.objects.all()
    serializer_class = ServiceSumSnapshotSerializer
    permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

class ServiceStatusLogViewSet(viewsets.ModelViewSet):
    queryset = ServiceStatusLog.objects.all()
    serializer_class = ServiceStatusLogSerializer
    # permission_classes = (IsSystemInternalPermission,)
    filter_fields = '__all__'
    filter_backends = [DjangoFilterBackend]

#####################
def name_filter(qmodel,filter_name):
    q_objects = Q()
    splitted_name_queries = [Q(complete_name__unaccent__icontains=term) for term in filter_name.split(" ")]
    q_name_objects = Q()
    for q in splitted_name_queries:
        q_name_objects.add(q, Q.AND)
    q_objects |= q_name_objects
    qmodel = qmodel.filter(q_objects)
    return qmodel

def address_filter(qmodel,filter_address):
    q_objects = Q()
    q_objects = Q(db_composite_address__unaccent__icontains=filter_address)
    qmodel = qmodel.filter(q_objects)
    return qmodel

def network_filter(qmodel, filter_network, base_model):
    filter_network = int(filter_network)
    if base_model == '0':
        NETWORK_STATUSES = {1: {"service__seen_connected": True},2: {"service__seen_connected": False},3: {"service__seen_connected": None},}
    elif base_model == '1':
        NETWORK_STATUSES = {1: {"seen_connected": True},2: {"seen_connected": False},3: {"seen_connected": None},}
    qmodel = qmodel.filter(**NETWORK_STATUSES.get(filter_network, {}))
    return qmodel

def balance_filter(qmodel, filter_from_balance, filter_to_balance):
    filter_from_balance = int(filter_from_balance)
    filter_to_balance = int(filter_to_balance)
    qmodel = qmodel.select_related('balance').filter(Q(balance__balance__gte=filter_from_balance), Q(balance__balance__lte=filter_to_balance) )
    return qmodel

def plan_filter(qmodel, filter_plan, base_model):
    filter_plan = int(filter_plan)
    if base_model == '0':
        qmodel = qmodel.filter(service__plan__pk=filter_plan)
    elif base_model == '1':
        qmodel = qmodel.filter(plan__pk=filter_plan)
    return qmodel

def customer_kind_filter(qmodel, filter_customer_kind):
    filter_customer_kind = int(filter_customer_kind)
    qmodel = qmodel.filter(kind=filter_customer_kind)
    return qmodel

def created_filter(qmodel, filter_created_initDate, filter_created_finalDate):
    qmodel = qmodel.filter(Q(created_at__gte = filter_created_initDate), Q(created_at__lte = filter_created_finalDate))
    return qmodel

def activated_filter(qmodel, filter_activated_initDate, filter_activated_finalDate):
    qmodel = qmodel.filter(Q(activated_on__gte = filter_activated_initDate), Q(activated_on__lte = filter_activated_finalDate))
    return qmodel

def installed_filter(qmodel, filter_installed_initDate, filter_installed_finalDate):
    qmodel = qmodel.filter(Q(installed_on__gte = filter_installed_initDate), Q(installed_on__lte = filter_installed_finalDate))
    return qmodel

def uninstalled_filter(qmodel, filter_uninstalled_initDate, filter_uninstalled_finalDate):
    qmodel = qmodel.filter(Q(uninstalled_on__gte = filter_uninstalled_initDate), Q(uninstalled_on__lte = filter_uninstalled_finalDate))
    return qmodel

def expired_filter(qmodel, filter_expired_initDate, filter_expired_finalDate):
    qmodel = qmodel.filter(Q(expired_on__gte = filter_expired_initDate), Q(expired_on__lte = filter_expired_finalDate))
    return qmodel

def technology_filter(qmodel, filter_technology):
    filter_technology = int(filter_technology)
    qmodel = qmodel.filter(technology_kind = filter_technology)
    return qmodel

def status_filter(qmodel, filter_status, filter_status_initDate, filter_status_finalDate):
    qmodel = qmodel.filter(status = int(filter_status))
    if filter_status_initDate and filter_status_finalDate:
        auxService = []
        for s in qmodel:
            last_status_change = list(s.status_history)[0].created_at
            try:
                filter_status_initDate = pytz.UTC.localize(filter_status_initDate)
            except:
                pass
            try:
                filter_status_finalDate = pytz.UTC.localize(filter_status_finalDate)
            except:
                pass
            try:
                last_status_change = pytz.UTC.localize(last_status_change)
            except:
                pass
            if filter_status_initDate <=  last_status_change and last_status_change <= filter_status_finalDate:
                auxService.append(s.id)
        qmodel = qmodel.filter(id__in = auxService)
    return qmodel

def payment_method_filter(qmodel, filter_payment_method):
    filter_payment_method = int(filter_payment_method)
    qmodel = qmodel.filter(kind = filter_payment_method)
    return qmodel

def payment_manual_filter(qmodel, filter_payment_manual):
    if filter_payment_manual == "1":
        qmodel = qmodel.filter(manual=True)
    elif filter_payment_manual == "2":
        qmodel = qmodel.filter(manual=False)
    return qmodel

def cliente_operator_filter(qmodel, filter_cliente_operator):
    clientes = (
        Service.objects.filter(operator=int(filter_cliente_operator))
        .order_by("customer_id")
        .values_list("customer_id")
        .distinct("customer_id")
        )
    qmodel = qmodel.filter(customer_id__in=clientes)
    return qmodel

def payment_agent_filter(qmodel, filter_payment_agent):
    filter_payment_agent = int(filter_payment_agent)
    payment_ids = Payment.history.filter(
    history_user_id=filter_payment_agent, history_type="+"
    ).values_list("id", flat=True)
    qmodel = qmodel.filter(id__in=payment_ids)
    return qmodel

def payment_operator_filter(qmodel, filter_payment_operator):
    filter_payment_operator = int(filter_payment_operator)
    qmodel = qmodel.filter(operator = filter_payment_operator)
    return qmodel

def payment_conciliado_filter(qmodel, filter_payment_conciliado):
    filter_payment_conciliado = (int(filter_payment_conciliado)-1)
    qmodel = qmodel.filter(cleared = filter_payment_conciliado)
    return qmodel

def servicio_asignado_filter(qmodel, filter_servicio_asignado):
    if filter_servicio_asignado == '1':
        qmodel = qmodel.exclude(service = None)
    elif filter_servicio_asignado == '2':
        qmodel = qmodel.filter(service = None)
    return qmodel

def paid_on_filter(qmodel, filter_payment_initDate, filter_payment_finalDate):
    qmodel = qmodel.filter(Q(paid_on__gte = filter_payment_initDate), Q(paid_on__lte = filter_payment_finalDate))
    return qmodel

def invoice_kind_filter(qmodel, filter_invoice_kind):
    filter_invoice_kind = int(filter_invoice_kind)
    qmodel = qmodel.filter(kind = filter_invoice_kind)
    return qmodel

def amount_filter(qmodel, filter_from_amount, filter_to_amount):
    filter_from_amount = int(filter_from_amount)
    filter_to_amount = int(filter_to_amount)
    qmodel = qmodel.filter(Q(total__gte = filter_from_amount), Q(total__lte = filter_to_amount))
    return qmodel

def invoice_pagado_filter(qmodel, filter_invoice_pagado):
    if filter_invoice_pagado == '1':
        qmodel = qmodel.exclude(paid_on = None)
    elif filter_invoice_pagado == '2':
        qmodel = qmodel.filter(paid_on = None)
    return qmodel

def invoice_operator_filter(qmodel, filter_invoice_operator):
    filter_invoice_operator = int(filter_invoice_operator)
    qmodel = qmodel.filter(operator = filter_invoice_operator)
    return qmodel


def general_report(request):
    base_model = [(0,"Clientes"),(1,"Servicios"),(2,"Pagos"),(3,"Boletas"),]
    c_customer = [
        (0, 'Rut'), (1,'Nombre'), (2,'Genero'), (3,'Razón Social'), (4,'Email'),
        (5,'Teléfono'), (6,'Dirección'), (7,'Comuna'), (8,'Estado de red'), (9,'Balance'),
        (10,'Fecha de Creación')
    ]
    c_service = [
        (0, 'Numero'), (1, 'Rut'), (2,'Nombre Cliente'), (3,'Razón Social'), (4,'Email'),
        (5,'Teléfono'), (6,'Dirección'), (7,'Comuna'), (8,'Estado de red'),
        (9, 'Status'), (10, 'Balance'), (11, 'Operador'), (12, 'Plan'), (13, 'Fecha de creación'),
        (14, 'Fecha de activación'), (15, 'Fecha de Instalación'), (16, 'Fecha de Desinstalación'),
        (17, 'Fecha de expiración'), (18, 'Tecnología')
    ]
    c_payment = [
        (0, 'Fecha de Creación'), (1, 'Id'), (2, 'Nombre Cliente'), (3, 'Operador Cliente'),
        (4, 'Fecha de Pago'), (5, 'Monto'), (6, 'Manual'), (7, 'Tipo'), (8, 'Agente'),
        (9, 'Operador de Pago'), (10, 'Conciliado'),
    ]
    c_invoice = [
        (0, 'Fecha de Creación'), (1, 'Id'), (2, 'Nombre Cliente'), (3, 'Tipo'), 
        (4, 'Monto'), (5, 'Pagada'), (6, 'Operador Factura'), ( 7, 'Operador Cliente'),
        (8, 'Descripción'), (9, 'Enviada')
    ]
    fields = [c_customer, c_service, c_payment, c_invoice]
    status_displays = dict(Service.STATUS_CHOICES)
    statuses = [
        {"code": status, "name": status_displays[status]}
        for status in sorted(
            Service.objects.values_list("status", flat=True)
            .distinct()
            .order_by("status")
        )
    ]
    plans = [{"code": plan.pk, "name": plan.name} for plan in Plan.objects.all()]
    sellers = Seller.objects.all()
    technicians = Technician.objects.filter(active=True)
    kind_displays = dict(Payment.PAYMENT_OPTIONS)
    kinds = [
        {"code": kind, "name": kind_displays[kind]}
        for kind in sorted(
            Payment.objects.values_list("kind", flat=True).distinct().order_by("kind")
        )
    ]
    network_status = [(1,"En Línea"), (2,"Cortado"), (3,"Desconocido")]
    customer_kind = [(1,"Hogar"), (2,"Empresa")]
    technology = [(1,"UTP"), (2,"GPON"), (3, "GEPON"), (4, "Antenna")]
    manual = [(1,"Manual"), (2, "Automático")]
    operator = [(1,"Optic"), (2, "Banda Ancha")]
    conciliado = [(1,"No Conciliado"), (2,"Conciliado")]
    servicio_asignado = [(1,"Sí"), (2,"No")]
    invoice_kind = [(1,"Boleta"), (2,"Factura"), (3,"Nota de crédito"), (4,"Ajuste por cargo"), (5,"Devolución"),
                 (6,"Nota de Cobro"), (7,"Nota de venta"), (8,"Nota de débito"), (9,"Ajuste incobrable")]
    agentsaux = get_user_model().objects.filter(is_active=True)
    agents = []
    for agent in agentsaux:
        agents.append((agent.pk, agent.username))
    exportLimit = getattr(config, "maximo_export")
    return render(
        request,
        "customers/general_report.html",
        {
            "bases": base_model,
            "columns": fields,
            "statuses": statuses,
            "first_of_month": timezone.now().date().replace(day=1).isoformat(),
            "last_of_month": (
                timezone.now().date() + relativedelta(day=31)
            ).isoformat(),
            "sellers": sellers,
            "technicians": technicians,
            "nodes": Node.objects.all(),
            "payment_kind": kinds,
            "network_status": network_status,
            "plans": plans,
            "customer_kind": customer_kind,
            "technology": technology,
            "manual": manual,
            "operator": operator,
            "agents": agents,
            "conciliado": conciliado,
            "servicio_asignado": servicio_asignado,
            "invoice_kind": invoice_kind,
            "exportLimit": exportLimit,
        },
    )

def customer_info(customers, selected_columns,extra_data):
    data_table = []
    KIND = ['Hogar', 'Empresa']
    for i in range(0,min(len(customers)-1,10)):
        customer_info = []
        if '0' in selected_columns:
            customer_info.append(customers[i].rut)
        if '1' in selected_columns:
            customer_info.append(customers[i].name)
        if '2' in selected_columns:
            customer_info.append(customers[i].gender)
        if '3' in selected_columns:
            customer_info.append(KIND[customers[i].kind-1])
        if '4' in selected_columns:
            customer_info.append(customers[i].email)
        if '5' in selected_columns:
            customer_info.append(customers[i].phone)
        if '6' in selected_columns:
            customer_info.append(customers[i].composite_address)
        if '7' in selected_columns:
            customer_info.append(customers[i].commune)
        if '8' in selected_columns:
            customer_info.append(customers[i].aggregated_network_status)
        if '9' in selected_columns:
            customer_info.append(price_tag(customers[i].balance.balance)) if customers[i].balance else customer_info.append('No aplica')
        if '10' in selected_columns:
            customer_info.append(customers[i].created_at.strftime("%d-%m-%Y %H:%M"))
        if i < len(extra_data):
            customer_info += extra_data[i]
        data_table.append(customer_info)
    return data_table
    
def service_info(services, selected_columns, extra_data):
    status_choices = ['Activo', 'Por Retirar', 'Retirado', 'No Instalado', 'Canje', 'Instalación Rechazada',
                      'Moroso', 'Cambio de Titular', 'Descartado', 'Baja Temporal',
                      'Perdido, no se pudo retirar']
    KIND = ['Hogar', 'Empresa']
    TECHNOLOGY = ['UTP', 'GPON', 'GEPON', 'Antenna']
    data_table = []
    for i in range(0,min(len(services),10)):
        service_info = []
        if '0' in selected_columns:
            service_info.append(services[i].number)
        if '1' in selected_columns:
            service_info.append(services[i].customer.rut) if services[i].customer else service_info.append('No aplica')
        if '2' in selected_columns:
            service_info.append(services[i].customer.name) if services[i].customer else service_info.append('No aplica')
        if '3' in selected_columns:
            service_info.append(KIND[services[i].customer.kind-1]) if services[i].customer else service_info.append('No aplica')
        if '4' in selected_columns:
            service_info.append(services[i].customer.email) if services[i].customer else service_info.append('No aplica')
        if '5' in selected_columns:
            service_info.append(services[i].customer.phone) if services[i].customer else service_info.append('No aplica')
        if '6' in selected_columns:
            service_info.append(services[i].composite_address)
        if '7' in selected_columns:
            service_info.append(services[i].commune)
        if '8' in selected_columns:
            if services[i].seen_connected != None:
                if services[i].seen_connected == True: service_info.append("En Línea")
                else: service_info.append("Cortado")
            else: service_info.append("Otro") 
        if '9' in selected_columns:
            service_info.append(status_choices[services[i].status-1])
        if '10' in selected_columns:
            service_info.append(services[i].customer.balance.balance) if services[i].customer else service_info.append('No aplica')
        if '11' in selected_columns:
            service_info.append(services[i].operator.name) if services[i].operator else service_info.append('No aplica')
        if '12' in selected_columns:
            service_info.append(services[i].plan.name) if services[i].plan else service_info.append('No aplica')
        if '13' in selected_columns:
            service_info.append(services[i].created_at.strftime("%d-%m-%Y %H:%M")) 
        if '14' in selected_columns:
            service_info.append(services[i].activated_on.strftime("%d-%m-%Y %H:%M")) if services[i].activated_on else service_info.append('No tiene')
        if '15' in selected_columns:
            service_info.append(services[i].installed_on.strftime("%d-%m-%Y %H:%M")) if services[i].installed_on else service_info.append('No tiene')
        if '16' in selected_columns:
            service_info.append(services[i].uninstalled_on.strftime("%d-%m-%Y %H:%M")) if services[i].uninstalled_on else service_info.append('No tiene')
        if '17' in selected_columns:
            service_info.append(services[i].expired_on.strftime("%d-%m-%Y %H:%M")) if services[i].expired_on else service_info.append('No tiene')
        if '18' in selected_columns:
            service_info.append(TECHNOLOGY[services[i].technology_kind-1])
        if i < len(extra_data):
            service_info += extra_data[i]
        data_table.append(service_info)
    return data_table
def payment_info(payments, selected_columns, extra_data):
    KIND = ['Transbank', 'Cash', 'Cheque', 'Mass Transfer',
            'Servipag', 'BCI Deposit', 'Manual Transfer', 'Webpay', 
            'Other', 'NO registrado en banco', 'Condonación', 
            'Pago Masivo BCI', 'Paypal', 'Multicaja', 'Caja Vecina']
    data_table = []
    for i in range(0,min(len(payments),10)):
        payment_info = []
        if '0' in selected_columns:
            payment_info.append(payments[i].created_at.strftime("%d-%m-%Y %H:%M"))
        if '1' in selected_columns:
            payment_info.append(payments[i].id)
        if '2' in selected_columns:
            payment_info.append(payments[i].customer.name) if payments[i].customer else payment_info.append("No tiene")
        if '3' in selected_columns:
            payment_info.append(str(payments[i].customer_operator)) if payments[i].customer else payment_info.append("No aplica")
        if '4' in selected_columns:
            payment_info.append(payments[i].paid_on.strftime("%d-%m-%Y %H:%M")) if payments[i].paid_on else payment_info.append("No aplica")
        if '5' in selected_columns:
            payment_info.append(payments[i].amount)
        if '6' in selected_columns:
            payment_info.append('Sí') if payments[i].manual else payment_info.append('No')
        if '7' in selected_columns:
            payment_info.append(KIND[payments[i].kind-1])
        if '8' in selected_columns:
            payment_info.append(payments[i].agent.username) if payments[i].agent else payment_info.append("No aplica")
        if '9' in selected_columns:
            payment_info.append(payments[i].operator.name) if payments[i].operator else payment_info.append("No aplica")
        if '10' in selected_columns:
            payment_info.append('Sí') if payments[i].cleared else payment_info.append('No')
        if i < len(extra_data):
            payment_info += extra_data[i]
        data_table.append(payment_info)
    return data_table
def invoice_info(invoices, selected_columns, extra_data):
    data_table = []
    KINDS = ["Boleta", "Factura", "Nota de crédito", "Ajuste por cargo", "Devolución",
                 "Nota de Cobro", "Nota de venta", "Nota de débito", "Ajuste incobrable"]
    for i in range(0,min(len(invoices),10)):
        invoice_info = []
        if '0' in selected_columns:
            invoice_info.append(invoices[i].created_at.strftime("%d-%m-%Y %H:%M"))
        if '1' in selected_columns:
            invoice_info.append(invoices[i].id)
        if '2' in selected_columns:
            invoice_info.append(invoices[i].customer.name) if invoices[i].customer else invoice_info.append("No tiene")
        if '3' in selected_columns:
            invoice_info.append(KINDS[invoices[i].kind-1])
        if '4' in selected_columns:
            invoice_info.append(invoices[i].total)
        if '5' in selected_columns:
            invoice_info.append('Sí') if invoices[i].paid_on else invoice_info.append('No')
        if '6' in selected_columns:
            invoice_info.append(invoices[i].operator.name) if invoices[i].operator else invoice_info.append("No tiene")
        if '7' in selected_columns:
            invoice_info.append(str(invoices[i].customer_operator)) if invoices[i].customer else invoice_info.append("No aplica")
        if '8' in selected_columns:
            invoice_info.append(str(invoices[i].comment))
        if '9' in selected_columns:
            invoice_info.append('Sí') if invoices[i].sended else invoice_info.append('No')
        if i < len(extra_data):
            invoice_info += extra_data[i]
        data_table.append(invoice_info)
    return data_table

def general_table_ajax(request):
    c_customer = {
        '0': 'Rut', '1':'Nombre', '2': 'Genero', '3':'Razón Social', '4':'Email',
        '5':'Teléfono', '6':'Dirección', '7':'Comuna', '8':'Estado de red', '9': 'Balance',
        '10': 'Fecha de Creación'
    }
    c_service = {
        '0': 'Numero', '1':'Rut', '2':'Nombre Cliente', '3':'Razón Social', '4':'Email',
        '5':'Teléfono', '6':'Dirección', '7':'Comuna', '8':'Estado de red',
        '9': 'Status', '10':'Balance', '11': 'Operador', '12': 'Plan', '13': 'Fecha de Creación',
        '14': 'Fecha de Activacion', '15': 'Fecha de Instalación', '16': 'Fecha de Desinstalación',
        '17': 'Fecha de expiración', '18': 'Tecnología'
    }
    c_payment = {
        '0': 'Fecha de Creación',  '1': 'Id', '2': 'Nombre Cliente', '3': 'Operador Cliente',
        '4': 'Fecha de Pago', '5': 'Monto', '6': 'Manual', '7': 'Tipo', '8': 'Agente',
        '9': 'Operador de Pago', '10': 'Conciliado',
    }

    c_invoice = {
        '0':'Fecha de Creación', '1':'Id', '2':'Nombre Cliente', '3': 'Tipo', 
        '4':'Monto', '5':'Pagada',  '6':'Operador Factura', '7':'Operador Cliente',
        '8':'Descripción', '9':'Enviada',
    }
    fields = [c_customer, c_service, c_payment, c_invoice]

    if request.is_ajax():
        base_model = request.POST.get("base_model")
        selected_columns = request.POST.getlist("elements[]")
        filter_name = request.POST.get("filter_name")
        filter_address = request.POST.get("filter_address")
        filter_network = request.POST.get("filter_network")
        filter_status = request.POST.get("filter_status")
        filter_from_balance = request.POST.get("filter_from_balance")
        filter_to_balance = request.POST.get("filter_to_balance")
        filter_plan = request.POST.get("filter_plan")
        filter_customer_kind = request.POST.get("filter_customer_kind")
        filter_created_initDate = date_parser.parse(request.POST.get("filter_created_initDate")) if request.POST.get("filter_created_initDate") else ""
        filter_created_finalDate = date_parser.parse(request.POST.get("filter_created_finalDate")) if request.POST.get("filter_created_finalDate") else ""
        filter_activated_initDate = date_parser.parse(request.POST.get("filter_activated_initDate")) if request.POST.get("filter_activated_initDate") else ""
        filter_activated_finalDate = date_parser.parse(request.POST.get("filter_activated_finalDate")) if request.POST.get("filter_activated_finalDate") else ""
        filter_installed_initDate = date_parser.parse(request.POST.get("filter_installed_initDate")) if request.POST.get("filter_installed_initDate") else ""
        filter_installed_finalDate = date_parser.parse(request.POST.get("filter_installed_finalDate")) if request.POST.get("filter_installed_finalDate") else ""
        filter_uninstalled_initDate = date_parser.parse(request.POST.get("filter_uninstalled_initDate")) if request.POST.get("filter_uninstalled_initDate") else ""
        filter_uninstalled_finalDate = date_parser.parse(request.POST.get("filter_uninstalled_finalDate")) if request.POST.get("filter_uninstalled_finalDate") else ""
        filter_expired_initDate = date_parser.parse(request.POST.get("filter_expired_initDate")) if request.POST.get("filter_expired_initDate") else ""
        filter_expired_finalDate = date_parser.parse(request.POST.get("filter_expired_finalDate")) if request.POST.get("filter_expired_finalDate") else ""
        filter_status_initDate = date_parser.parse(request.POST.get("filter_status_initDate")) if request.POST.get("filter_status_initDate") else ""
        filter_status_finalDate = date_parser.parse(request.POST.get("filter_status_finalDate")) if request.POST.get("filter_status_finalDate") else ""
        filter_technology = request.POST.get("filter_technology")
        filter_payment_method = request.POST.get("filter_payment_method")
        filter_payment_manual = request.POST.get("filter_payment_manual")
        filter_cliente_operator = request.POST.get("filter_payment_cliente_operator")
        filter_payment_agent = request.POST.get("filter_payment_agent")
        filter_payment_operator = request.POST.get("filter_payment_operator")
        filter_payment_conciliado = request.POST.get("filter_payment_conciliado")
        filter_servicio_asignado = request.POST.get("filter_payment_servicio_asignado")
        filter_payment_initDate = date_parser.parse(request.POST.get("filter_payment_initDate")) if request.POST.get("filter_payment_initDate") else ""
        filter_payment_finalDate = date_parser.parse(request.POST.get("filter_payment_initDate")) if request.POST.get("filter_payment_initDate") else ""
        filter_invoice_kind = request.POST.get("filter_invoice_kind")
        filter_from_amount = request.POST.get("filter_from_amount")
        filter_to_amount = request.POST.get("filter_to_amount")
        filter_invoice_pagado = request.POST.get("filter_invoice_pagado")
        filter_invoice_operator = request.POST.get("filter_invoice_operator")
        python_code = request.POST.get("python_code")
        export_data = request.POST.get("export_data")

        columns_names = []
        for x in selected_columns:
            columns_names.append(fields[int(base_model)][x])

        tam = 0
        data_table = []
        if base_model == '0':
            customers = Customer.objects.all().annotate(
                db_composite_address=Concat("street",Value(" "),"house_number",Value(" dpto "),"apartment_number",Value(" torre "),"tower",)
            ).annotate(complete_name = Concat(F("first_name"),Value(" "),F("last_name"),))
            
            if filter_name: 
                customers = name_filter(customers,filter_name)
            if filter_address:
                customers = address_filter(customers,filter_address)
            if filter_network:
                customers = network_filter(customers,filter_network, base_model)
            if filter_from_balance and filter_to_balance:
                customers = balance_filter(customers,filter_from_balance, filter_to_balance)
            if filter_plan:
                customers = plan_filter(customers,filter_plan,base_model)
            if filter_customer_kind:
                customers = customer_kind_filter(customers,filter_customer_kind)
            if filter_created_initDate and filter_created_finalDate:
                customers = created_filter(customers, filter_created_initDate, filter_created_finalDate)
            extra_data = []
            if python_code:
                ids = []
                exec(python_code)
                customers = customers.filter(id__in = ids)
            tam = len(customers)
            data_table = customer_info(customers,selected_columns,extra_data)
            if export_data == '1' and len(customers) <= getattr(config, "maximo_export"):
                return export_report('0', customers, columns_names, selected_columns, extra_data)

        elif base_model == '1':
            services = Service.objects.all().annotate(
                db_composite_address=Concat("street",Value(" "),"house_number",Value(" dpto "),"apartment_number",Value(" torre "),"tower",)
            ).annotate(complete_name = Concat(F("customer__first_name"),Value(" "),F("customer__last_name"),))
            
            if filter_name:
                services = name_filter(services, filter_name)
            if filter_address:
                services = address_filter(services, filter_address)
            if filter_network:
                services = network_filter(services, filter_network, base_model)
            if filter_plan:
                services = plan_filter(services, filter_plan, base_model)
            if filter_created_initDate and filter_created_finalDate:
                services = created_filter(services, filter_created_initDate, filter_created_finalDate)
            if filter_activated_initDate and filter_activated_finalDate:
                services = activated_filter(services, filter_activated_initDate, filter_activated_finalDate)
            if filter_installed_initDate and filter_installed_finalDate:
                services = installed_filter(services, filter_installed_initDate, filter_installed_finalDate)
            if filter_uninstalled_initDate and filter_uninstalled_finalDate:
                services = uninstalled_filter(services, filter_uninstalled_initDate, filter_uninstalled_finalDate)
            if filter_expired_initDate and filter_expired_finalDate:
                services = expired_filter(services, filter_expired_initDate, filter_expired_finalDate)
            if filter_technology:
                services = technology_filter(services, filter_technology)
            if filter_status:
                services = status_filter(services,filter_status,filter_status_initDate, filter_status_finalDate)

            extra_data = []
            if python_code:
                ids = []
                exec(python_code)
                services = services.filter(id__in = ids)
            tam = len(services)
            data_table = service_info(services,selected_columns, extra_data)

            if export_data == '1' and len(services) <= getattr(config, "maximo_export"):
                export_report('1', services, columns_names, selected_columns, extra_data)

        elif base_model == '2':
            payments = Payment.objects.all().annotate(complete_name = Concat(F("customer__first_name"),Value(" "),F("customer__last_name"),))
            
            if filter_name:
                payments = name_filter(payments,filter_name)
            if filter_created_initDate and filter_created_finalDate:
                payments = created_filter(payments, filter_created_initDate, filter_created_finalDate)
            if filter_payment_method:
                payments = payment_method_filter(payments, filter_payment_method)
            if filter_payment_manual:
                payments = payment_manual_filter(payments, filter_payment_manual)
            if filter_cliente_operator:
                payments = cliente_operator_filter(payments, filter_cliente_operator)
            if filter_payment_agent:
                payments = payment_agent_filter(payments, filter_payment_agent)
            if filter_payment_operator:
                payments = payment_operator_filter(payments, filter_payment_operator)
            if filter_payment_conciliado:
                payments = payment_conciliado_filter(payments, filter_payment_conciliado)
            if filter_servicio_asignado:
                payments = servicio_asignado_filter(payments, filter_servicio_asignado)
            if filter_payment_initDate and filter_payment_finalDate:
                payments = paid_on_filter(payments, filter_payment_initDate, filter_payment_finalDate)
            
            extra_data = []
            if python_code:
                ids = []
                exec(python_code)
                payments = payments.filter(id__in = ids)
            tam = len(payments)
            data_table = payment_info(payments,selected_columns, extra_data)
            if export_data == '1' and len(payments) <= getattr(config, "maximo_export"):
                return export_report('2', payments, columns_names, selected_columns, extra_data)

        elif base_model == '3':
            invoices = Invoice.objects.all().annotate(complete_name = Concat(F("customer__first_name"),Value(" "),F("customer__last_name"),))
            if filter_name:
                invoices = name_filter(invoices, filter_name)
            if filter_created_initDate and filter_created_finalDate:
                invoices = created_filter(invoices, filter_created_initDate, filter_created_finalDate)
            if filter_invoice_kind:
                invoices = invoice_kind_filter(invoices, filter_invoice_kind)
            if filter_from_amount and filter_to_amount:
                invoices = amount_filter(invoices, filter_from_amount, filter_to_amount)
            if filter_invoice_pagado:
                invoices = invoice_pagado_filter(invoices, filter_invoice_pagado)
            if filter_invoice_operator:
                invoices = invoice_operator_filter(invoices, filter_invoice_operator)
            if filter_cliente_operator:
                invoices = cliente_operator_filter(invoices, filter_cliente_operator)
            if filter_servicio_asignado:
                invoices = servicio_asignado_filter(invoices, filter_servicio_asignado)
            
            extra_data = []
            if python_code:
                ids = []
                exec(python_code)
                invoices = invoices.filter(id__in = ids)
            tam = len(invoices)
            data_table = invoice_info(invoices,selected_columns,extra_data)
            if export_data == '1' and len(invoices) <= getattr(config, "maximo_export"):
                export_report('3', invoices, columns_names, selected_columns, extra_data)

        data = json.dumps(
            {
                "columns_names": columns_names,
                "data_table": data_table,
                "length": tam,
            }
        )

        return HttpResponse(data, content_type="application/json")

@celery_app.task
def export_report(base_model, data, columns_names, selected_columns, extra_data):

    output_file = io.BytesIO()
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet("Reporte")
    col = 0
    for i in columns_names:
        worksheet.write(0,col,i)
        col += 1

    if base_model == '0':
        KIND = ['Hogar', 'Empresa']
        for i in range(0,len(data)):
            print(i)
            col = 0
            if '0' in selected_columns:
                worksheet.write(i+1,col,data[i].rut)
                col += 1
            if '1' in selected_columns:
                worksheet.write(i+1,col,data[i].name)
                col += 1
            if '2' in selected_columns:
                worksheet.write(i+1,col,data[i].gender)
                col += 1
            if '3' in selected_columns:
                worksheet.write(i+1,col,KIND[data[i].kind-1])
                col += 1
            if '4' in selected_columns:
                worksheet.write(i+1,col,data[i].email)
                col += 1
            if '5' in selected_columns:
                worksheet.write(i+1,col,data[i].phone)
                col += 1
            if '6' in selected_columns:
                worksheet.write(i+1,col,data[i].composite_address)
                col += 1
            if '7' in selected_columns:
                worksheet.write(i+1,col,data[i].commune)
                col += 1
            if '8' in selected_columns:
                worksheet.write(i+1,col,data[i].aggregated_network_status)
                col += 1
            if '9' in selected_columns:
                worksheet.write(i+1,col,data[i].balance.balance) if data[i].balance else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '10' in selected_columns:
                worksheet.write(i+1,col,data[i].created_at.strftime("%d-%m-%Y %H:%M"))
                col += 1
            if extra_data:
                for extra in extra_data[i]:
                    worksheet.write(i+1,col,extra)
                    col += 1
    elif base_model == '1':
        status_choices = ['Activo', 'Por Retirar', 'Retirado', 'No Instalado', 'Canje', 'Instalación Rechazada',
                        'Moroso', 'Cambio de Titular', 'Descartado', 'Baja Temporal',
                        'Perdido, no se pudo retirar']
        KIND = ['Hogar', 'Empresa']
        TECHNOLOGY = ['UTP', 'GPON', 'GEPON', 'Antenna']
        for i in range(0,len(data)):
            print(i)
            col = 0
            if '0' in selected_columns:
                worksheet.write(i+1,col,data[i].number)
                col += 1
            if '1' in selected_columns:
                worksheet.write(i+1,col,data[i].customer.rut) if data[i].customer else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '2' in selected_columns:
                worksheet.write(i+1,col,data[i].customer.name) if data[i].customer else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '3' in selected_columns:
                worksheet.write(i+1,col,KIND[data[i].customer.kind-1]) if data[i].customer else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '4' in selected_columns:
                worksheet.write(i+1,col,data[i].customer.email) if data[i].customer else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '5' in selected_columns:
                worksheet.write(i+1,col,data[i].customer.phone) if data[i].customer else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '6' in selected_columns:
                worksheet.write(i+1,col,data[i].composite_address)
                col += 1
            if '7' in selected_columns:
                worksheet.write(i+1,col,data[i].commune)
                col += 1
            if '8' in selected_columns:
                if data[i].seen_connected != None:
                    if data[i].seen_connected == True: worksheet.write(i+1,col,"En Línea")
                    else: worksheet.write(i+1,col,"Cortado")
                else: worksheet.write(i+1,col,"Otro")
                col += 1
            if '9' in selected_columns:
                worksheet.write(i+1,col,status_choices[data[i].status-1])
                col += 1
            if '10' in selected_columns:
                worksheet.write(i+1,col,data[i].customer.balance.balance) if data[i].customer else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '11' in selected_columns:
                worksheet.write(i+1,col,data[i].operator.name) if data[i].operator else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '12' in selected_columns:
                worksheet.write(i+1,col,data[i].plan.name) if data[i].plan else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '13' in selected_columns:
                worksheet.write(i+1,col,data[i].created_at.strftime("%d-%m-%Y %H:%M"))
                col += 1
            if '14' in selected_columns:
                worksheet.write(i+1,col,data[i].activated_on.strftime("%d-%m-%Y %H:%M")) if data[i].activated_on else worksheet.write(i+1,col,"No tiene")
                col += 1
            if '15' in selected_columns:
                worksheet.write(i+1,col,data[i].installed_on.strftime("%d-%m-%Y %H:%M")) if data[i].installed_on else worksheet.write(i+1,col,"No tiene")
                col += 1
            if '16' in selected_columns:
                worksheet.write(i+1,col,data[i].uninstalled_on.strftime("%d-%m-%Y %H:%M")) if data[i].uninstalled_on else worksheet.write(i+1,col,"No tiene")
                col += 1
            if '17' in selected_columns:
                worksheet.write(i+1,col,data[i].expired_on.strftime("%d-%m-%Y %H:%M")) if data[i].expired_on else worksheet.write(i+1,col,"No tiene")
                col += 1
            if '18' in selected_columns:
                worksheet.write(i+1,col,TECHNOLOGY[data[i].technology_kind-1])
                col += 1
            if extra_data:
                for extra in extra_data[i]:
                    worksheet.write(i+1,col,extra)
                    col += 1
    elif base_model == '2':
        KIND = ['Transbank', 'Cash', 'Cheque', 'Mass Transfer',
            'Servipag', 'BCI Deposit', 'Manual Transfer', 'Webpay', 
            'Other', 'NO registrado en banco', 'Condonación', 
            'Pago Masivo BCI', 'Paypal', 'Multicaja', 'Caja Vecina']
        for i in range(0,len(data)):
            print(i)
            col = 0
            if '0' in selected_columns:
                worksheet.write(i+1,col,data[i].created_at.strftime("%d-%m-%Y %H:%M"))
                col += 1
            if '1' in selected_columns:
                worksheet.write(i+1,col,data[i].id)
                col += 1
            if '2' in selected_columns:
                worksheet.write(i+1,col,data[i].customer.name) if data[i].customer else worksheet.write(i+1,col,"No tiene")
                col += 1
            if '3' in selected_columns:
                worksheet.write(i+1,col,str(data[i].customer_operator)) if data[i].customer else worksheet.write(i+1,col,"No aplica")
                col += 1
            if '4' in selected_columns:
                worksheet.write(i+1,col,data[i].paid_on.strftime("%d-%m-%Y %H:%M")) if data[i].paid_on else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '5' in selected_columns:
                worksheet.write(i+1,col,data[i].amount)
                col += 1
            if '6' in selected_columns:
                worksheet.write(i+1,col,'Sí') if data[i].manual else worksheet.write(i+1,col,'No')
                col += 1
            if '7' in selected_columns:
                worksheet.write(i+1,col,KIND[data[i].kind-1])
                col += 1
            if '8' in selected_columns:
                worksheet.write(i+1,col,data[i].agent.username) if data[i].agent else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '9' in selected_columns:
                worksheet.write(i+1,col,data[i].operator.name) if data[i].operator else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '10' in selected_columns:
                worksheet.write(i+1,col,'Sí') if data[i].cleared else worksheet.write(i+1,col,'No')
                col += 1
            if extra_data:
                for extra in extra_data[i]:
                    worksheet.write(i+1,col,extra)
                    col += 1
    elif base_model == '3':
        KINDS = ["Boleta", "Factura", "Nota de crédito", "Ajuste por cargo", "Devolución",
                 "Nota de Cobro", "Nota de venta", "Nota de débito", "Ajuste incobrable"]
        for i in range(0,len(data)):
            print(i)
            col = 0
            if '0' in selected_columns:
                worksheet.write(i+1,col,data[i].created_at.strftime("%d-%m-%Y %H:%M"))
                col += 1
            if '1' in selected_columns:
                worksheet.write(i+1,col,data[i].id)
                col += 1
            if '2' in selected_columns:
                worksheet.write(i+1,col,data[i].customer.name) if data[i].customer else worksheet.write(i+1,col,"No tiene")
                col += 1
            if '3' in selected_columns:
                worksheet.write(i+1,col,KINDS[data[i].kind-1])
                col += 1
            if '4' in selected_columns:
                worksheet.write(i+1,col,data[i].total)
                col += 1
            if '5' in selected_columns:
                worksheet.write(i+1,col,'Sí') if data[i].paid_on else worksheet.write(i+1,col,'No')
                col += 1
            if '6' in selected_columns:
                worksheet.write(i+1,col,data[i].operator.name) if data[i].operator else worksheet.write(i+1,col,'No aplica')
                col += 1
            if '7' in selected_columns:
                worksheet.write(i+1,col,str(data[i].customer_operator)) if data[i].customer else worksheet.write(i+1,col,"No aplica")
                col += 1
            if '8' in selected_columns:
                worksheet.write(i+1,col,str(data[i].comment))
                col += 1
            if '9' in selected_columns:
                worksheet.write(i+1,col,'Sí') if data[i].sended else worksheet.write(i+1,col,'No')
                col += 1
            if extra_data:
                for extra in extra_data[i]:
                    worksheet.write(i+1,col,extra)
                    col += 1
    workbook.close()
    output_file.seek(0)

    response = HttpResponse(
        output_file.read(),
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response["Content-Disposition"] = ('attachment; filename="reporte personalizado '
        + timezone.now().strftime("%d-%m-%Y")
        + '.xlsx"')
    return response

@csrf_exempt
def prueba(request):
    c_customer = {
        '0': 'Rut', '1':'Nombre', '2': 'Genero', '3':'Razón Social', '4':'Email',
        '5':'Teléfono', '6':'Dirección', '7':'Comuna', '8':'Estado de red', '9': 'Balance',
        '10': 'Fecha de Creación'
    }
    c_service = {
        '0': 'Numero', '1':'Rut', '2':'Nombre Cliente', '3':'Razón Social', '4':'Email',
        '5':'Teléfono', '6':'Dirección', '7':'Comuna', '8':'Estado de red',
        '9': 'Status', '10':'Balance', '11': 'Operador', '12': 'Plan', '13': 'Fecha de Creación',
        '14': 'Fecha de Activacion', '15': 'Fecha de Instalación', '16': 'Fecha de Desinstalación',
        '17': 'Fecha de expiración', '18': 'Tecnología'
    }
    c_payment = {
        '0': 'Fecha de Creación',  '1': 'Id', '2': 'Nombre Cliente', '3': 'Operador Cliente',
        '4': 'Fecha de Pago', '5': 'Monto', '6': 'Manual', '7': 'Tipo', '8': 'Agente',
        '9': 'Operador de Pago', '10': 'Conciliado',
    }

    c_invoice = {
        '0':'Fecha de Creación', '1':'Id', '2':'Nombre Cliente', '3': 'Tipo', 
        '4':'Monto', '5':'Pagada',  '6':'Operador Factura', '7':'Operador Cliente',
        '8':'Descripción', '9':'Enviada',
    }
    fields = [c_customer, c_service, c_payment, c_invoice]

    base_model = request.POST.get("base_model")
    selected_columns = request.POST.get("elements").split(',')
    filter_name = request.POST.get("filter_name")
    filter_address = request.POST.get("filter_address")
    filter_network = request.POST.get("filter_network")
    filter_status = request.POST.get("filter_status")
    filter_from_balance = request.POST.get("filter_from_balance")
    filter_to_balance = request.POST.get("filter_to_balance")
    filter_plan = request.POST.get("filter_plan")
    filter_customer_kind = request.POST.get("filter_customer_kind")
    filter_created_initDate = date_parser.parse(request.POST.get("filter_created_initDate")) if request.POST.get("filter_created_initDate") else ""
    filter_created_finalDate = date_parser.parse(request.POST.get("filter_created_finalDate")) if request.POST.get("filter_created_finalDate") else ""
    filter_activated_initDate = date_parser.parse(request.POST.get("filter_activated_initDate")) if request.POST.get("filter_activated_initDate") else ""
    filter_activated_finalDate = date_parser.parse(request.POST.get("filter_activated_finalDate")) if request.POST.get("filter_activated_finalDate") else ""
    filter_installed_initDate = date_parser.parse(request.POST.get("filter_installed_initDate")) if request.POST.get("filter_installed_initDate") else ""
    filter_installed_finalDate = date_parser.parse(request.POST.get("filter_installed_finalDate")) if request.POST.get("filter_installed_finalDate") else ""
    filter_uninstalled_initDate = date_parser.parse(request.POST.get("filter_uninstalled_initDate")) if request.POST.get("filter_uninstalled_initDate") else ""
    filter_uninstalled_finalDate = date_parser.parse(request.POST.get("filter_uninstalled_finalDate")) if request.POST.get("filter_uninstalled_finalDate") else ""
    filter_expired_initDate = date_parser.parse(request.POST.get("filter_expired_initDate")) if request.POST.get("filter_expired_initDate") else ""
    filter_expired_finalDate = date_parser.parse(request.POST.get("filter_expired_finalDate")) if request.POST.get("filter_expired_finalDate") else ""
    filter_status_initDate = date_parser.parse(request.POST.get("filter_status_initDate")) if request.POST.get("filter_status_initDate") else ""
    filter_status_finalDate = date_parser.parse(request.POST.get("filter_status_finalDate")) if request.POST.get("filter_status_finalDate") else ""
    filter_technology = request.POST.get("filter_technology")
    filter_payment_method = request.POST.get("filter_payment_method")
    filter_payment_manual = request.POST.get("filter_payment_manual")
    filter_cliente_operator = request.POST.get("filter_payment_cliente_operator")
    filter_payment_agent = request.POST.get("filter_payment_agent")
    filter_payment_operator = request.POST.get("filter_payment_operator")
    filter_payment_conciliado = request.POST.get("filter_payment_conciliado")
    filter_servicio_asignado = request.POST.get("filter_payment_servicio_asignado")
    filter_payment_initDate = date_parser.parse(request.POST.get("filter_payment_initDate")) if request.POST.get("filter_payment_initDate") else ""
    filter_payment_finalDate = date_parser.parse(request.POST.get("filter_payment_initDate")) if request.POST.get("filter_payment_initDate") else ""
    filter_invoice_kind = request.POST.get("filter_invoice_kind")
    filter_from_amount = request.POST.get("filter_from_amount")
    filter_to_amount = request.POST.get("filter_to_amount")
    filter_invoice_pagado = request.POST.get("filter_invoice_pagado")
    filter_invoice_operator = request.POST.get("filter_invoice_operator")
    python_code = request.POST.get("python_code")
    export_data = request.POST.get("export_data")

    columns_names = []
    if selected_columns:
        for x in selected_columns:
            columns_names.append(fields[int(base_model)][x])
    
    if base_model == '0':
        customers = Customer.objects.all().annotate(
            db_composite_address=Concat("street",Value(" "),"house_number",Value(" dpto "),"apartment_number",Value(" torre "),"tower",)
        ).annotate(complete_name = Concat(F("first_name"),Value(" "),F("last_name"),))
        
        if filter_name: 
            customers = name_filter(customers,filter_name)
        if filter_address:
            customers = address_filter(customers,filter_address)
        if filter_network:
            customers = network_filter(customers,filter_network, base_model)
        if filter_from_balance and filter_to_balance:
            customers = balance_filter(customers,filter_from_balance, filter_to_balance)
        if filter_plan:
            customers = plan_filter(customers,filter_plan,base_model)
        if filter_customer_kind:
            customers = customer_kind_filter(customers,filter_customer_kind)
        if filter_created_initDate and filter_created_finalDate:
            customers = created_filter(customers, filter_created_initDate, filter_created_finalDate)
        extra_data = []
        if python_code:
            ids = []
            exec(python_code)
            customers = customers.filter(id__in = ids)
        if export_data == '1' and len(customers) <= getattr(config, "maximo_export"):
            return export_report('0', customers, columns_names, selected_columns, extra_data)
    elif base_model == '1':
        services = Service.objects.all().annotate(
            db_composite_address=Concat("street",Value(" "),"house_number",Value(" dpto "),"apartment_number",Value(" torre "),"tower",)
        ).annotate(complete_name = Concat(F("customer__first_name"),Value(" "),F("customer__last_name"),))
        
        if filter_name:
            services = name_filter(services, filter_name)
        if filter_address:
            services = address_filter(services, filter_address)
        if filter_network:
            services = network_filter(services, filter_network, base_model)
        if filter_plan:
            services = plan_filter(services, filter_plan, base_model)
        if filter_created_initDate and filter_created_finalDate:
            services = created_filter(services, filter_created_initDate, filter_created_finalDate)
        if filter_activated_initDate and filter_activated_finalDate:
            services = activated_filter(services, filter_activated_initDate, filter_activated_finalDate)
        if filter_installed_initDate and filter_installed_finalDate:
            services = installed_filter(services, filter_installed_initDate, filter_installed_finalDate)
        if filter_uninstalled_initDate and filter_uninstalled_finalDate:
            services = uninstalled_filter(services, filter_uninstalled_initDate, filter_uninstalled_finalDate)
        if filter_expired_initDate and filter_expired_finalDate:
            services = expired_filter(services, filter_expired_initDate, filter_expired_finalDate)
        if filter_technology:
            services = technology_filter(services, filter_technology)
        if filter_status:
            services = status_filter(services,filter_status,filter_status_initDate, filter_status_finalDate)

        extra_data = []
        if python_code:
            ids = []
            exec(python_code)
            services = services.filter(id__in = ids)
        if export_data == '1' and len(services) <= getattr(config, "maximo_export"):
            return export_report('1', services, columns_names, selected_columns, extra_data)
    elif base_model == '2':
        payments = Payment.objects.all().annotate(complete_name = Concat(F("customer__first_name"),Value(" "),F("customer__last_name"),))
        
        if filter_name:
            payments = name_filter(payments,filter_name)
        if filter_created_initDate and filter_created_finalDate:
            payments = created_filter(payments, filter_created_initDate, filter_created_finalDate)
        if filter_payment_method:
            payments = payment_method_filter(payments, filter_payment_method)
        if filter_payment_manual:
            payments = payment_manual_filter(payments, filter_payment_manual)
        if filter_cliente_operator:
            payments = cliente_operator_filter(payments, filter_cliente_operator)
        if filter_payment_agent:
            payments = payment_agent_filter(payments, filter_payment_agent)
        if filter_payment_operator:
            payments = payment_operator_filter(payments, filter_payment_operator)
        if filter_payment_conciliado:
            payments = payment_conciliado_filter(payments, filter_payment_conciliado)
        if filter_servicio_asignado:
            payments = servicio_asignado_filter(payments, filter_servicio_asignado)
        if filter_payment_initDate and filter_payment_finalDate:
            payments = paid_on_filter(payments, filter_payment_initDate, filter_payment_finalDate)
        
        extra_data = []
        if python_code:
            ids = []
            exec(python_code)
            payments = payments.filter(id__in = ids)
        if export_data == '1' and len(payments) <= getattr(config, "maximo_export"):
            return export_report('2', payments, columns_names, selected_columns, extra_data)
    elif base_model == '3':
        invoices = Invoice.objects.all().annotate(complete_name = Concat(F("customer__first_name"),Value(" "),F("customer__last_name"),))
        if filter_name:
            invoices = name_filter(invoices, filter_name)
        if filter_created_initDate and filter_created_finalDate:
            invoices = created_filter(invoices, filter_created_initDate, filter_created_finalDate)
        if filter_invoice_kind:
            invoices = invoice_kind_filter(invoices, filter_invoice_kind)
        if filter_from_amount and filter_to_amount:
            invoices = amount_filter(invoices, filter_from_amount, filter_to_amount)
        if filter_invoice_pagado:
            invoices = invoice_pagado_filter(invoices, filter_invoice_pagado)
        if filter_invoice_operator:
            invoices = invoice_operator_filter(invoices, filter_invoice_operator)
        if filter_cliente_operator:
            invoices = cliente_operator_filter(invoices, filter_cliente_operator)
        if filter_servicio_asignado:
            invoices = servicio_asignado_filter(invoices, filter_servicio_asignado)
        
        extra_data = []
        if python_code:
            ids = []
            exec(python_code)
            invoices = invoices.filter(id__in = ids)
        if export_data == '1' and len(invoices) <= getattr(config, "maximo_export"):
            return export_report('3', invoices, columns_names, selected_columns, extra_data)

    json = []
    return JsonResponse(json, safe = False)