from .views import ( ServiceStatusLogViewSet, ServiceSumSnapshotViewSet)

# API section
from customers.urls import router
from .views import *
from django.conf.urls import url

router.register(r"service-status-logs", ServiceStatusLogViewSet)
router.register(r"service-sum-snapshots", ServiceSumSnapshotViewSet)

urlpatterns = [
    url(r"^general_report/", general_report, name = 'general_report'),
    url(r"^general-table-ajax/$", general_table_ajax, name="general-table-ajax"),
]
