from django.db import models
from django.db.models.fields import TextField
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from customers.models import Service
from django.contrib.postgres.fields import JSONField


class ServiceSumSnapshot(models.Model):
   created_at = models.DateField(default=timezone.now)
   status = models.PositiveSmallIntegerField(choices=Service.STATUS_CHOICES)
   count = models.PositiveIntegerField(default=0)


class ServiceStatusLog(models.Model):
   created_at = models.DateTimeField(default=timezone.now)
   status = models.PositiveSmallIntegerField(choices=Service.STATUS_CHOICES)
   seller = models.ForeignKey('customers.seller', verbose_name=_('seller'), null=True)
   node = models.ForeignKey('infra.Node', verbose_name=_('node'), null=True)
   technician = models.ForeignKey('hr.Technician', verbose_name=_('technician'), null=True)
   service = models.ForeignKey('customers.Service', verbose_name=('service'), null=True)

   def __str__(self):
      return f'{self.node} - {self.status} - {self.created_at}'

class ServiceChangeStatusLog(models.Model):
   HSU=1
   FIBRA=2
   KIND_CHOICES = (
        (HSU, _("HSU")),
        (FIBRA, _("Fibra")),
   )

   created_at = models.DateTimeField(default=timezone.now)
   status = models.PositiveSmallIntegerField(choices=Service.STATUS_CHOICES)
   prev_status = models.PositiveSmallIntegerField(choices=Service.STATUS_CHOICES)
   address = models.CharField(_('address'), max_length=255)
   plan = models.ForeignKey('customers.Plan', verbose_name=_('plan'))
   service = models.ForeignKey('customers.Service', verbose_name=('service'))
   kind = models.PositiveSmallIntegerField(
        _("Categoria"), default=HSU, choices=KIND_CHOICES
   )
   empresa = models.BooleanField(default=False)

   def __str__(self):
      return f'{self.service} - {self.status} - {self.created_at}'

class Report(models.Model):
   CLIENTES = 1
   SERVICIOS = 2
   PAGOS = 3
   BOLETAS = 4
   BASE_MODEL_CHOICES = (
        (CLIENTES, _("Clientes")),
        (SERVICIOS, _("Servicios")),
        (PAGOS, _("Pagos")),
        (BOLETAS, _("Boletas")),
   )

   name = models.CharField(_("name"), max_length=255, null = True)
   created_at = models.DateTimeField(default=timezone.now)
   base_model = models.PositiveSmallIntegerField(_("base_model"), choices=BASE_MODEL_CHOICES, null = True)
   selected_columns = JSONField(_("selected_columns"), null=True, blank=False)
   selected_filters = JSONField(_("selected_filters"), null=True, blank=False)
   python_code = models.TextField(_("python_code"), null = True, blank=True)

   def __str__(self):
      return f'{self.name}'

