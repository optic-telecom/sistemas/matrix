from django.contrib import admin
from .models import ServiceSumSnapshot, ServiceStatusLog, ServiceChangeStatusLog
from import_export.admin import ImportExportModelAdmin
from .resources import ServiceSumSnapshot, ServiceStatusLogResource, ServiceChangeStatusLogResource

@admin.register(ServiceSumSnapshot)
class CustomerAdmin(ImportExportModelAdmin):
    list_display = ('created_at', 'status', 'count')
    resource_class = ServiceSumSnapshot

@admin.register(ServiceStatusLog)
class ServiceStatusLogAdmin(ImportExportModelAdmin):
    resource_class = ServiceStatusLogResource

@admin.register(ServiceChangeStatusLog)
class ServiceChangeStatusLogAdmin(ImportExportModelAdmin):
    resource_class = ServiceChangeStatusLogResource