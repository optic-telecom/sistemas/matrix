import requests
from django.db.models.signals import post_save
from django.dispatch import receiver
from reports.models import ServiceStatusLog, ServiceChangeStatusLog
from constance import config
from customers.models import Service
from django.utils import timezone


@receiver(post_save, sender=Service)
def update_snapshot(sender, **kwargs):
    i = kwargs['instance']
    
    if i._original_status != i.status:
        if getattr(config, "allow_status_validation") == 'yes':
            ServiceStatusLog.objects.create(created_at=timezone.now(),
                                            status=i.status,
                                            node=i.node,
                                            seller=i.seller,
                                            technician=i.technician,
                                            service=i)
@receiver(post_save, sender=Service)
def register_change_snapshot(sender, **kwargs):
    i = kwargs['instance']
    
    if i._original_status != i.status:

        kind=1
        try:
            if i.node.is_optical_fiber:
                kind=2
        except:
            kind=1
            pass

        if i.plan.type_plan==2:
            kind_plan=True
        else:
            kind_plan=False

        ServiceChangeStatusLog.objects.create(created_at=timezone.now(),
                                            status=i.status,
                                            prev_status=i._original_status,
                                            address=i.best_address,
                                            plan=i.plan,
                                            empresa=kind_plan,
                                            kind=kind,
                                            service=i)