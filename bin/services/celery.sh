#!/bin/bash
LOGFILE=/home/matrix/matrix_backend/logs/celery.log
LOGDIR=$(dirname $LOGFILE)
source /home/matrix/matrix_env/bin/activate
cd /home/matrix/matrix_backend/matrix
exec celery worker -A tasks -B -Q queue1 -l WARNING --logfile=$LOGFILE 2>>$LOGFILE 