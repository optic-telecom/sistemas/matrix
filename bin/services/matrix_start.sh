#!/bin/bash

NAME="Servidor_matrix" # Name of the application
DJANGODIR=/home/matrix/matrix_backend/matrix/ # Django project directory
LOGFILE=/home/matrix/matrix_backend/logs/gunicorn_matrix.log
LOGDIR=$(dirname $LOGFILE)
SOCKFILE=/home/matrix/matrix_backend/gunicorn.sock # we will communicate using this unix socket
LOGDIR_PROJECT=/home/matrix/matrix_backend/logs # we will communicate using this unix socket
USER=matrix # the user to run as
GROUP=matrix # the group to run as
NUM_CPUS=$(nproc --all) # 3 how many worker processes should Gunicorn spawn
NUM_WORKERS=6;
DJANGO_SETTINGS_MODULE=config.settings.$1 # which settings file should Django use
DJANGO_WSGI_MODULE=config.wsgi
TIMEOUT=600

# if [ $NUM_CPUS -eq 2 ]
# then
# 	NUM_WORKERS=2;
# else
# 	NUM_WORKERS= NUM_CPUS - 2;
# fi

echo "Starting $NAME as `whoami`"

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
# Create the run directory if it doesn't exist for logs sentinel
test -d $LOGDIR_PROJECT || mkdir -p $LOGDIR_PROJECT

source /home/matrix/matrix_env/bin/activate

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/matrix/matrix_env/bin/gunicorn  ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--bind=unix:$SOCKFILE \
--log-level=debug \
--timeout $TIMEOUT \
--capture-output \
--log-file=$LOGFILE 2>>$LOGFILE
