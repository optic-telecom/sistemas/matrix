#!/bin/bash
export MATIC_DIR=$HOME/.config/matic
export REDIS_DIR=$MATIC_DIR
envsubst < redis.conf | redis-server -
